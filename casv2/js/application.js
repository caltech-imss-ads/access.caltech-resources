// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

// always blow out of frames
if (window != top) top.location.href = location.href;

// If this page has a login element, put the cursor in it
window.onload = focusForm;
function focusForm() {
    if(document.getElementById("login")){document.getElementById("login").focus();} 
}

// A smidge of js for our bulk upload functionality
var swfuploadSuccess = function (file, server_data, receivedResponse) {
  var progress = new FileProgress(file,  this.customSettings.upload_target);
  progress.setStatus("Complete.");
  progress.toggleCancel(false);
};

// try to add hide/show effect from jquery with unobtrusive js
// http://www.dtelepathy.com/blog/telepathy/unobtrusive-javascript-saves-you-time-money-keeps-your-website-agile/
//
// In the html, create a link with class="hiddenStuff" and an href set to 
// # + the id of the div you want to show or hide. The hidden div should have 
// the class "conceal" so we can start off with it hidden iff JavaScript is turned on

$(document).ready(function(){
    preHideByClass();
    $('a.showToggle').click(function(e){
        e.preventDefault();
        var reveal = $(this)[0].href.split('#')[1];
        if($('#'+reveal).css('display') == 'none'){
            $('#'+reveal).show();
        }else{
            $('#'+reveal).hide();
        }
        return false;
    });
});

/* We only want the concealed stuff to start out hidden if the user has JavaScript turned on  */
var preHideByClass = function(){
    var divs = $("div.conceal");
    divs.css('display', 'none');
};


/* swapImgRestore and swapImage added to control rollover social media icons added to right sidebar */
//-------------------------------------------------------------

function MM_swapImgRestore() { //v3.0
    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

//-------------------------------------------------------------

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

//-------------------------------------------------------------

function MM_swapImage() { //v3.0
    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
									if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}