function search_people() {
	options = "toolbar=0,status=0,menubar=0,resizable=1,scrollbars=1,top=0,width=800,height=690";
	h_win = window.open('','_sat', options);
	document.PopupForm.action = '/events/contact/person_search';
	document.PopupForm.target = '_sat';
	
	// substitute if needed so that a blank search returns a list of all people
	if (document.contact.search_name.value != "") {
		document.PopupForm.search_string.value = document.contact.search_name.value;
	}
	else {
		document.PopupForm.search_string.value = "%";
	}
	
	document.PopupForm.submit();
}

function selectEventsSpouse(id, first_name, middle_name, last_name) {
	document.contact.search_spouse_first_name.value = first_name;
	document.contact.search_spouse_middle_name.value = "";
	document.contact.search_spouse_last_name.value = last_name;
	document.contact.spouse_events_person_id.value = id;
	
	if (middle_name != null && middle_name.length > 0) {
		document.contact.search_spouse_middle_name.value = middle_name;
		middle_name = middle_name + " ";
	}
	document.contact.search_name.value = first_name + " " + middle_name + last_name;
	
	document.contact.target="_self";
	h_win.close();	
}

$.validity.outputs.addPerson = {
	// The start function will be called when validation starts.
	// This allows you to prepare the page for validation, for instance
	// you might remove any validation messages that are already on the page.
	start: function () { 
		$("input").removeClass("ui-state-error").each(function () {
			if ($(this).val() === "-- REQUIRED --") {
				$(this).val("")
			}
		});
		$("select,li").removeClass("ui-state-error");
	},
	
	// The raise function is called to raise an error for a specific input.
	// The first argument is a jQuery object of the input to raise the error message for.
	// The second argument is the string of the error message.
	raise: function ($obj, msg) {
		$obj.addClass("ui-state-error");
		if ($obj.attr("type") === "text") {
			$obj.val("-- REQUIRED --");
		}
		var p = $obj.parent().attr("id");
		$("a[href=#"+p+"]").parent().addClass("ui-state-error");
	},
};

var check_spouse = function () {
	var valid = false;
	var val = $("input[name='spouse_source']:checked").val();
	if (val === "manual") {
		$("input[name='manual_spouse_first_name']").require();
		$("input[name='manual_spouse_last_name']").require();
		valid = true;
	} else if (val === "search") {
		$("input[name='search_name']").require();
		$("input[name='search_spouse_first_name']").require();
		$("input[name='search_spouse_last_name']").require();
		valid = true;
	} else if (val === "nospouse") {
		valid = true;
	}
	return valid;
};

$(document).ready(function () {
	$.validity.setup({ outputMode:'addPerson' });
	$('div.jquery_tabs').tabs();
	$("#btn_save_person").button().click(function () {
		$("#frm_contact").validity(function () {
			$("select").assert(function (ele) {
				return ele.value !== "None";
			});
			$("input[name='first_name']").require();
			$("input[name='last_name']").require();
			$("input[name='phone_name_new1']").require();
			$("input[name='phone_number_new1']").require();
			$("input[name='email_name_new1']").require();
			$("input[name='email_address_new1']").require().match('email');
			$("input[name='address_name_new1']").require();
			$("input[name='address_line_1_new1']").require();
			$("input[name='spouse_source']").assert(check_spouse);
		});
	});
	$("input[type=text]").focus(function () { 
		this.select();
	}).change(function  () {
		if ($(this).val().length) {
			$(this).removeClass("ui-state-error");
		}
	}).keyup(function  () {
		if ($(this).val().length) {
			$(this).removeClass("ui-state-error");
		}
	});
	$("select").change(function () {
		$(this).removeClass("ui-state-error");
	});
});
