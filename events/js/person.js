if ((typeof(adsEvent) == "undefined") || !adsEvent) {
	adsEvent = {};
}
if ((typeof(adsEvent.vars) == "undefined") || !adsEvent.vars) {
	adsEvent.vars = {};
}
if ((typeof(adsEvent.methods) == "undefined") || !adsEvent.methods) {
	adsEvent.methods = {};
}

function addEntryBlock(renderBlock, type) {
	var base = ".pane > ul";
	if (type === "phone") {
		base = "#phone_pane" + base;
	} else if (type === "email") {
		base = "#email_pane" + base;
	} else if (type === "address") {
		base = "#address_pane" + base;
	} else if (type === "child_first") {
		base = "#family_pane" + base;
	}
	var nextIsEven = $(base + " > li:last").hasClass('odd');
	var rowclass = null
	if (nextIsEven == true) {
		rowclass = ""
	}
	else {
		rowclass = ' class="odd"'
	} 
	var nextID = null;
	var firstInput = $(base + " > li:last > ul input:first");
	if (firstInput.length > 0) {
		var lastID = firstInput.attr('name').substr(type.length + 6);
		if (lastID.substr(0, 3) != "new") {
			nextID = "new1"
		} else {
			var cur_id = parseInt(lastID.substr(3));
			nextID = "new" + (cur_id + 1);
		}
	} else {
		nextID = "new1"
	}
	$(base).append(renderBlock(rowclass, nextID));
}

function renderPhoneBlock(rowclass, nextID) {
	lines = [];
	lines.push('<li' + rowclass + '>');
	lines.push('<input type="radio" name="default_phone" value="' + nextID + '"/>');
	lines.push('<ul><li>');
	lines.push('<span style="font-weight:bold">Phone Label</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" name="phone_name_' + nextID + '" value="" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">Phone Number</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" name="phone_number_' + nextID + '" value="(626) " />');
	lines.push('</li></ul>');
	lines.push('<input type="hidden" name="add_phone" value="' + nextID + '" />');
	lines.push('<input type="button" name="delete_' + nextID + '" value="Delete" onclick="markBlockForDeletion(\'' + nextID + '\', \'Phone\', \'phone\');" />');
	lines.push('</li>');
	return(lines.join("\n"));
}

function addPhoneEntryBlock() {
	addEntryBlock(renderPhoneBlock, 'phone');
}

function renderChildBlock(rowclass, nextID) {
	lines = [];
	lines.push('<li' + rowclass + '>');
	lines.push('<ul><li>');
	lines.push('<div class="child_label">Name:</div>');
	lines.push('<input type="text" name="child_first_name_' + nextID + '" value="" />');
	lines.push('<input type="text" name="child_middle_name_' + nextID + '" value="" />');
	lines.push('<input type="text" name="child_last_name_' + nextID + '" value="" />');
	lines.push('</li><li>');
	lines.push('<div class="child_label">Birth Date:</div>');
	lines.push('<input type="text" name="birthdate_' + nextID + '" value="" />');
	lines.push('</li></ul>');
	lines.push('<input type="hidden" name="add_child" value="' + nextID + '" />');
	lines.push('<input type="button" name="delete_' + nextID + '" value="Delete" onclick="markBlockForDeletion(\'' + nextID + '\', \'Child\', \'child\');" />');
	/*lines.push('</li></ul>');*/
	lines.push('</li>');
	return(lines.join("\n"));
}

function addChildEntryBlock() {
	addEntryBlock(renderChildBlock, 'child_first');
}

function renderEmailBlock(rowclass, nextID) {
	lines = [];
	lines.push('<li' + rowclass + '>');
	lines.push('<input type="radio" name="default_email" value="' + nextID + '"/>');
	lines.push('<ul><li>');
	lines.push('<span style="font-weight:bold">Email Label</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" name="email_name_' + nextID + '" value="" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">Email Address</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" name="email_address_' + nextID + '" value="" />');
	lines.push('</li></ul>');
	lines.push('<input type="hidden" name="add_email" value="' + nextID + '" />');
	lines.push('<input type="button" name="delete_' + nextID + '" value="Delete" onclick="markBlockForDeletion(\'' + nextID + '\', \'Email\', \'email\');" />');
	lines.push('</li>');
	return(lines.join("\n"));
}

function addEmailEntryBlock() {
	addEntryBlock(renderEmailBlock, 'email');
}

function renderAddressBlock(rowclass, nextID) {
	lines = [];
	lines.push('<li' + rowclass + '>');
	lines.push('<input type="radio" name="default_address" value="' + nextID + '"/>');
	lines.push('<ul><li>');
	lines.push('<span style="font-weight:bold">Address Label</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" class="name" name="address_name_' + nextID + '" value="" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">Address Line 1</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" class="address_line" name="address_line_1_' + nextID + '" value="" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">Address Line 2</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" class="address_line" name="address_line_2_' + nextID + '" value="1200 E California Blvd" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">City, State, Zipcode</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" class="city" size="19" name="city_' + nextID + '" value="Pasadena" />');
	lines.push('<input type="text" class="state" size="19" name="state_' + nextID + '" value="CA" />');
	lines.push('<input type="text" class="zipcode" size="11" name="zipcode_' + nextID + '" value="91125" />');
	lines.push('</li><li>');
	lines.push('<span style="font-weight:bold">Country</span>');
	lines.push('</li><li>');
	lines.push('<input type="text" class="country" name="country_' + nextID + '" value="" />');
	lines.push('</li></ul>');
	lines.push('<input type="hidden" name="add_address" value="' + nextID + '" />');
	lines.push('<input type="button" name="delete_' + nextID + '" value="Delete" onclick="markBlockForDeletion(\'' + nextID + '\', \'Address\', \'address\');" />');
	lines.push('</li>');
	return(lines.join("\n"));
}

function addAddressEntryBlock(){
	addEntryBlock(renderAddressBlock, 'address');
}

function markBlockForDeletion(obj_id, label, type) {
	// All we do here is look for the parent <li> of block
	// that contains either an update or delete operation for
	// email email_id 
	var operation = $(".pane input[name='update_" + type + "'][value='" + obj_id + "']");
	if (operation.length === 0) {
		var operation = $(".pane input[name='add_" + type + "'][value='" + obj_id + "']");
	}
	containingLi = operation.parent();
	if (type == 'child') {
		obj_label = $(".pane input[name*='" + type + "_first_name_" + obj_id + "']").val() +
		' ' + $(".pane input[name*='" + type + "_middle_name_" + obj_id + "']").val();
	}
	else {
		obj_label = $(".pane input[name*='" + type + "_name_" + obj_id + "']").val();
	}
	if (obj_label.length == 0) {
		obj_label = "No label";
	}
	operation.replaceWith('<input type="hidden" name="delete_' + type + '" value="' + obj_id + '" />');
	containingLi.find("input").addClass("delete-mark");
	containingLi.after('<li class="deletion id_' + obj_id +  '"><p>' + label + ' "' + obj_label + '" has been marked for deletion.</p><input class="undo_delete" type="button" name="undo_delete_' + obj_id + '" value="Undo Delete" onclick="unmarkBlockForDeletion(\'' + obj_id + '\', \'' + type + '\');" /></li>');
	containingLi.hide();
}

function unmarkBlockForDeletion(obj_id, type) {
	// All we do here is look for the parent <li> of block
	// that contains either an update or delete operation for
	// email email_id 
	var operation = $(".pane input[name='delete_" + type + "'][value='" + obj_id + "']");
	var containingLi = operation.parent();
	if (obj_id.substr(0, 3) != 'new') {
		operation.replaceWith('<input type="hidden" name="update_' + type + '" value="' + obj_id + '" />');
	} else {
		operation.replaceWith('<input type="hidden" name="add_' + type + '" value="' + obj_id + '" />');
	}
	$('.deletion.id_' + obj_id).remove();
	containingLi.find("input").removeClass("delete-mark");
	containingLi.show();
}

function search_people() {
	options = "toolbar=0,status=0,menubar=0,resizable=1,scrollbars=1,top=0,width=800,height=690";
	h_win = window.open('','_sat', options);
	document.PopupForm.action = '/events/contact/person_search';
	document.PopupForm.target = '_sat';
	
	// substitute if needed so that a blank search returns a list of all people
	if (document.contact.search_name.value != "") {
		document.PopupForm.search_string.value = document.contact.search_name.value;
	}
	else {
		document.PopupForm.search_string.value = "%";
	}
	
	document.PopupForm.submit();
}

function selectEventsSpouse(id, first_name, middle_name, last_name) {
	document.contact.search_spouse_first_name.value = first_name;
	document.contact.search_spouse_middle_name.value = "";
	document.contact.search_spouse_last_name.value = last_name;
	document.contact.spouse_events_person_id.value = id;
	
	if (middle_name != null && middle_name.length > 0) {
		document.contact.search_spouse_middle_name.value = middle_name;
		middle_name = middle_name + " ";
	}
	document.contact.search_name.value = first_name + " " + middle_name + last_name;
	
	document.contact.target="_self";
	h_win.close();	
}

adsEvent.methods.show_tab = function (e, ui) {
	var href = ui.tab.hash.substr(1);
	var return_pane = "person";
	if (href === "name_title_pane") {
		return_pane = "person";
	} else if (href === "phone_pane") {
		return_pane = "person_phones";
	} else if (href === "email_pane") {
		return_pane = "person_emails";
	} else if (href === "address_pane") {
		return_pane = "person_addresses";
	} else if (href === "family_pane") {
		return_pane = "person_family";
	} else if (href === "diet_pane") {
		return_pane = "person_diet";
	} else if (href === "label_pane") {
		return_pane = "person_labels";
	} else if (href === "list_pane") {
		return_pane = "person_people_lists";
	} else if (href === "notes_pane") {
		return_pane = "person_notes";
	} else if (href === "history_pane") {
		return_pane = "person_history";
	}
	$("input[name='return_pane']").val(return_pane);
};

adsEvent.methods.build_dialog = function () {
	$("#msg_dialog").dialog({
		autoOpen: false,
		modal: true
	});
};

adsEvent.methods.build_tabs = function () {
	$('div.jquery_tabs').tabs({
		show: adsEvent.methods.show_tab
	});
	if (start_tab === "Phones") {
		$("a[href='#phone_pane']").click();
	} else if (start_tab === "Email") {
		$("a[href='#email_pane']").click();
	} else if (start_tab === "Addresses") {
		$("a[href='#address_pane']").click();
	} else if (start_tab === "Diet") {
		$("a[href='#diet_pane']").click();
	} else if (start_tab === "Family") {
		$("a[href='#family_pane']").click();
	} else if (start_tab === "Labels") {
		$("a[href='#label_pane']").click();
	} else if (start_tab === "People Lists") {
		$("a[href='#list_pane']").click();
	} else if (start_tab === "Notes") {
		$("a[href='#notes_pane']").click();
	} else if (start_tab === "History") {
		$("a[href='#history_pane']").click();
	}
};

adsEvent.methods.error_dialog = function (msg) {
	$("#msg_dialog").html("An error has occurred:<br />"+msg).dialog("option", {
		title: '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>ERROR',
		buttons: {
			'OK': function() {
				$(this).dialog('close');
			}
		},
		dialogClass: "ui-state-error"
	}).dialog("open");
};

adsEvent.methods.delete_dialog = function () {
	$("#msg_dialog").html("Remove "+ contact_name +"?").dialog("option", {
		title: '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>ALERT',
		buttons: {
			'YES': function() {
				$(this).dialog('close');
				adsEvent.methods.deceased_dialog();
			},
			'NO': function() {
				$(this).dialog('close');
			}
		},
		dialogClass: "ui-state-highlight"
	}).dialog("open");
};

adsEvent.methods.deceased_dialog = function () {
	$("#msg_dialog").html("Has "+ contact_name +" passed away?").dialog("option", {
		title: '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>ALERT',
		buttons: {
			'YES': function() {
				$(this).dialog('close');
				adsEvent.methods.copy_dialog();
			},
			'NO': function() {
				$(this).dialog('close');
				adsEvent.methods.print_dialog(adsEvent.methods.delete_contact);
			}
		},
		dialogClass: "ui-state-error"
	}).dialog("open");
};

adsEvent.methods.copy_dialog = function () {
	$("#msg_dialog").html("Copy "+ contact_name +"'s data to spouse record?").dialog("option", {
		title: '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>ALERT',
		buttons: {
			'YES': function() {
				$(this).dialog('close');
				adsEvent.methods.print_dialog(adsEvent.methods.transfer_contact);
			},
			'NO': function() {
				$(this).dialog('close');
				adsEvent.methods.print_dialog(adsEvent.methods.delete_contact);
			}
		},
		dialogClass: "ui-state-error"
	}).dialog("open");
};

adsEvent.methods.print_dialog = function (next_action) {
	$("#msg_dialog").html("Print an archival record for " + contact_name + "?").dialog("option", {
		title: '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>ARCHIVE?',
		buttons: {
			'YES': function() {
				$(this).dialog('close');
				window.open("/events/contact/print_person?id=" + person_id);
				if (next_action) {
					next_action();
				}
			},
			'NO': function() {
				$(this).dialog('close');
				if (next_action) {
					next_action();
				}
			}
		}
	}).dialog("open");
};

adsEvent.methods.transfer_contact = function () {
	window.location = "/events/contact/transfer_person?" + $("form[name=contact]").serialize();
};

adsEvent.methods.delete_contact = function () {
	window.location = "/events/contact/delete_person?id=" + person_id;
};

adsEvent.methods.validate = function () {
	var name_regex = /^[a-zA-Z][a-zA-Z\-\s\']*$/;
	$("form[name=contact]").validity(function () {
		//$("input[name='salutation_source']").assert(adsEvent.methods.check_salutation);
		$("input[name='name_source']").assert(adsEvent.methods.check_name);
		$("input[name='rank_source']").assert(adsEvent.methods.check_rank);
		$("input[name='title_source']").assert(adsEvent.methods.check_title);
		$("input[name='division_source']").assert(adsEvent.methods.check_division);
		$("input[name='default_phone']").assert(adsEvent.methods.check_phone);
		$("input[name='default_email']").assert(adsEvent.methods.check_email);
		$("input[name='default_address']").assert(adsEvent.methods.check_address);
		$("input[name='spouse_source']").assert(adsEvent.methods.check_spouse);
		// Check Children
		$("input[name^='child_first_name_']:not([class*=delete-mark])").require().match(name_regex, "alpha");
		$("input[name^='child_middle_name_']:not([class*=delete-mark])").match(name_regex, "alpha");
		$("input[name^='child_last_name_']:not([class*=delete-mark])").require().match(name_regex, "alpha");
		$("input[name^='birthdate_']:not([class*=delete-mark])").match(/^[\w\s.,\-\'\(\)\/]+$/, "alphanum");
		$("input[name='default_diet']").assert(adsEvent.methods.check_diet);
		$("input[name='name_tag_source']").assert(adsEvent.methods.check_name_tag);
		$("input[name='place_card_source']").assert(adsEvent.methods.check_place_card);
		$("input[name='invitation_name_source']").assert(adsEvent.methods.check_invitation_name);
		$("input[name='next_update']").match(/^\d{4}\/\d{2}\/\d{2}$/, "date");	  // accept yyyy/mm/dd and mm/dd/yyyy
	});
};

$.validity.outputs.updatePerson = {
	// The start function will be called when validation starts.
	// This allows you to prepare the page for validation, for instance
	// you might remove any validation messages that are already on the page.
	start: function () { 
		$("input").removeClass("ui-state-error").each(function () {
			if ($(this).val() === "-- REQUIRED --") {
				$(this).val("");
			}
		});
		$("select,li").removeClass("ui-state-error");
		$("label.validity-error").remove();
	},
	
	// The raise function is called to raise an error for a specific input.
	// The first argument is a jQuery object of the input to raise the error message for.
	// The second argument is the string of the error message.
	raise: function ($obj, msg) {
		var fix_msg, tag;
		if (msg === " is required.") {
			fix_msg = " REQUIRED FIELD";
		} else if (msg === " cannot contain HTML characters.") {
			fix_msg = " (NO HTML)";
		} else if (msg === "Alpha") {
			fix_msg = " (ALPHA ONLY)";
		} else if (msg === "Alphanum") {
			fix_msg = " (ALPHANUMERIC)";
		} else if (msg === "Date") {
			fix_msg = " (INVALID)";
		} else if (msg === "Phone") {
			fix_msg = " (INVALID)";
		} else if (msg === "Email") {
			fix_msg = " (INVALID)";
		} else if (msg === "Zipcode") {
			fix_msg = " (INVALID)";
		} else if (msg === "Invalid.") {
			fix_msg = " (INVALID)";
		} else {
			fix_msg = msg;
		}
		$obj.addClass("ui-state-error");
		if ($obj.attr("type") === "text") {
			value = $obj.val();
		} else if ($obj.attr("name") === "diet_notes") {
			if (fix_msg.indexOf("is required") != -1) {
				fix_msg = " REQUIRED FOR OTHER/ALLERGIES";
			} else if (fix_msg.indexOf("cannot contain HTML") != -1) {
				fix_msg = " NO HTML CHARACTERS";
			}
		} else if ($obj.attr("name") === "notes") {
			if (fix_msg.indexOf("cannot contain HTML") != -1) {
				fix_msg = " NO HTML CHARACTERS";
			}
		}
		tag = $obj.next("label");   // Find error label, if it exists
		if (!tag.length) {
			$obj.after('<label>' + fix_msg + '</label>');   //  Create non-existent error label
			tag = $obj.next("label");
		} else {
			tag.html(fix_msg);
		}
		tag.addClass("ui-state-error validity-error").css({
			marginLeft: "5px",
			padding: "1px 5px",
			fontSize: "75%"
		});
		var id = $obj.parents("[id$=_pane]").attr("id");
		$("a[href=#"+id+"]").parent().addClass("ui-state-error");
	},
};

adsEvent.methods.check_salutation = function () {
	var valid = false;
	var val = $("input[name='salutation_source']:checked").val();
	if (val === "manual") {
		$("select[name=salutation]").assert(function (ele) {
			valid = (ele.value !== "None");
			return ele.value !== "None";
		});
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_name = function () {
	var valid = false;
	var val = $("input[name='name_source']:checked").val();
	if (val === "manual") {
		$("input[name='first_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		$("input[name='middle_name']").match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		$("input[name='last_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_rank = function () {
	var valid = false;
	var val = $("input[name='rank_source']:checked").val();
	if (val === "manual") {
		$("input[name='rank']").require().match(/^[a-zA-Z\s.\-\'\(\)]+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_title = function () {
	var valid = false;
	var val = $("input[name='title_source']:checked").val();
	if (val === "manual") {
		$("input[name='title']").require().match(/^[a-zA-Z\s.\-\'\(\)]+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_division = function () {
	var valid = false;
	var val = $("input[name='division_source']:checked").val();
	if (val === "manual") {
		$("select[name=division]").assert(function (ele) {
			valid = (ele.value !== "None");
			return ele.value !== "None";
		});
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_phone = function () {
	var valid = false;
	var val = $("input[name='default_phone']:checked").val();
	if (val) {
		valid = true;
	}
	$("input[name^=phone_name_]:not([class*=delete-mark])").require().match(/^[\w\s.\-\'\(\)]+$/, "alphanum");
	$("input[name^=phone_number_]:not([class*=delete-mark])").require().match(/^[\+\s\-0-9\(\)]*(\sext)?(\sx)?[\s0-9]+$/, "phone");   // validity's phone format is US-only, doesn't like parens, and other warts
	return valid;
};

adsEvent.methods.check_email = function () {
	var valid = false;
	var val = $("input[name='default_email']:checked").val();
	if (val) {
		valid = true;
	}
	$("input[name^=email_name_]:not([class*=delete-mark])").require().match(/^[\w\s.\-\'\(\)]+$/, "alphanum");
	$("input[name^=email_address_]:not([class*=delete-mark])").require().match("email", "email");
	return valid;
};

adsEvent.methods.check_address = function () {
	var valid = false;
	var val = $("input[name='default_address']:checked").val();
	if (val) {
		valid = true;
	}
	$("input[name^=address_name_]:not([class*=delete-mark])").require().match(/^[\w\s.\-\'\(\)]+$/, "alphanum");
	$("input[name^=address_line_1_]:not([class*=delete-mark])").require().nonHtml();
	$("input[name^=address_line_2_]:not([class*=delete-mark])").nonHtml();
	$("input[name^=city_]:not([class*=delete-mark])").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
	$("input[name^=state_]:not([class*=delete-mark])").match(/^[a-zA-Z\s.\-\']+$/, "alpha");
	$("input[name^=zipcode_]:not([class*=delete-mark])").require().match("zip", "zipcode");
	$("input[name^=country_]:not([class*=delete-mark])").match(/^[a-zA-Z\s.\-\']+$/, "alpha");
	return valid;
};

adsEvent.methods.check_spouse = function () {
	var valid = false;
	var val = $("input[name='spouse_source']:checked").val();
	if (val === "manual") {
		$("input[name='manual_spouse_first_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		$("input[name='manual_spouse_last_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		valid = true;
	} else if (val === "search") {
		$("input[name='search_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		$("input[name='search_spouse_first_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		$("input[name='search_spouse_last_name']").require().match(/^[a-zA-Z\s.\-\']+$/, "alpha");
		valid = true;
	} else if (val === "nospouse") {
		valid = true;
	} else if (val === "sync" ) {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_diet = function () {
	var valid = false;
	var val = $("input[name='default_diet']:checked").val();
	if (val) {
		valid = true;
	}
	if (val === "5") {
		$("textarea[name='diet_notes']").require();
	}
	return valid;
};

adsEvent.methods.check_name_tag = function () {
	var valid = false;
	var val = $("input[name='name_tag_source']:checked").val();
	if (val === "manual") {
		$("input[name='name_tag']").require().match(/^[a-zA-Z\s.,\-\']+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_place_card = function () {
	var valid = false;
	var val = $("input[name='place_card_source']:checked").val();
	if (val === "manual") {
		$("input[name='place_card']").require().match(/^[a-zA-Z\s.,\-\']+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

adsEvent.methods.check_invitation_name = function () {
	var valid = false;
	var val = $("input[name='invitation_name_source']:checked").val();
	if (val === "manual") {
		$("input[name='invitation_name']").require().match(/^[a-zA-Z\s.,\-\']+$/, "alpha");
		valid = true;
	} else if (val === "sync") {
		valid = true;
	}
	return valid;
};

$(document).ready(function () {
	$.validity.setup({ outputMode:'updatePerson' });
	adsEvent.methods.build_tabs();
	$("#btn_save_person,#btn_delete_person,#btn_print_person").button();
	adsEvent.methods.build_dialog();
	if (delete_flag) {
		adsEvent.methods.error_dialog(contact_name + " has been deleted. Please be aware that this is an archival record.");
		$("#btn_delete_person").remove();
		$("#btn_save_person").remove();
	} else {
		$("#btn_delete_person").click(adsEvent.methods.delete_dialog);
	}
	$("#btn_print_person").click(adsEvent.methods.print_dialog);
	$("#btn_save_person").click(adsEvent.methods.validate);
	$("input[type=text]").focus(function () { 
		this.select();
	}).change(function  () {
		if ($(this).val().length) {
			$(this).removeClass("ui-state-error");
		}
	}).keyup(function  () {
		if ($(this).val().length) {
			$(this).removeClass("ui-state-error");
		}
	});
	$("select").change(function () {
		$(this).removeClass("ui-state-error");
	});
});
