function onConfirm() {
    if (validatePending()) {
        alert('You have not accepted/declined all of the awards in pending status.  Please return to the prior screen and complete your choices, or come back to this page at a later time to complete the entire process.');
        return false;
    }
    if (validateDeclinePell()) {
        if (!confirm('You have chosen to decline your Federal Pell Grant. If this is your intent, click OK to proceed. If not, click Cancel to return to the Awards screen.')) {
            return false;
        }
    }
    if (validateDeclineNonPell()) {
        if (!confirm('You have chosen to decline at least one of the awards offered you. If this is your intent, click OK. If not, click Cancel to return to the Accept Awards page.')) {
            return false;
        }
    }
    if (validateDecrease()) {
        if (!confirm('You have chosen to reduce the amount of at least one of the awards offered you. If this is your intent, click OK.  If not, click Cancel to return to the Accept Awards page.')) {
            return false;
        }
    }
    return true;
}
function onDeclineAll() {
    if (confirm('You are about to Decline all the financial aid displayed. If this is your intent, click OK. If not, click Cancel to return to the Accept Awards page.')) {
        declineAll();
    }
    return false;
}
function onAcceptAll() {
    if (confirm('You are about to Accept all your awards that are in pending status. If this is your intent, click OK. If not, click Cancel to return to the Accept Awards page.')) {
        acceptAll();
    }
    return false;
}
function formatCurrency(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + '$' + num + '.' + cents);
}
function isInt(str) {
    var i = parseInt(str);

    if (isNaN(i))
        return false;

    i = i.toString();
    if (i != str)
        return false;

    return true;
}
function StatusOnblur(id1, id2, amt) {
    if (document.getElementById(id1).value == 'D') {
        if (!(document.getElementById(id2).value == amt)) {
            alert('Award amount can not be changed for declined award');
            document.getElementById(id2).value = amt;
            document.getElementById(lblTotalClientId()).value = TotalAward();
        }
        document.getElementById(id2).disabled = true;
    }
    else {
        document.getElementById(id2).disabled = false;
    }
}

