provider "aws" {
  region = "us-west-2"
  profile = "sso-acs-prod"
}

# access.caltech-testserver statefile (for this Terraform stack)
terraform {
  backend "s3" {
    bucket = "caltech-terraform-remotestate-file"
    key    = "access.caltech-resources/app-terraform-state"
    region = "us-west-2"
    profile = "sso-acs-prod"
  }
}

# ==================
# Remote state files
# ==================

# VPC infrastructure statefile
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "caltech-terraform-remotestate-file"
    key    = "remote-terraform-state"
    region = "us-west-2"
    profile = "sso-acs-prod"
  }
}

# ========
# Locals
# ========

locals {
  subnet-map = {
    test = data.terraform_remote_state.vpc.outputs.subnet-map-proxy-servers["test"],
    qa = data.terraform_remote_state.vpc.outputs.subnet-map-proxy-servers["qa"],
    prod = data.terraform_remote_state.vpc.outputs.subnet-map-proxy-servers["prod"]
  }
  security-group-map = {
    test = data.terraform_remote_state.vpc.outputs.sg-map-proxy-servers["qa"],
    qa = data.terraform_remote_state.vpc.outputs.sg-map-proxy-servers["qa"],
    prod = data.terraform_remote_state.vpc.outputs.sg-map-proxy-servers["prod"]
  }
}

# ==========
# Resources
# ==========

# Allow only our cluster machines to access our EFS file system
resource "aws_security_group" "efs-from-proxy-servers" {
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc-id
  name        = "efs-from-${terraform.workspace}-proxy-servers"
  description = "EFS from ${terraform.workspace} proxy servers"

  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    security_groups = [local.security-group-map[terraform.workspace]]
  }


  tags = {
      Project = "access.caltech"
      Group = "ADS"
      Environment = terraform.workspace
      Client = "IMSS"
      Contact = "imss-ads-staff@caltech.edu"
  }
}

module "efs-volume" {
  source = "s3::https://s3-us-west-2.amazonaws.com/imss-code-drop/terraform-caltech-commons/terraform-caltech-commons-0.27.5.zip//modules/efs-volume"
  volume_name = "access-caltech-resources-${terraform.workspace}"
  subnet_ids = local.subnet-map[terraform.workspace]
  mount_point = "/mnt/access.caltech-resources"
  security_group_ids = [aws_security_group.efs-from-proxy-servers.id]
  client = "IMSS"
  project = "access.caltech"
  environment = terraform.workspace
  contact = "imss-ads-staff@caltech.edu"
  group = "ADS"
}


module "pipeline" {
  source = "./pipeline"
  artifact-bucket-arn = "arn:aws:s3:::codepipeline-artifact-store"
  pipeline-name = "access-caltech-resources-${terraform.workspace}"
  codestar-connection-name = "bitbucket-imss-ads"
  slack-channel = "#ads_deploys"
  repository = "caltech-imss-ads/access.caltech-resources"
  branch = terraform.workspace
  efs-volume = "${module.efs-volume.efs-file-system-dns-name}:/"
  mount-point = "/mnt/access.caltech-resources"
  vpc-id = data.terraform_remote_state.vpc.outputs.vpc-id
  subnet-ids = local.subnet-map[terraform.workspace]
  security-group-ids = [local.security-group-map[terraform.workspace]]
  client = "IMSS"
  project = "access.caltech"
  environment = terraform.workspace
  contact = "imss-ads-staff@caltech.edu"
  group = "ADS"
}


# =======
# Outputs
# =======

output "efs-file-system-id" {
  value = module.efs-volume.efs-file-system-id
}

output "efs-file-system-arn" {
  value = module.efs-volume.efs-file-system-arn
}

output "efs-file-system-dns-name" {
  value = module.efs-volume.efs-file-system-dns-name
}

output "efs-file-system-mount-point" {
  value = module.efs-volume.efs-file-system-mount-point
}
