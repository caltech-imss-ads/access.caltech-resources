/*
Module: codepipeline/recipes/bitbucket-docker-build-deploy

Create a CodePipeline that will:

  * Be triggered by changes on a particular branch of a Python Bitbucket git repo
  * Mount an EFS volume
  * rsync the data from the git repo into the EFS volume


Basic usage example::

    module "pipeline" {
      source = "./pipeline"
      pipepline-name = "my-codepipeline"
      artifact-bucket-arn = "arn:aws:s3:::my-artifact-bucket"
      repository = "caltech-imss-ads/foobar"
      branch = "build"
      efs-volume = "volume-id"
      mount-point = "/mnt/foo"
      mount
      vpc-id = "my-vpc-id"
      subnet-ids = [
        "subnet-id-1",
        "subnet-id-2"
      ]
      security-group-ids = [
        "security-group-id-1",
        "security-group-id-2",
      ]
      terraform-statefile-bucket-arn = "my-bucket-arn"
      service-name = "foobar-test"
      client = "IMSS"
      project = "my-project"
      environment = "prod"
      contact = "email@caltech.edu"
      group = "MyIMSSGroup"
    }
*/

variable "pipeline-name" {
  type        = string
  default     = ""
  description = "[required] The name of this pipeline."
}

variable "artifact-bucket-arn" {
  type        = string
  default     = ""
  description = "The ARN of the S3 bucket CodePipeline can use to store its intermediate artifacts"
}

variable "codestar-connection-name" {
  type        = string
  default     = "bitbucket-imss-ads"
  description = "The name of the codestar-connection entry from module.reference.codestar-connection-arns[] for this account.  This controls how we connect with Bitbucket."
}

variable "repository" {
  type        = string
  default     = ""
  description = "What Bitbucket git repository should we connect to? Example: caltech-imss-ads/foobar"
}

variable "branch" {
  type        = string
  default     = "build"
  description = "What branch should we monitor in {bitbucket-repo} to trigger our builds?  Default: build"
}

variable "efs-volume" {
  type        = string
  default     = ""
  description = "The identifier of the efs volume"
}

variable "mount-point" {
  type        = string
  default     = "/mnt/efs"
  description = "What should the mount point for the efs volume be in the CodeBuild environment?  Default: build"
}

variable "subfolder" {
  type        = string
  default     = ""
  description = "What subfolder of the EFS mount will we be working in?  Default: top of the EFS mount."
}

variable "slack-api-token" {
  type        = string
  default     = ""
  description = "The Slack OAuth token to use for ci-buildbot"
}

variable "slack-channel" {
  type        = string
  default     = "#jenkins"
  description = "Which Slack channel should we report to?  Default: ads_deploys."
}

variable "vpc-id" {
  type        = string
  default     = null
  description = "The id of the VPC in which to run this build step"
}

variable "subnet-ids" {
  type        = list(string)
  default     = []
  description = "A list of subnets that this build step should run in"
}

variable "security-group-ids" {
  type        = list(string)
  default     = []
  description = "A list of security group ids to asign to this build step"
}

variable "client" {
  type        = string
  default     = ""
  description = "The value for the Client tag"
}

variable "project" {
  type        = string
  default     = ""
  description = "The value for the Project tag"
}

variable "environment" {
  type        = string
  default     = "prod"
  description = "The value for the Environment tag"
}

variable "group" {
  type        = string
  default     = ""
  description = "The value for the Group tag"
}

variable "contact" {
  type        = string
  default     = ""
  description = "The value for the Contact tag.  This should be an email address."
}


# ======================
# Resources
# ======================

resource "aws_cloudwatch_log_group" "logs" {
  name = "${var.pipeline-name}-codepipeline"
  retention_in_days = 14

  tags = {
    Client = var.client
    Project = var.project
    Environment = var.environment
    Group = var.group
    Contact = var.contact
  }
}

module "bitbucket-action" {
  source = "s3::https://s3-us-west-2.amazonaws.com/imss-code-drop/terraform-caltech-commons/terraform-caltech-commons-0.27.5.zip//modules/codepipeline/actions/bitbucket-source"
  pipeline-name                  = var.pipeline-name
  codestar-connection-name       = var.codestar-connection-name
  repository                     = var.repository
  branch                         = var.branch
  output-artifact-format         = "CODEBUILD_CLONE_REF"
  client                         = var.client
  project                        = var.project
  environment                    = var.environment
  group                          = var.client
  contact                        = var.contact
}

module "unpack-to-efs-action" {
  source = "./unpack-to-efs"
  artifact-store-bucket-arn      = var.artifact-bucket-arn
  pipeline-name                  = var.pipeline-name
  pipeline-log-group-name        = aws_cloudwatch_log_group.logs.name
  input-artifact-name            = module.bitbucket-action.output-artifact
  slack-api-token                = var.slack-api-token
  slack-channel                  = var.slack-channel
  efs-volume                     = var.efs-volume
  mount-point                    = var.mount-point
  subfolder                      = var.subfolder
  vpc-id                         = var.vpc-id
  subnet-ids                     = var.subnet-ids
  security-group-ids             = var.security-group-ids
  client                         = var.client
  project                        = var.project
  environment                    = var.environment
  group                          = var.client
  contact                        = var.contact
}


module "pipeline" {
  source = "s3::https://s3-us-west-2.amazonaws.com/imss-code-drop/terraform-caltech-commons/terraform-caltech-commons-0.27.5.zip//modules/codepipeline/codepipeline-base"
  pipeline-name = var.pipeline-name
  artifact-bucket-arn = var.artifact-bucket-arn
  extra-iam-policy-arns = flatten([
    module.bitbucket-action.extra-codepipeline-iam-policy-arns,
  ])
  stages = [
    {
      name = "Source"
      actions = [module.bitbucket-action.action]
    },
    {
      name = "Deploy"
      actions = [module.unpack-to-efs-action.action]
    },
  ]
  client = var.client
  project = var.project
  environment = var.environment
  group = var.client
  contact = var.contact
}
