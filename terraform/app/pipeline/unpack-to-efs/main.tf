/*
Module: common/pipeline/actions/unpack-to-efs

Create a Codebuild step that mounts an EFS volume and updates a checked out git repository in a particular subfolder.

Basic usage example::

    module "codebuild-efs-action" {
      source = "./unpack-to-efs'
      pipeline-name = "my-codepipeline"
      pipeline-log-group-name = "my-log-group"
      artifact-store-bucket-arn = "my-codepipeline-artifact-store-bucket-arn"
      efs-volume = "efs-identifier"
      client = "client"
      project = "project"
      environment = "environment"
      group = "group"
      contact = "contact"
    }

Options::

* Set ``extra-role-policy-arns`` to a list of IAM policy arns to attach to our CodeBuild role in addition to what we'll
  create to support the basic CodeBuild operations for source archiving.
* Set ``slack-api-token`` to set the OAth token for ci-buildbot.  If not set, we'll use the one for the Caltech workspace
* Set ``slack-channel`` to set the Slack channel to which ci-buildbot will report.  If this is a private channel, you'll
  need to invite the app to your channel before it will report.  In IMSS, the bot is named "ADS Buildbot".
* Set ``input-artifact-name`` to change the input artifact name for the CodeBuild step.  Do this if you have a
  complicated CodePipline with many parallel steps and you need to choose a specific input artifact for this step.
* Set ``output-artifact-name`` to change the output artifact name for the CodeBuild step.  Do this if you have a
  complicated CodePipline with many parallel steps and you need to differentiate this step's output from that of others.
*/

variable "pipeline-name" {
  type        = string
  default     = ""
  description = "[required] The name of the parent codepipeline."
}

variable "artifact-store-bucket-arn" {
  type        = string
  default     = ""
  description = "The ARN of the Artifact Store S3 bucket"
}

variable "efs-volume" {
  type        = string
  default     = ""
  description = "The identifier of the efs volume"
}

variable "efs-volume-name" {
  type        = string
  default     = "efs_volume"
  description = "The name of the efs volume, used to set an environment variable"
}

variable "mount-point" {
  type        = string
  default     = "/mnt/efs"
  description = "What should the mount point for the efs volume be in the CodeBuild environment?  Default: build"
}

variable "subfolder" {
  type        = string
  default     = ""
  description = "What subfolder of the EFS mount will we be working in?  Default: top of the EFS mount."
}

variable "extra-role-policy-arns" {
  type       = list(string)
  default    = []
}

variable "pipeline-log-group-name" {
  type        = string
  default     = ""
  description = "Which Slack channel should we report to?  Default: ads_deploys."
}

variable "slack-api-token" {
  type        = string
  default     = ""
  description = "The Slack OAuth token to use for ci-buildbot"
}

variable "slack-channel" {
  type        = string
  default     = "#jenkins"
  description = "Which Slack channel should we report to?  Default: jenkins."
}

variable "vpc-id" {
  type        = string
  default     = null
  description = "The id of the VPC in which to run this build step"
}

variable "subnet-ids" {
  type        = list(string)
  default     = []
  description = "A list of subnets that this build step should run in"
}

variable "security-group-ids" {
  type        = list(string)
  default     = []
  description = "A list of security group ids to asign to this build step"
}

variable "input-artifact-name" {
  type        = string
  default     = "source"
  description = "What is the name of our codepipeline input artifact? Default: source."
}

variable "output-artifact-name" {
  type        = string
  default     = "archive-source"
  description = "What should we name our output_artifact in the action definition?  Default: archive-source"
}

variable "client" {
  type        = string
  default     = ""
  description = "The value for the Client tag"
}

variable "project" {
  type        = string
  default     = ""
  description = "The value for the Project tag"
}

variable "environment" {
  type        = string
  default     = "prod"
  description = "The value for the Environment tag"
}

variable "group" {
  type        = string
  default     = ""
  description = "The value for the Group tag"
}

variable "contact" {
  type        = string
  default     = ""
  description = "The value for the Contact tag.  This should be an email address."
}

# ============================================
# Data
# ============================================

data "aws_arn" "artifact-store" {
  arn = var.artifact-store-bucket-arn
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_subnet" "vpc" {
  count = length(var.subnet-ids)
  id = var.subnet-ids[count.index]
}

# ====================
# Reference module
# ====================

module "reference" {
  source = "s3::https://s3-us-west-2.amazonaws.com/imss-code-drop/terraform-caltech-commons/terraform-caltech-commons-0.27.5.zip//modules/reference"
}

# ============================================
# Locals
# ============================================

locals {
  build-image = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
  artifact-store-bucket-name = length(var.artifact-store-bucket-arn) > 0 ? data.aws_arn.artifact-store.resource : module.reference.artifact-stores[data.aws_caller_identity.current.account_id]["s3-bucket-arn"]
  slack-api-token = length(var.slack-api-token) > 0 ? var.slack-api-token : module.reference.slack-api-tokens["ci-buildbot"]
  subnet-arns = data.aws_subnet.vpc.*.arn
}

# ============================================
# Resources
# ============================================

# ----------------
# CLoudwatch Logs
# ----------------

resource "aws_cloudwatch_log_stream" "stream" {
  name = "${var.pipeline-name}-codebuild-unpack-to-efs"
  log_group_name = var.pipeline-log-group-name
}

# -------------
# IAM
# -------------

# Policy

# We just need the JSON for the artifact-store s3 bucket policies here
module "artifact-store-policies" {
  source = "s3::https://s3-us-west-2.amazonaws.com/imss-code-drop/terraform-caltech-commons/terraform-caltech-commons-0.27.5.zip//modules/s3-iam-policies"
  bucket_name = local.artifact-store-bucket-name
  policy_types = []
}

data "aws_iam_policy_document" "codebuild" {
  source_policy_documents = [module.artifact-store-policies.iam-s3-bucket-rw-objects-policy-json]

  statement {
    sid = "CloudWatchLogsPolicy"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }

  # We need this one to be able to deal with CODEBUILD_CLONE_REF archive types
  # It lets CodeBuild do git clone for us from bitbucket
  statement {
    sid = "CodePipelineCodeStarConnections"
    effect = "Allow"
    actions = [
      "codestar-connections:UseConnection"
    ]
    resources = ["*"]
  }

  # The next two statements allow Codebuild to set itself up in a VPC
  statement {
    sid = "CodeBuildVPC"
    effect = "Allow"
    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeDhcpOptions",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeVpcs"
    ]
    resources = ["*"]
  }

  statement {
    sid = "CodebuildVPCCreateNetworkInterface"
    effect = "Allow"
    actions = [
      "ec2:CreateNetworkInterfacePermission"
    ]
    resources = [
      "arn:aws:ec2:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:network-interface/*"
    ]
    condition {
      test = "StringEquals"
      variable = "ec2:Subnet"
      values = local.subnet-arns
    }
    condition {
      test = "StringEquals"
      variable = "ec2:AuthorizedService"
      values = [
        "codebuild.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_policy" "codebuild" {
  name        = "${var.pipeline-name}-codebuild-unpack-to-efs"
  path        = "/${var.project}/${var.environment}/"
  description = "Permissions for the ${var.pipeline-name} CodePipeline unpack to EFS step"
  policy      = data.aws_iam_policy_document.codebuild.json
}

resource "aws_iam_role" "role" {
  name = "${var.pipeline-name}-codebuild-unpack-to-efs"
  path = "/${var.project}/${var.environment}/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  tags = {
    Client = var.client
    Project = var.project
    Environment = var.environment
    Group = var.group
    Contact = var.contact
  }
}

resource "aws_iam_role_policy_attachment" "codebuild" {
  role = aws_iam_role.role.name
  policy_arn = aws_iam_policy.codebuild.arn
}

resource "aws_iam_role_policy_attachment" "extra" {
  count = length(var.extra-role-policy-arns)
  role = aws_iam_role.role.name
  policy_arn = var.extra-role-policy-arns[count.index]
}



# ------------------------------
# The CodeBuild project itself
# ------------------------------

resource "aws_codebuild_project" "codebuild" {
  name = "${var.pipeline-name}-archive"
  description  = "Archive code to code-drop for pipeline for ${var.pipeline-name}"
  build_timeout = 60
  service_role = aws_iam_role.role.arn

  artifacts {
    type                = "CODEPIPELINE"
    artifact_identifier = var.output-artifact-name
  }

  environment {
    compute_type    = "BUILD_GENERAL1_MEDIUM"
    image           = local.build-image
    type            = "LINUX_CONTAINER"
    # privileged_mode must be true in order to mount EFS volumes
    privileged_mode = true

    environment_variable {
      name = "SLACK_API_TOKEN"
      value = local.slack-api-token
    }

    environment_variable {
      name = "CHANNEL"
      value = var.slack-channel
    }

    environment_variable {
      name = "SUBFOLDER"
      value = var.subfolder
    }
  }

  file_system_locations {
    identifier = var.efs-volume-name
    location = var.efs-volume
    mount_point = var.mount-point
    mount_options = "nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2"
    type = "EFS"
  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<__EOF__
version: 0.2

phases:
  install:
    commands:
      - pip install ci-buildbot
  pre_build:
    commands:
      - test ! -z "$${SUBFOLDER}" && DESTINATION=${var.mount-point}/$${SUBFOLDER} || DESTINATION=${var.mount-point}
  build:
    commands:
      - test -e "$${DESTINATION}" || mkdir "$${DESTINATION}"
      - rsync --delete -av . "$${DESTINATION}"
    finally:
      - test $${CODEBUILD_BUILD_SUCCEEDING} -eq 1 && buildbot report general success "Update access.caltech-resources EFS volume"|| buildbot report general failure "Update access.caltech-resources EFS volume"

artifacts:
  files:
    - "**/*"
  name: ${var.output-artifact-name}
__EOF__
  }

  vpc_config {
    vpc_id = var.vpc-id
    subnets = var.subnet-ids
    security_group_ids = var.security-group-ids
  }

  logs_config {
    cloudwatch_logs {
      group_name = var.pipeline-log-group-name
      stream_name = aws_cloudwatch_log_stream.stream.name
    }
  }

  tags = {
    Client = var.client
    Project = var.project
    Environment = var.environment
    Group = var.group
    Contact = var.contact
  }
}

# ====================================
# Outputs
# ====================================

output "action" {
  value = {
    name = "UnpackToEFS"
    category = "Build"
    owner = "AWS"
    version = "1"
    provider = "CodeBuild"
    configuration = {
      ProjectName = aws_codebuild_project.codebuild.name
    }
    input_artifacts = [var.input-artifact-name]
    output_artifacts = [var.output-artifact-name]
    region = data.aws_region.current.name
    namespace = "UnPackToEFSDeployVariables"
  }
}

output "output-artifact" {
  value = var.output-artifact-name
}

output "extra-codepipeline-iam-policy-arns" {
  value = []
}


