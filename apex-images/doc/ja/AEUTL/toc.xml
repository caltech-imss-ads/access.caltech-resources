<?xml version='1.0' encoding='iso-8859-1'?>
<toc version="1.0">
  <tocitem target="title.htm" text="Oracle Application Express SQL Workshop Guide, Release 4.0">
    <tocitem target="preface.htm" text="Preface">
      <tocitem target="preface.htm-CIAJEGBC" text="Topic Overview" />
      <tocitem target="preface.htm-BABFEFEA" text="Audience" />
      <tocitem target="preface.htm-CIADEJBJ" text="Documentation Accessibility" />
      <tocitem target="preface.htm-i972232" text="Related Documents" />
      <tocitem target="preface.htm-i971172" text="Conventions" />
    </tocitem>
    <tocitem target="what_new.htm" text="What's New">
      <tocitem target="what_new.htm-CEGIEDHE" text="Oracle Application Express SQL Workshop Guide Updates for Release 4.0" />
    </tocitem>
    <tocitem target="obj_brows.htm" text="Managing Database Objects with Object Browser">
      <tocitem target="obj_brows_about.htm-CHDCJCJJ" text="About Object Browser">
        <tocitem target="obj_brows_goto.htm-BACGHAIC" text="Accessing Object Browser" />
      </tocitem>
      <tocitem target="obj_brows_srch.htm-CHDFBJAD" text="Searching for and Browsing Database Objects">
        <tocitem target="obj_brows_filter.htm-CHDIJHEG" text="Searching For and Selecting Database Objects" />
        <tocitem target="obj_brows_sel_pane.htm-CHDIBCCC" text="Hiding the Object Selection Pane" />
      </tocitem>
      <tocitem target="obj_brows_create.htm-CHDIDBII" text="About Creating Database Objects" />
      <tocitem target="obj_table.htm-CHDHEABH" text="Managing Tables">
        <tocitem target="obj_table_create.htm-CHDFCIJJ" text="Creating a Table" />
        <tocitem target="obj_table_browse.htm-CHDCJBHH" text="Browsing a Table">
          <tocitem target="obj_table_browse.htm-CHDIAHGD" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_table_edit.htm-CHDDGJCJ" text="Editing a Table" />
        <tocitem target="obj_table_drop.htm-CIHBIBFF" text="Dropping a Table" />
      </tocitem>
      <tocitem target="obj_views.htm-CHDJJJIC" text="Managing Views">
        <tocitem target="obj_views_create.htm-CHDICCCJ" text="Creating a View" />
        <tocitem target="obj_views_browse.htm-CHDDDEDF" text="Browsing a View">
          <tocitem target="obj_views_browse.htm-sthref50" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_views_edit.htm-CHDHFEBF" text="Editing a View">
          <tocitem target="obj_views_edit.htm-sthref57" text="Editing a View Manually" />
          <tocitem target="obj_views_edit.htm-sthref58" text="Using Find and Replace" />
          <tocitem target="obj_views_edit.htm-sthref59" text="Downloading a View" />
        </tocitem>
        <tocitem target="obj_views_compile.htm-BACBJHDI" text="Compiling a View" />
        <tocitem target="obj_views_drop.htm-CHDICEGI" text="Dropping a View" />
      </tocitem>
      <tocitem target="obj_index.htm-CHDIHDAA" text="Managing Indexes">
        <tocitem target="obj_index_create.htm-CHDBFGAA" text="Creating an Index" />
        <tocitem target="obj_index_browse.htm-CHDHGJCD" text="Browsing an Index">
          <tocitem target="obj_index_browse.htm-sthref67" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_index_drop.htm-CIHCBBAA" text="Dropping an Index" />
      </tocitem>
      <tocitem target="obj_seq.htm-CHDJIHDJ" text="Managing Sequences">
        <tocitem target="obj_seq_create.htm-CHDIFAJH" text="Creating a Sequence" />
        <tocitem target="obj_seq_browse.htm-CHDCBCFH" text="Browsing a Sequence">
          <tocitem target="obj_seq_browse.htm-sthref76" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_seq_drop.htm-CIHHHDIE" text="Dropping a Sequence" />
      </tocitem>
      <tocitem target="obj_type.htm-CHDFDGGE" text="Managing Types">
        <tocitem target="obj_type_create.htm-CHDGEIBD" text="Creating a Type" />
        <tocitem target="obj_type_browse.htm-CHDHJEAE" text="Browsing a Type">
          <tocitem target="obj_type_browse.htm-sthref85" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_type_drop.htm-CIHHGIJA" text="Dropping a Type" />
      </tocitem>
      <tocitem target="obj_pack.htm-CHDDBIIF" text="Managing Packages">
        <tocitem target="obj_pack_create.htm-CHDIDBCD" text="Creating a Package" />
        <tocitem target="obj_pack_view.htm-CHDIBCGE" text="Viewing a Package">
          <tocitem target="obj_pack_view.htm-sthref93" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_pack_edit.htm-CHDCAHBF" text="Editing a Package">
          <tocitem target="obj_pack_edit.htm-sthref103" text="Editing a Package Manually" />
        </tocitem>
        <tocitem target="obj_pack_compile.htm-CHDEJHHI" text="Compiling a Package" />
        <tocitem target="obj_pack_downld.htm-CHDCDCDG" text="Downloading a Package" />
        <tocitem target="obj_pack_drop.htm-CHDDFBAH" text="Dropping a Package" />
      </tocitem>
      <tocitem target="obj_proc.htm-CHDIHHCG" text="Managing Procedures">
        <tocitem target="obj_proc_create.htm-CHDEAHFJ" text="Creating a Procedure" />
        <tocitem target="obj_proc_browse.htm-CHDDABID" text="Browsing a Procedure">
          <tocitem target="obj_proc_browse.htm-sthref110" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_proc_edit.htm-CHDEGHCE" text="Editing a Procedure">
          <tocitem target="obj_proc_edit.htm-sthref114" text="Editing a Procedure Manually" />
        </tocitem>
        <tocitem target="obj_proc_compile.htm-CHDEBEAC" text="Compiling a Procedure" />
        <tocitem target="obj_proc_download.htm-CHDDDJBH" text="Downloading a Procedure" />
        <tocitem target="obj_proc_drop.htm-CHDBDJHB" text="Dropping a Procedure" />
      </tocitem>
      <tocitem target="obj_func.htm-CHDFGFEH" text="Managing Functions">
        <tocitem target="obj_func_create.htm-CHDGJIAB" text="Creating a Function" />
        <tocitem target="obj_func_browse.htm-CHDEIEIB" text="Browsing a Function">
          <tocitem target="obj_func_browse.htm-sthref122" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_func_edit.htm-CHDHHJFA" text="Editing a Function">
          <tocitem target="obj_func_edit.htm-sthref125" text="Editing a Function Manually" />
        </tocitem>
        <tocitem target="obj_func_compile.htm-CHDCBEHB" text="Compiling a Function" />
        <tocitem target="obj_func_dnload.htm-CHDGBIBB" text="Downloading a Function" />
        <tocitem target="obj_func_drop.htm-CHDDAJHA" text="Dropping a Function" />
      </tocitem>
      <tocitem target="obj_trig.htm-CHDHEHCC" text="Managing Triggers">
        <tocitem target="obj_trig_create.htm-CHDEHFHI" text="Creating Triggers" />
        <tocitem target="obj_trig_browse.htm-CHDFFIJI" text="Browsing a Trigger">
          <tocitem target="obj_trig_browse.htm-sthref132" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_trig_edit.htm-CHDBBGJB" text="Editing a Trigger">
          <tocitem target="obj_trig_edit.htm-sthref135" text="Editing a Trigger Manually" />
        </tocitem>
        <tocitem target="obj_trig_compile.htm-CHDCJHGE" text="Compiling a Trigger" />
        <tocitem target="obj_trig_down.htm-CHDJBBCH" text="Downloading a Trigger" />
        <tocitem target="obj_trig_drop.htm-CHDBHGFB" text="Dropping a Trigger" />
      </tocitem>
      <tocitem target="obj_dblink.htm-CHDCEAAB" text="Managing Database Links">
        <tocitem target="obj_dblink_create.htm-CHDDHIBA" text="Creating a Database Link" />
        <tocitem target="obj_dblink_brows.htm-CHDFECGG" text="Browsing a Database Link">
          <tocitem target="obj_dblink_brows.htm-sthref142" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_dblink_drop.htm-CIHEAGIJ" text="Dropping a Database Link" />
      </tocitem>
      <tocitem target="obj_mview.htm-CHDEGDEA" text="Managing Materialized Views">
        <tocitem target="obj_mview_create.htm-CHDJHBFG" text="Creating a Materialized View" />
        <tocitem target="obj_mview_brows.htm-CHDGAAFJ" text="Browsing a Materialized View">
          <tocitem target="obj_mview_brows.htm-sthref148" text="Summary of Available Views" />
        </tocitem>
        <tocitem target="obj_mview_drop.htm-CIHEBDFC" text="Dropping a Materialized View" />
      </tocitem>
      <tocitem target="obj_syn.htm-CHDCBIJH" text="Managing Synonyms">
        <tocitem target="obj_syn_create.htm-CHDDCJDE" text="Creating Synonyms" />
        <tocitem target="obj_syn_view.htm-CHDGAFEA" text="Viewing a Synonym" />
        <tocitem target="obj_syn_drop.htm-CHDDGFCG" text="Dropping a Synonym" />
      </tocitem>
    </tocitem>
    <tocitem target="sql_proc.htm" text="Using SQL Commands">
      <tocitem target="sql_proc_about.htm-BABDIEAG" text="What is SQL Commands?" />
      <tocitem target="sql_about.htm-sthref160" text="About the SQL Workshop Page" />
      <tocitem target="sql_proc_access.htm-BABIAFHE" text="Accessing SQL Commands" />
      <tocitem target="sql_proc_hm.htm-CHDHBDFG" text="About the SQL Commands Home Page" />
      <tocitem target="sql_proc_execute.htm-CEGCECEI" text="Using the Command Editor">
        <tocitem target="sql_proc_run.htm-CHDJBCCH" text="Running a SQL Command" />
        <tocitem target="sql_proc_trans.htm-BABDBJDG" text="About Transactions in SQL Commands" />
        <tocitem target="sql_proc_unsupport.htm-BABHADFA" text="About Unsupported SQL*Plus Commands" />
        <tocitem target="sql_proc_term.htm-CHDBHGFD" text="About Command Termination" />
        <tocitem target="sql_proc_bind.htm-CHDBIIJJ" text="Using Bind Variables" />
        <tocitem target="wrkshp_table_finder.htm-BACIHGCA" text="Using the Find Tables Icon" />
      </tocitem>
      <tocitem target="sql_proc_save.htm-CHDIFFED" text="Saving an SQL Command" />
      <tocitem target="sql_proc_copy.htm-CHDCIGBA" text="Copying a Command" />
      <tocitem target="sql_proc_sav_scripts.htm-CHDFEBFJ" text="Using Saved Commands">
        <tocitem target="sql_proc_sav_scripts_go.htm-BABIEJBI" text="Accessing Saved Commands" />
        <tocitem target="sql_proc_sql_pane.htm-BABHDGGE" text="About the Saved SQL Pane" />
      </tocitem>
      <tocitem target="sql_proc_hist.htm-CHDFGBFG" text="Using SQL Command History">
        <tocitem target="sql_proc_hist_go.htm-BABJAICF" text="Accessing a Command from Command History" />
        <tocitem target="sql_proc_hist_pane.htm-BABJBGCD" text="About the History Pane" />
      </tocitem>
      <tocitem target="sql_proc_results.htm-CHDDJJED" text="Viewing Results">
        <tocitem target="sql_proc_rslt_pane.htm-CHDDFGGC" text="Accessing the Results Pane" />
        <tocitem target="sql_proc_reslt_pane.htm-CHDBECJE" text="About the Results Pane" />
      </tocitem>
      <tocitem target="sql_proc_expl.htm-CHDJJFJI" text="Using Explain Plan">
        <tocitem target="sql_proc_expl_view.htm-BABCCHJA" text="Viewing an Explain Plan" />
        <tocitem target="sql_proc_expl_about.htm-CHDHCICH" text="About Explain Plan Pane" />
      </tocitem>
    </tocitem>
    <tocitem target="sql_rep.htm" text="Using SQL Scripts">
      <tocitem target="sql_rep_about.htm-BABCIFHB" text="What is SQL Scripts?" />
      <tocitem target="sql_rep_access.htm-BABEJFCA" text="Accessing SQL Scripts">
        <tocitem target="sql_rep_about_pg.htm-BABCAJHI" text="About the SQL Scripts Page" />
      </tocitem>
      <tocitem target="sql_rep_create.htm-BABBHDEB" text="Creating a SQL Script">
        <tocitem target="sql_rep_create_editor.htm-BABEEDAB" text="Creating a SQL Script in the Script Editor" />
        <tocitem target="sql_rep_script_upload.htm-BABDEIEG" text="Uploading a SQL Script" />
      </tocitem>
      <tocitem target="sql_rep_editor_using.htm-BABFJJCE" text="Using the Script Editor">
        <tocitem target="sql_rep_edit.htm-CHDEGIBC" text="Editing an Existing Script" />
        <tocitem target="sql_rep_find.htm-CHDIDDDJ" text="Searching and Replacing Text or Regular Expressions" />
        <tocitem target="sql_rep_editor_ctrls.htm-CHDIEAEJ" text="Summary of Script Editor Controls" />
      </tocitem>
      <tocitem target="sql_rep_del_pg.htm-BABJHBJD" text="Deleting a SQL Script">
        <tocitem target="sql_rep_del_frm_pg.htm-BABBEFBE" text="Deleting Scripts from the SQL Scripts Page" />
        <tocitem target="sql_rep_del_editor.htm-BABGJFFJ" text="Deleting a Script in the Script Editor" />
      </tocitem>
      <tocitem target="sql_rep_copy.htm-BABHFEJE" text="Copying a SQL Script" />
      <tocitem target="sql_rep_execute.htm-BABJBFDJ" text="Executing a SQL Script">
        <tocitem target="sql_rep_script_edit.htm-BABJCACE" text="Executing a SQL Script in the Script Editor" />
        <tocitem target="sql_rep_scripts_pg.htm-BABHGAHB" text="Executing a SQL Script from the SQL Scripts Page" />
        <tocitem target="sql_rep_run_script.htm-BABIGCGH" text="About the Run Script Page" />
      </tocitem>
      <tocitem target="sql_rep_results.htm-BABCFDBI" text="Viewing SQL Script Results">
        <tocitem target="sql_rep_rsults_script.htm-BABHEGIH" text="Viewing Results from the SQL Scripts Page" />
        <tocitem target="sql_rep_results_pg.htm-BABJEBCD" text="About the Results Page" />
      </tocitem>
      <tocitem target="sql_rep_transfer.htm-BABBHEHA" text="Exporting and Importing SQL Scripts">
        <tocitem target="sql_rep_exprt.htm-BABDCGIE" text="Copying Scripts to an Export Script">
          <tocitem target="sql_rep_exprt.htm-BABGGADD" text="About the Scripts Pane" />
          <tocitem target="sql_rep_exprt.htm-BABIHBAH" text="About the Scripts to Export Pane" />
        </tocitem>
        <tocitem target="sql_rep_import.htm-BABFHBHF" text="Importing Scripts from an Export Script">
          <tocitem target="sql_rep_import.htm-BABFFIJC" text="About the Import Scripts Pane" />
        </tocitem>
      </tocitem>
      <tocitem target="sql_rep_quotas.htm-BABJHCBH" text="Viewing Script and Result Quotas" />
    </tocitem>
    <tocitem target="qry_bldr.htm" text="Building Queries with Query Builder">
      <tocitem target="qry_blr_about.htm-CHDGIDIF" text="About Query Builder">
        <tocitem target="qry_blr_goto.htm-BABIJAAA" text="Accessing Query Builder" />
        <tocitem target="qry_blr_proc.htm-BABIBBFD" text="Understanding the Query Building Process" />
      </tocitem>
      <tocitem target="qry_blr_objsel.htm-BABCBACJ" text="Using the Object Selection Pane">
        <tocitem target="qry_blr_search.htm-BABDECCJ" text="Searching and Filtering Objects" />
        <tocitem target="qry_blr_hide.htm-BABBCDGG" text="Hiding the Object Selection Pane" />
      </tocitem>
      <tocitem target="qry_blr_design.htm-BABBGFGG" text="Selecting Objects">
        <tocitem target="qry_blr_col_type.htm-BABBECGI" text="About Supported Column Types" />
        <tocitem target="qry_blr_sel_obj.htm-BABIAJAI" text="Adding an Object to the Design Pane">
          <tocitem target="qry_blr_sel_obj.htm-BABJDEBG" text="Resizing the Design and Results Panes" />
        </tocitem>
        <tocitem target="qry_blr_obj.htm-BABEEICA" text="Removing or Hiding Objects in the Design Pane" />
      </tocitem>
      <tocitem target="qry_blr_cond.htm-BABCGHHE" text="Specifying Query Conditions" />
      <tocitem target="qry_blr_join.htm-BABHBFEC" text="Creating Relationships Between Objects">
        <tocitem target="qry_blr_join_about.htm-BABJABCD" text="About Join Conditions" />
        <tocitem target="qry_blr_join_manual.htm-BABJHDED" text="Joining Objects Manually" />
        <tocitem target="qry_blr_join_auto.htm-BABFEFHF" text="Joining Objects Automatically" />
      </tocitem>
      <tocitem target="qry_blr_save.htm-CHDBHIEI" text="Working with Saved Queries">
        <tocitem target="qry_blr_saving.htm-BABHIEDI" text="Saving a Query" />
        <tocitem target="qry_blr_editing.htm-BABDJJID" text="Editing a Saved Query" />
        <tocitem target="qry_blr_deleting.htm-BABGGIDI" text="Deleting a Saved Query" />
      </tocitem>
      <tocitem target="qry_blr_sql.htm-BABBHABE" text="Viewing Generated SQL" />
      <tocitem target="qry_blr_results.htm-CHDDIIGC" text="Viewing Query Results" />
    </tocitem>
    <tocitem target="sql_utl.htm" text="Using Oracle Application Express Utilities">
      <tocitem target="sql_utl_exprt_imprt_about.htm-BCEJBFDF" text="About Importing, Exporting, Loading, and Unloading Data">
        <tocitem target="sql_utl_exprt_imprt_choose.htm-CBHBGGGD" text="Choosing the Right Import/Export/Load/Unload Option" />
      </tocitem>
      <tocitem target="sql_utl_exprt_imprt.htm-BABJHFEC" text="Loading and Unloading Data from the Database">
        <tocitem target="sql_utl_exprt_imprt_pg.htm-CEGHIFIF" text="Accessing the Data Load/Unload Page" />
        <tocitem target="sql_utl_imprt.htm-BCEDIEAH" text="Loading Data">
          <tocitem target="sql_utl_imprt.htm-BCECBIEA" text="Loading a Text File or Spreadsheet Data" />
          <tocitem target="sql_utl_imprt.htm-BCEHEFHB" text="Loading an XML Document" />
        </tocitem>
        <tocitem target="sql_utl_exprt.htm-BCEFBDJA" text="Unloading Data">
          <tocitem target="sql_utl_exprt.htm-BCEFEDEH" text="Unloading a Text File" />
          <tocitem target="sql_utl_exprt.htm-BCEDBFEG" text="Unloading to an XML Document" />
        </tocitem>
        <tocitem target="sql_utl_imprt_rep.htm-BABIECGA" text="Using Repository" />
      </tocitem>
      <tocitem target="sql_utl_obj_rpt.htm-BABHJCAB" text="Viewing Object Reports">
        <tocitem target="sql_utl_tbl.htm-CBHCEEGB" text="Table Reports" />
        <tocitem target="sql_utl_sec.htm-BABHBCIH" text="Security Reports" />
        <tocitem target="sql_utl_plsql.htm-BABDHEBE" text="PL/SQL Reports">
          <tocitem target="sql_utl_plsql.htm-CBHBAGGC" text="Program Unit Arguments" />
          <tocitem target="sql_utl_plsql.htm-CBHJIFBI" text="Unit Line Counts" />
          <tocitem target="sql_utl_plsql.htm-CBHHIFFE" text="Search PL/SQL Source Code" />
        </tocitem>
        <tocitem target="sql_utl_excp_rpt.htm-CBHEEHEJ" text="Exception Reports" />
        <tocitem target="sql_utl_objs.htm-BABHACBJ" text="All Object Reports" />
      </tocitem>
      <tocitem target="sql_utl_ddl.htm-BABBIJIH" text="Generating DDL" />
      <tocitem target="sql_utl_user_def.htm-CBHGGFCC" text="Managing User Interface Defaults">
        <tocitem target="sql_utl_user_def_create.htm-CBHIGFJF" text="Creating User Interface Defaults for a Table" />
        <tocitem target="sql_utl_user_def_mod.htm-CBHCEJIC" text="Modifying Table User Interface Defaults" />
        <tocitem target="sql_utl_user_att_create.htm-CBHECFBI" text="Creating User Interface Attributes" />
        <tocitem target="sql_utl_user_att_mod.htm-CBHIFFHA" text="Modifying User Interface Attributes" />
        <tocitem target="sql_utl_user_def_import.htm-CEGCADJI" text="About Exporting and Importing User Interface Defaults" />
      </tocitem>
      <tocitem target="sql_utl_restor.htm-CHDFCEDH" text="Using the Recycle Bin to View and Restore Dropped Objects">
        <tocitem target="sql_utl_rcycl_.htm-BABHFDAI" text="Managing Objects in the Recycle Bin" />
        <tocitem target="sql_utl_rcycl_empty.htm-BABEDIBA" text="Emptying the Recycle Bin Without Viewing the Objects" />
      </tocitem>
      <tocitem target="sql_utl_comp_schema.htm-CBHEGADC" text="Comparing Schemas" />
      <tocitem target="dbadm_montitor.htm-BABFGDIB" text="Monitoring the Database">
        <tocitem target="dbadm_sessions.htm-CIHJBGCE" text="Sessions">
          <tocitem target="dbadm_sessions.htm-BEJDGBFC" text="Sessions Report" />
          <tocitem target="dbadm_sessions.htm-sthref340" text="Locks Report" />
          <tocitem target="dbadm_sessions.htm-sthref343" text="Waits Report" />
          <tocitem target="dbadm_sessions.htm-sthref346" text="I/O Report" />
          <tocitem target="dbadm_sessions.htm-sthref349" text="SQL Report" />
          <tocitem target="dbadm_sessions.htm-sthref352" text="Open Cursors" />
        </tocitem>
        <tocitem target="dbadm_stats.htm-CIHFAAFD" text="About System Statistics" />
        <tocitem target="dbadm_topsql.htm-CBHJACJG" text="About Top SQL" />
        <tocitem target="dbadm_long_opr.htm-CBHHADCC" text="About Long Operations" />
      </tocitem>
      <tocitem target="dbadm_db_detail.htm-CIHDBAJF" text="Viewing Database Details" />
    </tocitem>
  </tocitem>
</toc>
