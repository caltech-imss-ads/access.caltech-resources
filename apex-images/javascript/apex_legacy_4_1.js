/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 4.0
B32468-02
Copyright © 2003, 2008, Oracle. All rights reserved.
The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.
The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.
If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:
U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.
Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.
The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/
/**
 * @fileOverview
 * This file holds for legacy purpose all the old non namespaced javascript functions of APEX. 
 *
 * */


/**
 * Submits the page setting the Application Express Request value (pRequest).
 * @function
 * @param {String} pRequest
 */
function doSubmit(pOptions){
  apex.submit(pOptions);
};

/* needs some fixing */


/**
 * Displays a confirmation showing a message (pMessage) and depending on user's choice, submits a page setting request value (pRequest) or cancels page submit.
 * @function
 * @param {String}[pMessage]
 * @param {String}[pRequest]
 */
function confirmDelete(pMessage,pOptions){
	apex.confirm(pMessage,pOptions);
};

/**
 * Returns true or false if a form element is empty, this will consider any whitespace including a space, a tab, a form-feed, as empty.
 * @function
 * @param {DOM node | String} pThis
 * @return {true | false}
 * */
function $v_IsEmpty(pThis) {
	return apex.item(pThis).isEmpty();
}

/**
 * @ignore
 * */
function html_submitFormFromKeyPress(key){
  if(event.keyCode == "13"){
    apex.submit();
  }
};

/**
 * Submits a page when ENTER is pressed in a text field, setting the request value to the ID of a DOM node (pNd).
 * @function
 * */
function submitEnter(pNd,e){
  var lThis = $x(pNd);
  var lRequest = (lThis)?lThis.id:'';
  var keycode;
  if(window.event){keycode = window.event.keyCode;
  }else if(e){keycode = e.which;
  }else{return true;}
  if (keycode == 13){
    apex.submit(lRequest);
    return false;
  }else{
    return true;
  }
};









/* all deprecated functions here */
/**
 * @deprecated
 * @function
 * */
function html_processing(){
  var t = $x("htmldbWait");
   if (!t) {
    var l_newDiv = document.createElement('DIV');
    l_newDiv.className="htmldbProcessing";
    l_newDiv.style.zIndex=20000;
    l_newDiv.id = "htmldbDisablePage";
    l_newDiv.style.width = "100%";
    l_newDiv.style.height = "100%";
    l_newDiv.onclick = "return false;";
    l_newDiv.style.position="absolute";
    l_newDiv.style.top="0";
    l_newDiv.style.left="0";
    document.body.insertBefore(l_newDiv,document.body.firstChild);
  }
}
/**
 * @deprecated
 * @function
 * */
function html_enableBase(){
  var t = $x("htmldbDisablePage");
   if (t){t.parentNode.removeChild(t);}
}
/**
 * @deprecated
 * @function
 * */
function html_disableBase(z,c){
  var t = $x("htmldbDisablePage");
   if (!t) {
    var l_newDiv = document.createElement('DIV');
    l_newDiv.className = (!!c)?c:"htmldbDisablePage";
    l_newDiv.style.zIndex=z;
    l_newDiv.id = "htmldbDisablePage";
    l_newDiv.style.width = "100%";
    l_newDiv.style.height = "100%";
    l_newDiv.onclick = "return false;";
    l_newDiv.style.position="absolute";
    l_newDiv.style.top="0";
    l_newDiv.style.left="0";
    document.body.insertBefore(l_newDiv,document.body.firstChild);
  }
}
/**
 * @deprecated
 * @function
 * */
function dhtml_CloseDialog(pThis){
  html_enableBase();
  $x_Hide($x_UpTill(pThis,'TABLE'));
  toolTip_disable();
}
/**
 * @deprecated
 * @function
 * */
function html_Centerme(id){
	var t = $x(id);
	if(document.all){
		l_Width = document.body.clientWidth;
		l_Height = document.body.clientHeigth;
	}else{
		l_Width = window.innerWidth;
		l_Height = window.innerHeight;
	}
	var tW=t.offsetWidth;
	var tH=t.offsetHeight;
	t.style.top = '40%';
	t.style.left = '40%';
}
/**
 * @deprecated
 * @function
 * */
function hideShow(objectID,imgID,showImg,hideImg){
    var theImg = $x(imgID);
    var theDiv = $x(objectID);
    if(theDiv.style.display == 'none' || theDiv.style.display == '' || theDiv.style == null){
        theImg.src = hideImg;
        $x(objectID).style.display = 'block';}
    else{
        theImg.src = showImg;
        $x(objectID).style.display = 'none';}
    return;
}
/**
 * @deprecated
 * @function
 * */
function disableItems(testString,item1,item2,item3,item4,item5,item6,item7,item8,item9,item10){
	var theTest = eval(testString);
	var i = 1;
	if(theTest){
		for(i;i<12;i++){
			if (!!arguments[i]){
				disItem = $x(arguments[i]);
				disItem.style.background = '#cccccc';
				disItem.disabled = true;
			}
		}
	}else{
		for(i;i<12;i++){
			if (!!arguments[i]){
				disItem = $x(arguments[i]);
				disItem.disabled = false;
				disItem.style.background = '#ffffff';
			}
		}
	}
}
/**
 * @deprecated
 * @function
 * */
function htmldbCheckCookie(pThis){
  SetCookie ('ISCOOKIE','true');
  flow = GetCookie ('ISCOOKIE');
  return;
}
/**
 * @deprecated
 * @function
 * */
function shuttleItem(theSource, theDest, moveAll) {
    var srcList  = $x(theSource),destList = $x(theDest),arrsrcList = [],arrdestList = [],arrLookup = [],i;
    if (moveAll){
    for (i = 0;i <= srcList.length-1; i++ ){
			  srcList.options[i].selected = true;}
    }
    for (i = 0; i < destList.options.length; i++) {
        arrLookup[destList.options[i].text] = destList.options[i].value;
        arrdestList[i] = destList.options[i].text;}
    var fLength = 0;
    var tLength = arrdestList.length;
    for(i = 0; i < srcList.options.length; i++) {
        arrLookup[srcList.options[i].text] = srcList.options[i].value;
        if (!!srcList.options[i].selected && !!srcList.options[i].value) {
            arrdestList[tLength] = srcList.options[i].text;
            tLength++;}
        else {
            arrsrcList[fLength] = srcList.options[i].text;
            fLength++;}
    }
    arrsrcList.sort();
    arrdestList.sort();
    srcList.length = 0;
    destList.length = 0;
    var c,no;
    for(c = 0; c < arrsrcList.length; c++) {
        no = new Option();
        no.value = arrLookup[arrsrcList[c]];
        no.text = arrsrcList[c];
        srcList[c] = no;
    }
    for(c = 0; c < arrdestList.length; c++) {
        no = new Option();
        no.value = arrLookup[arrdestList[c]];
        no.text = arrdestList[c];
        destList[c] = no;
       }
}
/**
 * @deprecated
 * @function
 * */
function cDebug(pThis,pThat){}
/**
 * @deprecated
 * @function
 * */
function html_VisibleElement(pNd){var l_Node = $x(pNd);if(l_Node){l_Node.style.visibility = "visible";}return l_Node;}
/**
 * @deprecated
 * @function
 * */
function html_HiddenElement(pNd){var l_Node = $x(pNd);if(l_Node){l_Node.style.visibility = "hidden";}return l_Node;}
/**
 * @deprecated use
 * @function
 * */
function html_TabMakeCurrent(pThis){var node = $x(pThis);if(node){var nodeSibs = node.parentNode.parentNode.childNodes;for(var i=0;i < nodeSibs.length;i++){if(nodeSibs[i] && nodeSibs[i].nodeType == 1 && nodeSibs[i].getElementsByTagName('A')[0]){nodeSibs[i].getElementsByTagName('A')[0].className = "";}}pThis.className = "tabcurrent";}return node;}
/**
 * @deprecated use $x_HideSiblings
 * @function
 * */
html_HideSiblings = $x_HideSiblings;
/**
 * @deprecated use $x_ShowSiblings
 * @function
 * */
html_ShowSiblings = $x_ShowSiblings;
/**
 * @deprecated
 * @function
 * */
function html_ShowAllByClass(pThis,pClass,pTag) {$x_ShowAllByClass(pThis,pClass,pTag);}
/**
 * @deprecated
 * @function
 * */
function f_Hide_On_Value_Item(pThis,pThat,pValue){return $f_Hide_On_Value_Item(pThis,pThat,pValue);}
/**
 * @deprecated
 * @function
 * */
function f_Hide_On_Value_Item_Row(pThis,pThat,pValue){return $f_Hide_On_Value_Item_Row(pThis,pThat,pValue);}
/**
 * @deprecated
 * @function
 * */
function html_disableItems(a,nd){if(nd){var lArray = [];for (var i=1,len=arguments.length;i<len;i++){if(arguments[i]){lArray[lArray.length]=arguments[i];}}html_disableItem(lArray,a);}return;}
/**
 * @deprecated
 * @function
 * */
function html_GetPageScroll(){return getScrollXY()[1];}
/**
 * @deprecated
 * @function
 * */
function popUpNamed(pURL,pName) {html_PopUp(pURL,pName,720,600);}
/**
 * @deprecated
 * @function
 * */
function popUp2(pURL,pWidth,pHeight) {day = new Date();pName = day.getTime();html_PopUp(pURL,pName,pWidth,pHeight);}
/**
 * @deprecated
 * @function
 * */
function popUp(pURL) {day = new Date();pName = day.getTime();html_PopUp(pURL,pName,null,null);}
/**
 * @deprecated
 * @function
 * */
function popupURL(pURL){html_PopUp(pURL,"winLov",800,600);}
/**
 * @deprecated
 * @function
 * */
function $x_SetClassArray(pNd,pClass){$x_Class(pNd,pClass);}
/**
 * @deprecated
 * @function
 * */
function html_TabClick(pThis,pId){var nodeSibs = $x(pThis).parentNode.parentNode.childNodes , lSibArray = [];for(var i=0;i < nodeSibs.length;i++){if($x(nodeSibs[i]) && nodeSibs[i].getElementsByTagName('A')[0]){lSibArray[lSibArray.length] = nodeSibs[i].getElementsByTagName('A')[0];}}$d_TabClick(pThis,pId,'tabcurrent',lSibArray);return;}
/**
 * @deprecated
 * @function
 * */
function detailTab(id){html_TabClick(id);return;}
/**
 * @deprecated
 * @function
 * */
function retFalse(){return false;}
/**
 * @deprecated
 * @function
 * */
function getSelected(opt){var selected=[];for (var i=0;i<opt.length;i++){if (opt[i].selected){selected[selected.length]=opt[i];}}return selected;}
/**
 * @deprecated
 * @function
 * */
 $x_AddTag = $dom_AddTag;
/**
 * @deprecated
 * @function
 * */
function html_CreateFormElement(pType,pName,pValue){return $dom_AddInput(false,pType,'',pName,pValue);}
/**
 * @deprecated
 * @function
 * */
html_disableItem = $x_disableItem;
/**
 * @deprecated
 * @function
 * */
html_CascadeUpTill = $x_UpTill;
/**
 * @deprecated
 * @function
 * */
html_HideItemRow=$x_HideItemRow;
/**
 * @deprecated
 * @function
 * */
html_ShowItemRow=$x_ShowItemRow;
/**
 * @deprecated
 * @function
 * */
html_ToggleItemRow=$x_ToggleItemRow;
/**
 * @deprecated
 * @function
 * */
html_ShowAllChildren = $x_ShowChildren;
/**
 * @deprecated
 * @function
 * */
setStyle = $x_Style;
/**
 * @deprecated
 * @function
 * */
html_replace = html_StringReplace;
/**
 * @deprecated
 * @function
 * */
upperMe = $v_Upper;
/**
 * @deprecated
 * @function
 * */
html_DisableOnValue = $f_DisableOnValue;
/**
 * @deprecated
 * @function
 * */
htmldb_ToggleTableBody = $x_ToggleWithImage;
/**
 * @deprecated
 * @function
 * */
htmldb_ToggleWithImage = $x_ToggleWithImage;
/**
 * @deprecated
 * @function
 * */
html_Return_Form_Items = $x_FormItems;
/**
 * @deprecated
 * @function
 * */
html_Find = $d_Find;
/**
 * @deprecated
 * @function
 * */
$f_SetValue = $x_Value;
/**
 * @deprecated
 * @function
 * */
setValue = $x_Value;
/**
 * @deprecated
 * @function
 * */
html_MakeParent = $dom_MakeParent;
/**
 * @deprecated
 * @function
 * */
isEmpty = $v_IsEmpty;
/**
 * @deprecated see $x()
 * @function
 * */
html_GetElement=$x;
/**
 * @deprecated see $x()
 * @function
 * */
$x_El = $x;
/**
 * @deprecated see $x_Toggle()
 * @function
 * */
html_ToggleElement = $x_Toggle;
/**
 * @deprecated see $x_Hide()
 * @function
 * */
html_HideElement = $x_Hide;
/**
 * @deprecated see $x_Show()
 * @function
 * */
html_ShowElement = $x_Show;
/**
 * @deprecated see $u_SubString()
 * @function
 * */
html_SubString = $u_SubString;
/**
 * @deprecated
 * @function
 * */
getElementsByClass = $x_ByClass;
/**
 * @deprecated
 * @function
 * */
html_SwitchImageSrc = $x_SwitchImageSrc;
/** @ignore */
html_CheckValueAgainst = $v_CheckValueAgainst;
/**
 * @deprecated
 * @function
 * */
html_CheckAll = $f_CheckAll;
/**
 * @deprecated
 * @function
 * */
first_field = $f_First_field;
/**
 * @deprecated
 * @function
 * */
$f_InitTextFieldSubmits = html_InitTextFieldSubmits;
/**
 * @deprecated
 * @function
 * */
html_CheckImageSrc = $x_CheckImageSrc;
/**
 * @deprecated
 * @function
 * */
setStyleByClass = $x_StyleByClass;
/**
 * @deprecated
 * @function
 * */
function html_CleanRegionId(pRid){
	var l_PTest = pRid.indexOf('.');
	var l_CTest = pRid.indexOf(',');
	var l_Rid = pRid;
	if(l_PTest >= 0){l_Rid = l_Rid.substring(0,l_PTest);
	}else if (l_CTest >= 0){l_Rid = l_Rid.substring(0,l_CTest);}
	return l_Rid;
}
/**
 * @deprecated
 * @function
 * */
function init_htmlPPRReport2(pId){
  var l_Table = $x('report'+pId);
  if(l_Table){
    var l_THS = l_Table.getElementsByTagName('TH');
    for(var i = 0;i<l_THS.length;i++){
    if(l_THS[i].getElementsByTagName('A')[0]){
      var oldHREF = l_THS[i].getElementsByTagName('A')[0].href;
      l_THS[i].getElementsByTagName('A')[0].href = 'javascript:html_PPR_Report_Page(this,\''+pId+'\',\''+oldHREF+'\');';
      }
    }
  }
  return;
}
/**
 * @deprecated
 * @function
 * */
function init_htmlPPRReport(pId){
  if(document.all){
    var ie_HACK = 'init_htmlPPRReport2(\''+pId+'\')';setTimeout(ie_HACK,100);}
    else{init_htmlPPRReport2(pId);}
  return;
}
/**
 * @deprecated
 * @function
 * */
function html_PPR_Report_Page (pThis,pRid,pURL,pHeader,pFooter){
	var l_pRid = html_CleanRegionId(pRid);
	document.body.style.cursor = 'wait';
    var l_URL = pURL;
    var start = l_URL.indexOf('?');
    l_URL = l_URL.substring(start + 1);
    l_URL = html_replace(l_URL,'pg_R_','FLOW_PPR_OUTPUT_'+l_pRid+'_pg_R_');
    l_URL = html_replace(l_URL,'fsp_sort_','FLOW_PPR_OUTPUT_'+l_pRid+'_fsp_sort_');
    var http = new htmldb_Get('report'+ l_pRid,null,null,null,null,'f',l_URL);
    http.get(null,'<htmldb:'+l_pRid+'>','</htmldb:'+l_pRid+'>');
    if(pHeader){$x('report'+ l_pRid).innerHTML =  pHeader + $x('report'+ l_pRid).innerHTML;}
    if(pFooter){$x('report'+ l_pRid).innerHTML += pFooter;}
    document.body.style.cursor = '';
    init_htmlPPRReport(l_pRid);
    http = null;
    return;
}
/**
 * @deprecated
 * @function
 * */
function PPR_Tabluar_Submit(pId,pFlowID,pPageId,pRequest,pInsertReturn,pReportId,pReplacementOveride){
	var pThis = $x(pId),get,i,q;
	if(pInsertReturn){get = new htmldb_Get(pId,pFlowID,pRequest,pPageId,null,'wwv_flow.accept');}
	else{get = new htmldb_Get(null,pFlowID,pRequest,pPageId,null,'wwv_flow.accept');}
	var lItems = $x_FormItems(pThis);
	for(i=0;i<lItems.length;i++){
		if(lItems[i].type == 'checkbox'){
			if(!!lItems[i].checked){get.addParam(lItems[i].name,lItems[i].value);}
		}else{
			if(lItems[i].name && lItems[i].name != 'fcs'){get.addParam(lItems[i].name,lItems[i].value);}
		}
	}
	var lSelects = $x_FormItems(pThis,'SELECT');
	for(i=0;i<lSelects.length;i++){get.addParam(lSelects[i].name,html_SelectValue(lSelects[i]));}
	var lTextarea= $x_FormItems(pThis,'TEXTAREA');
	for(i=0;i<lTextarea.length;i++){get.addParam(lTextarea[i].name,lTextarea[i].value);}
	if(pReplacementOveride){
		q = get.get(null,'<htmldb:'+pReplacementOveride+'>','</htmldb:'+pReplacementOveride+'>');
		}else{
		q = get.get(null,'<htmldb:PPR_'+pId+'>','</htmldb:PPR_'+pId+'>');
	}
		if(pReportId){init_htmlPPRReport(pReportId);}
		get = null;
	return q;
}

/**
 * @deprecated
 * @function
 * */
function removeMessageTimeout(){setTimeout(function(){$x('htmldbMessageHolder').innerHTML = '';},5000);}

/**
 * @deprecated
 * */
html_RowHighlight = $x_RowHighlight;
/**
 * @deprecated
 * @function
 * */
html_RowHighlightOff = $x_RowHighlightOff;
/**
 * @deprecated
 * @function
 * */
html_SelectedOptions = $f_SelectedOptions;
/**
 * @deprecated
 * @function
 * */
html_SelectValue = $f_SelectValue;


