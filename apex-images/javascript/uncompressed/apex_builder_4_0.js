/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 4.0

B32468-02

Copyright © 2003, 2010, Oracle. All rights reserved.

Primary Author: Carl Backstrom, Patrick Wolf

The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.

The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.

If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:

U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.

The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.

Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.

The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/

/**
 * @fileOverview
 * This file contains javascript specific to apex builder enviroment functions and objects for Oracle Application Express
 *
 * */

// initialize namespace
if (!window.apex)  window.apex  = {};
if (!apex.builder) apex.builder = {
  /* Current application and page which gets edited */
  gApplicationId:null,
  gPageId:null };

/**
 * Used by the search box plugin to add the necessary javascript events to the search field
 * @param {String} pSearchField: Id of the search field
 * @param {String, Function} pTarget: URL or function which should be called when the user executes the search. The search value is append in case of an URL.
 * @param {String} pSearchHint: Text which is displayed in the search field and which is removed when the user clicks into the field
 */
apex.builder.searchBox = function(pSearchField, pTarget, pSearchHint)
{
  var $SearchField = apex.jQuery("#"+pSearchField);

  function searchValue(pValue) {
    if (pValue === "" || pValue == pSearchHint) { return; }

    if (typeof(pTarget) == "function") {
      pTarget(pValue); }
    else {
      redirect(pTarget+encodeURIComponent(pValue));
    }
  }; // searchValue
  
  $SearchField
    .keypress(function(pEvent)
     {
       // has ENTER been pressed and does the search field contain a value?
       if (pEvent.keyCode == 13) {
         searchValue($SearchField.val());
         pEvent.preventDefault();
       }
     })
    .focus(function()
     {
       // clear the search field
       if (this.value == pSearchHint) { this.value=""; }
     })
    .blur(function()
     {
       // restore the search field
       if (this.value === "") { this.value=pSearchHint; }
     })
    .next() /* the search icon */
    .click(function(pEvent)
     {
       searchValue($SearchField.val());
       pEvent.preventDefault();
     });
}; // apex.builder.searchBox


/**
 * Namespace for the anchor handling functions of attribute pages
 **/
apex.builder.anchor = {};

(function($){

  var cActiveClass = 'anchorCurrent',
      cCookieName = 'ORA_WWV_ATTRIBUTE_PAGE';

  /** 
   * Called when an anchor link is clicked to show the region which is assigned to the anchor   
   **/
  $.activate = function() {
    // "this" is initialized with the clicked anchor entry
    
    // get all anchor links but ignore the hidden ones (eg. plugin settings)
    var $AnchorList = apex.jQuery('#anchorList a:visible');
    // the links store in href which region should be displayed, based on that get all region objects
    var $RegionList = apex.jQuery(apex.jQuery.map($AnchorList.not('#ALL,#DEFAULTALL'),
                                                  function(pValue){
                                                     return pValue.href.substr(pValue.href.indexOf('#'));
                                                  }).join(','));
    // for the current selection remove URL before #
    var lThisAnchor = this.href.substr(this.href.indexOf('#'));
  
    if (lThisAnchor == '#ALL') {
      $RegionList.show();
    }
    else {
      // hide all
      $RegionList.hide();
      // show the selected region
      $RegionList.filter(lThisAnchor).show();
    }
    // show the enclosing div if it's not displayed yet
    apex.jQuery('#BB,#ContentArea,#ContentBody').show();
    // remove current anchor link highlighting
    $AnchorList.removeClass(cActiveClass);
    // highlight pressed anchor link
    apex.jQuery(this).addClass(cActiveClass);
    // remember pressed anchor
    SetCookie(cCookieName, $v('pFlowStepId')+','+lThisAnchor);
    // don't activate the default event handling
    return false;
  }; // activate
  
  /** 
   * Makes a hidden anchor and its TD visible again
   **/
  $.show = function(pRegion) {
    apex.jQuery('#'+pRegion+"_ANCHOR")
      .show() // a tag
      .parent()
      .show(); // td
  }; // show
  
  /** 
   * Makes an anchor and its TD hidden again
   **/
  $.hide = function(pRegion) {
    apex.jQuery('#'+pRegion+"_ANCHOR")
      .hide() // a tag
      .parent()
      .hide(); // td
  }; // show
  
  /** 
   * Called during page init to restore the saved state of the attribute page
   * It also binds the activate function to all anchors  
   **/
  $.init = function(){
    var lCurrentURL = document.URL;
    var lPos = lCurrentURL.lastIndexOf('#');
    var lAnchorName = null;
    var lAnchor = null;
  
    if(lPos >= 0) {
      // extract anchor from URL
      lAnchorName = lCurrentURL.substr(lPos);
    } else {
      // get anchor from cookie
      var lCookie = GetCookie(cCookieName);
      if (lCookie && $v('pFlowStepId') == lCookie.split(',')[0]) {
        lAnchorName = lCookie.split(',')[1];
      }
    }
    // default with ALL
    lAnchorName = (lAnchorName===null)?'#ALL':lAnchorName;
    // lets have a look if the stored anchor has really been rendered (conditions!)
    lAnchor = apex.jQuery(lAnchorName+'_ANCHOR:visible');
    if (lAnchor.length === 0) {
      // fallback to ALL
      lAnchor = apex.jQuery('#ALL_ANCHOR');
    }
    $.activate.call(lAnchor[0]);
  
    // bind our activate function to all links of the anchor widget
    apex.jQuery('#anchorList a').click($.activate);
    
    // because the enhanced anchor is used, we also have to replace the uF function
    // which is used in the region template. The replaced uF is taking care of hidden
    // regions. Bug# 9714082
    uF = function(){
           apex.jQuery('#ALL_ANCHOR').click();
           scroll(0,0);
         };
  }; // init

})(apex.builder.anchor); /* pass in the namespace variable for anchor */


/**
 * Namespace for page 4150
 **/
apex.builder.p4150 = {};

(function($){

  /**
   * Namespace for contextMenu on page 4150
   **/
  $.contextMenu = {};

  function getUrl(pTargetPage, pParameters) {
    pParameters = (pParameters?pParameters:{});
    return "f?p=4000:"+pTargetPage+":$SESSION$::$DEBUG$:RP,"+(pParameters.clear?pParameters.clear:pTargetPage)+":FB_FLOW_ID,FB_FLOW_PAGE_ID"+
           (pParameters.items?","+pParameters.items:"")+":"+
           "$FB_FLOW_ID$,$FB_FLOW_PAGE_ID$"+(pParameters.values?","+pParameters.values:"");
  }; // getUrl

  /* Closure variable which stores a list of all actions the context menu can perform. This list is used by doAction */
  var gActionList = {
    expand_all:  function(pNode, pTree){pTree.open_all(pNode); pTree.open_branch(pNode);},
    collapse_all:function(pNode, pTree){pTree.close_all(pNode); pTree.close_branch(pNode);},
    rename:      function(pNode, pTree){pTree.rename(pNode);},

    /* page */
    page_edit:   getUrl(4301,{items:"F4000_P4301_ID",values:"$id$"}),
    page_create: getUrl( 336,{clear:"336,181,4016,4017,4018,831,399,382,402,259,380"}),
    page_delete: getUrl(  21),
    page_copy:   getUrl( 301,{clear:"618,301,302,305,308,309,310,315,316,317,319",items:"P618_COPY_OPTION",values:"SAME"}),
    page_check:  "f?p=4000:8100:$SESSION$:BRANCH_TO_PAGE_ACCEPT|PERFORM_CHECK:$DEBUG$:RP,8100,8101:P8100_PAGE_ID:$FB_FLOW_PAGE_ID$",
    page_lock:   getUrl( 444,{items:"P444_LOCK_PAGES",values:"$FB_FLOW_PAGE_ID$"}),
    page_unlock: function(){apex.submit('UNLOCK_PAGE');},
    page_cleanup:getUrl( 531),
  
    /* region */
    main_regions_edit_all:     getUrl(4650),
    regions_edit_all:          getUrl(4650),
    region_edit:               getUrl(4651,{clear:"4651,960,420,601,4050,27,196,121,232,695,754,832,287,2000",items:"F4000_P4651_ID",values:"$id$"}),
    region_classic_report_edit:getUrl( 420,{clear:"420,960,4651",items:"P420_REGION_ID,F4000_P4651_ID,P960_ID",values:"$id$,$id$,$id$"}),
    region_dynamic_query_edit: getUrl( 601,{clear:"601,4651",items:"P601_REGION_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_list_edit:          getUrl(4050,{clear:"4050,4651",items:"F4000_P4050_LIST_ID,F4000_P4651_ID",values:"$detail_edit_id$,$id$"}),
    region_jstree_edit:        getUrl( 370,{clear:"370,4651",items:"P370_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_tree_edit:          getUrl(  27,{clear:"27,4651",items:"F4000_P27_ID,F4000_P4651_ID",values:"$detail_edit_id$,$id$"}),
    region_web_service_edit:   getUrl( 196,{clear:"196,4651",items:"F4000_P196_ID,F4000_P4651_ID",values:"$detail_edit_id$,$id$"}),
    region_simple_chart_edit:  getUrl( 121,{clear:"121,4651",items:"F4000_P121_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_svg_chart_edit:     getUrl( 232,{clear:"232,4651",items:"F4000_P232_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_flash_chart_edit:   getUrl( 695,{clear:"695,4651",items:"P695_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_flash_chart5_edit:  getUrl( 754,{clear:"754,4651",items:"P754_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_flash_map_edit:     getUrl( 832,{clear:"832,4651",items:"P832_ID,F4000_P4651_ID",values:"$id$,$id$"}),
    region_breadcrumb_edit:    getUrl( 287,{clear:"287,4651",items:"F4000_P287_MENU_ID,P287_PAGE,F4000_P4651_ID",values:"$detail_edit_id$,$FB_FLOW_PAGE_ID$,$id$"}),
    region_calendar_edit:      getUrl(2000,{clear:"2000,4651",items:"F4000_P4651_ID",values:"$id$"}),
  
    region_classic_report_print_edit:getUrl(960,{clear:"420,960,4651",items:"P420_REGION_ID,F4000_P4651_ID,P960_ID",values:"$id$,$id$,$id$"}),
    region_dynamic_query_print_edit: getUrl(774,{clear:"774,601,4651",items:"P774_ID,P742_REGION_ID,P601_REGION_ID,F4000_P4651_ID",values:"$id$,$id$,$id$,$id$"}),
  
    region_ir_saved_reports_edit:getUrl(742,{clear:"742,601,4651",items:"P742_REGION_ID,P601_REGION_ID,F4000_P4651_ID",values:"$id$,$id$,$id$"}),

    region_drag_and_drop:  getUrl(5150,{items:"F4000_P5150_ID,F4000_P5150_FLOW_ID,P749_REGION,P5150_REGION_NAME",values:"$id$,$FB_FLOW_ID$,$id$,$id$"}),
    region_da_event_create:getUrl( 532,{clear:"532,300,423,653,198,675,662",items:"P423_TRIGGERING_ELEMENT_TYPE,P423_TRIGGERING_REGION_ID,P423_TRIGGERING_CONDITION_TYPE",values:"REGION,$region_id$,ALWAYS"}),

    region_create:getUrl(181,{clear:"181,263,4016,4017,4721,4018,399,382,402,259,380,4470,4475",items:"F4000_P4016_DISPLAY_POINT,P2006_DISPLAY_POINT,P263_DISPLAY_POINT",values:"$display_point$,$display_point$,$display_point$"}),
    region_copy:  getUrl(194,{clear:"194,195",items:"F4000_P194_PLUG_ID_FROM,F4000_P194_COPY_FROM_PAGE_ID",values:"$ID$,$FB_FLOW_PAGE_ID$"}),
    region_delete:getUrl(102,{items:"F4000_P102_REGION_ID",values:"$ID$"}),
  
    /* page item */
    page_items_edit_all:getUrl(4200),
    page_item_edit:     getUrl(4311,{items:"F4000_P4311_ID",values:"$ID$"}),
    page_item_create:   getUrl( 366,{clear:"4717,4718,4719,4720,4721,4062,543,4480",items:"F4000_P4717_REGION",values:"$region_id$"}),
    page_item_copy:     getUrl( 171,{clear:"171,172",items:"F4000_P171_COPY_FROM_ID,F4000_P171_COPY_FROM_PAGE_ID,F4000_P172_COPY_TO_REGION",values:"$ID$,$FB_FLOW_PAGE_ID$,$region_id$"}),
  
    page_item_button_edit:  getUrl( 143,{items:"F4000_P143_ID",values:"$ID$"}),
    page_item_button_create:getUrl(4062,{clear:"379,4731,190,4732,4733,4019,4062",items:"P167_REGION,P379_WHERE,F4000_P4717_REGION,F4000_REGION_SELECTED,P379_REGION",values:"$region_id$,ITEM,$region_id$,$region_id$,$region_id$"}),
    page_item_button_copy:  getUrl( 171,{clear:"171,172",items:"F4000_P171_COPY_FROM_ID,F4000_P171_COPY_FROM_PAGE_ID,F4000_P172_COPY_TO_REGION",values:"$ID$,$FB_FLOW_PAGE_ID$,$region_id$"}),
  
    page_item_validation_create: getUrl(  91,{clear:"252,248,837,91,4737,4738,4739",items:"P248_LEVEL,P252_ITEM",values:"ITEM,$page_item_name$"}),
    page_item_computation_create:getUrl(4015,{clear:"4015,4026,4021,4022",items:"F4000_P4026_IS,F4000_P4015_ITEM,F4000_P4015_POINT",values:"THIS_PAGE,$page_item_name$,BEFORE_HEADER"}),
    page_item_da_event_create:   getUrl( 532,{clear:"532,300,423,653,198,675,662",items:"P423_TRIGGERING_ELEMENT_TYPE,P423_TRIGGERING_ELEM_ITEM",values:"ITEM,$page_item_name$"}),
  
    /* classic report column */
    classic_report_column_edit:getUrl(422,{clear:"4651,960,420,422",items:"P422_COLUMN_ID,P420_REGION_ID,F4000_P4651_ID,P960_ID",values:"$ID$,$region_id$,$region_id$,$region_id$,$region_id$"}),
  
    /* ir report column */
    ir_report_column_edit:getUrl(687,{clear:"687,601,4651",items:"P687_ID,P601_REGION_ID,P601_ID",values:"$ID$,$region_id$,$worksheet_id$"}),
  
    /* tab form column */
    tabform_column_edit:getUrl(422,{clear:"4651,960,420,422",items:"P422_COLUMN_ID,P420_REGION_ID,F4000_P4651_ID,P960_ID",values:"$ID$,$region_id$,$region_id$,$region_id$,$region_id$"}),
  
    tabform_column_validation_create:getUrl( 91,{clear:"252,248,837,91,4737,4738,4739",items:"P248_LEVEL,P837_COLUMN,P837_COLUMN_ALIAS,P837_TABULAR_FORM_REGION_ID",values:"COLUMN,$column_id$,$column_alias$,$region_id$"}),
    tabform_column_da_event_create:  getUrl(532,{clear:"532,300,423,653,198,675,662",items:"P423_TRIGGERING_ELEMENT_TYPE,P423_TRIGGERING_REGION_ID,P423_TRIGGERING_ELEM_COLUMN",values:"COLUMN,$region_id$,$column_alias$"}),
 
    /* chart series */
    svg_chart_serie_edit:     getUrl(324,{items:"F4000_P232_ID,P324_SERIES_ID",values:"$region_id$,$ID$"}),
    svg_chart_serie_create:   getUrl(324,{items:"F4000_P232_ID,P324_ID",values:"$region_id$,$region_id$"}),
    flash_chart_serie_edit:   getUrl(696,{items:"P695_ID,P696_SERIES_ID",values:"$region_id$,$ID$"}),
    flash_chart_serie_create: getUrl(696,{items:"P695_ID,P696_CHART_ID",values:"$region_id$,$chart_id$"}),
    flash_chart5_serie_edit:  getUrl(834,{items:"P754_ID,P834_SERIES_ID",values:"$region_id$,$ID$"}),
    flash_chart5_serie_create:getUrl(834,{items:"P754_ID,P834_CHART_ID,P834_SHOW_ACTION_LINK",values:"$region_id$,$chart_id$,N"}),
    flash_map_serie_edit:     getUrl(187,{items:"P832_ID,P187_SERIES_ID",values:"$region_id$,$ID$"}),
    flash_map_serie_create:   getUrl(187,{items:"P832_ID,P187_CHART_ID",values:"$region_id$,$chart_id$"}),
  
    /* region button */
    region_buttons_edit_all:getUrl(4203),
    region_button_edit:     getUrl(4314,{items:"F4000_P4314_ID",values:"$ID$"}),
    region_button_create:   getUrl(4731,{clear:"379,4731,190,4732,4733,4019,4062",items:"P167_REGION,P379_WHERE,F4000_P4717_REGION,F4000_REGION_SELECTED,P379_REGION",values:"$region_id$,REGION,$region_id$,$region_id$,$region_id$"}),
    region_button_copy:     getUrl( 231,{clear:"231,191",items:"F4000_P231_COPY_FROM_ID,F4000_P231_COPY_FROM_PAGE_ID,F4000_P191_COPY_TO_REGION",values:"$ID$,$FB_FLOW_PAGE_ID$,$region_id$"}),
 
    /* sub region */
    sub_region_create:getUrl(181,{clear:"181,263,4016,4017,4721,4018,399,382,402,259,380,4470,4475",items:"P4016_PARENT_PLUG_ID,P2006_PARENT_PLUG_ID",values:"$region_id$,$region_id$"}),
  
    /* da event */
    da_events_edit_all:getUrl(773),
    da_event_edit:     getUrl(793,{items:"F4000_P793_ID",values:"$ID$"}),
    da_event_create:   getUrl(532,{clear:"532,300,423,653,198,675,662"}),
    da_event_copy:     getUrl(796,{clear:"128,796,797",items:"F4000_P796_COPY_FROM_ID,F4000_P796_COPY_FROM_PAGE_ID",values:"$ID$,$FB_FLOW_PAGE_ID$"}),
  
    /* da action */
    da_action_edit:  getUrl(591,{items:"F4000_P591_ID,F4000_P793_ID",values:"$ID$,$event_id$"}),
    da_action_create:getUrl(591,{items:"P591_EVENT_RESULT,P591_EVENT_ID,F4000_P793_ID",values:"$event_result$,$event_id$,$event_id$"}),
    da_actions_true_create: getUrl(591,{items:"P591_EVENT_RESULT,P591_EVENT_ID,F4000_P793_ID",values:"TRUE,$event_id$,$event_id$"}),
    da_actions_false_create:getUrl(591,{items:"P591_EVENT_RESULT,P591_EVENT_ID,F4000_P793_ID",values:"FALSE,$event_id$,$event_id$"}),
  
    /* branch */
    branches_edit_all:getUrl(4202),
    branch_edit:      getUrl(4313,{items:"F4000_P4313_ID",values:"$ID$"}),
    branch_go_to_target_page:"f?p=4000:4150:$SESSION$::$DEBUG$:RP,4150:FB_FLOW_ID,FB_FLOW_PAGE_ID:$FB_FLOW_ID$,$target_page_id$",
    branch_create:    getUrl(4728,{clear:"4728,4729,4730",items:"F4000_P4728_BRANCH_POINT",values:"$branch_point$"}),
    branch_copy:      getUrl( 587,{clear:"587,685",items:"P587_ID",values:"$ID$"}),
  
    /* process */
    processes_edit_all:getUrl(4201),
    process_edit:    getUrl(4312,{items:"F4000_P4312_ID",values:"$ID$"}),
    process_create:  getUrl( 230,{clear:"230,4734,4735,4736,4020",items:"F4000_P4734_POINT",values:"$process_point$"}),
    process_copy:    getUrl( 590,{clear:"590,645",items:"P590_ID",values:"$ID$"}),
  
    /* computation */
    computations_edit_all:getUrl(4204),
    computation_edit:     getUrl(4315,{items:"F4000_P4315_ID",values:"$ID$"}),
    computation_create:   getUrl(4026,{clear:"4015,4026,4021,4022",items:"F4000_P4015_POINT",values:"$computation_point$"}),
    computation_copy:     getUrl( 592,{clear:"592,597",items:"P592_ID",values:"$ID$"}),
  
    /* validation */
    validations_edit_all:getUrl(4205),
    validation_edit:     getUrl(4316,{items:"F4000_P4316_ID",values:"$ID$"}),
    validation_create:   getUrl( 248,{clear:"252,248,837,91,4737,4738,4739"}),
    validation_copy:     getUrl( 583,{clear:"583,596",items:"P583_ID",values:"$ID$"}),
  
    /* tabs */
    tabs_create:      getUrl(9000,{clear:"92,94,95,96,4722,4723,4724,4725,4726,4727",items:"F4000_P9000_PAGE",values:$v("pFlowPageId")}),
    tabs_edit_all:    getUrl(9000,{clear:"92,94,95,96,4722,4723,4724,4725,4726,4727",items:"F4000_P9000_PAGE",values:$v("pFlowPageId")}),
    toplevel_tab_edit:getUrl(4318,{items:"F4000_P4318_ID",values:"$ID$"}),
    std_tab_edit:     getUrl(4305,{items:"F4000_P4305_ID",values:"$ID$"}),
  
    /* lov */
    lovs_edit_all:getUrl(4110),
    lov_edit:     getUrl(4111, {items:"F4000_P4111_ID",values:"$ID$"}),
    lov_create:   getUrl( 137, {clear:"4004,4007,4014,137,138,139"}),
    lov_copy:     getUrl( 603, {items:"P603_ID",values:"$ID$"}),
  
    /* breadcrumb */
    breadcrumbs_edit_all:getUrl(287),
    breadcrumb_edit:     getUrl(287,{items:"P287_PAGE,F4000_P287_MENU_ID",values:"$FB_FLOW_PAGE_ID$,$ID$"}),
    breadcrumb_create:   getUrl(465),
  
    /* list */
    lists_edit_all:getUrl( 405),
    list_create:   getUrl( 427),
    list_copy:     getUrl( 602,{clear:"602,658,659,663",items:"P602_ID",values:"$ID$"}),
    list_edit:     getUrl(4050,{items:"F4000_P4050_LIST_ID",values:"$ID$"}),
  
    /* security */
    securities_edit_all:getUrl(4070),
    security_edit:  getUrl(4008,{items:"F4000_P4008_ID",values:"$ID$"}),
    security_create:getUrl( 184,{clear:"184,185,186,188"}),
    security_copy:  getUrl( 493,{items:"P493_ID",values:"$ID$"}),
  
    /* template */
    templates_edit_all:getUrl(4003),
    template_create:   getUrl( 162),
    template_copy:     getUrl(4003),
    template_page_edit:         getUrl(4307,{items:"F4000_P4307_ID",values:"$ID$"}),
    template_region_edit:       getUrl(4653,{items:"F4000_P4653_ID",values:"$ID$"}),
    template_label_edit:        getUrl(  85,{items:"F4000_P85_ID",values:"$ID$"}),
    template_popup_lov_edit:    getUrl( 251,{items:"F4000_P251_ID",values:"$ID$"}),
    template_button_edit:       getUrl( 204,{items:"F4000_P204_ID",values:"$ID$"}),
    template_calendar_type_edit:getUrl( 697,{items:"P697_ID",values:"$ID$"}),
    template_breadcrumb_edit:   getUrl( 289,{items:"F4000_P289_ID",values:"$ID$"}),
    template_report_edit:       getUrl( 258,{items:"F4000_P258_ID",values:"$ID$"}),
    template_list_edit:         getUrl(4655,{items:"F4000_P4655_ID",values:"$ID$"})
    };
  
  /**
   * xxx
   */
  $.contextMenu.doAction = function(pNode, pTree, pAction){
    var lAction,
        lType = pTree.get_type(pNode),
        lSingularType = lType.replace(/^regions$/,         "region")
                             .replace(/^main_regions$/,    "region")
                             .replace(/^region_da_event$/, "da_event")
                             .replace(/^region_da_events$/,"region_da_event")
                             .replace(/^sub_regions$/,     "sub_region")
                             .replace(/^page_item_validation$/,  "validation")
                             .replace(/^page_item_validations$/, "page_item_validation")
                             .replace(/^page_item_computation$/, "computation")
                             .replace(/^page_item_computations$/,"page_item_computation")
                             .replace(/^page_item_da_event$/,    "da_event")
                             .replace(/^page_item_da_events$/,   "page_item_da_event")
                             .replace(/^tabform_column_validation$/, "validation")
                             .replace(/^tabform_column_validations$/,"tabform_column_validation")
                             .replace(/^tabform_column_da_event$/,   "da_event")
                             .replace(/^tabform_column_da_events$/,  "tabform_column_da_event")
                             .replace(/^tabform_validation$/,    "validation")
                             .replace(/^tabform_validations$/,   "tabform_validation")
                             .replace(/^flash_chart5_series$/,   "flash_chart5_serie")
                             .replace(/^flash_chart_series$/,    "flash_chart_serie")
                             .replace(/^flash_map_series$/,      "flash_map_serie")
                             .replace(/^svg_chart_series$/,      "svg_chart_serie")
                             .replace(/^da_events$/,       "da_event")
                             .replace(/^da_actions_true$/, "da_action")
                             .replace(/^da_actions_false$/,"da_action")
                             .replace(/^region_buttons$/,  "region_button")
                             .replace(/^branches$/,        "branch")
                             .replace(/^processes$/,       "process")
                             .replace(/^computations$/,    "computation")
                             .replace(/^validations$/,     "validation")
                             .replace(/^lovs$/,            "lov")
                             .replace(/^lists$/,           "list")
                             .replace(/^breadcrumbs$/,     "breadcrumb")
                             .replace(/^templates/,        "template")
                             .replace(/^securities$/,      "security")
                             .replace(/^toplevel_tabset$/, "tabs")
                             .replace(/^toplevel_tab$/,    "tabs")
                             .replace(/^std_tabset$/,      "tabs")
                             .replace(/^std_tab$/,         "tabs");
    // if it's a template or a parent of a template, convert it to template
    lSingularType = /templates{0,1}_\w*/.test(lSingularType)?"template":lSingularType;
    // if it's a report, get the action named based on the region type
    if (pAction==="report_edit") { pAction = 'region_'+pNode.attr("region_type")+"_edit"; }
    // if it's a chart, get the action named based on the region type
    if (pAction==="region_chart_edit") { pAction = 'region_'+pNode.attr("region_type")+"_edit"; }
    // if it's a "print edit", get the action named based on the region type
    if (pAction==="region_print_edit") { pAction = 'region_'+pNode.attr("region_type")+"_print_edit"; }
    // if it's a "chart_serie_create", get the action named based on the region type
    if (pAction==="chart_serie_create") { pAction = pNode.attr("region_type")+"_serie_create"; }
  
    // try to find the action by prefixing the node type
    if (gActionList[lType+"_"+pAction]) {
      lAction = gActionList[lType+"_"+pAction];
    // try to find the action based on the singular type of the node
    } else if (gActionList[lSingularType+"_"+pAction]) {
      lAction = gActionList[lSingularType+"_"+pAction];
    // as last action try a direct match with the passed action
    } else if (gActionList[pAction]) {
      lAction = gActionList[pAction];
    } else {
      alert("No action found for "+pAction);
    }
  
    // just execute the action if it's a javascript function, otherwise replace the URL placeholders
    if (typeof lAction=="function") {
      lAction.call(null, pNode, pTree);
    } else {
      var lId = pNode.attr("id");
      // perform basic replace operations
      lAction = lAction.replace(/\$SESSION\$/g,    $v('pInstance'))
                       .replace(/\$DEBUG\$/g,      $v('pDebug'))
                       .replace(/\$FB_FLOW_ID\$/g, apex.builder.gApplicationId)
                       .replace(/\$FB_FLOW_PAGE_ID\$/g, apex.builder.gPageId)
                       .replace(/\$ID$\$/g,             (isNaN(lId)?"":lId));
      // is there still a placeholder in the format $name$ ?
      var lPlaceholderList = /\$\w*\$/.exec(lAction);
      while (lPlaceholderList) {
        // first try to read the value from the current node
        var lPlaceholder = lPlaceholderList[0].replace(/\$/g, "");
        var lPlaceholderValue = pNode.attr(lPlaceholder);
        // if the attribute hasn't been found, have a look at the parent node
        if (lPlaceholderValue===undefined) {
          lPlaceholderValue = pTree.parent(pNode).attr(lPlaceholder);
          // still not found? have a look at the grand parent node
          if (lPlaceholderValue===undefined) {
            lPlaceholderValue = pTree.parent(pTree.parent(pNode)).attr(lPlaceholder);
          }
        }
        lAction = lAction.replace(lPlaceholderList[0], (lPlaceholderValue===undefined?"":lPlaceholderValue));
        lPlaceholderList = /\$\w*\$/.exec(lAction);
      }
      // store the current selected node
      SetCookie('ORA_WWV_F4000_P4150_TREE', pTree.container.attr("id")+":"+(pTree.parent(pNode)!=-1?pTree.parent(pNode).attr("id"):pTree.container.attr("id"))+":"+lId);
      // go to the URL
      document.location.href=lAction;
    }
  }; // doAction

  /**
   * Namespace for Tree
   **/
  $.tree = {};

  // Used to determin if drag & drop is allowed
  $.tree.gReadOnly = false;
  
  // Used by drag_start to dynamically create nodes. These values are set during
  // the page initialization if language isn't EN
  $.tree.gPageItemsTitle = "Items";
  $.tree.gRegionButtonsTitle = "Region Buttons";

  $.tree.isRenameable = function(pNode, pTree) {

    var lNodeType = pTree.get_type(pNode),
        lNodeTitle = pTree.get_text(pNode),
        lRegExp = /^\[.*\]$/; // no updated allowed if string contains [whatever]
    switch(lNodeType)
    {
      case "page_item":
        return (lRegExp.test(lNodeTitle))?false:true;
        break;
      case "page_item_button":
        return (lRegExp.test(lNodeTitle))?false:true;
        break;
      case "classic_report_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          return (lRegExp.test(lNodeTitle))?false:true;
        } else {
          return false;
        }
        break;
      case "tabform_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          return (lRegExp.test(lNodeTitle))?false:true;
        } else {
          return false;
        }
        break;
      case "ir_report_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          return (lRegExp.test(lNodeTitle))?false:true;
        } else {
          return false;
        }
        break;
      default:
        return true;
    }
  }; // isRenameable
  
  /* Constants which store the default settings for the different trees */
  $.tree.cRenderingTreeTypes = {
    "default":{
      clickable:true,
      renameable:false,
      deletable:false,
      creatable:true,
      draggable:false,
      copyable:false,
      editable:false,
      max_children:-1,
      max_depth:-1,
      valid_children:"none",
      icon:{
        image:false,
        position:false}
      },
    "page":{
      creatable:true,
      renameable:true,
      deletable:true,
      copyable:true,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_page.png"}
      },
    "on_new_instance":{
      creatable:false
      },
    "before_header":{
      creatable:false
      },
    "after_header":{
      creatable:false
      },
    "before_regions":{
      creatable:false
      },
    "rendering_regions":{
      creatable:false
      },
    "after_regions":{
      creatable:false
      },
    "before_footer":{
      creatable:false
      },
    "after_footer":{
      creatable:false
      },
    "computations":{
      valid_children:["computation"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_computation.png"}
      },
    "computation":{
      draggable:true,
      copyable:true,
      editable:true
      },
    "processes":{
      valid_children:["process"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_process.png"}
      },
    "process":{
      draggable:true,
      renameable:true,
      copyable:true,
      editable:true
      },
    "branches":{
      valid_children:["branch"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_branch.png"}
      },
    "branch":{
      draggable:true,
      copyable:true,
      editable:true
      },
    "main_regions":{
      valid_children:["regions"]
      },
    "regions":{
      valid_children:["region"]
      },
    "sub_regions":{
      valid_children:["region"]
      },
    "region":{
      draggable:true,
      renameable:true,
/*      deletable:true, not supported yet */
      copyable:true,
      editable:true,
      valid_children:["page_items","region_buttons"]
      },
    "region_on_page_0":{
      creatable:false
      },
    "region_da_events":{
      valid_children:["region_da_event"]
      },
    "region_da_event":{
      renameable:true,
      copyable:true,
      editable:true,
      valid_children:["da_actions_true", "da_actions_false"]
      },
    "page_items":{
      creatable:false,
      valid_children:["page_item","page_item_button"]
      },
    "page_item":{
      draggable:true,
      renameable:$.tree.isRenameable,
      copyable:true,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_page_item.png"}
      },
    "page_item_button":{
      draggable:true,
      renameable:$.tree.isRenameable,
      copyable:true,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_button.png"}
      },
    "page_item_validations":{
      valid_children:["page_item_validation"]
      },
    "page_item_validation":{
      renameable:true,
      copyable:true,
      editable:true
      },
    "page_item_computations":{
      valid_children:["page_item_computation"]
      },
    "page_item_computation":{
      copyable:true,
      editable:true
      },
    "page_item_da_events":{
      valid_children:["page_item_da_event"]
      },
    "page_item_da_event":{
      renameable:true,
      copyable:true,
      editable:true,
      valid_children:["da_actions_true", "da_actions_false"]
      },
    "report_columns":{
      creatable:false,
      valid_children:["classic_report_column","ir_report_column","tabform_column"]
      },
    "classic_report_column":{
      renameable:$.tree.isRenameable,
      draggable:true,
      creatable:false,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_page_item.png"}
      },
    "ir_report_column":{
      renameable:$.tree.isRenameable,
      draggable:true,
      creatable:false,
      editable:true
      },
    "tabform_column":{
      renameable:$.tree.isRenameable,
      draggable:true,
      creatable:false,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_tabform_column.png"}
      },
    "tabform_column_validations":{
      valid_children:["tabform_column_validation"]
      },
    "tabform_column_validation":{
      renameable:true,
      copyable:true,
      editable:true
      },
    "tabform_column_da_events":{
      valid_children:["tabform_column_da_event"]
      },
    "tabform_column_da_event":{
      renameable:true,
      copyable:true,
      editable:true,
      valid_children:["da_actions_true", "da_actions_false"]
      },
    "tabform_validations":{
      valid_children:["tabform_validation"]
      },
    "tabform_validation":{
      renameable:true,
      copyable:true,
      editable:true
      },
    "flash_chart5_series":{
      creatable:false,
      valid_children:["flash_chart5_serie"]
      },
    "flash_chart5_serie":{
      draggable:true,
      creatable:false,
      renameable:true,
      editable:true
      },
    "flash_chart_series":{
      creatable:false,
      valid_children:["flash_chart_serie"]
      },
    "flash_chart_serie":{
      draggable:true,
      creatable:false,
      renameable:true,
      editable:true
      },
    "flash_map_series":{
      valid_children:["flash_map_serie"]
      },
    "flash_map_serie":{
      draggable:true,
      renameable:true,
      editable:true
      },
    "svg_chart_series":{
      valid_children:["svg_chart_serie"]
      },
    "svg_chart_serie":{
      editable:true
      },
    "region_buttons":{
      valid_children:["region_button"]
      },
    "region_button":{
      draggable:true,
      renameable:true,
      copyable:true,
      editable:true,
      icon:{
        image:apex_img_dir+"apex/builder/tree_button.png"}
      },
    "da_events":{
      valid_children:["da_event"]
      },
    "da_event":{
      draggable:true,
      renameable:true,
      copyable:true,
      editable:true,
      valid_children:["da_actions_true", "da_actions_false"]
      },
    "da_actions_true":{
      valid_children:["da_action"]
      },
    "da_actions_false":{
      valid_children:["da_action"]
      },
    "da_action":{
      draggable:true,
      copyable:false,
      editable:true
      }
    };
  
  $.tree.cProcessingTreeTypes = {
    "default":{
      clickable:true,
      renameable:false,
      deletable:false,
      creatable:true,
      draggable:false,
      copyable:false,
      editable:false,
      max_children:-1,
      max_depth:-1,
      valid_children:"none",
      icon:{
        image:false,
        position:false}
      },
    "after_submit":{
      creatable:false
      },
    "validating":{
      creatable:false
      },
    "processing":{
      creatable:false
      },
    "after_processing":{
      creatable:false
      },
    "validations":{
      valid_children:["validation"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_validation.png"}
      },
    "validation":{
      draggable:true,
      renameable:true,
      copyable:true,
      editable:true
      },
    "computations":{
      valid_children:["computation"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_computation.png"}
      },
    "computation":{
      draggable:true,
      copyable:true,
      editable:true
      },
    "processes":{
      valid_children:["process"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_process.png"}
      },
    "process":{
      draggable:true,
      renameable:true,
      copyable:true,
      editable:true
      },
    "branches":{
      valid_children:["branch"],
      icon:{
        image:apex_img_dir+"apex/builder/tree_branch.png"}
      },
    "branch":{
      draggable:true,
      copyable:true,
      editable:true
      }
    };
  
  $.tree.cSharedTreeTypes = {
    "default":{
      clickable:true,
      renameable:false,
      deletable:false,
      creatable:true,
      draggable:false,
      copyable:true,
      editable:true,
      max_children:-1,
      max_depth:-1,
      valid_children:"none",
      icon:{
        image:false,
        position:false}
      },
    "tabs":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_tab.png"}
      },
    "toplevel_tabset":{
      copyable:false,
      editable:false
      },
    "toplevel_tab":{
      copyable:false
      },
    "std_tabset":{
      valid_children:["std_tab"],
      copyable:false,
      editable:false
      },
    "std_tab":{
      draggable:true,
      copyable:false,
      renameable:true
      },
    "lovs":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_lov.png"}
      },
    "breadcrumbs":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_breadcrumb.png"}
      },
    "breadcrumb":{
      copyable:false
      },
    "lists":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_list.png"}
      },
    "templates":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_template.png"}
      },
    "securities":{
      copyable:false,
      editable:false,
      icon:{
        image:apex_img_dir+"apex/builder/tree_security.png"}      
      },
    "no_edit_security":{
      creatable:false,
      copyable:false,
      editable:false
      }
    };

  /**
   * xxx
   **/
  $.tree.init = function(pTreeId, pTypes, pStaticData, pContextMenu, pShowLabels) {
    // store our show label flag for the tree (used in rename)
    apex.jQuery('#'+pTreeId).data('showLabels', pShowLabels);
    // Initialize the tree
    var lTree = apex.jQuery("#"+pTreeId).tree({
      data:{ 
        type:"json",
        async:true,
        opts:{
          "static":pStaticData,
          isTreeLoaded:false,
          method:"POST",
          url:"wwv_flow.show"
        }
      },
      ui:{theme_name:"classic"},
      types:pTypes,
      rules:{
        valid_children:"none"
      },
      callback:{
        beforedata:$.tree.beforedata,
        ondblclk:$.tree.ondblclk,
        onrgtclk:$.tree.hideTooltip,
        check:$.tree.check,
        check_move:$.tree.check_move,
        onmove:$.tree.move,
        onrename:$.tree.rename
      },
      plugins:{
        contextmenu:pContextMenu
        }
      });
    //
    apex.jQuery.tree.drag_start = $.tree.drag_start;
    // get the last selected node from cookie. But only select it if it's our current tree
    var lCookie = GetCookie('ORA_WWV_F4000_P4150_TREE');
    if (lCookie) {
      var lLastSelection = GetCookie('ORA_WWV_F4000_P4150_TREE').split(":");
      if (lLastSelection[0]===pTreeId) {
        apex.jQuery.tree.reference(lTree).select_branch(apex.jQuery("#"+lLastSelection[2], "#"+lLastSelection[1]));
      }
    }
    // open all nodes and there parents which contain a branch, computation, ...
    apex.jQuery.tree.reference(lTree).open_branch(apex.jQuery('li[rel=branch],li[rel=computation],li[rel=process],li[rel=validation],li[rel=region],li[rel=page_item],li[rel=page_item_button],li[rel=region_button],li[rel=da_event]').parents('li.closed'), true);
    // bind tooltips for tree nodes
    apex.jQuery('a[tooltip]', '#'+pTreeId).bind("mouseover", $.tree.showTooltip);
  }; // init
    
  /**
   * xxx
   **/
  $.tree.executeOperation = function(pAction, pRollback, lCallback, pParameter1, pParameter2, pParameter3, pParameter4) {
    var lAjax = new htmldb_Get(null, $v('pFlowId'), 'APPLICATION_PROCESS=execute_tree_operation', $v('pFlowStepId'));
    lAjax.addParam('p_widget_action', pAction);
    lAjax.addParam('x01', pParameter1);
    lAjax.addParam('x02', pParameter2);
    lAjax.addParam('x03', pParameter3);
    lAjax.addParam('x04', pParameter4);
    var lReturn = lAjax.get();
    lAjax = null;
    // if noting is returned, everything is ok. Otherwise it contains an error
    if (lReturn==="") {
      if (typeof lCallback === "function") {
        lCallback.call();
      }
    } else {
      apex.jQuery.tree.rollback(pRollback);
      alert(lReturn);
    }
  }; // executeOperation
  
  /**
   * xxx
   **/
  $.tree.beforedata = function(pNode, pTree){
    if(pNode===false){
      if (pTree.settings.data.opts.isTreeLoaded){
        pTree.settings.data.opts["static"] = false;
        return { "p_flow_id":$v("pFlowId"),
                 "p_flow_step_id":$v("pFlowStepId"),
                 "p_instance":$v("pInstance"),
                 "p_request":"APPLICATION_PROCESS=get_data",
                 "p_widget_action":pTree.container.attr("id"),
                 "x01":apex.builder.gApplicationId,
                 "x02":apex.builder.gPageId,
                 "x03":apex.jQuery("#"+pTree.container.attr("id")).data("showLabels")
               };
      } else {
        pTree.settings.data.opts.isTreeLoaded = true;
      }
    }
  }; // beforedata
  
  /**
   * xxx
   **/
  $.tree.check = function(pRule, pNode, pValue, pTree) {
    if (pRule==="draggable" && $.tree.gReadOnly) {
      return false;
    } else {
      return pValue;
    }
  }; // check

  /**
   * xxx
   **/
  $.tree.check_move = function(pNode, pRefNode, pType, pTree) {
    var lParentNode    = pTree.parent(pNode),
        lParentRefNode = pTree.parent(pRefNode);
    // only some node types can be dropped everywhere, but all other node types
    // have to stay within the same parent (like report columns, ...)
    if (apex.jQuery.inArray(pTree.get_type(pNode), ["computation", "process", "branch",
                                                    "page_item", "page_item_button", "region_button",
                                                    "da_action"]) != -1) {
      return true;
    // a region which is below a display point can be moved to every display point
    } else if (lParentNode.attr("rel") === "regions") {
      return true;
    // sub regions have to stay within the same parent region
    } else if (lParentNode.attr("rel") === "sub_regions" && lParentNode.attr("region_id") === lParentRefNode.attr("region_id")) {
      return true;
    } else {
      return (lParentNode.attr("id") === lParentRefNode.attr("id"));
    }
  }; // check_move

  /**
   * Adds a new node to region nodes where the specified node types doesn't exist yet
   */
  $.tree.drag_start = function(pEvent, pDragger) {
    // only for page items and buttons we have to add new nodes on the fly
    var lNodeType = apex.jQuery(pEvent.target).closest('li').attr("rel");
    if (apex.jQuery.inArray(lNodeType, ["page_item", "page_item_button", "region_button"]) == -1) {return;}

    // get tree and all regions
    var lTree=apex.jQuery.tree.reference("#RenderingTree"),
        lRegions=apex.jQuery('li[rel=region]', '#RenderingTree'); /* all regions */
    
    /**
     * Adds a new node to region nodes where the specified node types doesn't exist yet
     */
    function addNodes(pNodeType, pTitle, pPrepend) {

      /* add the grouping node pNodeType for all regions which don't have one */
      var lCreatable=lTree.settings.types[pNodeType].creatable;
          lFinal=lRegions.not('*:has(li[rel='+pNodeType+'])');
      
      lTree.settings.types[pNodeType].creatable=true;
      lFinal.each(function(){
        /* the native tree function is to slow on bigger pages, we are adding the necessary HTML code ourself $$$
        lTree.create({
          attributes:{id:this.id+"_page_items",rel:"page_items"},
          data:{attributes:{"class":"grey_node"},title:"Items"}
          },
          this); 
        */
        var lList = apex.jQuery(this).children('ul'),
            lHtml1 = '<li rel="'+pNodeType+'" id="'+this.id+'_'+pNodeType+'" class="leaf',
            lHtml2 = '"><a class="grey_node" style="" href=""><ins> </ins>'+pTitle+'</a></li>';
        
        /* if childs already exist, we prepend or append our new entry */
        if (lList.size() > 0) {
          if (pPrepend) {
            apex.jQuery(lList).prepend(lHtml1+lHtml2);
          } else {
            apex.jQuery(lList).children('ul li.last').removeClass('last');
            apex.jQuery(lList).append(lHtml1+' last'+lHtml2);
          }
        } else {
          /* no childs exist, we have to create a new list and remove the leaf class from the parent */
          apex.jQuery(this).append(
            '<ul>'+lHtml1+' last'+lHtml2+'</ul>')
            .filter('.leaf')
            .removeClass('leaf')
            .addClass('closed');        
        }
      });
      // restore the old setting
      lTree.settings.types[pNodeType].creatable=lCreatable;
    }; // addNodes
    
    addNodes("page_items", $.tree.gPageItemsTitle, true);
    addNodes("region_buttons", $.tree.gRegionButtonsTitle, false);
  }; // drag_start

  /**
   * xxx
   **/
  $.tree.ondblclk = function(pNode, pTree) {
    var lNode = pTree.get_node(pNode);
    pTree.select_branch.call(pTree, pNode);
    // only if we have a numeric id we will also have an edit action
    if (!isNaN(lNode.attr("id"))){
      $.contextMenu.doAction(lNode, pTree, "edit");
    }
  }; // ondblclk

  /**
   * xxx
   **/
  $.tree.showTooltip = function(pEvent) {
    // only if the context menu is not visible, display the tooltip
    if (apex.jQuery('#jstree-contextmenu:hidden').length > 0) {
      toolTip_enable(pEvent,this,apex.jQuery(this).attr("tooltip"));
    }
  }; // showTooltip

  /**
   * xxx
   **/
  $.tree.hideTooltip = function() {
    toolTip_disable();
  }; // hideTooltip
  
  /**
   * xxx
   */
  $.tree.rename = function(pNode, pTree, pRollback) {

    function refreshTrees() {
      pTree.refresh();
      refreshProcessingTree();
    };

    function refreshRenderingTree() {
      apex.jQuery.tree.reference('#RenderingTree').refresh();
    };

    function refreshProcessingTree() {
      apex.jQuery.tree.reference('#ProcessingTree').refresh();
    };
                    
    var lAction   = null,
        lCallback = null,
        lNodeType = pTree.get_type(pNode);
  
    switch(lNodeType)
    {
      case "page":
        lAction = "UPDATE_PAGE_NAME";
        break;
      case "region":
        lAction = "UPDATE_PLUG_NAME";
        break;
      case "page_item":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          lAction = "UPDATE_PAGE_ITEM_LABEL";
        } else {
          lAction = "UPDATE_PAGE_ITEM_NAME";
          lCallback = refreshTrees;
        }
        break;
      case "page_item_button":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          lAction = "UPDATE_PAGE_ITEM_LABEL";
        } else {
          lAction = "UPDATE_PAGE_ITEM_NAME";
          lCallback = refreshTrees;
        }
        break;
      case "flash_chart5_serie":
        lAction = "UPDATE_FLASH_CHART5_SERIES_NAME";
        break;
      case "flash_chart_serie":
        lAction = "UPDATE_FLASH_CHART_SERIES_NAME";
        break;
      case "flash_map_serie":
        lAction = "UPDATE_FLASH_CHART5_SERIES_NAME";
        break;
      case "region_button":
        lAction = (apex.jQuery("#RenderingTree").data("showLabels")?"UPDATE_BUTTON_LABEL":"UPDATE_BUTTON_NAME");
        break;
      case "classic_report_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          lAction = "UPDATE_CLASSIC_REPORT_COLUMN_HEADING";
        } else {
          return 0;
        }
        break;
      case "tabform_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          lAction = "UPDATE_CLASSIC_REPORT_COLUMN_HEADING";
        } else {
          return 0;
        }
        break;
      case "ir_report_column":
        if (apex.jQuery("#RenderingTree").data("showLabels")) {
          lAction = "UPDATE_IR_REPORT_COLUMN_HEADING";
        } else {
          return 0;
        }
        break;
      case "std_tab":
        lAction = "UPDATE_TAB_TEXT";
        break;
      case "validation":
        lAction = "UPDATE_VALIDATION_NAME";
        break;
      case "page_item_validation":
        lAction = "UPDATE_VALIDATION_NAME";
        lCallback = refreshProcessingTree;
        break;
      case "tabform_column_validation":
        lAction = "UPDATE_VALIDATION_NAME";
        lCallback = refreshProcessingTree;
        break;
      case "tabform_validation":
        lAction = "UPDATE_VALIDATION_NAME";
        lCallback = refreshProcessingTree;
        break;
      case "process":
        lAction = "UPDATE_PROCESS_NAME";
        break;
      case "da_event":
        lAction = "UPDATE_DA_EVENT_NAME";
        lCallback = refreshRenderingTree;
        break;
      case "region_da_event":
        lAction = "UPDATE_DA_EVENT_NAME";
        lCallback = refreshRenderingTree;
        break;
      case "page_item_da_event":
        lAction = "UPDATE_DA_EVENT_NAME";
        lCallback = refreshRenderingTree;
        break;
      case "tabform_column_da_event":
        lAction = "UPDATE_DA_EVENT_NAME";
        lCallback = refreshRenderingTree;
        break;
      default:
        alert('undefined action for '+lNodeType);
        return 0;
    }
    $.tree.executeOperation(lAction, pRollback, lCallback, pNode.id, pTree.get_text(pNode), apex.builder.gApplicationId);
  }; // rename
  
  /**
   * xxx
   */
  $.tree.move = function(pNode, pRefNode, pType, pTree, pRollback) {
  
    // returns the specified attribute value from the parent or grand parent node
    function getParentValue(pAttribute) {
      var lValue = pTree.parent(pNode).attr(pAttribute);
      if (lValue===undefined) {
        // if the attribute hasn't been found, have a look at the parent node
        lValue = pTree.parent(pTree.parent(pNode)).attr(pAttribute);
        if (lValue===undefined) {
          lValue=null;
        }
      }
      return lValue;
    } // getParentValue
  
    var lPrevNode = pTree.prev(pNode, true),
        lPrevId   = (!lPrevNode?null:lPrevNode.attr("id")),
        lNodeType = pTree.get_type(pNode);
  
    switch(lNodeType)
    {
      case "region":
        $.tree.executeOperation("MOVE_PLUG", pRollback, null, pNode.id, lPrevId, getParentValue("display_point"), getParentValue("region_id"));
        break;
      case "page_item":
        $.tree.executeOperation("MOVE_PAGE_ITEM", pRollback, null, pNode.id, lPrevId, getParentValue("region_id"));
        break;
      case "page_item_button":
        $.tree.executeOperation("MOVE_PAGE_ITEM", pRollback, null, pNode.id, lPrevId, getParentValue("region_id"));
        break;
      case "classic_report_column":
        $.tree.executeOperation("MOVE_CLASSIC_REPORT_COLUMN", pRollback, null, pNode.id, lPrevId);
        break;
      case "tabform_column":
        $.tree.executeOperation("MOVE_CLASSIC_REPORT_COLUMN", pRollback, null, pNode.id, lPrevId);
        break;
      case "ir_report_column":
        $.tree.executeOperation("MOVE_IR_REPORT_COLUMN", pRollback, null, pNode.id, lPrevId);
        break;
      case "flash_chart5_serie":
        $.tree.executeOperation("MOVE_FLASH_CHART5_SERIES", pRollback, null, pNode.id, lPrevId);
        break;
      case "flash_chart_serie":
        $.tree.executeOperation("MOVE_FLASH_CHART_SERIES", pRollback, null, pNode.id, lPrevId);
        break;
      case "flash_map_serie":
        $.tree.executeOperation("MOVE_FLASH_CHART5_SERIES", pRollback, null, pNode.id, lPrevId);
        break;
      case "region_button":
        $.tree.executeOperation("MOVE_BUTTON", pRollback, null, pNode.id, lPrevId, getParentValue("region_id"));
        break;
      case "da_event":
        $.tree.executeOperation("MOVE_DA_EVENT", pRollback, null, pNode.id, lPrevId);
        break;
      case "da_action":
        $.tree.executeOperation("MOVE_DA_ACTION", pRollback, null, pNode.id, lPrevId, getParentValue("event_id"), getParentValue("event_result"));
        break;
      case "std_tab":
        $.tree.executeOperation("MOVE_TAB", pRollback, null, pNode.id, lPrevId);
        break;
      case "validation":
        $.tree.executeOperation("MOVE_VALIDATION", pRollback, null, pNode.id, lPrevId);
        break;
      case "computation":
        $.tree.executeOperation("MOVE_COMPUTATION", pRollback, null, pNode.id, lPrevId, getParentValue("computation_point"));
        break;
      case "process":
        $.tree.executeOperation("MOVE_PROCESS", pRollback, null, pNode.id, lPrevId, getParentValue("process_point"));
        break;
      case "branch":
        $.tree.executeOperation("MOVE_BRANCH", pRollback, null, pNode.id, lPrevId, getParentValue("branch_point"));
        break;
      default:
        alert('undefined action for '+lNodeType);
        return 0;
    }
  }; // move


})(apex.builder.p4150); /* pass in the namespace variable for p4150 */




try{

var gReturn = 'F4000_P4017_SOURCE_CHART';

/** @ignore  */
function ChartSqlReturn(pThis){
  $v_PopupReturn('P4000_CHART_SQL',gReturn);
  window.close();
}

var gTestArray;
var gTestId;
var gReSequence = false;
var gReSequenceBy = 10;
var gId = false;
var gReorder;

/** @ignore  */
function rAlign(pThis){
    var lText;
    var lThis = pThis.split("-");
    switch(lThis[0]){
      case 'CENTER' : lText='text-align:center;'; break;
      case 'RIGHT' : lText='text-align:right;'; break;
      case 'LEFT' : lText='text-align:left;'; break;
      default : lText = ' '; break;
    }
    if(lThis[1]){
    switch(lThis[1]){
      case 'BOTTOM' : lText+='vertical-align:bottom;'; break;
      case 'CENTER' : lText+='vertical-align:middle;'; break;
      case 'TOP' : lText+='vertical-align:top;'; break;
      default : lText += ' '; break;
    }
    }
    return lText;
}

/** @ignore  the following runs item layout preview */
function rpreview(gId){
  var lnode = $x('pview');
  var node = $x(gId);
  node = (node)?node:$x(g_rpreview_global);
  if(!lnode){return false;}
  var lH = getElementsByClass('orderby',node,'INPUT');
  lnode.innerHTML = '';
  var lTable = $x_AddTag(lnode,'TABLE');
  lTable.className='itemlayout';
  lLastRow = false;
  lLastCell = false;

  for (var i=1;i<node.rows.length;i++){
    var lSelect = $x_FormItems(node.rows[i],'SELECT');
    var lText = $x_FormItems(node.rows[i],'TEXT');
    var lValue = node.rows[i].cells[0].innerHTML;
    var lLabel = lText[0].value;
    var lType = lText[2].value;
    if(lType){}
    var lNewLine = lSelect[0].value;
    var lNewField = lSelect[1].value;
    var lAlign = lSelect[2].value;
  if(lType == 'STOP_AND_START_HTML_TABLE'){
      $x_AddTag(lnode,'BR');
      lTable = $x_AddTag(lnode,'TABLE');
      lTable.className='itemlayout';
  }else{
    if(lNewLine == 'YES' || i==1){lLastRow = $x_AddTag(lTable,'TR');}
    if(lType == 'BUTTON'){lLabel = '<br />';}
    lLastCell = $tr_AddTD(lLastRow,lLabel);
    lLastCell.setAttribute('style',rAlign(lAlign));
    if(lType == 'HIDDEN'){lLastCell.setAttribute('style','font-weight:normal;background-color:#FFF;');}
    if(lNewField == 'YES' && lAlign != 'ABOVE' && lAlign != 'BELOW'){
      lLastCell = $tr_AddTD(lLastRow,lValue);
      lLastCell.className='itemlayout';
      if(lType == 'TEXTAREA'){lLastCell.setAttribute('style','height:75px;');}
      if(lType == 'HIDDEN'){lLastCell.setAttribute('style','font-weight:normal;background-color:#FFF;');}
        lLastCell.colSpan = lText[1].value;
      }else{
          if(lAlign == 'ABOVE'){lLastCell.innerHTML += '<br />'+ lValue;}
        else if(lAlign == 'BELOW'){lLastCell.innerHTML = lValue + '<br />' + lLastCell.innerHTML;}
        else{lLastCell.innerHTML += lValue;}
        if(lType == 'TEXTAREA'){lLastCell.setAttribute('style','height:75px;');}
        lLastCell.colSpan = lText[1].value;
    }
  }
  }
  if(document.all){lnode.innerHTML = lnode.innerHTML;}
}

/** @ignore  */
function a4000pg749_init(){rpreview();}

/** @ignore  */
function callConditionsPopup(s1, sessionId) {
  var pURL = 'f?p=4000:271:' + sessionId + ':::271:PASSBACK:' + s1.name;
  html_PopUp(pURL,null,null,null);
}

/** @ignore   
 *
 * used for custom popup of page template preview 
 * */
function callPageTemplatePopup(s1, sessionId, flowId, pageId) {
  var pURL = 'f?p=4000:74:' + sessionId + ':::74:F4000_P74_PASSBACK,F4000_P74_FLOW_ID,F4000_P74_PAGE_ID:' + s1.name + ',' + flowId + ',' + pageId;
  html_PopUp(pURL,null,null,null);
}

var gThis = false;
var lEl = false;
var lH = false;
var gFARButtonListCurrent = 'htmldbButtonListCurrent';
var gFARButtonList = 'htmldbButtonList2';
var gFARCookieName = 'ORA_WWV_ATTRIBUTE_PAGE';

/** 
 * @ignore   
 **/
function uR(){
  if(gThis){
    gThis.className='htmldbButtonList';
    if(gThis==$x('ALL')){gThis=$x('ALL');}
  }
}


function filterAttributeRegions(pThis,pThat){
  var lHolder = ($x('BB'))?$x('BB'):$x('ContentArea');
  lHolder = (!lHolder)?$x('ContentBody'):lHolder;
  try{
    var lThis = pThat.substr(1);
    if(lThis == 'ALL' || lThis == 'DEFAULTALL'){
        pThis.className = gFARButtonListCurrent;
        if(lThis != 'DEFAULTALL'){SetCookie(gFARCookieName,$x('pFlowStepId').value+','+pThat);}
        uF();
        $x_Show(['BB','ContentArea','ContentBody']);
    }else{
       if($x('ALL')){$x('ALL').className = gFARButtonList;}
       uR();
        pThis.className = gFARButtonListCurrent;
       gThis = pThis;
       lH = $x_ByClass('rc-title',lHolder,'DIV');
       for (var i=0;i<lH.length;i++){
            $x_Hide($x_UpTill(lH[i],'','rounded-corner-region'));
           if(lH[i].getElementsByTagName('A')){
               var lTr = lH[i].getElementsByTagName('A')[0];
              if(lTr && lTr.name && lTr.name == lThis){lEl = lTr;}
            }
        }    
        if(lEl){$x_Show($x_UpTill(lEl,'','rounded-corner-region'));}
        SetCookie (gFARCookieName, $x('pFlowStepId').value+','+pThat);
        $x_Show(['BB','ContentArea','ContentBody']);
    }
  }catch(e){
    uF();
    $x_Show(['BB','ContentArea','ContentBody']);
  }
};

/** 
 * @ignore   
 **/
function propTest(){
  var currentURL = document.URL;
  var lInd = currentURL.lastIndexOf('#');
  var lId;
  if(lInd != -1){
    lId = currentURL.substring(lInd);
  }else if(
    GetCookie (gFARCookieName)){
    var lPage = GetCookie (gFARCookieName).split(',')[0];
    if ($x('pFlowStepId').value == lPage && lInd == -1){lId = GetCookie(gFARCookieName).split(',')[1];}
  }else{
    lId = false;
  }
  if(lId){
    var lLinks = $x('ql').getElementsByTagName('A');
    for (var i=0,len=lLinks.length;i<len;i++){
      if(lLinks[i].getAttribute('href').lastIndexOf(lId) != -1){qF(lLinks[i],lId);}
    }
  }else{qF($x('ALL'),'#DEFAULTALL');}
}

/** 
 * @ignore   
 **/
function unfilterAttributeRegions(){
  var lThis = ($x('BB'))?$x('BB'):$x('ContentArea');
  lThis = (!lThis)?$x('ContentBody'):lThis;
  try{
    var lH = $x_ByClass('rc-title',lThis,'DIV');
    for (var i=0,len=lH.length;i<len;i++){$x_Show($x_UpTill(lH[i],'','rounded-corner-region'));}
    uR();
    scroll(0,0);
  }catch(e){
    scroll(0,0);
  }
}

/** 
 * @ignore   
 **/
uF = unfilterAttributeRegions;

/** 
 * @ignore   
 **/
qF = filterAttributeRegions;

}catch(e){}

function show_download_format_page(){
  var lItem = ($x('P687_FORMAT_MASK'))?'P687_FORMAT_MASK':'P422_COLUMN_FORMAT';
  html_PopUp('wwv_flow_file_mgr.show_download_format_page?p_format='+$v(lItem));

}

function show_download_format_page_set(){
  var l_Array = [$v('a01'),$v('a02'),$v('a03'),$v('a04'),$v('a05'),$v('a06'),$v('a07'),$v('a08'),$v('a09'),$v('a10'),$v('a11')];
  var lItem = (opener.$x('P687_FORMAT_MASK'))?'P687_FORMAT_MASK':'P422_COLUMN_FORMAT';
  var l_Value = $u_ArrayToString(l_Array,':');
  opener.$s(lItem,l_Value);
  window.close();
  opener.$x(lItem).focus();
}

/**
 * @class dynamicAttributes
   * Handles dynamic attributes and shows/hides them depending on the plugin meta data
 * */
apex.builder.dynamicAttributes = {

  // stores the selected plugin. This information is required by the help method
  gCurrentSelection: {
    pluginType: null,
    pluginName: null
  },

  // Used to hide all dynamic attribute page items (Pxxxx_ATTRIBUTE_xx)
  //   pItemPrefix: Prefix used for the items. eg P4311_
  hide: function(pItemPrefix){
  
    // hide all attribute fields
    apex.jQuery('#'+pItemPrefix+'ATTRIBUTE_01,#'+pItemPrefix+'ATTRIBUTE_02,#'+pItemPrefix+'ATTRIBUTE_03,#'+pItemPrefix+'ATTRIBUTE_04,#'+pItemPrefix+'ATTRIBUTE_05,'+
                '#'+pItemPrefix+'ATTRIBUTE_06,#'+pItemPrefix+'ATTRIBUTE_07,#'+pItemPrefix+'ATTRIBUTE_08,#'+pItemPrefix+'ATTRIBUTE_09,#'+pItemPrefix+'ATTRIBUTE_10'
               ).closest('TR').hide();
    
    // hide format mask attribute fields
    apex.jQuery('#'+pItemPrefix+'FORMAT_MASK_DATE,#'+pItemPrefix+'FORMAT_MASK_NUMBER,#'+pItemPrefix+'IS_REQUIRED').each(function(){
      $x_HideItemRow(this);
      });
  }, // hide
  
  // Used to set the dynamic attribute page items (Pxxxx_ATTRIBUTE_xx) based on the display as type.
  //   pPluginType: Type of plugin (eg. ITEM TYPE)
  //   pPluginName: Selected plugin (eg. value stored in DISPLAY_AS item)
  //   pItemPrefix: Prefix used for the items. eg P4311_
  //   pPluginList: Array indexed by the component type with the following format
  //                { standardAttributes: "xx",
  //                  sqlMinColumnCount: x,
  //                  sqlMaxColumnCount: x,
  //                  attributeList:[{label: "xx", fieldDef: "html code", defaultValue: "default", isRequired: true/false, dependingOnAttribute: "00", dependingOnCondType:"not_null", dependingOnExpr:"xx"}]
  //                }
  //   pKeepValues: false will initialize the items with the default value
  //   pRegionId:   Optional parameter to show/hide the region containing the dynamic attributes
  //                If specified it will also try to show/hide the anchor link with the id [pRegionId]_ANCHOR
  show: function(pPluginType, pPluginName, pItemPrefix, pPluginList, pKeepValues, pRegionId){
  
    apex.builder.dynamicAttributes.hide(pItemPrefix);
    
    // get an jQuery array with all the dynamic attribute fields
    var cDynamicAttributeSelector = '#'+pItemPrefix+'ATTRIBUTE_01,#'+pItemPrefix+'ATTRIBUTE_02,#'+pItemPrefix+'ATTRIBUTE_03,#'+pItemPrefix+'ATTRIBUTE_04,#'+pItemPrefix+'ATTRIBUTE_05,'+
                                    '#'+pItemPrefix+'ATTRIBUTE_06,#'+pItemPrefix+'ATTRIBUTE_07,#'+pItemPrefix+'ATTRIBUTE_08,#'+pItemPrefix+'ATTRIBUTE_09,#'+pItemPrefix+'ATTRIBUTE_10';
                                     
    var lAttributeList = apex.jQuery(cDynamicAttributeSelector),
        lAttributeTrList = lAttributeList.closest('TR'),
        lNextElement     = lAttributeTrList.filter(':last').next();
        lTable           = lAttributeTrList.filter(':last').closest('TABLE');
        lInputNameList   = [];

    // does the selected plugin name have dynamic attributes?
    if (pPluginList[pPluginName] && pPluginList[pPluginName].attributeList.length>0) {
      // loop over all attribute definitions in our array and bring them into the display order
      apex.jQuery.each(pPluginList[pPluginName].attributeSortList, function(pIndex, pValue){
        var lTr = apex.jQuery('#'+pItemPrefix + 'ATTRIBUTE_' + ((pValue < 10)?'0':'')+(pValue)).closest('TR');
        if (lNextElement.length===0) {
          lTable.append(lTr);
        } else {
          lNextElement.before(lTr);
        }
      });
      // After reordering the dynamic attributes we have to bring the "name" atttribute again into syn with the display order
      // We have to re-read the attributes, because of the re-arrange the DOM reference is not valid anymore
      lAttributeList = apex.jQuery(cDynamicAttributeSelector);
      // get the internal name attributes used by the dynamic attributes
      lAttributeList.each(function(pIndex){
        lInputNameList[pIndex] = apex.jQuery(this).attr('name');
        });
      lInputNameList = lInputNameList.sort();
      // set the sorted "name" attributes in the order the dynamic attributes appear in the DOM tree
      lAttributeList.each(function(pIndex){
        apex.jQuery(this).attr("name", lInputNameList[pIndex]);
      });
    }    

    // clear the existing values if necessary
    if (!pKeepValues) {
      // we have to set back the item to a hidden field, because if it's a selectlist
      // setting to null will have no effect
      lAttributeList.each(function(){
        apex.jQuery(this).replaceWith('<input type="hidden" id="'+this.id+'" name="'+this.name+'" value="">');
        });
    }
    
    // does the selected plugin name have dynamic attributes or displays the format mask or required field?
    if (pPluginList[pPluginName] &&
       ((pPluginList[pPluginName].attributeList.length>0) ||
        (/FORMAT_MASK/.test(pPluginList[pPluginName].standardAttributes)) ||
        (/SESSION_STATE/.test(pPluginList[pPluginName].standardAttributes))
       )) {
      // remember the current settings needed by the help method
      apex.builder.dynamicAttributes.gCurrentSelection = {
        pluginType: pPluginType,
        pluginName: pPluginName
      };

      // loop over all attribute definitions in our array
      apex.jQuery.each(pPluginList[pPluginName].attributeList, function(pIndex, pAttributeDef){
        // exit if we have a gap in our attribute definition
        if (!pAttributeDef) { return; }

        var lId = pItemPrefix + 'ATTRIBUTE_' + ((pIndex < 9)?'0':'')+(pIndex+1); // array index starts with 0 but our items start with 1
        
        // get the jQuery object, the name and value of the dynamic attribute
        var lField = apex.jQuery('#' + lId),
            lName  = lField.attr('name'),
            lValue = lField.val();
        
        // replace label
        var lLabelObj = apex.jQuery('label[for=' + lId + ']'),
            lImageObj = lLabelObj.find('img');

        lLabelObj.children('a')
          .html(pAttributeDef.label)
          .prepend(lImageObj);
        // show required indicator
        if (pAttributeDef.isRequired) {
          lImageObj.show();
        } else {
          lImageObj.hide();
        }
        
        // replace the dynamic attribute field with the new html definition
        lField.val(null).replaceWith(pAttributeDef.fieldDef.replace('#ID#', lId).replace('#NAME#', lName));
        
        // restore original value if necessary
        if (pKeepValues) {
          // if no value is set and the attribute has a default value, use the default
          if (!lValue && pAttributeDef.defaultValue) {
            lValue = pAttributeDef.defaultValue;
          }
          apex.jQuery('#' + lId).val(lValue);
        }
        else {
          apex.jQuery('#' + lId).val(pAttributeDef.defaultValue);
        }
      });

      // show/hide attributes depending on defined condition. We have to do this AFTER
      // all the dynamic attribute fields have been initialized, because the conditions
      // are checking these fields
      apex.jQuery.each(pPluginList[pPluginName].attributeList, function(pIndex, pAttributeDef){
        // exit if we have a gap in our attribute definition
        if (!pAttributeDef) { return; }

        apex.builder.dynamicAttributes.showHideAttribute(pItemPrefix, pIndex, pPluginList[pPluginName].attributeList);

        // if this field has a condition, create a change listener on depending on field
        // so that we also show/hide the field if the value gets changed
        if (pAttributeDef.dependingOnAttrSeq && pAttributeDef.dependingOnCondType) {
          apex.jQuery('#'+pItemPrefix+'ATTRIBUTE_'+pAttributeDef.dependingOnAttrSeq).change(function(){
            apex.builder.dynamicAttributes.showHideAttribute(pItemPrefix, pIndex, pPluginList[pPluginName].attributeList);
          });
        }
      });

      // show the format mask
      if (/FORMAT_MASK_DATE/.test(pPluginList[pPluginName].standardAttributes)) {
        $x_ShowItemRow(pItemPrefix+'FORMAT_MASK_DATE');
        $x_HideItemRow(pItemPrefix+'FORMAT_MASK');
      } else if (/FORMAT_MASK_NUMBER/.test(pPluginList[pPluginName].standardAttributes)) {
        $x_ShowItemRow(pItemPrefix+'FORMAT_MASK_NUMBER');
        $x_HideItemRow(pItemPrefix+'FORMAT_MASK');
      } else {
        $x_ShowItemRow(pItemPrefix+'FORMAT_MASK');
      }

      // stores session state
      if (/SESSION_STATE/.test(pPluginList[pPluginName].standardAttributes)) {
        $x_ShowItemRow(pItemPrefix+'IS_REQUIRED');
      }

      // show the dynamic attribute region only if we really have attributes
      if (pRegionId) {
        if ((pPluginList[pPluginName].attributeList.length > 0) ||
            (/FORMAT_MASK/.test(pPluginList[pPluginName].standardAttributes)) ||
            (/SESSION_STATE/.test(pPluginList[pPluginName].standardAttributes))
           ) {
          $x_Show(pRegionId);
          apex.builder.anchor.show(pRegionId);
        } else {
          $x_Hide(pRegionId);
          apex.builder.anchor.hide(pRegionId);
        }
      }
    }
    else {
      // remember the current settings needed by the help method
      apex.builder.dynamicAttributes.gCurrentSelection = {
        pluginType: null,
        pluginName: null
      };
      if (pPluginType==="ITEM TYPE") {
        // always show the format mask
        $x_ShowItemRow(pItemPrefix+'FORMAT_MASK');
      }
      if (pRegionId) {
        $x_Hide(pRegionId);
        apex.builder.anchor.hide(pRegionId);
      }
    }
    
    // show/hide the standard attributes
    var lStandardAttributeList=[],
        lSqlMinColumnCount=null,
        lSqlMaxColumnCount=null,
        lAttributeList=[],
        lRegionList=[];

    // only handle the standard attributes when it's not page 4410 (plug-in configuration page)
    if ($v('pFlowStepId') != 4410) {
      switch(pPluginType) {
        case 'ITEM TYPE':
          // hide standard attribute fields
          apex.jQuery.each(['LIST_OF_VALUES', 'BEGIN_ON_NEW_LINE', 'LABEL_ALIGNMENT', 'CATTRIBUTES', 'CSIZE', 'CHEIGHT', 'TAG_ATTRIBUTES2', 'LOV_DISPLAY_NULL', 'LOV_NULL_TEXT', 'LOV_CASCADE_PARENT_ITEMS', 'LOV_ITEMS_TO_SUBMIT', 'LOV_OPTIMIZE_REFRESH', 'STATIC_LOV_POPUP', 'DYNAMIC_LOV_POPUP', 'ENCRYPT_SESSION_STATE_YN', 'ESCAPE_ON_HTTP_OUTPUT'], function(){
            $x_HideItemRow(pItemPrefix+this);
          });
          // hide standard attribute regions
          apex.jQuery('#LABEL,#ELEMENT,#VALIDATION,#DEFAULT,#SOURCE,#LOV,#QP,#READONLY,#READONLY,#HELP,#HELP')
            .hide()
            .each(function(){
              apex.builder.anchor.hide(this.id);
            });
  
          // provide default values if it's not a plugin
          if (!pPluginList[pPluginName]) {
            lStandardAttributeList=["VISIBLE","SOURCE","SESSION_STATE","ELEMENT","WIDTH","HEIGHT", "ELEMENT_OPTION", "QUICKPICK","READONLY","ENCRYPT","LOV","LOV_DISPLAY_NULL"];
            lSqlMinColumnCount=2;
            lSqlMaxColumnCount=2;
          } else {
            lStandardAttributeList=pPluginList[pPluginName].standardAttributes.split(':');
            lSqlMinColumnCount=pPluginList[pPluginName].sqlMinColumnCount;
            lSqlMaxColumnCount=pPluginList[pPluginName].sqlMaxColumnCount;
          }
          // widget is visible
          if (apex.jQuery.inArray("VISIBLE", lStandardAttributeList)!=-1) {
            lAttributeList.push('BEGIN_ON_NEW_LINE');
            // has width
            if (apex.jQuery.inArray("WIDTH", lStandardAttributeList)!=-1) {
              lAttributeList.push('CSIZE');
            }
            // has height
            if (apex.jQuery.inArray("HEIGHT", lStandardAttributeList)!=-1) {
              lAttributeList.push('CHEIGHT');
            }
            lRegionList.push('#LABEL');
            lRegionList.push('#HELP');
            // always show this attributes, because they are hided by Stop and Start Table
            lAttributeList.push('LABEL_ALIGNMENT');
            lAttributeList.push('CATTRIBUTES');
            // Form Element Options
            if (apex.jQuery.inArray("ELEMENT_OPTION", lStandardAttributeList)!=-1) {
              lAttributeList.push('TAG_ATTRIBUTES2');
            }
            // has LOV
            if (apex.jQuery.inArray("LOV", lStandardAttributeList)!=-1) {
              lAttributeList.push('LIST_OF_VALUES');
              lRegionList.push('#LOV');
              // show LOV static popup
              if (lSqlMinColumnCount<=2 && lSqlMaxColumnCount >= 2) {
                lAttributeList.push('STATIC_LOV_POPUP');
                lAttributeList.push('DYNAMIC_LOV_POPUP');
              }
              // show LOV examples
              apex.builder.dynamicAttributes.setSqlExamples(pPluginType, pPluginName);
              // lov columns
              if (apex.jQuery.inArray("LOV_DISPLAY_NULL", lStandardAttributeList)!=-1) {
                lAttributeList.push('LOV_DISPLAY_NULL');
                if ($v(pItemPrefix+'LOV_DISPLAY_NULL')==='YES') {
                  lAttributeList.push('LOV_NULL_TEXT');
                }
              }
              if (apex.jQuery.inArray("CASCADING_LOV", lStandardAttributeList)!=-1) {
                lAttributeList.push('LOV_CASCADE_PARENT_ITEMS');
                if (!apex.item(pItemPrefix+'LOV_CASCADE_PARENT_ITEMS').isEmpty()) {
                  lAttributeList.push('LOV_ITEMS_TO_SUBMIT');
                  lAttributeList.push('LOV_OPTIMIZE_REFRESH');
                }
              }
            }
            // has Quick Pick
            if (apex.jQuery.inArray("QUICKPICK", lStandardAttributeList)!=-1) {
              lRegionList.push('#QP');
            }
            // has Read Only
            if (apex.jQuery.inArray("READONLY", lStandardAttributeList)!=-1) {
              lRegionList.push('#READONLY');
            }
            // has Escape Output
            if (apex.jQuery.inArray("ESCAPE_OUTPUT", lStandardAttributeList)!=-1) {
              lAttributeList.push('ESCAPE_ON_HTTP_OUTPUT');
            }
          }
          // has element
          if (apex.jQuery.inArray("ELEMENT", lStandardAttributeList)!=-1) {
            lRegionList.push('#ELEMENT');
          }
          // has source section
          if (apex.jQuery.inArray("SOURCE", lStandardAttributeList)!=-1) {
            lRegionList.push('#SOURCE');
            lRegionList.push('#DEFAULT');
          }
          // has encrypt
          if (apex.jQuery.inArray("ENCRYPT", lStandardAttributeList)!=-1) {
            lAttributeList.push('ENCRYPT_SESSION_STATE_YN');
          }
          // special handling for Stop and Start HTML Table which is a non-visible item type but which should still show the label! Very strange widget!
          if (pPluginName==="NATIVE_STOP_AND_START_HTML_TABLE") {
            lRegionList.push('#LABEL');
            lRegionList.push('#HELP');
          } else if (pPluginName==="NATIVE_HIDDEN") {
            lAttributeList.push('BEGIN_ON_NEW_LINE');
          }

          // show all standard attributes which should be visible
          apex.jQuery.each(lAttributeList, function(){
            $x_ShowItemRow(pItemPrefix+this);
          });
          // show all standard regions which should be visible
          apex.jQuery(lRegionList.join())
            .show()
            .each(function(){
              apex.builder.anchor.show(this.id);
            });
          break;
        case 'REGION TYPE':
        
          // hide standard attribute regions
          apex.jQuery('#SOURCE,#SQL_EXAMPLES')
            .hide()
            .each(function(){
              apex.builder.anchor.hide(this.id);
            });
  
          // provide default values if it's not a plugin
          if (!pPluginList[pPluginName]) {
            lStandardAttributeList=["SOURCE_PLAIN"];
          } else {
            lStandardAttributeList=pPluginList[pPluginName].standardAttributes.split(':');
            lHasSqlExamples=pPluginList[pPluginName].hasSqlExamples;
          }
          // region has source
          if (apex.jQuery.inArray("SOURCE_PLAIN", lStandardAttributeList)!=-1 || apex.jQuery.inArray("SOURCE_SQL", lStandardAttributeList)!=-1) {
            lRegionList.push('#SOURCE');
            
            // show SQL examples
            if (apex.jQuery.inArray("SOURCE_SQL", lStandardAttributeList)!=-1) {
              apex.builder.dynamicAttributes.setSqlExamples(pPluginType, pPluginName);
              lRegionList.push('#SQL_EXAMPLES');
            }
          }
          // show all standard regions which should be visible
          if (lRegionList.length > 0) {
            apex.jQuery(lRegionList.join())
              .show()
              .each(function(){
                apex.builder.anchor.show(this.id);
              });
          }

          break;
        case 'DYNAMIC ACTION':
        
          // hide standard attributes
          $x_HideItemRow(pItemPrefix+'STOP_EXECUTION_ON_ERROR');
          // if the region exists
          $x_Hide('AFFECTED_ELEMENTS');
          apex.builder.anchor.hide('AFFECTED_ELEMENTS');
  
          // exit if no plug-in has been selected yet
          if (!pPluginName) { return; }

          lStandardAttributeList=pPluginList[pPluginName].standardAttributes.split(':');

          // has stop execute on error
          if (apex.jQuery.inArray("STOP_EXECUTION_ON_ERROR", lStandardAttributeList)!=-1) {
            $x_ShowItemRow(pItemPrefix+'STOP_EXECUTION_ON_ERROR');
          }

          // check page on load flag
          if (!pKeepValues) {
            if (apex.jQuery.inArray("ONLOAD", lStandardAttributeList)!=-1) {
              $s(pItemPrefix+'EXECUTE_ON_PAGE_INIT', 'Y');
            } else {
              $s(pItemPrefix+'EXECUTE_ON_PAGE_INIT', null);
            }
          }

          // handling of the affected element types
          var lCurrentElementsType = $v(pItemPrefix+'AFFECTED_ELEMENTS_TYPE');
          // remove all affected element type options except of null entry
          apex.jQuery('#'+pItemPrefix+'AFFECTED_ELEMENTS_TYPE option[value!=""]').remove();
          // enable all those which are available
          var lAffectedTypeList = [];
          if (apex.jQuery.inArray("ITEM",               lStandardAttributeList)!=-1) { lAffectedTypeList.push("ITEM"); }
          if (apex.jQuery.inArray("REGION",             lStandardAttributeList)!=-1) { lAffectedTypeList.push("REGION"); }
          if (apex.jQuery.inArray("DOM_OBJECT",         lStandardAttributeList)!=-1) { lAffectedTypeList.push("DOM_OBJECT"); }
          if (apex.jQuery.inArray("JQUERY_SELECTOR",    lStandardAttributeList)!=-1) { lAffectedTypeList.push("JQUERY_SELECTOR"); }
          if (apex.jQuery.inArray("TRIGGERING_ELEMENT", lStandardAttributeList)!=-1) { lAffectedTypeList.push("TRIGGERING_ELEMENT"); }
          if (apex.jQuery.inArray("EVENT_SOURCE",       lStandardAttributeList)!=-1) { lAffectedTypeList.push("EVENT_SOURCE"); }

          // if type plugin doesn't have any type, hide region or field
          if (lAffectedTypeList.length == 0) {
            // if the region exists
            $x_Hide('AFFECTED_ELEMENTS');
            apex.builder.anchor.hide('AFFECTED_ELEMENTS');
            $x_HideItemRow(pItemPrefix+'AFFECTED_ELEMENTS_TYPE');
            $s(pItemPrefix+'AFFECTED_ELEMENTS_TYPE', ''); // will also trigger hiding of all fields
          } else {
            // affected elements are available for this plugin
            $x_Show('AFFECTED_ELEMENTS');
            apex.builder.anchor.show('AFFECTED_ELEMENTS');
            $x_ShowItemRow(pItemPrefix+'AFFECTED_ELEMENTS_TYPE');
            // clone all affected element types which have been defined for this plugin from our ELEMENTS_TYPE_CLONE select list
            // created in the region footer of "Affect Elements"
            apex.jQuery.each(lAffectedTypeList, function(){
              apex.jQuery('#ELEMENTS_TYPE_CLONE option[value="'+this+'"]').clone().appendTo('#'+pItemPrefix+'AFFECTED_ELEMENTS_TYPE');
            });
            // Restore original value, if it's not in the list it will not be set
            $s(pItemPrefix+'AFFECTED_ELEMENTS_TYPE', lCurrentElementsType);
          }
          break;
        default:
      }
    } // not on page 4410
  }, // show

  // Used to show a specific dynamic attribute based on if the condition is true.
  // If it's false the field is hided.
  //   pItemPrefix:    Prefix for all dynamic attribute fields
  //   pIndex:         Index withing the pAttributeList
  //   pAttributeList: Array of attribute definitions for the selected plug-in
  showHideAttribute: function(pItemPrefix, pIndex, pAttributeList){
    var lPrefix = pItemPrefix + 'ATTRIBUTE_',
        lId     = lPrefix + ((pIndex < 9)?'0':'')+(pIndex+1); // array index starts with 0 but our items start with 1

    function isDisplayed(pIndex){
      var lDisplay = true;
      // check condition
      if (pAttributeList[pIndex].dependingOnAttrSeq && pAttributeList[pIndex].dependingOnCondType) {
        
        // recursive call to check the whole dependency chain
        // Note: we have to use -1 because the array starts with 0
        lDisplay = isDisplayed(parseInt(pAttributeList[pIndex].dependingOnAttrSeq, 10)-1);
        // only if all parents are displayed, we check the current attribute as well
        if (lDisplay) {
          var lValue = $v(lPrefix+pAttributeList[pIndex].dependingOnAttrSeq);
    
          switch (pAttributeList[pIndex].dependingOnCondType) {
            case 'EQUALS':
              lDisplay = (lValue === pAttributeList[pIndex].dependingOnExpr);
              break;
            case 'NOT_EQUALS':
              lDisplay = (lValue !== pAttributeList[pIndex].dependingOnExpr);
              break;
            case 'IN_LIST':
              lDisplay = (apex.jQuery.inArray(lValue, pAttributeList[pIndex].dependingOnExpr.split(','))!==-1);
              break;
            case 'NOT_IN_LIST':
              lDisplay = (apex.jQuery.inArray(lValue, pAttributeList[pIndex].dependingOnExpr.split(','))===-1);
              break;
            case 'NULL':
              lDisplay = !(lValue);
              break;
            case 'NOT_NULL':
              lDisplay = (lValue);
              break;
          }
        }
      }
      return lDisplay;
    }; // isDisplayed

    // show/hide the dynamic attribute
    if (isDisplayed(pIndex)){
      $x_ShowItemRow(lId);
    } else {
      $x_HideItemRow(lId);      
    }
    // simulate a change so that depending attributes are getting refreshed as well
    apex.jQuery('#'+lId).change();
  }, // showHideAttribute
 
  // Used to display the help text for a dynamic attribute
  help: function(pApplicationId, pItemName){
    var lApplicationId = (pApplicationId ? pApplicationId : $v('pFlowId')),
        lAttributeScope = ($v('pFlowStepId') == 4410 ? "APPLICATION" : "COMPONENT");
    
    apex.jQuery.getJSON(
      'wwv_flow_item_help.show_plugin_attribute_help' +
      '?p_application_id='+lApplicationId +
      '&p_builder_page_id='+$v('pFlowStepId') +
      '&p_session_id='+$v('pInstance') +
      '&p_plugin_type='+apex.builder.dynamicAttributes.gCurrentSelection.pluginType +
      '&p_plugin_name='+apex.builder.dynamicAttributes.gCurrentSelection.pluginName +
      '&p_attribute_scope='+lAttributeScope +
      '&p_attribute_sequence='+pItemName.substr(pItemName.length-2), // get the last two digits
      function(pData){
        // add a new div with the retrieved page
        var lDialog = apex.jQuery('<div>' + pData.helpText + '</div>');
        // open created div as a dialog
        lDialog.dialog({
          title: pData.title,
          bgiframe: true,
          width: 500,
          height: 350,
          show: 'drop',
          hide: 'drop'
        });
      });
    return;
  }, // help
  
  setSqlExamples: function(pPluginType, pPluginName) {
    // initialize the AJAX call parameters
    var lData = { p_arg_names:  [],
                  p_arg_values: [],
                  p_request: "APPLICATION_PROCESS=get_plugin_sql_examples",
                  p_flow_id: $v('pFlowId'),
                  p_flow_step_id: $v('pFlowStepId'),
                  p_instance: $v('pInstance'),
                  x01: pPluginType,
                  x02: pPluginName
                };

    // perform the AJAX call
    apex.jQuery.ajax({
      // try to leverage ajaxQueue plugin to abort previous requests
      mode: "abort",
      // limit abortion to this input
      port: "setSqlExamples",
      type: "post",
      url: "wwv_flow.show",
      data: lData,
      success: function(pData){ apex.jQuery('#bodySQL_EXAMPLES').html(pData); }
      });    
  }
  
}; // dynamicAttributes


/**
 * @class plugin
   * Namespace for APEX Builder specific plugins
 * */
apex.builder.plugin = {

  /**
   * @class setConditionItems
     * Shows/hides the EXPRESSION1/2 fields depending on the selected condition
   * */
  setConditionItems: function(pConditionType, pExpression1, pExpression2, pExpressionCheck, pReadOnlyAttribute, pLovNullValue){
    var lValue = $v(pConditionType),
        lNoExpressionsList = [ /* no additional input required */
          'NEVER', 'ALWAYS', 'BROWSER_IS_NSCP', 'BROWSER_IS_MSIE', 'BROWSER_IS_MSIE_OR_NSCP', 'BROWSER_IS_OTHER', 'WHEN_ANY_ITEM_IN_CURRENT_PAGE_HAS_CHANGED', 
          'WHEN_THIS_PAGE_SUBMITTED', 'WHEN_THIS_PAGE_NOT_SUBMITTED', 'PAGE_IS_IN_PRINTER_FRIENDLY_MODE', 'PAGE_IS_NOT_IN_PRINTER_FRIENDLY_MODE', 
          'USER_IS_NOT_PUBLIC_USER', 'USER_IS_PUBLIC_USER', 'DISPLAYING_INLINE_VALIDATION_ERRORS', 'NOT_DISPLAYING_INLINE_VALIDATION_ERRORS', 
          'MAX_ROWS_LT_ROWS_FETCHED', 'MIN_ROW_GT_THAN_ONE' ],
        lOneExpressionList = [ /* Expression 1 field required */
          'EXISTS', 'NOT_EXISTS', 'SQL_EXPRESSION', 'PLSQL_EXPRESSION', 'FUNCTION_BODY',
          'ITEM_IS_NULL', 'ITEM_IS_NOT_NULL', 'ITEM_IS_ZERO', 'ITEM_IS_NOT_ZERO', 'ITEM_IS_NULL_OR_ZERO', 
          'ITEM_NOT_NULL_OR_ZERO', 'ITEM_CONTAINS_NO_SPACES', 'ITEM_IS_NUMERIC', 'ITEM_IS_NOT_NUMERIC', 
          'ITEM_IS_ALPHANUMERIC', 'REQUEST_EQUALS_CONDITION', 'REQUEST_NOT_EQUAL_CONDITION', 'REQUEST_IN_CONDITION', 
          'REQUEST_NOT_IN_CONDITION', 'CURRENT_PAGE_EQUALS_CONDITION', 'CURRENT_PAGE_NOT_EQUAL_CONDITION', 
          'CURRENT_PAGE_IN_CONDITION', 'CURRENT_PAGE_NOT_IN_CONDITION', 'CURRENT_LANG_IN_COND1', 'CURRENT_LANG_NOT_IN_COND1', 'CURRENT_LANG_NOT_EQ_COND1',
          'CURRENT_LANG_EQ_COND1', 'DAD_NAME_EQ_CONDITION', 'DAD_NAME_NOT_EQ_CONDITION', 'SERVER_NAME_EQ_CONDITION',
          'SERVER_NAME_NOT_EQ_CONDITION', 'HTTP_HOST_EQ_CONDITION', 'HTTP_HOST_NOT_EQ_CONDITION' ],
        lCheckExpressionList = [ /* Show "Check" checkbox for Expression 1 */
          'EXISTS', 'NOT_EXISTS', 'SQL_EXPRESSION', 'PLSQL_EXPRESSION', 'FUNCTION_BODY' ];
          
    // no additional input required
    if (apex.jQuery.inArray(lValue, lNoExpressionsList) != -1 || lValue===pLovNullValue || lValue==="" || lValue==="%null%") {
      $x_ItemRow(pExpression1, 'HIDE');
      $x_ItemRow(pExpression2, 'HIDE');
    // expression 1 is required
    } else if (apex.jQuery.inArray(lValue, lOneExpressionList) != -1) {
      $x_ItemRow(pExpression1, 'SHOW');
      $x_ItemRow(pExpression2, 'HIDE');
    // both expression fields are required
    } else {
      $x_ItemRow(pExpression1, 'SHOW');
      $x_ItemRow(pExpression2, 'SHOW');      
    }

    // show/hide the "Check" expression checkbox if available
    if (pExpressionCheck!=="") {
      if (apex.jQuery.inArray(lValue, lCheckExpressionList) != -1) {
        $x_ItemRow(pExpressionCheck, 'SHOW');
      } else {
        $x_ItemRow(pExpressionCheck, 'HIDE');        
      }
    }

    // show/hide the read only attribute field if available
    if (pReadOnlyAttribute!=="") {
      if (lValue==="" || lValue===pLovNullValue || lValue==="NEVER") {
        $x_ItemRow(pReadOnlyAttribute, 'HIDE');
      } else {
        $x_ItemRow(pReadOnlyAttribute, 'SHOW');
      }
    }
  }, // setConditionItems

  /**
   * @class getData
     * Executes a SQL statement on the server and writes the result into the
     * specified DOM object
   * */
  getData: function() {
    var lPageItemsToSubmit = this.action.attribute01;
    var lAjaxRequest = new htmldb_Get(null, $v('pFlowId'), "PLUGIN="+this.action.ajaxIdentifier, $v('pFlowStepId'));
    var lAjaxResult  = null;
    // Set session state with the AJAX request for all page items which are defined
    // in our "Page Items to submit" attribute. Again we can use jQuery.each to
    // loop over the array of page items.
    apex.jQuery.each(
      lPageItemsToSubmit.split(','), // this will create an array
      function() {
        var lPageItem = apex.jQuery('#'+this)[0]; // get the DOM object
        // Only if the page item exists, we add it to the AJAX request
        if (lPageItem) {
          lAjaxRequest.add(this, $v(lPageItem)); }
      });
    // let's execute the AJAX request
    lAjaxResult = lAjaxRequest.get();
    // Assign the result to each affected element. Again we use jQuery.each to
    // loop over all affected elements. Remember, "this.affectedElements" is a jQuery object, that's
    // why we can use all the jQuery functions on it.
    if (lAjaxResult) {
      this.affectedElements.each(
        function() {
          $s(this, lAjaxResult);
        });
    }
  }
}; // plugin
