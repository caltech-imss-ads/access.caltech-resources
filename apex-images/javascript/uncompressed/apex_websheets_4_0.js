/*try{*/

if (apex.spreadsheet==null || typeof(apex.spreadsheet)!="object"){apex.spreadsheet = new Object()}

/*
 *The spreadsheet object extends interactive reports object
 * */
function ws_Spreadsheet (pThis){
    that = this;
	this.Spreadsheet	= $x(pThis);
	this.spreadsheet_id	= (pThis)?pThis.id:false;
	this.numberRows		= false;
	this.numberCells	= false;
	this.currentRow		= false;
	this.classArray		= false;
	this.currentCell	= false;
	this.currentForm	= false;
	this.currentCol		= false;
	this.currentColType = false;
	this.cell			= {};
	/**/
	this.Reset		= Reset;
	this.CellInit	= CellInit;
	this.init = init;

    /*
     * this.row
     *
     */
    this.row = function(pRowId){
    	var get = new htmldb_Get(null, $v('pFlowId'), 'APPLICATION_PROCESS=ITEM_ROW', $v('pFlowStepId'));
		get.add('GOTO_WORKSHEET_ROW', pRowId, 50);
        var $lContainer, $lSingleEls, $lCompoundEls, $lAllEls, lNumber;
        $lContainer = apex.jQuery('#row')[0];
        $lSingleEls = apex.jQuery('input:text,input:hidden,textarea,select', $lContainer);
        $lCompoundEls = apex.jQuery(':radio:checked', $lContainer);
        $lAllEls = apex.jQuery($lSingleEls).add($lCompoundEls);

        var lArray = [];
        for (i=0; i < $lAllEls.length; i++) {
            lArray[0] = apex.jQuery($lAllEls)[i];
            get.AddArrayItems(lArray, parseInt(apex.jQuery(lArray[0]).attr('name').substr(1),10));
        };
    	get.GetAsync(that.row._Return);
    	get = null;
    }

    /*
     * this.row._Return
     * Handled the old fashioned way to enable setting page id in request.
     */
    this.row._Return = function(){
			var l_s = p.readyState;
    		if(l_s == 1){
    		}else if(l_s == 2){
    		    that._BusyGraphic(l_s);
    		}else if(l_s == 3){
    		}else if(l_s == 4){
				    var lReturn, lErrFlag, lErrMsg;
				    lReturn = p.responseText;
				    lErrFlag = (lReturn.substr(0, 5) === 'error');
				    // Removed the message that could have been displayed by full page refresh validation failure
				    $x_Remove('MESSAGE');
				    if (lErrFlag) {
                        lErrMsg = lReturn.substr(6);
				        apex.jQuery('#ajaxMESSAGE').show();
				        apex.jQuery('#theMESSAGE').html(lErrMsg);
				    } else {
				        apex.jQuery('#ajaxMESSAGE').hide();
				        apex.jQuery('#drop').html(lReturn);
				    }
				    gReport.dialog.reset();
					that._BusyGraphic(l_s);
    		}else{return false;}
    }


    this.item_help = function(pColName, pThis) {
        // Show jQuery div based dialog if not running in screen reader mode, if not fall back to popup
        if (!$x('pScreenReaderMode')) {
            var lRequest, l_s, lDialog, lLabel;
            lRequest = new apex.ajax.ondemand('GET_ITEM_HELP', function() {
                // Callback
                l_s = p.readyState;
                if(l_s == 1 || l_s == 2 || l_s == 3) {
                } else if (l_s == 4) {
                    gReturn = p.responseText;
                    var lDialog = apex.jQuery("#apex_popup_field_help");
                    // if dialog doesn't exist, create new one
                    if (lDialog.length === 0) {
                        lDialog = apex.jQuery('<div id="apex_popup_field_help">' + gReturn + '</div>');
                        // open created div as a dialog
                        lDialog.dialog({
                            title   : apex.jQuery(pThis).text(),
                            bgiframe: true,
                            width   : 500,
                            height  : 350,
                            show    : 'drop',
                            hide    : 'drop'
                        });
                    } else {
                        lDialog
                            .html(gReturn)
                            .dialog('option', 'title', apex.jQuery(pThis).text())
                            .dialog('open');
                    }
                } else {
                    return false;
                }
            });
            // Pass the column name as a parameter in the call
            lRequest.ajax.AddNameValue('COLUMN_NAME', pColName);
            // Make the call
            lRequest._get();
        } else {
            html_PopUp('wwv_flow.show?' +
                            'p_request=APPLICATION_PROCESS=GET_ITEM_HELP' + '&' +
                            'p_instance=' + $v('pInstance') + '&' +
                            'p_flow_id=' + $v('pFlowId') + '&' +
                            'p_flow_step_id=' + $v('pFlowStepId') + '&' +
                            'f01=COLUMN_NAME' + '&' +
                            'f02=' + pColName,
                       'Help',
                       500,
                       350);
        }
    }

	/*
	 * Calls all Dialogs
	 *
	 * */
	this.dialog = function(pDialog,p_X01,p_X02){
		var lRequest = new apex.ajax.ondemand('DIALOG',
		function(){
		    /* start the return function */
			var l_s = p.readyState;
    		if(l_s == 1){
    		}else if(l_s == 2){
    		    that._BusyGraphic(l_s);
    		}else if(l_s == 3){
    		}else if(l_s == 4){
				gReturn = p.responseText;
			    if(pDialog == 'LOV_TEXT'){
					$s('apexir_LOV_ENTRIES',gReturn);
				}else{
					/* base init's for dialog returns calls */
					// remove help dialog, if shown
				    if (apex.jQuery('#apex_popup_field_help').dialog('isOpen')) {
				        apex.jQuery('#apex_popup_field_help').dialog('close');
				    }
					$s('apexir_CONTROL_PANEL_DROP',gReturn);
					$x_Show('apexir_CONTROL_PANEL_DROP');
					/* check for shuttle and init, change this to look for shuttle class in dialog */
					if($x('apexir_SHUTTLE_LEFT')){
						window.g_Shuttlep_v01 = new dhtml_ShuttleObject('apexir_SHUTTLE_LEFT','apexir_SHUTTLE_RIGHT');
					}
					/* custom init code for different dialogs*/
					if(pDialog == 'NOTIFY'){
						apex.jQuery('#apexir_NOTIFY_INTERVAL').change(function(){$f_Hide_On_Value_Item(this, 'apexir_DATEPICKERS', 'I')}).change();
					}
        		    // focus on first input field of drop panel, if any form fields exist within a drop panel
                    var $lDropFields = apex.jQuery(':input:visible', apex.jQuery('#apexir_CONTROL_PANEL_DROP'));
                    if ($lDropFields.length > 0) {
                        $lDropFields[0].focus();
                    }
				}
				that._BusyGraphic(l_s);
			}else{return false;}
			/* end the return function */
			}
		);

		lRequest.ajax.AddNameValue('DIALOG', pDialog);
		lRequest.ajax.AddNameValue('VALUE',  p_X01);
		lRequest.ajax.AddNameValue('VALUE2', p_X02);
		if($v('pFlowStepId') == 20){
			lRequest.ajax.AddNameValue('MODE','DETAIL');
		}
		lRequest._get();
	}

	this.actions = function(pAction,pThis){
		if($v('pFlowStepId') == 20){
			if(pAction == 'ATTACHMENT_SAVE'){
				$x('P20_X').name = 'p_ignore_10';
				$x('apexir_FILE').name = 'p_t01';
				$x('apexir_DIALOG_MESSAGE').innerHTML = '<img src="/i/processing3.gif" /><iframe name="myNewWin" id="myNewWin" height="100" width="20" style="display:none;" src="/i/processing3.gif"></iframe>';
				$x('wwvFlowForm').target = 'myNewWin';
				var a = window.setTimeout(function(){apex.submit('ATTACHMENT');},500);
			}else{
				var lRequest = new apex.ajax.ondemand('websheet_detail',
					function(){
						/* start the return function */
					var l_s = p.readyState;
            		if(l_s == 1){
            		}else if(l_s == 2){
            		    that._BusyGraphic(l_s);
            		}else if(l_s == 3){
            		}else if(l_s == 4){
						websheet_return(p,pAction);
						that._BusyGraphic(l_s);
					}else{return false;}
						/* end the return function */
					}
				);
				(!!pThis)?lRequest.ajax.addParam('p_widget_action_mod',pThis):null;
				that.call_action(lRequest,pAction);
			}

		}else if(pAction == 'EXPORT'){
            switch (apex.jQuery(':radio:checked', apex.jQuery('#apexir_EXPORT_FORMAT')[0]).val()){
                case 'APEX':
                    null;
                break;
                case 'TEMPLATE':
                    redirect('f?p='+$v('pFlowId')+':80:'+$v('pInstance')+'::NO:::');
                break;
                default:
        			/*Show error message in dialogue box*/
        			$s('apexir_DIALOG_MESSAGE','Export location must be specified.');
        			$x_Show('apexir_DIALOG_MESSAGE');
                break;
            }
        }else{
    		var lRequest = new apex.ajax.ondemand('websheet',
    			function(){
    			/* start the return function */
    			var l_s = p.readyState;
        		if(l_s == 1){
        		}else if(l_s == 2){
        		    that._BusyGraphic(l_s);
        		}else if(l_s == 3){
        		}else if(l_s == 4){
    				websheet_return(p,pAction);
    				that._BusyGraphic(l_s);
    			}else{return false;}
    			/* end the return function */
    			}
    		);
    		that.call_action(lRequest,pAction);
		}
	}

	this._BusyGraphic = function(pState){
		if(pState == 2){
			$x_Show('apexir_LOADER');
		}else{
			$x_Hide('apexir_LOADER');
		}
		return;
	}

	this.collect_checked_rows = function(pThis){
		$f_CheckFirstColumn(pThis);
		gReport.websheet.actions('save_checked');
	}

	this.call_action = function(pAjax,pAction){

		(!!pAction)?pAjax.ajax.addParam('p_widget_action',pAction):null;
		switch(pAction){
			case 'set_geocode':
				pAjax.ajax.addParam('x01',$v('apexir_SHUTTLE_RIGHT'));
			;break;
			case 'reset_geocode':false;break;
			default:
				if(pAction == 'delete_rows' || pAction == 'set_column_value' || pAction == 'replace_column_value'){
					pAjax.ajax.addParam('x01',$u_ArrayToString(websheets_collect_checks(),':'));
				}
				if($v('pFlowStepId') == 20){
					pAjax.ajax.addParam('x03','DETAIL');
				}
				pAjax.ajax.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
			;break;
		}

		/* fire ajax */
		gReport.l_LastFunction = function (){gReport.websheet.init()}
		pAjax._get();

	}

	this.dialog.utility = function(pThis){}

    this.cells = {}
	this.cells.edit = function(pThis,pThat){}

	that.init()
	return;

	function init(){
		apex.jQuery('div.edit:not(div.readonly)',apex.jQuery('#'+that.spreadsheet_id)[0])
		    .unbind('click')
		    .click(function(event) {
			    gReport.websheet.CellInit(this, event);
		    });
	}

    /*
     *  Intitiates all editable controls in the data grid
     *  pThis - The div container element for the cell
     *  pEvent - The click event object, passed from the handler
     */
    function CellInit(pThis, pEvent){
		that = this;
		this.Reset();
		this.currentRow = pThis.parentNode.parentNode;
		this.currentCell = pThis;
		this.classArray = pThis.className.split(" ");
		this.currentCol = this.classArray[0];
		this.currentColTest = !isNaN(this.currentCol.substring(1));
		this.currentColType = this.classArray[1];
		// Store div container element in local variable, and unbind the click handler
		var $lCell = apex.jQuery(pThis).unbind("click");
		if(this.currentColType == 'text' && this.currentColTest){
			// Get the current computer width (includes padding and border) for the table td
		    var lWidth = apex.jQuery(pThis.parentNode).outerWidth();
			// Get the current value
			var lValue = $lCell.text();
			// Clear the value from the cell and set the style padding to zero
			$lCell
			    .html('')
			    .css('padding','0');
            // Create a dom input as a child of the main div, with a value of the current value
			var $lTextEl = apex.jQuery($dom_AddInput(pThis, 'TEXT', '', '', lValue));
			// Set css width, event handler to handle the save when focus is lost and set focus
			$lTextEl
			    .css('width', lWidth)
			    .blur(function(e) {
			        cellsave(e, this);
			    })
			    .keypress(function(e) {
			        if (e.which==13) {
			            return false;
			        }
			    })
			    .focus();

			this.currentForm = $lTextEl[0];
			this.currentValue = lValue;
		} else if (this.currentColType == 'textarea' && this.currentColTest) {
			var lValue = $lCell.html();
			// Clear the value from the cell and set the style padding to zero
			$lCell
			    .html('')
			    .css('padding','0');
            // Create a dom textarea as a child of the main div, with a value of the current value
			var $lTextAreaEl = apex.jQuery($dom_AddTag(pThis,'TEXTAREA'));
            // Set the css properties, value, event handler to handle the save when focus is
            // lost and set focus
            $lTextAreaEl
                .css('width','100%')
                .val(lValue)
			    .blur(function(e) {
			        cellsave(e, this)
			    })
                .focus();
			this.currentForm = $lTextAreaEl[0];
			this.currentValue = lValue;
		} else if (this.currentColType == 'date' && this.currentColTest) {
			// Get the current computer width (includes padding and border) for the table td
		    var lWidth = apex.jQuery(pThis.parentNode).outerWidth();
            // Get the current value
            var lValue = $lCell.text();
			$lCell
			    .html('')
			    .css('padding','0');
            // Create a dom input as a child of the main div, with a value of the current value
			var $lTextEl = apex.jQuery($dom_AddInput(pThis, 'TEXT', 'theCal', '', lValue));
			$lTextEl.css('width', lWidth);
			this.currentForm = $lTextEl[0];
			this.currentValue = lValue;

			//get the date format
            var lRequest = new apex.ajax.ondemand('get_dtFmt',
            	function(){
            		var l_s = p.readyState;
            		if(l_s == 1){
            		}else if(l_s == 2){
            		    that._BusyGraphic(l_s);
            		}else if(l_s == 3){
            		}else if(l_s == 4){
            			that._BusyGraphic(l_s);
            			var myObject = eval('(' + p.responseText + ')');
			            apex.widget.datepicker(
			                "#theCal",
			                {
			                    buttonText      : 'Calendar',
			                    showTime        : myObject.showTime,
			                    time24h         : myObject.time24h,
			                    timeFormat      : myObject.timeFormat,
			                    defHour         : myObject.defHour,
			                    defMinute       : myObject.defMinute,
			                    defAmpm         : myObject.defAmpm,
			                    defaultDate     : myObject.defaultDate,
			                    showOn          : 'focus',
			                    showAnim        : 'drop',
			                    showOtherMonths : false,
			                    changeMonth     : false,
			                    changeYear      : false,
			                    onClose         : function(dateText, inst) {
			                        cellsave(null, this);
			                    }
			                },
			                myObject.dtFmt,
			                myObject.lang
			            );
			            // Important to set the focus, as this activates the datepicker
			            $lTextEl.focus();
            		} else {
            		    return false;
            		}
            	}
            );
            lRequest.ajax.AddNameValue('COLUMN'	,gReport.websheet.currentCol);
            lRequest.ajax.AddNameValue('DATE'	,lValue);
            lRequest._get();
		} else if (this.currentColType == 'selectlist' && this.currentColTest) {
			//gReport.controls.value = that.currentCell.innerHTML;
			var lValue = $lCell.text();
			gReport.controls.value = lValue;
			$s(that.currentCell,'');
			gReport.controls.lov = true;
			gReport.controls.lov_dom = $lCell.get(0);
			gReport.controls.col_lov(that.currentCol);
			this.currentValue = lValue;
		} else if (this.currentColType == 'radiogroup' && this.currentColTest) {
			// Get the current computer width (includes padding and border) for the table td
		    var lWidth = apex.jQuery(pThis.parentNode).outerWidth();
            // Get the current value
            var lValue = $lCell.text();
			$lCell
			    .html('')
			    .css('padding','0');

            // Create a dom input as a child of the main div, with a value of the current value
			var $lTextEl = apex.jQuery($dom_AddInput(pThis, 'TEXT', '', '', lValue));
			// Set css width...
			$lTextEl
			    .css({'width':lWidth,'position':'relative'})
			    .attr('readonly', 'readonly');

			this.currentForm = $lTextEl[0];
			this.currentValue = lValue;

            var lLabel = '';
            // add a new div with the retrieved page, add title attribute here instead of as option, IE workaround
            lDialog = apex.jQuery('<div title="' + lLabel + '"></div>');

            gReport.controls.value = lValue;
			gReport.controls.lov = true;
			gReport.controls.lov_dom = lDialog[0];
			gReport.controls.radio_lov(that.currentCol);
		} else {
			this.Reset();
		}
	}

	function Reset(){
		if($x('killme')) {
		    $x_Remove($x('killme'))
		}
		if($x(this.currentCell)){
			apex.jQuery(this.currentCell)
				.text(this.currentValue)
				.css('padding','')
				.unbind('click')
				.one('click',function() {
				    gReport.websheet.CellInit(this);
				});
		}
		this.currentRow		= false;
		this.currentCell	= false;
		this.currentForm	= false;
		this.currentCol		= false;
	}
}

function cellkey(e,pThis){
    var keycode;
    if(window.event){keycode = window.event.keyCode;}
    else if (e){keycode = e.which;}
    else {return false;}
	if(e.ctrlKey){
	switch(keycode){
		case 9 :runTab(pThis); break;
		case 13 :runTab(pThis);/*enter*/ null; break;
		case 37 :runTab(pThis);/*left*/ null; break;
		case 38 :runTab(pThis);/*up*/ null; break;
		case 39 :runTab(pThis);/*down*/ null; break;
		case 40 :runTab(pThis);/*right*/ null; break;
		case 17 :runTab(pThis);/*ctrl*/ null; break;
		default:null;
	}
	}
}

function runTab(pThis){
	if(pThis.parentNode.cellIndex < gReport.websheet.numberCells-1){
	 gReport.websheet.CellInit(pThis.parentNode.nextSibling);
	}else if(pThis.parentNode.cellIndex == gReport.websheet.numberCells-1){
	 if(gReport.websheet.currentRow.rowIndex == gReport.websheet.Spreadsheet.rows.length-1){
		gReport.websheet.CellInit(gReport.websheet.Spreadsheet.rows[1].cells[1]);
	 }else{
	  gReport.websheet.CellInit(gReport.websheet.Spreadsheet.rows[gReport.websheet.currentRow.rowIndex+1].cells[1]);
	 }
	}
}

function cellsave(e,pThis){
    if(gReport.websheet.currentValue != pThis.value || apex.jQuery(pThis).hasClass('apex-tabular-form-error')){
		gReport.websheet.currentValue = $v(pThis);

        var get = new htmldb_Get(null, $v('pFlowId'),'APPLICATION_PROCESS=CELL',$v('pFlowStepId'));
		get.AddNameValue('ACTION'	,'SAVE');
		get.AddNameValue('ROW'	,gReport.websheet.currentRow.id.substring(3));
		get.AddNameValue('COLUMN'	,gReport.websheet.currentCol);
		get.AddNameValue('VALUE'	,pThis.value);
		get.AddNameValue('CHANGE'	,apex.jQuery(gReport.websheet.currentRow).attr("apex:c"));
        var lReturn = get.get();

		if (lReturn != 'true'){
		    var l = apex.jQuery(pThis);
		    l.addClass('apex-tabular-form-error');
		    apex.jQuery("#ajaxMESSAGE").show();
		    apex.jQuery("#theMESSAGE").html(lReturn);
		    pThis.focus();
            if (that.currentColType === 'date') {l.datepicker('show');}
		} else {
		    var l = apex.jQuery(gReport.websheet.currentRow);
		    l.attr("apex:c",(parseInt(l.attr("apex:c"))+1));
		    var c = apex.jQuery(pThis);
		    c.removeClass('apex-tabular-form-error');
		    apex.jQuery("#ajaxMESSAGE").hide();
		    gReport.websheet.Reset();
	    }  
        get = null;

	}else{
		gReport.websheet.Reset();
	}
	return;
}

function addOption(selectbox,text,value ){
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
	return optn
}

function ws_GetSelectItem(pThis,pVal){
		this.dGet = dGet;
		var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=ws_GetSelectItem',0);
		this.dGet();
	return;

	function dGet(){
		get.add('CURRENT_WORKSHEET_ROW',gReport.websheet.currentRow.id.substring(3));
		get.add('AJAX_WS_COLUMN',gReport.websheet.currentCol);
	 	get.GetAsync(dShow);
	}

	function dShow(){
		var l_s = p.readyState;
	 	if(l_s == 1||l_s == 2||l_s == 3){
	 	ajax_Loading(l_s);
	 	}else if(p.readyState == 4){
			ajax_Loading(l_s);
			var myObject = eval('(' + p.responseText + ')');
			for (var i=0;i<myObject.row.length;i++){
				var lOpt = addOption(pThis,myObject.row[i].R,myObject.row[i].R);
				if(myObject.row[i].R == pVal){lOpt.selected=true;}
			}
		}
	}
}
/*
}catch(e){}
*/

/*
 collect all checkboxs in the data area. Skip anything that doesn't have an ID.
 */

function websheets_collect_checks(){
	var lReturn = [];
	var lSelect = $x_FormItems('apexir_DATA_PANEL','CHECKBOX');
	for(var i=0,l=lSelect.length;i<l;i++){
		if(lSelect[i].checked && lSelect[i].id){
			lReturn[lReturn.length]=lSelect[i].value;
		}
	}
	return lReturn;
}

/*

   This function takes care of all AJAX returns at the report level,
   use this to enforce specific actions based on return value and action being fired

 */
function websheet_return(p,pAction){
	if($v('pFlowStepId') == 20){
		/* if on the detail page automatically call detail specific return*/
	   if(p.responseText == 'true'){
			/* Pull current detail row */
			$x_Remove('actionsMenu');
			gReport.websheet.row($v('apexir_current_row'));
	   }else{
			/*Show error message in dialogue box*/
			$s('apexir_DIALOG_MESSAGE',p.responseText);
			$x_Show('apexir_DIALOG_MESSAGE');
	   }
	}else{
		if(p.responseText == 'true'){
			if(pAction == 'delete_websheet'){
				document.location = 'f?p='+$v('pFlowId')+':902:'+$v('pInstance')+'::NO:::';
			}else if(pAction == 'save_checked'){
			}else{
			    /*Pull report*/
				gReport.pull();
			}
		}else{
			/*Show error message in dialogue box*/
			$s('apexir_DIALOG_MESSAGE',p.responseText);
			$x_Show('apexir_DIALOG_MESSAGE');
		}
	}
}

/* remove the call to this in wwv_flow_ws_dialog.plb*/
function pull_lov(pThis){
	gReport.websheet.dialog('LOV',$v(pThis));
}

function closeupload(){
	$x('P20_X').name = 'p_t01';
	$x_Remove('actionsMenu');
	gReport.websheet.row($v('apexir_current_row'));
	apex.jQuery('#wwvFlowForm').removeAttr('target');
}

function select_test(pThis){
	switch($v(pThis)){
		case'NEW':
			$x_Show('apexir_LOV_ATTRIBUTES');
			break;
		case'':
			$x_Hide('apexir_LOV_ATTRIBUTES');
			break;
		case'NEW_CURRENT_VALUES':
			$x_Hide('apexir_LOV_ATTRIBUTES');
		break;
		default:
			gReport.websheet.dialog('LOV_TEXT',$v(pThis));
			$s('apexir_LOV_NAME',$f_SelectedOptions(pThis).text);
			$x_Show('apexir_LOV_ATTRIBUTES');
			break;
	}
}

