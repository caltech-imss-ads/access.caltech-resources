/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 4.0

B32468-02

Copyright � 2003, 2010, Oracle. All rights reserved.

Primary Author:  Anthony Rayner

The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.

The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.

If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:

U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.

The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.

Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.

The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/

/**
 * @fileOverview
 * This file holds namespaced objects and functions for dynamic actions in Oracle Application Express
 *
 * */
if (!apex.da || typeof(apex.da) != "object"){apex.da = {};}

/**
 * @namespace = apex.da
 */

apex.da = {
    // Stores meta data about all defined dynamic actions of the current page.
    // Format: see database package wwv_flow_dynamic_action
    gEventList : [],

    // Flag used to control tabular form processing, signals when the triggering element is columnar
    gTriggerIsColumn : false,

    /**
     * init Function
     * Binds the dynamic actions and fires the initial action for page initialization
     * */
    init : function(){
        apex.jQuery(apex.da.gEventList).each(function(){
  	        var lDefaults,
  	            lEvent,
  	            lSelector;

  	        apex.da.gTriggerIsColumn = false;

            // make sure that all the required attributes are there, because we don't
            // set them on the database side if they are null
            lDefaults = {
  	  	        name : null
  	  	    };
            // use jQuery extend, properties present in object passed as 2nd parameter will override properties of 1st
            lEvent = apex.jQuery.extend(lDefaults, this);

            // Construct jQuery selector
            lSelector = apex.da.constructSelector({
                elementType     : lEvent.triggeringElementType,
                element         : lEvent.triggeringElement,
                regionId        : lEvent.triggeringRegionId,
                buttonId        : lEvent.triggeringButtonId
            });

            // bind event handlers to all elements for this triggering event
            apex.jQuery(lSelector)[lEvent.bindType](lEvent.bindEventType, function(pBrowserEvent, pData){
                apex.da.actions(this,
                                lEvent,
                                pBrowserEvent,
                                pData);
            });
            // page initialization
  	        apex.da.actions(lSelector,
  	                        lEvent,
  	                        'load');
        });
    },

    /**
     * constructSelector function
     * construct jQuery selector for elements, by type
     * */
    constructSelector : function (pOptions){
        var lDefaults,
            lSelector;

        // Define default option values
        lDefaults = {
            elementType         : null,
            element             : null,
            regionId            : null,
            buttonId            : null,
            triggeringElement   : null,
            eventTarget         : null
        };
        // Extend default option values with anything passed via pOptions
        lOptions = apex.jQuery.extend(lDefaults, pOptions);
        lSelector = '';

        // Construct selector based on element type ('ITEM', 'REGION', etc)
        switch (lOptions.elementType) {
        case 'ITEM':
            lSelector = '#' + lOptions.element.replace(/,/g,',#');
            break;
        case 'REGION':
            lSelector = '#' + lOptions.regionId;
            break;
        case 'BUTTON':
            lSelector = '#' + lOptions.buttonId;
            break;
        case 'DOM_OBJECT':
            lSelector = '#' + lOptions.element.replace(/,/g,',#');
            /* Check if this selector returns an element, if it does great. If not evaluate lOptions.element.
               (This allows any DOM object, or any variable to be used here as well.) */
            if (!apex.jQuery(lSelector).length) {
                // ~EVALuate lOptions.element inside a try block, in case it doesn't hold a valid DOM object constant
                try {
                    lSelector = eval(lOptions.element);
                } catch (err) {
                    lSelector = lOptions.element;
                }
            }
            break;
        case 'JQUERY_SELECTOR':
            lSelector = lOptions.element;
            break;
        case 'TRIGGERING_ELEMENT':
            lSelector = lOptions.triggeringElement;
            break;
        case 'EVENT_SOURCE':
            lSelector = lOptions.eventTarget;
            break;
        default:
            // Default to the document node. This is used when no 'Selection Type' has been specified.
            lSelector = document;
        }
        return lSelector;
    },

    /**
     * show function
     * shows all affected elements
     * */
    show : function(){
        var showRow;
        if (this.affectedElements){
            showRow = (this.action.attribute01==='Y');
            this.affectedElements.each(function(){
                apex.item(this).show(showRow);
            });
        }
    },

    /**
     * hide function
     * hides all affected elements, also has option to clear contents of affected elements
     * */
    hide : function(){
        var hideRow;
        if (this.affectedElements){
            hideRow = (this.action.attribute01==='Y');
            this.affectedElements.each(function(){
                apex.item(this).hide(hideRow);
            });
        }
    },

    /**
     * enable function
     * enables all affected elements
     * */
    enable : function(){
        if (this.affectedElements){
            this.affectedElements.each(function(){
                apex.item(this).enable();
            });
        }
    },

    /**
     * disable function
     * disables all affected elements, also has option to clear contents of affected elements
     * */
    disable : function(){
        if (this.affectedElements){
            this.affectedElements.each(function(){
                apex.item(this).disable();
            });
        }
    },

    /**
     * setValue function
     * sets the value of all affected elements
     * */
    setValue : function(){
        var lAction,
            lSetType,
            lStaticAssignment,
            lPageItemsToSubmit,
            lJavaScriptExpression,
            lSuppressChangeEvent,
            lValueToSet,
            lAjaxRequest,
            lAjaxResult,
            lData,
            lPageItem,
            lJSExpression,
            lAffectedElementArray;
        // Initialise variables
        lAction                 = this.action;
        lSetType                = lAction.attribute01;
        lStaticAssignment       = lAction.attribute02;
        lPageItemsToSubmit      = lAction.attribute04;
        lJavaScriptExpression   = lAction.attribute05;
        lSuppressChangeEvent    = (lAction.attribute09 === 'Y' ? true : false);
        //
        if (lSetType==='STATIC_ASSIGNMENT'){
            this.affectedElements.each(function(i){
                // Set the value to the first affected element, null the rest.
                lValueToSet = (i===0?lStaticAssignment:'');
                $s(this, lValueToSet, null, lSuppressChangeEvent);
            });
        } else if (lSetType === 'SQL_STATEMENT' || lSetType === 'PLSQL_EXPRESSION' || lSetType === 'FUNCTION_BODY') {
            lAjaxRequest = new htmldb_Get(null, $v('pFlowId'), "NATIVE="+lAction.ajaxIdentifier, $v('pFlowStepId'));
            lAjaxResult  = null;
            // Set session state with the AJAX request for all page items which are defined
            // in our "Page Items to submit" attribute. Again we can use jQuery.each to
            // loop over the array of page items.
            if (lPageItemsToSubmit){
                apex.jQuery.each(lPageItemsToSubmit.split(','), function() {
                    lPageItem = apex.jQuery('#'+this)[0]; // get the DOM object
                    // Only if the page item exists, we add it to the AJAX request
                    if (lPageItem) {
                        lAjaxRequest.add(this, $v(lPageItem));
                    }
                });
            }
            // let's execute the AJAX request
            lAjaxResult = lAjaxRequest.get();
            if (lAjaxResult) {
                lData = apex.jQuery.parseJSON(lAjaxResult);
                // check for errors first
                if (lData.values[0].error) {
                    alert(lData.values[0].error);
                    return false; // indicate to the framework that an error occurred
                } else {
                    // Use original lAction.affectedElements instead of this.affectedElements
                    // jQuery object, because the developer-specified order will be preserved.
                    lAffectedElementArray = lAction.affectedElements.split(',');
                    for(var i=0,len=lAffectedElementArray.length;i<len;i++) {
                        // If the value corresponding to the current affected element exists,
                        // use that, otherwise set to null. So in the case where the user has
                        // defined more affected elements than values, the remaining affected
                        // elements are set to null.
                        lValueToSet = (typeof(lData.values[i])!='undefined'?lData.values[i]:'');
                        // Set the affected element
                        $s(lAffectedElementArray[i], lValueToSet, null, lSuppressChangeEvent);   
                    }
                }
            }
        } else if (lSetType === 'JAVASCRIPT_EXPRESSION') {
            lJSExpression = eval(lJavaScriptExpression);
            this.affectedElements.each(function(i){
                // Set the value to the first affected element, null the rest.
                lValueToSet = (i===0?lJSExpression:'');
                $s(this, lValueToSet, null, lSuppressChangeEvent);
            });
        }
    },

    /**
     * clear function
     * clears the value/html of all affected elements
     * */
    clear : function(){
        if (this.affectedElements){
            this.affectedElements.each(function(){
                $s(this, '', '');
            });
        }
    },

    /**
     * addClass function
     * adds 1 or more classes to all affected elements
     * */
    addClass : function(){
        if (this.affectedElements){
            this.affectedElements.addClass(this.action.attribute01);
        }
    },

    /**
     * removeClass function
     * removes 1, more aor all class information from all affected elements
     * */
    removeClass : function(){
        if (this.affectedElements){
            //if attribute01 is null, need to just call removeClass with nothing provided.
            if (this.action.attribute01){
                this.affectedElements.removeClass(this.action.attribute01);
            }else{
                this.affectedElements.removeClass();
            }
        }
    },

    /**
     * setCSS function
     * sets CSS style properties to all affected elements
     * */
    setCSS : function(){
        if (this.affectedElements){
            this.affectedElements.css(this.action.attribute01, this.action.attribute02);
        }
    },

    /**
     * setFocus function
     * sets the focus to the specified element
     * */
    setFocus : function(){
        if (this.affectedElements){
            this.affectedElements.focus();
        }
    },

    /**
     * submitPage function
     * submits the current page with the specified request value
     * */
    submitPage : function(){
        var lAction, lRequest, lShowProcessing;
        lAction         = this.action;
        lRequest        = lAction.attribute01;
        lShowProcessing = (lAction.attribute02 === 'Y' ? true : false);
        apex.submit({
            request  : lRequest,
            showWait : lShowProcessing
        });
    },

    /**
     * refresh function
     * sends the apexrefresh event to the specified DOM elements
     * */
    refresh : function(){
        if (this.affectedElements){
            this.affectedElements.trigger('apexrefresh');
        }
    },

    /**
     * executePlSqlCode function
     * Executes PL/SQL code and will alert the user if an error has occurred
     * */
    executePlSqlCode : function(){
        // Declare variables. In dealing with attribute values, it's better to have
        // named variables instead of using the generic ones, that makes the code more
        // readable.
        var lPageItemsToSubmit,
            lAjaxRequest,
            lAjaxResult,
            lSuppressChangeEvent;

        // Initialise local variables to their corresponding plug-in attributes.
        lPageItemsToSubmit      = this.action.attribute01;
        lSuppressChangeEvent    = (this.action.attribute04 === 'Y' ? true : false);

        // Build an AJAX request as you would do for an on-demand call. The only
        // difference is that instead of "APPLICATION_PROCESS=" you have to use "NATIVE=".
        lAjaxRequest = new htmldb_Get(null, $v('pFlowId'), "NATIVE=" + this.action.ajaxIdentifier, $v('pFlowStepId'));
        lAjaxResult  = null;

        // Set session state with the AJAX request for all page items which are defined
        // in our "Page Items to submit" attribute. Again we can use apex.jQuery.each to
        // loop over the array of page items.
        if (lPageItemsToSubmit) {
            apex.jQuery.each(
                lPageItemsToSubmit.split(','), // this will create an array
                function () {
                    var lPageItem;
                    lPageItem = apex.jQuery('#' + this)[0]; // get the DOM object
                    // Only if the page item exists, we add it to the AJAX request
                    if (lPageItem) {
                        lAjaxRequest.add(this, $v(lPageItem));
                    }
                }
            );
        }
        // Let's execute the AJAX request.
        lAjaxResult = lAjaxRequest.get();

        // If the result of the AJAX call is not null, we know that either the call has 
        // errored and the response contains the error message, or the response contains
        // the returning page item values ready to set.
        // If the result is null, then the call was successful and there are no
        // returning page items to set.
        if (lAjaxResult) {
            var lResultObj = apex.jQuery.parseJSON(lAjaxResult);
            // check for errors first
            if (lResultObj.error) {
                alert(lResultObj.error);
                return false; // indicate to the framework that an error occurred
            } else {
                // If there's no errors, proceed with setting the 'items to return'
                apex.jQuery.each(lResultObj.item, function(){
                    $s(this.id, this.value, null, lSuppressChangeEvent);
                });
            }
        }
    },

    /**
     * cancelEvent function
     * Cancels event processing...
     * */
    cancelEvent : function(){
        // Set cancel flag in the apex.event namespace to true. This value can be used to cancel subsequent
        // processing, such as in page submission to stop the page from being submitted.
        apex.event.gCancelFlag = true;

        // Set cancel actions flag in apex.event namespace to true. This value is used in dynamic
        // actions processing to stop further actions firing.
        apex.event.gCancelActions = true;

        // Call the event method stopImmediatePropagation. This prevents any event handlers
        // bound to the current event from executing. It also calls stopPropagation to stop the
        // event from bubbling up the DOM (if it supports bubbling), so any event handlers bound
        // to ancestrial DOM elements will not fire either.
        this.browserEvent.stopImmediatePropagation();

        // Call the event method preventDefault. This prevents the default behaviour of the
        // event (for example prevents going to the URL of a link, if a link is clicked).
        this.browserEvent.preventDefault();
    },

    /**
     * showAlert function
     * displays a JavaScript alert with the specified text
     * */
    showAlert : function(){
        alert(this.action.attribute01);
    },

    /**
     * askConfirm function
     * displays a JavaScript confirm dialog with the specified text
     * */
    askConfirm : function(){
        if (!confirm(this.action.attribute01)) {
            // don't continue with dynamic actions
            apex.da.cancelEvent.call(this);
        }
    },

    /**
     * doAction function
     * Executes the action (pAction) on certain elements (pSelector)
     * */
    doAction : function(pTriggeringElement, pSelector, pAction, pBrowserEvent, pData, pDynamicActionName){
        var lContext;
        lContext = {
            triggeringElement   : pTriggeringElement,
            affectedElements    : apex.jQuery(pSelector),
            action              : pAction,
            browserEvent        : pBrowserEvent,
            data                : pData
        };
        // Call the javascript function if one is defined and pass the lContext object as this
        if (pAction.javascriptFunction) {
            // Log details of dynamic action fired out to the console (only outputs when running in debug mode).
            apex.debug('Dynamic Action Fired: ' + pDynamicActionName + ' (' + pAction.action + ')');
            return pAction.javascriptFunction.call(lContext);
        }
    },

    /**
     * actions function
     * Fires the stored actions based on the triggering expression result
     * */
    actions : function(pSelector, pEvent, pBrowserEvent, pData){
        var lEventResult, lContext;
        // reset both cancel flags to false
        apex.event.gCancelFlag = false;
        apex.event.gCancelActions = false;

        apex.jQuery(pSelector).each(function(){
            var lElement,
                lApexItem,
                lValue,
                lExpressionArray,
                lTempFunc;

            // Initialise lElement to the current jQuery object being iterated over
            lElement = this;

            // Setup an apex item object
            lApexItem = apex.item(lElement.id);

            // Get it's value, could be either a single value or array of values, depending on the
            // item type.
            lValue = lApexItem.getValue();

            switch (pEvent.triggeringConditionType) {
                case 'EQUALS':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is equal to the triggering expression.
                        lEventResult = (lValue == pEvent.triggeringExpression);
                    } else {
                        lEventResult = false;
                        // If the item's value is an array, need to loop over it and check if any of the values in the
                        // value array are equal to the triggering expression.
                        apex.jQuery.each(lValue, function(index, value){
                            lEventResult = (value == pEvent.triggeringExpression);
                            // If event result is true, then exit iterator.
                            if (lEventResult) { return false; };
                        });
                    }
                    break;
                case 'NOT_EQUALS':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is not equal to the triggering expression.
                        lEventResult = (lValue != pEvent.triggeringExpression);
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if ALL of the values in the value array are
                        // not equal to the triggering expression.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = (value != pEvent.triggeringExpression);
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'IN_LIST':
                    lExpressionArray = pEvent.triggeringExpression.split(',');
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if it's in the expression array
                        lEventResult = (apex.jQuery.inArray(lValue, lExpressionArray) !== -1) ? true : false;
                    } else {
                        lEventResult = false;
                        // If the item's value is an array, need to check if any of the values in the value array equals any of
                        // the values in the expression array
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = (apex.jQuery.inArray(value, lExpressionArray) !== -1) ? true : false;
                            // If event result is true, then exit iterator.
                            if (lEventResult) { return false; };
                        });
                    }
                    break;
                case 'NOT_IN_LIST':
                    lExpressionArray = pEvent.triggeringExpression.split(',');
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if it's not in the expression array
                        lEventResult = (apex.jQuery.inArray(lValue, lExpressionArray) === -1) ? true : false;
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if any of the values in the value array do not
                        // equal any the values in the expression array.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = (apex.jQuery.inArray(value, lExpressionArray) === -1) ? true : false;
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'GREATER_THAN':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is greater than the triggering expression.
                        lEventResult = lValue > parseFloat(pEvent.triggeringExpression, 10);
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if ALL of the values in the value array are
                        // greater than the triggering expression.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = value > parseFloat(pEvent.triggeringExpression, 10);
                            // If iterated value is not greater than triggering expression, then exit iterator
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'GREATER_THAN_OR_EQUAL':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is greater than or equal the triggering expression.
                        lEventResult = lValue >= parseFloat(pEvent.triggeringExpression, 10);
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if ALL of the values in the value array are
                        // greater than or equal the triggering expression.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = value >= parseFloat(pEvent.triggeringExpression, 10);
                            // If iterated value is not greater than or equal triggering expression, then exit iterator
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'LESS_THAN':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is less than the triggering expression.
                        lEventResult = lValue < parseFloat(pEvent.triggeringExpression, 10);
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if ALL of the values in the value array are
                        // less than the triggering expression.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = value < parseFloat(pEvent.triggeringExpression, 10);
                            // If iterated value is not less than triggering expression, then exit iterator
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'LESS_THAN_OR_EQUAL':
                    if (!apex.jQuery.isArray(lValue)) {
                        // If the item's value is not an array, just check if the value is less than or equal the triggering expression.
                        lEventResult = lValue <= parseFloat(pEvent.triggeringExpression, 10);
                    } else {
                        lEventResult = true;
                        // If the item's value is an array, need to check if ALL of the values in the value array are
                        // less than or equal the triggering expression.
                        apex.jQuery.each(lValue, function(index, value) {
                            lEventResult = value <= parseFloat(pEvent.triggeringExpression, 10);
                            // If iterated value is not less than triggering expression, then exit iterator
                            if (!lEventResult) { return false; };
                        });
                    }
                    break;
                case 'NULL':
                    lEventResult = lApexItem.isEmpty();
                    break;
                case 'NOT_NULL':
                    lEventResult = !lApexItem.isEmpty();
                    break;
                case 'JAVASCRIPT_EXPRESSION':
                    // Set context to be used by "JavaScript Expression" condition type
                    lContext = {
                        triggeringElement   : lElement,
                        browserEvent        : pBrowserEvent,
                        data                : pData
                    };
                    lTempFunc = new Function('return ' + pEvent.triggeringExpression);
                    lEventResult = lTempFunc.call(lContext);
                    break;
                default:
                    //catches when no condition has been selected
                    lEventResult = true;
            }
            apex.jQuery.each(pEvent.actionList, function(){
                // check if no further actions should be executed, in the case where the event has been supressed
                // if the event cancelActions flag is true, return out of this each iterator
                if (apex.event.gCancelActions) {
                    return false;
                }

                // make sure that all the required attributes are there, because we don't
  	            // set them on the database side if they are null
  	            var lDefaults,
      	            lAction,
  	                lSelector;

                lDefaults = {
      	  	        eventResult:null,
  	  	            executeOnPageInit:false,
                    stopExecutionOnError:true,
      	            action:null,
      	            affectedElementsType:null,
      	            affectedRegionId:null,
      	            affectedElements:null,
                    javascriptFunction:null,
                    ajaxIdentifier:null,
      	            attribute01:null,
      	            attribute02:null,
      	            attribute03:null,
      	            attribute04:null,
      	            attribute05:null,
      	            attribute06:null,
      	            attribute07:null,
      	            attribute08:null,
      	            attribute09:null,
      	            attribute10:null,
      	            attribute11:null,
      	            attribute12:null,
      	            attribute13:null,
      	            attribute14:null,
      	            attribute15:null
  	            };
  	            // use jQuery extend, properties present in object passed as 2nd parameter will override properties of 1st
                lAction = apex.jQuery.extend(lDefaults, this);

                // check if action should be processed, process when pContext is not equal to load OR when pContext is load and     executeOnPageInit is true
                if (pBrowserEvent != 'load' || (pBrowserEvent == 'load' && lAction.executeOnPageInit)){
                    //only proceed if the result of the triggering event evaluation is equal to the eventResult property of the action
                    if (lAction.eventResult==lEventResult){
                        //Construct jQuery selector for the affected elements, to be used in call to doAction
                        lSelector = apex.da.constructSelector({
                            elementType         : lAction.affectedElementsType,
                            element             : lAction.affectedElements,
                            regionId            : lAction.affectedRegionId,
                            triggeringElement   : lElement,
                            eventTarget         : pBrowserEvent.target
                        });

                        // do the action.
                        // if it returns false (= error), stop executing other actions if the user has defined that
                        if (apex.da.doAction(lElement, lSelector, lAction, pBrowserEvent, pData, pEvent.name)===false && lAction.stopExecutionOnError) {
                          apex.event.gCancelActions = true;
                        };
                    }
                }
            });
            // Reset cancelActions flag to false, ready for next action
            apex.event.gCancelActions = false;
        });
    }
};