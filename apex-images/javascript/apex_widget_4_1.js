/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 4.0

B32468-02

Copyright � 2003, 2009, Oracle. All rights reserved.

Primary Author:  Patrick Wolf

The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.

The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.

If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:

U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.

The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.

Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.

The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/

/**
 * @fileOverview
 * This file holds all namespaced objects and functions for Oracle Application Express
 *
 * */
if (!apex.widget || typeof(apex.widget) != "object") apex.widget = {};
if (apex.widget.calendar===null || typeof(apex.widget.calendar)!="object"){apex.widget.calendar={};}

/**
 * @namespace = apex.widget
 */

apex.widget = {
  /**
   * @class textarea
     * Uses to initialize the textarea widget with the defined options in pOptions
   * */
  textarea: function(pId, pOptions){
    var gTextarea = apex.jQuery('#'+pId),
        gOptions  = apex.jQuery.extend({
                      isResizable: false,
                      hasCharCounter: false,
                      maxChar: null
                      }, pOptions);

    // exit if the id didn't exist
    if (gTextarea.length===0) return;

    /**
     * Adds the necessary events to an object with appended size-bar to make it resizeable
     * */
    function resizable()
    {
      // static closure variables used by startResize and performResize
      var gOffsetX   = null,
          gOffsetY   = null,
          // the resized object should not become smaller as it's original size
          gMinWidth  = gTextarea.width(),
          gMinHeight = gTextarea.height();

      // Disable browser based resizing, because it's handled by us
      gTextarea.css("resize", "none");

      // Add the mouse events to the size bar divs
      apex.jQuery('div.apex_size_bar, div.apex_size_grip', gTextarea.parent()).mousedown(startResize);

      // The enclosing object uses 100% space, but the resizable object probably doesn't
      // -> we have to adjust the parent object to use the same width as the resizable object
      // Note: outerWidth is important because a textarea, ... has a scrollbar which isn't
      //       included in the width!
      gTextarea.parent().width(gTextarea.outerWidth());

      /**
       * Function called when the mouse button has been pressed in the size-bar div
       * */
      function startResize(pEvent)
      {
        gOffsetX = gTextarea.width()  - pEvent.pageX;
        gOffsetY = gTextarea.height() - pEvent.pageY;
        gTextarea.css('opacity', 0.25);
        apex.jQuery(document)
          .bind('mousemove.apex_startResize', function(pE){ return performResize(pE, (apex.jQuery(pEvent.currentTarget).css("cursor")=="se-resize"));})
          .bind('mouseup.apex_startResize', endResize);
        return false;
      }; // startResize

      /**
       * Function called when the mouse is moved while the button is pressed in
       * the size bar div
       * Parameter pSetWidth should only be set if the size bar has been selected
       * in the right corner of the size bar
       * */
      function performResize(pEvent, pSetWidth)
      {
        gTextarea.height
          ( Math.max(gMinHeight, gOffsetY + pEvent.pageY) + 'px'
          );
        if (pSetWidth)
        {
          gTextarea.width
            ( Math.max(gMinWidth, gOffsetX + pEvent.pageX) + 'px'
            );
          // The enclosing object uses 100% space, but the resizable object probably doesn't
          // -> we have to adjust the parent object to use the same width as the resizable object
          // Note: outerWidth is important because a textarea, ... has a scrollbar which isn't
          //       included in the width!
          gTextarea.parent().width(gTextarea.outerWidth());
        }
        return false;
      }; // performResize

      /**
       * Function called when the mouse button is released in the size bar div
       * this will de-register the events and restore the opacity of the textarea.
       * */
      function endResize(pEvent)
      {
        apex.jQuery(document)
          .unbind("mousemove.apex_startResize")
          .unbind("mouseup.apex_startResize");
        gTextarea.css('opacity', 1);
      }; // endResize
    }; // resizable

    /**
     * Function called when textarea gets focus or a character is typed to update
     * the character counter attached to the textarea.
     * */
    function charCount() {
      var lLength  = gTextarea.val().length,
          lPctFull = lLength / gOptions.maxChar * 100;

      // remove characters which are above the limit and highlight the field
      if (lLength >= gOptions.maxChar) {
        gTextarea.val(gTextarea.val().substr(0, gOptions.maxChar));
        gTextarea.css('color', 'red');
        gCounter.html(gOptions.maxChar);
      } else {
        gTextarea.css('color', 'black');
        gCounter.html(lLength);
      }
      // only show the counter area if something has been entered
      if (lLength > 0){
        gCountDiv.show();
      } else {
        gCountDiv.hide();
      }
      // show a color indicator for counter area
      if (lPctFull > 95) {
        gCountDiv.css('color', 'red');
      } else if (lPctFull >= 90) {
        gCountDiv.css('color', '#EAA914');
      } else {
        gCountDiv.css('color', 'black');
      }
    }; // charCount

    // make textarea resizable
    if (gOptions.isResizable) {
      resizable();
    }
    // add character counter
    if (gOptions.hasCharCounter) {
      var gCountDiv = apex.jQuery('#'+pId+'_CHAR_COUNT'),
          gCounter  = apex.jQuery('#'+pId+'_CHAR_COUNTER');
      gTextarea
        .change(charCount)
        .keyup(charCount)
        .focus(charCount);
      // Always recalculate count to avoid wrong value in FF (bug# 10011941)
      charCount();
    }

    // Let's hide/show the fieldset so that the resizebar is covered as well
    apex.widget.initPageItem(pId, {
      show: function() {
        gTextarea.closest('fieldset').show();
      },
      hide: function() {
        gTextarea.closest('fieldset').hide();
      } } );

  }, // textarea

  /**
   * @class resizable
   * Adds the necessary events to an object with appended size-bar to make it resizeable
   * */
  resizable: function(pId)
  {
    apex.widget.textarea(pId, {isResizable:true});
  }, // resizable

  /**
   * @class waitPopup
   * Shows a wait popup. If pContent is not provided the default wait indicator is displayed.
   * */
  waitPopup: function(pContent)
  {
    var lContent = (pContent?pContent:'<img id="img_progress" src="'+apex_img_dir+'processing3.gif" alt="">');

    // if the popup doesn't exist create it, otherwise just replace the content
    if (!$x('apex_wait_popup')) {
      apex.jQuery('<div id="apex_wait_popup" class="apex_wait_popup">'+
             lContent+
             '</div><div id="apex_wait_overlay" class="apex_wait_overlay"></div>').prependTo('body'); }
    else {
      apex.jQuery('#apex_wait_popup').html(lContent); }

    $x('apex_wait_overlay').style.visibility='visible';
    window.setTimeout('apex.jQuery("#apex_wait_popup").html(apex.jQuery("#apex_wait_popup").html());', 100);
  }, // waitPopup

  /**
   * @class regionDisplaySelector
   * Initializes the region display selector region type.
   * */
  regionDisplaySelector: function(pRegionDisplaySelectorRegion) {
    // store closures for our region display selector for better performance
    var lRegionDisplaySelector = apex.jQuery("#"+pRegionDisplaySelectorRegion+"_RDS"),
        lAllRegionLinks = apex.jQuery("a[href!=#SHOW_ALL]", lRegionDisplaySelector);

    // bind an event handler to all links in our region display selector
    apex.jQuery(lRegionDisplaySelector).delegate("a", "click", function(pEvent){
      var lClickedLink = this,
          lClickedRegionName = apex.jQuery(lClickedLink).attr("href");

      if (lClickedRegionName=="#SHOW_ALL") {
        // show all regions which are hidden
        lAllRegionLinks.each(function(){
          apex.jQuery(apex.jQuery(this).attr("href")+":hidden").show();
          });
      } else {
        // hide all visible regions
        lAllRegionLinks.each(function(){
          apex.jQuery(apex.jQuery(this).attr("href")+":visible").hide();
          });
        // and now show the clicked region
        apex.jQuery(lClickedRegionName).show();
      }
      // remove the currently set "selected" CSS class and add it to the LI of our clicked link
      apex.jQuery('.apex-rds-selected', lRegionDisplaySelector).removeClass("apex-rds-selected");
      apex.jQuery(lClickedLink).parent().addClass("apex-rds-selected");
      // don't fire the browser default link behavior so that the href is not displayed
      pEvent.preventDefault();
    });
  } // regionDisplaySelector

} // apex.widget

/*
 * #function initPageItem
 *   Given a page item name different options can be registered for a page item with the
 *   Application Express JavaScript framework. This is necessary to seamlessly integrate
 *   a plug-in item type with the built-in page item related JavaScript functions of
 *   Application Express.
 *   Possible callsbacks for pOptions are:
 *     getValue, setValue, enable, disable, show, hide, nullValue
 *
 *     Example:
 *     { getValue: function(){},
 *       setValue: function(){},
 *       nullValue: "%null%"
 *     }
 */
apex.widget.initPageItem = function (pName, pOptions) {
    apex.item(pName, pOptions);
};

/*
 * #function enableIcon
 *   Utility function to enable any icons descendant of $pContainer
 *   If passing pClickHandler to rebind the icon's click handler, the
 *   $pContainer must be the same as the element you wish to bind the
 *   handler to (eg the icon's wrapping anchor).
 */
apex.widget.enableIcon = function($pContainer, pHref, pClickHandler) {
    $pContainer
        .find('img')                // locate any images descendant of $pContainer
        .css({'opacity' : 1,
              'cursor'  : ''})      // set their opacity and remove cursor
        .parent('a')                // go to parent, which should be an anchor
        .attr('href', pHref);       // add the href
    // check if pClickHandler is passed, if so, bind it
    if (pClickHandler) {
        $pContainer.click(pClickHandler);      // rebind the click handler
    }
}; // enableIcon

/*
 * #function disableIcon
 *   Utility function to disable any icons descendant of $pContainer
 */
apex.widget.disableIcon = function($pContainer) {
    $pContainer
        .find('img')
        .css({'opacity' : 0.5,
              'cursor'  : 'default'})
        .parent('a')
        .removeAttr('href')
        .unbind('click');
}; // disableIcon


/**
 * @class autocomplete
   * Autocomplete textfield which shows a list of values based on the entered text.
   * Internally the jQuery autocomplete plug-in http://plugins.jquery.com/project/autocompletex
   * is used. See the plug-in docu for available options.
 * */
apex.widget.autocomplete = function(pSelector, pData, pOptions) {

  var gAutoComplete = null;

  // Triggers the "refresh" event of the autocomplete which actually does the AJAX call
  function _triggerRefresh() {
    gAutoComplete.trigger('apexrefresh');
  }; // _triggerRefresh

  // Clears the existing value
  function refresh() {
    // trigger the before refresh event
    gAutoComplete.trigger('apexbeforerefresh');

    // Clear the autocomplete field
    $s(gAutoComplete[0], "", "");

    // clear the auto complete select list
    gAutoComplete.flushCache();

    // trigger the after refresh event
    gAutoComplete.trigger('apexafterrefresh');
  }; // refresh

  // Returns the value which should be displayed
  function formatOneColumn(pItem) {
    var lValue = typeof pItem === "string"?pItem:pItem[0];
    if (lOptions.isEscaped) {
      return apex.util.escapeSc(lValue);
    } else {
      return lValue;
    }
  };

  function formatTwoColumn(pItem) {
    if (lOptions.isEscaped) {
      return apex.util.escapeSc(pItem.d);
    } else {
      return pItem.d;
    }
  };

  // Converts the JSON output into the format required by the autocomplete plug-in
  function parseOneColumn(pData) {
     if (!pData) { return [];}
     return apex.jQuery.map(pData, function(pRow) {
                                      return {data: pRow, value: pRow, result: pRow};
                                   }
                           );
  };

  function parseTwoColumn(pData) {
     return apex.jQuery.map(pData, function(pRow) {
                                      return {data: pRow, value: pRow.r, result: pRow.d};
                                   }
                           );
  };

  function formatResult(pValue) { return pValue; };

  function formatMatch(pValue) {
    return typeof pValue === "string"?pValue:pValue[0];
  };

  // Writes the not visible value into our hidden field
  function resultTwoColumn(pEvent, pItem) {
    $s(this.id+"_HIDDENVALUE", (pItem?pItem.r:""));
  };

  // Makes sure that our hidden field is set/cleared when necessary
  function changeTwoColumn(pEvent) {
    if (!this.value) {
      $s(this.id+"_HIDDENVALUE", "");
    } else {
      // if any value is allowed, make sure to write the value into the hidden
      // field, because the "result" event doesn't fire if it's not a value
      // from the list. The "result" event will overwrite the value afterwards
      // if fired for a list entry
      if (!pOptions.mustMatch) { $s(this.id + "_HIDDENVALUE", this.value); }
    }
  };

  // Reads additional data if lacy loading is active
  function retrieveData(pParameter, pSuccess) {
    // map the parameters of the autocomplete plug-in to the APEX syntax
    // the native callback of the plugin can't be used, because it adds parameters
    // to the URL which APEX/mod_plsql isn't able to handle.
    var lData = apex.jQuery.extend({
      x01: pParameter.q,
      x02: pParameter.limit,
      x03: pParameter.timestamp,
      p_request: "NATIVE="+pOptions.ajaxIdentifier,
      p_flow_id: $v('pFlowId'),
      p_flow_step_id: $v('pFlowStepId'),
      p_instance: $v('pInstance')
      }, pParameter);
    delete lData.q;
    delete lData.limit;
    delete lData.timestamp;

    // We only have to read data from the server if all our depending values are not null
    if (pOptions.optimizeRefresh) {
      var lNullFound = false;
      apex.jQuery(pOptions.dependingOnSelector+','+pOptions.pageItemsToSubmit).each(function(){
          if (apex.item(this).isEmpty()) {
            lNullFound = true;
            return false; // stop execution of the loop
          }
        });
      if (lNullFound) {
        return; // we are done
      }
    }

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(pOptions.dependingOnSelector+','+pOptions.pageItemsToSubmit).each(function(){
      var lIdx;
      if (lData.p_arg_names===undefined) {
        lData.p_arg_names  = [];
        lData.p_arg_values = [];
        lIdx = 0;
      } else {
        lIdx = lData.p_arg_names.length;
      }
      lData.p_arg_names [lIdx] = this.id;
      lData.p_arg_values[lIdx] = $v(this);
    });

    apex.jQuery.ajax({
      // try to leverage ajaxQueue plugin to abort previous requests
      mode: "abort",
      // limit abortion to this input
      port: "autocomplete",
      dataType: "json",
      type: "post",
      url: "wwv_flow.show",
      traditional: true,
      data: lData,
      success: pSuccess });
  };

  // Based on our custom settings, add addition properties to the autocomplete options
  var lOptions = apex.jQuery.extend({
    formatItem:  pOptions.useHiddenField?formatTwoColumn:formatOneColumn,
    formatResult:formatResult,
    formatMatch: formatMatch,
    parse:       pOptions.useHiddenField?parseTwoColumn :parseOneColumn,
    multiple:    pOptions.multipleSeparator?true:false,
    matchSubset: !pOptions.matchContains,
    isEscaped:   true
    }, pOptions);
  // clear our own attributes which are not used by the autocomplete plug-in
  delete lOptions.useHiddenField;
  delete lOptions.ajaxIdentifier;
  delete lOptions.dependingOnSelector;
  delete lOptions.optimizeRefresh;
  delete lOptions.pageItemsToSubmit;

  // initialize the autocomplete plug-in
  gAutoComplete = apex.jQuery(pSelector).autocomplete((pData)?pData:retrieveData, lOptions);

  // the hidden field option needs special treatment when a value is selected
  if (pOptions.useHiddenField) {
    gAutoComplete
      .result(resultTwoColumn)
      .change(changeTwoColumn);
    }

  // if it's a cascading select list we have to register change events for our masters
  if (pOptions.dependingOnSelector) {
    apex.jQuery(pOptions.dependingOnSelector).change(_triggerRefresh);
  }
  // register the refresh event which is triggered by triggerRefresh or a manual refresh
  gAutoComplete.bind("apexrefresh", refresh);

}; // autocomplete



/**
 * @class selectList
   * Select List field which is able to refresh itself with an AJAX call.
 * */
apex.widget.selectList = function(pSelector, pOptions) {

  // Default our options and store them with the "global" prefix, because it's
  // used by the different functions as closure
  var gOptions = apex.jQuery.extend({
                   dependingOnSelector:null,
                   optimizeRefresh:true,
                   pageItemsToSubmit:null,
                   optionAttributes:null,
                   nullValue:""
                   }, pOptions),
      gSelectList = apex.jQuery(pSelector);

  // Register apex.item callbacks
  gSelectList.each(function(){
    apex.widget.initPageItem(this.id, {
      nullValue:gOptions.nullValue });
    });

  // Clears the existing options
  function _clearList() {
    // remove everything except of the null value. If no null value is defined,
    // all options will be removed
    apex.jQuery('option[value!="'+gOptions.nullValue+'"]', gSelectList).remove();
  }; // _clearList

  // Called by the AJAX success callback and adds the entries stored in the
  // JSON structure: {"values":[{"r":"10","d":"SALES"},...], "default":"xxx"}
  function _addResult(pData) {
    // remove loading indicator
    gSelectList.next('.loading-indicator').remove();
    // check for errors first
    if (pData.error) {
      alert(pData.error);
      return false;
    }

    var lHtml="";
    // create an HTML string first and append it, that's faster.
    apex.jQuery.each(pData.values, function(){
      lHtml=lHtml+'<option value="'+this.r+'" '+gOptions.optionAttributes+'>'+this.d+'</option>';
      });
    gSelectList.append(lHtml)

    // Set the default value of the page item.
    // The change event is also needed by cascading LOVs so that they are refreshed with the
    // current selected value as well (bug# 9907473)
    $s(gSelectList[0], pData["default"]);

    // Also trigger the after refresh event
    gSelectList.trigger('apexafterrefresh');
  }; // _addResult

  // Clears the existing options and executes an AJAX call to get new values based
  // on the depending on fields
  function refresh() {

    _clearList();

    // trigger the before refresh event
    gSelectList.trigger('apexbeforerefresh');

    // We only have to refresh if all our depending values are not null
    if (gOptions.optimizeRefresh) {
      var lNullFound = false;
      apex.jQuery(gOptions.dependingOnSelector).each(function(){
          if (apex.item(this).isEmpty()) {
            lNullFound = true;
            return false; // stop execution of the loop
          }
        });
      if (lNullFound) {
        // Trigger the change event for the select list because the current value might has changed.
        // The change event is also needed by cascading LOVs so that they are refreshed with the
        // current selected value as well (bug# 9907473)
        // If the select list actually reads data, the change event is fired in the _addResult as soon as
        // a new value has been set (in case the LOV doesn't contain a null display entry)
        gSelectList.change();
        // trigger the after refresh event
        gSelectList.trigger('apexafterrefresh');
        return; // we are done
      }
    }

    // initialize the AJAX call parameters
    var lData = { p_request: "NATIVE="+gOptions.ajaxIdentifier,
                  p_flow_id: $v('pFlowId'),
                  p_flow_step_id: $v('pFlowStepId'),
                  p_instance: $v('pInstance')
                };

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(gOptions.dependingOnSelector+','+gOptions.pageItemsToSubmit).each(function(){
      var lIdx;
      if (lData.p_arg_names===undefined) {
        lData.p_arg_names  = [];
        lData.p_arg_values = [];
        lIdx = 0;
      } else {
        lIdx = lData.p_arg_names.length;
      }
      lData.p_arg_names [lIdx] = this.id;
      lData.p_arg_values[lIdx] = $v(this);
    });

    // add a loding indicator to the select list
    gSelectList.after('<span class="loading-indicator"></span>');

    // perform the AJAX call
    apex.jQuery.ajax({
      // try to leverage ajaxQueue plugin to abort previous requests
      mode: "abort",
      // limit abortion to this input
      port: "selectlist"+pSelector,
      dataType: "json",
      type: "post",
      url: "wwv_flow.show",
      traditional: true,
      data: lData,
      success: _addResult
      });
  }; // refresh

  // if it's a cascading select list we have to register apexbeforerefresh and change events for our masters
  if (gOptions.dependingOnSelector) {
    apex.jQuery(gOptions.dependingOnSelector)
      .bind('apexbeforerefresh', _clearList)
      .change(refresh);
  }
  // register the refresh event which is triggered by a manual refresh
  gSelectList.bind("apexrefresh", refresh);

}; // selectList




/**
 * @class popupLov
   * Popup LOV field which is able to refresh itself with an AJAX call.
 * */
apex.widget.popupLov = function(pSelector, pOptions) {

  // Default our options and store them with the "global" prefix, because it's
  // used by the different functions as closure
  var gOptions = apex.jQuery.extend({
                   dependingOnSelector:null,
                   optimizeRefresh:true,
                   pageItemsToSubmit:null,
                   nullValue:"",
                   filterWithValue:false,
                   windowParameters:null,
                   inputField:"NOT_ENTERABLE"
                   }, pOptions),
  gPopupLov = apex.jQuery(pSelector);
  // Boolean to control if popup is enterable (for popup lov) or non-enterable
  // (for popup key lov)
  var gEnterable = (gOptions.inputField == 'ENTERABLE');
      
  // Register apex.item callbacks
  apex.jQuery(pSelector).each(function(){

    apex.widget.initPageItem(this.id, {
      enable        : function() {
        // Enable the icon, set the context as the anchor for the icon
        apex.widget.enableIcon(apex.jQuery('#' + this.id + '_holder').find('a'), '#', _callPopup);
        // if input field is enterable, need to enable it
        if (gEnterable) {
          // enable both input and and popup icon
          gPopupLov
            .prop('disabled', false)            // enable LOV
            .removeClass('apex_disabled');      // remove disabled class
        } else {
            // if non-enterable, need to re-enable the associated hidden
            // element used to store the value POSTed
            apex.jQuery('#' + this.id + '_HIDDENVALUE')
              .prop('disabled', false);
        }
      },
      disable       : function() {
        // Disable the icon, set the context as the table row containing the lov element
        apex.widget.disableIcon(apex.jQuery(gPopupLov).closest('tr'));
        // if input field is enterable, need to disable it
        if (gEnterable) {
          // disable both input and and popup icon
          gPopupLov
            .prop('disabled', true)     // add disabled attribute
            .addClass('apex_disabled'); // add move disabled class
        } else {
            // if non-enterable, need to disable the associated hidden element
            // used to store the value POSTed
            apex.jQuery('#' + this.id + '_HIDDENVALUE')
              .prop('disabled', true);  // add disabled attribute
        }
      },
      show          : function() {
        // traverse up to the table row container, and show that
        apex.jQuery('#' + this.id).closest('tr').show();
      },
      hide          : function() {
        // traverse up to the table row container, and hide that
        apex.jQuery('#' + this.id).closest('tr').hide();
      },
      setValue      : function(pValue, pDisplayValue) {
        // if input is enterable (popup lov), then just set pValue
        if (gEnterable) {
          apex.jQuery('#' + this.id).val(pValue);
        } else {
          // popup key lovs store their value in a hidden field
          apex.jQuery('#' + this.id + '_HIDDENVALUE').val(pValue);
          // and their return value in the displayed field
          apex.jQuery('#' + this.id).val(pDisplayValue);
        }
      },
      getValue      : function() {
        var lReturn;
        // if input is enterable (popup lov), then get the displayed value
        if (gEnterable) {
          lReturn =  apex.jQuery('#' + this.id).val();
        } else {
          // popup key lovs store their value in a hidden field
          lReturn = apex.jQuery('#' + this.id + '_HIDDENVALUE').val();
        }
        return lReturn;
      },
      nullValue: gOptions.nullValue
    });
  });

  // Triggers the "refresh" event of the popup lov which actually does the AJAX call
  function _triggerRefresh() {
    gPopupLov.trigger('apexrefresh');
  }; // triggerRefresh

  // Clears the existing values from the popup lov fields and fires the before
  // and after refresh events
  function refresh() {
    // trigger the before refresh event
    gPopupLov.trigger('apexbeforerefresh');

    // remove everything except of the null value. If no null value is defined
    $s(gPopupLov.attr("id"), gOptions.nullValue, gOptions.nullValue);

    // trigger the after refresh event
    gPopupLov.trigger('apexafterrefresh');
    return; // we are done
  }; // refresh

  function _callPopup() {

    var lUrl = "wwv_flow.show" +
               "?p_flow_id=" + $v('pFlowId') +
               "&p_flow_step_id=" + $v('pFlowStepId') +
               "&p_instance=" + $v('pInstance') +
               "&p_request=NATIVE%3D" + gOptions.ajaxIdentifier;

    // add filter with the current value if popup lov is configured for that
    if (gOptions.filterWithValue) {
      lUrl = lUrl + '&x02=' + encodeURIComponent(gPopupLov.val());
    }

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(gOptions.dependingOnSelector+','+gOptions.pageItemsToSubmit).each(function(){
      lUrl = lUrl +
             '&p_arg_names=' + encodeURIComponent(this.id) +
             '&p_arg_values=' + encodeURIComponent($v(this));
    });

    var lWindow = open(lUrl, "winLovList", gOptions.windowParameters);
    if (lWindow.opener == null) {
      lWindow.opener = self;
    }
    lWindow.focus();

    return false;
  }; // _callPopup

  // register the click event for the icon anchor to call the popup lov dialog
  apex.jQuery(pSelector+"_fieldset a").click(_callPopup);

  // if it's a cascading popup lov we have to register change events for our masters
  if (gOptions.dependingOnSelector) {
    apex.jQuery(gOptions.dependingOnSelector).change(_triggerRefresh);
  }
  // register the refresh event which is triggered by triggerRefresh or a manual refresh
  gPopupLov.bind("apexrefresh", refresh);

  // if popup is non-enterable, register focus event handler to move focus to the
  // icon anchor
  if (!gEnterable) {
    gPopupLov.focus(function(){
      apex.jQuery(pSelector+"_fieldset a").focus();
    });      
  } 
}; // popupLov


/**
 * @class checkboxAndRadio
   * Checkbox/Radio field which is able to refresh itself with an AJAX call.
 * */
apex.widget.checkboxAndRadio = function(pSelector, pType, pOptions) {

  // Default our options and store them with the "global" prefix, because it's
  // used by the different functions as closure
  var gOptions = apex.jQuery.extend({
                   action:null,
                   dependingOnSelector:null,
                   optimizeRefresh:true,
                   pageItemsToSubmit:null,
                   numberOfColumns:1,
                   nullValue:"",
                   inputName:null,
                   attributes:null,
                   optionAttributes:null
                   }, pOptions),
      gFieldset = apex.jQuery(pSelector);

  // Register apex.item callbacks
  apex.jQuery(pSelector).each(function(){
    var lFieldset = this;
    
    apex.widget.initPageItem(this.id, {
      enable : function() {
        apex.jQuery(':' + pType, lFieldset)
          .prop('disabled', false)             // enable checkbox/radio
          .removeClass('apex_disabled_multi'); // remove the relevant disabled class
      },
      disable : function() {
        apex.jQuery(':' + pType, lFieldset)
          .prop('disabled', true)
          .addClass('apex_disabled_multi');
      },
      setValue : function(pValue) {
        var $lRadios    = apex.jQuery(':' + pType, lFieldset),
            lValueArray = [];
        // clear any checked values first
        $lRadios.prop('checked', false);
        // if pType is 'checkbox', pValue could be multiple values
        if (pType == 'checkbox') {
          lValueArray = apex.util.toArray(pValue);
        } else {
          lValueArray[0] = pValue;
        }
        // loop through lValue array
        for (var i=0; i < lValueArray.length; i++) {
          // filter all radio inputs if value equals an array value,
          // then add checked to filtered results
          $lRadios.filter('[value=' + lValueArray[i] + ']').prop('checked', true);
        }
      },
      getValue : function() {
        // get checked input value, in the context of the fieldset
        // note: can't use $lRadios here because this is a reference
        // to the initial state
        var lReturn, $lRadio;
        if (pType==='checkbox') {
          // checkbox will return an array
          lReturn = [];
          apex.jQuery(':checked', lFieldset).each(function(){
            lReturn[lReturn.length] = this.value;
          });
        } else {
          // radio group should return a single value
          $lRadio = apex.jQuery(pSelector+' :checked');
          if ($lRadio.length === 0) {
            // check if the length of the jQuery object is zero (nothing checked)
            // if so return an empty string.
            lReturn = "";
          } else {
            // otherwise return the value
            lReturn = $lRadio.val();
          }
        }
        return lReturn;
      },
      nullValue: gOptions.nullValue
    });
  });

  // Triggers the "refresh" event of the checkbox/radiogroup fieldset which actually does the AJAX call
  function _triggerRefresh() {
    gFieldset.trigger('apexrefresh');
  }; // triggerRefresh

  // Called by the AJAX success callback and adds the entries stored in the
  // JSON structure: {"values":[{"r":"10","d":"SALES"},...], "default":"xxx"}
  function _addResult(pData) {
    // remove loading indicator
    gFieldset.empty();
    // check for errors first
    if (pData.error) {
      alert(pData.error);
      return false;
    }

    /* ***
       *** Note: Synchronize the output with render_checkbox_and_radiogroup in wwv_flow_native_item.plb
       *** */
    var lHtml="";
    // create an HTML string first and append it, that's faster.
    // Also trigger the after refresh event

    // if we print multiple columns of checkboxes/radio, use a table for layout
    if (gOptions.numberOfColumns > 1) {
      lHtml=lHtml+'<table summary="" role="presentation" datatable=0 class="'+(pType==='checkbox'?'checkboxs':'radiogroup')+'">';
    }

    apex.jQuery.each(pData.values, function(pIdx){
      if (gOptions.numberOfColumns > 1) {
        if ((pIdx+1) % gOptions.numberOfColumns == 1) {
          if (pIdx > 0) { // javascript arrays start at index 0
            lHtml=lHtml+"</tr>";
          }
          lHtml=lHtml+"<tr>";
        }
        lHtml=lHtml+"<td>";
      }

      lHtml=lHtml+
            '<input type="'+pType+'" id="'+gFieldset.attr("id")+'_'+pIdx+'" '+
            (gOptions.inputName?'name="'+gOptions.inputName+'" ':"")+
            'value="'+this.r+'" '+
            gOptions.attributes+' />';

      // Only show label if available
      if (this.d) {
        if (gOptions.optionAttributes) {
          lHtml=lHtml+"<span "+gOptions.optionAttributes+">";
        }
        lHtml=lHtml+'<label for="'+gFieldset.attr("id")+'_'+pIdx+'">'+this.d+'</label>';
        if (gOptions.optionAttributes) {
          lHtml=lHtml+"</span>";
        }
      }

      if (gOptions.numberOfColumns > 1) {
        lHtml=lHtml+"</td>";
      } else {
        // only add the line break if it's not the last record
        if ((pData.length-1) != pIdx) {
          lHtml=lHtml+"<br />";
        }
      }
    });

    gFieldset.append(lHtml);

    $s(gFieldset[0], pData["default"]);

    gFieldset.trigger('apexafterrefresh');
  }; // addResult

  // Clears the existing options and executes an AJAX call to get new values based
  // on the depending on fields
  function refresh() {
    // trigger the before refresh event
    gFieldset.trigger('apexbeforerefresh');

    // remove everything within the fieldset
    gFieldset.empty();

    // We only have to refresh if all our depending values are not null
    if (gOptions.optimizeRefresh) {
      var lNullFound = false;
      apex.jQuery(gOptions.dependingOnSelector).each(function(){
          if (apex.item(this).isEmpty()) {
            lNullFound = true;
            return false; // stop execution of the loop
          }
        });
      if (lNullFound) {
        // trigger the change event for the checkbox/radio fieldset
        gFieldset.change();

        // trigger the after refresh event
        gFieldset.trigger('apexafterrefresh');
        return; // we are done
      }
    }

    // initialize the AJAX call parameters
    var lData = { p_request: "NATIVE="+gOptions.ajaxIdentifier,
                  p_flow_id: $v('pFlowId'),
                  p_flow_step_id: $v('pFlowStepId'),
                  p_instance: $v('pInstance')
                };

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(gOptions.dependingOnSelector+','+gOptions.pageItemsToSubmit).each(function(){
      var lIdx;
      if (lData.p_arg_names===undefined) {
        lData.p_arg_names  = [];
        lData.p_arg_values = [];
        lIdx = 0;
      } else {
        lIdx = lData.p_arg_names.length;
      }
      lData.p_arg_names [lIdx] = this.id;
      lData.p_arg_values[lIdx] = $v(this);
    });

    // add a loading indicator instead of the checkboxes/radio
    gFieldset.append('<span class="loading-indicator"></span>');

    // perform the AJAX call
    apex.jQuery.ajax({
      // try to leverage ajaxQueue plugin to abort previous requests
      mode: "abort",
      // limit abortion to this input
      port: "checkboxRadio"+pSelector,
      dataType: "json",
      type: "post",
      url: "wwv_flow.show",
      traditional: true,
      data: lData,
      success: _addResult
      });
  }; // refresh

  // if it's a cascading checkbox/radio we have to register change events for our masters
  if (gOptions.dependingOnSelector) {
    apex.jQuery(gOptions.dependingOnSelector).change(_triggerRefresh);
  }
  // register the refresh event which is triggered by triggerRefresh or a manual refresh
  gFieldset.bind("apexrefresh", refresh);

  // register click events on the single radio input elements if an action has been defined
  if (gOptions.action === "REDIRECT_SET_VALUE") {
    gFieldset.find('input').click(function(){
        location.href="f?p="+$v("pFlowId")+":"+$v("pFlowStepId")+":"+$v("pInstance")+"::"+$v("pdebug")+"::"+gFieldset.attr("id")+":"+$v(gFieldset.attr("id"));
      });
  } else if (gOptions.action === "SUBMIT") {
    gFieldset.find('input').click(function(){apex.submit(gFieldset.attr("id"));});
  }
  
  // register focus handling, so when the fieldset receives focus, we move
  // focus to the first input
  gFieldset.focus(function(){
    apex.util.setFocusFirstDescendant(this);
  });  
}; // checkboxAndRadio


/**
 * @class checkbox
   * Checkbox field which is able to refresh itself with an AJAX call.
 * */
apex.widget.checkbox = function(pSelector, pOptions) {
  apex.widget.checkboxAndRadio(pSelector, 'checkbox', pOptions);

}; // checkbox


/**
 * @class radioGroup
   * Radio Group field which is able to refresh itself with an AJAX call.
 * */
apex.widget.radioGroup = function(pSelector, pOptions) {
  apex.widget.checkboxAndRadio(pSelector, 'radio', pOptions);
}; // radioGroup


/**
 * @class shuttle
   * Shuttle control which is able to refresh itself with an AJAX call.
 * */
apex.widget.shuttle = function(pSelector, pOptions) {

  // Default our options and store them with the "global" prefix, because it's
  // used by the different functions as closure
  var gOptions = apex.jQuery.extend({
                   dependingOnSelector:null,
                   optimizeRefresh:true,
                   pageItemsToSubmit:null,
                   optionAttributes:null
                   }, pOptions);

  // get shuttle controls
  var gShuttle           = apex.jQuery(pSelector),
      gShuttleListLeft   = apex.jQuery(pSelector+'_LEFT'),
      gShuttleListRight  = apex.jQuery(pSelector+'_RIGHT'),
      gSavedOptionsLeft  = apex.jQuery('option', gShuttleListLeft),
      gSavedOptionsRight = apex.jQuery('option', gShuttleListRight);

  // Register apex.item callbacks
  apex.jQuery(pSelector).each(function(){
   apex.widget.initPageItem(this.id, {
        enable      : function() {
            var lFieldset;
            lFieldset = $x(this.id);
            // rebind click handlers
            _bindIconClickHandlers();
            // call enableIcon to enable all the icons, don't pass any value for pClickHandler
            // as these have been re-bound already
            apex.widget.enableIcon(apex.jQuery('td.shuttleControl a', lFieldset).add('td.shuttleSort2 a', lFieldset), '#');
            // enable selects
            apex.jQuery('select', lFieldset)
              .prop('disabled', false)
              .removeClass('apex_disabled');
        },
        disable     : function() {
            var lFieldset;
            lFieldset = $x(this.id);
            // deselect all options first
            apex.jQuery('option:selected', lFieldset).attr('selected', false);
            // call disableIcon to disable all the icons
            apex.widget.disableIcon(apex.jQuery('td.shuttleControl a', lFieldset).add('td.shuttleSort2 a', lFieldset));
            // disable selects
            apex.jQuery('select', lFieldset)
              .prop('disabled', true)
              .addClass('apex_disabled');
        },
        setValue    : function(pValue) {
            var lValueArray;
            // remove all values from right
            _removeAll();
            // create array from pValue
            lValueArray = apex.util.toArray(pValue);
            // iterate over values to set, compare with left hand values, and if matched
            // move to right, if no match don't add to right
            for (var i=0; i < lValueArray.length; i++) {
                apex.jQuery('option', gShuttleListLeft[0]).each(function(){
                    if (this.value === lValueArray[i]) {
                      // move the found options from the left list into the right list
                      apex.jQuery(this)
                        .appendTo(gShuttleListRight).attr('selected', true);
                      // stop execution of 'each', to get to next i iterator faster
                      return false;
                    }
                });
            }
        },
        getValue    : function() {
            var lReturn = [];
            apex.jQuery('option', gShuttleListRight[0]).each(function(){
                lReturn[lReturn.length] = this.value;
            });
            return lReturn;
        }
    });
  });

  // Triggers the "refresh" event of the select list which actually does the AJAX call
  function _triggerRefresh() {
    gShuttle.trigger('apexrefresh');
  }; // _triggerRefresh

  // Called by the AJAX success callback and adds the entries stored in the
  // JSON structure: {"values":[{"r":"10","d":"SALES"},...], "default":"xxx"}
  function _addResult(pData) {

    // remove loading indicator
    apex.jQuery('.loading-indicator', gShuttle).remove();
    // check for errors first
    if (pData.error) {
      alert(pData.error);
      return false;
    }

    var lHtml="";
    // create an HTML string first and append it to the left select list, that's faster.
    apex.jQuery.each(pData.values, function(){
      lHtml=lHtml+'<option value="'+this.r+'" '+gOptions.optionAttributes+'>'+this.d+'</option>';
      });
    // add the options and store them for reset
    gSavedOptionsLeft = gShuttleListLeft
                          .append(lHtml)
                          .children();

    $s(gShuttle[0], pData["default"]);

    // save new saved options based on the default value
    gSavedOptionsLeft  = apex.jQuery('option', gShuttleListLeft);
    gSavedOptionsRight = apex.jQuery('option', gShuttleListRight);

    // trigger the after refresh event for both lists (fieldset)
    gShuttle.trigger('apexafterrefresh');
  }; // _addResult

  // Clears the existing options and executes an AJAX call to get new values based
  // on the depending on fields
  function refresh() {
    // trigger the before refresh event for the left and right side (fieldset)
    gShuttle.trigger('apexbeforerefresh');

    // remove everything from both lists and store the empty options for reset
    gSavedOptionsLeft  = gShuttleListLeft.empty().children();
    gSavedOptionsRight = gShuttleListRight.empty().children();

    // We only have to refresh if all our depending values are not null
    if (gOptions.optimizeRefresh) {
      var lNullFound = false;
      apex.jQuery(gOptions.dependingOnSelector).each(function(){
          if (apex.item(this).isEmpty()) {
            lNullFound = true;
            return false; // stop execution of the loop
          }
        });
      if (lNullFound) {
        // trigger the change event for the shuttle
        gShuttle.change();

        // trigger the after refresh for both lists (fieldset)
        gShuttle.trigger('apexafterrefresh');
        return; // we are done
      }
    }

    // initialize the AJAX call parameters
    var lData = { p_request: "NATIVE="+gOptions.ajaxIdentifier,
                  p_flow_id: $v('pFlowId'),
                  p_flow_step_id: $v('pFlowStepId'),
                  p_instance: $v('pInstance')
                };

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(gOptions.dependingOnSelector+','+gOptions.pageItemsToSubmit).each(function(){
      var lIdx;
      if (lData.p_arg_names===undefined) {
        lData.p_arg_names  = [];
        lData.p_arg_values = [];
        lIdx = 0;
      } else {
        lIdx = lData.p_arg_names.length;
      }
      lData.p_arg_names [lIdx] = this.id;
      lData.p_arg_values[lIdx] = $v(this);
    });

    // add a loding indicator to the shuttle.
    // Note: the load indicator will not show up if the move controls are not there. Putting them
    //       somewhere else would result in a jumping layout
    apex.jQuery('td.shuttleControl', gShuttle).prepend('<span class="loading-indicator"></span>');

    // perform the AJAX call
    apex.jQuery.ajax({
      // try to leverage ajaxQueue plugin to abort previous requests
      mode: "abort",
      // limit abortion to this input
      port: "shuttle"+pSelector,
      dataType: "json",
      type: "post",
      url: "wwv_flow.show",
      traditional: true,
      data: lData,
      success: _addResult
      });
  }; // refresh

  function _reset() {
    // restore the original left and right list
    gShuttleListLeft
      .empty()
      .append(gSavedOptionsLeft)
      .children() // options
      .attr('selected', false);
    gShuttleListRight
      .empty()
      .append(gSavedOptionsRight)
      .children() // options
      .attr('selected', false);
    // trigger the change event for the shuttle
    gShuttle.change();
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _reset

  function _move(pEvent, pAll) {
    var $OptionsToMove = apex.jQuery('option'+(pAll?'':':selected'), gShuttleListLeft);
    // deselect everyting on the right side first
    apex.jQuery('option:selected', gShuttleListRight).attr('selected', false);
    // if there are options to move, move them and trigger change event
    if ($OptionsToMove.length) {
      // move the selected options from the left list into the right list
      $OptionsToMove
        .appendTo(gShuttleListRight).attr('selected', true);
      // trigger the change event for the shuttle
      gShuttle.change();
    }
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _move

  function _moveAll() {
    _move(null, true);
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _moveAll

  function _remove(pEvent, pAll) {
    var $OptionsToRemove = apex.jQuery('option'+(pAll?'':':selected'), gShuttleListRight);
    // deselect everyting on the left side first
    apex.jQuery('option:selected', gShuttleListLeft).attr('selected', false);
    // if there are options to remove, remove them and trigger change event
    if ($OptionsToRemove.length) {
      // move the selected options from the right list into the left list
      $OptionsToRemove
        .appendTo(gShuttleListLeft).attr('selected', true);
      // trigger the change event for the shuttle
      gShuttle.change();
    }
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _remove

  function _removeAll() {
    _remove(null, true);
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _removeAll

  function _moveTop() {
    // move the selected options in the right list to the top and select them
    apex.jQuery('option:selected', gShuttleListRight)
      .prependTo(gShuttleListRight).attr('selected', true);
    // trigger our change order event for the shuttle
    gShuttle.trigger('shuttlechangeorder');
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _moveTop

  function _moveUp() {
    apex.jQuery('option:selected', gShuttleListRight).each(function(){
      var lPrevOption = apex.jQuery(this).prev();
      // don't do anything if the selected is already at the top or selected
      if (lPrevOption.length===0 || lPrevOption.attr('selected')) {
        return;
      }
      // move the option before the previous one and select it again
      apex.jQuery(this).insertBefore(lPrevOption).attr('selected', true);
     });

    // trigger our change order event for the shuttle
    gShuttle.trigger('shuttlechangeorder');
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _moveUp

  function _moveDown() {
    apex.jQuery('option:selected', gShuttleListRight).each(function(){
      var lNextOption = apex.jQuery(this).next();
      // don't do anything if the selected is already at the bottom or selected
      if (lNextOption.length===0 || lNextOption.attr('selected')) {
        return;
      }
      // move the option before the previous one and select it again
      apex.jQuery(this).insertAfter(lNextOption).attr('selected', true);
     });

    // trigger our change order event for the shuttle
    gShuttle.trigger('shuttlechangeorder');
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _moveDown

  function _moveBottom() {
    // move the selected options in the right list to the bottom and select them
    apex.jQuery('option:selected', gShuttleListRight)
      .appendTo(gShuttleListRight).attr('selected', true);
    // trigger our change order event for the shuttle
    gShuttle.trigger('shuttlechangeorder');
    // return false, so event handler doesn't execute default click of anchor
    return false;
  }; // _moveBottom

  function _stopEvent(pEvent) {
    pEvent.stopImmediatePropagation();
  }; // _stopEvent

  function _bindIconClickHandlers() {
    // register control events
    apex.jQuery(pSelector+"_RESET").click(_reset);
    apex.jQuery(pSelector+"_MOVE").click(_move);
    apex.jQuery(pSelector+"_MOVE_ALL").click(_moveAll);
    apex.jQuery(pSelector+"_REMOVE").click(_remove);
    apex.jQuery(pSelector+"_REMOVE_ALL").click(_removeAll);

    apex.jQuery(pSelector+"_TOP").click(_moveTop);
    apex.jQuery(pSelector+"_UP").click(_moveUp);
    apex.jQuery(pSelector+"_DOWN").click(_moveDown);
    apex.jQuery(pSelector+"_BOTTOM").click(_moveBottom);
  }

  // register our items so that it gets selected when the page is submitted
  // this is necessary, otherwise the browser wouldn't pick up the values
  if(!window.flowSelectArray){
    window.flowSelectArray = [];
  }
  window.flowSelectArray[window.flowSelectArray.length] = gShuttleListRight.attr("id");

  // if it's a cascading select list we have to register change events for our masters
  if (gOptions.dependingOnSelector) {
    apex.jQuery(gOptions.dependingOnSelector).change(_triggerRefresh);
  }

  // register the refresh event which is triggered by triggerRefresh or a manual refresh
  gShuttle.bind("apexrefresh", refresh);

  // don't fire change events for the left side and right side, otherwise the change event would fire
  // as soon as an entry is selected in the list, but that's not what we want. The change event should
  // only fire if something is moved or reordered
  gShuttleListLeft.change(_stopEvent);
  gShuttleListRight.change(_stopEvent);

  // register the double click events
  apex.jQuery(gShuttleListLeft).dblclick(_move);
  apex.jQuery(gShuttleListRight).dblclick(_remove);

  _bindIconClickHandlers();
  
  // register focus handling, so when the fieldset receives focus, we move
  // focus to the first select list in the shuttle
  gShuttle.focus(function(){
    apex.util.setFocusFirstDescendant(this);
  });

}; // shuttle


/**
 * @class listManager
   * List Manager widget which is able to refresh itself with an AJAX call.
 * */
apex.widget.listManager = function(pSelector, pOptions) {

  // Default our options and store them with the "global" prefix, because it's
  // used by the different functions as closure
  var gOptions = apex.jQuery.extend({
                   dependingOnSelector:null,
                   optimizeRefresh:true,
                   pageItemsToSubmit:null,
                   filterWithValue:false,
                   windowParameters:null
                   }, pOptions),
      gListManager = apex.jQuery(pSelector);

  apex.jQuery(pSelector).each(function(){
    // register callbacks
   apex.widget.initPageItem(this.id, {
        enable      : function() {
            // store fieldset dom element that contains all the list manager's elements
            var lFieldset;
            lFieldset = $x(this.id + '_fieldset');

            // enable all the input elements
            apex.jQuery(':input', lFieldset)
              .prop('disabled', false)          // enable all input elements in the fieldset
              .filter('[type!=button]')         // filter out buttons
              .removeClass('apex_disabled');    // and remove class from non buttons

            // register the click event for the icon anchor to call the popup lov dialog
            registerIconEvent();

            // enable the icon, don't pass a value for pClickHandler as this has been
            // rebound via registerIconEvent
            apex.widget.enableIcon(apex.jQuery(lFieldset), '#');

        },
        disable     : function() {
            // store fieldset dom element that contains all the list manager's elements
            var lFieldset;
            lFieldset = $x(this.id + '_fieldset');

            // deselect all options first
            apex.jQuery('option:selected', $x(this.id)).attr('selected', false);

            // disable all the input elements
            apex.jQuery(':input', lFieldset)
              .prop('disabled', true)           // disble all input elements in the fieldset
              .filter('[type!=button]')         // filter out buttons
              .addClass('apex_disabled');       // and add class to non buttons

            // disable the icon
            apex.widget.disableIcon(apex.jQuery(lFieldset));

        },
        hide        : function() {
            apex.jQuery('#' + this.id + '_fieldset').hide();
        },
        show        : function() {
            apex.jQuery('#' + this.id + '_fieldset').show();
        },
        setValue    : function(pValue) {
            // only proceed with set if pValue is not undefined
            if (typeof(pValue) !== 'undefined'){
                var lValueArray, lHtml;
                lValueArray = [];
                // set new value, we don't check if value exists here as the existing list manager
                // allows any value to be added to the list
                // create array from pValue
                lValueArray = apex.util.toArray(pValue);
                // loop through lValue array and build new options html string
                apex.jQuery.each(lValueArray, function(key, value)
                {
                    lHtml += '<option value="' + value + '">' + value + '</option>';
                });
                gListManager                // select list manager
                    .find('option')             // find options
                        .remove()                   // remove them
                        .end()                      // end option find
                    .append(lHtml);             // append new options
            }
        },
        getValue    : function() {
            var lReturn = [];
            // iterate over list manager options and populate array with values
            apex.jQuery('option', gListManager[0]).each(function(){
                lReturn[lReturn.length] = this.value;
            });
            return lReturn;
        }
    });
  });




  // Triggers the "refresh" event of the list manager which actually does the AJAX call
  function _triggerRefresh() {
    gListManager.trigger('apexrefresh');
  }; // triggerRefresh

  // Clears the existing values from the list manager fields and fires the before
  // and after refresh events
  function refresh() {
    // trigger the before refresh event
    gListManager.trigger('apexbeforerefresh');

    // remove everything
    apex.jQuery(pSelector+"_ADD").val("");
    apex.jQuery('option', gListManager).remove();
    gListManager.change();

    // trigger the after refresh event
    gListManager.trigger('apexafterrefresh');
    return; // we are done
  }; // refresh

  function _callPopup() {

    var lUrl = "wwv_flow.show" +
               "?p_flow_id=" + $v('pFlowId') +
               "&p_flow_step_id=" + $v('pFlowStepId') +
               "&p_instance=" + $v('pInstance') +
               "&p_request=NATIVE%3D" + gOptions.ajaxIdentifier;

    // add filter with the current value if popup lov is configured for that
    if (gOptions.filterWithValue) {
      lUrl = lUrl + '&x02=' + encodeURIComponent(apex.jQuery(pSelector+"_ADD").val());
    }

    // add all page items we are depending on and the one we always have to submit to the AJAX call
    apex.jQuery(gOptions.dependingOnSelector+','+gOptions.pageItemsToSubmit).each(function(){
      lUrl = lUrl +
             '&p_arg_names=' + encodeURIComponent(this.id) +
             '&p_arg_values=' + encodeURIComponent($v(this));
    });

    var lWindow = open(lUrl, "winLovList", gOptions.windowParameters);
    if (lWindow.opener == null) {
      lWindow.opener = self;
    }
    lWindow.focus();

    return false;
  }; // _callPopup

  function registerIconEvent() {
    // register the click event for the icon anchor to call the popup lov dialog
    apex.jQuery(pSelector+"_ADD_fieldset a").click(_callPopup);
  }; //registerIconEvent

  // register the click event for the icon anchor to call the popup lov dialog
  registerIconEvent();

  // if it's a cascading list manager we have to register change events for our masters
  if (gOptions.dependingOnSelector) {
    apex.jQuery(gOptions.dependingOnSelector).change(_triggerRefresh);
  }
  // register the refresh event which is triggered by triggerRefresh or a manual refresh
  gListManager.bind("apexrefresh", refresh);

  // register our items so that it gets selected when the page is submitted
  // this is necessary, otherwise the browser wouldn't pick up the values
  if(!window.flowSelectArray){
    window.flowSelectArray = [];
  }
  window.flowSelectArray[window.flowSelectArray.length] = gListManager.attr("id");

}; // listManager


/**
 * @class ckeditor3
   * Rich Text Editor.
   * Internally the CKEditor http://www.ckeditor.com is used.
   * See the CKEditor documentation for available options.
 * */
apex.widget.ckeditor3 = function(pSelector, pOptions) {

  // Based on our custom settings, add addition properties to the autocomplete options
  var lOptions = apex.jQuery.extend({
                   toolbar: "Basic",
                   toolbarStartupExpanded: true,
                   disableNativeSpellChecker: false,
                   "menu_groups": "clipboard,tablecell,tablecellproperties,tablerow,tablecolumn,table,anchor,link,image,flash"
                   }, pOptions);

  // Get min width for the HTML Editor
  var lMinWidth = 0;
  if (lOptions.toolbar==="Basic") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?185:205;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?181:201;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?175:195;
    }
  } else if (lOptions.toolbar==="Intermediate") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?240:260;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?235:255;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?230:250;
    }
  } else if (lOptions.toolbar==="Full") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?530:530;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?590:605;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?575:595;
    }
  }

  // Get editor padding
  var lEditorPadding = (lOptions.skin==="kama"?25:20);

  // We don't want to show all toolbar entries of basic and full
  if (lOptions.toolbar==="Basic") {
    lOptions.toolbar = [['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink']];
  } else if (lOptions.toolbar==="Intermediate") {
    lOptions.toolbar = [
                 ['Cut','Copy','Paste','-','Bold', 'Italic','Underline', '-', 'NumberedList', 'BulletedList','-','Outdent','Indent', '-', 'Link', 'Unlink'],
                         '/',
             ['Format','Font','FontSize','TextColor','-','JustifyLeft','JustifyCenter','JustifyRight']
                   ];
  } else if (lOptions.toolbar==="Full") {
    lOptions.toolbar = [
                         ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','Preview'],
                         ['Templates'],
                         ['Link','Unlink','Anchor'],
                         ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
                         '/',
                         ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                         ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                         ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                         ['TextColor','BGColor'],
                         ['ShowBlocks'],
                         '/',
                         ['Styles','Format','Font','FontSize'],
                         ['Maximize', 'Source']
                       ];
  }

  // No user will hide the toolbar if it's already displayed at startup
  if (lOptions.toolbarStartupExpanded) {
    lOptions.toolbarCanCollapse = false;
  }

  // Instanciate the CKeditor
  apex.jQuery(pSelector).each (function() {
    var lFinalOptions = lOptions;
    // calculate the editor size depending on the textarea settings
    lFinalOptions.height = (this.rows*15)+lEditorPadding;
    lFinalOptions.width  = (this.cols*9.5 < lMinWidth)?lMinWidth:this.cols*9.5;
    lFinalOptions.resize_minHeight = lFinalOptions.height;
    lFinalOptions.resize_minWidth  = lFinalOptions.width;

    CKEDITOR.replace(this.id, lFinalOptions);

    // Register apex.item callbacks
   apex.widget.initPageItem(this.id, {
        enable      : function() {
            alert('Enable not supported.');
        },
        disable     : function() {
            alert('Disable not supported.');
        },
        show        : function() {
            apex.jQuery('#cke_' + this.id).show();
        },
        hide        : function(pDuration) {
            apex.jQuery('#cke_' + this.id).hide();
        },
        setValue    : function(pValue) {
            var oEditor = CKEDITOR.instances[this.id];
            oEditor.setData(pValue);
        },
        getValue    : function() {
            var oEditor = CKEDITOR.instances[this.id];
            return oEditor.getData();
        }
    });
  });

  // register focus handling, so when the non-displayed textarea of the CKEditor
  // receives focus, focus is moved to the editor.
  apex.jQuery(pSelector).focus(function(){
    var oEditor = CKEDITOR.instances[this.id];
    oEditor.focus();
  });  

}; // ckeditor3


/**
 * @class datepicker
   * Allows to pick date from the selector for any date based items.
   * Internally uses the jQuery datepicker plug-in
   * is used. See the plug-in docu for available options.
 * */
apex.widget.datepicker = function(pSelector, pOptions, pFormat, pLocale) {
  // initialize the Date Picker plug-in

  /*function setDefaults(pSelector,pLocale) {
      var lDatePicker = apex.jQuery(pSelector).datepicker(apex.jQuery.datepicker.regional[pLocale]);
  }*/

  var lOnSelectCallBack = null;
  if (!pOptions.showTime || (pOptions.showTime && pOptions.showOn == 'inline')) { //sathikum added for inline with time
      lOnSelectCallBack = function(dateText, inst) {
    if (inst.inline) {
        var altField = inst.settings.altField;
        if (altField.indexOf('#') == 0 ) altField = altField.substr(1,altField.length) ; //remove an extra # used by jQuery
        if (altField) $s(altField, dateText);
    } else
        $s(inst.id, dateText);
     };
  }
  var lLang = pLocale ;
  var lOptions = apex.jQuery.extend({dateFormat: pFormat,duration: '', constrainInput: false,onSelect: lOnSelectCallBack, locale: lLang},pOptions);
  var lLocale = apex.jQuery.datepicker.regional[pLocale];
  delete lLocale.maxDate;
  delete lLocale.minDate;
  delete lLocale.defaultDate;
  delete lLocale.dateFormat;
  delete lLocale.yearRange;
  delete lLocale.numberOfMonths;
  delete lLocale.altField;
  lOptions = apex.jQuery.extend(lLocale,lOptions);

  var lDatePicker = apex.jQuery(pSelector).datepicker(lOptions);

  // Register apex.item callbacks
  apex.jQuery(pSelector).each(function(){
   apex.widget.initPageItem(this.id, {
      enable      : function() {
        apex.jQuery('#' + this.id)
          .datepicker('enable')                     // call native jQuery UI enable
          .removeClass('apex_disabled');            // remove disabled class
      },
      disable   : function() {
        apex.jQuery('#' + this.id)
          .datepicker('disable')                  // call native jQuery UI disable
          .addClass('apex_disabled');             // add disabled class to ensure value is not POSTed
      },
      show      : function() {
        apex.jQuery('#' + this.id).parent().children().show();
      },
      hide      : function() {
        apex.jQuery('#' + this.id).parent().children().hide();
      }
    });
  });

}; // datepicker


/**
 * @class colorpicker
   * Allows to use a color picker dialog to pick a color.
   * Internally uses the jQuery colorpicker plug-in http://www.eyecon.ro/colorpicker/
 * */
apex.widget.colorpicker = function(pSelector, pOptions) {
  apex.jQuery(pSelector).each(function() {
    var lColorPicker = apex.jQuery(this).ColorPicker({
                         eventName:    "xxx", // don't fire on the default click event, we have our own icon
                         onSubmit:     function(pHsb, pHex, pRgb, pElement) {
                                         $s(pElement, '#'+pHex.toUpperCase());
                                         apex.jQuery(pElement).ColorPickerHide();
                                       },
                         onBeforeShow: function() {
                                         apex.jQuery(this).ColorPickerSetColor(this.value);
                                       },
                         onShow:       function(pElement) {
                                         apex.jQuery(pElement).fadeIn("fast");
                                         return false;
                                       },
                         onHide:       function(pElement) {
                                         apex.jQuery(pElement).fadeOut("fast");
                                         return false;
                                       }
                         }),
        lColorPickerFieldset = apex.jQuery('#'+this.id+'_fieldset');

    lColorPicker
      .bind('keyup',  function(){lColorPicker.ColorPickerSetColor(this.value);})
      .bind('blur',   function(){lColorPicker.ColorPickerHide();})
      .bind('change', function(){
                        this.value = this.value.toUpperCase();
                        apex.jQuery("#"+this.id+'_PREVIEW').css("background", this.value);
                      });

    // clicking on our color picker icon should open the dialog
    apex.jQuery('#'+this.id+'_PICKER').click(function(pEvent){
      lColorPicker.ColorPickerShow();
      pEvent.preventDefault(); // otherwise the browser would jump to the top of the document because of the #
    });

    // show the current entered color in our preview icon
    apex.jQuery("#"+this.id+'_PREVIEW').css("background", this.value);

    // register item callbacks
    apex.widget.initPageItem(this.id, {
      enable    : function() {
        if (lColorPicker.prop('disabled') === true) {
          lColorPicker
            .prop('disabled', false)
            .removeClass('apex_disabled');
          // enable color picker icons
          // bind click event handler to popup icon
          apex.jQuery('#'+this.id+'_PICKER').click(function(pEvent){
            lColorPicker.ColorPickerShow();
            pEvent.preventDefault(); // otherwise the browser would jump to the top of the document because of the #
          });
          // do other enabling on icons
          apex.widget.enableIcon(lColorPickerFieldset, '#');
        }
      },
      disable   : function() {
        if (lColorPicker.prop('disabled') === false) {
          lColorPicker
            .prop('disabled', true)
            .addClass('apex_disabled');
          // disable color picker icons
          apex.widget.disableIcon(lColorPickerFieldset);
        }
      },
      show      : function() {
        lColorPickerFieldset.show();
      },
      hide      : function() {
        lColorPickerFieldset.hide();
      }
    });
  });
};


/**
 * @class datepickerClassic
   * Allows to pick date from the selector for any date based items.
   * This is the classic, non jQuery-UI datepicker
 * */
apex.widget.datepickerClassic = function(pSelector, pOptions) {

  // Register apex.item callbacks
  apex.jQuery(pSelector).each(function(){
    // Store jQuery object containing the date picker icon link element
    var $lAnchor = apex.jQuery('#' + this.id + '_IMG').parent('a');
   apex.widget.initPageItem(this.id, {
      enable      : function() {
        var lHref;
        apex.jQuery('#' + this.id)
          .prop('disabled', false)
          .removeClass('apex_disabled');
          // If old_href data is defined, set the current href to it. Otherwise just default
          // to the current href.
          // This is set if the date picker has previously been disabled
          lHref = $nvl($lAnchor.data('old_href'), $lAnchor.attr('href'));
          // enbable date picker icon
          apex.widget.enableIcon($lAnchor, lHref);
      },
      disable   : function() {
        apex.jQuery('#' + this.id)
          .prop('disabled', true)
          .addClass('apex_disabled');
          // Set old_href data attribute to be the current href, used when enabling
          $lAnchor
            .data('old_href', $lAnchor.attr('href'));
            // disable date picker icon
            apex.widget.disableIcon($lAnchor);
      },
      show      : function() {
        // traverse up to the table row container, and show that
        apex.jQuery('#' + this.id).closest('tr').show();
      },
      hide      : function() {
        // traverse up to the table row container, and hide that
        apex.jQuery('#' + this.id).closest('tr').hide();
      }
    });
  });
}; // datepickerClassic


/**
 * @class tree
   *
   * Uses the jQuery jsTree for tree regions
 * */
/**
 * Namespace for tree widget
 **/
apex.widget.tree = {};

(function($){

  /**
   * Namespace for Tree
   **/

  $.cTreeTypes = {
    "default":{
      clickable:true,
      renameable:false,
      deletable:false,
      creatable:true,
      draggable:false,
      copyable:false,
      editable:false,
      max_children:-1,
      max_depth:0,
      valid_children:"none",
      icon:{
        image:false,
        position:false}
      }
    };

  /**
   * xxx
   **/
  $.init = function(pTreeId, pTypes, pStaticData, pTreeTemplate, pStartNodeId, pTreeAction, pSelectedNodeId) {

    if ($v('pScreenReaderMode') === 'YES') {

        // in screen reader mode we just generate a hierarchical unordered list
        var lUnorderedList = '';

        function addNodes(pNodeList) {

          if (pNodeList.length > 0) {
            lUnorderedList += '<ul>';

            // process all tree nodes in the array
            for (var i=0;i<pNodeList.length;i++) {
              lUnorderedList += '<li>';

              // do we have a link for this tree node?
              if (pNodeList[i].data.attributes.href) {
                lUnorderedList += '<a href="'+pNodeList[i].data.attributes.href+'"'+
                                  (pNodeList[i].data.attributes.tooltip?' title="'+pNodeList[i].data.attributes.tooltip+'"':"")+
                                  '>'+pNodeList[i].data.title+'</a>';
              } else {
                lUnorderedList += pNodeList[i].data.title;
              }

              // if the node has child nodes, create an unordered list for them as well
              if (pNodeList[i].children) {
                addNodes(pNodeList[i].children);
              }

              lUnorderedList += '</li>';
            } // for
            lUnorderedList += '</ul>';
          }
        }; // addNodes

        // call it for the root nodes
        addNodes(pStaticData);

        // add our hierarchical unordered list to the tree div
        apex.jQuery("#"+pTreeId).append(lUnorderedList);

    } else {
        // use jsTree to render the tree
        var lTree = apex.jQuery("#"+pTreeId).tree({
          data:{
            type:"json",
            async:true,
            opts:{
              "static":pStaticData,
              isTreeLoaded:false,
              method:"POST",
              url:"wwv_flow.show"
            }
          },
          ui:{theme_name:pTreeTemplate},
          types:pTypes,
          rules:{
            valid_children:"root",
            use_max_depth:true
          },
          callback:{
           // onselect:$.onselect
          },
          root:{
            draggable:false,
            valid_children: "folder"
          },
          folder:{
            valid_children: "file"
          },
          file:{
            valid_children: "none",
            max_children: 0,
            max_depth:0
          }
          });

        if (pTreeAction == 'S'){
          apex.jQuery('#'+pTreeId).click($.onclick);}
        else {
          apex.jQuery('#'+pTreeId).dblclick($.onclick);
        }

        // Bind Tooltips for tree nodes
        apex.jQuery('a[tooltip]', '#'+pTreeId).bind("mouseover", $.showTooltip);

        // Set Tree Focus on Parent Node
        if (apex.jQuery.tree.reference(lTree) != null){
          apex.jQuery.tree.reference(lTree).open_branch(apex.jQuery("#"+pStartNodeId));
        }
        //  Set Selected Node
        if (pSelectedNodeId){
          // Expand parent node and selected node
          if (apex.jQuery.tree.reference(lTree) != null){
            apex.jQuery.tree.reference(lTree).select_branch(apex.jQuery("#"+pSelectedNodeId));
          }
        }
    }
  }; // init

  /**
   * onselect
   **/
  $.onselect = function(pNode, pTree) {
      var lAction, lNode = pTree.get_node(pNode);
      lAction = apex.jQuery('a', lNode).attr("href");
      document.location.href=lAction;
  }; // onselect

  /**
   * onclick
   **/
  $.onclick = function(pEvent) {
      var lAction = apex.jQuery(pEvent.target).attr("href");
      if (lAction && lAction != "") {
        document.location.href=lAction;
      }
  }; // onclick


  $.expand_all = function(pTreeId) {
    if ($v('pScreenReaderMode') === 'YES') {
      // in screen reader mode everything is expanded by default
    } else {
      var lTree = apex.jQuery("#"+pTreeId);
      if (apex.jQuery.tree.reference(lTree) != null){
        apex.jQuery.tree.reference(lTree).open_all();
      }
    }
  }; //expand_all

  $.collapse_all = function(pTreeId) {
    if ($v('pScreenReaderMode') === 'YES') {
      // in screen reader mode everything is expanded by default
    } else {
      var lTree = apex.jQuery("#"+pTreeId);
      if (apex.jQuery.tree.reference(lTree) != null){
        apex.jQuery.tree.reference(lTree).close_all();
      }
    }
  }; //collapse_all

  $.reset = function(pTreeId,pStartNodeId) {
    if ($v('pScreenReaderMode') === 'YES') {
      // in screen reader mode everything is expanded by default
    } else {
      var lTree = apex.jQuery("#"+pTreeId);
      if (apex.jQuery.tree.reference(lTree) != null){
        apex.jQuery.tree.reference(lTree).close_all();
        apex.jQuery.tree.reference(lTree).open_branch(apex.jQuery("#"+pStartNodeId));
      }
    }
  }; //reset to only expand parent node


  /**
   * xxx
   **/
  $.showTooltip = function(pEvent) {
        var lAction = apex.jQuery(pEvent.target).attr("tooltip");
        if (lAction && lAction != "") {
            toolTip_enable(pEvent,this,apex.jQuery(this).attr("tooltip"));
        }
  }; // showTooltip

  /**
   * xxx
   **/
  $.hideTooltip = function() {
    toolTip_disable();
  }; // hideTooltip

})(apex.widget.tree); /* pass in the namespace variable*/ //apex.widget.tree


/**
 * @class textareaClob
 * Allows to upload the content of a textarea as CLOB
 * */
apex.widget.textareaClob = {
  upload: function(pItemName, pRequest) {
    var lClob = new apex.ajax.clob(function(){
                                     if      (p.readyState === 1){}
                                     else if (p.readyState === 2){}
                                     else if (p.readyState === 3){}
                                     else if (p.readyState === 4){
                                       $s(pItemName, "");
                                       apex.submit(pRequest);
                                     } else {
                                       return false;
                                     }
                                   });
    lClob._set($v(pItemName));
  }
};


var hour_regexp = new RegExp("hh((24)|(12))?", "gi"); // expression for hour match hh,hh12,hh24
var min_regexp = new RegExp("ii","gi"); // expresion for minute match
var sec_regexp = new RegExp("ss","gi"); // expression for seconds match
var ampm_regexp = new RegExp("((%at%)|(%pt%))", "gi"); // expression for am,pm match

function Timepicker() {}

Timepicker.prototype = {
    init: function()
    {
        this._mainDivId = 'ui-timepicker-div';
        this._inputId   = null;
        this._orgValue  = null;
        this._orgHour   = null;
        this._orgMinute = null;
        this._colonPos  = -1;
        this._visible   = false;
        this.tpDiv      = apex.jQuery('<div id="' + this._mainDivId + '" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all ui-helper-hidden-accessible" style="width: 100px; display: none; position: absolute;"></div>');

        /**
    * Extending default values
    */
    apex.jQuery.extend(apex.jQuery.datepicker._defaults, {
        'time24h': false, // True if 24h time
        'showTime': false, // Show timepicker with datepicker
        'ampmNames': ['AM','PM'], // value for the hour display, should be included part of regionalization
        'timeFormat': 'HH:NI',
        'altTimeField': '', // Selector for an alternate field to store time into
        'defHour': 0,
        'defMinute' : 0,
        'defAmpm' : '',
        'escapeKey' : false
    });

    /**
     * _hideDatepicker must be called with null
     */
    apex.jQuery.datepicker._connectDatepickerOverride = apex.jQuery.datepicker._connectDatepicker;
    apex.jQuery.datepicker._connectDatepicker = function(target, inst) {
        apex.jQuery.datepicker._connectDatepickerOverride(target, inst);

        // showButtonPanel is required with timepicker
        if (this._get(inst, 'showTime')) {
            inst.settings['showButtonPanel'] = true;
        }

        var showOn = this._get(inst, 'showOn');

        if (showOn == 'button' || showOn == 'both') {
            // Unbind all click events
            inst.trigger.unbind('click');

            // Bind new click event
            inst.trigger.click(function() {
                if (apex.jQuery.datepicker._datepickerShowing && apex.jQuery.datepicker._lastInput == target)
                    apex.jQuery.datepicker._hideDatepicker(null); // This override is all about the "null"
                else
                    apex.jQuery.datepicker._showDatepicker(target);
                return false;
            });
        }
    };

     /**
     * Datepicker does not have an onShow event so I need to create it.
     * What I actually doing here is copying original _showDatepicker
     * method to _showDatepickerOverload method.
     */
    apex.jQuery.datepicker._showDatepickerOverride = apex.jQuery.datepicker._showDatepicker;
    apex.jQuery.datepicker._showDatepicker = function (input) {
        // Call the original method which will show the datepicker
        apex.jQuery.datepicker._showDatepickerOverride(input);

        input = input.target || input;

        // find from button/image trigger
        if (input.nodeName.toLowerCase() != 'input') input = apex.jQuery('input', input.parentNode)[0];

        // Do not show timepicker if datepicker is disabled
        if (apex.jQuery.datepicker._isDisabledDatepicker(input)) return;

        // Get instance to datepicker
        var inst = apex.jQuery.datepicker._getInst(input);

        var showTime = apex.jQuery.datepicker._get(inst, 'showTime');

        // If showTime = True show the timepicker
        if (showTime) apex.jQuery.timepicker.show(input);
    };

    /** To control the click on regions other than Date and timepicker region
    * this function is original copy of __checkExternalClick, except with
    * additional line of code to handle, date with time format
    */
    apex.jQuery.datepicker._checkExternalClickOverride = apex.jQuery.__checkExternalClick;
    apex.jQuery.datepicker._checkExternalClick = function(event) {
        if (!apex.jQuery.datepicker._curInst)
        return;
        var $target = apex.jQuery(event.target);
        if ($target[0].id != apex.jQuery.datepicker._mainDivId &&
            $target.parents('#' + apex.jQuery.datepicker._mainDivId).length == 0 &&
            !$target.hasClass(apex.jQuery.datepicker.markerClassName) &&
            !$target.hasClass(apex.jQuery.datepicker._triggerClass) &&
            apex.jQuery.datepicker._datepickerShowing && !(apex.jQuery.datepicker._inDialog && apex.jQuery.blockUI)) {
        apex.jQuery.datepicker._curInst.escapeKey = true; //this is additional line included for date with time formats
        apex.jQuery.datepicker._hideDatepicker();
        }

    };

    /**
    * To control the Keypress event for the timepicker, need to create this function
    * this function is original copy of _doKeyDown, except additional code to suppress
    * escape key for date with time.
    */
    apex.jQuery.datepicker._doKeyDownOverride = apex.jQuery.datepicker._doKeyDown;
    apex.jQuery.datepicker._doKeyDown = function(event) {

        var inst = apex.jQuery.datepicker._getInst(event.target);
        var handled = true;
        var isRTL = inst.dpDiv.is('.ui-datepicker-rtl');
        inst._keyEvent = true;
        inst.escapeKey = false; // added for timepicker
        if (apex.jQuery.datepicker._datepickerShowing)
            switch (event.keyCode) {
                case 9: if ( apex.jQuery.datepicker._get(inst, 'showTime') ) inst.escapeKey = true;
                        apex.jQuery.datepicker._hideDatepicker();
                        handled = false;
                        break; // hide on tab out
                case 13: var sel = apex.jQuery('td.' + apex.jQuery.datepicker._dayOverClass, inst.dpDiv).
                            add(apex.jQuery('td.' + apex.jQuery.datepicker._currentClass, inst.dpDiv));
                        if (sel[0])
                            apex.jQuery.datepicker._selectDay(event.target, inst.selectedMonth, inst.selectedYear, sel[0]);
                        else
                            apex.jQuery.datepicker._hideDatepicker();
                        return false; // don't submit the form
                        break; // select the value on enter
                case 27: inst.escapeKey = true; //added for timepicker
                         apex.jQuery.datepicker._hideDatepicker();
                         break; // hide on escape
                case 33: apex.jQuery.datepicker._adjustDate(event.target, (event.ctrlKey ?
                            -apex.jQuery.datepicker._get(inst, 'stepBigMonths') :
                            -apex.jQuery.datepicker._get(inst, 'stepMonths')), 'M');
                        break; // previous month/year on page up/+ ctrl
                case 34: apex.jQuery.datepicker._adjustDate(event.target, (event.ctrlKey ?
                            +apex.jQuery.datepicker._get(inst, 'stepBigMonths') :
                            +apex.jQuery.datepicker._get(inst, 'stepMonths')), 'M');
                        break; // next month/year on page down/+ ctrl
                case 35: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._clearDate(event.target);
                        handled = event.ctrlKey || event.metaKey;
                        break; // clear on ctrl or command +end
                case 36: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._gotoToday(event.target);
                        handled = event.ctrlKey || event.metaKey;
                        break; // current on ctrl or command +home
                case 37: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._adjustDate(event.target, (isRTL ? +1 : -1), 'D');
                        handled = event.ctrlKey || event.metaKey;
                        // -1 day on ctrl or command +left
                        if (event.originalEvent.altKey) apex.jQuery.datepicker._adjustDate(event.target, (event.ctrlKey ?
                                    -apex.jQuery.datepicker._get(inst, 'stepBigMonths') :
                                    -apex.jQuery.datepicker._get(inst, 'stepMonths')), 'M');
                        // next month/year on alt +left on Mac
                        break;
                case 38: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._adjustDate(event.target, -7, 'D');
                        handled = event.ctrlKey || event.metaKey;
                        break; // -1 week on ctrl or command +up
                case 39: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._adjustDate(event.target, (isRTL ? -1 : +1), 'D');
                        handled = event.ctrlKey || event.metaKey;
                        // +1 day on ctrl or command +right
                        if (event.originalEvent.altKey) apex.jQuery.datepicker._adjustDate(event.target, (event.ctrlKey ?
                                    +apex.jQuery.datepicker._get(inst, 'stepBigMonths') :
                                    +apex.jQuery.datepicker._get(inst, 'stepMonths')), 'M');
                        // next month/year on alt +right
                        break;
                case 40: if (event.ctrlKey || event.metaKey) apex.jQuery.datepicker._adjustDate(event.target, +7, 'D');
                        handled = event.ctrlKey || event.metaKey;
                        break; // +1 week on ctrl or command +down
                default: handled = false;
            }
        else if (event.keyCode == 36 && event.ctrlKey) // display the date picker on ctrl+home
            apex.jQuery.datepicker._showDatepicker(this);
        else {
            handled = false;
        }
        if (handled) {
            event.preventDefault();
            event.stopPropagation();
        }

    };

    /**
     * Datepicker has onHide event, which is used to add the time part to the Date
     */
    apex.jQuery.datepicker._hideDatepickerOverride = apex.jQuery.datepicker._hideDatepicker;
    apex.jQuery.datepicker._hideDatepicker = function(input, duration) {
        // Some lines from the original method
        var inst = this._curInst;

        if (!inst || (input && inst != apex.jQuery.data(input, PROP_NAME))) return;

        // Get the value of showTime property
        var showTime = this._get(inst, 'showTime');

        if (input === undefined && showTime && !inst.escapeKey) {
            if (inst.input) {
                inst.input.val(this._formatDate(inst));
               // inst.input.trigger('change'); // fire the change event
            }

            this._updateAlternate(inst);

            if (showTime) apex.jQuery.timepicker.update(this._formatDate(inst),inst);
        }
        inst.escapeKey = false;
        // Hide datepicker
        apex.jQuery.datepicker._hideDatepickerOverride(input, duration);

    };

    /**
     * This is a complete replacement of the _selectDate method.
     * If showed with timepicker do not close when date is selected.
     */
    apex.jQuery.datepicker._selectDate = function(id, dateStr) {
        var target = apex.jQuery(id);
        var inst = this._getInst(target[0]);
        var showTime = this._get(inst, 'showTime');
        dateStr = (dateStr != null ? dateStr : this._formatDate(inst));
        if (!showTime || (inst.inline && showTime)) {
            if (inst.input)
                inst.input.val(dateStr);
            this._updateAlternate(inst);
        }
        var onSelect = this._get(inst, 'onSelect');
        if (onSelect)
            onSelect.apply((inst.input ? inst.input[0] : null), [dateStr, inst]);  // trigger custom callback
        else if (inst.input && !showTime)
            inst.input.trigger('change'); // fire the change event
        if (inst.inline)
            this._updateDatepicker(inst);
        else if (!inst.stayOpen) {
            if (showTime) {
                this._updateDatepicker(inst);
            } else {
                this._hideDatepicker(null, this._get(inst, 'duration'));
                this._lastInput = inst.input[0];
                if (typeof(inst.input[0]) != 'object')
                    inst.input[0].focus(); // restore focus
                this._lastInput = null;
            }
        }
    };

    /**
    * This is method overriding _updateAlternate
    *  this is to support inline datepicker with time
    */

    apex.jQuery.datepicker._updateAlternateOverride = apex.jQuery.datepicker._updateAlternate;
    apex.jQuery.datepicker._updateAlternate = function(inst) {
        var altField = this._get(inst, 'altField');
        if (altField) { // update alternate field too
            var altFormat = this._get(inst, 'altFormat') || this._get(inst, 'dateFormat');
            var date = this._getDate(inst);
            var dateStr = this.formatDate(altFormat, date, this._getFormatConfig(inst));
            //code added for time support in inline mode
            if (inst.inline && this._get(inst, 'showTime')) {
                if (dateStr.match(hour_regexp)) dateStr = dateStr.replace(hour_regexp,apex.jQuery.datepicker._get(inst, 'defHour'));
                if (dateStr.match(min_regexp))  dateStr = dateStr.replace(min_regexp,apex.jQuery.datepicker._get(inst, 'defMinute'));
                if (dateStr.match(sec_regexp))  dateStr = dateStr.replace(sec_regexp,'00');
                if (dateStr.match(ampm_regexp))  dateStr = dateStr.replace(ampm_regexp,apex.jQuery.datepicker._get(inst, 'defAmpm'));
            }
            //end of code
            apex.jQuery(altField).each(function() { apex.jQuery(this).val(dateStr); });
        }
    };

    /**
    * This is a complete replacement of _selectDay function
    * this function is extended to support time in inline version of Datepicker
    */

    apex.jQuery.datepicker._selectDayOverride = apex.jQuery.datepicker._selectDay;
    apex.jQuery.datepicker._selectDay = function(id, month, year, td) {
        var target = apex.jQuery(id);
        if (apex.jQuery(td).hasClass(this._unselectableClass) || this._isDisabledDatepicker(target[0])) {
            return;
        }
        var inst = this._getInst(target[0]);
        inst.selectedDay = inst.currentDay = apex.jQuery('a', td).html();
        inst.selectedMonth = inst.currentMonth = month;
        inst.selectedYear = inst.currentYear = year;
        //sathikum code to support time in inline mode.
        var hour, minute, ampm ;
        if (inst.inline && inst.settings.showTime) {
            hour = apex.jQuery(target).find('select[id$="hour"]').val();
            minute = apex.jQuery(target).find('select[id$="minute"]').val();
            ampm = apex.jQuery(target).find('select[id$="ampm"]').val();
        }
        this._selectDate(id, this._formatDate(inst,
        inst.currentDay, inst.currentMonth, inst.currentYear, hour, minute, 0, ampm));
    };
    /**
    * This is overriding function of _generateHTML, the prime need to override, is to include time functionality in inline mode
    *
    */

    apex.jQuery.datepicker._generateHTMLOverride = apex.jQuery.datepicker._generateHTML;
    apex.jQuery.datepicker._generateHTML = function(inst) {
        if (!inst.dpuuid) {
            for(attr in window) {
                if(/^DP_jQuery_/.test(attr)) {
                    inst.dpuuid = attr.replace(/^DP_jQuery_([0-9]+)/, '$1');
                }
            }
        }
        var dpuuid = inst.dpuuid;
        var today = new Date();
        today = this._daylightSavingAdjust(
            new Date(today.getFullYear(), today.getMonth(), today.getDate())); // clear time
        var isRTL = this._get(inst, 'isRTL');
        var showButtonPanel = this._get(inst, 'showButtonPanel');
        var hideIfNoPrevNext = this._get(inst, 'hideIfNoPrevNext');
        var navigationAsDateFormat = this._get(inst, 'navigationAsDateFormat');
        var numMonths = this._getNumberOfMonths(inst);
        var showCurrentAtPos = this._get(inst, 'showCurrentAtPos');
        var stepMonths = this._get(inst, 'stepMonths');
        var isMultiMonth = (numMonths[0] != 1 || numMonths[1] != 1);
        var currentDate = this._daylightSavingAdjust((!inst.currentDay ? new Date(9999, 9, 9) :
            new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
        var minDate = this._getMinMaxDate(inst, 'min');
        var maxDate = this._getMinMaxDate(inst, 'max');
        var drawMonth = inst.drawMonth - showCurrentAtPos;
        var drawYear = inst.drawYear;
        if (drawMonth < 0) {
            drawMonth += 12;
            drawYear--;
        }
        if (maxDate) {
            var maxDraw = this._daylightSavingAdjust(new Date(maxDate.getFullYear(),
                maxDate.getMonth() - (numMonths[0] * numMonths[1]) + 1, maxDate.getDate()));
            maxDraw = (minDate && maxDraw < minDate ? minDate : maxDraw);
            while (this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1)) > maxDraw) {
                drawMonth--;
                if (drawMonth < 0) {
                    drawMonth = 11;
                    drawYear--;
                }
            }
        }
        inst.drawMonth = drawMonth;
        inst.drawYear = drawYear;
        var prevText = this._get(inst, 'prevText');
        prevText = (!navigationAsDateFormat ? prevText : this.formatDate(prevText,
            this._daylightSavingAdjust(new Date(drawYear, drawMonth - stepMonths, 1)),
            this._getFormatConfig(inst)));
        var prev = (this._canAdjustMonth(inst, -1, drawYear, drawMonth) ?
            '<a class="ui-datepicker-prev ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._adjustDate(\'#' + inst.id + '\', -' + stepMonths + ', \'M\');"' +
            ' title="' + prevText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>' :
            (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="'+ prevText +'"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'e' : 'w') + '">' + prevText + '</span></a>'));
        var nextText = this._get(inst, 'nextText');
        nextText = (!navigationAsDateFormat ? nextText : this.formatDate(nextText,
            this._daylightSavingAdjust(new Date(drawYear, drawMonth + stepMonths, 1)),
            this._getFormatConfig(inst)));
        var next = (this._canAdjustMonth(inst, +1, drawYear, drawMonth) ?
            '<a class="ui-datepicker-next ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._adjustDate(\'#' + inst.id + '\', +' + stepMonths + ', \'M\');"' +
            ' title="' + nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>' :
            (hideIfNoPrevNext ? '' : '<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="'+ nextText + '"><span class="ui-icon ui-icon-circle-triangle-' + ( isRTL ? 'w' : 'e') + '">' + nextText + '</span></a>'));
        var currentText = this._get(inst, 'currentText');
        var gotoDate = (this._get(inst, 'gotoCurrent') && inst.currentDay ? currentDate : today);
        currentText = (!navigationAsDateFormat ? currentText :
            this.formatDate(currentText, gotoDate, this._getFormatConfig(inst)));
        var controls = (!inst.inline ? '<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._hideDatepicker();">' + this._get(inst, 'closeText') + '</button>' : '');

        var buttonPanel = (showButtonPanel) ? '<div class="ui-datepicker-buttonpane ui-widget-content">' + (isRTL ? controls : '') +
            (this._isInRange(inst, gotoDate) ? '<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" onclick="DP_jQuery_' + dpuuid +
            '.datepicker._gotoToday(\'#' + inst.id + '\');"' +
            '>' + currentText + '</button>' : '') + (isRTL ? '' : controls) + '</div>' : '';
        // sathikum code for including the timepicker as part of inline datepicker
        var showTime = this._get(inst,"showTime");
        buttonPanel = (inst.inline  && showTime ? apex.jQuery.timepicker._generateInlineTimeHTML(inst) + buttonPanel : buttonPanel);
        var firstDay = parseInt(this._get(inst, 'firstDay'),10);
        firstDay = (isNaN(firstDay) ? 0 : firstDay);
        var showWeek = this._get(inst, 'showWeek');
        var dayNames = this._get(inst, 'dayNames');
        var dayNamesShort = this._get(inst, 'dayNamesShort');
        var dayNamesMin = this._get(inst, 'dayNamesMin');
        var monthNames = this._get(inst, 'monthNames');
        var monthNamesShort = this._get(inst, 'monthNamesShort');
        var beforeShowDay = this._get(inst, 'beforeShowDay');
        var showOtherMonths = this._get(inst, 'showOtherMonths');
        var selectOtherMonths = this._get(inst, 'selectOtherMonths');
        var calculateWeek = this._get(inst, 'calculateWeek') || this.iso8601Week;
        var defaultDate = this._getDefaultDate(inst);
        var html = '';
        //sathikum code for inline date with time
       // controls = controls + (inst.inline ? '<span>Time here</span>' : '');
        //**end**
        for (var row = 0; row < numMonths[0]; row++) {
            var group = '';
            for (var col = 0; col < numMonths[1]; col++) {
                var selectedDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, inst.selectedDay));
                var cornerClass = ' ui-corner-all';
                var calender = '';
                if (isMultiMonth) {
                    calender += '<div class="ui-datepicker-group';
                    if (numMonths[1] > 1)
                        switch (col) {
                            case 0: calender += ' ui-datepicker-group-first';
                                cornerClass = ' ui-corner-' + (isRTL ? 'right' : 'left'); break;
                            case numMonths[1]-1: calender += ' ui-datepicker-group-last';
                                cornerClass = ' ui-corner-' + (isRTL ? 'left' : 'right'); break;
                            default: calender += ' ui-datepicker-group-middle'; cornerClass = ''; break;
                        }
                    calender += '">';
                }
                calender += '<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix' + cornerClass + '">' +
                    (/all|left/.test(cornerClass) && row == 0 ? (isRTL ? next : prev) : '') +
                    (/all|right/.test(cornerClass) && row == 0 ? (isRTL ? prev : next) : '') +
                    this._generateMonthYearHeader(inst, drawMonth, drawYear, minDate, maxDate,
                    row > 0 || col > 0, monthNames, monthNamesShort) + // draw month headers
                    '</div><table class="ui-datepicker-calendar"><thead>' +
                    '<tr>';
                var thead = (showWeek ? '<th class="ui-datepicker-week-col">' + this._get(inst, 'weekHeader') + '</th>' : '');
                for (var dow = 0; dow < 7; dow++) { // days of the week
                    var day = (dow + firstDay) % 7;
                    thead += '<th' + ((dow + firstDay + 6) % 7 >= 5 ? ' class="ui-datepicker-week-end"' : '') + '>' +
                        '<span title="' + dayNames[day] + '">' + dayNamesMin[day] + '</span></th>';
                }
                calender += thead + '</tr></thead><tbody>';
                var daysInMonth = this._getDaysInMonth(drawYear, drawMonth);
                if (drawYear == inst.selectedYear && drawMonth == inst.selectedMonth)
                    inst.selectedDay = Math.min(inst.selectedDay, daysInMonth);
                var leadDays = (this._getFirstDayOfMonth(drawYear, drawMonth) - firstDay + 7) % 7;
                var numRows = (isMultiMonth ? 6 : Math.ceil((leadDays + daysInMonth) / 7)); // calculate the number of rows to generate
                var printDate = this._daylightSavingAdjust(new Date(drawYear, drawMonth, 1 - leadDays));
                for (var dRow = 0; dRow < numRows; dRow++) { // create date picker rows
                    calender += '<tr>';
                    var tbody = (!showWeek ? '' : '<td class="ui-datepicker-week-col">' +
                        this._get(inst, 'calculateWeek')(printDate) + '</td>');
                    for (var dow = 0; dow < 7; dow++) { // create date picker days
                        var daySettings = (beforeShowDay ?
                            beforeShowDay.apply((inst.input ? inst.input[0] : null), [printDate]) : [true, '']);
                        var otherMonth = (printDate.getMonth() != drawMonth);
                        var unselectable = (otherMonth && !selectOtherMonths) || !daySettings[0] ||
                            (minDate && printDate < minDate) || (maxDate && printDate > maxDate);
                        tbody += '<td class="' +
                            ((dow + firstDay + 6) % 7 >= 5 ? ' ui-datepicker-week-end' : '') + // highlight weekends
                            (otherMonth ? ' ui-datepicker-other-month' : '') + // highlight days from other months
                            ((printDate.getTime() == selectedDate.getTime() && drawMonth == inst.selectedMonth && inst._keyEvent) || // user pressed key
                            (defaultDate.getTime() == printDate.getTime() && defaultDate.getTime() == selectedDate.getTime()) ?
                            // or defaultDate is current printedDate and defaultDate is selectedDate
                            ' ' + this._dayOverClass : '') + // highlight selected day
                            (unselectable ? ' ' + this._unselectableClass + ' ui-state-disabled': '') +  // highlight unselectable days
                            (otherMonth && !showOtherMonths ? '' : ' ' + daySettings[1] + // highlight custom dates
                            (printDate.getTime() == currentDate.getTime() ? ' ' + this._currentClass : '') + // highlight selected day
                            (printDate.getTime() == today.getTime() ? ' ui-datepicker-today' : '')) + '"' + // highlight today (if different)
                            ((!otherMonth || showOtherMonths) && daySettings[2] ? ' title="' + daySettings[2] + '"' : '') + // cell title
                            (unselectable ? '' : ' onclick="DP_jQuery_' + dpuuid + '.datepicker._selectDay(\'#' +
                            inst.id + '\',' + printDate.getMonth() + ',' + printDate.getFullYear() + ', this);return false;"') + '>' + // actions
                            (otherMonth && !showOtherMonths ? '&#xa0;' : // display for other months
                            (unselectable ? '<span class="ui-state-default">' + printDate.getDate() + '</span>' : '<a class="ui-state-default' +
                            (printDate.getTime() == today.getTime() ? ' ui-state-highlight' : '') +
                            (printDate.getTime() == selectedDate.getTime() ? ' ui-state-active' : '') + // highlight selected day
                            (otherMonth ? ' ui-priority-secondary' : '') + // distinguish dates from other months
                            '" href="#">' + printDate.getDate() + '</a>')) + '</td>'; // display selectable date
                        printDate.setDate(printDate.getDate() + 1);
                        printDate = this._daylightSavingAdjust(printDate);
                    }
                    calender += tbody + '</tr>';
                }
                drawMonth++;
                if (drawMonth > 11) {
                    drawMonth = 0;
                    drawYear++;
                }
                calender += '</tbody></table>' + (isMultiMonth ? '</div>' +
                            ((numMonths[0] > 0 && col == numMonths[1]-1) ? '<div class="ui-datepicker-row-break"></div>' : '') : '');
                group += calender;
            }
            html += group;
        }
        html += buttonPanel + (apex.jQuery.browser.msie && parseInt(apex.jQuery.browser.version,10) < 7 && !inst.inline ?
            '<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>' : '');
        inst._keyEvent = false;
        return html;
    };

    /**
    * This is a overriding method of _formatDate, with additional parameters like hours, minutes, seconds and ampm
    * the function is extended to support time in inline version of Datepicker.
    */

    apex.jQuery.datepicker._formatDateOverride = apex.jQuery.datepicker._formatDate;
    apex.jQuery.datepicker._formatDate = function(inst, day, month, year, hour, minute, second, ampm) {
        if (!day) {
            inst.currentDay = inst.selectedDay;
            inst.currentMonth = inst.selectedMonth;
            inst.currentYear = inst.selectedYear;
        }
        //sathikum code to support time part for inline datepicker
        var newHour = hour;
        var newDate;
        if (inst.inline && inst.settings.showTime) {
            if (ampm == 'PM') {
                if (newHour != 12) newHour += 12; //added to fix AM/PM issue
            } else {
                if (newHour == 12) newHour = 0;
            }

            var date = (day ? (typeof day == 'object' ? day :
               // this._daylightSavingAdjust(new Date(year, month, day, newHour, minute, second))) :
                this._daylightSavingAdjust(new Date(year, month, day))) :
                this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
            newDate = this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst)).replace(hour_regexp, hour).replace(min_regexp,minute).replace(sec_regexp,"00").replace(ampm_regexp,ampm);
        } else {
            var date = (day ? (typeof day == 'object' ? day :
                this._daylightSavingAdjust(new Date(year, month, day))) :
                this._daylightSavingAdjust(new Date(inst.currentYear, inst.currentMonth, inst.currentDay)));
            newDate = this.formatDate(this._get(inst, 'dateFormat'), date, this._getFormatConfig(inst));
        }
        return newDate;
    };

    /**
     * We need to resize the timepicker when the datepicker has been changed.
     */
    apex.jQuery.datepicker._updateDatepickerOverride = apex.jQuery.datepicker._updateDatepicker;
    apex.jQuery.datepicker._updateDatepicker = function(inst) {
        apex.jQuery.datepicker._updateDatepickerOverride(inst);
            apex.jQuery.timepicker._generateTimeHtml(inst);
        };
    },

    show: function (input)
    {
        // Get instance to datepicker
        var inst = apex.jQuery.datepicker._getInst(input);
        var h;
        var m;
        var ampm;
        this._time24h = apex.jQuery.datepicker._get(inst, 'time24h');
        this._altTimeField = apex.jQuery.datepicker._get(inst, 'altTimeField');
        this._timeFormat = apex.jQuery.datepicker._get(inst, 'timeFormat');
        ldateFormat = apex.jQuery.datepicker._get(inst, 'dateFormat');
        this._inputId = input.id;

        if (!this._visible) {
            this._parseTime(this._timeFormat);
            this._orgValue = apex.jQuery('#' + this._inputId).val();
        }
        if ( apex.jQuery('#' + this._inputId).val() != '' && apex.jQuery('#' + this._inputId).val() != null )
        {
            try {
                   var settings = this._getFormatConfig(inst);
               var date = this._parseDateTime(ldateFormat, apex.jQuery('#' + this._inputId).val(), settings, inst.settings.locale) ;
               h = date.getHours();
               ampm = (h >= 12 ) ? 'PM':'AM';
               m = date.getMinutes();
            } catch (e) {
                h = apex.jQuery.datepicker._get(inst, 'defHour');
                m = apex.jQuery.datepicker._get(inst, 'defMinute');
            }
           this._setTime('hour',   h);
           this._setTime('minute', m);
           if (!this._time24h) this._setTime('ampm',ampm);

           this._orgHour   = h;
           this._orgMinute = m;

        }
        this.resize();

        apex.jQuery('#' + this._mainDivId).show();

        this._visible = true;

        var dpDiv     = apex.jQuery('#' + apex.jQuery.datepicker._mainDivId);
        var dpDivPos  = dpDiv.position();

        var viewWidth = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) + apex.jQuery(document).scrollLeft();
        var tpRight   = this.tpDiv.offset().left + this.tpDiv.outerWidth();

        if (tpRight > viewWidth) {
            dpDiv.css('left', dpDivPos.left - (tpRight - viewWidth) - 5);
            this.tpDiv.css('left', dpDiv.offset().left + dpDiv.outerWidth() + 'px');
        }
    },

    update: function (fd,inst)
    {

        var timeFormat = apex.jQuery.datepicker._get(inst, 'timeFormat');
        var curHour = $v(this._mainDivId + 'hour');
        var curMinute = $v(this._mainDivId + 'minute');
        var curampm = '';
        var dummystr = ''

        var curTime ;
        if (this._time24h)
            curTime = timeFormat.replace('HH24',curHour).replace('ii',curMinute).replace('SS','00') ;
        else {
              curampm = $v(this._mainDivId + 'ampm');
              dummystr = (timeFormat.indexOf('SS')==-1) ? ' ' + curampm : '';
              curTime = timeFormat.replace('HH',curHour).replace('ii',curMinute + dummystr).replace('SS','00 ' + curampm) ;
        }


        var curDate = apex.jQuery('#' + this._inputId).val();

        $s(this._inputId, fd.replace(hour_regexp, curHour).replace(min_regexp,curMinute).replace(sec_regexp,"00").replace(ampm_regexp,curampm));

        if (this._altTimeField) {
            apex.jQuery(this._altTimeField).each(function() { apex.jQuery(this).val(curTime); });
        }
    },

    hide: function ()
    {
        this._visible = false;
        apex.jQuery('#' + this._mainDivId).hide();
    },

    resize: function ()
    {
        //var dpDiv = apex.jQuery('#' + apex.jQuery.datepicker._mainDivId);
        //var dpDivPos = dpDiv.position();
        //var lAdjpixel = 5;
        //if (apex.jQuery.browser.msie && parseInt(apex.jQuery.browser.version,10) < 7) lAdjpixel = 0;
        var hdrHeight = apex.jQuery('#' + apex.jQuery.datepicker._mainDivId +  ' > div.ui-datepicker-header:first-child').height();

        apex.jQuery('#' + this._mainDivId + ' > div.ui-datepicker-header:first-child').css('height', hdrHeight);

    },
    _saveHour: function(hourItem)
    {
        this._defHour = $v(hourItem);
    },
    _saveMinute: function(minuteItem)
    {
        this._defMinute = $v(minuteItem);
    },
    _saveampm: function(ampmItem)
    {
        this._defAmpm = $v(ampmItem);
    },
    //time functionality with the datepicker (inline mode)
    _generateInlineTimeHTML : function(inst) {
        var inputItemId = this._mainDivId;
        var inputItemPrefix = this._mainDivId.replace('datepicker','timepicker');
        var h,m,ampm;
        var html = '';
        var maxHours = 24;
        var curhour = '00' ;
        var curminute = '00';
        var curampm = '';
        var time24h = apex.jQuery.datepicker._get(inst, 'time24h');
        this._time24h = time24h ;
        var ampmNames =apex.jQuery.datepicker._get(inst,'ampmNames');
        var ldateFormat = apex.jQuery.datepicker._get(inst, 'dateFormat');
        var curValue = inst.input[0].value ;
        var hourStart = 0;
        var altField = apex.jQuery.datepicker._get(inst,'altField');
        if (altField.indexOf('#') == 0 ) altField = altField.substr(1,altField.length) ; //remove an extra # used by jQuery
        if (!time24h) {
            maxHours = 13;
            curhour = 12;
            hourStart = 1;
        }

        try {
            var settings = this._getFormatConfig(inst);
            var date = apex.jQuery.timepicker._parseDateTime(ldateFormat, $v(altField), settings) ;
            if ( date == null) throw 'myexp';
            h = date.getHours();
            ampm = (h >= 12 ) ? 'PM':'AM';
            m = date.getMinutes();
        } catch (e) {
            h = apex.jQuery.datepicker._get(inst, 'defHour');
            m = apex.jQuery.datepicker._get(inst, 'defMinute');
            ampm = apex.jQuery.datepicker._get(inst, 'defAmpm');
            $s(altField,$v(altField).replace(hour_regexp, h).replace(min_regexp,m).replace(sec_regexp,"00").replace(ampm_regexp,ampm));
        }

        curhour = h;
        curminute = m;
        curampm = ampm;
        /*if ( time24h && curhour < 12  && curhour != 0 ) {
            curhour += 12;
        } else*/ if ( !time24h && ampm == 'PM' && curhour != 12) {
            curhour -= 12;
        }
        //curhour = ( time24h && curhour == 0 ) ? 12 : h;
        curhour = ( !time24h  && curhour == 0 ) ? 12 : curhour;

        html = '<div class="ui-helper-clearfix ui-corner-all">';
        html += '<span class="spanTime" style="float:right;"><table><tr><td>&nbsp;</td><td>&nbsp;</td><td>' ;
        html += '<table style="float:right;"><tr><td>' ;

        if (!time24h) {
           html +='<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  inputItemPrefix + altField + 'ampm" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker.refreshTime(\'#' + inst.id  + '\');" >';
           for (var ampm = 0 ; ampm < 2 ; ampm++) {
               html += '<option value="' + ampmNames[ampm] + '"' +
                     (ampmNames[ampm] == curampm ? ' selected="selected"' : '') +
               '>' + ampmNames[ampm] + '</option>';
           }
           html += '</select>';
        }

        html += '<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  inputItemPrefix + altField + 'minute" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker.refreshTime(\'#' + inst.id  + '\');" >' ;
        for (var minute = 0; minute < 60; minute++) {
            if ( minute < 10 ) {
                html += '<option value="0' + minute + '"' +
                         ('0'+minute == curminute ? ' selected="selected"' : '') +
                                                '>0' +minute + '</option>';
             }
             else {
                     html += '<option value="' + minute + '"' +
                         (minute == curminute ? ' selected="selected"' : '') +
                            '>' +minute + '</option>';
             }
        }
        html += '</select>' ;

        html += '<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  inputItemPrefix + altField + 'hour" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker.refreshTime(\'#' + inst.id + '\');" >' ;
        for (var hour = hourStart; hour < maxHours; hour++) {
        if ( hour < 10 ) {
            html += '<option value="0' + hour + '"' +
             ('0'+hour == curhour ? ' selected="selected"' : '') +
                                        '>0' +hour + '</option>';
         }
         else {
            html += '<option value="' + hour + '"' +
            (hour == curhour ? ' selected="selected"' : '') +
                        '>' +hour + '</option>';
             }
        }
        html+='</select>';
        html += '</td></tr></table></td></tr></table>';
        html += '</span></div>';
        return html;
    },
    _generateTimeHtml: function(inst)
    {
        var html = '';
        var maxHours = 24;
        var curhour = '00' ;
        var curminute = '00';
        var curampm = '';
        var time24h = apex.jQuery.datepicker._get(inst, 'time24h');
        this._time24h = time24h ;
        var ampmNames =apex.jQuery.datepicker._get(inst,'ampmNames');
        var curValue = inst.input[0].value ;
        var hourStart = 0;
        // do not execute the following code if the Datepicker is inline with Time
        if ( inst.settings.showTime && !inst.inline ) {
            if (!time24h) {
                maxHours = 13;
                curhour = 12;
                hourStart = 1;
            }
            if (this._defHour != undefined )
                if ( curValue != '' )
                    curhour = this._defHour ;
                else
                    curhour = apex.jQuery.datepicker._get(inst, 'defHour');
            else
                curhour = apex.jQuery.datepicker._get(inst, 'defHour');

            if (this._defMinute != undefined )
                if ( curValue != '' )
                    curminute = this._defMinute ;
                else
                    curminute = apex.jQuery.datepicker._get(inst, 'defMinute');
            else
                curminute = apex.jQuery.datepicker._get(inst, 'defMinute');

            if (this._defAmpm != undefined)
                if ( curValue != '' )
                    curampm = this._defAmpm ;
                else
                    curampm = apex.jQuery.datepicker._get(inst, 'defAmpm');
            else
                curampm = apex.jQuery.datepicker._get(inst, 'defAmpm');

            html = '<div class="ui-helper-clearfix ui-corner-all">';
            html += '<span class="spanTime" style="float:right;"><table><tr><td>&nbsp;</td><td>&nbsp;</td><td>' ;
            html += '<table style="float:right;"><tr><td>' ;

            if (!time24h) {
               html +='<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  this._mainDivId + 'ampm" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker._saveampm(this)" >';
                for (var ampm = 0 ; ampm < 2 ; ampm++) {
                   html += '<option value="' + ampmNames[ampm] + '"' +
                         (ampmNames[ampm] == curampm ? ' selected="selected"' : '') +
                   '>' + ampmNames[ampm] + '</option>';
                }
            html += '</select>';
            }

            html += '<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  this._mainDivId + 'minute" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker._saveMinute(this)" >' ;
            for (var minute = 0; minute < 60; minute++) {
                if ( minute < 10 ) {
                    html += '<option value="0' + minute + '"' +
                             ('0'+minute == curminute ? ' selected="selected"' : '') +
                                                    '>0' +minute + '</option>';
                 }
                 else {
                         html += '<option value="' + minute + '"' +
                             (minute == curminute ? ' selected="selected"' : '') +
                                '>' +minute + '</option>';
                 }
            }
            html += '</select>' ;

            html += '<select style="font-size:1em; margin:1px 0;float:right;" id="'  +  this._mainDivId + 'hour" class="datetimepicker_newMonth" onchange="apex.jQuery.timepicker._saveHour(this)" >' ;
            for (var hour = hourStart; hour < maxHours; hour++) {
                if ( hour < 10 ) {
                    html += '<option value="0' + hour + '"' +
                     ('0'+hour == curhour ? ' selected="selected"' : '') +
                                                '>0' +hour + '</option>';
                }
                else {
                    html += '<option value="' + hour + '"' +
                    (hour == curhour ? ' selected="selected"' : '') +
                                '>' +hour + '</option>';
                 }
            }
            html += '</select>';
            html += '</td></tr></table></td></tr></table>';
            html += '</span></div>';
       }
        var buttonpaneHtml =  apex.jQuery('#' + apex.jQuery.datepicker._mainDivId + ' div.ui-datepicker-buttonpane.ui-widget-content').html();
        apex.jQuery('#' + apex.jQuery.datepicker._mainDivId + ' div.ui-datepicker-buttonpane.ui-widget-content').html(html+ buttonpaneHtml);
    },
    _writeTime: function (type, value)
    {
        if (type == 'hour') {
            if (!this._time24h) {
                if (value <= 12) { //need to relook this
                    //apex.jQuery('#' + this._mainDivId + ' span.fragAmpm').text('am');
                    $x(this._mainDivId + 'ampm').selectedIndex = 0; // for AM
                } else {
                    //apex.jQuery('#' + this._mainDivId + ' span.fragAmpm').text('pm');
                    $x(this._mainDivId + 'ampm').selectedIndex = 1; // for PM
                    value -= 12;
                }
                this._ampm = $v(this._mainDivId + 'ampm');
                if (value == 0) value = 12;
            } else {
                apex.jQuery('#' + this._mainDivId + ' span.fragAmpm').text('');
            }

            if (value < 10) value = '0' + value;
            //apex.jQuery('#' + this._mainDivId + ' span.fragHours').text(value);
            $s(this._mainDivId + 'hour',value);
            this._hour = value ;
        } else if (type == 'minute') {
            if (value < 10) value = '0' + value;
            //apex.jQuery('#' + this._mainDivId + ' span.fragMinutes').text(value);
             $s(this._mainDivId + 'minute',value);
             this._minute = value;
        } else if (type == 'ampm') {
        if ( value == 'PM' )
        $x(this._mainDivId + 'ampm').selectedIndex = 1; // for PM
        else
                $x(this._mainDivId + 'ampm').selectedIndex = 0; // for AM
            this._ampm = $v(this._mainDivId + 'ampm');
        }

    },
    _getFormatConfig: function(inst) {
    var shortYearCutoff = apex.jQuery.datepicker._get(inst, 'shortYearCutoff');
    shortYearCutoff = (typeof shortYearCutoff != 'string' ? shortYearCutoff :
        new Date().getFullYear() % 100 + parseInt(shortYearCutoff, 10));
    return {shortYearCutoff: shortYearCutoff,
        dayNamesShort: apex.jQuery.datepicker._get(inst, 'dayNamesShort'), dayNames: apex.jQuery.datepicker._get(inst, 'dayNames'),
        monthNamesShort: apex.jQuery.datepicker._get(inst, 'monthNamesShort'), monthNames: apex.jQuery.datepicker._get(inst, 'monthNames')};
    },

    // The below function is exact duplicate of parseDate function except with time parsing feature
    _parseDateTime: function (format, value, settings, pLocale) {
        if (format == null || value == null)
            throw 'Invalid arguments';
        value = (typeof value == 'object' ? value.toString() : value + '');
        if (value == '')
            return null;
        var shortYearCutoff = (settings ? settings.shortYearCutoff : null) || this._defaults.shortYearCutoff;
        var dayNamesShort = (settings ? settings.dayNamesShort : null) || this._defaults.dayNamesShort;
        var dayNames = (settings ? settings.dayNames : null) || this._defaults.dayNames;
        var monthNamesShort = (settings ? settings.monthNamesShort : null) || this._defaults.monthNamesShort;
        var monthNames = (settings ? settings.monthNames : null) || this._defaults.monthNames;
        var year = -1;
        var month = -1;
        var day = -1;
        var hour = -1;
        var minute = 0;
        var second = 0;
        var ampm = '';
        var literal = false;
        // Check whether a format character is doubled
        var lookAhead = function(match) {
            var matches = (iFormat + 1 < format.length && format.charAt(iFormat + 1) == match);
            if (matches)
                iFormat++;
            return matches;
        };
        // Extract a number from the string value
        var getNumber = function(match) {
            lookAhead(match);
            var size = (match == 'y' ? 4 : 2);
            var num = 0;
            while (size > 0 && iValue < value.length &&
                    value.charAt(iValue) >= '0' && value.charAt(iValue) <= '9') {
                num = num * 10 + (value.charAt(iValue++) - 0);
                size--;
            }
            if (size == (match == 'y' ? 4 : 2))
                throw 'Missing number at position ' + iValue;
            return num;
        };
        // Extract a name from the string value and convert to an index
        var getName = function(match, shortNames, longNames,pLocale) {
            var names = (lookAhead(match) ? longNames : shortNames);
            var size = 0;
            for (var j = 0; j < names.length; j++)
                size = Math.max(size, names[j].length);
            var name = '';
            var iInit = iValue;
            while (size > 0 && iValue < value.length) {
                name += value.charAt(iValue++);
                for (var i = 0; i < names.length; i++) {
                    if (pLocale == undefined ) {
                        if (name == names[i])
                            return i + 1;
                    }                            
                    else {
                        if ( pLocale == 'en' || pLocale == 'EN' || pLocale.indexOf('en-') == 1 || pLocale.indexOf('EN-') == 1 ) {
                            if (name.toUpperCase() == names[i].toUpperCase())
                                return i + 1;
                        }
                        else
                        {
                        if (name == names[i])
                            return i + 1;
                        }
                    }
                }
                size--;
            }
            throw 'Unknown name at position ' + iInit;
        };
        // Extract the AM PM vart of the value
        var getAmPm = function() {
            var lAmPm = '';
            var size = 2; // for AM and PM
                if ( format.charAt(iFormat) == '%' && ( format.charAt(iFormat+1) == 'a' || format.charAt(iFormat+1) == 'p')
                          && format.charAt(iFormat+2) == 't' && format.charAt(iFormat+3) == '%' )
                    {
                        while (size > 0 && iValue < value.length) {
                            lAmPm += value.charAt(iValue++);
                            size--;
                        }
                        iFormat = iFormat + 4;
                    }
            return lAmPm;
        };
        // Confirm that a literal character matches the string value
        var checkLiteral = function() {
            if (value.charAt(iValue) != format.charAt(iFormat))
            {
                         /*   if ( format.charAt(iFormat) == '%' && ( format.charAt(iFormat+1) == 'a' || format.charAt(iFormat+1) == 'p')
                                && format.charAt(iFormat+2) == 't' && format.charAt(iFormat+3) == '%' )
                            {
                                iValue = iValue + 1;
                                iFormat = iFormat + 4;
                            }
                            else*/
                throw 'Unexpected literal at position ' + iValue;
            }
                        iValue++;
        };
        var iValue = 0;
        for (var iFormat = 0; iFormat < format.length; iFormat++) {
            if (literal)
                if (format.charAt(iFormat) == "'" && !lookAhead("'"))
                    literal = false;
                else
                    checkLiteral();
            else
                switch (format.charAt(iFormat)) {
                    case 'h':
                        hour = getNumber('h');
                        break;
                    case 'H':
                        hour = getNumber('H');
                        break;
                    case 'i':
                        minute = getNumber('i');
                        break;
                    case 's':
                        second = getNumber('s');
                        break;
                    case '%':
                        ampm   = getAmPm();
                        break;
                    case 'd':
                        day = getNumber('d');
                        break;
                    case 'D':
                        getName('D', dayNamesShort, dayNames,pLocale);
                        break;
                    case 'm':
                        month = getNumber('m');
                        break;
                    case 'M':
                        month = getName('M', monthNamesShort, monthNames,pLocale);
                        break;
                    case 'y':
                        year = getNumber('y');
                        break;
                    case "'":
                        if (lookAhead("'"))
                            checkLiteral();
                        else
                            literal = true;
                        break;
                    default:
                        checkLiteral();
                }
        }
        if (year < 100) {
            year += new Date().getFullYear() - new Date().getFullYear() % 100 +
                (year <= shortYearCutoff ? 0 : -100);
        }
        if (ampm == 'PM') {
            if (hour != 12) hour += 12; //added to fix AM/PM issue
        } else if ( ampm == 'AM' ){
            if (hour == 12) hour = 0;
        }
        var date = new Date(year, month - 1, day,hour,minute,second);
        if (date.getFullYear() != year || date.getMonth() + 1 != month || date.getDate() != day) {
            throw 'Invalid date'; // E.g. 31/02/*
        }
        return date;
    },

    refreshTime : function(id)
    {
        var target = apex.jQuery(id);
        var inst = apex.jQuery.datepicker._getInst(target[0]);

        //sathikum code to support time in inline mode.
        var hour, minute, ampm ;
        if (inst.inline && inst.settings.showTime) {
            hour = apex.jQuery(target).find('select[id$="hour"]').val();
            minute = apex.jQuery(target).find('select[id$="minute"]').val();
            ampm = apex.jQuery(target).find('select[id$="ampm"]').val();
        }
        apex.jQuery.datepicker._selectDate(id, apex.jQuery.datepicker._formatDate(inst,
            inst.currentDay, inst.currentMonth, inst.currentYear, hour, minute, 0, ampm));

    },

    _parseTime: function (timeFormat)
    {
        var dt = apex.jQuery('#' + this._inputId).val();
        if ( apex.jQuery.trim(dt) != '' ) {
            var seperator = ':';
        if (/^HH\d\d.NI/i.test(timeFormat))
            seperator = timeFormat.substr(4,1);
        else if (/^HH.NI/i.test(timeFormat))
            seperator = timeFormat.substr(2,1);
            this._colonPos = dt.search(seperator);
            var m = 0, h = 0, s = 0, a = '';
            var ampmPos = -1;
            if (this._colonPos != -1) {
                var sPos = timeFormat.indexOf('SS');
                var ampmlength ;
                if ( ampmPos != -1 ) null;
                  //ampmlength = ( dt.substr(ampmPos,ampmPos+dt.length).indexOf(' ') = -1) ? dt.substr(ampmPos,ampmPos+dt.length).length : dt.substr(ampmPos,ampmPos+dt.length).indexOf(' ');
                  //h = (hPos != -1) ? parseInt(dt.substr(hPos, 2), 10) : -1;
              //m = (mPos != -1) ? parseInt(dt.substr(mPos, 2), 10) : -1;
              //a = (ampmPos != -1) ? apex.jQuery.trim(dt.substr(ampmPos,ampmlength)) : '';
                h = parseInt(dt.substr(this._colonPos - 2, 2), 10);
                m = parseInt(dt.substr(this._colonPos + 1, 2), 10);
                if (sPos == -1 ) {
                    ampmlength =  dt.substr(this._colonPos + 3,this._colonPos + 3+dt.length).indexOf(' ');
                    if (ampmlength == -1 ) ampmlength = this._colonPos + 3+dt.length;
                    a = apex.jQuery.trim(dt.substr(this._colonPos + 3, ampmlength));
                }
                else {
                    ampmlength =  dt.substr(this._colonPos + 7,this._colonPos + 7+dt.length).indexOf(' ');
                    if (ampmlength == -1 ) ampmlength = this._colonPos + 7+dt.length;
                    a = apex.jQuery.trim(dt.substr(this._colonPos + 7, ampmlength));
              }

           }
           a = a.toLowerCase();

           if (a != 'am' && a != 'pm') {
               a = '';
           }

           if (h < 0) h = 0;
           if (m < 0) m = 0;

           if (h > 23) h = 23;
           if (m > 59) m = 59;

           if (a == 'pm' && h  < 12) h += 12;
           if (a == 'am' && h == 12) h  = 1; // changed 0 to 1

           this._setTime('hour',   h);
           this._setTime('minute', m);

           this._orgHour   = h;
           this._orgMinute = m;
       }

    },

    _setTime: function (type, value)
    {
        if (type != 'ampm') { // do the following only for numeric value
        if (isNaN(value)) value = 0;
        if (value < 0)    value = 0;
        if (value > 23 && type == 'hour')   value = 23;
        if (value > 59 && type == 'minute') value = 59;
    }
        this._writeTime(type, value);
    }

};

// code to initialize time picking functionality
apex.jQuery(document).ready(function ()
{
    apex.jQuery.timepicker = new Timepicker();
    apex.jQuery.timepicker.init();
});




