/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 3.1

B32468-02

Copyright � 2003, 2008, Oracle. All rights reserved.

The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.

The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.

If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:

U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.

The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.

Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.

The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/
/**
 * @fileOverview
 * This file holds all namespaced objects and functions for Oracle Application Express
 *
 * */
if (apex.worksheet===null || typeof(apex.worksheet)!="object"){apex.worksheet={};}

/**
 * @namespace = apex.worksheet
 */
apex.worksheet = {
	/**
	* @class ws
	* the worksheet object
	*/
   ws : function (pId){

		/**
		 * use that as call to worksheet object in contained namespaces.
		 * */
		var that = this;

		/**
		 * only one ajax at a time
		 * */
		 this.ajax_busy = false;

		/**
		 * the worksheet id
		 * @type string
		 * */
		this.worksheet_id = false;

		/**
		 * current report id
		 * @type string
		 * */
		this.report_id = false;

		/**
		 * current column id
		 * @type string
		 * */
		this.current_col_id = false;

		/**
		 * the last current column id
		 * @type string
		 * */
		this.last_col_id = false;

		/**
		 * the current control
		 * @type string
		 * */

		this.current_control = false;

		/**
		 * the active control
		 * @type string
		 * */
		this.active_dialog = false;

		/**
		 * supress the ajax update useful for setting a preference or value without bugging user
		 * @type string
		 * */
		this.supress_update = false;

		/**
		 * array of items to post with go button
		 * @type String CSV of PAGE items to post with the Go Button
		 * */
		this.external_items = false;

		/**
		 *This holds the temporary ID
		 * */

		/**
		 * @ignore
		 * */
		this.init = init;

		/**
		 * @ignore
		 * */
		this.init(pId);

		/**
		 * @function
		 * toggles the control bar between full view and icon view
		 * */
		this.toggle_controls = function(){
			var lTest = ($x('apexir_CONTROL_PANEL_COMPLETE').style.display != 'none')?'Y':'N';
			$x_ToggleWithImage('apexir_CONTROLS_IMAGE',['apexir_CONTROL_PANEL_COMPLETE','apexir_CONTROL_PANEL_SUMMARY']);
			that.supress_update = true;
			that.action('CONTROL_MIN',false,false,lTest);
		}

		/**
		 * @namespace
		 * holds functions that return a worksheet dom item
		 * */
		this.item = {}

		/**
		 * @ignore
		 **/
		this.item.worksheet_holder = function(){return $x('apexir_WORKSHEET_REGION');}

		/**
		 * @ignore
		 **/
		this.item.worksheet_detail = function(){return $x('apexir_DETAIL');}

		/**
		 * @ignore
		 **/
		this.item.worksheet_report = function(){return $x('apexir_REPORT');}

		/**
		 * @ignore */
		this.item.worksheet_div = function(){return $x('apexir_WORKSHEET');}

		/**
		 * @ignore */
		this.item.control_panel_drop = function(){return $x('apexir_CONTROL_PANEL_DROP');}

		/**
		 * @ignore */
		this.item.ws_control_panel = function(){return $x('apexir_CONTROL_PANEL');}

		/**
		 * @ignore */
		this.item.worksheet_id = function(){return $x('apexir_NUM_ROWS');}

		/**
		 * @ignore */
		this.item.search = function(){return $x('apexir_SEARCH');}

		/**
		 * @ignore
		 * */
		this.item.search_column = function(){return $x('apexir_CURRENT_SEARCH_COLUMN');}

		/**
		 * @namespace
		 * holds functions specifc to worksheet dialogs
		 * */
		this.dialog = {}

		/**
		 * check to see if something in the dialog was pressed or something else, if something else then reset dialog
		 * */
		this.dialog.check = function (e){
		    // Get the event target (where the user clicked)
			var tPar = html_GetTarget(e);
			// Store the DOM element that we want to test to see if the user clicked in
			// aka the 'test container'
			var lEl = $x('apexir_rollover');
			// Set local flag to true
			var l_Test = true;
			// Iterate up the DOM tree, starting at the event target and traversing up by parentNode.
			// If any ancestor is equal to the 'test container', then set the local flag to false
			while(tPar.nodeName != 'BODY'){
				tPar = tPar.parentNode;
				if(tPar == lEl){l_Test = false;}
			}
			// If the user clicked outside of the 'test container', then reset the dialog
			if(l_Test){that.dialog.reset();}
		}

		this.dialog.check2 = function (e) {
		    // Get the event target (where the user clicked)
			var tPar = html_GetTarget(e);
			// Store the DOM element that we want to test to see if the user clicked in
			// aka the 'test container'
			var lEl = $x('apexir_col_values_drop');
			// If 'test container' still exists, proceed with the check (fixes bug 8692777)
			if (lEl) {
    			// Set local flag to true
    			var l_Test = true;
    			// Iterate up the DOM tree, starting at the event target and traversing up by parentNode.
    			// If any ancestor is equal to the 'test container', then set the local flag to false
    			while (tPar.nodeName != 'BODY') {
    				tPar = tPar.parentNode;
    				if (tPar == lEl) {
    				    l_Test = false;
    				}
    			}
    			// If the user clicked outside of the 'test container', then remove the menu
    			if (l_Test) {
    			    $x_Remove('apexir_col_values_drop');
    			}
			}
		}

        this.dialog.colorPicker = function(pSelector, pOptions) {
            apex.jQuery(pSelector).each(function() {
              var lColorPicker = apex.jQuery(this)
                                   .ColorPicker({
                                     eventName:    "xxx", // don't fire on the default click event, we have our own icon
                                     onSubmit:     function(pHsb, pHex, pRgb, pElement) {
                                                     $s(pElement, '#'+pHex.toUpperCase());
                                                     apex.jQuery(pElement).ColorPickerHide();
                                                   },
                                     onBeforeShow: function() {
                                                     apex.jQuery(this).ColorPickerSetColor(this.value);
                                                   },
                                     onShow:       function(pElement) {
                                                     apex.jQuery(pElement).fadeIn("fast");
                                                     return false;
                                                   },
                                     onHide:       function(pElement) {
                                                     apex.jQuery(pElement).fadeOut("fast");
                                                     return false;
                                                   }
                                     })
                                   .ColorPickerHide();

              lColorPicker
                .bind('keyup',  function(){lColorPicker.ColorPickerSetColor(this.value);})
                .bind('blur',   function(){lColorPicker.ColorPickerHide();})
                .bind('change', function(){
                                  this.value = this.value.toUpperCase();
                                  apex.jQuery("#"+this.id+'_PREVIEW').css("background", this.value);
                                });

              // clicking on our color picker icon should open the dialog
              apex.jQuery('#'+this.id+'_PICKER').click(
                function(pEvent){
                  lColorPicker.ColorPickerShow();
                  pEvent.preventDefault(); // otherwise the browser would jump to the top of the document because of the #
                });

              // show the current entered color in our preview icon
              apex.jQuery("#"+this.id+'_PREVIEW').css("background", this.value);

            });
        }

		/**
		* @namespace
		*/
		this.email= {}

		/**
		* calls SEND_EMAIL action with the items posted to the f01 array in the order listed
		* @function
		*/
		this.email.send = function(){
		   var lValidate = $f_get_emptys('apexir_EMAIL_TO','error','');              if(!!lValidate){return}
		   //that.supress_update = true;
			 var lTemp = ['apexir_EMAIL_TO','apexir_EMAIL_CC','apexir_EMAIL_BCC','apexir_EMAIL_SUBJECT','apexir_EMAIL_BODY'];
		   that.get.AddArrayItems(lTemp,1);
		   that.action('SEND_EMAIL');
		}

		/** @ignore */
		this.email.show = function(pThis){
		   $x_Show_Hide(['apexir_EMAIL','apexir_EMAIL_BUTTON'],'apexir_DOWNLOAD_BUTTON');
		}

		/**
		 * @function
		 *	reset dialog and page to no dialog showing
		 * */
		this.dialog.reset = function (){
			if(that.supress_update){}else{
				$d_ClearAndHide(['searchdrop',that.item.control_panel_drop(),'apexir_SEARCHDROP']);
				$x_Hide(['searchdrop','apexir_rollover']);
				$s('apexir_rollover_content','');
				if($x(that.last_col_id)){$x_Class($x(that.last_col_id).parentNode,'');}
				that.dialog.id = false;
                document.body.onclick = '';
                apex.jQuery(document).unbind("keydown.menu_keys_colsearch");
			}
		}

		/**
		 * This returns an object that can be used in dialog validate or highlight save or filter save
		 * @function
		 */
		this.dialog.util_exp_type = function(){
			var l_ob = {};
			l_ob.col = $x('apexir_COLUMN_NAME');
			l_ob.col_type = l_ob.col.options[l_ob.col.selectedIndex].className;
			l_ob.col_opt = $x('apexir_'+l_ob.col_type+'_OPT');
			l_ob.col_opt_val = $v(l_ob.col_opt);
			if(l_ob.col_type == 'DATE' && !(l_ob.col_opt_val=='is in the last' || l_ob.col_opt_val=='is not in the last' || l_ob.col_opt_val=='is in the next' || l_ob.col_opt_val=='is not in the next')){
				l_ob.form_items = ['apexir_BETWEEN_FROM','apexir_BETWEEN_TO'];
			}else{
				l_ob.form_items = ['apexir_EXPR','apexir_EXPR2'];
			}
			return l_ob;
		}

		this.dialog.validate = function(pThis){
			var lTest = [];
			var l_OB = that.dialog.util_exp_type();
			switch(true){
				case (l_OB.col_opt_val=='between'):
					lTest = [l_OB.form_items[0],l_OB.form_items[1]];
				break;
				case (l_OB.col_opt_val=='is null' || l_OB.col_opt_val=='is not null'):
					lTest = [];
				break;
				case (l_OB.col_opt_val=='is in the last' || l_OB.col_opt_val=='is not in the last' || l_OB.col_opt_val=='is in the next' || l_OB.col_opt_val=='is not in the next'):
					lTest = [l_OB.form_items[0],'apexir_EXPR3'];
				break;
				default:
					lTest = [l_OB.form_items[0]];
			}
			if($f_get_emptys(lTest,'error','')){
				return false;
			}else{
				return l_OB;
			}
			//return $f_get_emptys(lTest,'error','');
		}

		/**
		 * @namespace
		 * This namespace holds all functions related to pulling or running page controls
		 * */
		this.controls = {};

		/** resets the control dialog */
		this.controls.cancel = function(){
			that.dialog.reset();
		}

		/**
		 * Toggles betweeen the two different save dialogs
		 * */
		this.controls.save_toggle = function(pThis){
			var lTest = ($v(pThis)=='NAMED')?'apexir_SAVE_NAMED':'apexir_SAVE_DEFAULT';
			$x_HideSiblings(lTest);
		}

		/**
		 * @function
		 * */
		this.controls.drop = function(){
			return that.item.control_panel_drop();
		}

		/**
		 * @function
		 * standard call to get a control
		 * */
        this.controls.get =	function(pControl,pID){
			that.dialog.reset();
			that.l_Action = 'CONTROL';
			that.l_Type = pControl;
			that.current_control= pControl;
			that.current_dom = $x(pID)
			if(pID){that.current_col_id = pID;}
			that._Get('CONTROL',pControl,false,pID);
		}

		/**
		 * Controls Dropdown
		 * @function
		 * */
        this.controls.menu = function(pThis,pId){
			that.dialog.reset();
			app_AppMenuMultiOpenBottom2(pThis,pId,false);
		}

        /**
		 * Pull IR table header widget.
		 * @function
		 */
        this.controls.widget = function(pID){
			this.get('SORT_WIDGET',pID);
			that.current_col_dom = $x(pID);
			document.body.onclick = that.dialog.check;
		}

		/**
		 * Pulls a Narrow Filter dropdown on a text area
		 *@function
		 */
		this.controls.narrow = function(pID){
			that.supress_update = true;
			that.temp_return_element = $x(pID);
			this.get('NARROW',$v('apexir_COLUMN_NAME'));
		}

		/**
		 * Pulls a Format Mask dropdown on a text area
		 *@function
		 */
		this.controls.format_mask = function(pID){
			that.supress_update = true;
			that.temp_return_element = $x(pID);
			this.get('FORMAT_MASK_LOV');
		}

		/**
		 * @ignore
		 * */
		this.controls.col_lov =			function(pID){
			this.get('COL_LOV',pID);
		}

		/**
		 * @ignore
		 * */
		this.controls.radio_lov =			function(pID){
			this.get('RADIO_LOV',pID);
		}

		/**
		 * @ignore
		 * */
        this.controls.filter2 =			function(){
			that.controls.set = true;
			this.get('SHOW_FILTER');
		}

		this.group_by = {};

		this.group_by.view = function(){
			  that.action('VIEW_GROUP_BY',false,false);
		 }

		this.group_by.save=function(){
		    that.supress_update = true;
			that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
			that.action('GROUP_BY_SAVE');
		}

		this.group_by.remove=function(){
			 that.action('GROUP_BY_REMOVE');
		}

		/**
		 * Controls any adjustments needed when displaying group by control
		 * @function
		 */
        this.group_by.control= function(pThis){
            var elId, numFunctions, rowNum;
            if(pThis){
                elId = pThis.id;
                numFunctions = ["SUM","AVG","MAX","MIN","MEDIAN","RATIO_TO_REPORT_SUM"];
                rowNum = elId.substr(elId.length-2);
                if(apex.jQuery.inArray($v(pThis), numFunctions )!==-1){
                    $x_Show_Hide('NUMBER_COLUMNS_'+rowNum,'ALL_COLUMNS_'+rowNum);
                }else{
                    $x_Show_Hide('ALL_COLUMNS_'+rowNum,'NUMBER_COLUMNS_'+rowNum);
                }
            }
        }

		/**
		 * @ignore
		 * */
        this.controls.row =				function(pID){
			this.get('SHOW_DETAIL',pID);
		}

		/**
		 * @ignore
		 * */
        this.controls.computation =		function(pID){
			pID = (pID)?pID:that.dialog.id;
			this.get('SHOW_COMPUTATION',pID);
			that.dialog.id = false;
		}

		/**
		 * @ignore
		 *
		this.controls.download =		function(pID){
			this.get('SHOW_DOWNLOAD',pID);
		}
		*/
		/**
		 * @ignore
		 * */
		this.controls.info=				function(pID){
			this.get('INFO',that.current_col_id);
		}

		/**
		 * @namespace
		 * */
		this.highlight = {};

		/**
		 * Removes a higlight rule.
		 * @param {String} pId highlight id
		 * */
		this.highlight.clear = function(pId){
			that.action('CLEAR_HIGHLIGHT',false,$nvl(pId,$x('HIGHLIGHT_ID').value));
		}

		/**
		 * toggles a higlight rule
		 * @param {Dom node} pThis this must be a checkbox for now
		 * @param {String} pId highlight id
		 * */
		this.highlight.toggle = function(pThis,pId){
			that.action('TOGGLE_HIGHLIGHT',false,pId,(pThis.checked)?'Y':'N');
		}

		/**
		 * @function
		 * Save a highlight rule (Step 1 of 2) next call is
		 **/
		this.highlight.save = function(pThis){
			var l_OB = that.dialog.validate();
			if(!l_OB){return}
			that.supress_update = true;

			that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
			that.action('SAVE_HIGHLIGHT');
			l_OB = null;
			return;
		}

		/**
		 * @namespace
		 * */
		this.navigate = {};

		/**
		 * @ignore
		 * */
		this.navigate.paginate = function(pThis){
			that.get.addParam('p_widget_num_return',pThis.split('max_rows=')[1].split('rows_fetched')[0])
			that.action('PAGE',pThis);
		}

		/**
		 * @namespace
		 * */
		this.column = {};

		/**
		 * @ignore
		 * */
		this.column.pOb = this;

		/**
		 * @ignore */
		this.column.break_on = function(pThis){
			if(pThis){that.current_col_id = pThis};
			that.action('BREAK',false,that.current_col_id);
		}

		/**
		 * @ignore */
		this.column.break_toggle = function(pThis,pId){
			that.action('BREAK_TOGGLE',false,pId,(pThis.checked)?'Y':'N');
		}

		/**
		 * @ignore
		 * hides column*/
		this.column.hide=function(pThis){
			var lValue = (!!pThis)?pThis:that.current_col_id;
			that.action('HIDE',false,lValue);
		}

		/**
		 * @ignore
		 * applies quick filter and filter dialog
		 * */
		this.column.filter=function(pThis){
			var lAction = 'ADD';
			if(pThis){
				that.current_col_id = pThis;
				lAction = 'UPDATE';
			}else{
				pThis = $v('apexir_COLUMN_NAME');
			}
			if ($v('apexir_FILTER_TYPE')=='COLUMN'){
			    var l_OB = that.dialog.validate();
			    if(!l_OB){return}
			    var lTemp = ['apexir_FILTER_TYPE',l_OB.col,l_OB.col_opt,l_OB.form_items[0],l_OB.form_items[1],'apexir_EXPR3'];
			}else{
			    var lTemp = ['apexir_FILTER_TYPE','apexir_FILTER_EXPR','apexir_FILTER_NAME'];
			}

			that.get.AddArrayItems(lTemp,1);
			that.supress_update = true;
			that.action('FILTER',lAction,pThis);
		}

		/**
		 * @function
		 * Deletes the display of search or a filter.
		 * */
		this.column.filter_delete=function(pThis){
			that.action('FILTER_DELETE',false,pThis);
		}

        /**
		 * @function
		 * toggle display of search or a filter
		 * */
		this.column.filter_toggle=function(pThis,pId){
			that.action('FILTER_TOGGLE',false,pId,(pThis.checked)?'Y':'N');
		}

		/**
		 * @ignore
		 */
		this.column.filter_control = function(pThis){
				if($v('apexir_FILTER_TYPE')=='COLUMN'){
				    $x_Show('apexir_COLUMN_FILTER');
					$x_Hide('apexir_ROW_FILTER');
				}else{
					$x_Show('apexir_ROW_FILTER');
					$x_Hide('apexir_COLUMN_FILTER');
				}

		}

		/**
		 * @function
		 * applies columns to display
		 * */
		this.column.display=function(pThis){
			var lTemp = [];
			var lSelects = $x('apexir_shuttle2').getElementsByTagName('SELECT')[0];
			for(i=0,len = lSelects.options.length;i<len;i++){lTemp[lTemp.length]=lSelects.options[i].value}
			that.get.AddArray(lTemp,1)
			that.action('SET_COLUMNS');
		}

		/**
		 * @function
		 * applies quick sort and order dialog
		 * */
		this.column.order=function(pThis){
			if(pThis == 'ASC' || pThis=='DESC'){
					that.get.addParam('f01',that.last_col_id);
					that.get.addParam('f02',pThis);
					that.action('COLUMN_ORDER');
			}else{
				that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
				that.action('SORT');
			}
		}

		/**
		 * @function
		 * applies control break dialog
		 * */
		this.column.break_save=function(pThis){
			 if(pThis == 'ENABLE' || pThis=='DISABLE'){
					   that.get.AddNameValue('apexir_COLUMN_01',that.last_col_id,1);
					   that.get.AddNameValue('apexir_ENABLE_01',pThis,2);
					   /*
						that.get.addParam('f01',that.last_col_id);
					   that.get.addParam('f02',pThis);
					   */
			 }else{
				that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
			 }
			 that.action('SAVE_BREAK');
		}

		/**
		 * @namespace
		 * */
		this.flashback = {}

		/**
		 * @function
		 * Calls flashback to pull report data from N minutes ago (Step 1 of 2)
		 */
		this.flashback.save = function(){
			that.supress_update = true;
			that.action('FLASHBACK_SET',false,false,$v('apexir_FLASHBACK_TIME'));
		}

		/**
		 * @ignore
		 */
		this.flashback.clear = function(){
			that.action('FLASHBACK_CLEAR',false,false,false);
		}

		/**
		 * @ignore
		 */
		this.flashback.toggle = function(){
			that.action('FLASHBACK_TOGGLE',false,false,false);
		}

		/**
		 * @namespace
		 * */
		this.aggregate = {}

		/**
		 * Controls any adjustments needed when displaying aggregate control
		 * @function
		 */
		this.aggregate.control = function(){
		    var lAggregateBy,lArray;
		    lAggregateBy = $v('apexir_AGGREGATE_BY');
		    lArray = ['COUNT','COUNT_DISTINCT'];
		    if (apex.jQuery.inArray(lAggregateBy,lArray)!==-1){
				$x_Show_Hide('ALL_COLUMNS','NUMBER_COLUMNS');
			}else{
				$x_Show_Hide('NUMBER_COLUMNS','ALL_COLUMNS');
			}
		}

		/**
		 * Save an aggregate on report
		 * @function
		 */
		this.aggregate.save = function(){
		    var lAggregateBy, lAggregation, lArray, lCol;
		    lAggregateBy = $v('apexir_AGGREGATE_BY');
		    lAggregation = $v('apexir_AGGREGATION');
		    lArray = ['COUNT','COUNT_DISTINCT'];
		    lCol = (apex.jQuery.inArray(lAggregateBy,lArray)!==-1)?$v('apexir_COLUMN_NAME_ALL'):$v('apexir_COLUMN_NAME');
			that.action('SAVE_AGGREGATE', false, lAggregation, lAggregateBy, lCol);
		}

		/**
		 * Delete an Aggrigation
		 * @function
		 */
		this.aggregate.clear = function(){
			that.action('DELETE_AGGREGATE',false,$v('apexir_AGGREGATION'));
		}

		/**
		 * @ignore
		 **/
		this.aggregate.toggle = function(){}

		/**
		 * @namespace
		 **/
		this.computation = {}

		/**
		 *	Part 1 : save computation this is a 2 part process first to see if the computation is valid then show messaging or to repull the report
		 *	@function
		 **/
		this.computation.save = function(){
			var lValidate = $f_get_emptys(['apexir_COLUMN_LABEL','apexir_COMPUTATION_EXPR'],'error','');
			if(!!lValidate){return}
			that.supress_update = true;
			that.action('SAVE_COMPUTATION',false,$v('apexir_COMPUTATION_ID'),$v('apexir_COLUMN_LABEL'),$v('apexir_REPORT_LABEL'),$v('apexir_FORMAT_MASK'),$v('apexir_COMPUTATION_EXPR'));
		}

		/**
		 * @ignore
		 */
		this.computation.clear = function(){
			that.action('DELETE_COMPUTATION',false,$v('apexir_COMPUTATION_ID'));
		}

		/**
		 * @ignore
		 */
		this.computation.toggle = function(){}

		/**
		 * @namespace
		 */
		this.chart= {}

		/**
		 * @ignore
		 */
		this.chart.control = function(pThis){
				var l_P = [];
				l_P[0] = $x('apexir_LABEL_AXIS_TITLE').parentNode
				l_P[1] = $x('apexir_VALUE_AXIS_TITLE').parentNode
				l_P[2]= l_P[0].previousSibling;
				l_P[3] = l_P[1].previousSibling;
				if($x('apexir_CHART_TYPE_2').checked){
					$x_Hide(l_P);
				}else{
					$x_Show(l_P);
				}

		}

		/**
		 * calls SAVE_CHART action with the items posted to the f01 array in the order listed
		 * @function
		 */
		this.chart.save = function(){
			that.supress_update = true;		    
			var lTemp = ['apexir_CHART_TYPE','apexir_CHART_LABEL','apexir_CHART_VALUE','apexir_AGGREGATE_BY','apexir_LABEL_AXIS_TITLE','apexir_VALUE_AXIS_TITLE','apexir_SORT'];
			that.get.AddArrayItems(lTemp,1);
			that.action('SAVE_CHART');
		}

		/**
		 * @namespace
		 */
		this.chart.clear = function(){
			that.action('DELETE_CHART');
		}

		/**
		 *@function
		 *calls SAVE_CHART action with the items posted to the f01 array in the order listed
		 */
		this.chart.view = function(){
			that.action('VIEW_CHART',false,false);
		}

        /**
		 * @namespace
		 */
		this.email= {}

        /**
		 * calls SEND_EMAIL action with the items posted to the f01 array in the order listed
		 * @function
		 */
		this.email.send = function(){
		    var lValidate = $f_get_emptys('apexir_EMAIL_TO','error','');
			if(!!lValidate){return}
			//that.supress_update = true;

			var lTemp = ['apexir_EMAIL_TO','apexir_EMAIL_CC','apexir_EMAIL_BCC','apexir_EMAIL_SUBJECT','apexir_EMAIL_BODY'];
			that.get.AddArrayItems(lTemp,1);
			that.action('SEND_EMAIL');
		}

		/** @ignore */
		this.email.show = function(pThis){
		    $x_Show_Hide(['apexir_EMAIL','apexir_EMAIL_BUTTON'],'apexir_DOWNLOAD_BUTTON');
		}

		/**
		 * @namespace
		 */
		this.calendar= {}

		/**
		 * @ignore
		 */
		this.calendar.save = function(){
			that.action('SAVE_CALENDAR',false,false,$v('DATE_COLUMN'),$v('DISPLAY_COLUMN'));
		}

		/**
		 * @ignore
		 */
		this.calendar.view = function(){
			that.action('VIEW_CALENDAR',false,false);
		}

		/**
		 * @namespace
		 */
		this.data = {}

		/**
		 * @ignore
		 */
        this.data.view =function(pViewMode) {
			if (pViewMode) {
			    that.get.addParam('p_widget_view_mode', pViewMode);
			}
			that.action('VIEW_REPORT',false,false);
		}

		/**
		 * @namespace
		 * */
		this.detail = {}

		/** @ignore */
		this.detail.last_row_opened = false;

		/** @ignore */
		this.detail.show = function(pThis){
			if(that.ajax_busy){return;}
			$x_Show(getElementsByClass('displayed'));
			$x_Show(getElementsByClass('other'));
			if($x('apexir_EXCLUDE_NULL_0').checked && $x('apexir_DISPLAY_OPTION').checked){
				$x_Hide(getElementsByClass('null'));
				$x_Hide(getElementsByClass('other'));
			}else if ($x('apexir_EXCLUDE_NULL_0').checked) {
				$x_Hide(getElementsByClass('null'));
			}else if ($x('apexir_DISPLAY_OPTION').checked) {
				$x_Hide(getElementsByClass('other'));
			}
			that.l_Action = 'CHANGE_DETAIL_OPTION';
			that.supress_update = true;
			that.action('CHANGE_DETAIL_OPTION',false,false,$v('apexir_EXCLUDE_NULL_0'),$v('apexir_DISPLAY_OPTION'));
		}

		/**
		 * Delete current worksheet report
		 * @function
		 * */
		this.remove = function(pAction){
		    var lAction;
		    lAction = $nvl(pAction,'DELETE');
			that.action(lAction,false,false,false);
		}

		/**
		 * Reset current  worksheet report to initial state
		 * @function
		 * */
		this.reset = function(){
			that.action('RESET',false,false,false);
		}

		/**
		 * Save or Rename current worksheet report state
		 * @function
		 * @param {} pThis
		 * */
		this.save = function(pThis){
			var l_c = $x('create_category');
			var lValidate = $f_get_emptys(['apexir_WORKSHEET_NAME'],'error','');
			if(l_c){lValidate = $f_get_emptys([l_c],'error','')};
			if(!!lValidate){return}
			l_cat = (l_c)?l_c.value:$v('apexir_WORKSHEET_CATEGORY');
			pThis =(!!pThis)?'RENAME':'SAVE';
			that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
			that.action(pThis,false,false);
		}

        /**
        cbcho
        * */
        this.get_saved_rpt_xml = function (){
        	var l_Select = $x('apexir_SAVED_REPORTS');
        	var get = new htmldb_Get(null,$v('pFlowId'),'INTERNAL_APPLICATION_PROCESS=ir_reports_select_xml2',$v('pFlowStepId'));
        	get.addParam('x01',$v('apexir_WORKSHEET_ID'));
		    get.addParam('x02',that.report_id);
        	gReturn = get.get('XML');
        	if(gReturn && l_Select){
        		var l_Count = gReturn.getElementsByTagName("option").length;
        		l_Select.length = 0;
        		for(var i=0;i<l_Count;i++){
        			var l_Opt_Xml = gReturn.getElementsByTagName("option")[i];
        			that.appendToSelect(l_Select, l_Opt_Xml.getAttribute('value'), l_Opt_Xml.firstChild.nodeValue)
        		}
        	}
        	get = null;
         }

         this.appendToSelect = function (pSelect, pValue, pContent){
            var l_Opt = document.createElement("option");
            l_Opt.value = pValue;
        	if(document.all){/* why is ie different ask bill */
        		pSelect.options.add(l_Opt);
        		l_Opt.innerText = pContent;
        	 }else{
        		l_Opt.appendChild(document.createTextNode(pContent));
        		pSelect.appendChild(l_Opt);
        	}
          }

		/**
		 * Save current worksheet report state as default worksheet report
		 * @function
		 * */
		this.save_default = function(pAction){
            var lAction, lValidate;
            if ($v('apexir_DEFAULT_TYPE')==='ALTERNATIVE'){
                lValidate = $f_get_emptys(['apexir_WORKSHEET_NAME'],'error','');
            }
            if(!!!lValidate){
                lAction = $nvl(pAction, 'SAVE_DEFAULT');
                that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),1);
                that.action(lAction,false,false,false);
            }
        }

        /** @ignore */
        this.save_default_type_check = function(){
            if($v('apexir_DEFAULT_TYPE')==='PRIMARY'){
                $x_ItemRow('apexir_WORKSHEET_NAME','HIDE');
            }else{
                $x_ItemRow('apexir_WORKSHEET_NAME','SHOW');
            }
        }

		/** @ignore */
		this.save_category_check = function(pThis){
			if(pThis.value=='new'){$dom_AddInput(pThis.parentNode,'text','create_category','');}
			else{$x_Remove('create_category')}
		}

		/**
		 * Pull worksheet report setting pThis to current report
		 * @function
		 * @param {String} [pThis] worksheet id
		 * */
		this.pull = function(pThis,pAction){
			if(!!pThis){that.report_id = pThis;}
			that._Get('PULL',pAction);
		}

		/** @ignore */
		this.download=function(pThis){}



		/**
		 * @namespace for keyboard access related functions
		 */

		this.key= {};
		
		//set up globals for the key namespace
        this.key.$gAnchorList;

		/**
		 * @namespace
		 */
        this.key.navigate = {};
        /**
         * if up, set focus to previous anchor
         *
         */
        this.key.navigate.up = function(event) {
            var $lCurrent, lIndex;
            $lCurrent = apex.jQuery(event.target);
            lIndex = that.key.$gAnchorList.index($lCurrent);
            that.key.$gAnchorList.eq(lIndex - 1).focus();
            event.preventDefault();
        }
        /**
         * if down, set focus to next anchor
         */
        this.key.navigate.down = function(event) {
            var $lCurrent, lIndex;
            $lCurrent = apex.jQuery(event.target);
            lIndex = that.key.$gAnchorList.index($lCurrent)
            that.key.$gAnchorList.eq(lIndex + 1).focus();
            event.preventDefault();
        }

        /**
         * if esc, reset and hide
         */
        this.key.navigate.esc = function(event, $pReturnFocus, pEventNamespace) {
            that.dialog.reset();
            apex.jQuery(document).unbind('keydown.' + pEventNamespace + '');
            $pReturnFocus.focus();
            event.preventDefault();
        }
        
        /**
         * if return, reset base current level. don't preventDefault
         */
        this.key.navigate.enter = function(pEventNamespace) {
            apex.jQuery(document).unbind('keydown.' + pEventNamespace + '');
        }


        /**
         *
         */
        this.key.action = function(event, $pReturnFocus, pEventNamespace) {
            // switch on the key code
            switch (event.which) {
            case 40:
                that.key.navigate.down(event);
                break;
            case 38:
                that.key.navigate.up(event);
                break;
            case 27:
                that.key.navigate.esc(event, $pReturnFocus, pEventNamespace);
                break;
            case 13:
                that.key.navigate.enter(pEventNamespace);
                break;
            default:
                null;
            }
        }
		/**
		 * @namespace
		 */
		this.notify= {}

        /**
         * Saves a report notification via SAVE_NOTIFY action, passing item values via f01
         */
        this.notify.save = function(){
			that.supress_update = true;

    	    var lTemp = ['apexir_NOTIFY_ID','apexir_EMAIL_ADDRESS','apexir_EMAIL_SUBJECT',
                         'apexir_NOTIFY_INTERVAL','apexir_START_DATE','apexir_START_HH','apexir_START_AM_PM',
                         'apexir_END_DAY','apexir_END_DAY_UNIT'];
		    that.get.AddArrayItems2(lTemp,1);
            that.action('SAVE_NOTIFY');
        }

        /**
		 * @ignore
		 */
        this.notify.clear = function(){
			 that.action('DELETE_NOTIFY',false,$v('apexir_NOTIFY_ID'));
		}


		/**
		 * Runs the basic search functionality of the worksheet.
		 * @param {String} [pThis] if set to SEARCH check
		 *
		 * */
		this.search=function(pThis, pRows){
			var lSearch = that.item.search();
			var lSearch_Col = that.item.search_column();
            var lReport = $v('apexir_REPORT_ID');
			var lTemp;
			if (pThis='SEARCH') {
				if (pRows) {
				    that.get.addParam('p_widget_num_return', pRows);
				} else {
				    if ($x('apexir_NUM_ROWS')) {
				        that.get.addParam('p_widget_num_return', $v('apexir_NUM_ROWS'));
				    }
				}
			}

			if ($v_IsEmpty(lSearch)) {
				that.get.AddArrayItems2($x_FormItems('apexir_TOOLBAR'),1);
				that.pull(lReport);
			} else {
				if (pThis='SEARCH') {
					//lTemp = [$v('apexir_CURRENT_SEARCH_COLUMN'),'contains',$v(lSearch),$v('apexir_NUM_ROWS')];
					that.get.AddArrayItems2($x_FormItems('apexir_TOOLBAR'), 1);
					pThis = 'QUICK_FILTER';
				} else {
					lTemp = [this.current_col_id, '=', $v(lSearch)];
					pThis = 'FILTER';
				    that.get.AddArray(lTemp,1);
				}
				that.action(pThis, 'ADD');
			}
			$s(lSearch, '');
		}


		/**
		 * generic call for returning dialogs with validation
		 * @function
		 */
		this.valid_action = function(pTest){
			if(pTest == 'true'){
				that.pull();
			}else{
				$s('apexir_DIALOG_MESSAGE',pTest);
			}
		}

		/**
		 * the basic AJAX call for ACTIONs for the ws.
		 * @param {String} p_widget_action widget action to call
		 * @param {String} [p_widget_action_mod] modify widget action to call
		 * @param {String} [p_id] id of whatever you want to send
		 * @param {String} [p_value] value of whatever you want to send
		 * @param {String} [p_x05] extra value
		 * @param {String} [p_x06] extra value
		 * @param {String} [p_x07] extra value
		 * @param {String} [p_x08] extra value
		 * @param {String} [p_x09] extra value
		 * @param {String} [p_x10] extra value
		 * */
		this.action = function(p_widget_action,p_widget_action_mod,p_id,p_value,p_x05,p_x06,p_x07,p_x08,p_x09,p_x10){
			that.l_Action = p_widget_action;
			that._Get('ACTION',p_widget_action,p_widget_action_mod,p_id,p_value,p_x05,p_x06,p_x07,p_x08,p_x09,p_x10);
		}

		/**
		 * @function
		 * This function is the base worksheet ajax call
		 * */
	this._Get = function(p_widget_mod,p_widget_action,p_widget_action_mod,p_id,p_value,p_x05,p_x06,p_x07,p_x08,p_x09,p_x10){
		if(that.ajax_busy){
			return;
		}else{
			that.ajax_busy = true;
		}
        
        // General loading for all AJAX calls
		that._Loading();

		var lArray;
		lArray = ['ACTION', 'PULL'];
		// We only want to do certain things when doing some action (applying a new filter, adding a highlight)
		// or a pull (simple search).
		if (p_widget_mod && apex.jQuery.inArray(p_widget_mod, lArray) !== -1 ) {
		    // Trigger the before refresh event, and pass the current report_id for convenience.
    		// Event is only triggered when the actual report information is refreshed. This is
    		// identified by widget module values of either 'PULL' or 'ACTION'.
    		// Event is triggered on table element with the id of the worksheet ID. This element
    		// is part of the refresh.
    		// Event handlers can be bound to this element in conjunction with the jQuery 'live'
    		// bind type, or can be bound to higher element (such as the main region ID) and use
    		// the regular bind type. The latter works because the event bubbles and is how this
    		// is handled within the dynamic action framework.
		    apex.jQuery('#' + $v("apexir_WORKSHEET_ID")).trigger('apexbeforerefresh', that.report_id);

            // Add page items to submit, if there are any
    		if (!!that.external_items) {
    		 /*
    		  * if there are external items to submit
    		  * */
    		  var l_external_items = that.external_items.split(",");
    		  that.get.AddPageItems(l_external_items);
    		}		    
		}

		that.get.addParam('p_widget_name','worksheet');
		(!!p_widget_mod)?that.get.addParam('p_widget_mod',p_widget_mod):null;
		(!!p_widget_action)?that.get.addParam('p_widget_action',p_widget_action):null;
		(!!p_widget_action_mod)?that.get.addParam('p_widget_action_mod',p_widget_action_mod):null;
		that.get.addParam('x01',$v('apexir_WORKSHEET_ID'));
		(!!that.report_id)?that.get.addParam('x02',that.report_id):'0';
		(!!p_id)?that.get.addParam('x03',p_id):null;
		(!!p_value)?that.get.addParam('x04',p_value):null;
		/*
		 * extra values
		 */
		(!!p_x05)?that.get.addParam('x05',p_x05):null;
		(!!p_x06)?that.get.addParam('x06',p_x06):null;
		(!!p_x07)?that.get.addParam('x07',p_x07):null;
		(!!p_x08)?that.get.addParam('x08',p_x08):null;
		(!!p_x09)?that.get.addParam('x09',p_x09):null;
		(!!p_x10)?that.get.addParam('x10',p_x10):null;
		/*
		if($x('apexir_CONTROL_PANEL_DROP')){
			console.log($x_FormItems('apexir_CONTROL_PANEL_DROP'));
			that.get.AddArrayItems2($x_FormItems('apexir_CONTROL_PANEL_DROP'),4);
		}
		*/
		that.get.GetAsync(that._Return);
	}

	/**
	 * Gets called before all worksheet AJAX calls
	 * @function
	 */
	this._Loading = function(){
		$x_Hide('apexir_rollover');
		that._BusyGraphic(1);
		return;
	}

	/**
	 * Gets called after all worksheet AJAX calls
	 * @function
	 */
	this._Finished_Loading = function(){
		that._BusyGraphic(4);
		/**
		 *This fixes IE bug for a style section in AJAX pulled HTML snippet.
		 */
		if(ie){
			if(!!!that.lCSS){
				that.lCSS = document.createStyleSheet();
			}
			startTag = '<style id="apexir_WORKSHEET_CSS" type="text/css">' ;
			endTag = '</style>';
			var start = p.responseText.indexOf(startTag);
			var response  = p.responseText.substring(start+startTag.length);
			var end   = response.indexOf(endTag);
			response  = response.substring(0,end);
			that.lCSS.cssText = response;
		}
		document.onclick = null;
		that.init();
		if(gReport.websheet){
				gReport.websheet.init();
		}
		if(!!that.l_LastFunction){
			that.l_LastFunction();
			that.l_LastFunction = false;
		}
		that.ajax_busy = false;

		return;
	}


	/**
	 * This function shows a loading graphic based on readyState
	 *@function
	 */
	this._BusyGraphic = function(pState){
		if(pState == 1) {
			$x_Show('apexir_LOADER');
		}
		else{$x_Hide('apexir_LOADER');}
		return;
	}

	/**
	 * This function is the central control for AJAX returns in the worksheet
	 * @function
	 */
	this._Return = function(){
		if(p.readyState == 1){
		}else if(p.readyState == 2){
		}else if(p.readyState == 3){
		}else if(p.readyState == 4){

			$x_Hide('searchdrop').innerHTML='';
			if(that.l_Action=='CONTROL'){
				if(that.current_control == 'COL_LOV'){
					/* HACK !!!! */
					var lBuild = new $d_LOV_from_JSON();
					lBuild.l_Type = 'SELECT';
					lBuild.create(gReport.controls.lov_dom.parentNode,p.responseText);
					$s(lBuild.l_Dom,gReport.controls.value);
					lBuild.l_Dom.focus();
					lBuild.l_Dom.id='killme';
					apex.jQuery(lBuild.l_Dom).one('blur change', function(e){cellsave(e,this)})
					/* HACK !!!! */
				}else if(that.current_control == 'RADIO_LOV'){
					var lBuild = new $d_LOV_from_JSON();
					lBuild.l_Type = 'RADIO';
					lBuild.create(  gReport.controls.lov_dom,
					                p.responseText,
					                null,
					                null,
					                gReport.controls.value,
					                true);
                    // open created div as a dialog, use lDialog which is available as a closure from CellInit
                    lDialog.dialog({
                        bgiframe    : true,
                        draggable   : false,
                        show        : 'drop',
                        hide        : 'drop',
                        modal       : true,
                        position    : [gReport.websheet.currentForm.offsetLeft, gReport.websheet.currentForm.offsetTop + gReport.websheet.currentForm.offsetHeight],
                        close       : function(){
                            cellsave(null, gReport.websheet.currentForm);
                        }
                    });
					// Bind event handler to change event for all radios created. This will handle
					// setting the value back to the original cell, closing the dialog.
					// Save is handled by close event callback of the dialog, which catches both when something
					// has been selected and when the user closes the dialog without changing anything?!?!?!
                    apex.jQuery(':radio',gReport.controls.lov_dom).change(function(){
                        //set value back to original cell
                        $s(gReport.websheet.currentForm, this.id);
                        //close dialog
                        apex.jQuery(gReport.controls.lov_dom).dialog('close');
                    });
				}else if(that.current_control == 'SORT_WIDGET'){
					var myObject = $u_eval('(' + p.responseText + ')');
					that.dialog.id = myObject.dialog.id;
					var l = $x('apexir_rollover').getElementsByTagName('TABLE')[0];
					/*
					 * This code shows or hides control elements
					**/
					$x_Show_Hide([l,'apexir_sortup','apexir_sortdown','apexir_search','apexir_removefilter','apexir_hide','apexir_break','apexir_info','apexir_computation'],myObject.dialog.hide);
					$x_Hide(myObject.dialog.hide);
					/*
					 * The following code populates the unquie value list
					 */
					if(myObject.dialog.uv){
						var len = myObject.dialog.row.length
						for (var i=0;i<len;i++){
							if(myObject.dialog.row[i].R != null){
								if(myObject.dialog.row[i].D != null){var tDisplay = myObject.dialog.row[i].D}
								else {var tDisplay = myObject.dialog.row[i].R}
								var lTemp = $dom_AddTag('apexir_rollover_content','a',tDisplay);
								lTemp.apexir_RETURN_VALUE = myObject.dialog.row[i].R;
								lTemp.href= 'javascript:void(false);';
								lTemp.onclick = function(){
									var lTemp = [that.current_col_id,'=',this.apexir_RETURN_VALUE,'',''];
									that.get.AddArray(lTemp,1);
									that._Get('ACTION','COL_FILTER');
								}
							}
						}
						if(len > 10){$x_Style('apexir_rollover_content','height','210px')}
						else{$x_Style('apexir_rollover_content','height','')}
						$x_Show('apexir_rollover_content');
						$s('apexir_search','');
					}else{
						$x_Hide('apexir_rollover_content');
					}

					var pThis = $x(that.current_col_dom);
					$x_Show('apexir_rollover');
					$x_Style('apexir_rollover','left',(findPosX(pThis.parentNode))+'px');
					$x_Style('apexir_rollover','top',(findPosY(pThis)+pThis.offsetHeight+5)+'px');
					$x_Class(pThis.parentNode,'current');
					that.last_col_id = pThis.id;
					document.body.onclick = that.dialog.check;
				}else if(that.current_control == 'SHOW_FILTER' || that.current_control == 'SHOW_HIGHLIGHT'){
					var lDrop = that.item.control_panel_drop();
					apex.jQuery(lDrop).html(p.responseText);
					$x_Show(lDrop);
					/* switch out */
					ws_ColumnCheck($x('apexir_COLUMN_NAME'));

				}else if(that.current_control == 'SHOW_SAVE_DEFAULT'){
					var lDrop = that.item.control_panel_drop();
					$s(lDrop,p.responseText);
					$x_Show(lDrop);
					/* switch out */
					that.save_default_type_check();

				}else if(that.current_control == 'NARROW' || that.current_control == 'FORMAT_MASK_LOV'){
					var lBuild = new $d_LOV_from_JSON();
					lBuild.l_Type = 'a';
					lBuild.create(lDiv,p.responseText);
					lBuild.l_Dom.id='apexir_col_values_drop';
					$x_Style(lBuild.l_Dom,'height','200px');
					$x_Style(lBuild.l_Dom,'display','block');
					if(!$x('apexir_col_values_drop_space')){
						lThis = $dom_AddTag($x(that.temp_return_element).parentNode,'BR');
						lThis.id="apexir_col_values_drop_space";
					}
					$x(that.temp_return_element).parentNode.appendChild(lBuild.l_Dom);
					for(var i=0,len=lBuild.l_NewEls.length;i<len;i++){
						lTemp = lBuild.l_NewEls[i];
						lTemp.href="javascript:void(false)";
						if(that.current_control == 'NARROW'){
							lTemp.onclick = function(){
								var lCol = $x('apexir_COLUMN_NAME');
								var lOpt = $x('apexir_'+lCol.options[lCol.selectedIndex].className+'_OPT');
								var l_Test = $v(lOpt);
								if(l_Test == 'in' || l_Test == 'not in'){
									l_Test = $v(that.temp_return_element);
									$s(that.temp_return_element,(isEmpty(that.temp_return_element))?this.id:l_Test+','+this.id);
								}else{
									$s(that.temp_return_element,this.id);
									$x_Remove('apexir_col_values_drop');
								}
							}
						}else{
							lTemp.onclick = function(){
									$s(that.temp_return_element,this.id);
									$x_Remove('apexir_col_values_drop');
							}
						}
					}
					that.supress_update = false;
					that.l_Action = false;
					//set up event handler to handle when the user clicks outside the container
					document.body.onclick = that.dialog.check2;
				}else if(that.current_control == 'INFO'){
					var l = $x('apexir_rollover').getElementsByTagName('TABLE')[0];
					$x_Hide(l);
					$x_Show('apexir_rollover_content');
					$s('apexir_rollover_content',p.responseText);
					$x_Style('apexir_rollover_content','height','')
					var pThis = $x(that.current_col_dom);
					$x_Show('apexir_rollover');
					$x_Style('apexir_rollover','left',(findPosX(pThis.parentNode))+'px');
					$x_Style('apexir_rollover_content','top',(findPosY(pThis)+pThis.offsetHeight+5)+'px');
					$x_Class(pThis.parentNode,'current');
					that.last_col_id = pThis.id;
					document.body.onclick = that.dialog.check;
				}else if(that.current_control == 'SEARCH_COLUMN'){
					var lDiv = $x('apexir_SEARCHDROP');
					$s(lDiv,'');
					var lBuild = new $d_LOV_from_JSON();
					lBuild.l_Type = 'a';
					lBuild.create(lDiv,p.responseText);
					lBuild.l_Dom.id='apexir_columnsearch';
					var lSearch_Col = that.item.search_column();
					for(var i=0,len=lBuild.l_NewEls.length;i<len;i++){
						lBuild.l_NewEls[i].href='javascript:void(0)';
						lBuild.l_NewEls[i].onclick=function(){
							if(this.id != 0){
								$s('apexir_SEARCH_COLUMN_DROP',this.innerHTML+'');
								$s(lSearch_Col,this.id);
							}else{
								$s('apexir_SEARCH_COLUMN_DROP','');
								$s(lSearch_Col,'');
							}
							$x_Hide(lDiv);
							apex.jQuery('#apexir_SEARCH').focus();
							apex.jQuery(document).unbind("keydown.menu_keys_colsearch");
						};
					}
					$x_Show(lDiv);
					document.body.onclick = that.dialog.check;
        			that.key.$gAnchorList = apex.jQuery('#apexir_SEARCHDROP').children().children().filter('a');

                    apex.jQuery(document).bind("keydown.menu_keys_colsearch", function(event) {
                        var lKeyCodes = [40,38,27,13];
                        if (apex.jQuery("#apexir_SEARCHDROP:visible")[0] && apex.jQuery.inArray(event.which, lKeyCodes) !== -1 ) {
                            that.key.action(event, apex.jQuery('#apexir_SEARCHDROPROOT'), 'menu_keys_colsearch');
                        }
                    });

				}else if(that.current_control  == 'SHOW_DETAIL'){
						$x_Hide(that.item.worksheet_report());
						$s('apexir_DATA_PANEL','');
						$s('apexir_CONTROL_PANEL_DROP','');
						$s(that.item.worksheet_detail(),p.responseText);
						$x_Show(that.item.worksheet_detail());
				}else{
					var lDrop = that.item.control_panel_drop();
					apex.jQuery(lDrop).html(p.responseText);
					if(that.current_control  == 'SHOW_CHART'){
						that.chart.control();
					}
					if(that.controls.set){
						$s('apexir_COLUMN_NAME',that.last_col_id);
						$s('apexir_EXPR',$v('apexir_SEARCH'));
						that.controls.set = false;
						$s('apexir_SEARCH','');
					}
                    if(that.current_control == 'SHOW_COLUMN'){
                        window.g_Shuttlep_v01 = null;
                        if(!flowSelectArray){var flowSelectArray = [];}
                        flowSelectArray[2] = $x('apexir_SHUTTLE_LEFT');
                        flowSelectArray[1] = $x('apexir_SHUTTLE_RIGHT');
                        window.g_Shuttlep_v01 = new dhtml_ShuttleObject(flowSelectArray[2],flowSelectArray[1]);
                    }
					/* finally show */
					$x_Show(lDrop);
				}
		    // focus on first input field of drop panel, if any form fields exist within a drop panel
            var $lDropFields = apex.jQuery(':input:visible', apex.jQuery('#apexir_CONTROL_PANEL_DROP'));
            if ($lDropFields.length > 0) {
                $lDropFields[0].focus();
            }			
            /*
            }else if(that.l_Action=='DELETE'||that.l_Action=='SAVE_REPORT'||that.l_Action=='SAVE_DEFAULT'){
              console.log('in l_Action in DELETE, SAVE_REPORT, SAVE_DEFAULT');
              that.get_saved_rpt_xml();             */
			}else{
				if(that.supress_update){
					if(that.l_Action == 'SAVE_NOTIFY' || that.l_Action == 'SAVE_COMPUTATION' || that.l_Action == 'FLASHBACK_SET' || that.l_Action == 'SAVE_HIGHLIGHT' || that.l_Action == 'FILTER' || that.l_Action == 'GROUP_BY_SAVE' || that.l_Action == 'SAVE_CHART'){
						that.l_LastFunction =  function(){that.valid_action(p.responseText);}
					}
					that.ajax_busy = false;
				    that.supress_update = false;
					that.l_Action = false;
				}else{
					that.ajax_busy = false;
					$x_Hide('apexir_rollover');

					// Store current worksheet element locally
					lTemp = $x('apexir_WORKSHEET');
					// Rename this to '_old'
					lTemp.id = 'apexir_WORKSHEET_old';

					// Store current toolbar element locally
					var lTempToolbar = $x('apexir_TOOLBAR_OPEN');
					lTempToolbar.id = 'apexir_TOOLBAR_OPEN_old';

					var lTempActionsMenu = $x('apexir_ACTIONSMENU');
					var lTempFormatMenu = $x('apexir_FORMAT_MENU');
					var lTempRowsMenu = $x('apexir_ROWS_PER_PAGE_MENU');
					lTempActionsMenu.id = 'apexir_ACTIONSMENU_old';
					lTempFormatMenu.id = 'apexir_FORMAT_MENU_old';
					lTempRowsMenu.id = 'apexir_ROWS_PER_PAGE_MENU_old';
                    
                    // websheet 'Manage' menus
                    var lTempManageMenu = $x('apexir_WEBSHEETMENU');
                    if (lTempManageMenu) {
                        var lTempManageRows = $x('apexir_ROWS');
                        var lTempManageColumn = $x('apexir_COLUMN');
                        lTempManageMenu.id = 'apexir_WEBSHEETMENU_old';
                        lTempManageRows.id = 'apexir_ROWS_old';
                        lTempManageColumn.id = 'apexir_COLUMN_old';
                    }
                    
					// Create hidden, temporary div element, to be used to store the response containing the new elements
					lThis = $u_js_temp_drop();
					// Set the response text to the temporary div element
					$s(lThis,p.responseText);

					// Replace the old worksheet element (lTemp) with the new one (returned by $x call)
					lTemp.parentNode.replaceChild($x('apexir_WORKSHEET'), lTemp);

					// If the response contains the toolbar element, replace the old toolbar element (lTempToolbar)
					// with the new one (returned by $x call) and get rid of all old menu ul's (this is necessary
					// because the replaceChild call may not replace the menu ul's, because they may no longer be
					// within the apexir_TOOLBAR_OPEN div (if the menu's been used).
					if ($x('apexir_TOOLBAR_OPEN')) {
					    apex.jQuery('#apexir_ACTIONSMENU_old,#apexir_FORMAT_MENU_old,#apexir_ROWS_PER_PAGE_MENU_old').remove();
					    //websheet menus
					    if (lTempManageMenu) {
					        apex.jQuery('#apexir_WEBSHEETMENU_old,#apexir_ROWS_old,#apexir_COLUMN_old').remove();
					    }
					    lTempToolbar.parentNode.replaceChild($x('apexir_TOOLBAR_OPEN'), lTempToolbar);
					} else {
					    // If no toolbar is returned, just reset the original nodes back to their original IDs
    					lTempActionsMenu.id = 'apexir_ACTIONSMENU';
    					lTempFormatMenu.id = 'apexir_FORMAT_MENU';
    					lTempRowsMenu.id = 'apexir_ROWS_PER_PAGE_MENU';
    					//websheet menus
    					if (lTempManageMenu) {
                            lTempManageMenu.id = 'apexir_WEBSHEETMENU';
                            lTempManageRows.id = 'apexir_ROWS';
                            lTempManageColumn.id = 'apexir_COLUMN';  
                        }  					
					}

					$d_ClearAndHide('apexir_DETAIL');
					$s('apexir_CURRENT_SEARCH_COLUMN','');
					$s('apexir_SEARCH_COLUMN_DROP','');
					$x_Show('apexir_REPORT');
					$u_js_temp_clear();

					// Trigger the after refresh event, and pass the current report_id for convenience.
					// Event is triggered on table element with the id of the worksheet ID. This element
					// is part of the refresh.
					// Event handlers can be bound to this element in conjunction with the jQuery 'live'
					// bind type, or can be bound to higher element (such as the main region ID) and use
					// the regular bind type. The latter works because the event bubbles and is how this
					// is handled within the dynamic action framework.
					apex.jQuery('#' + $v("apexir_WORKSHEET_ID")).trigger('apexafterrefresh', that.report_id);
				}
			}
			that._Finished_Loading();
		}else{return false;}
	}
		this.dialog2 = this.controls.get;
return;

	/** @function
	 *	Initializes Interactive Report
	 * */
	function init(pId){
		this.l_Action = false;
		this.l_Type = false;
		that.ajax_busy = false;
		if(!!pId){that.worksheet_id = pId}
		that.report_id = ($v('apexir_REPORT_ID'))?$v('apexir_REPORT_ID'):'0';
		// Bind event handler to the apexrefresh event for the main region element. Dynamic actions can then
		// refresh the IRR via the 'Refresh' action.
		if ($x('apexir_REGION_ID')) {
    		apex.jQuery('#' + $v('apexir_REGION_ID')).bind('apexrefresh', function(){
    		    that.search();
    		});
    	}
		this.get = new htmldb_Get(null,$v('pFlowId'),'APXWGT',$v('pFlowStepId'));
	}

	}
}

