/*jslint nomen: false, evil: false, browser: true, eqeqeq: false, white: false, undef: false */
/*
Oracle Database Application Express, Release 3.1

B32468-02

Copyright © 2003, 2008, Oracle. All rights reserved.

Primary Author:  Carl Backstrom

The Programs (which include both the software and documentation) contain proprietary information; they are provided under a license agreement containing restrictions on use and disclosure and are also protected by copyright, patent, and other intellectual and industrial property laws. Reverse engineering, disassembly, or decompilation of the Programs, except to the extent required to obtain interoperability with other independently created software or as specified by law, is prohibited.

The information contained in this document is subject to change without notice. If you find any problems in the documentation, please report them to us in writing. This document is not warranted to be error-free. Except as may be expressly permitted in your license agreement for these Programs, no part of these Programs may be reproduced or transmitted in any form or by any means, electronic or mechanical, for any purpose.

If the Programs are delivered to the United States Government or anyone licensing or using the Programs on behalf of the United States Government, the following notice is applicable:

U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable Federal Acquisition Regulation and agency-specific supplemental regulations. As such, use, duplication, disclosure, modification, and adaptation of the Programs, including documentation and technical data, shall be subject to the licensing restrictions set forth in the applicable Oracle license agreement, and, to the extent applicable, the additional rights set forth in FAR 52.227-19, Commercial Computer Software--Restricted Rights (June 1987). Oracle USA, Inc., 500 Oracle Parkway, Redwood City, CA 94065.

The Programs are not intended for use in any nuclear, aviation, mass transit, medical, or other inherently dangerous applications. It shall be the licensee's responsibility to take all appropriate fail-safe, backup, redundancy and other measures to ensure the safe use of such applications if the Programs are used for such purposes, and we disclaim liability for any damages caused by such use of the Programs.

Oracle, JD Edwards, PeopleSoft, and Siebel are registered trademarks of Oracle Corporation and/or its affiliates. Other names may be trademarks of their respective owners.

The Programs may provide links to Web sites and access to content, products, and services from third parties. Oracle is not responsible for the availability of, or any content provided on, third-party Web sites. You bear all risks associated with the use of such content. If you choose to purchase any products or services from a third party, the relationship is directly between you and the third party. Oracle is not responsible for: (a) the quality of third-party products or services; or (b) fulfilling any of the terms of the agreement with the third party, including delivery of products or services and warranty obligations related to purchased products or services. Oracle is not responsible for any loss or damage of any sort that you may incur from dealing with any third party.
*/
/**
 * @fileOverview
 * This file holds all non namespaced functions and objects for Oracle Application Express
 *
 * */

var gColor = '#FFFFFF';

function d_Color(pThis){
 var lColor = $x('color_pallet_tab');
colorArray=['#993366','#CC6699','#FF99CC','#FF0099','#990066','#CC3399','#FF66CC','#CC0099','#FF33CC','#FF00CC','#FF00FF','#CC00CC','#FF33FF','#990099','#CC33CC','#FF66FF','#660066','#993399','#CC66CC','#FF99FF','#330033','#663366','#996699','#CC99CC','#FFCCFF','#CC00FF','#9900CC','#CC33FF','#660099','#9933CC','#CC66FF','#9900FF','#330066','#663399','#9966CC','#CC99FF','#6600CC','#9933FF','#6600FF','#330099','#6633CC','#9966FF','#3300CC','#6633FF','#3300FF','#0000FF','#0000CC','#000099','#000066','#000033','#3333FF','#3333CC','#333399','#333366','#6666FF','#6666CC','#666699','#9999FF','#9999CC','#CCCCFF','#0033FF','#0033CC','#3366FF','#003399','#3366CC','#6699FF','#0066FF','#0066CC','#3399FF','#003366','#336699','#6699CC','#99CCFF','#0099FF','#006699','#3399CC','#66CCFF','#0099CC','#33CCFF','#00CCFF','#00FFFF','#00CCCC','#009999','#006666','#003333','#33FFFF','#33CCCC','#339999','#336666','#66FFFF','#66CCCC','#669999','#99FFFF','#99CCCC','#CCFFFF','#00FFCC','#00CC99','#33FFCC','#009966','#33CC99','#66FFCC','#00FF99','#006633','#339966','#66CC99','#99FFCC','#00CC66','#33FF99','#00FF66','#009933','#33CC66','#66FF99','#00CC33','#33FF66','#00FF33','#00FF00','#00CC00','#009900','#006600','#003300','#33FF33','#33CC33','#339933','#336633','#66FF66','#66CC66','#669966','#99FF99','#99CC99','#CCFFCC','#33FF00','#33CC00','#66FF33','#339900','#66CC33','#99FF66','#66FF00','#66CC00','#99FF33','#336600','#669933','#99CC66','#CCFF99','#99FF00','#669900','#99CC33','#CCFF66','#99CC00','#CCFF33','#CCFF00','#FFFF00','#CCCC00','#999900','#666600','#333300','#FFFF33','#CCCC33','#999933','#666633','#FFFF66','#CCCC66','#999966','#FFFF99','#CCCC99','#FFFFCC','#FFCC00','#CC9900','#FFCC33','#996600','#CC9933','#FFCC66','#FF9900','#663300','#996633','#CC9966','#FFCC99','#CC6600','#FF9933','#FF6600','#993300','#CC6633','#FF9966','#CC3300','#FF6633','#FF3300','#FF0000','#CC0000','#990000','#660000','#330000','#FF3333','#CC3333','#993333','#663333','#FF6666','#CC6666','#996666','#FF9999','#CC9999','#FFCCCC','#FF0033','#CC0033','#FF3366','#990033','#CC3366','#FF6699','#FF0066','#CC0066','#FF3399','#660033','#FFFFFF','#CCCCCC','#999999','#666666','#333333','#000000'];
 for (i=0;i<colorArray.length;i++){
 d_ColorDiv(lColor,colorArray[i]);
 if(i % 13 == 13 - 1){
 var lBR = $dom_AddTag(lColor,'br');
 lBR.style.clear='both';
 lBR.style.height='1px';
  lBR.style.fontSize='1px';
 }
 }
}


function d_ColorDiv(pThis,r){
var lPick = $dom_AddTag(pThis,'div','<br />');
lPick.className='color';
lPick.style.backgroundColor = r;
if(ie){lPick.attachEvent ('onmouseover', function(){d_Current(r)});}
else{lPick.setAttribute("onmouseover","d_Current('"+r+"')");}
if(ie){lPick.attachEvent ('onclick', function(){passBack(r)});}
else{lPick.setAttribute("onclick","passBack('"+r+"')");}
return 
}

function d_Current(r){gColor = r;$x('current_color').style.backgroundColor = r;}

