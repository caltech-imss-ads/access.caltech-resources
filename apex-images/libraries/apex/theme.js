/**
 * @fileOverview
 * The {@link apex.theme} namespace is used to store functions used by all themes of Oracle Application Express.
 **/

/**
 * The {@link apex.theme} namespace is used to store functions used by all themes of Oracle Application Express.
 * @namespace
 **/
apex.theme = {};

(function( theme, navigation, $, undefined ) {
    "use strict";

    /**
     * @TODO Add documentation
     *
     * @memberOf apex.theme
     */
    theme.popupFieldHelpClassic = function ( pItemId, pSessionId ) {
        navigation.popup ({
            url:    "wwv_flow_item_help.show_help?p_item_id=" + pItemId + "&p_session=" + pSessionId + "&p_output_format=HTML",
            name:   "Help",
            width:  500,
            height: 350
        });
        return;
    }; //popupFieldHelpClassic


    /**
     * @TODO Add documentation
     *
     * @memberOf apex.theme
     */
    theme.popupFieldHelp = function ( pItemId, pSessionId ) {

        /* Show jQuery div based dialog if not running in screen reader mode, if we are using screen reader mode, use
         old new window popup.*/
        if ( !$x( "pScreenReaderMode" ) ) {
            $.getJSON(
                "wwv_flow_item_help.show_help?p_item_id=" + pItemId + "&p_session=" + pSessionId + "&p_output_format=JSON",
                function( pData ) {
                    var lDialog = $( "#apex_popup_field_help" );
                    if ( lDialog.length === 0 ) {

                        // add a new div with the retrieved page
                        lDialog = $( '<div id="apex_popup_field_help">' + pData.helpText + '</div>' );

                        // open created div as a dialog
                        lDialog
                            .dialog({
                            title:      pData.title,
                            bgiframe:   true,
                            width:      500,
                            height:     350,
                            show:       "drop",
                            hide:       "drop" });
                    } else {

                        // replace the existing dialog and open it again
                        lDialog
                            .html( pData.helpText )
                            .dialog( "option", "title", pData.title )
                            .dialog( "open" );
                    }
                }
            );
        } else {
            theme.popupFieldHelpClassic( pItemId, pSessionId );
        }
        return;
    }; //popupFieldHelp


})(apex.theme, apex.navigation, apex.jQuery);
