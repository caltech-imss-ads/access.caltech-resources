/**
 * @fileOverview
 * The {@link apex}.util namespace is used to store all utility functions of Oracle Application Express.
 **/

/**
 * @namespace
 **/
apex.util = {};

(function( util, $, undefined ) {

/**
 * Function that returns an array based on the value passed in pValue.
 *
 * @param {String|*} pValue If this is a string, then the string will be split into an array using the pSeparator parameter.
 *                          If it's not a string, then we try to convert the value with apex.jQuery.makeArray to an array.
 * @param {String} [pSeparator=":"] Separator used to split a string passed in pValue, defaults to colon if not specified.
 * @return {Array}
 *
 * @example
 * lProducts = apex.util.toArray( "Bags:Shoes:Shirts" );
 * lProducts = apex.util.toArray( "Bags,Shoes,Shirts", "," );
 * lTextFields = apex.util.toArray( jQuery("input[type=text]") );
 *
 * @function toArray
 * @memberOf apex.util
 **/
util.toArray = function( pValue, pSeparator ) {
    var lSeparator,
        lReturn = [];

    // If pValue is a string, we have to split the string with the separator
    if ( typeof pValue === "string" ) {

        // Default separator to a colon, if not supplied
        if ( pSeparator === undefined ) {
            lSeparator = ":";
        } else {
            lSeparator = pSeparator;
        }

        // Split into an array, using the defined separator
        lReturn = pValue.split( lSeparator );

    // If it's not a string, we try to convert pValue to an array and return it
    } else {
        lReturn = $.makeArray( pValue );
    }
    return lReturn;
}; // toArray

/**
 * Function that returns a string where all special HTML characters (&<>"'/) are escaped to prevent XSS attacks.
 * It provides the same functionality as sys.htf.escape_sc in PL/SQL.
 *
 * Note: This function should always be used when emitting untrusted data!
 *
 * @param {String} pValue   String which should be escaped.
 * @return {String} The escaped string.
 *
 * @example
 * jQuery("#show_user).append(apex.util.escapeHTML( $v("P1_UNTRUSTED_NAME") ));
 *
 * @function escapeHTML
 * @memberOf apex.util
 **/
util.escapeHTML = function(pValue) {
    return pValue.replace(/&/g, "&amp;")
                 .replace(/</g, "&lt;")
                 .replace(/>/g, "&gt;")
                 .replace(/"/g, "&quot;")
                 .replace(/'/g, "&#x27;")
                 .replace(/\//g, "&#x2F;");
}; // escapeHTML

/**
 * Function that returns a string where CSS special characters (:. ,<>"') are escaped. This gets around
 * the problem where jQuery has issues with ID selectors that may contain one of these characters. In this case,
 * these characters will be double escaped and treated literally, rather than as CSS notation.
 *
 * @param {String} pValue   String which should be escaped.
 * @return {String} The escaped string.
 *
 * @example
 * jQuery("#" + apex.util.escapeCSS( "my.id" ) );
 *
 * @function escapeCSS
 * @memberOf apex.util
 **/
util.escapeCSS = function(pValue) {
    return pValue.replace(/([:. ,<>"'])/g,'\\$1');
}; //escapeCSS

})(apex.util, apex.jQuery);
