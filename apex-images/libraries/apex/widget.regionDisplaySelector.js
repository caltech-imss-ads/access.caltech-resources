/**
 * @fileOverview
 * The {@link apex.widget}.regionDisplaySelector is used for the region display selector widget of Oracle Application Express.
 **/

(function( widget, $ ) {

/**
 * @param {String} pRegionDisplaySelectorRegion
 *
 * @function regionDisplaySelector
 * @memberOf apex.widget
 * */
widget.regionDisplaySelector = function( pRegionDisplaySelectorRegion ) {
    // store closures for our region display selector for better performance
    var lRegionDisplaySelector = $( "#" + pRegionDisplaySelectorRegion + "_RDS", apex.gPageContext$ ),
        lAllRegionLinks        = $( "a[href!=#SHOW_ALL]", lRegionDisplaySelector );

    // bind an event handler to all links in our region display selector
    $( lRegionDisplaySelector ).delegate( "a", "click", function( pEvent ) {
        var lClickedLink = this,
            lClickedRegionName = $( lClickedLink ).attr( "href" );

        if ( lClickedRegionName === "#SHOW_ALL" ) {
            // show all regions which are hidden
            lAllRegionLinks.each(function(){
                $( $( this ).attr( "href" ) + ":hidden", apex.gPageContext$ ).show();
            });
        } else {
            // hide all visible regions
            lAllRegionLinks.each( function() {
                $( $( this ).attr( "href" ) + ":visible", apex.gPageContext$ ).hide();
            });
            // and now show the clicked region
            $( lClickedRegionName ).show();
        }
        // remove the currently set "selected" CSS class and add it to the LI of our clicked link
        $( ".apex-rds-selected", lRegionDisplaySelector ).removeClass( "apex-rds-selected" );
        $( lClickedLink ).parent().addClass( "apex-rds-selected" );
        // don't fire the browser default link behavior so that the href is not displayed
        pEvent.preventDefault();
    });
}; // regionDisplaySelector

})( apex.widget, apex.jQuery );
