/**
 * @fileOverview
 * The {@link apex.widget}.selectList is used for the select list widget of Oracle Application Express.
 **/

(function( widget, $ ) {

/**
 * @param {String} pSelector  jQuery selector to identify APEX page item(s) for this widget.
 * @param {Object} [pOptions]
 *
 * @function selectList
 * @memberOf apex.widget
 * */
widget.selectList = function( pSelector, pOptions ) {

    // Default our options and store them with the "global" prefix, because it's
    // used by the different functions as closure
    var gOptions = $.extend({
                        optionAttributes: null,
                        nullValue:        ""
                        }, pOptions ),
        gSelectList = $( pSelector, apex.gPageContext$ );

    // Register apex.item callbacks
    gSelectList.each( function() {
        if ( $.mobile ) {
            widget.initPageItem( this.id, {
                enable:      function() {
                                 gSelectList.selectmenu( "enable" );
                             },
                disable:     function() {
                                 gSelectList.selectmenu( "disable" );
                             },
                afterModify: function() {
                                 gSelectList.selectmenu( "refresh" );
                             },
                nullValue:   gOptions.nullValue });
        } else {
            widget.initPageItem( this.id, {
                nullValue:   gOptions.nullValue });
        }
    });

    // Clears the existing options
    function _clearList() {
        // remove everything except of the null value. If no null value is defined,
        // all options will be removed
        $( 'option[value!="' + gOptions.nullValue + '"]', gSelectList ).remove();

        if( $.mobile ) {
            gSelectList.selectmenu('refresh', true);
        }
    } // _clearList

    // Called by the AJAX success callback and adds the entries stored in the
    // JSON structure: {"values":[{"r":"10","d":"SALES"},...], "default":"xxx"}
    function _addResult( pData ) {
        var lHtml="";
        // create an HTML string first and append it, that's faster.
        $.each( pData.values, function() {
            lHtml = lHtml + '<option value="' + this.r + '" ' + gOptions.optionAttributes + '>' + this.d + '</option>';
        });
        gSelectList.append( lHtml );

        if( $.mobile ) {
            gSelectList.selectmenu('refresh', true);
        }
        // Set the default value of the page item.
        // The change event is also needed by cascading LOVs so that they are refreshed with the
        // current selected value as well (bug# 9907473)
        $s( gSelectList[0], pData[ "default" ]);
    } // _addResult

    // Clears the existing options and executes an AJAX call to get new values based
    // on the depending on fields
    function refresh() {

        widget.util.cascadingLov(
            gSelectList,
            gOptions.ajaxIdentifier,
            {
                pageItems: $( gOptions.pageItemsToSubmit, apex.gPageContext$ )
            },
            {
                optimizeRefresh: gOptions.optimizeRefresh,
                dependingOn:     $( gOptions.dependingOnSelector, apex.gPageContext$ ),
                success:         _addResult,
                clear:           _clearList
            });

    } // refresh

    // if it's a cascading select list we have to register apexbeforerefresh and change events for our masters
    if ( gOptions.dependingOnSelector ) {
        $( gOptions.dependingOnSelector, apex.gPageContext$ )
            .bind( "apexbeforerefresh", _clearList )
            .change( refresh );
    }
    // register the refresh event which is triggered by a manual refresh
    gSelectList.bind( "apexrefresh", refresh );

}; // selectList

})( apex.widget, apex.jQuery );
