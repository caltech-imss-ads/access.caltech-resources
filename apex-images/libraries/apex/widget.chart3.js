/**
 * @fileOverview
 * The {@link apex.widget}.chart3 is used to store all AnyChart3 chart related functions of Oracle Application Express.
 **/

( function( widget, $ ) {

/**
 * @param {String} pRegionId
 * @param {Number} pRefreshInterval
 *
 * @function chart3
 * @memberOf apex.widget
 * */
widget.chart3 = function( pRegionId, pRefreshInterval ) {

    var lSwfName = "c" + pRegionId;

    _swfFormFix();

    if ( pRefreshInterval > 0 ) {
        setTimeout( _refresh, pRefreshInterval*1000 );
    }

    function _refresh() {
        apex.server.widget( "chart3",
            {
                x01: pRegionId
            }, {
                dataType: "text",
                success:  _setData
            });
    }

    function _setData( pData ) {
        if ( pRefreshInterval ) {
            setTimeout( _refresh, pRefreshInterval*1000 );
        }
        if ( ie ) {
            $( "#c" + pRegionId ).get(0).SetXMLText( pData );
        } else {
            document[ "c" + pRegionId ].SetXMLText( pData );
        }
    } //_setData

    function _swfFormFix(){
        if ( !ie ){ return false; }
        var lTestnodename = "SWFFormFix";
        $( document.body ).append( '<div id="' + lTestnodename + '" style="display:none">&nbsp;</div>' );
        $( "#" + lTestnodename, apex.gPageContext$ )
            .on( "click", function() {
                _swfFormFixCallback( this, lSwfName );
            })
            .click();
    }

    function _swfFormFixCallback( pObj ){
        var lPath     = document,
            lTestnode = pObj;

        while ( pObj == pObj.parentNode ) {
            if ( pObj.nodeName.toLowerCase() === "form" ) {
                if (!!pObj.name && pObj.name.length > 0 ) {
                    lPath = lPath.forms[ pObj.name ];
                }
            }
        }
        lTestnode.parentNode.removeChild( lTestnode );
        window[ lSwfName ]= lTestnode[ lSwfName ];
        return true;
    }

};

})( apex.widget, apex.jQuery);
