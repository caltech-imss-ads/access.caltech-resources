/**
 * @fileOverview
 * This file holds {@link apex.widget} namespace.
 * */

/**
 * @namespace
 */
apex.widget = {

  /**
   * Shows a wait popup.
   *
   * @param {String} [pContent] HTML code for a wait indicator. If it is not provided, the default wait indicator
   *                            will be displayed.
   * */
  waitPopup: function(pContent)
  {
    var lContent = (pContent?pContent:'<img id="img_progress" src="'+apex_img_dir+'processing3.gif" alt="">');

    // if the popup doesn't exist create it, otherwise just replace the content
    if (!$x('apex_wait_popup')) {
      apex.jQuery('<div id="apex_wait_popup" class="apex_wait_popup">'+
             lContent+
             '</div><div id="apex_wait_overlay" class="apex_wait_overlay"></div>').prependTo('body'); }
    else {
      apex.jQuery('#apex_wait_popup').html(lContent); }

    $x('apex_wait_overlay').style.visibility='visible';
    window.setTimeout('apex.jQuery("#apex_wait_popup").html(apex.jQuery("#apex_wait_popup").html());', 100);
  } // waitPopup

}; // apex.widget

/**
 * Given a page item name different options can be registered for a page item with the
 * Application Express JavaScript framework. This is necessary to seamlessly integrate
 * a plug-in item type with the built-in page item related JavaScript functions of
 * Application Express.
 *
 * @param {DOM node | String} pName    APEX page item identified by its name/DOM ID or the entire DOM node.
 * @param {Object}            pOptions Object of options to specify callbacks for specific events and the nullValue. Supported
 *                                     callbacks: getValue, setValue, enable, disable, show, hide, afterModify.
 *
 * @example
 *
 * apex.widget.initPageItem(
 *   "P100_COMPANY_NAME",
 *   { getValue: function(){},
 *     setValue: function(){},
 *     nullValue: "%null%"
 *   }
 * );
 *
 */
apex.widget.initPageItem = function (pName, pOptions) {
    apex.item(pName, pOptions);
};

/**
 * Allows to upload the content of a textarea as CLOB
 * @namespace
 * */
apex.widget.textareaClob = {
  upload: function(pItemName, pRequest) {
    var lClob = new apex.ajax.clob(function(){
                                     if      (p.readyState === 1){}
                                     else if (p.readyState === 2){}
                                     else if (p.readyState === 3){}
                                     else if (p.readyState === 4){
                                       $s(pItemName, "");
                                       apex.submit(pRequest);
                                     } else {
                                       return false;
                                     }
                                   });
    lClob._set($v(pItemName));
  }
};
