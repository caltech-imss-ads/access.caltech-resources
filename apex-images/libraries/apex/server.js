/**
@license

Oracle Database Application Express, Release 4.2

Copyright © 2012, Oracle. All rights reserved.
*/

/**
 * @fileOverview
 * The {@link apex}.server namespace is used to store all AJAX functions to communicate with the server part
 * of Oracle Application Express.
 **/

/**
 * @namespace
 **/
apex.server = {};

/* required for AJAX calls to APEX engine */
apex.jQuery.ajaxSettings.traditional = true;

(function( server, $, undefined ) {
    "use strict";

/**
 * Function that calls the PL/SQL AJAX function which has been defined for a plug-in. This function is
 * a wrapper of the jQuery.ajax function and supports all the setting the jQuery function provides but
 * provides additional APEX features.
 *
 * @param {String} pAjaxIdentifier Use the value returned by the PL/SQL package apex_plugin.get_ajax_identifier to identify your plug-in.
 * @param {Object} [pData]         Object which can optionally be used to send additional values which are sent with the AJAX request.
 *                                 The special attribute "pageItems" which can be of type jQuery selector, jQuery-, DOM object or array of item names
 *                                 identifies the page items which should be included in the URL. But you can also set additional
 *                                 parameters that wwv_flow.show procedure provides. For example you can set the scalar parameters
 *                                 x01 - x10 and the arrays f01 - f20
 * @param {Object} [pOptions]      Object which can optionally be used to set additional options used by the AJAX.
 *                                 It supports the following optional APEX specific attributes:
 *                                   - "refreshObject"      jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                          for which the apexbeforerefresh and apexafterrefresh events are fired
 *                                   - "clear"              JavaScript function which can be used to clear the DOM after the
 *                                                          "apexbeforerefresh" event has fired and before the actual AJAX call is triggered.
 *                                   - "loadingIndicator"   jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                          where the loading indicator should be displayed next to it.
 *                                                          loadingIndicator can also be a function which gets the loading Indicator as
 *                                                          jQuery object and has to return the jQuery reference to the created loading indicator.
 *                                                          eg. function( pLoadingIndicator ) { return pLoadingIndicator.prependTo ( apex.jQuery( "td.shuttleControl", gShuttle )) }
 *                                  - "loadingIndicatorPosition"
 *                                                          6 options to define the position of the loading indicator displayed. Only considered if the value passed to
 *                                                          loadingIndicator is not a function.
 *                                                          - "before":   Displays before the DOM element(s) defined by loadingIndicator
 *                                                          - "after":    Displays after the DOM element(s) defined by loadingIndicator
 *                                                          - "prepend":  Displays inside at the beginning of the DOM element(s) defined by loadingIndicator
 *                                                          - "append":   Displays inside at the end of the DOM element(s) defined by loadingIndicator
 *                                                          - "centered": Displays in the center of the DOM element defined by loadingIndicator
 *                                                          - "page"    : Displays in the center of the page.
 *                                 See jQuery documentation of jQuery.ajax for all other available attributes. The attribute dataType is defaulted to json.
 * @return {jqXHR}
 *
 * @example
 *
 * apex.server.plugin ( pAjaxIdentifier, {
 *     x01: "test",
 *     pageItems: "#P1_DEPTNO,#P1_EMPNO"
 *     }, {
 *     refreshObject:     "#P1_MY_LIST",
 *     loadingIndicator:  "#P1_MY_LIST",
 *     success: function( pData ) { ... do something here ... }
 *     } );
 *
 * @function plugin
 * @memberOf apex.server
 **/
server.plugin = function( pAjaxIdentifier, pData, pOptions ) {

    return _call( "PLUGIN=" + pAjaxIdentifier, pData, pOptions );

}; // plugin


/**
 * Function that returns the URL to issue a GET request to the PL/SQL AJAX function which has been defined for a plug-in.
 *
 * @param {String} pAjaxIdentifier Use the value returned by the PL/SQL package apex_plugin.get_ajax_identifier to identify your plug-in.
 * @param {Object} [pData]         Object which can optionally be used to set additional values which are included into the URL.
 *                                 The special attribute "pageItems" which can be of type jQuery selector, jQuery-, DOM object or array of item names
 *                                 identifies the page items which should be included in the URL. But you can also set additional
 *                                 parameters that wwv_flow.show procedure provides. For example you can set the scalar parameters
 *                                 x01 - x10 and the arrays f01 - f20
 * @return {String}
 *
 * @example
 *
 * apex.server.pluginUrl ( pAjaxIdentifier, {
 *     x01: "test",
 *     pageItems: "#P1_DEPTNO,#P1_EMPNO" } );
 *
 * @function pluginUrl
 * @memberOf apex.server
 **/
server.pluginUrl = function( pAjaxIdentifier, pData ) {

    var lUrl = "wwv_flow.show"    +
               "?p_flow_id="      + $v( "pFlowId" ) +
               "&p_flow_step_id=" + $v( "pFlowStepId" ) +
               "&p_instance="     + $v( "pInstance" ) +
               "&p_debug="        + $v( "pdebug" ) +
               "&p_request=PLUGIN%3D" + pAjaxIdentifier;

    // add all data parameters to the URL
    for ( var lKey in pData ) {
        if ( pData.hasOwnProperty( lKey )) {
            // the pageItems is a special parameter and will actually store all the specified page items in p_arg_names/values
            if ( lKey === "pageItems" ) {

                if ( $.isArray( pData.pageItems )) {
                    for ( var i = 0, lItem; i < pData.pageItems.length; i++ ) {
                        lItem = $x( pData.pageItems[i] );
                        if ( lItem ) {
                            lUrl = lUrl +
                                '&p_arg_names='  + encodeURIComponent( lItem.id ) +
                                '&p_arg_values=' + encodeURIComponent( $v( lItem ));
                        }
                    }
                } else {
                    $( pData.pageItems, apex.gPageContext$ ).each( function() {
                        lUrl = lUrl +
                            '&p_arg_names='  + encodeURIComponent( this.id ) +
                            '&p_arg_values=' + encodeURIComponent( $v( this ));
                    });
                }

            } else {
                lUrl = lUrl + '&' + lKey + '=' + encodeURIComponent( pData[lKey] );
            }
        }
    }

    return lUrl;
}; // pluginUrl


/**
 * Function that calls a PL/SQL on-demand process defined on page or application level. This function is
 * a wrapper of the jQuery.ajax function and supports all the setting the jQuery function provides but
 * provides additional APEX features.
 *
 * @param {String} pName      The name of the PL/SQL on-demand page or application process you wish to call.
 * @param {Object} [pData]    Object which can optionally be used to send additional values which are sent with the AJAX request.
 *                            The special attribute "pageItems" which can be of type jQuery selector, jQuery-, DOM object or array of item names
 *                            identifies the page items which should be included in the URL. But you can also set additional
 *                            parameters that wwv_flow.show procedure provides. For example you can set the scalar parameters
 *                            x01 - x10 and the arrays f01 - f20
 * @param {Object} [pOptions] Object which can optionally be used to set additional options used by the AJAX.
 *                            It supports the following optional APEX specific attributes:
 *                              - "refreshObject"       jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                      for which the apexbeforerefresh and apexafterrefresh events are fired
 *                              - "clear"               JavaScript function which can be used to clear the DOM after the
 *                                                      "apexbeforerefresh" event has fired and before the actual AJAX call is triggered.
 *                              - "loadingIndicator"    jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                      where the loading indicator should be displayed next to it.
 *                                                      loadingIndicator can also be a function which gets the loading Indicator as
 *                                                      jQuery object and has to return the jQuery reference to the created loading indicator.
 *                                                      eg. function( pLoadingIndicator ) { return pLoadingIndicator.prependTo ( apex.jQuery( "td.shuttleControl", gShuttle )) }
 *                              - "loadingIndicatorPosition"
 *                                                      6 options to define the position of the loading indicator displayed. Only considered if the value passed to
 *                                                      loadingIndicator is not a function.
 *                                                      - "before":   Displays before the DOM element(s) defined by loadingIndicator
 *                                                      - "after":    Displays after the DOM element(s) defined by loadingIndicator
 *                                                      - "prepend":  Displays inside at the beginning of the DOM element(s) defined by loadingIndicator
 *                                                      - "append":   Displays inside at the end of the DOM element(s) defined by loadingIndicator
 *                                                      - "centered": Displays in the center of the DOM element defined by loadingIndicator
 *                                                      - "page"    : Displays in the center of the page.
 *                            See jQuery documentation of jQuery.ajax for all other available attributes. The attribute dataType is defaulted to json.
 * @return {jqXHR}
 *
 * @example
 *
 * apex.server.process ( "MY_PROCESS", {
 *     x01: "test",
 *     pageItems: "#P1_DEPTNO,#P1_EMPNO"
 *     }, {
 *     success: function( pData ) { ... do something here ... }
 *     } );
 *
 * @function process
 * @memberOf apex.server
 **/
server.process = function( pName, pData, pOptions ) {

    return _call( "APPLICATION_PROCESS=" + pName, pData, pOptions );

}; // process


/**
 * FOR INTERNAL USE ONLY!!!
 *
 * Function that calls the server side part of a widget. This function is a wrapper of the jQuery.ajax function and
 * supports all the setting the jQuery function provides but provides additional APEX features.
 *
 * @param {String} pName      Name of the internal widget.
 * @param {Object} [pData]    Object which can optionally be used to send additional values which are sent with the AJAX request.
 *                            The special attribute "pageItems" which can be of type jQuery selector, jQuery-, DOM object or array of item names
 *                            identifies the page items which should be included in the URL. But you can also set additional
 *                            parameters that wwv_flow.show procedure provides. For example you can set the scalar parameters
 *                            x01 - x10 and the arrays f01 - f20
 * @param {Object} [pOptions] Object which can optionally be used to set additional options used by the AJAX.
 *                            It supports the following optional APEX specific attributes:
 *                              - "refreshObject"       jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                      for which the apexbeforerefresh and apexafterrefresh events are fired
 *                              - "clear"               JavaScript function which can be used to clear the DOM after the
 *                                                      "apexbeforerefresh" event has fired and before the actual AJAX call is triggered.
 *                              - "loadingIndicator"    jQuery selector, jQuery- or DOM object which identifies the DOM element
 *                                                      where the loading indicator should be displayed next to it.
 *                                                      loadingIndicator can also be a function which gets the loading Indicator as
 *                                                      jQuery object and has to return the jQuery reference to the created loading indicator.
 *                                                      eg. function( pLoadingIndicator ) { return pLoadingIndicator.prependTo ( apex.jQuery( "td.shuttleControl", gShuttle )) }
 *                              - "loadingIndicatorPosition"
 *                                                      6 options to define the position of the loading indicator displayed. Only considered if the value passed to
 *                                                      loadingIndicator is not a function.
 *                                                      - "before":   Displays before the DOM element(s) defined by loadingIndicator
 *                                                      - "after":    Displays after the DOM element(s) defined by loadingIndicator
 *                                                      - "prepend":  Displays inside at the beginning of the DOM element(s) defined by loadingIndicator
 *                                                      - "append":   Displays inside at the end of the DOM element(s) defined by loadingIndicator
 *                                                      - "centered": Displays in the center of the DOM element defined by loadingIndicator
 *                                                      - "page"    : Displays in the center of the page.
 *                            See jQuery documentation of jQuery.ajax for all other available attributes. The attribute dataType is defaulted to json.
 * @return {jqXHR}
 *
 * @example
 *
 * apex.server.widget ( "calendar", {
 *     x01: "test",
 *     pageItems: "#P1_DEPTNO,#P1_EMPNO"
 *     }, {
 *     success: function( pData ) { ... do something here ... }
 *     } );
 *
 * @private
 * @function widget
 * @memberOf apex.server
 **/
server.widget = function( pName, pData, pOptions ) {

    var lData = pData || {};

    lData.p_widget_name = pName;

    return _call( "APXWGT", lData, pOptions );

}; // widget


/**
 * @TODO documentation missing
 * @private
 */
function _call( pRequest, pData, pOptions ) {

    // Initialize the AJAX call parameters required by APEX
    var lOptions = $.extend( {
                        dataType:                   "json",
                        type:                       "post",
                        async:                      true,
                        url:                        "wwv_flow.show",
                        traditional:                true,
                        loadingIndicatorPosition:   "after" },
                        pOptions ),
        // Save the callbacks for later use because we overwrite them with standard handlers
        lSuccessCallback = lOptions.success,
        lErrorCallback   = lOptions.error,
        // Initialize all the default parameters which are expected by APEX
        lData = $.extend( {
                    p_request:      pRequest,
                    p_flow_id:      $v( 'pFlowId' ),
                    p_flow_step_id: $v( 'pFlowStepId' ),
                    p_instance:     $v( 'pInstance' ),
                    p_debug:        $v( 'pdebug' ),
                    p_arg_names:    [],
                    p_arg_values:   [] },
                    pData),
        lLoadingIndicatorTmpl$ = $( '<span class="apex-loading-indicator"></span>' ),
        lLoadingIndicator$,
        lLoadingIndicators$ = $();

    // Get the value of each page item and assign it to the p_arg_names/p_arg_values array
    if ( $.isArray( lData.pageItems )) {
        for ( var i = 0, lIdx, lItem; i < lData.pageItems.length; i++ ) {
            lItem = $x( lData.pageItems[i] );
            if ( lItem ) {
                lIdx  = lData.p_arg_names.length;
                lData.p_arg_names [ lIdx ] = lItem.id;
                lData.p_arg_values[ lIdx ] = $v( lItem );
            }
        }
    } else {
        $( lData.pageItems, apex.gPageContext$ ).each( function() {
            var lIdx = lData.p_arg_names.length;
            lData.p_arg_names [ lIdx ] = this.id;
            lData.p_arg_values[ lIdx ] = $v( this );
        });
    }
    // Remove pageItems so that it's not included in the transmitted data
    delete lData.pageItems;

    // Trigger the before refresh event if the attribute has been specified
    $( lOptions.refreshObject, apex.gPageContext$ ).trigger( 'apexbeforerefresh' );

    // Call clear callback if the attribute has been specified and if it's a function
    if ( $.isFunction( lOptions.clear ) ) {
        lOptions.clear();
    }

    // Add a loading indicator if the attribute has been specified and store the reference to it to remove it later on
    if ( $.isFunction( lOptions.loadingIndicator ) ) {

        // function has to return the created jQuery object or a function which removes the loading indicator
        lLoadingIndicators$ = lOptions.loadingIndicator ( lLoadingIndicatorTmpl$ );
    } else {

        // Iterate over elements in the loadingIndicator as this could be more than 1 element
        $( lOptions.loadingIndicator ).each( function() {

            lLoadingIndicator$ = lLoadingIndicatorTmpl$.clone();

            // First check if element has a loadingIndicator callback, if so use it
            if ( apex.item( this ).callbacks.loadingIndicator !== undefined ) {
                lLoadingIndicator$ = apex.item( this ).loadingIndicator( lLoadingIndicator$ );
            } else {

                // Now we know loadingIndicator is not a function, we consider the position passed as well.
                if ( lOptions.loadingIndicatorPosition === "before" ) {
                    lLoadingIndicator$ = lLoadingIndicator$.insertBefore( $( this, apex.gPageContext$ ).filter( ":not(:hidden)" ));
                } else if ( lOptions.loadingIndicatorPosition === "after" ) {
                    lLoadingIndicator$ = lLoadingIndicator$.insertAfter( $( this, apex.gPageContext$ ).filter( ":not(:hidden)" ));
                } else if ( lOptions.loadingIndicatorPosition === "prepend" ) {
                    lLoadingIndicator$ = lLoadingIndicator$.prependTo( $( this, apex.gPageContext$ ));
                } else if ( lOptions.loadingIndicatorPosition === "append" ) {
                    lLoadingIndicator$ = lLoadingIndicator$.appendTo( $( this, apex.gPageContext$ ));
                } else if ( lOptions.loadingIndicatorPosition === "centered" ) {
                    // TODO Add centered loading indicator position support

                    // lLoadingIndicator$ = '<div class="apex-loading-indicator-centered"></div>';
                    // and add that to the page
                    alert( "Centered Position not yet supported" );

                } else if ( lOptions.loadingIndicatorPosition === "page" ) {
                    // TODO Add page loading indicator position support

                    // lLoadingIndicator$ = '<div class="apex-loading-indicator-page"></div>';
                    // and add that to the page
                    alert( "Page Position not yet supported" );

                }
            }

            lLoadingIndicators$ = lLoadingIndicators$.add( lLoadingIndicator$ );
        });
    }

    // Remove loadingIndicatorPosition, so that it's not in the transmitted data
    delete lOptions.loadingIndicatorPosition;

    // Set the values which should get submitted and register our callbacks
    // which will perform some basic handling after an AJAX call completes.
    lOptions.data    = lData;
    lOptions.error =
        function( pjqXHR, pTextStatus, pErrorThrown ) {
            _error( pjqXHR, pTextStatus, pErrorThrown, {
                callback:         lErrorCallback,
                loadingIndicator: lLoadingIndicators$ });
        };
    lOptions.success =
        function( pData, pTextStatus, pjqXHR ) {
            _success( pData, pTextStatus, pjqXHR, {
                callback:         lSuccessCallback,
                errorCallback:    lOptions.error,
                loadingIndicator: lLoadingIndicators$,
                refreshObject:    lOptions.refreshObject });
        };

    // perform the AJAX call and return the jQuery object
    return $.ajax( lOptions );
} // _call


/**
 * @TODO documentation missing
 * @private
 */
function _removeLoadingIndicator ( pLoadingIndicator ) {

    // Remove a loading indicator if the attribute has been specified
    if ( $.isFunction( pLoadingIndicator ) ) {
        pLoadingIndicator();
    } else {
        $( pLoadingIndicator, apex.gPageContext$ ).remove();
    }

} // _removeLoadingIndicator

/**
 * @TODO documentation missing
 * @private
 */
// noinspection FunctionWithMultipleReturnPointsJS
function _success( pData, pTextStatus, pjqXHR, pOptions ) {

    var lResult = true;

    // check for errors first, allowing for pData to be null where the call returns nothing
    if ( pData !== null ) {
        if ( pData.error ) {
            // TODO not sure why we can't just use pjqXHR.error - have to investigate
            return pOptions.errorCallback( pjqXHR, "APEX", pData.error );
        }
    }

    _removeLoadingIndicator( pOptions.loadingIndicator );

    // call success callback if one is specified
    if ( $.isFunction( pOptions.callback ) ) {
        lResult = pOptions.callback( pData, pTextStatus, pjqXHR );
    }

    // Trigger the after refresh event if the attribute has been specified
    // But only do it if the callback returned <> false.
    // Note: By intention we check with == to capture null as well
    if ( lResult || lResult == undefined ) {
        $( pOptions.refreshObject, apex.gPageContext$ ).trigger( 'apexafterrefresh' );
    }

    return lResult;
} // _success


/**
 * @TODO documentation missing
 * @private
 */
function _error( pjqXHR, pTextStatus, pErrorThrown, pOptions ) {

    var lMsg,
        lResult = false;

    _removeLoadingIndicator( pOptions.loadingIndicator );

    // TODO Handle APEX standard errors here $$$ (eg. session expired, ...)

    // call error callback if one is specified
    if ( $.isFunction( pOptions.callback ) ) {
        lResult = pOptions.callback( pjqXHR, pTextStatus, pErrorThrown );
    } else if ( pjqXHR.status !== 0 ) {
        // When pjqXHR.status is zero it indicates that the page is unloading
        // (or a few other cases that can't be distinguished such as server not responding)
        // and it is very important to not call alert (or any other action that could
        // potentially block on user input or distract the user) when the page is unloading.
        if ( pTextStatus === "APEX" ) {

            // If this is an APEX error, then just show the error thrown
            lMsg = pErrorThrown;
        } else {

            // Otherwise, also show more information about the status
            lMsg = "Error: " + pTextStatus + " - " + pErrorThrown;
        }
        window.alert( lMsg );
    }

    return lResult;
} // _error


})( apex.server, apex.jQuery );
