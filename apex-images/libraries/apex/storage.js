/**
 * @fileOverview
 * The {@link apex}.storage namespace is used to store storage related functions of Oracle Application Express.
 **/

/**
 * @namespace
 **/
apex.storage = {};

(function( storage, $, undefined ) {
    "use strict";

    /**
     * @ignore
     **/
    storage.getCookieVal = function ( pOffset ) {
        var lEndPos = document.cookie.indexOf ( ";", pOffset );
        if ( lEndPos === -1 ) {
            lEndPos = document.cookie.length;
        }
        // todo unescape is deprecated?
        return unescape( document.cookie.substring( pOffset, lEndPos ) );
    };

    /**
     * Returns the value of cookie name (pName).
     *
     * @param {String} pName
     *
     * @function getCookie
     * @memberOf apex.storage
     * */
    storage.getCookie = function ( pName ) {
        var lArg = pName + "=",
            lArgLength = lArg.length,
            lCookieLength = document.cookie.length,
            i = 0;
        while ( i < lCookieLength ) {
            var j = i + lArgLength;
            if ( document.cookie.substring( i, j ) === lArg ) {
                return storage.getCookieVal( j );
            }
            i = document.cookie.indexOf( " ", i ) + 1;
            if ( i === 0 ) {
                break;
            }
        }
        return null;
    };

    /**
     * Sets a cookie (pName) to a specified value (pValue).
     *
     * @param {String} pName
     * @param {String} pValue
     *
     * @function setCookie
     * @memberOf apex.storage
     * */
    storage.setCookie = function ( pName, pValue ) {
        var argv    = arguments,
            argc    = arguments.length,
            expires = ( argc > 2 ) ? argv[ 2 ]  : null,
            path    = ( argc > 3 ) ? argv[ 3 ]  : null,
            domain  = ( argc > 4 ) ? argv[ 4 ]  : null,
            secure  = ( argc > 5 ) ? true       : false;

        // todo unescape is deprecated?
        document.cookie = pName + "=" + escape ( pValue ) +
            ( ( expires === null ) ? "" : ( "; expires=" + expires.toGMTString() ) ) +
            ( ( path    === null ) ? "" : ( "; path=" + path ) ) +
            ( ( domain  === null ) ? "" : ( "; domain=" + domain ) ) +
            ( ( secure  === true || window.location.protocol === "https:" ) ? "; secure" : "" );
    };

})(apex.storage, apex.jQuery);
