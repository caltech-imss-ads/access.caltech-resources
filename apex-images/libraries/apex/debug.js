/**
 * @fileOverview
 * The {@link apex}.debug namespace is used to store all debug functions of Oracle Application Express.
 **/

/**
 * @namespace
 **/
apex.debug = {};

/* API for compatibility. Use apex.debug.log instead */ 
apex.debug = function() {
    apex.debug.log.apply( this, arguments );
}; // debug


(function( debug, $, undefined ) {
    "use strict";

/**
 * Method that returns the debug level, based on item "#pdebug"
 *
 * @example
 * apex.debug.log("Level=", apex.debug.getLevel());
 *
 * @memberOf apex.debug
 */
debug.getLevel = function() {
    var lReturnLevel,
        lDebugValue = $( "#pdebug", apex.gPageContext$ ).val();

    if ( lDebugValue === "YES" ) {
        lReturnLevel = 4;
    } else {
        if ( /^LEVEL[0-9]$/.test( lDebugValue ) ) {
            lReturnLevel = parseInt( lDebugValue.substr( 5 ), 10 );
        } else {
            lReturnLevel = 0;
        }
    }
    return lReturnLevel;
}; // getLevel

/**
 * Method used for simple logging to console, when running in 'Debug' mode.
 *
 * @param {...*} arguments Any number of parameters which will be logged to the console
 *
 * @example
 * apex.debug.log( "Test" );
 * apex.debug.log( "Text fields: ", jQuery("input[type=text]") );
 *
 * @memberOf apex.debug
 **/
debug.log = function() {
    // Only show message if running in APEX 'Debug Mode'
    if ( debug.getLevel() > 0 ) {
        // try it with console.log which is supported by most browsers
        try {
            console.log.apply( console, arguments );
        }
        catch( e ) {
            // Opera is using a different logging mechanism
            try {
                opera.postError.apply( opera, arguments );
            }
            catch( e ) {
            }
        }
    }
};  // log


})( apex.debug, apex.jQuery );
