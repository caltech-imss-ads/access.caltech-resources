/**
 * @fileOverview
 * The {@link apex}.navigation namespace is used to store popup and redirect related functions of Oracle Application Express.
 **/

/**
 * @namespace
 **/
apex.navigation = {};

(function( navigation, $, undefined ) {
    "use strict";

    /**
     * Opens the specified page.
     *
     * @param {String} pWhere URL of the page to open.
     *
     * @function redirect
     * @memberOf apex.navigation
     */
    navigation.redirect = function ( pWhere ) {
        if ( $.mobile ) {
            // Force the page to always reload, because otherwise we would use a copy available in the DOM
            $.mobile.changePage( pWhere, { reloadPage: true });
        } else {
            location.href = pWhere;
        }
        return;
    };


    /**
     * @TODO Add documentation
     *
     * @function popup
     * @memberOf apex.navigation
     */
    navigation.popup = function ( pOptions ) {
        var // Initialize default parameter values
            lOptions = $.extend( {
                            url:        "about:blank",
                            name:       "_blank",
                            width:      600,        //min value 100
                            height:     600,        //min value 100
                            scroll:     "yes",
                            resizable:  "yes",
                            toolbar:    "no",
                            location:   "no",
                            statusbar:  "no",
                            menubar:    "no" },
                            pOptions),
            // Open the new window with those parameters
            lWindow = window.open(
                lOptions.url,
                lOptions.name,
                "toolbar="      + lOptions.toolbar      + "," +
                "scrollbars="   + lOptions.scroll       + "," +
                "location="     + lOptions.location     + "," +
                "statusbar="    + lOptions.statusbar    + "," +
                "menubar="      + lOptions.menubar      + "," +
                "resizable="    + lOptions.resizable    + "," +
                "width="        + lOptions.width        + "," +
                "height="       + lOptions.height
            );

        if ( lWindow.opener === null ) {
            lWindow.opener = window.self;
        }
        lWindow.focus();
        return lWindow;
    }; //popup


    /**
     * Due to hard-coded name attribute, subsequent new popups launched with this API will replace existing popups, not stack.
     * @TODO Add documentation
     *
     * @function popup.url
     * @memberOf apex.navigation
     */
    navigation.popup.url = function ( pURL ) {
        navigation.popup( {
            url:    pURL,
            name:   "winLov",
            width:  800,
            height: 600
        });
    }; //popup.url


    /**
     * Sets the value of the item in the parent window (pThat), with (pValue) and then closes the popup window.
     *
     * @TODO Update documentation:
     * uses $x_Value, not $s. $x_Value supports passing an array of DOM nodes, $s does not.
     * Old doc. didn't state that it supported an array.
     *
     * @param {String} pValue
     * @param {DOM node | string ID} pThat
     *
     * @function popup.close
     * @memberOf apex.navigation
     * */
     navigation.popup.close = function ( pItem, pValue ) {
        opener.$x_Value( pItem, pValue );
        window.close();
    }; // popup.return


})(apex.navigation, apex.jQuery);
