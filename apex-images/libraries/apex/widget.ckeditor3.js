/**
 * @fileOverview
 * The {@link apex.widget}.ckeditor3 is used for the Rich Text Editor widget of Oracle Application Express.
 * Internally the CKEditor http://www.ckeditor.com is used.
 * See the CKEditor documentation for available options.
 **/

(function( widget, $ ) {

/**
 * @param {String} pSelector  jQuery selector to identify APEX page item(s) for this widget.
 * @param {Object} [pOptions]
 *
 * @function ckeditor3
 * @memberOf apex.widget
 * */
widget.ckeditor3 = function(pSelector, pOptions) {

  // Based on our custom settings, add addition properties to the autocomplete options
  var lOptions = $.extend({
                   toolbar: "Basic",
                   toolbarStartupExpanded: true,
                   disableNativeSpellChecker: false,
                   "menu_groups": "clipboard,tablecell,tablecellproperties,tablerow,tablecolumn,table,anchor,link,image,flash"
                   }, pOptions);

  // Get min width for the HTML Editor
  var lMinWidth = 0;
  if (lOptions.toolbar==="Basic") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?185:205;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?181:201;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?175:195;
    }
  } else if (lOptions.toolbar==="Intermediate") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?240:260;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?235:255;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?230:250;
    }
  } else if (lOptions.toolbar==="Full") {
    if (lOptions.skin==="kama") {
      lMinWidth = lOptions.toolbarStartupExpanded?530:530;
    } else if (lOptions.skin==="office2003") {
      lMinWidth = lOptions.toolbarStartupExpanded?590:605;
    } else if (lOptions.skin==="v2") {
      lMinWidth = lOptions.toolbarStartupExpanded?575:595;
    }
  }

  // Get editor padding
  var lEditorPadding = (lOptions.skin==="kama"?25:20);

  // We don't want to show all toolbar entries of basic and full
  if (lOptions.toolbar==="Basic") {
    lOptions.toolbar = [['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink']];
  } else if (lOptions.toolbar==="Intermediate") {
    lOptions.toolbar = [
                 ['Cut','Copy','Paste','-','Bold', 'Italic','Underline', '-', 'NumberedList', 'BulletedList','-','Outdent','Indent', '-', 'Link', 'Unlink'],
                         '/',
             ['Format','Font','FontSize','TextColor','-','JustifyLeft','JustifyCenter','JustifyRight']
                   ];
  } else if (lOptions.toolbar==="Full") {
    lOptions.toolbar = [
                         ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','Preview'],
                         ['Templates'],
                         ['Link','Unlink','Anchor'],
                         ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
                         '/',
                         ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
                         ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
                         ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                         ['TextColor','BGColor'],
                         ['ShowBlocks'],
                         '/',
                         ['Styles','Format','Font','FontSize'],
                         ['Maximize', 'Source']
                       ];
  }

  // No user will hide the toolbar if it's already displayed at startup
  if (lOptions.toolbarStartupExpanded) {
    lOptions.toolbarCanCollapse = false;
  }

  // Instanciate the CKeditor
  $(pSelector, apex.gPageContext$).each (function() {
    var lFinalOptions = lOptions;
    // calculate the editor size depending on the textarea settings
    lFinalOptions.height = (this.rows*15)+lEditorPadding;
    lFinalOptions.width  = (this.cols*9.5 < lMinWidth)?lMinWidth:this.cols*9.5;
    lFinalOptions.resize_minHeight = lFinalOptions.height;
    lFinalOptions.resize_minWidth  = lFinalOptions.width;

    $(this).wrap("<div id='" + this.id + "_DISPLAY'></div>");
    CKEDITOR.replace(this.id, lFinalOptions);

    // Register apex.item callbacks
    widget.initPageItem(this.id, {
        enable      : function() {
            alert('Enable not supported.');
        },
        disable     : function() {
            alert('Disable not supported.');
        },
        setValue    : function(pValue) {
            var oEditor = CKEDITOR.instances[this.id];
            oEditor.setData(pValue);
        },
        getValue    : function() {
            var oEditor = CKEDITOR.instances[this.id];
            return oEditor.getData();
        }
    });
  });

  // register focus handling, so when the non-displayed textarea of the CKEditor
  // receives focus, focus is moved to the editor.
  $(pSelector, apex.gPageContext$).focus(function(){
    var oEditor = CKEDITOR.instances[this.id];
    oEditor.focus();
  });

}; // ckeditor3

})( apex.widget, apex.jQuery );
