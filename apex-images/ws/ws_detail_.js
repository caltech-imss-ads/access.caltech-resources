/*

*/
function ws_GetRow(pDir){
	pg20_CleanMessage();
	var get = new htmldb_Get('drop',$x('pFlowId').value,'APPLICATION_PROCESS=Column_Pull',0);
	if(pDir == 'prev'){
		if(!isEmpty('ajax_prev')){get.add('CURRENT_WORKSHEET_ROW',$x('ajax_prev').value);}
	}else{
		if(!isEmpty('ajax_next')){get.add('CURRENT_WORKSHEET_ROW',$x('ajax_next').value);}
	}
	gReturn = get.get();
	get = null;
	$x('rowcount').innerHTML = $x('ajax_rowcount').value;
	ws_TextArea();
	wsCheckNav();
}

function wsCheckNav(){
		$x('rowcount').innerHTML = $x('ajax_rowcount').value;
		if(isEmpty('ajax_prev')){$x_Hide('prev_button');}else{$x_Show('prev_button');}
		if(isEmpty('ajax_next')){$x_Hide('next_button');}else{$x_Show('next_button');}

}

function wsPageInit(){wsCheckNav();}


function initResizeColumns(pThis){
	var lTable = $x(pThis);
	var lTable_TH = lTable.getElementsByTagName('TH');
	for(var i=0;i<lTable_TH.length;i++){
		var lDiv = $dom_AddTag(lTable_TH[i],'span');
		lDiv.className = 'mv';
		lDiv.onmousedown = function(){
			var lCol = $x_GetColumn(this);
			var l_left = findPosX(this.parentNode);
			$x_Style(document.body,'cursor','col-resize')
			document.body.onmousemove = function (event){
				$x_Style(lCol,'width',event.pageX-(l_left-4));
			}
			document.body.onmouseup = function (event){
			   document.body.onmousemove = '';
			   $x_Style(document.body,'cursor','')
			}
	}
	}
}


function $x_GetColumn(pThis){
	var l = pThis.parentNode;
	var l_Col = [];
	var l_TD = $x_UpTill(pThis,'TABLE').getElementsByTagName('TD');
	var l_TH = $x_UpTill(pThis,'TABLE').getElementsByTagName('TH');
    var l_D = $dom_JoinNodeLists(l_TD,l_TH);
	var l_Column = l.cellIndex;
	for(var i=0;i<l_D.length;i++){if(l_D[i].cellIndex == l_Column){l_Col.push(l_D[i])}}
    return l_Col
}



if (apex==null || typeof(apex)!="object"){var apex = new Object()}
if (apex.DD==null || typeof(apex.DD)!="object"){apex.DD = new Object()}
//apex.DD.gDragger
apex.DD = {
   /*Dragable Items*/
   gDragger : false,
   dragable : [],
   dropable : [],
	init : function (pDrag,pDrop){
		if(pDrag){apex.DD.dragable = pDrag};
		if(pDrop){apex.DD.dropable = pDrop};
		for(var i=0;i<apex.DD.dragable.length;i++){if($x(apex.DD.dragable[i])){apex.DD.SetMouseDown($x(apex.DD.dragable[i]))}}
	},
	
   SetMouseDown: function(pThis,pFunction){
   	if(!pFunction){pFunction = apex.DD.onmousedown}
    		if(window.addEventListener){pThis.addEventListener('mousedown',pFunction,true);}
	 		else{pThis.attachEvent('onmousedown',pFunction);}
	 		return;
   },
   
   onmousedown : function (e){
		pThis = html_GetTarget(e);
		if(pThis.nodeName != 'A'){
			if(!html_SubString(pThis.className,'icon')){pThis = $x_UpTill(pThis,'DIV');}
			apex.DD.gDragger = pThis;
			document.onmouseup = apex.DD.onmouseup;
			document.onmousemove = apex.DD.onmousemove;
			supressSelect();
		}
   },
   
   onmouseup:function (e){
		apex.DD.check_pos_final();
	  	document.onmouseup = null;
	 	document.onmousemove = null;
	 	drag=false;
	 	targ = false;
	 	currentSpacer = false;
   },
   check_pos_start:function (){/* default on moving */},
   check_pos_move:function (){/* default on moving */},
   check_pos_final:function (){/* default on final drop */},

   
   onmousemove:function (e){
	if(!e){var e=window.event};
	if(!$x('tempdrag')){$dom_AddTag(document.body,'DIV').id = 'tempdrag'}
	var lTargParent = $x('tempdrag');
	targ = apex.DD.gDragger.cloneNode(true);
	apex.DD.check_pos_start();
	$x_Hide(apex.DD.gDragger);
	for (var i=0;i<gDropables.length;i++){
		lDrop[i] = findPos(gDropables[i]);
		lDrop[i][2] = lDrop[i][0] + gDropables[i].offsetWidth;
		lDrop[i][3] = lDrop[i][1] + gDropables[i].offsetHeight;
		lDrop[i][4] = gDropables[i];
		}
	with (targ){
	 removeAttribute('onmouseover');
	 removeAttribute('onmouseout');
	 removeAttribute('onmousemove');
	 style.position = 'absolute';
	}
	lTargParent.appendChild(targ);
	// calculate event X,Y coordinates
	offsetX=e.clientX + getScrollXY()[0]
	offsetY=e.clientY + getScrollXY()[1]
	// assign default values for top and left properties
	if(!targ.style.left){targ.style.left=offsetX-(targ.offsetWidth /2)};
	if(!targ.style.top){targ.style.top=offsetY-(targ.offsetHeight /2)};
	// calculate integer values for top and left properties
	coordX=parseInt(targ.style.left);
	coordY=parseInt(targ.style.top);
	drag=true;
	document.onmousemove = dragDiv;
   }
   
}



var gDragX = 0;
var gDragY = 0;
var gDropables = [];
var lDrop = [];
function init_Drag(){
   var lItems2 = [];
	var lItems = getElementsByClass('icon','icons','div');
	for(var i=0;i<lItems.length;i++){if(lItems[i].id != '1' && lItems[i].id != 'UPDIR'){
	    apex.DD.dragable[apex.DD.dragable.length] = lItems[i];
	}
	
	}
	gDropables = getElementsByClass('folder','icons','div');
	apex.DD.dropable = gDropables;
	apex.DD.check_pos_final = function(){check_pos()}
    apex.DD.init();
	return;
}

// continue dragging

function dragDiv(e){
	if(!e){var e=window.event};
	if(!drag){return};
	lDragX = (coordX+getScrollXY()[0])+e.clientX-offsetX;
	lDragY = (coordY+getScrollXY()[1])+e.clientY-offsetY;
	targ.style.left = lDragX + 'px';
	targ.style.top = lDragY+'px';
	if(Math.abs(lDragX-gDragX)>=5||Math.abs(lDragY-gDragY)>=5){
		gDragX = lDragX;
		gDragY = lDragY;
		check_Move()
	}
    apex.DD.check_pos_move();
	supressSelect();
	toolTip_disable();
	return false;
}


function supressSelect(){
try {if(!ie){window.getSelection().removeAllRanges()}else{document.selection.empty()}
}catch(e){return}
}

var gD = false;
function check_Move(){
		var lD = drag_Drop(targ);
		if(lD){$x_Style(lD,'border','1px solid #369');
		gD = lD;
		}else{
		$x_Style(gD,'border','1px solid #FFF');
		gD = false;
		}
}

function check_pos(){
   this._Drop = _Drop;
	this._Clean = _Clean;
   this._Action = _Action;
	this._Return = _Return;
   try {
   	var lTest = drag_Drop(targ);
   	if(lTest){this._Action(lTest.id,targ.id,targ.className);}
   	else{this._Clean()}
   	return;
	} catch (e) {
		return;
	}

	function _Drop(){
       this._Clean();
		 return lEl;
	}
	
	function _Clean(){
		$x_Remove('oldholder');
		$x('tempdrag').innerHTML='';
		$x_Show(apex.DD.gDragger);
		$x_Style(gD,'border','1px solid #FFF');
	}
	
	
	function _Action(pParent,pChild,pClass){
	   if(pParent != '1'){
		 var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=NewParent',0);
		}else{
		 var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=SetShared',0);
		}
		get.add('TEMPORARY_FOLDER',pParent);
		get.add('TEMPORARY_ITEM',pChild);
		if(html_SubString(pClass,'folder')){
			get.add('TEMPORARY_ACTION','FOLDER');
		}else if (html_SubString(pClass,'webpage')){
			get.add('TEMPORARY_ACTION','WEBPAGE');
		}else{
			get.add('TEMPORARY_ACTION','WORKSHEET');
		}
		get.GetAsync(this._Return);
	}
		
	function _Return(){
		if(p.readyState == 1){
			ajax_Loading(p.readyState);
		}else if(p.readyState == 2){
		}else if(p.readyState == 3){
		}else if(p.readyState == 4){			
			var lRet =  eval('('+p.responseText+')');
			if(lRet.action == 'set_shared' && lRet.result == true){
				apex.DD.gDragger.getElementsByTagName('DIV')[0].style.backgroundImage = 'url('+lRet.img+')';
				parent._Clean();
			}else if (lRet.action == 'reparent_item' && lRet.result == true){
				if(lRet.message){lTest.getElementsByTagName('A')[0].innerHTML = lRet.message;}
				$x_Remove(apex.DD.gDragger);
				parent._Clean();
			}
			ajax_Loading(p.readyState);
		}else{return false;}
	}	
	
}

function ajax_Loading(pState){
		if(pState == 1){
			$x_Class('htmldbBodyMargin','wait');
		}else{
			$x_Class('htmldbBodyMargin','htmldbBodyMargin');
		}
}



function drag_Drop(pThis){
		//var lReturn=false,lReturn2=false;
		var l = findPos(pThis)
		var lX = l[0];
		var lY = l[1];
		for (var i=0;i<lDrop.length;i++){
		   var lReturn=false,lReturn2=false;
			if(lDrop[i][0] < lX && lX < lDrop[i][2]){lReturn = true}
			if(lDrop[i][1] < lY && lY < lDrop[i][3]){lReturn2 = true}
			if(lReturn && lReturn2){return lDrop[i][4];break;}
		}
		return false;
		apex.DD.check_pos_move()
}

function findPos(obj){
	var curleft=0,curtop=0;
	if (obj.offsetParent){
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while(obj = obj.offsetParent){
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}
	curtop += html_GetPageScroll();
	return [curleft,curtop];
}

function GoOrDrag(pURL){
   document.location = pURL;
}

function ws_ReportPage2(pReq,pThis,pSearch,pColumn){
       this._Return = _Return;
		 var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=PullWorksheet',0);
		 if(pReq){get.add('TEMPORARY_ACTION',pReq)}
		 get.add('P2_DISPLAY',$x('P2_DISPLAY').value);
		 if(pSearch){get.add('TEMPORARY_FILTER',TEMPORARY_FILTER)}
		 get.GetAsync(this._Return);
		 return;
		 
		 function _Return(){
		if(p.readyState == 1){
			ajax_Loading(p.readyState);
		}else if(p.readyState == 2){
		}else if(p.readyState == 3){
		}else if(p.readyState == 4){
		 $x('worksheet').innerHTML = p.responseText;
		 ajax_Loading(p.readyState);
		}else{return false;}
		 
		 } 
}

/* creates worksheet object */
if (apex.page==null || typeof(apex.page)!="object"){apex.page = new Object()}
if (apex.worksheet==null || typeof(apex.worksheet)!="object"){apex.worksheet = new Object()}

apex.worksheet = {
   ws : function (){
   this.l_Action = false;
   this.l_Search = false;
   this.l_Type = false;
   this.l_Sort = false;
   this.l_Num = 15;
   this.l_Col = false;
   this.lRpt = false;
   this._Return = _Return;
   this._Get = _Get;
   return;
	function _Get(pAction,pSearch,pType,pSort,pNum,pRpt){
		if(pAction){this.l_Action = pAction}
		if(pSearch){this.l_Search = pSearch}
		if(pType){this.l_Type = pType}
		if(pSort){this.l_Sort = pSort}
		if(pNum){this.l_Num = pNum}
		if(pRpt){this.lRpt = pRpt}
		var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=PullWorksheet',0);
		if(this.l_Action){get.add('AJAX_WS_ACTION',this.l_Action)}
		if(this.l_Search){get.add('AJAX_WS_FILTER',this.l_Search)}
		if(this.l_Type){get.add('AJAX_WS_FILTER_TYPE',this.l_Type)}
		if(this.l_Col){get.add('AJAX_WS_COLUMN',this.l_Col)}
		if(this.l_Num){get.add('AJAX_WS_NUM',this.l_Num)}
		if(this.lRpt){get.add('RPT_ID',this.lRpt)}
		get.GetAsync(this._Return);
	}
	function _Return(){
		if(p.readyState == 1){
			$x_Hide('rollover');
			 ajax_Loading(p.readyState);
		}else if(p.readyState == 2){
		}else if(p.readyState == 3){
		}else if(p.readyState == 4){
			$x('worksheet').innerHTML = p.responseText;
			$x_Hide('rollover');
			ajax_Loading(p.readyState);
			document.onclick = null;
			pg2_Init();
		}else{return false;}
	} 
   }
}

apex.ws = apex.worksheet;

function ws_SearchUp(){html_Find('rollover_content',$x('search').value);}
function ws_SearchApply(pType){
			var lReport = new apex.worksheet.ws();
			if(pType == 'SEARCH'){lReport._Get('SEARCH',$x('P2_SEARCH').value,false,false,$x('P2_DISPLAY').value);}
			else{lReport._Get('FILTER',$x('search').value,'LIKE',false,$x('P2_DISPLAY').value);}
}

function ws_Sort(pThis){
	var lReport = new apex.worksheet.ws();
	lReport._Get('SORT',false,pThis,false,$x('P2_DISPLAY').value);
}

function ws_Highlight(pThis){
	var lReport = new apex.worksheet.ws();
	if(pThis.checked){lHighlight = 'Y'}else{lHighlight = 'N'}
	lReport._Get('HIGHLIGHT',pThis.id,lHighlight,false,$x('P2_DISPLAY').value);
}

function ws_Hide(){
	var lReport = new apex.worksheet.ws();
	lReport._Get('HIDE',false,false,false,$x('P2_DISPLAY').value);
}

function ws_Break(pCol){
	var lReport = new apex.worksheet.ws();
	if(pCol){lReport.l_Col = pCol};
	lReport._Get('BREAK',false,false,false,$x('P2_DISPLAY').value);
}

function ws_ReportPull(pThis){
	var lReport = new apex.worksheet.ws();
	lThis = pThis.id
	lReport._Get(false,false,false,false,$x('P2_DISPLAY').value,lThis);
}

function ws_Clear(pCol){
	var lReport = new apex.worksheet.ws();
	if(pCol){lReport.l_Col = pCol};
	lReport._Get('CLEAR',false,false,false,$x('P2_DISPLAY').value);
}

var lT = false;

function ws_ReportPage(pReq,pThis,pSearch){
 	if(lT){lT.dCancel()}
   lT = this;
   $x('rollover_content').innerHTML = '';
	this.dTimeout;
 	//clearTimeout(this.dTimeout);
 	this.dGet = dGet;
 	this.dGet.pThis = pThis;
 	this.dShow = dShow;
	this.dShow.pThis = pThis;
 	this.dCancel = dCancel;
	this.dCancel.pThis = pThis;
   var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=PullSortWidget',0);
 	this.dGet();
	return;
	
	function dGet(){
	 	//this.dTimeout = setTimeout("this.dCancel()",5000);
		get.add('AJAX_WS_COLUMN',pThis.id);
	 	get.GetAsync(dShow);
	}
	
	function dShow(){
	 	$x_Hide('rollover');
	 	if(p.readyState == 1){
	 				ajax_Loading(p.readyState);
	 	}else if(p.readyState == 2){
	 	}else if(p.readyState == 3){
	 	}else if(p.readyState == 4){

   var myObject = eval('(' + p.responseText + ')');
   if(myObject.dialog.coltype == 'COMPUTED'){$x_Show('calc')}else{$x_Hide('calc')}
   if(myObject.dialog.filtered){$x_Show('removefilter')}else{$x_Hide('removefilter')}
   if(myObject.dialog.uv){
	for (var i=0;i<myObject.dialog.row.length;i++){
      if(myObject.dialog.row[i].R){
      	if(myObject.dialog.row[i].D){var tDisplay = myObject.dialog.row[i].D}
      	else {var tDisplay = myObject.dialog.row[i].R}
      	var lTemp = $dom_AddTag('rollover_content','DIV',tDisplay);
      	lTemp.apexReturnValue = myObject.dialog.row[i].R
      	lTemp.onclick = function(){
			var lReport = new apex.worksheet.ws();
			lReport._Get("FILTER",this.apexReturnValue,"=",false,$x('P2_DISPLAY').value);
	      }
	      
	      
	      if(ie){
      	lTemp.onmouseover = function(){$x_Class(this,'hover')}
	      
      	lTemp.onmouseout = function(){$x_Class(this,'')}
	      }
      }
   }
   if(myObject.dialog.row.length > 10){$x_Style('rollover_content','height','210')}
   else{$x_Style('rollover_content','height','')}
   $x_Show('rollover_content');
   }else{
   $x_Hide('rollover_content');
   }
	 	 	$x_Show('rollover');
	 	 	$x_Style('rollover','left',findPosX(pThis)+(pThis.offsetWidth/2)-($x('rollover').offsetWidth/2));
	 	 	$x_Style('rollover','top',findPosY(pThis)+pThis.offsetHeight+5);
	 	 	$x_Class(pThis.parentNode,'currentCol');
	 	 	document.onclick = function(e){
			dCheckClick(e);
			}
			ajax_Loading(p.readyState);
	 	}else{return false;}
	}
	function dCheckClick(e){
		var elem = html_GetTarget(e);
		try{
			var lTable = $x_UpTill(elem,"DIV");
			if(lTable.id!='rollover_content' && lTable.id!='rollover'){dCancel();}
			else{}
		}catch(err){dCancel();}
	}
	
	function dCancel(){
	   try{
	 	$x_Hide('rollover');
	 	$x_Class(pThis.parentNode,'');
		document.onclick = null;
	 	get = null;
	 	lT = false;
	 	}catch(e){
		document.onclick = null;
	 	get = null;
	 	lT = false;
	 	}
	}
}

function ws_RC(pThis){
	var lRow = $x_UpTill(pThis,'TR');
	var lColor = '';
	if(pThis.checked){lColor = '#F00';}
	html_RowHighlight(lRow,lColor);
}

function ws_RCA(pThis){
var lArray = $f_CheckFirstColumn(pThis);
var lLength = lArray.length
for(var i=0;i<lLength;i++){ws_RC(lArray[i])}
}

function ws_TextArea(){
 var lTextArea = document.body.getElementsByTagName('TEXTAREA')
 init_ResizeableTextAreas(lTextArea,false,false,'textarea_resize')
}

function init_ResizeableTextAreas(pNd,pWidth,pHeight,pClass){
	if($x(pNd)){pNd = [pNd];}
	if(!pClass){pClass = 'textarea_resize'}
	for (var i=0;i<pNd.length;i++){
	   var lNd = $x(pNd[i]);
   	var lDiv = $dom_AddTag(lNd.parentNode,'DIV');
   	$x_Class(lDiv,'textarea_resize');
   	if(pHeight){$x_Style(lNd,'height',pHeight);}
   	if(!pWidth){pWidth = lNd.offsetWidth}
   	$x_Style([lDiv,lNd],'width',pWidth);
   	lDiv.onmousedown = function(){run_ResizeTextArea(this)}
	}
	return
}




function run_ResizeTextArea(pThis){
       this.Dragger = pThis;
       this.Textarea = pThis.parentNode.getElementsByTagName('TEXTAREA')[0];
       this.TAH = findPosY(this.Textarea);
		 this._Move = _Move;
       this._Move();
       
			return;
			function _Move(){
			document.onmousemove = function (e){
				if(!e){e = window.event;}
				offsetY=e.clientY + getScrollXY()[1];
				document.body.style.cursor = 's-resize'
				$x(Textarea).style.height = (offsetY-findPosY(Textarea)-2);
			};
			document.onmouseup   = function (){
				document.onmousemove = '';
				document.onmouseup ='';
				document.body.style.cursor = ''
			};
			}

}

/*workspace pages*/
function pg63_pageInit(){
init_ResizeableTextAreas(['P63_CONTENT','P63_HEADING_TEXT','P63_FOOTER_TEXT'],'100%',false,false);
}

function apex_RegionButtons(pThis,pThat){
 var lThis = $x(pThis);
 var lThat = $x(pThat);
 if(lThat){
	$x_SetSiblingsClass(lThis,'','current');
	if(lThis.id =='all'){$x_ShowChildren(lThat)}
	else{html_HideSiblings(lThat);}
 }
}

apexRB = apex_RegionButtons;

function $f_Enter(pThis,e){
    var keycode;
    if(window.event){keycode = window.event.keyCode;}
    else if (e){keycode = e.which;}
    else {return false;}
    if(keycode == 13){return true;}
    else{return false;}
}

function ws_SearchOnEnter(pThis){
 var lTest = $f_Enter(pThis);
 if(lTest){ws_SearchApply('SEARCH')}
}

function ws_Pd(pThis,pId){
	if(lT){lT.dCancel();}
	app_AppMenuMultiOpenBottom2(pThis,pId,false)
}


function ws_Calc(){
	document.location = 'f?p='+$x('pFlowId').value+':25:'+$x('pInstance').value+':::::';
}


