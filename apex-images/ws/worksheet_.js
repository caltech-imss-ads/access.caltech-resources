function wsrpt_getResult(appID,pageID){
  var maxRow = $x("P2_DISPLAY").value;
  var search = $x("P2_SEARCH").value;
  var holder = $x("rptResults");    
  var get = new htmldb_Get(null,appID,null,pageID, null,'wwv_flow.accept');
    get.addParam('P2_DISPLAY',maxRow);
    get.addParam('P2_SEARCH',search);    
  cDebug(get.url());
  var results = get.get(null,'<apex:DS_RPT>','</apex:DS_RPT>');
  holder.innerHTML = results;
  results = null;
  get = null;
  cDebug(x);
}