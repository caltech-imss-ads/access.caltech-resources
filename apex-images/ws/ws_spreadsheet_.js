try{
if (apex.spreadsheet==null || typeof(apex.spreadsheet)!="object"){apex.spreadsheet = new Object()}

	
function ws_Spreadsheet (pThis){
    this.Spreadsheet = $x(pThis)
	this.numberRows = false;
	this.numberCells = false;
	this.currentRow = false;
	this.classArray = false;


	this.currentCell = false;
	this.currentForm = false;
	this.currentCol = false;
	this.currentColType = false;
	
	/**/
	this.Reset = Reset;
	this.CellInit = CellInit;
	this.SpreadInit = SpreadInit;


    this.cells = {}
	this.cells.edit = function(pThis,pThat){
		that.currentCell.innerHTML = '';
		gReport.controls.lov = true;
		pThat.onclick=''
		gReport.controls.lov_dom = pThat;
		gReport.controls.col_lov(pThis);
	}
	return;

	function SpreadInit(){
		var lRows = this.Spreadsheet.rows;
		this.numberRows = lRows.length;
		if(!!this.numberRows[0]){
		this.numberCells = lRows[0].cells.length;

		var EditableCol = [];
		var EditableColN = [];
		var lDivs = lRows[0].getElementsByTagName('DIV');

		for (var i = 0;i<lDivs.length;i++){
			if(!isNaN(parseInt(lDivs[i].id.substring(1)))){
				EditableCol[EditableCol.length] = lDivs[i].parentNode;
				EditableColN[EditableColN.length] = lDivs[i].id;
				lDivs[i].parentNode.style.width = lDivs[i].parentNode.offsetWidth;
			}
		}
		}
	}

    function CellInit(pThis){
		that = this;
		this.Reset();
		this.currentRow = pThis.parentNode.parentNode;
		this.currentCell = pThis;
		this.classArray = pThis.className.split(" ");
		this.currentCol = this.classArray[0];
		this.currentColType = this.classArray[1];
		//pThis.style.padding = '0';//pThis.parentNode.offsetWidth;
		if(this.currentColType == 'textarea' && !isNaN(this.currentCol.substring(1))){
			var lValue = pThis.innerHTML;
			pThis.innerHTML = '';
			pThis.onclick=''
			//pThis.parentNode.style.padding = '0';//pThis.parentNode.offsetWidth;
			pThis.style.padding = '0';//pThis.parentNode.offsetWidth;
			var lT = $dom_AddTag(pThis,'TEXTAREA');
			lT.style.background = '#fff';
			lT.style.position = 'absolute';
			lT.style.height = pThis.parentNode.offsetHeight;
			lT.focus();
			if(pThis.offsetWidth<250){lT.style.width = '250px';}else{lT.style.width = pThis.offsetWidth;}
			lT.style.zIndex = '999';

			this.currentForm = lT;
			this.currentValue = lValue;
			lT.value=lValue;
			lT.onkeydown = function(e){cellkey(e,this)}
			lT.onblur = function(e){cellsave(e,this)}
			lT.focus();
		}else if(this.currentColType == 'text' && !isNaN(this.currentCol.substring(1))){
			$x_Style(pThis.parentNode,'visibility','hidden');
			var lValue = pThis.innerHTML;
			pThis.innerHTML = '';
			pThis.onclick=''
			//pThis.parentNode.style.width = pThis.parentNode.offsetWidth;

			var lT = $dom_AddInput(pThis,'TEXT','','',lValue);
			lT.style.position = 'relative';
			lT.focus();
			lT.style.zIndex = '999';
			lT.parentNode.style.padding = '0';
			lT.style.width='100%';
			this.currentForm = lT;
			this.currentValue = lValue;
			lT.value=lValue;
			lT.onkeydown = function(e){cellkey(e,this)}
			lT.onblur = function(e){cellsave(e,this)}
			lT.focus();
			$x_Style(pThis.parentNode,'visibility','visible');
		}else if(this.currentColType == 'selectlist' && !isNaN(this.currentCol.substring(1))){
			 that.currentValue = pThis.innerHTML;
			 that.cells.edit(that.currentCol,pThis);
		}else{this.Reset();}

	}

	function Reset(){
	    var lValue = this.currentValue;
		if(this.currentCell){
			//console.log(lValue)
			this.currentCell.innerHTML = lValue;

			this.currentCell.style.padding = '';
			this.currentCell.onclick = function(){initCell(this)}
		}
		this.currentRow = false;
		this.currentCell = false;
		this.currentForm = false;
		this.currentCol = false;
	}
}

function initCell(pThis){gSpread.CellInit(pThis);}

function cellkey(e,pThis){
    var keycode;
    if(window.event){keycode = window.event.keyCode;}
    else if (e){keycode = e.which;}
    else {return false;}
	if(e.ctrlKey){
	switch(keycode){
		case 9 :runTab(pThis); break;
		case 13 :runTab(pThis);/*enter*/ null; break;
		case 37 :runTab(pThis);/*left*/ null; break;
		case 38 :runTab(pThis);/*up*/ null; break;
		case 39 :runTab(pThis);/*down*/ null; break;
		case 40 :runTab(pThis);/*right*/ null; break;
		case 17 :runTab(pThis);/*ctrl*/ null; break;
		default:null;
	}
	}
}

function runTab(pThis){
	if(pThis.parentNode.cellIndex < gSpread.numberCells-1){
	 gSpread.CellInit(pThis.parentNode.nextSibling);
	}else if(pThis.parentNode.cellIndex == gSpread.numberCells-1){
	 if(gSpread.currentRow.rowIndex == gSpread.Spreadsheet.rows.length-1){
		gSpread.CellInit(gSpread.Spreadsheet.rows[1].cells[1]);
	 }else{
	  gSpread.CellInit(gSpread.Spreadsheet.rows[gSpread.currentRow.rowIndex+1].cells[1]);
	 }
	}
}




function cellsave(e,pThis){
    if(gSpread.currentValue != pThis.value){
		this.dGet = dGet;
		var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=ws_SaveCell',0);
		this.dGet();
		gSpread.currentValue = $v(pThis);
	}else{
		gSpread.Reset();
	}
	return;
	
	function dGet(){
		get.add('AJAX_WS_COLUMN',gSpread.currentCol);
		get.add('TEMPORARY_ITEM',pThis.value);
		get.add('CURRENT_WORKSHEET_ROW',gSpread.currentRow.id.substring(3));
	 	get.GetAsync(dShow);
	}
	
	function dShow(){
	 	if(p.readyState == 1){
	 				ajax_Loading(p.readyState);
	 	}else if(p.readyState == 2){
	 	}else if(p.readyState == 3){
	 	}else if(p.readyState == 4){
	 				ajax_Loading(p.readyState);
					gSpread.Reset();
		
		}
	}
}

function addOption(selectbox,text,value ){
	var optn = document.createElement("OPTION");
	optn.text = text;
	optn.value = value;
	selectbox.options.add(optn);
	return optn
}




function ws_GetSelectItem(pThis,pVal){
		this.dGet = dGet;
		var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=ws_GetSelectItem',0);
		this.dGet();
	return;
	
	function dGet(){
		get.add('CURRENT_WORKSHEET_ROW',gSpread.currentRow.id.substring(3));
		get.add('AJAX_WS_COLUMN',gSpread.currentCol);
	 	get.GetAsync(dShow);
	}
	
	function dShow(){
	 	if(p.readyState == 1){
	 				ajax_Loading(p.readyState);
	 	}else if(p.readyState == 2){
	 	}else if(p.readyState == 3){
	 	}else if(p.readyState == 4){
			ajax_Loading(p.readyState);
			var myObject = eval('(' + p.responseText + ')');
			for (var i=0;i<myObject.row.length;i++){
				var lOpt = addOption(pThis,myObject.row[i].R,myObject.row[i].R);
				if(myObject.row[i].R == pVal){lOpt.selected=true;}
			}
		}
	}
}

}catch(e){}
