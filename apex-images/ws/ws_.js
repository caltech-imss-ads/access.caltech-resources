/*

*/
function ws_GetRow(pDir){
	var get = new htmldb_Get('drop',$x('pFlowId').value,'APPLICATION_PROCESS=Column_Pull',0);
	if(pDir == 'prev'){
		if(!isEmpty('ajax_prev')){get.add('CURRENT_WORKSHEET_ROW',$x('ajax_prev').value);}
	}else{
		if(!isEmpty('ajax_next')){get.add('CURRENT_WORKSHEET_ROW',$x('ajax_next').value);}
	}
	gReturn = get.get();
	get = null;
	$x('rowcount').innerHTML = $x('ajax_rowcount').value;
	wsCheckNav();
}

function wsCheckNav(){
		$x('rowcount').innerHTML = $x('ajax_rowcount').value;
		if(isEmpty('ajax_prev')){$x_Hide('prev_button');}else{$x_Show('prev_button');}
		if(isEmpty('ajax_next')){$x_Hide('next_button');}else{$x_Show('next_button');}
		
}

function wsPageInit(){wsCheckNav();}


function initResizeColumns(pThis){
	var lTable = $x(pThis);
	var lTable_TH = lTable.getElementsByTagName('TH');
	for(var i=0;i<lTable_TH.length;i++){
		var lDiv = $dom_AddTag(lTable_TH[i],'span');
		lDiv.className = 'mv';
		lDiv.onmousedown = function(){
			var lCol = $x_GetColumn(this);
			var l_left = findPosX(this.parentNode);
			$x_Style(document.body,'cursor','col-resize')
			
			document.body.onmousemove = function (event){
				$x_Style(lCol,'width',event.pageX-(l_left-4));
			}
			
			document.body.onmouseup = function (event){
			   document.body.onmousemove = '';
			   $x_Style(document.body,'cursor','')
			}
	}
	}
}


function $x_GetColumn(pThis){
	var l = pThis.parentNode;
	var l_Col = [];
	var l_TD = $x_UpTill(pThis,'TABLE').getElementsByTagName('TD');
	var l_TH = $x_UpTill(pThis,'TABLE').getElementsByTagName('TH');
    var l_D = $dom_JoinNodeLists(l_TD,l_TH);
	var l_Column = l.cellIndex;
	for(var i=0;i<l_D.length;i++){if(l_D[i].cellIndex == l_Column){l_Col.push(l_D[i])}}
    return l_Col
}

var gDragger = false;
var gDragX = 0;
var gDragY = 0;
var gDropables = [];
function init_Drag(){
	var lItems = getElementsByClass('icon','icons','div');
	
	for(var i=0;i<lItems.length;i++){if(lItems[i].id!='1'){drag_MouseDown(lItems[i]);}}

	return;
}

function drag_MouseDown(pThis){
	supressSelect();
  if(window.addEventListener){pThis.addEventListener('mousedown',drag_ob,true);}
	else{pThis.attachEvent('onmousedown',drag_ob);}
	pThis.ondblclick = function(){return false;};
	//pThis.onclick = function(){return false;};
	supressSelect();

	return;
}

function drag_ob(e){
	supressSelect();
	pThis = html_GetTarget(e);
	if(pThis.nodeName != 'A'){
	if(!html_SubString(pThis.className,'icon')){pThis = $x_UpTill(pThis,'DIV');}
	gDragger = pThis;
	document.onmouseup = drag_stop;
	document.onmousemove = drag_move;
	supressSelect();
	}
}


function drag_stop(){
	check_pos();
	document.onmouseup = null;
	document.onmousemove = null;
	drag=false;
	gFormRows = false;
	gDragger = false;
	targ = false;
	//$x_Remove('spacer');
	currentSpacer = false;
}

function drag_move(e){
	if(!e){var e=window.event};
	var lTargParent =$x('tempdrag');
	targ = gDragger.cloneNode(true);   $x_Hide(gDragger);
   gDropables = getElementsByClass('folder','icons','div');
	with (targ){
	 removeAttribute('onmouseover');
	 removeAttribute('onmouseout');
	 removeAttribute('onmousemove');
	 style.position = 'absolute';
	}
	lTargParent.appendChild(targ);
	// calculate event X,Y coordinates
	offsetX=e.clientX + getScrollXY()[0]
	offsetY=e.clientY + getScrollXY()[1]
	// assign default values for top and left properties
	if(!targ.style.left){targ.style.left=offsetX-(targ.offsetWidth /2)};
	if(!targ.style.top){targ.style.top=offsetY-(targ.offsetHeight /2)};
	// calculate integer values for top and left properties
	coordX=parseInt(targ.style.left);
	coordY=parseInt(targ.style.top);
	drag=true;
	document.onmousemove = dragDiv;

 }

// continue dragging
function dragDiv(e){
	if(!e){var e=window.event};
	if(!drag){return};
	// move div element	supressSelect();
	lDragX = (coordX+getScrollXY()[0])+e.clientX-offsetX;
	lDragY = (coordY+getScrollXY()[1])+e.clientY-offsetY;
	targ.style.left = lDragX + 'px';
	targ.style.top = lDragY+'px';
	if(Math.abs(lDragX-gDragX)>=5||Math.abs(lDragY-gDragY)>=5){
		gDragX = lDragX;
		gDragY = lDragY;
	}
	supressSelect();
	toolTip_disable();
	return false;
}


function supressSelect(){if(!ie){window.getSelection().removeAllRanges()}else{document.selection.empty()}}

function check_pos(){
  this._Drop = _Drop;
	this._Clean = _Clean;
	var lDelete = true;
   var lTest = drag_Drop(targ);
   if(lTest){
    var lRet = ws_ReParent(lTest.id,targ.id,targ.className);
    if(lRet.result == true){
      $x_Remove(gDragger);
    }
   }
    	this._Clean();
	return;
	  function _Drop(){
       this._Clean();
		 return lEl;
	}
	function _Clean(){
		   $x_Remove('oldholder');
			$x('tempdrag').innerHTML='';
			$x_Show(gDragger);
	}
}


function drag_Drop(pThis){
		var lReturn=false,lReturn2=false;
		var l = findPos(pThis)
		var lX = l[0];
		var lY = l[1];
		var lDrop = [];
	for (var i=0;i<gDropables.length;i++){
		lDrop[i] = findPos(gDropables[i]);
		lDrop[i][2] = lDrop[i][0] + gDropables[i].offsetWidth;
		lDrop[i][3] = lDrop[i][1] + gDropables[i].offsetHeight;
		lDrop[i][4] = gDropables[i];
	}
		for (var i=0;i<lDrop.length;i++){
			if(lDrop[i][0] < lX && lX < lDrop[i][2]){lReturn = true}
			if(lDrop[i][1] < lY && lY < lDrop[i][3]){lReturn2 = true}
			if(lReturn && lReturn2){return lDrop[i][4];break;}
		}
		return false;

}



function findPos(obj){
	var curleft=0,curtop=0;
	if (obj.offsetParent){
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while(obj = obj.offsetParent){
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}
	curtop += html_GetPageScroll();
	return [curleft,curtop];
}



function ws_ReParent(pParent,pChild,pClass){
	var get = new htmldb_Get(null,$x('pFlowId').value,'APPLICATION_PROCESS=NewParent',0);
	get.add('TEMPORARY_FOLDER',pParent);
	get.add('TEMPORARY_ITEM',pChild);
	if(html_SubString(pClass,'folder')){
		get.add('TEMPORARY_ACTION','FOLDER');
	}else if (html_SubString(pClass,'webpage')){
		get.add('TEMPORARY_ACTION','WEBPAGE');
	}else{
		get.add('TEMPORARY_ACTION','WORKSHEET');
	}
	gReturn = get.get();
	get = null;
	return eval('('+gReturn+')');
}


