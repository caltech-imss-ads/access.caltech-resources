var isMax = false;

setTimeout(resizeCodeArea, 1000);

function saveCode(){

   document.body.style.cursor = 'wait';

   var get = new htmldb_Get(null,4500,'SAVE_CODE',2250, null, 'wwv_flow.accept');
   var plsqlCode = lEditor.getCode();
   var i=0;

   get.addParam('f02',plsqlCode.length);

   if (plsqlCode.length<=4000) {
     get.addParam('f01',plsqlCode);
   } else {
     while (plsqlCode.length>4000) {
       get.addParam('f01',plsqlCode.substr(0,4000));
       plsqlCode = plsqlCode.substr(4000,plsqlCode.length-4000);
       i++;
     }
     get.addParam('f01',plsqlCode);
   }

   var x = get.get('FULL');

   document.body.style.cursor = 'default';
}


function doCompile(){
   saveCode();

   document.body.style.cursor = 'wait';

   var get = new htmldb_Get(null,4500,'APPLICATION_PROCESS=compile_plsql_code',0);
   var message = get.get('FULL');
   var status = message.substr(0,1);
   message = message.substr(1);
   html_GetElement('results').innerHTML = message;
   if (status=='0') {
      html_GetElement('results').className = 'compileFailure';
   } else {
      html_GetElement('results').className = 'compileSuccess';
   }

   document.body.style.cursor = 'default';
}


function goToLine(lineID){
   lEditor.jumpToLine(Number(lineID));
}


function downloadCode() {
   saveCode();
   doSubmit('DOWNLOAD_CODE');
}


function resizeCodeArea(){
  var pageId = $v("pFlowStepId");
  var codeArea = apex.jQuery('.CodeMirror-wrapping')[0];
  var iframeEl = window.parent.$x('dbaseContent');
  var parentNode = window.parent.$x("dbaseContent");
  if ( pageId == "2250" ) {
    if ( isMax ) {
       codeArea.style.height = iframeEl.clientHeight - 190;
    } else {
       codeArea.style.height = iframeEl.scrollHeight * 0.70;
	  } 
  }
}

function maximize() {
  var iframeEl = window.parent.$x('dbaseContent'), l_Width, lHeight;
  html_HiddenElement(iframeEl);
  isMax = true;
  html_HideElement('func_tab_menu');
  document.getElementById('maximizer').className = 'maximized';
  html_GetElement('ob_ObjectsDetail').innerHTML = window.parent.html_GetElement('ob_ObjectsDetail').innerHTML;
  html_ShowElement('ob_ObjectsDetail');
  if(document.all){
    l_Width = parent.document.body.offsetWidth;
    l_Height = parent.document.body.offsetHeight;
  } else {
    l_Width = '100%';
    l_Height = '100%';
  }
	iframeEl.style.position = "absolute";
	iframeEl.style.top = "0px";
	iframeEl.style.left = "0px";
	iframeEl.style.width = l_Width;
	iframeEl.style.height = l_Height;
  html_GetElement('ed_PageMargin').style.margin = 0;
  parent.document.body.style.overflow = 'hidden';
  parent.g_SkipResize = true;
  resizeCodeArea();
  html_VisibleElement(iframeEl);
}


function restoreSize() {
  var iframeEl = window.parent.$x('dbaseContent');
  parent.document.body.style.overflow = 'auto';  
  window.parent.html_ShowElement('obLeftColumn');
  if (isMax) {
    html_GetElement('maximizer').className = 'notMaximized';
	  html_HideElement('ob_ObjectsDetail');
    html_ShowElement('func_tab_menu');
    iframeEl.style.position = "relative";
	  iframeEl.style.top = "0px";
	  iframeEl.style.left = "0px";
	  iframeEl.style.width = "100%";
	  if(document.all){
	    iframeEl.style.height =(parseInt(window.parent.document.getElementById('obTable').offsetHeight, 10) - parseInt(window.parent.document.getElementById('obRightHeader').offsetHeight, 10) - 200) + "px";
    }else{
	    iframeEl.style.height = (parseInt(window.parent.document.getElementById('obTable').offsetHeight, 10) - parseInt(window.parent.document.getElementById('obRightHeader').offsetHeight, 10)) + "px";
		}  
    html_GetElement('ed_PageMargin').style.margin = 10;
    isMax = false;
    resizeCodeArea();
  }
  parent.g_SkipResize = false;
  parent.document.body.style.overflow = '';
}

function findInCodeArea(searchString, caseSensitive, notFoundMsg) {
  var searchHandler = lEditor.getSearchCursor(searchString, true, (!caseSensitive));
  if (searchHandler.findNext()) {
    searchHandler.select();
  } else {
    alert(searchString + ' ' + notFoundMsg);
  }
}

function replaceInCodeArea(searchString, replaceString, caseSensitive, notFoundMsg) {
  if (lEditor.options.readOnly===false) {
    // if something has already been selected, replace it. Otherwise execute a search
    if (lEditor.selection()) {
      lEditor.replaceSelection(replaceString);
    } else {
      findInCodeArea(searchString, caseSensitive, notFoundMsg);
    }
  }
}

function replaceAllInCodeArea(searchString, replaceString, caseSensitive) {
  if (lEditor.options.readOnly===false) {
    // always start from beginning to replace everything in the document
    var searchHandler = lEditor.getSearchCursor(searchString, false, (!caseSensitive));
    while(searchHandler.findNext()){
      searchHandler.replace(replaceString);
    }
  }
}

function undoEdit() {
  lEditor.undo();
}

function redoEdit() {
  lEditor.redo();
}
