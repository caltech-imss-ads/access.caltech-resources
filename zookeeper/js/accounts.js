function start_job(job_type, button, redirect) {
  button.before("<div id='job-status' class='new'>Please wait while the {0} job is being prepared.".format(job_type.replace('_', ' ')) + 
      "<br>This may take up to a minute.<br> <img src='/res/zookeeper/images/busy.gif'></div>");
  button.hide();
  // Disable all the inputs on the edit form, so that uers aren't tempted to alter them while a job is in progress.
  $('#edit-account').find('input, select').prop('disabled', true);
  
  $.ajax({
    url:  g_ajax_add_job_url,
    data: {target: $('#account-name').val(), job_type: job_type},
    type: "POST",
    success: function(response) {
      switch (response.result) {
        case "OK":
          setInterval(poll_job, 5000, response.job_id, redirect);
          break;
        case "ERROR":
          alert(response.message);
          // Reload the page by redirecting back to itself.
          window.location.replace(window.location.href);
          break;
        default:
          alert("An unexpected result came back from the {0} job start command!".format(job_type));
          break;
      }
    },
    error: error_alert
  });
}

$(document).ready(function(){
  $('#sync-account').click(function(event) {
    answer = confirm("Please remember that if you changed any group associations for this account, the Save Changes button must be pressed first.\n" +
      "Do you want to perform a Sync now?"
    );
    if (answer) {
      // Start the account sync job, and tell it to redirect to the current page when it's done.
      start_job('account_sync', $(this), window.location.href);
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
  });
  
  $('#reset-account').click(function(event) {
    answer = confirm("You should only perform this action if there is an irreconcilable sync problem with this account.\n" +
      "It will completely erase all contacts and groups on the MailChimp side.\n" +
      "Are you sure you want to reset this account?"
    );
    if (answer) {
      // Start the account reset job, and tell it to redirect to the current page when it's done.
      start_job('account_reset', $(this), window.location.href);
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
  });
    
  $('#delete-account').click(function(event) {
    answer = confirm("Are you sure you want to delete Zookeeper's record of this MailChimp Account?\n" +
      "For information security purposes, this action will also erase all contacts on the MailChimp account, which can take several minutes."
    );
    if (answer) {
      // Start the account delete job, and tell it to redirect to the account list page when it's done.
      start_job('account_delete', $(this), g_list_accounts_url);
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
  });
});
