function start_job(job_type, button, group, redirect) {
  button.before("<div id='job-status' class='new'>Please wait while the {0} job is being prepared.".format(job_type.replace('_', ' ')) + 
      "<br>This may take up to a minute.<br> <img src='/res/zookeeper/images/busy.gif'></div>");
  button.hide();
  // Disable all the inputs on the edit form, so that uers aren't tempted to alter them while a job is in progress.
  $('#edit-group').find('input, select').prop('disabled', true);
  
  $.ajax({
    url:  g_ajax_add_job_url,
    data: {target: group, job_type: job_type},
    type: "POST",
    success: function(response) {
      switch (response.result) {
        case "OK":
          setInterval(poll_job, 5000, response.job_id, redirect);
          break;
        case "ERROR":
          alert(response.message);
          window.location.replace(redirect);
          break;
        default:
          alert("An unexpected result came back from the {0} job start command!".format(job_type));
          break;
      }
    },
    error: error_alert
  });
}

function change_list_name_dropdown(service) {
  var list_name_dropdown = $('select[id$="-list_name"]');
  
  // Empty out the dropdown, then re-build it from one of the pre-built services'
  // list name dropdowns (e.g. select[id$="-ldap-list-names"])
  list_name_dropdown.empty();
  list_names_dropdown = $('select[id$="-{0}-list-names"]'.format(service)).clone()
  $.each(list_names_dropdown.children(), function(ndx, option) {
    list_name_dropdown.append(option);
  });
  
  // Sets the selected list_name, if it's known. Otherwise selects the first option.
  var known_list_name = $('#list_name').val()
  option = list_name_dropdown.children('option[value="{0}"]'.format(known_list_name)).eq(0)
  if (option.length > 0) {
    option.attr('selected', 'selected');
  }
  else {
    list_name_dropdown.children().eq(0).attr('selected', 'selected');
  }
}

function ajax_add_group(event) {
  // Instead of posting the form normally, post it via AJAX, so that the sync job can be started 
  // on the same page. If it comes back with an error status, the response will also have the
  // pre-rendered form HTML, so replace the form with that HTML.
  var form = $(this).parent();
  var button = $(this);
  // Hide the button instantly (rather than after the ajax call completes), so that the user can't accidentally click it twice. 
  button.hide();
  
  $.ajax({
    url:  form.attr("action"),
    data: form.serialize(),
    type: "POST",
    success: function(response) {
      switch (response.result) {
        case "OK":
          // Now that the group is created, start the group sync job, and tell it to redirect to the groups list page when it's done.
          start_job('group_sync', button, $('#ZKGroup--name').val(), g_list_groups_url)
          break;
        case "ERROR":
          $("#add-group").replaceWith(response.error_form);
          setup();
          break;
        default:
          alert("An unexpected result came back from the group add command!");
          break;
      }
    },
    error: error_alert
  });
  
  event.preventDefault();
  event.stopPropagation();
  return false;
}

function setup() {
  var service_dropdown = $('select[id$="-service"]')
  
  if (service_dropdown.length > 0) {
    var selected_service = service_dropdown.val();
    
    // Populate the list_name select.
    change_list_name_dropdown(selected_service)
    
    // Set up the on-change handler.
    service_dropdown.change(function() {
      change_list_name_dropdown($(this).val());
    });
    
    $('input#add-group-button').click(ajax_add_group);
  }
}

// On doc load, sets up the list_name dropdown with the appropriate pre-rendered one,
// the on-change handler for the service dropdown, and the "syncing" animated gif 
// shown when adding a new group or syncing one manually.
$(document).ready(function() {
  // If the form is classed with "disabled-synced" all the form elements inside it should be disabled.
  $('form.disabled-synced').find('input, select').prop('disabled', true);
  
  setup();

  $('input#delete_group').click(function() {
    answer = confirm('Are you sure you want to delete this group?')
    return answer;
  });
  
  // Attach the ajax call which adds the group sync job on the Group Listing page.
  sync_contacts_button = $('input#sync-group-button');
  if (sync_contacts_button.length) {
    sync_contacts_button.click(function() {
      // Start the group sync job, and tell it to redirect to the current page when it's done.
      start_job('group_sync', sync_contacts_button, $('#group-name').val(), window.location.href)
    });
  }
});
