// For IE8, which doesn't support setTimeout(func, 1000, param1, param2, ...)
/*@cc_on
(function(f) {
  window.setTimeout = f(window.setTimeout);   // overwrites the global function!
  window.setInterval = f(window.setInterval); // overwrites the global function!
})(function(f) {
  return function(c, t) {
    var a = [].slice.call(arguments, 2);  // gathers the extra args
    return f(function() {
      c.apply(this, a);                   // passes them to your function
    }, t);
  };
});
@*/
function error_alert(XMLHttpRequest, textStatus, errorThrown) {
  if (console !== undefined) {
    console.log(XMLHttpRequest);
  }
  alert(
    "AJAX CALL FAILED:\nStatus = " + textStatus + 
    "\nError = " + errorThrown + "\n" + 
    XMLHttpRequest.responseText + 
    "\nThe Zookeeper admins have been notified of this error."
  );
}

// Define a Python-like format function for strings.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

var job_counter = 0;
function job_timer() {
  job_counter++;
  var minutes = Math.floor(job_counter / 60);
  var seconds = job_counter % 60;
  $('#job-timer').html("{0}m{1}s".format(minutes, seconds));
}

function poll_job(job_id, redirect) {
  $.ajax({
    url:  g_ajax_poll_job_url,
    data: {job_id: job_id},
    type: "GET",
    success: function(response) {
      if (console !== undefined) {
        console.log("{0} job: {1}".format(response.job_type, response.status));
      }
      switch (response.status) {
        case 'new':
          // Just wait for the job to become active
          break;
        case 'active':
          // The first time the job is detected as active, change the status message and set up the timer.
          if ($('#job-status').hasClass('new')) {
            $('#job-status').html("The {0} job is now active, and has been running for <span id='job-timer'>0s</span>.".format(response.job_type) + 
                "<br>When it's finished, this page will reload.<br> <img src='/res/zookeeper/images/busy.gif'>");
            $('#job-status').addClass('active').removeClass('new');
            setInterval(job_timer, 1000);
          }
          break;
        case 'error':
          alert('An error occured during the {0} job. You will now be redirected to the Sync History page for details.'.format(response.job_type));
          window.location.replace(g_sync_history_url);
          break;
        case 'warning':
          alert('The {0} job finished, but with warnings. You will now be redirected to the Sync History page for details.'.format(response.job_type));
          window.location.replace(g_sync_history_url)
          break;
        case 'complete':
          window.location.replace(redirect)
          break;
        default:
          alert('An unknown job status was encountered: {0}'.format(response.status));
          break;
      }
    },
    error: error_alert
  });
}

// Make form errors stand out, and associate them more strongly with their attached field.
$(document).ready(function() {
  $("span.field_error").each(function(ndx, elem) {
    $(elem).parent().addClass("error_border");
  });
});
