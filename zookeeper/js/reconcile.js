function error_alert(XMLHttpRequest, textStatus, errorThrown) {
  if (errorThrown === undefined) {
    alert("An unknown error has occured, please inform the ADS group.");
  }
  else {
    alert("AJAX CALL FAILED:\nStatus = " + textStatus + "\nError = " + errorThrown + "\nXMLHttpRequest = " + XMLHttpRequest);
  }
}

function show_reconciling() {
  // Displays at least 3 reconciling contacts at once.
  $('li.unreconciled').slice(0, 3).addClass('reconciling');
  
  // Update the "Displaying X out of Y Contact Conflicts" line
  $('#conflict-count span:nth-child(1)').text($('li.reconciling').length);
  $('#conflict-count span:nth-child(2)').text($('li.unreconciled').length);
}

function ajax_reconcile_contact(event) {
  var form = $(this).parent().parent().parent().parent().parent(); // This contact's <form> is 5 generations above the save button.
  var button = $(this);
  
  $.ajax({
    url: form.attr('action'),
    data: form.serialize(),
    type: 'POST',
    success: function(response) {
      switch (response.result) {
        case "OK":
          form.parent().removeClass('unreconciled reconciling').fadeOut(500, show_reconciling);
          break;
        case "ERROR":
          alert(response.error_message);
          break;
        default:
          alert("An unknown error has occured!");
          break;
      }
    },
    error: error_alert
  });
  
  event.preventDefault();
  event.stopPropagation();
  return false;
}

function enable_save_button(event) {
    // Traverse up the DOM to find the table to which $(this) belongs.  It's not always the same distance upwards.
    var ancestor = $(this);
    while (!ancestor.hasClass('reconciler-table')) {
      ancestor = ancestor.parent()
    }
    var other_radios;
    
    // Only enable the save button if the other set of radio buttons already has a selection.
    if ($(this).hasClass('keep-radio')) {
      other_radios = ancestor.find('input.reconcile-type-radio:checked');
    }
    else {
      other_radios = ancestor.find('input.keep-radio:checked');
    }
    
    if (other_radios.length > 0) {
      ancestor.find('input.reconcile-submit').removeAttr('disabled');
    }
}

$(document).ready(function(){
  // All the conflicts are initially hidden, so start reconciling them by showing the first group.
  show_reconciling();
  
  $('input.reconcile-submit').click(ajax_reconcile_contact);

  // The save buttons start as disabled.  Enable it only when the attached "keep" an "type" radio button groups have a selected option.
  $('input.keep-radio').click(enable_save_button);
  $('input.reconcile-type-radio').click(enable_save_button);
});
