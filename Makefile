VERSION = 1.2.4
PACKAGE = access.caltech-resources

clean:
	rm -rf *.tar.gz dist *.egg-info *.rpm
	find . -name "*.pyc" -exec rm '{}' ';'

version:
	@echo ${VERSION}

image_name:
	@echo ${PACKAGE}

release:
	@echo "For access.caltech-resources, we publish to many environments, so unlike our other repos, you need to do"
	@echo
	@echo "  bin/release.sh <environment>"
	@echo
	@echo "  Example:  bin/release.sh test"
	@echo
	@echo "Note: you need a pipeline for each environment you want to release to.  To create a new pipeline:"
	@echo
	@echo "   cd terraform/app"
	@echo "   chtf 0.12.31"
	@echo "   terraform init --upgrade"
	@echo "   terraform workspace new <environment>"
	@echo "   terraform plan"
	@echo "   terraform apply"
