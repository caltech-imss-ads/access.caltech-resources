/*!
 *
 * Require JS configuration for JET
 * Copyright (c) 1999, 2019, Oracle and/or its affiliates. All rights reserved.
 *
 */
/**
 * Require.js configuration
 */
( function( $, debug ) {
    "use strict";

    var dbg = !!$v( "pdebug" );

    requirejs.config({

        // Path mappings for the logical module names
        baseUrl: apex_img_dir + "libraries/",
        paths: {
            "jquery":               "./jquery/3.4.1/jquery-3.4.1.min",
            "jqueryui-amd":         "./oraclejet/7.2.0/js/libs/jquery/jqueryui-amd-1.12.1.min",
            "ojs":                  "./oraclejet/7.2.0/js/libs/oj/v7.2.0/" + ( dbg ? "debug" : "min" ),
            "ojL10n":               "./oraclejet/7.2.0/js/libs/oj/v7.2.0/ojL10n",
            "ojtranslations":       "./oraclejet/7.2.0/js/libs/oj/v7.2.0/resources",
            "text":                 "./oraclejet/7.2.0/js/libs/require/text",
            "promise":              "./oraclejet/7.2.0/js/libs/es6-promise/es6-promise.min",
            "hammerjs":             "./hammer/2.0.8/hammer-2.0.8.min",
            "signals":              "./oraclejet/7.2.0/js/libs/js-signals/signals.min",
            "ojdnd":                "./oraclejet/7.2.0/js/libs/dnd-polyfill/dnd-polyfill-1.0.0.min",
            "css":                  "./oraclejet/7.2.0/js/libs/require-css/css.min",
            "customElements":       "./oraclejet/7.2.0/js/libs/webcomponents/custom-elements.min",
            "proj4":                "./oraclejet/7.2.0/js/libs/proj4js/dist/proj4",
            "touchr":               "./oraclejet/7.2.0/js/libs/touchr/touchr"
        },

        // Shim configurations for modules that do not expose AMD
        shim: {
            "jquery": {
                exports: [ "jQuery", "$" ]
            }
        },

        // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
        // resources with a custom translation file.
        // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
        // a path that is relative to the location of this main.js file.
        config: {
            ojL10n: {
                merge: {
                    //"ojtranslations/nls/ojtranslations": "./oraclejet/3.0.0/js/libs/oj/v3.0.0/resources/nls/myTranslations"
                }
            },
            text: {
                // Override for the requirejs text plugin XHR call for loading text resources on CORS configured servers
                // eslint-disable-next-line no-unused-vars
                useXhr: function (url, protocol, hostname, port) {
                    // Override function for determining if XHR should be used.
                    // url: the URL being requested
                    // protocol: protocol of page text.js is running on
                    // hostname: hostname of page text.js is running on
                    // port: port of page text.js is running on
                    // Use protocol, hostname, and port to compare against the url being requested.
                    // Return true or false. true means "use xhr", false means "fetch the .js version of this resource".
                    return true;
                }
            }
        }
    });

})( apex.jQuery, apex.debug );