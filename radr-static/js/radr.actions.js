var bmk = function (ele) {
	document.select_form.q.value = ele;
	document.select_form.action = "/dms_web/Bookmark/Add";
	document.select_form.target = "_self";
	document.select_form.submit();
};

var edit_res = function (ele) {
	document.select_form.q.value = ele;
	document.select_form.target = "_self";
	document.select_form.action = "/dms_web/Document/Edit";
	document.select_form.submit();
};

var edit_links = function (ele) {
	document.select_form.q.value = ele;
	document.select_form.target = "_self";
	document.select_form.action = "/dms_web/Link/ListLinks";
	document.select_form.submit();
};

var sort = function (attr, asc) {
	document.list_form.attr.value = attr;
	document.list_form.asc.value = asc;
	document.list_form.submit();
};

var span = function (comp) {
	var text = comp.options[comp.selectedIndex].value;
	document.list_form.span.value = text;
	document.list_form.submit();
};

var nextSpan = function () {
	document.list_form.span.value = cur_span + 1;
	document.list_form.submit();
	
};

var prevoiusSpan = function () {
	document.list_form.span.value = cur_span - 1;
	document.list_form.submit();
};

var add_doc = function () {
	document.select_form.target = "_self";
	document.select_form.action = '/dms_web/Document/Add';
	document.select_form.submit();
};