jQuery(function() {
   var one_minute = 60*1000;
   var twenty_five_minutes = 25*one_minute;
   if (keepActive == '1' ) {
      // Send a heartbeat every 25 minutes.
      var updateInterval = setInterval(heartbeat, twenty_five_minutes);
      // Stop beating after 8 hrs.
      setTimeout(function() { clearInterval(updateInterval); },  8*60*one_minute);
   }
});
function heartbeat() {
   $.ajax({type: "GET", url: "https://access.caltech.edu/regis_resp/regisMenu.do"});
}
