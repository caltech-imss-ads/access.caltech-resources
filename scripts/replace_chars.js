function replace_chars(textField)
{
     	var replacements, regex, key;
     	var text = textField.value;
     
     	replacements = {
		"\xa0": " ",		"\xa1": "",		"\xa6": "|",
		"\xa9": "(c)",		"\xab": '"',		"\xae": "(r)",
		"\xb0": " deg",		"\xb1": "+/-",		"\xb2": "2",
		"\xb3": "3",		"\xb7": "*",		"\xb9": "1",
		"\xbb": '"',		"\xbc": "1/4",		"\xbd": "1/2",
		"\xbe": "3/4",		"\xbf": "",		"\xc0": "A",
		"\xc1": "A",		"\xc2": "A",		"\xc3": "A",
		"\xc4": "A",		"\xc5": "A",		"\xc6": "AE",
		"\xc7": "C",		"\xc8": "E",		"\xc9": "E",
		"\xca": "E",		"\xcb": "E",		"\xcc": "I",
		"\xcd": "I",		"\xce": "I",		"\xcf": "I",
		"\xd0": "DH",		"\xd1": "N",		"\xd2": "O",
		"\xd3": "O",		"\xd4": "O",		"\xd5": "O",
		"\xd6": "O",		"\xd7": "x",		"\xd8": "O",
		"\xd9": "U",		"\xda": "U",		"\xdb": "U",
		"\xdc": "U",		"\xdd": "Y",		"\xde": "TH",
		"\xdf": "ss",		"\xe0": "a",		"\xe1": "a",
		"\xe2": "a",		"\xe3": "a",		"\xe4": "a",
		"\xe5": "a",		"\xe6": "ae",		"\xe7": "c",
		"\xe8": "e",		"\xe9": "e",		"\xea": "e",
		"\xeb": "e",		"\xec": "i",		"\xed": "i",
		"\xee": "i",		"\xef": "i",		"\xf0": "dh",
		"\xf1": "n",		"\xf2": "o",		"\xf3": "o",
		"\xf4": "o",		"\xf5": "o",		"\xf6": "o",
		"\xf7": "/",		"\xf8": "o",		"\xf9": "u",
		"\xfa": "u",		"\xfb": "u",		"\xfc": "u",
		"\xfd": "y",		"\xfe": "th",		"\xff": "y",
		"\u0152": "OE",		"\u0153": "oe",		"\u0160": "S",
		"\u0161": "s",		"\u0178": "Y",		"\u017d": "Z",
		"\u017e": "z",		"\u02c6": "^",		"\u2002": " ",
          	"\u2003": " ",		"\u2009": " ",		"\u2013": "-",
		"\u2014": "--",		"\u2018": "'",		"\u2019": "'",
		"\u201c": '"',		"\u201d": '"',		"\u2022": "*",
		"\u2026": "...",	"\u2030": "%",		"\u2039": "'",
		"\u203A": "'",		"\u2122": "(tm)"
	};
          
     	regex = {};
     
     	for (key in replacements) {
        	regex[key] = new RegExp(key, 'g');
     	}
     
     	for (key in replacements) {
        	text = text.replace(regex[key], replacements[key]);
     	}
     
     	textField.value = text;
}