function formatDate(dateField)
{  
    // apply the logic only when date is not null
    if (dateField.value != '')
    {
        var strDate   = "invalid";
        var separator = "/";
    
        var sDate = dateField.value;
        var delim = getDateDelimiter(sDate);

        var arrDate = new Array();
    
        // check delimiter length or string length
        if (delim.length == 1)
        {
            // separate the date string into parts
            arrDate = sDate.split(delim);
        }
        else if (isValidLength(sDate))
        {
            arrDate = new Array();

            arrDate[0] = sDate.substring(0,2);
            arrDate[1] = sDate.substring(2,4);
            arrDate[2] = sDate.substring(4);
        }
        
	// check array size
        if (arrDate.length == 3)
        {
            month = arrDate[0];
            mdate = arrDate[1];
            year4 = arrDate[2];

	    // check month and date
            if (month > 0 && month < 13 && mdate > 0 && mdate < 32)
            {
                if (month.length == 1)
                {
                    month = "0" + arrDate[0];
                }
                if (mdate.length == 1)
                {
                    mdate = "0" + arrDate[1];
                }
        
	        // check year
                if (isNumeric(year4))
                {
                    if (year4.length <= 4)
                    {
            	        if (year4.length == 1)
            	        {
                	    year4 = "200" + year4;
            	        } 
            	        if (year4.length == 2)
            	        {
                            if (year4 < 50)
                            {
                                year4 = "20" + year4;
                            }
                            else 
                            {
                                year4 = "19" + year4;
                            }
            	        }
                        if (year4.length == 3)
                        {
                            if (arrDate[2] < 900)
                            {
                                year4 = "2" + year4;
                            }
                            else
                            {
                                year4 = "1" + year4;
                            }
                        }

                        // check that the date is within range for the month/year
                        if (mdate <= daysInMonth(month,year4))
                        {
                            // construct the new date string
                            strDate = month + separator + mdate + separator + year4;
                        }
                    }
                }
            }
        }

        dateField.value = strDate;
    }
}

function daysInMonth(month,year)
{
    var m = [31,28,31,30,31,30,31,31,30,31,30,31];

    if (month != 2)
    {
        return m[month - 1];
    }

    if (year%4 != 0)
    {
        return m[1];
    }

    if (year%100 == 0 && year%400 != 0)
    {
        return m[1];
    }

    return m[1] + 1;
}

function isValidLength(input)
{
    var valid = false;

    if (isNumeric(input))
    {
        if (input.length == 6 || input.length == 8)
        {
            valid = true;
        }
    }

    return valid;
}

function isNumeric(input)
{
    return (input - 0) == input && input.length > 0;
}

function getDateDelimiter(sDate)
{
    // set a default return value
    var delim = "invalid";

    // check for a forward slash
    var pos1 = sDate.indexOf('/');
    
    if (pos1 != -1)
    {
        delim = '/';
    }
    else
    {
        // check for a dash
        pos1 = sDate.indexOf('-');
        
        if (pos1 != -1)
        {
            delim = '-';
        }
        else
        {
            // check for a period
            pos1 = sDate.indexOf('.');
            
            if (pos1 != -1)
            {
                delim = '.';
            }
        }
    }
    	
    return delim;
}
