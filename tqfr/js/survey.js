$(document).ready(function(){
  // Add the datepicker to the start_date and end_date inputs.
  $("input[id$='end_date']").datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm', separator: ' @ ', showMinute: false, hourGrid: 4})
  $("input[id$='start_date']").datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm', separator: ' @ ', showMinute: false, hourGrid: 4})
  $("#complete-surveys-date").datetimepicker({dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm', separator: ' @ ', showMinute: true, hourGrid: 4})

  $("#survey-form").submit(function(event) {
    $("#survey-form").hide().after('Please wait.  The cloning process will take several minutes...');
    return true;
  })

  $("#eligible-students-button").click(get_list);
  $("#incomplete-surveys-button").click(get_list);
  $("#students-complete-one-or-more-button").click(get_list);
  $("#complete-surveys-button").click(get_list);
  $('#import-survey-answers form').submit(qualtrix_upload);
  $('#refresh-report-tables form').submit(refresh_report_tables);
});

function get_list(event) {
  var this_form = $(this).parent();
  window.open(this_form.attr("action") + "?" + this_form.serialize(), "_blank")
  event.stopPropagation();
  event.preventDefault();
}

function qualtrix_upload(event) {
  // NOTE: This code will fail on IE9 and below, since they don't implement the FormData API.
  var form_data = new FormData($(this)[0]);
  var promise = $.ajax({
    type: 'POST',
    url: $(this).attr('action'),
    data: form_data,
    // You *must* include these two parameters in the POST, or jquery will screw up the data for some reason.
    processData: false,
    contentType: false
  });

  // During the import, display a "Loading...." animation below the (disabled) import form.
  $('#import-survey-answers').append($('<div id="progress-display"></div>'));
  $('#progress-display').Loadingdotdotdot({
    'speed': 400,
    'maxDots': 4,
    'word': 'Importing'
  });
  toggle_inputs('#import-survey-answers', false);

  // An error occured. Alert the user with the detailed error message. This will usually be something like:
  // "There's no instructor/TA in Exeter with that name. Please fix it."
  promise.fail(function(jqXHR, text_status, error_message) {
    toggle_inputs('#import-survey-answers', true);
    if (jqXHR.responseJSON) {
      // If the response contains JSON, alert the 'message'.
      alert(jqXHR.responseJSON.message);
    }
    else {
      // If there's no JSON, something really bad happened, so we alert the HTTP error message.
      alert('An unknown error has occured: ' + text_status + ' ' + jqXHR.status + ' ' + error_message);
    }
  });

  // The import was successful, so alert the user with info about how it went down.
  promise.done(function(data, textStatus, jqXHR) {
    toggle_inputs('#import-survey-answers', true);
    alert(data.message);
  });

  event.preventDefault();
  return false;
}

function refresh_report_tables(event) {
  // NOTE: This code will fail on IE9 and below, since they don't implement the FormData API.
  var form_data = new FormData($(this)[0]);
  var promise = $.ajax({
    type: 'POST',
    url: $(this).attr('action'),
    data: form_data,
    // You *must* include these two parameters in the POST, or jquery will screw up the data for some reason.
    processData: false,
    contentType: false
  });

  // During the import, display a "Loading...." animation below the (disabled) import form.
  $('#refresh-report-tables').append($('<div id="progress-display"></div>'));
  $('#progress-display').Loadingdotdotdot({
    'speed': 400,
    'maxDots': 4,
    'word': 'Refreshing report data'
  });
  toggle_inputs('#refresh-report-tables', false);

  // An error occured. Alert the user an error message.
  promise.fail(function(jqXHR, text_status, error_message) {
    toggle_inputs('#refresh-report-tables', true);
    if (jqXHR.responseJSON) {
      // If the response contains JSON, alert the 'message'.
      alert(jqXHR.responseJSON.message);
    }
    else {
      // If there's no JSON, something really bad happened, so we alert the HTTP error message.
      alert('An unknown error has occured: ' + text_status + ' ' + jqXHR.status + ' ' + error_message);
    }
  });

  // The import was successful, so alert the user with info about how it went down.
  promise.done(function(data, textStatus, jqXHR) {
    toggle_inputs('#refresh-report-tables', true);
    alert(data.message);
  });

  event.preventDefault();
  return false;
}

// Pass in the identifier for the parent (e.g. '#refresh-report-tables'), and whether you want them enabled or disabled.
function toggle_inputs(identifier, enabled) {
  if (enabled) {
    $('#progress-display').remove();
    $(identifier + ' input').prop('disabled', false);
  }
  else {
    $(identifier + ' input').prop('disabled', true);
  }
}
