$(document).ready(function(){
  var searchBox = $("input#search");
  var searchBoxDefault = "Search";
  
  //Searchbox show/hide default text
  searchBox.focus(function(){
    if ($(this).attr("value") == searchBoxDefault) {
      $(this).attr("value", "");
    }
  });
  
  searchBox.blur(function(){
    if ($(this).attr("value") == "") {
      $(this).attr("value", searchBoxDefault);
    }
  });
});
