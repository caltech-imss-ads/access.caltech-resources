/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSEVENT.js 115.1 1999/11/30 12:19:53 pkm ship   $ */

/*------------------------- Toolbar Events ------------------------*/


/* cancelClicked
 * -------------
 * This event handler is triggered when the user clicks the cancel button
 * or the cancel icon on the toolbar.  It will open up a modal window called
 * 'Cancel' that will prompt the user to confirm the 'cancel' action.
 */
function cancelClicked()
{
  var winWidth = 400;
  var winHeight = 200;
  var winAttributes = "menubar=no,location=no,toolbar=no," + 
                      "width=" + winWidth + ",height=" + winHeight + 
	              ",screenX=" + (screen.width - winWidth)/2 + 
                      ",screenY=" + (screen.height - winHeight)/2 + 
                      ",resizable=yes,scrollbars=yes";
  var url = top.getTop().scriptName + "/pos_window_sv.dialogbox";
  top.openModalWindow(url, "Cancel", winAttributes, winWidth, winHeight);
}


function submitDlg()
{
  var winWidth = 400;
  var winHeight = 200;
  var winAttributes = "menubar=no,location=no,toolbar=no," + 
                      "width=" + winWidth + ",height=" + winHeight + 
	              ",screenX=" + (screen.width - winWidth)/2 + 
                      ",screenY=" + (screen.height - winHeight)/2 + 
                      ",resizable=yes,scrollbars=yes";
  var url = top.getTop().scriptName + "/pos_ack_window_util.dialogbox";
  top.openModalWindow(url, "Cancel", winAttributes, winWidth, winHeight);
}

function submitDlg(l_rows)
{
  var winWidth = 400;
  var winHeight = 200;
  var winAttributes = "menubar=no,location=no,toolbar=no," +
                      "width=" + winWidth + ",height=" + winHeight +
                      ",screenX=" + (screen.width - winWidth)/2 +
                      ",screenY=" + (screen.height - winHeight)/2 +
                      ",resizable=yes,scrollbars=yes";
  var url = top.getTop().scriptName + "/pos_ack_window_util.dialogbox?" +
		"l_rows=" + l_rows;
  top.openModalWindow(url, "Cancel", winAttributes, winWidth, winHeight);
}




/* refreshFrame
 * ------------
 * This is the event handler for the refresh/reload button on the
 * toolbar.  It will only refresh frames with names other than those
 * listed below.
 */
function refreshFrame(p_frame)
{
  var i;
  var frameName;

  if (!p_frame)
    p_frame = top;

  if (p_frame.frames.length == 0)
  {  
    frameName = p_frame.name;
    if ((frameName != "toolbar") && (frameName != "borderLeft") &&
        (frameName != "upperbanner") && (frameName != "lowerbanner") &&
        (frameName != "borderRight") && (frameName != "controlregion"))
    {
      p_frame.location.reload();
    }
    return;
  }

  for (i=0; i<p_frame.frames.length; i++)
  {
     refreshFrame(p_frame.frames[i]);
  }

}


/* stopLoading
 * -----------
 * This is the event handler for the stop button on the toolbar.  It stops
 * all frames in the current window from loading.
 * Note: this does not work on IE.
 */
function stopLoading(p_win)
{
  var i;

  if (top.IS_IE)
  {
    alert("This does not work on IE");
    return;
  }

  if (!p_win)
    p_win = top;

  if (p_win.frames.length == 0)
  {
    p_win.stop();
  }

  for (i=0; i<p_win.frames.length; i++)
  {
    stopLoading(p_win.frames[i]);
  }

}


/* printWindow
 * -----------
 * Don't know how print is supposed to work yet, so just
 * print out an error message.
 */
function printWindow()
{
  if(top.IS_IE)
  {
   alert();
   return;
  }

  // Nav
//  var content=top.findFrame(top, frameName);
//  if(content && content.frames.length==0)
//  {
    top.frames[5].print();
    return;
//  }
  alert("The current page cannot be printed.");
}



/* loadHelp
 * --------
 * this does not load anything yet...
 */
function loadHelp()
{

  alert("no help available...");

}




/*------------------------- Control Region Events ------------------------*/

/* nextClicked
 * -----------
 * so far, this only repaints the control region...obviously, we need to
 * repaint the other frames a well.
 */
function nextClicked()
{
  var f = parent.controlregion;
  var c = parent.content;
  var b = parent.upperbanner;

  var controlRegionURL = top.scriptName + 
                         "/pos_control_region_sv.PaintControlRegion";
  var contentEditRegionURL = top.scriptName + "/pos_asn.show_edit_page";
  var contentReviewRegionURL = top.scriptName + "/pos_asn_review_pkg.review_page";
  var bannerURL = top.scriptName + "/pos_upper_banner_sv.PaintUpperBanner";

  if (top.whereAmI == "SELECT")
  {

    top.getTop().updateSearchCriteria();

    c.result.document.POS_ASN_RESULT_R.target = "content";
    c.result.document.POS_ASN_RESULT_R.pos_submit.value = "NEXT";
    c.result.document.POS_ASN_RESULT_R.submit();

    f.location.href = controlRegionURL + "?p_position=EDIT";
    b.location.href = bannerURL + "?p_product=ICX" + 
                       "&p_title=ICX_POS_ASN_EDIT";
    top.whereAmI = "EDIT";
  } 
  else if (top.whereAmI == "EDIT")
  {
    c.shipments.document.POS_ASN_SHIPMENTS.target = "content";
    c.shipments.document.POS_ASN_SHIPMENTS.POS_SUBMIT.value = "NEXT";
    c.header.document.POS_ASN_HEADER.submit();

    f.location.href = controlRegionURL + "?p_position=REVIEW";
    b.location.href = bannerURL + "?p_product=ICX" +
                       "&p_title=ICX_POS_ASN_REVIEW"; 
    top.whereAmI = "REVIEW";

  }

}


/* backClicked
 * -----------
 * so far, this only repaints the control region...obviously, we need to
 * repaint the other frames a well.
 */
function backClicked()
{ 
  var f = parent.controlregion;
  var c = parent.content;
  var b = parent.upperbanner;

  var controlRegionURL = top.scriptName + 
                         "/pos_control_region_sv.PaintControlRegion";
  var contentEditRegionURL = top.scriptName + "/pos_asn.show_edit_page";
  var contentSelectRegionURL = top.scriptName + 
                         "/pos_asn_search_pkg.search_page";
  var bannerURL = top.scriptName + "/pos_upper_banner_sv.PaintUpperBanner";

  var l_s_url;

  if (top.whereAmI == "EDIT")
  {
    l_s_url = top.re_start_row;

    c.shipments.document.POS_ASN_SHIPMENTS.target = "content";
    c.shipments.document.POS_ASN_SHIPMENTS.POS_SUBMIT.value = "BACK" + l_s_url;
    c.header.document.POS_ASN_HEADER.submit();

    f.location.href = controlRegionURL + "?p_position=SELECT";
    b.location.href = bannerURL + "?p_product=ICX" + 
                       "&p_title=ICX_POS_ASN_SELECT";
    top.whereAmI = "SELECT";
  } 
  else if (top.whereAmI == "REVIEW")
  {
    f.location.href = controlRegionURL + "?p_position=EDIT";
    c.location.href = contentEditRegionURL;
    b.location.href = bannerURL + "?p_product=ICX" + 
                       "&p_title=ICX_POS_ASN_EDIT";
    top.whereAmI = "EDIT";
  }

}


/* submitClicked
 * -------------
 */
function submitClicked(p_frame)
{
  if (top.whereAmI == "EDIT")
    p_frame.header.document.POS_ASN_HEADER.submit();
  else if (top.whereAmI == "REVIEW")
  {
    p_frame.document.POS_ASN_REVIEW.submit();
  }

}


/*------------------------- Other Button Events ------------------------*/

/* discard
 * -------
 * This event is triggered in the 'Cancel' window when the user clicks on
 * the discard button.  It closes all windows without saving.
 */
function discard()
{
   var scriptName = top.getTop().scriptName;
   var url = scriptName + "/pos_asn_master_pkg.discard";
   self.location.href = url;
   top.getTop().recursiveClose();
}


/* continue
 * --------
 * This event is triggered in the 'Cancel' window when the user clicks on
 * the continue button.  It closes the modal window so the user can
 * continue working.
 */
function continueWork(p_win)
{
  p_win.close();
}


/* openShipmentDetails
 * -------------------
 */
function openShipmentDetails(p_lineID, p_splitLineID, p_quantity, p_unit_of_measure)
{
  var scriptName = top.getTop().scriptName;
  var url = scriptName + "/pos_window_sv.modalWindow?p_asn_line_id=" +
            p_lineID + 
            "&p_asn_line_split_id=" + p_splitLineID + 
            "&p_quantity=" + p_quantity +
            "&p_unit_of_measure=" + p_unit_of_measure;
  top.getTop().openModalWindow(url, "userPref","",800,600);

}


/* cancelShipmentDetails
 * ---------------------
 */
function cancelShipmentDetails(p_win)
{
  p_win.close();
}


/* acceptShipmentDetails
 * ---------------------
 */
function acceptShipmentDetails(p_win)
{
  p_win.content.document.pos_asn_details.submit();

}



/* ASBNClicked
 * -------------
 * This event handler is triggered when the user clicks ASBN button
 * It will open up a modal window called  'ASBN Details'  Where the 
 * user supplier could enter ASBN Details
 */

function ASBNClicked()
{
  var winWidth = 500;
  var winHeight = 250;
  var winAttributes = "menubar=no,location=no,toolbar=no," +
                      "width=" + winWidth + ",height=" + winHeight +
                      ",screenX=" + (screen.width - winWidth)/2 +
                      ",screenY=" + (screen.height - winHeight)/2 +
                      ",resizable=yes,scrollbars=yes";

  var scriptName = top.getTop().scriptName;
  var url = scriptName + "/POS_ASBN.ASBN_DETAILS";
  top.getTop().openModalWindow(url, "test", winAttributes, winWidth, winHeight);

}

function ASBNCancel(p_win)
{
  p_win.close();
}

function ASBNSubmit(p_win)
{
  p_win.header.document.POS_ASBN_HEADER.submit();
}

function SearchPOs()
{
  document.POS_ACK_SEARCH.submit();
}

function clearFields()
{
  document.POS_ACK_SEARCH.POS_ACK_SUPPLIER_SITE.value="";
  document.POS_ACK_SEARCH.POS_ACK_PO_NUMBER.value="";
}


function updateSearchCriteria()
{
  var s_form = parent.content.criteria.document.POS_ASN_SEARCH_R;

  if (s_form.POS_VENDOR_SITE_ID != null)
    top.sr_supplier_site_id = s_form.POS_VENDOR_SITE_ID.value;

  if (s_form.POS_VENDOR_SITE_NAME != null)
    top.sr_supplier_site = s_form.POS_VENDOR_SITE_NAME.value;

  if (s_form.POS_SHIP_TO_LOCATION_ID != null)
    top.sr_ship_to_loc_id = s_form.POS_SHIP_TO_LOCATION_ID.value;

  if (s_form.POS_SHIP_TO_LOCATION != null)
    top.sr_ship_to_location = s_form.POS_SHIP_TO_LOCATION.value;

  if (s_form.POS_SUPPLIER_ITEM_NUMBER != null)
    top.sr_supplier_item_num = s_form.POS_SUPPLIER_ITEM_NUMBER.value;

  if (s_form.POS_ITEM_DESCRIPTION != null)
    top.sr_item_description = s_form.POS_ITEM_DESCRIPTION.value;

  if (s_form.POS_PO_NUMBER != null)
    top.sr_po_number = s_form.POS_PO_NUMBER.value;

  if (s_form.POS_ITEM_NUMBER != null)
    top.sr_item_number = s_form.POS_ITEM_NUMBER.value;

  if (s_form.POS_DATE_START != null)
    top.sr_date_start = s_form.POS_DATE_START.value;

  if (s_form.POS_DATE_END != null)
    top.sr_date_end = s_form.POS_DATE_END.value;

  if (parent.content.result.document.POS_ASN_RESULT_R != null)
  {
    top.re_start_row = 
       parent.content.result.document.POS_ASN_RESULT_R.pos_start_row.value;
  } else
    top.re_start_row = "0";
}


