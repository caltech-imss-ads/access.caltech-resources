/* $Header: czCmpMgr.js 115.7 2000/12/10 16:10:12 pkm ship     $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash K MacClay Created.                       |
 |                                                                           |
 +===========================================================================*/
 
function ComponentManager(wndName)
{
  this.compHash = new Array();
  this.superCompHash = new Array();
  if (wndName) {
    if (Styles.instances[wndName]) {
      Styles.clearInstance (wndName);
    }
    this.stylesInstance = new Styles (wndName);
  } else
    this.stylesInstance = new Styles ();
}

function ComponentManager_addComponent()
{
  if(arguments) {
    for (var i=0; i<arguments.length; i++) {
      if(arguments[i]) {
        if(arguments[i].enclosure)
          this.superCompHash[arguments[i].getName()] = arguments[i];
        else
          this.compHash[arguments[i].getName()] = arguments[i];
      }
      if(self.documentLoaded)
        arguments[i].runtimeRender();
    }
  }
}

function ComponentManager_addStyle(styl)
{
  if(styl)
    this.stylesInstance.addStyle(styl);

}

function ComponentManager_getStyle(name)
{	
  return (this.stylesInstance.getStyle(name));
}

function ComponentManager_removeComponent(componentId)
{
  //empty
}

function ComponentManager_populateComponents(dataSrc)
{
  for (var i in this.compHash) {
    if(this.compHash[i].populate) 
      this.compHash[i].populate(dataSrc);
  }
}

function ComponentManager_generateCSS(docObj)
{
  if (! docObj) {
    docObj = document;
  }
  docObj.writeln ('<STYLE TYPE="text/css">');
  for(var j in this.superCompHash) {
    docObj.writeln (this.superCompHash[j].generateCSS(this));
  } 
  for (var i in this.compHash) {
    docObj.writeln (this.compHash[i].generateCSS(this));
  }
  docObj.writeln (this.stylesInstance.generateCSS(this));
  docObj.writeln ('</STYLE>');
}

function ComponentManager_renderComponents(docObj)
{
  if (! docObj) {
    docObj = document;
  }
  for(var j in this.superCompHash) {
    docObj.writeln (this.superCompHash[j].render(this));
  }
  for (var i in this.compHash) {
    docObj.writeln (this.compHash[i].render(this));
  }
}

function ComponentManager_launchComponents(em, bLateBind)
{
  for(var j in this.superCompHash)
    this.superCompHash[j].launch(em);
  for (var i in this.compHash)
    this.compHash[i].launch(em);
  
  if (bLateBind) return;
  em.init();
  self.documentLoaded = true;
}

function ComponentManager_destroy ()
{
  if (! this.bDestroyed) {
    var id;
    for (id in this.superCompHash) {
      this.superCompHash[id].destroy();
      
      this.superCompHash[id] = null;
      delete this.superCompHash[id];
    }
    this.superCompHash = null;
    delete this.superCompHash;

    for (id in this.compHash) {
      this.compHash[id].destroy ();
      
      this.compHash[id] = null;
      delete this.compHash[id];
    }
    this.compHash = null;
    delete this.compHash;
    
    this.bDestroyed = true;
  }
}

ComponentManager.prototype.addComponent 	= ComponentManager_addComponent;
ComponentManager.prototype.addStyle 		= ComponentManager_addStyle;
ComponentManager.prototype.getStyle 		= ComponentManager_getStyle;
ComponentManager.prototype.removeComponent 	= ComponentManager_removeComponent;
ComponentManager.prototype.populateComponents 	= ComponentManager_populateComponents;
ComponentManager.prototype.generateCSS 		= ComponentManager_generateCSS;
ComponentManager.prototype.renderComponents = ComponentManager_renderComponents;
ComponentManager.prototype.launchComponents = ComponentManager_launchComponents;
ComponentManager.prototype.destroy = ComponentManager_destroy;
