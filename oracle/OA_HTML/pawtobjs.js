//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtobjs.js                       |
//  |                                                |
//  | Description: Client-side class definition      |
//  |              Employee, Employees               |
//  |              Type, Types                       |
//  |              Preference, WorkDays, Alias,      |
//  |              Aliases                           |
//  |              Timecard, Header, Line, Lines     |
//  |              Error, Errors                     |
//  |                                                |
//  | History:     Created 07/13/99 Akita Ling,      |
//  |              Jing Feng, Anitha Andra           |
//  |                                                |
//  +================================================+
/* $Header: pawtobjs.js 115.14 2001/04/23 18:49:10 pkm ship      $ */


//  ---------------------------------------------------------------------------
//  Employee Class
//  ---------------------------------------------------------------------------
// Constructor used to create an Employee object with the given values
function Employee(p_strName, p_iID, p_iNumber, p_dWeekEndingDate){

  this.strName = p_strName;
  this.iID     = p_iID;
  this.iNumber = p_iNumber;
  this.dWeekEndingDate = p_dWeekEndingDate;
}

Employee.prototype.mGetstrName  = mGetEmpstrName;
Employee.prototype.mGetiNumber  = mGetiNumber;
Employee.prototype.mGetiID      = mGetiID;
Employee.prototype.mGetdWeekEndingDate = mGetdWeekEndingDate;
Employee.prototype.mSetstrName  = mSetstrName;
Employee.prototype.mSetiNumber  = mSetiNumber;
Employee.prototype.mSetiID      = mSetiID;
Employee.prototype.mSetdWeekEndingDate = mSetdWeekEndingDate;

// Get the employee name associated with this Employee object
function mGetEmpstrName(){
  return(this.strName);
}

// Get the employee number associated with this Employee object
function mGetiNumber(){
  return(this.iNumber);
}

// Get the employee ID associated with this Employee object
function mGetiID(){
  return(this.iID);
}

// Get the week ending date associated with this Employee object
function mGetdWeekEndingDate(){
  return(this.dWeekEndingDate);
}

// Set the employee name of this Employee object with the given value.
function mSetstrName(p_strName){
  this.strName = p_strName;
}

// Set the employee number of this Employee object with the given value.
function mSetiNumber(p_iNumber){
  this.iNumber = p_iNumber;
}

// Set the employee ID of this Employee object with the given value.
function mSetiID(p_iID){
  this.iID = p_iID;
}

// Set the week ending date of this Employee object with the given value.
function mSetdWeekEndingDate(p_dWeekEndingDate){
  this.dWeekEndingDate = p_dWeekEndingDate;
}

//  ---------------------------------------------------------------------------
//  Employees Class
//  ---------------------------------------------------------------------------

// Constructor to create the Employees object
function Employees(){
  this.arrEmployee = new Array();
}

Employees.prototype.mAddEmployee   = mAddEmployee;
Employees.prototype.miGetNumEmployees = miGetNumEmployees;
Employees.prototype.mobjFindEmployee  = mobjFindEmployee;
 
// Add an Employee object to the employees array
function mAddEmployee(p_strName, p_iID, p_iNumber, p_dWeekEndingDate){ 
  this.arrEmployee[this.miGetNumEmployees()] = 
    new Employee(p_strName, p_iID, p_iNumber, p_dWeekEndingDate);
}

// Get the length of the employees array
function miGetNumEmployees(){
  return(this.arrEmployee.length);
}

// Find the employee in the employees array by a given employee ID
function mobjFindEmployee(p_iEmployeeID){
  for(var i=0; i<this.miGetNumEmployees(); i++){
    if (this.arrEmployee[i].mGetiID() == p_iEmployeeID){
      return(this.arrEmployee[i]);
    }
  }
  return null;
}

//  ---------------------------------------------------------------------------
//  Type Class
//  ---------------------------------------------------------------------------

// Constructor to create the Type object
function Type(p_strName, p_strSystemLinkage, p_strExpenditureType, p_dStartDate, 
              p_dEndDate){

  this.strName            = p_strName;
  this.strSystemLinkage   = p_strSystemLinkage;
  this.strExpenditureType = p_strExpenditureType;
  this.dStartDate         = p_dStartDate;
  this.dEndDate           = p_dEndDate
}

Type.prototype.mGetstrName            = mGetstrTypeName;
Type.prototype.mGetstrSystemLinkage   = mGetstrSystemLinkage;
Type.prototype.mGetstrExpenditureType = mGetstrExpenditureType;
Type.prototype.mGetdStartDate         = mGetdStartDate;
Type.prototype.mGetdEndDate           = mGetdEndDate;

// Get the type name associated with this Type object
function mGetstrTypeName(){
  return this.strName;
}

// Get the system linkage code associated with this Type object
function mGetstrSystemLinkage(){
  return this.strSystemLinkage;
}

// Get the expenditure type associated with this Type object
function mGetstrExpenditureType(){
  return this.strExpenditureType;
}

// Get the start date of the expenditure type active period associated with this Type object
function mGetdStartDate(){
  return this.dStartDate;
}

// Get the end date of the expenditure type active period associated with this Type object
function mGetdEndDate(){
  return this.dEndDate;
}

//  ---------------------------------------------------------------------------
//  Types Class
//  ---------------------------------------------------------------------------

// Constructor used to create the Types array
function Types(){
  this.arrType = new Array();
}

Types.prototype.mAddType   = mAddType;
Types.prototype.miGetNumTypes = miGetNumTypes;
Types.prototype.mobjFindType  = mobjFindType;
Types.prototype.miGetTypeIndex = miGetTypeIndex;
Types.prototype.mobjGetTypeByName = mobjGetTypeByName;

// Add a Type object to the Types array
function mAddType(p_strName, p_strSystemLinkage, p_strExpenditureType, 
                     p_dStartDate, p_dEndDate){ 
  this.arrType[this.arrType.length] = new Type(p_strName, p_strSystemLinkage, 
                                               p_strExpenditureType, p_dStartDate, 
                                               p_dEndDate);
}

// Get the length of the Types array
function miGetNumTypes(){ 
  return(this.arrType.length); 
}

// Find the type object with a given system linkage code and expenditure type
function mobjFindType(p_strSystemLinkage, p_strExpenditureType){
  for(i=0; i<this.miGetNumTypes(); i++){
    if ((this.arrType[i].mGetstrSystemLinkage() == p_strSystemLinkage) && 
        (this.arrType[i].mGetstrExpenditureType() == p_strExpenditureType)){
      return(this.arrType[i]);
    }
  }
  return null;
}

function miGetTypeIndex(p_strTypeName)
{
  for (i=0; i<this.miGetNumTypes(); i++)
  {
    if (this.arrType[i].mGetstrName() == p_strTypeName)
      return i;
  }
  return -1;
}

// Get the type object given a string name from the poplist
function mobjGetTypeByName(p_strName){
  for (var j=0; j<this.miGetNumTypes(); j++){
    if (this.arrType[j].mGetstrName() == p_strName) {
      return this.arrType[j];
    }
  }

  return null;
}

//  ---------------------------------------------------------------------------
//  Preference Class
//  ---------------------------------------------------------------------------

// Constructor used to create a preference object with the given values.
function Preference(p_strDefaultComment, p_objEmployee, p_objWorkDays,
                    p_objAliases)
{
  this.strDefaultComment = p_strDefaultComment;
  if ( p_objEmployee == null ) {
    this.objDefaultApprover = new employee(null, null, null, null, null);
  }
  else {
    this.objDefaultApprover = p_objEmployee;
  }
  if ( p_objWorkDays == null ) {
    this.objWorkDays = new WorkDays();
  }
  else {
    this.objWorkDays = p_objWorkDays;
  }
  if ( p_objAliases == null ) {
    p_objAliases = new Aliases();
  }
  else
  {
    this.objAliases = p_objAliases;
  }
}

Preference.prototype.mGetstrDefaultComment = mGetstrDefaultComment;
Preference.prototype.mGetobjDefaultApprover = mGetobjDefaultApprover;
Preference.prototype.mGetobjWorkDays = mGetobjWorkDays;
Preference.prototype.mGetobjAliases = mGetobjAliases;
Preference.prototype.mSetstrDefaultComment = mSetstrDefaultComment;
Preference.prototype.mSetobjDefaultApprover = mSetobjDefaultApprover;
Preference.prototype.mstrSerialize = mstrPrefSerialize;

// Get the default comment associated with this preference.
function mGetstrDefaultComment() {
  return (this.strDefaultComment);
}

// Get the default approver associated with this preference.
function mGetobjDefaultApprover() {
  return (this.objDefaultApprover);
}

// Get the work days objects associated with this preference.
function mGetobjWorkDays() {
  return (this.objWorkDays);
}

// Get the aliases group associated with this preference.
function mGetobjAliases() {
  return (this.objAliases);
}

// Set the default comment of this preference to the given value.
function mSetstrDefaultComment(p_strDefaultComment) {
  this.strDefaultComment = p_strDefaultComment;
}

// Set the default approver of this preference to the given value.
function mSetobjDefaultApprover(p_strName, p_iID)
{
  // old DefaultApprover should be automatically deleted (JS).
  this.objDefaultApprover.mSetstrName( p_strName );
  this.objDefaultApprover.mSetiID(p_iID);
}

// Create the message string that represents this preference.

function mstrPrefSerialize() {
  var prefString;
  prefString =  '@att@' + this.mGetstrDefaultComment() +
         '@att@' + this.mGetobjDefaultApprover().mGetstrName() +
         '@att@' + this.mGetobjDefaultApprover().mGetiID() + 
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iSUNDAY] + 
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iMONDAY] + 
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iTUESDAY] +
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iWEDNESDAY] +
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iTHURSDAY] +
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iFRIDAY] +
         '@att@' + this.mGetobjWorkDays().arrWorkDay[C_iSATURDAY] +
         '@att@';
  for ( i = 0; i<this.mGetobjAliases().miGetNumAliases(); i++)
  {
    if ( ! this.mGetobjAliases().mGetarrAlias()[i].mbIsEmpty() )
    {
      prefString +=  
        '@line@' + 
        '@att@' + eval(i+1) + 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetstrName() + 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetstrProjectNum()+ 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetiProjectID() + 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetstrTaskNum() +
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetiTaskID() + 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetobjType().mGetstrSystemLinkage() + 
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetobjType().mGetstrExpenditureType() +
        '@att@' + this.mGetobjAliases().mGetarrAlias()[i].mGetbIsDeleted() +
        '@att@';
    }
  }
  prefString += "@line@";
  
  return prefString;
}

//  ---------------------------------------------------------------------------
//  WorkDays Class
//  ---------------------------------------------------------------------------

// Constructor. Instantiate a work days object that contain 7 work days.
function WorkDays() {
  this.arrWorkDay = new Array();
  
  this.arrWorkDay[C_iSUNDAY] = false;
  this.arrWorkDay[C_iMONDAY] = false;
  this.arrWorkDay[C_iTUESDAY] = false;
  this.arrWorkDay[C_iWEDNESDAY] = false;
  this.arrWorkDay[C_iTHURSDAY] = false;
  this.arrWorkDay[C_iFRIDAY] = false;
  this.arrWorkDay[C_iSATURDAY] = false;
}

WorkDays.prototype.mSetWorkDay = mSetWorkDay;
WorkDays.prototype.mSetNonWorkDay = mSetNonWorkDay;
WorkDays.prototype.bIsWorkDay = bIsWorkDay;
WorkDays.prototype.mSetWorkDays = mSetWorkDays;

// Set the given day of the week to be a work day.
function mSetWorkDay(p_iDayOfWeek) {
  // Set this day as a work day
  this.arrWorkDay[p_iDayOfWeek] = true;
}

// Set the given day of the week to be a non-work day.
function mSetNonWorkDay(p_iDayOfWeek) {
  // Set this day as a non-work day
  this.arrWorkDay[p_iDayOfWeek] = false;
}

// Check if the given day of the week is a work day.
function bIsWorkDay(p_iDayOfWeek) {
  // True value indicates this day is a work day. Vice versa.
  return ( this.arrWorkDay[p_iDayOfWeek] );
}

// Set the WorkDays objects based on the given values for each day of the week.

function mSetWorkDays( p_bIsSunWorkDay, p_bIsMonWorkDay, p_bIsTueWorkDay,
                       p_bIsWedWorkDay, p_bIsThuWorkDay, p_bIsFriWorkDay, 
                       p_bIsSatWorkDay)
{
  this.arrWorkDay[C_iSUNDAY] = p_bIsSunWorkDay;
  this.arrWorkDay[C_iMONDAY] = p_bIsMonWorkDay;
  this.arrWorkDay[C_iTUESDAY] = p_bIsTueWorkDay;
  this.arrWorkDay[C_iWEDNESDAY] = p_bIsWedWorkDay;
  this.arrWorkDay[C_iTHURSDAY] = p_bIsThuWorkDay;
  this.arrWorkDay[C_iFRIDAY] = p_bIsFriWorkDay;
  this.arrWorkDay[C_iSATURDAY] = p_bIsSatWorkDay;
}

//  ---------------------------------------------------------------------------
//  Alias Class
//  ---------------------------------------------------------------------------

// Constructor used to create an alias object with the given values.
function Alias(p_strName, p_iProjectID, p_strProjectNum, p_iTaskID,
               p_strTaskNum, p_objType){

  this.strName       = p_strName;
  this.iProjectID    = p_iProjectID;
  this.strProjectNum = p_strProjectNum;
  this.iTaskID       = p_iTaskID;
  this.strTaskNum    = p_strTaskNum;
  this.objType       = p_objType;
  this.bIsDeleted    = false;
  this.iDisplayLineNum      = 0;

}

Alias.prototype.mGetstrName       = mGetstrName;
Alias.prototype.mGetiProjectID    = mGetiProjectID;
Alias.prototype.mGetstrProjectNum = mGetstrProjectNum;
Alias.prototype.mGetiTaskID       = mGetiTaskID;
Alias.prototype.mGetstrTaskNum    = mGetstrTaskNum;
Alias.prototype.mGetobjType       = mGetobjType;
Alias.prototype.mGetbIsDeleted    = mGetbIsDeleted;
Alias.prototype.mGetiDisplayLineNum      = mGetiDisplayLineNum;

Alias.prototype.mSetiDisplayLineNum      = mSetiDisplayLineNum;
Alias.prototype.mSetbIsDeleted    = mSetbIsDeleted;
Alias.prototype.mSetAlias      = mSetAlias;
Alias.prototype.mClearAlias    = mClearAlias;
Alias.prototype.mbIsEmpty         = mbIsEmpty;

// Get the alias name.
function mGetstrName(){
  return ( this.strName );
}

// Get the project ID associated with this alias.
function mGetiProjectID(){
  return ( this.iProjectID );
}

// Get the project name associated with this alias.
function mGetstrProjectNum(){
  return ( this.strProjectNum );
}

// Get the task ID associated with the project in this alias.
function mGetiTaskID(p_iIndex){
  return ( this.iTaskID );
}

// Get the task name  associated the project in this alias.
function mGetstrTaskNum(p_iIndex){
  return ( this.strTaskNum );
}

// Get the expenditure type associated with this alias.
function mGetobjType(p_iIndex){
  return ( this.objType );
}

// Get the flag indicating if this aliase is deleted from the UI.
function mGetbIsDeleted(){
  return ( this.bIsDeleted );
}

// Set the flag that indicates this alias is delete from the UI.
function mSetbIsDeleted(p_bIsDeleted){
  this.bIsDeleted = p_bIsDeleted;
}

// Get this alias's display line number 
function mGetiDisplayLineNum() {
  return ( this.iDisplayLineNum );
}

function mSetiDisplayLineNum(p_iDisplayLineNum) {
  this.iDisplayLineNum = p_iDisplayLineNum;
}

// Set the attributes of this alias based on given values.
function mSetAlias(p_strName, p_iProjectID, p_strProjectNum, p_iTaskID,
                      p_strTaskNum, p_objType) {
  this.strName = p_strName;
  this.iProjectID = p_iProjectID;
  this.strProjectNum = p_strProjectNum;
  this.iTaskID = p_iTaskID;
  this.strTaskNum = p_strTaskNum;
  this.objType = p_objType;
}

// Empty out all the attributes in this alias.
function mClearAlias() {
  this.strName = "";
  this.iProjectID = "";
  this.strProjectNum = "";
  this.iTaskID = "";
  this.strTaskNum = "";
  this.objType = new Type("", "", "", "", "");
  this.bIsDeleted = false;
}

// Check if this alias is empty object. If so return true else return false.
function mbIsEmpty() {
  return ( this.strName.length == 0 && this.strProjectNum.length == 0 &&
           this.strTaskNum.length == 0 );
}

//  ---------------------------------------------------------------------------
//  Aliases Class
//  ---------------------------------------------------------------------------

// Constructor used to create the aliases object.
// arbitrary number of aliases.
function Aliases(){
  this.arrAlias   = new Array();
}

Aliases.prototype.mGetarrAlias = mGetarrAlias;
Aliases.prototype.miGetNumAliases  = miGetNumAliases;
Aliases.prototype.miGetActualNumAliases  = miGetActualNumAliases;
Aliases.prototype.mAddAlias     = mAddAlias;
Aliases.prototype.mRemoveAlias = mRemoveAlias;
Aliases.prototype.mobjFindAlias = mobjFindAlias;
Aliases.prototype.mbIsExist = mbIsExist;

// Get the array that contains all the aliases in this alias group.
function mGetarrAlias() {
  return this.arrAlias;
}

// Get the total number of aliases in this group.
function miGetNumAliases(){
  return(this.arrAlias.length);
}

// Get the actual number of aliases in this array.
function miGetActualNumAliases(){

  var j=0;
  for(var i=0; i<this.miGetNumAliases(); i++){
    if ((this.arrAlias[i].mGetbIsDeleted() == false) && 
	(this.arrAlias[i].mGetstrName() != '')){
      j++;
    }
  }
  return j;

}

// Add a new alias to the alias array.
function mAddAlias(p_strName, p_iProjectID, p_strProjectNum, p_iTaskID,
                      p_strTaskNum, p_objType){
  this.arrAlias[this.miGetNumAliases()] = new Alias(p_strName, p_iProjectID,
                                             p_strProjectNum, p_iTaskID, 
                                             p_strTaskNum, p_objType);
}

// Remove the alias from the alias array whose name matches the given name.
function mRemoveAlias(p_strName) {
  var index = -1;
  for ( i=0; i<this.miGetNumAliases(); i++ ) {
    if ( this.arrAlias[i].mGetstrName() == p_strName ) { index = i; }
  }
  if ( index >= 0 ) {
    for ( var j = index; j < this.miGetNumAliases() - 1; j++)
    {
      this.arrAlias[j].mSetAlias(
           this.arrAlias[j+1].strName, this.arrAlias[j+1].iProjectID, 
           this.arrAlias[j+1].strProjectNum, this.arrAlias[j+1].iTaskID, 
           this.arrAlias[j+1].strTaskNum, this.arrAlias[j+1].objType);
    }
    this.arrAlias[this.miGetNumAliases() - 1].mClearAlias();
  }
  else // if
    return false;
  return true;
}

// Find the alias from this alias group by the given name.
function mobjFindAlias( p_strName ) {
  for ( var i=0; i<this.miGetNumAliases(); i++ ) {
    if ( this.arrAlias[i].mGetstrName() == p_strName )
      return this.arrAlias[i];
  }
  return null;
}

// Given an alias name, check to see if it exists in this alias group.
function mbIsExist(p_strName ) {
  return ( ( this.mobjFindAlias(p_strName) != null ) );
}

function fPadPrefAliases(p_objAliases) {

  var l_iNumOfAliases = p_objAliases.arrAlias.length;
  var l_iNewNumOfAliasesToAdd = 0;

   // If no alias defined, add minimum
  if (l_iNumOfAliases == 0)
    l_iNewNumOfAliasesToAdd = C_iMinAliasLines;

  // Set new number of lines so that it is a multiple
  l_iNewNumOfAliasesToAdd = l_iNewNumOfAliasesToAdd + 
    (C_iMinAliasLines - (l_iNumOfAliases % C_iMinAliasLines));

  if ((l_iNumOfAliases % C_iMinAliasLines) == 0)
    l_iNewNumOfAliasesToAdd = l_iNewNumOfAliasesToAdd - C_iMinAliasLines;

  // Add lines to p_objAliases
  for(var i=0;i<l_iNewNumOfAliasesToAdd;i++)
    p_objAliases.mAddAlias("", "", "", "", "", 
                           g_objTypes.mobjFindType("", ""));
}

//  ---------------------------------------------------------------------------
//  Timecard Class
//  ---------------------------------------------------------------------------

// Constructor used to create a Timecard object
function Timecard(){
  this.objHeader   = new Header();
  this.objLines    = new Lines();
  this.nTotalHours = null;
}

Timecard.prototype.mGetobjHeader = mGetobjHeader;
Timecard.prototype.mGetobjLines  = mGetobjLines;
Timecard.prototype.mGetnTotalHours  = mGetnTotalHours;
Timecard.prototype.mstrSerializeHeader  = mstrSerializeHeader;
Timecard.prototype.mstrSerializeLines  = mstrSerializeLines;
// The following 3 methods are new in R11i.
Timecard.prototype.mSetobjLines = mSetobjLines;
Timecard.prototype.mbIsReverseTimecard = mbIsReverseTimecard;

// Get the header object associated with this Timecard object
function mGetobjHeader(){
  return(this.objHeader);
}

// Get the line object associated with this Timecard object
function mGetobjLines(){
  return(this.objLines);
}

// Get the timecard number associated with this Header object
function mGetnTotalHours(){
  var nTotalHours = 0.00;
  
  for(var i=0; i<this.objLines.arrLine.length; i++) {
    if (this.mGetobjLines().arrLine[i].mGetbIsDeleted()==false) {
    this.mGetobjLines().arrLine[i].mSetnWeekTotal();
    nTotalHours += 
      this.mGetobjLines().arrLine[i].mGetnWeekTotal();
    }
  }
  return nTotalHours;
}

function mSetobjLines(p_objLines)
{
  this.objLines = p_objLines;
}

function mbIsReverseTimecard() {
  for ( var i=0; i<this.mGetobjLines().miGetNumLines(); i++ )
  {
    if ( this.mGetobjLines().arrLine[i].mbIsReverseLine() ) { return true; }
  }
  return false;
}

// Serialize the Header object
function mstrSerializeHeader() {
  var C_strFIELD_DELIMITER = '@att@';

  return C_strFIELD_DELIMITER + 
	this.mGetobjHeader().mGetiExpenditureID()		+ C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetstrTimecardNum()		+ C_strFIELD_DELIMITER +
	fDateToLongString(g_strSessDateFormat, 
			  this.mGetobjHeader().mGetdWeekEndingDate(), 
			  true)                 		+ C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetobjEmployee().mGetiID()        + C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetiPreparerID()                  + C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetobjApprover().mGetstrName()    + C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetobjApprover().mGetiID()        + C_strFIELD_DELIMITER +
	this.mGetobjHeader().mGetobjApprover().mGetiNumber()    + C_strFIELD_DELIMITER +
        this.mGetobjHeader().mGetstrComment()			+ C_strFIELD_DELIMITER +
        this.mGetobjHeader().mGetstrBusinessMesg()		+ C_strFIELD_DELIMITER +
        this.mGetobjHeader().mGetstrWFStatusCode()		+ C_strFIELD_DELIMITER +
	this.mGetnTotalHours()					+ C_strFIELD_DELIMITER;
}

function mstrSerializeLines() {

  var C_strFIELD_DELIMITER = '@att@';
  var C_strLINE_DELIMITER  = '@line@';
  var C_iWEEK_DAYS         = 7;

  var l_strSerializeString;	
  var l_nHours;
  var l_strComment;
  var l_sDayDelimiter;
  var l_bSerialize = false;

  var l_arrTemp = new Array();

  l_strSerializeString = '';
  for (i=0; i < this.mGetobjLines().miGetNumLines(); i++) {
    var l_objLine = this.mGetobjLines().arrLine[i];
    if ((l_objLine.iDenormID == '') &&
        ((l_objLine.bIsEmpty == true) ||
         (l_objLine.bIsDeleted == true) ) ) 
      l_bSerialize = false;
    else 
      l_bSerialize = true;
    
    if (l_bSerialize) {

      l_strSerializeString += C_strFIELD_DELIMITER +
        l_objLine.mGetnDenormID() 
		+ C_strFIELD_DELIMITER +
  	l_objLine.mGetbIsDeleted()
		+ C_strFIELD_DELIMITER +
// Commented out to fix bug 1746601
//    	l_objLine.mGetiProjectID() + 
		C_strFIELD_DELIMITER +
		C_strFIELD_DELIMITER +
     	l_objLine.mGetstrProjectNum()
		+ C_strFIELD_DELIMITER +
// Commented out to fix bug 1746601
//     	l_objLine.mGetiTaskID() +
		C_strFIELD_DELIMITER +
		C_strFIELD_DELIMITER +
     	l_objLine.mGetstrTaskNum()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetobjType().mGetstrExpenditureType()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetobjType().mGetstrSystemLinkage()
		+ C_strFIELD_DELIMITER +
        l_objLine.mGetnOrigDenormID() 
                + C_strFIELD_DELIMITER;

    for (j=0; j<C_iWEEK_DAYS; j++) {
      var l_objDay = this.mGetobjLines().arrLine[i].mGetobjDays().mobjGetDay(j);
      l_nHours     = l_objDay.mGetnHours();
      l_strComment = l_objDay.mGetstrComment();

      if (j==0) 
        l_sDayDelimiter = '@7@';
      else
        l_sDayDelimiter = '@' + j + '@';

      if ((l_nHours != null) || (l_strComment != null)) {
        l_strSerializeString += l_sDayDelimiter + 
				l_nHours     + C_strFIELD_DELIMITER +
				l_strComment + C_strFIELD_DELIMITER ;
    
        // Include DFlex
        // Moved serialize logic to pawdflex.js (Bug#1082684)
        if (fbIsDFlexUsed()) {
          l_strSerializeString += 
            fSerializeDFlex(fGetContSensField(l_objLine), l_objDay.arrDFlex);
          }
        }

    }
    l_strSerializeString += C_strLINE_DELIMITER;
   } // If line not empty
  }
  return l_strSerializeString;
}

//  ---------------------------------------------------------------------------
//  Header Class
//  ---------------------------------------------------------------------------

// Constructor used to create a Header object
function Header() {

  this.iExpenditureID  = null;
  this.strTimecardNum  = null;
  this.strStatus       = null;
  this.objEmployee     = null;
  this.dWeekEndingDate = null;
  this.strComment      = null;
  this.objApprover     = null;
  this.iPreparerID     = null;
  this.strBusinessMesg = null;
  this.strWFStatusCode = null;
  this.objLastUpdatedBy= null;
  this.dLastUpdateDate = null;
}

Header.prototype.mGetiExpenditureID  = mGetiExpenditureID;
Header.prototype.mGetstrTimecardNum  = mGetstrTimecardNum;
Header.prototype.mGetstrStatus       = mGetstrStatus;
Header.prototype.mGetobjEmployee     = mGetobjEmployee;
Header.prototype.mGetdWeekEndingDate = mGetdWeekEndingDate;
Header.prototype.mGetstrComment      = mGetstrComment;
Header.prototype.mGetobjApprover     = mGetobjApprover;
Header.prototype.mGetiPreparerID     = mGetiPreparerID;
Header.prototype.mGetstrBusinessMesg = mGetstrBusinessMesg;
Header.prototype.mGetstrWFStatusCode = mGetstrWFStatusCode;
Header.prototype.mGetobjLastUpdatedBy= mGetobjLastUpdatedBy;
Header.prototype.mGetdLastUpdateDate = mGetdLastUpdateDate;

Header.prototype.mSetiExpenditureID  = mSetiExpenditureID;
Header.prototype.mSetstrTimecardNum  = mSetstrTimecardNum;
Header.prototype.mSetstrStatus       = mSetstrStatus;
Header.prototype.mSetobjEmployee     = mSetobjEmployee;
Header.prototype.mSetdWeekEndingDate = mSetdWeekEndingDate;
Header.prototype.mSetstrComment      = mSetstrHeaderComment;
Header.prototype.mSetobjApprover     = mSetobjApprover;
Header.prototype.mSetiPreparerID     = mSetiPreparerID;
Header.prototype.mSetstrBusinessMesg = mSetstrBusinessMesg;
Header.prototype.mSetstrWFStatusCode = mSetstrWFStatusCode;
Header.prototype.mSetobjLastUpdatedBy= mSetobjLastUpdatedBy;
Header.prototype.mSetdLastUpdateDate = mSetdLastUpdateDate;
Header.prototype.mSetHeader          = mSetHeader;

// Get the expenditure ID associated with this Header object
function mGetiExpenditureID(){
  return(this.iExpenditureID);
}

// Get the timecard number associated with this Header object
function mGetstrTimecardNum(){
  return(this.strTimecardNum);
}

// Get the Expenditure Status associated with this Header object
function mGetstrStatus(){
  return(this.strStatus);
}

// Get the employee object of the employee 
function mGetobjEmployee(){
  return(this.objEmployee);
}

// Get the week ending date associated with this Header object
function mGetdWeekEndingDate(){
  return(this.dWeekEndingDate);
}

// Get the header comment associated with this Header object
function mGetstrComment(){
  return(this.strComment);
}

// Get the employee object of the approver 
function mGetobjApprover(){
  return(this.objApprover);
}

// Get the preparer ID associated with this Header object
function mGetiPreparerID(){
  return(this.iPreparerID)
}

// Get the Business Message associated with this Header object
function mGetstrBusinessMesg(){
  return(this.strBusinessMesg);
}

// Get the WF Status Code associated with this Header object
function mGetstrWFStatusCode(){
  return(this.strWFStatusCode);
}

// Get the employee object of the last person who updated the timecard
function mGetobjLastUpdatedBy() {
  return (this.objLastUpdatedBy);
}

// Get the date when the timecard was last updated
function mGetdLastUpdateDate() {
  return (this.dLastUpdateDate);
}

// Set the expenditure ID associated this Header object
function mSetiExpenditureID(p_iExpenditureID){
  this.iExpenditureID = p_iExpenditureID;
}

// Set the timecard number associated this Header object
function mSetstrTimecardNum(p_strTimecardNum){
  this.strTimecardNum = p_strTimecardNum;
}

// Set the expenditure Status associated this Header object
function mSetstrStatus(p_strStatus){
  this.strStatus = p_strStatus;
}

// Set the employee object of this Header
function mSetobjEmployee(p_objEmployee){
  this.objEmployee = p_objEmployee;
}

// Set the week ending date associated with this Header object
function mSetdWeekEndingDate(p_dWeekEndingDate){
  this.dWeekEndingDate = p_dWeekEndingDate;
}

// Set the comment associated with this Header object
function mSetstrHeaderComment(p_strComment){
  this.strComment = p_strComment;
}

// Set the Approver as an Employee object
function mSetobjApprover(p_objApprover){
  this.objApprover = p_objApprover;
}

// Set the preparer ID associated with this Header object
function mSetiPreparerID(p_iPreparerID){
  this.iPreparerID = p_iPreparerID;
}

// Set the Business Message associated this Header object
function mSetstrBusinessMesg(p_strBusinessMesg){
  this.strBusinessMesg = p_strBusinessMesg;
}

// Set the WF Status Code associated this Header object
function mSetstrWFStatusCode(p_strWFStatusCode){
  this.strWFStatusCode = p_strWFStatusCode;
}

// Set the employee who last updated the timecard
function mSetobjLastUpdatedBy(p_objLastUpdatedBy) {
  this.objLastUpdatedBy = p_objLastUpdatedBy;
}

// Set the date when the timecard was last updated
function mSetdLastUpdateDate(p_dLastUpdateDate) {
  this.dLastUpdateDate = p_dLastUpdateDate;
}

// Set the header object
function mSetHeader(p_iExpenditureID, p_strTimecardNum,p_strStatus, 
                p_objEmployee, p_dWeekEndingDate, p_strComment, 
		p_objApprover, p_iPreparerID, p_strBusinessMesg,
		p_strWFStatusCode, p_objLastUpdatedBy, p_dLastUpdateDate){

  this.mSetiExpenditureID(p_iExpenditureID);
  this.mSetstrTimecardNum(p_strTimecardNum);
  this.mSetstrStatus(p_strStatus);
  this.mSetobjEmployee(p_objEmployee);
  this.mSetdWeekEndingDate(p_dWeekEndingDate);
  this.mSetstrComment(p_strComment);
  this.mSetobjApprover(p_objApprover);
  this.iPreparerID = p_iPreparerID;
  this.mSetstrBusinessMesg(p_strBusinessMesg);
  this.mSetstrWFStatusCode(p_strWFStatusCode);
  this.mSetobjLastUpdatedBy(p_objLastUpdatedBy);
  this.mSetdLastUpdateDate(p_dLastUpdateDate);
}

//  ---------------------------------------------------------------------------
//  Line Class
//  ---------------------------------------------------------------------------

// Constructor used to create a line object
function Line(p_iDenormID, p_iDisplayNum, p_iProjectID, p_strProjectNum, 
              p_strProjectName, p_iTaskID, p_strTaskNum, p_strTaskName,
              p_objType, p_objDays, p_nOrigDenormID){
  this.iDenormID       = p_iDenormID;
  this.iDisplayLineNum = p_iDisplayNum;
  this.iProjectID      = p_iProjectID;
  this.strProjectNum   = p_strProjectNum;
  this.strProjectName  = p_strProjectName;
  this.iTaskID         = p_iTaskID;
  this.strTaskNum      = p_strTaskNum;
  this.strTaskName     = p_strTaskName;
  this.objType         = p_objType; 
  this.objDays         = p_objDays;
  this.nWeekTotal      = 0;
  this.bIsDeleted      = false;
  this.bIsEmpty        = true;
  this.nOrigDenormID   = p_nOrigDenormID;  // New in R11i
}

Line.prototype.mGetnDenormID       = mGetnDenormID;
Line.prototype.mGetiDisplayLineNum = mGetiDisplayLineNum;
Line.prototype.mGetiProjectID      = mGetiProjectID;
Line.prototype.mGetstrProjectNum   = mGetstrProjectNum;
Line.prototype.mGetstrProjectName  = mGetstrProjectName;
Line.prototype.mGetiTaskID         = mGetiTaskID;
Line.prototype.mGetstrTaskNum      = mGetstrTaskNum;
Line.prototype.mGetstrTaskName     = mGetstrTaskName;
Line.prototype.mGetbIsDeleted      = mGetbIsDeleted;
Line.prototype.mGetobjType         = mGetobjType;
Line.prototype.mGetobjDays         = mGetobjDays;
Line.prototype.mGetnWeekTotal      = mGetnWeekTotal;
Line.prototype.mGetbIsEmpty        = mGetbIsEmpty;
Line.prototype.mGetnOrigDenormID   = mGetnOrigDenormID;  // New in R11i

Line.prototype.mSetnDenormID       = mSetnDenormID; // New for Audit
Line.prototype.mSetiDisplayLineNum = mSetiLineDisplayLineNum;
Line.prototype.mSetobjType         = mSetobjType;
Line.prototype.mSetnWeekTotal      = mSetnWeekTotal;
Line.prototype.mSetobjLine         = mSetobjLine;
Line.prototype.mSetProject         = mSetProject;
Line.prototype.mSetTask            = mSetTask;
Line.prototype.mSetbIsDeleted      = mSetbIsDeleted;
Line.prototype.mSetbIsEmpty        = mSetbIsEmpty;
Line.prototype.mobjClearLine       = mobjClearLine;
Line.prototype.mSetnOrigDenormID   = mSetnOrigDenormID; // New in R11i
Line.prototype.mbIsReverseLine     = mbIsReverseLine;  // New in R11i

// Get the Denorm ID associated with this Line object
function mGetnDenormID(){
  return this.iDenormID;
}

// Get the display line number associated with this Line object
function mGetiDisplayLineNum(){
  return this.iDisplayLineNum;
}

// Get the project ID associated with this Line object
function mGetiProjectID(){
  return this.iProjectID;
}

// Get the project number associated with this Line object
function mGetstrProjectNum(){
  return this.strProjectNum;
}

// Get the project name associated with this Line object
function mGetstrProjectName(){
  return this.strProjectName;
}

// Get the task ID associated with this Line object
function mGetiTaskID(){
  return this.iTaskID;
}

// Get the task number associated with this Line object
function mGetstrTaskNum(){
  return this.strTaskNum;
}

// Get the task name associated with this Line object
function mGetstrTaskName(){
  return this.strTaskName;
}

// Get the bIsDeleted flag associated with this Line object
function mGetbIsDeleted(){
  return this.bIsDeleted;
}

// Get the type object associated with this Line object
function mGetobjType(){
  return this.objType;
}

// Get the day object associated with this Line object
function mGetobjDays(){
  return this.objDays;
}

// Get the week total associated with this Line object
function mGetnWeekTotal(){
  return this.nWeekTotal;
}

// Get the bIsEmpty flag value associated with this Line object
function mGetbIsEmpty(){
  return this.bIsEmpty;
}

// New in R11i
function mGetnOrigDenormID() {
  return this.nOrigDenormID;
}

// Set the Denorm ID of this Line object with the given value (for audit)
function mSetnDenormID(p_iDenormID){
  this.iDenormID = p_iDenormID;
}

// Set the display line number of this Line object with the given value
function mSetiLineDisplayLineNum(p_iDisplayLineNum){
  this.iDisplayLineNum = p_iDisplayLineNum;
}

// Set the type object of this Line object with the given value
function mSetobjType(p_objType){
  this.objType = p_objType;
}

// Set the week total of this Line object with the given value
function mSetnWeekTotal(){
var l_nWeekTotal=0;

  for(var i=0; i<=C_iSATURDAY; i++) {
    l_nWeekTotal += eval('this.objDays.arrDay[' + i + '].nHours') - 0;
  }
  this.nWeekTotal = l_nWeekTotal;
}

// Set a Line object with the given value
function mSetobjLine(p_iDenormID, p_iDisplayLineNum, p_iProjectID, 
                     p_strProjectNum, p_strProjectName, p_iTaskID, 
                     p_strTaskNum, p_strTaskName, p_strTypeName, 
                     p_strSystemLinkage, p_strExpenditureType, 
                     p_dStartDate, p_dEndDate, p_nHours0, p_strComment0, 
                     p_nHours1, p_strComment1, p_nHours2, p_strComment2, 
                     p_nHours3, p_strComment3, p_nHours4, p_strComment4, 
                     p_nHours5, p_strComment5, p_nHours6, p_strComment6, 
                     p_nWeekTotal, p_bIsDeleted, p_bIsEmpty, p_nOrigDenormID){

  this.iDenormID                  = p_iDenormID;
  this.iDisplayLineNum            = p_iDisplayLineNum;
  this.mSetProject(p_iProjectID, p_strProjectNum, p_strProjectName);
  this.mSetTask(p_iTaskID, p_strTaskNum, p_strTaskName);
  this.mSetobjType(new Type(p_strTypeName, p_strSystemLinkage, p_strExpenditureType, p_dStartDate, p_dEndDate));
  this.objDays.nHours0            = p_nHours0;
  this.objDays.strComment0        = p_strComment0;
  this.objDays.nHours1            = p_nHours1;
  this.objDays.strComment1        = p_strComment1;
  this.objDays.nHours2            = p_nHours2;
  this.objDays.strComment2        = p_strComment2;
  this.objDays.nHours3            = p_nHours3;
  this.objDays.strComment3        = p_strComment3;
  this.objDays.nHours4            = p_nHours4;
  this.objDays.strComment4        = p_strComment4;
  this.objDays.nHours5            = p_nHours5;
  this.objDays.strComment5        = p_strComment5;
  this.objDays.nHours6            = p_nHours6;
  this.objDays.strComment6        = p_strComment6;
  this.nWeekTotal                 = p_nWeekTotal;
  this.bIsDeleted                 = p_bIsDeleted;
  this.bIsEmpty                   = p_bIsEmpty;
  this.nOrigDenormID              = p_nOrigDenormID;
}

// Set the project ID, project number and project name of this Line object with the given values
function mSetProject(p_iProjectID, p_strProjectNum, p_strProjectName){
  this.iProjectID     = p_iProjectID;
  this.strProjectNum  = p_strProjectNum;
  this.strProjectName = p_strProjectName;
}

// Set the task ID, task numbe, and task name of this Line object with the given values
function mSetTask(p_iTaskID, p_strTaskNum, p_strTaskName){
  this.iTaskID     = p_iTaskID;
  this.strTaskNum  = p_strTaskNum;
  this.strTaskName = p_strTaskName;
}

// Set the bIsDeleted flag of this Line object with the given value
function mSetbIsDeleted(p_bIsDeleted){
  this.bIsDeleted = p_bIsDeleted;
}

// Set the bIsEmpty flag of this Line object with the given value
function mSetbIsEmpty(p_bIsEmpty){
  this.bIsEmpty = p_bIsEmpty;
}

// Clear the values of one line
function mobjClearLine(){
  this.mSetProject('', '', '');
  this.mSetTask('', '', '');
  this.mSetobjType(new Type('','','','',''));
  this.mSetnWeekTotal('');
  this.mSetiDisplayLineNum('');
  // Null out the day entry
  for(j=0; j<7; j++){
    this.arrDay[j].mSetnHours('');
    this.arrDay[j].mSetstrComment('');
  }
  this.mSetnOrigDenormID('');
}

// New in R11i
function mSetnOrigDenormID(p_nOrigDenormID) {
  this.nOrigDenormID = p_nOrigDenormID;
}

// Check to see if this line is a reverse line. 
// New in R11i
function mbIsReverseLine() {
var undefined;
  return ((this.nOrigDenormID != undefined) && (this.nOrigDenormID != '')) ;
}
//  ---------------------------------------------------------------------------
//  Lines Class
//  ---------------------------------------------------------------------------

// Constructor used to create a lines array
function Lines(){
  this.arrLine = new Array();
}

Lines.prototype.mobjGetarrLine         = mobjGetarrLine;
Lines.prototype.mGetarrLine            = mGetarrLine;
Lines.prototype.miGetNumLines          = miGetNumLines;
Lines.prototype.miGetNumDisplayedLines = miGetNumDisplayedLines;
Lines.prototype.miGetMaxDisplayNum     = miGetMaxDisplayNum;
Lines.prototype.miGetEmptyLineNum      = miGetEmptyLineNum;
Lines.prototype.miGetNumEmptyLines     = miGetNumEmptyLines;
Lines.prototype.miGetNumNotEmptyLines  = miGetNumNotEmptyLines ;
Lines.prototype.miGetNumEnteredLines   = miGetNumEnteredLines ;
Lines.prototype.miGetNumReversedLines   = miGetNumReversedLines ;
Lines.prototype.miGetNumValidReversedLines   = miGetNumValidReversedLines ;
Lines.prototype.mobjDeleteLine         = mobjDeleteLine;
Lines.prototype.mAddLine               = mAddLine;
Lines.prototype.mobjCopyLine           = mobjCopyLine;
Lines.prototype.miFindLine             = miFindLine;
Lines.prototype.mobjFindLine           = mobjFindLine;
Lines.prototype.mobjFindLineByOrigDenormID = mobjFindLineByOrigDenormID; // New in R11i
Lines.prototype.mobjInsertLine         = mobjInsertLine;
Lines.prototype.mResetDisplayNum    = mResetDisplayNum;
Lines.prototype.miFindFirstNonReversalLine    = miFindFirstNonReversalLine;

// Get the display line number for the first non reversal line
// Reversal lines are before the non reversal lines
function miFindFirstNonReversalLine() {
  for (i=0; i<this.miGetNumLines(); i++) {
    if (!this.arrLine[i].mbIsReverseLine()) {
      return this.arrLine[i].mGetiDisplayLineNum();
    }
  }
}


// Get the line object with a given display line number
function mobjGetarrLine(p_iDisplayLineNum){
  for(var i=0; i<this.miGetNumLines(); i++) {
    if (this.arrLine[i].mGetiDisplayLineNum() == p_iDisplayLineNum){
      return(this.arrLine[i]);
    }
  }
  return 0;
}

// JFENG. Added for R11i
function mGetarrLine() {
  return this.arrLine;
}

// Get the length of the array
function miGetNumLines(){
  return(this.arrLine.length);
}

// Get the number of displayed lines
function miGetNumDisplayedLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if (this.arrLine[i].iDisplayLineNum != ''){
      j++;
    }
  }
  return j;
}

// Get the max number of displayed lines
function miGetMaxDisplayNum(){
  for(var i=this.miGetNumLines()-1; i>0; i--){
   if ((this.arrLine[i].bIsEmpty == false) &&
       (this.arrLine[i].bIsDeleted!=true)){
      break;
    }
  }
  return this.arrLine[i].iDisplayLineNum;
}

// Get the first available empty line number
function miGetEmptyLineNum(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if (this.arrLine[i].bIsEmpty == true){
      return i;
    }
  }
  return this.miGetNumLines;
}

// Get the number of empty lines
function miGetNumEmptyLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if (this.arrLine[i].bIsEmpty != false){
      j++;
    }
  }
  return j;
}

// Get the number of not empty lines
function miGetNumNotEmptyLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if (this.arrLine[i].bIsEmpty == false){
      j++;
    }
  }
  return j;
}

// Get the number of reversed lines  (Valid and Invalid)
function miGetNumReversedLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if (this.arrLine[i].bIsReversed == true) {
      j++;
    }
  }
  return j;
}

// Get the number of reversed lines (valid ones only)
function miGetNumValidReversedLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if ((this.arrLine[i].mbIsReverseLine() == true) &&
 	(this.arrLine[i].bIsDeleted == false) &&
	(this.arrLine[i].bIsEmpty == false) ){
      j++;
    }
  }
  return j;
}

// Get the number of not empty lines
function miGetNumEnteredLines(){
  var j=0;
  for(var i=0; i<this.miGetNumLines(); i++){
    if ((this.arrLine[i].bIsEmpty == false) && (this.arrLine[i].bIsDeleted == false)){
      j++;
    }
  }
  return j;
}

// Add one line
function mAddLine(p_iDenormID, p_iDisplayNum,
                  p_iProjectID, p_strProjectNum, p_strProjectName, 
                  p_iTaskID, p_strTaskNum, p_strTaskName, p_objType, 
                  p_objDays, p_nOrigDenormID){  

  var l_iIndex = this.arrLine.length;

  this.arrLine[l_iIndex] = 
        new Line(p_iDenormID, p_iDisplayNum, p_iProjectID, 
                 p_strProjectNum, p_strProjectName, p_iTaskID, 
                 p_strTaskNum, p_strTaskName, p_objType, p_objDays, p_nOrigDenormID);

  if((p_iDenormID != '') || (p_iProjectID != '') 
	|| (p_strProjectNum != '') || (p_strProjectName != '') 
	|| (p_iTaskID != '' )      || (p_strTaskNum != '') 
	|| (p_strTaskName != '')   || (!p_objDays.mGetbIsEmptyWeek())) {
    this.arrLine[l_iIndex].mSetbIsEmpty(false);
  }

  return this.arrLine[l_iIndex];
}


// Mark the bIsDelete flag to true and clear the display line number
function mobjDeleteLine(p_iDisplayLineNum){

  for(var i=0; i<this.miGetNumLines(); i++) {
    if (this.arrLine[i].mGetiDisplayLineNum() == p_iDisplayLineNum){
      this.arrLine[i].mSetbIsDeleted(true);
      this.arrLine[i].mSetbIsEmpty(false);
      this.arrLine[i].mSetiDisplayLineNum('');
      break;
    }
  }

  for(var j=i+1; j<this.miGetNumLines(); j++){
    if(this.arrLine[j].mGetbIsDeleted() != true){
      this.arrLine[j].mSetiDisplayLineNum(p_iDisplayLineNum);
      p_iDisplayLineNum++;
    }
  }

  //add one more line to the data structure to make up the total
  this.mAddLine('',p_iDisplayLineNum, '','','','','','',new Type('','','','',''),new Days());

}

// Copy one line to the end of the array 
function mobjCopyLine(p_iToIndex, p_iFromIndex){

  this.arrLine[p_iToIndex].iDenormID                  = '';
  this.arrLine[p_iToIndex].iProjectID                 = this.arrLine[p_iFromIndex].mGetiProjectID();
  this.arrLine[p_iToIndex].strProjectNum              = this.arrLine[p_iFromIndex].mGetstrProjectNum();
  this.arrLine[p_iToIndex].strProjectName             = this.arrLine[p_iFromIndex].mGetstrProjectName();
  this.arrLine[p_iToIndex].iTaskID                    = this.arrLine[p_iFromIndex].mGetiTaskID();
  this.arrLine[p_iToIndex].strTaskNum                 = this.arrLine[p_iFromIndex].mGetstrTaskNum();
  this.arrLine[p_iToIndex].strTaskName                = this.arrLine[p_iFromIndex].mGetstrTaskName();
  
  this.arrLine[p_iToIndex].nOrigDenormID              = this.arrLine[p_iFromIndex].nOrigDenormID;
  
  this.arrLine[p_iToIndex].mSetobjType(new Type(this.arrLine[p_iFromIndex].objType.mGetstrName(),this.arrLine[p_iFromIndex].objType.mGetstrSystemLinkage(), this.arrLine[p_iFromIndex].objType.mGetstrExpenditureType(), this.arrLine[p_iFromIndex].objType.mGetdStartDate(), this.arrLine[p_iFromIndex].objType.mGetdEndDate())); 

  for(var i=0; i<C_iWEEKDAYS; i++)
    this.arrLine[p_iToIndex].objDays.arrDay[i].mCopy(
      this.arrLine[p_iFromIndex].objDays.arrDay[i]);

  this.arrLine[p_iToIndex].nWeekTotal	= this.arrLine[p_iFromIndex].mGetnWeekTotal();
  this.arrLine[p_iToIndex].bIsDeleted   = false;
  this.arrLine[p_iToIndex].bIsEmpty     = false;
  
}

// Find the array index with a given display line number. If no match can be found, return -1;
function miFindLine(p_iDisplayLineNum){
  for(var i=0; i<this.miGetNumLines(); i++) {
    if (this.arrLine[i].mGetiDisplayLineNum() == p_iDisplayLineNum){
      return i;
    }
  }
  return -1;
}

// Find the array index with a given display line number. If no match can be found, return -1;
function mobjFindLine(p_iDisplayLineNum){
  var l_iIndex = this.miFindLine(p_iDisplayLineNum);

  if (l_iIndex == -1)
    return "";
  else
    return this.arrLine[l_iIndex];  
}

// JFENG. Added for R11i.
function mobjFindLineByOrigDenormID(p_nOrigDenormID) {
  for ( var i=0; i<this.miGetNumLines(); i++)
  {
    if ( this.arrLine[i].mGetnOrigDenormID() == p_nOrigDenormID )
      return this.arrLine[i];
  }
  return null;
}

// JFENG Added for R11i
// Insert a line object p_objLine into Lines array at position p_iPosition
function mobjInsertLine(p_objLine, p_iPosition)
{
  for (var j = this.miGetNumLines(); j>p_iPosition; j-- ) {
    this.arrLine[j] = this.arrLine[j-1];
  }
  this.arrLine[p_iPosition] = p_objLine;
}

// JFENG Added for R11i
function mSetLineiDisplayLineNum(p_iLineIndex, p_iDisplayNum)
{
  this.arrLine[p_iLineIndex].mSetiDisplayLineNum(p_iDisplayNum);
}

function mResetDisplayNum() {
var l_iDisplayLineNum = 1;
  for ( var i=0; i<this.miGetNumLines(); i++)
  {
    if (this.arrLine[i].mGetbIsDeleted() != true) {
      this.arrLine[i].iDisplayLineNum = l_iDisplayLineNum;
      l_iDisplayLineNum++;      
    }
    else {
      this.arrLine[i].iDisplayLineNum = '';
    }
  }
}

//----------------------------------------------------------------------------
// SingleEntryLine Class
// New in R11i Added by JFENG.
//----------------------------------------------------------------------------

// Constructor used to create a SingleEntryLine object
function SingleEntryLine(
	      p_iDenormID, p_iDisplayLineNum, 
              p_iProjectID, p_strProjectNum, p_strProjectName, 
	      p_iTaskID, p_strTaskNum, p_strTaskName,
              p_objType, p_objDay, p_nOrigDenormID, p_nDayOfWeek){
  this.iDenormID       = p_iDenormID;
  this.iDisplayLineNum = p_iDisplayLineNum;
  this.iProjectID      = p_iProjectID;
  this.strProjectNum   = p_strProjectNum;
  this.strProjectName  = p_strProjectName;
  this.iTaskID         = p_iTaskID;
  this.strTaskNum      = p_strTaskNum;
  this.strTaskName     = p_strTaskName;
  this.objType         = p_objType; 
  this.objDay          = p_objDay;
  this.nWeekTotal      = 0;
  this.nOrigDenormID   = p_nOrigDenormID;
  this.nDayOfWeek      = p_nDayOfWeek;
  this.bIsReversed     = false;
  this.bIsNewlyReversed = false;
  this.bIsObsolete     = false;
}

SingleEntryLine.prototype.mGetnDenormID        = mGetnDenormID;
SingleEntryLine.prototype.mGetiDisplayLineNum  = mGetiDisplayLineNum;
SingleEntryLine.prototype.mGetiProjectID       = mGetiProjectID;
SingleEntryLine.prototype.mGetstrProjectNum    = mGetstrProjectNum;
SingleEntryLine.prototype.mGetstrProjectName   = mGetstrProjectName;
SingleEntryLine.prototype.mGetiTaskID          = mGetiTaskID;
SingleEntryLine.prototype.mGetstrTaskNum       = mGetstrTaskNum;
SingleEntryLine.prototype.mGetstrTaskName      = mGetstrTaskName;
SingleEntryLine.prototype.mGetobjType          = mGetobjType;
SingleEntryLine.prototype.mGetobjDay           = mGetobjDay;
SingleEntryLine.prototype.mGetnOrigDenormID    = mGetnOrigDenormID;
SingleEntryLine.prototype.mGetbIsReversed      = mGetbIsReversed;
SingleEntryLine.prototype.mGetbIsNewlyReversed = mGetbIsNewlyReversed;
SingleEntryLine.prototype.mGetbIsObsolete      = mGetbIsObsolete;
SingleEntryLine.prototype.mGetnDayOfWeek       = mGetnDayOfWeek;
SingleEntryLine.prototype.mSetbIsReversed      = mSetbIsReversed;
SingleEntryLine.prototype.mSetobjState         = mSetobjState;
SingleEntryLine.prototype.mobjConvertToLine   = mobjConvertToLine;

// Get the Denorm ID associated with this Line object
function mGetnDenormID(){
  return this.iDenormID;
}

function mGetiDisplayLineNum() {
  return this.iDisplayLineNum;
}

// Get the project ID associated with this Line object
function mGetiProjectID(){
  return this.iProjectID;
}

// Get the project number associated with this Line object
function mGetstrProjectNum(){
  return this.strProjectNum;
}

// Get the project name associated with this Line object
function mGetstrProjectName(){
  return this.strProjectName;
}

// Get the task ID associated with this Line object
function mGetiTaskID(){
  return this.iTaskID;
}

// Get the task number associated with this Line object
function mGetstrTaskNum(){
  return this.strTaskNum;
}

// Get the task name associated with this Line object
function mGetstrTaskName(){
  return this.strTaskName;
}

// Get the type object associated with this Line object
function mGetobjType(){
  return this.objType;
}

// Get the day object associated with this Line object
function mGetobjDay(){
  return this.objDay;
}

function mGetnOrigDenormID() {
  return this.nOrigDenormID;
}

function mGetnDayOfWeek() {
  return this.nDayOfWeek;
}

function mGetbIsReversed() {
  return this.bIsReversed;
}

function mGetbIsNewlyReversed() {
  return this.bIsNewlyReversed;
}

function mGetbIsObsolete() {
  return this.bIsObsolete;
}

function mSetbIsReversed(p_bIsReversed) {
  this.bIsReversed = p_bIsReversed;
}

function mSetobjState(p_strState) {
  switch(p_strState) {

    case C_strNEW_CHECK_YES:
      this.bIsReversed = true;
      this.bIsNewlyReversed = true;
      this.bIsObsolete = false;
      break;

    case C_strNEW_CHECK_NO:
      this.bIsNewlyReversed = false;
      break;

    case C_strNEW_UNCHECK_YES:
      this.bIsReversed = false;
      this.bIsNewlyReversed = false;
      this.bIsObsolete = true;
      break;

    case C_strNEW_UNCHECK_NO:
      this.bIsObsolete = false;
      break;
  }

}

function mobjConvertToLine() {
  var objDays = new Days();
  var l_nRevHours;

  var objLine = new Line( this.mGetnDenormID(),
                          null, 
                          this.mGetiProjectID(),
                          this.mGetstrProjectNum(), 
                          this.mGetstrProjectName(),
                          this.mGetiTaskID(), 
                          this.mGetstrTaskNum(),
                          this.mGetstrTaskName(), 
                          this.mGetobjType(), 
                          objDays, 
                          this.mGetnOrigDenormID() );
   objDays.mobjGetDay(this.mGetnDayOfWeek()).mSetDay(fFormatDecimals(-(this.mGetobjDay().mGetnHours())),this.mGetobjDay().mGetstrComment());
   objLine.mSetbIsEmpty(false);
   fCopyDFlex(this.mGetobjDay().arrDFlex, objDays.mobjGetDay(this.mGetnDayOfWeek()).arrDFlex);
   return objLine;
}
    
//----------------------------------------------------------------------------
// SingleEntryLines Class
// New in R11i Added by JFENG.
//----------------------------------------------------------------------------

// Constructor used to create a SingleEntryLines object.
function SingleEntryLines(){
  this.arrSingleEntryLine = new Array();
}

SingleEntryLines.prototype.miGetNumLines          = miSGetNumLines;
SingleEntryLines.prototype.mGetarrSingleEntryLine = mGetarrSingleEntryLine;
SingleEntryLines.prototype.mobjAddSingleEntryLine = mobjAddSingleEntryLine;
SingleEntryLines.prototype.mobjInsertSingleEntryLineSorted = mobjInsertSingleEntryLineSorted;
SingleEntryLines.prototype.mobjFindSingleEntryLine = mobjFindSingleEntryLine;

// Get the length of the array
function miSGetNumLines(){
  return this.arrSingleEntryLine.length;
}

function mGetarrSingleEntryLine() {
  return this.arrSingleEntryLine; 
}

// Add one SingleEntryLine
function mobjAddSingleEntryLine(p_iDenormID, p_iDisplayLineNum,
                  p_iProjectID, p_strProjectNum, p_strProjectName, 
                  p_iTaskID, p_strTaskNum, p_strTaskName, p_objType, 
                  p_objDays, p_nOrigDenormID, p_nDayOfWeek){  

  var l_iIndex = this.arrSingleEntryLine.length;
  this.arrSingleEntryLine[l_iIndex] = 
        new SingleEntryLine(p_iDenormID, p_iDisplayLineNum,
                 p_iProjectID, p_strProjectNum, p_strProjectName, 
                 p_iTaskID, p_strTaskNum, p_strTaskName, p_objType, 
                 p_objDays, p_nOrigDenormID, p_nDayOfWeek);

  return this.arrSingleEntryLine[l_iIndex];
}

// This function will insert SingleEntryLine p_objSingleEntryLine to
// sorted SingleEntryLines array this at Position p_iPosition
function mobjInsertSingleEntryLineSorted(p_objSingleEntryLine, p_iPosition)
{
  for (var i=this.miGetNumLines(); i>p_iPosition; i--)
  {
    this.arrSingleEntryLine[i] = this.arrSingleEntryLine[i-1];
  }
  // i = p_iPosition when exit out of the loop
  this.arrSingleEntryLine[p_iPosition] = p_objSingleEntryLine;
}


// Find a line by denorm id and week day.
function mobjFindSingleEntryLine(p_nDenormId, p_nWeekDay) {
  for (var i=0; i<this.miGetNumLines() ; i++) {
    if ((this.arrSingleEntryLine[i].mGetnOrigDenormID() == p_nDenormId) &&
       (this.arrSingleEntryLine[i].mGetnDayOfWeek() == p_nWeekDay)) {
      return this.arrSingleEntryLine[i];
    }
  }
}


//----------------------------------------------------------------------------
// SingleEntryTimecard Class
// New in R11i Added by JFENG.
//----------------------------------------------------------------------------

// Constructor used to create a SingleEntryTimecard object
function SingleEntryTimecard() {
  this.objHeader   = new Header();
  this.objSingleEntryLines    = new SingleEntryLines();
  this.nTotalHours = null;
}

SingleEntryTimecard.prototype.mGetobjHeader = mSGetobjHeader;
SingleEntryTimecard.prototype.mGetobjSingleEntryLines  = mGetobjSingleEntryLines;
SingleEntryTimecard.prototype.mGetnTotalHours  = mSGetnTotalHours;
SingleEntryTimecard.prototype.mSetobjSingleEntryLines = mSetobjSingleEntryLines;

// Get the header object associated with this Timecard object
function mSGetobjHeader(){
  return this.objHeader;
}

// Get the line object associated with this Timecard object
function mGetobjSingleEntryLines(){
  return(this.objSingleEntryLines);
}

// Get the timecard number associated with this Header object
function mSGetnTotalHours(){
  var nTotalHours = 0.00;
  for(var i=0; i<this.mGetobjSingleEntryLines().miGetNumLines(); i++) {
    nTotalHours += 
      (this.mGetobjSingleEntryLines().mGetarrSingleEntryLine()[i].mGetobjDay().mGetnHours() - 0);
  }
  return nTotalHours;
}

function mSetobjSingleEntryLines(p_objSingleEntryLines)
{
  this.objSingleEntryLines = p_objSingleEntryLines;
}

//  ---------------------------------------------------------------------------
//  Day Class
//  ---------------------------------------------------------------------------

// Constructor to create a Day object
function Day(p_nHours, p_strComment){
  this.nHours     = p_nHours;
  this.strComment = p_strComment;
  this.arrDFlex   = new Array();
}

Day.prototype.mGetnHours     = mGetnHours;
Day.prototype.mGetstrComment = mGetstrComment;
Day.prototype.mSetnHours     = mSetnHours;
Day.prototype.mSetstrComment = mSetstrDetailComment;
Day.prototype.mSetDay        = mSetDay;
Day.prototype.mCopy          = mCopy;

// Get number of hours associated with this Day object
function mGetnHours(){
  return(this.nHours);
}

// Get the comment associated with this Day object
function mGetstrComment(){
  return(this.strComment);
}

// Set number of hours with the given value
function mSetnHours(p_nHours){
  this.nHours = p_nHours;
}

// Set the comment with the given value
function mSetstrDetailComment(p_strComment){
  this.strComment = p_strComment;
}

// Set both the number of hours and comment with the given values
function mSetDay(p_nHours, p_strComment){
  this.mSetnHours(p_nHours);
  this.mSetstrComment(p_strComment);
}

// Copies contents of p_objSource to p_objTarget
function mCopy(p_objSource) {
  this.mSetDay(p_objSource.mGetnHours(), 
               new String(p_objSource.mGetstrComment()));

  // Copy DFlex fields
  // Moved copy logic to pawdflex.js (Bug#1082684)
  fCopyDFlex(p_objSource.arrDFlex, this.arrDFlex);
}

//  ---------------------------------------------------------------------------
//  Days Class
//  ---------------------------------------------------------------------------

// Constructor used to create the Days array
function Days(){
  this.arrDay = new Array();
  this.arrDay[C_iSUNDAY] = new Day('', '');
  this.arrDay[C_iMONDAY] = new Day('', '');
  this.arrDay[C_iTUESDAY] = new Day('', '');
  this.arrDay[C_iWEDNESDAY] = new Day('', '');
  this.arrDay[C_iTHURSDAY] = new Day('', '');
  this.arrDay[C_iFRIDAY] = new Day('', '');
  this.arrDay[C_iSATURDAY] = new Day('', '');
}

Days.prototype.mobjGetDay = mobjGetDay;
Days.prototype.mGetbIsEmptyWeek = mGetbIsEmptyWeek;

// Get the day object
function mobjGetDay(p_iIndex){
  return(this.arrDay[p_iIndex]);
}
  
function mGetbIsEmptyWeek()
{
  for (var i=0; i<7; i++) {
    if ((this.mobjGetDay(i).mGetnHours() != '') || (this.mobjGetDay(i).mGetstrComment() != '')) {
      return false;      
    } 
  }
  return true;
}

//  ---------------------------------------------------------------------------
//  Error Class
//  ---------------------------------------------------------------------------

// Constructor used to create an error object with the given values
function Error(p_iLine, p_dCellDate, p_strMessage, p_strMode, p_strFieldName, p_strType) {
  this.iLine        = p_iLine;
  this.dCellDate    = p_dCellDate;
  this.strMessage   = p_strMessage;
  this.strMode      = p_strMode;
  this.strFieldName = p_strFieldName;
  this.strType      = p_strType;
}

Error.prototype.mGetiLine        = mGetiLine;
Error.prototype.mGetdCellDate    = mGetdCellDate;
Error.prototype.mGetstrMessage   = mGetstrMessage;
Error.prototype.mGetstrMode      = mGetstrMode;
Error.prototype.mGetstrFieldName = mGetstrFieldName;
Error.prototype.mGetstrType      = mGetstrType;
Error.prototype.mbIsGeneralError = mbIsGeneralError;
Error.prototype.mbIsHeaderError  = mbIsHeaderError;
Error.prototype.mbIsLineError    = mbIsLineError;
Error.prototype.mbIsCellError    = mbIsCellError;
Error.prototype.mbIsSetupError   = mbIsSetupError;
Error.prototype.mbIsSetupWarning = mbIsSetupWarning;

// Accessor to return Line number
function mGetiLine() {
  return (this.iLine);
}

// Accessor to return Call Date
function mGetdCellDate() {
  return (this.dCellDate);
}

// Accessor to return Error Message
function mGetstrMessage() {
  return (this.strMessage);
}

// Accessor to return Error Mode
function mGetstrMode() {
  return (this.strMode);
}

// Accessor to return Field Name
function mGetstrFieldName() {
  return (this.strFieldName);
}

// Accessor to return Type Name
function mGetstrType() {
  return (this.strType);
}

// Method to check if General Error
function mbIsGeneralError() {
  return ( ((this.iLine) == "") && ((this.dCellDate) == "") &&((this.strType) == 'E') &&((this.strMode) == C_strGENERAL));
}

// Method to check if this is of Header error
function mbIsHeaderError() {
  if ((this.strMode == C_strHEADER) && (this.strType == 'E'))
    return true;
  else
    return false;
}

// Method to check if Line Error
function mbIsLineError() {
  return ( ((this.iLine) != "") && ((this.dCellDate) == "") && ((this.strType) == 'E'));
}

// Method to check if Cell Error
function mbIsCellError() {
  return ( ((this.iLine) != "") && ((this.dCellDate) != "") && ((this.strType) == 'E'));
}

// Method to check if this is a setup error 
function mbIsSetupError() {
  if ((this.strMode == C_strSETUP) &&(this.strType == 'E'))
    return true;
  else
    return false;
}

// Method to check if this is a setup warning 
function mbIsSetupWarning() {
  if ((this.strMode == C_strSETUP) &&(this.strType == 'W'))
    return true;
  else
    return false;
}



//  ---------------------------------------------------------------------------
// Errors Class
//  ---------------------------------------------------------------------------

// Constructor used to create the errors object
function Errors() {
  this.arrErrors   = new Array();
}

Errors.prototype.miGetNumOfErrors    = miGetNumOfErrors;
Errors.prototype.mbContainsErrors    = mbContainsErrors;
Errors.prototype.mobjAddError        = mobjAddError;
Errors.prototype.mobjAddGeneralError = mobjAddGeneralError;
Errors.prototype.mobjAddHeaderError  = mobjAddHeaderError;
Errors.prototype.mobjAddLineError    = mobjAddLineError;
Errors.prototype.mobjAddCellError    = mobjAddCellError;
Errors.prototype.mobjAddSetupError   = mobjAddSetupError;
Errors.prototype.mobjAddSetupWarning = mobjAddSetupWarning;

// Method to get the number of errors
function miGetNumOfErrors() {
  return (this.arrErrors.length);
}

// Method to check if errors are present
function mbContainsErrors() {
  return ( (this.arrErrors.length > 0) );
}

// Method to create an error and add it to the Errors object
function mobjAddError(p_iLine, p_dCellDate, p_strMessage, p_strMode, p_strFieldName) {
  this.arrErrors[this.arrErrors.length] = new Error(p_iLine, p_dCellDate, p_strMessage, p_strMode, p_strFieldName, "E");
}

// Method to create a general error and add the error to Errors
function mobjAddGeneralError(p_strMessage) {
  this.arrErrors[this.arrErrors.length] = new Error("", "", p_strMessage, C_strGENERAL, "", "E");
}

// Method to create a Line error and add the error to Errors
function mobjAddHeaderError(p_strMessage, p_strFieldName) {
  this.arrErrors[this.arrErrors.length] = new Error("", "", p_strMessage, C_strHEADER, p_strFieldName, "E");
}

// Method to create a Line error and add the error to Errors
function mobjAddLineError(p_iLine, p_strMessage, p_strFieldName) {
  this.arrErrors[this.arrErrors.length] = new Error(p_iLine, "", p_strMessage, C_strLINES, p_strFieldName, "E");
}

// Method to create a Cell error and add the error to Errors
function mobjAddCellError(p_iLine, p_dCellDate, p_strMessage, p_strFieldName) {
  this.arrErrors[this.arrErrors.length] = new Error(p_iLine, p_dCellDate, p_strMessage, C_strCELL, p_strFieldName, "E");
}

// Method to create a setup error and add the error to Errors
function mobjAddSetupError(p_strMessage) {
  this.arrErrors[this.arrErrors.length] = new Error("", "", p_strMessage, C_strSETUP, "", "E");
}

// Method to create a setup warning and add the error to Errors
function mobjAddSetupWarning(p_strMessage) {
  this.arrErrors[this.arrErrors.length] = new Error("", "", p_strMessage, C_strSETUP, "", "W");
}

//  ---------------------------------------------------------------------------
//  ProfileOption Class
//  ---------------------------------------------------------------------------

// Constructor to create the ProfileOption object
function ProfileOption() {
  this.arrProfile = new Array();
}

ProfileOption.prototype.mvAddProfOpt    = mvAddProfOpt;
ProfileOption.prototype.mvGetProfOpt   = mvGetProfOpt;

// Add a profile option and value
function mvAddProfOpt(p_strProfileName, p_vValue) {
  this.arrProfile[p_strProfileName] = p_vValue;
  return this.arrProfile[p_strProfileName];
}

// Get a profile option and value.  If the p_strProfileName does not
// exist, the function will return null.
function mvGetProfOpt(p_strProfileName) {
var undefined;

  if (this.arrProfile[p_strProfileName] == undefined)
    return null;
  return this.arrProfile[p_strProfileName];
}

//  ---------------------------------------------------------------------------
//  Regions Class
//  ---------------------------------------------------------------------------

// Constructor to create the Regions object
function Regions() {
  this.arrRegion = new Array();
}

Regions.prototype.mobjAddRegion    = mobjAddRegion;
Regions.prototype.mobjGetRegion    = mobjGetRegion;

// Create a Region object with name p_strRegionName and add to the
// Regions object
function mobjAddRegion(p_strRegionName) {
  this.arrRegion[p_strRegionName] = new Region();
  return this.arrRegion[p_strRegionName];
}

// Gets the Region object with the name p_strRegionName.  If the region
// does not exist then the function will return null.
function mobjGetRegion(p_strRegionName) {
var undefined;
  if (this.arrRegion[p_strRegionName] == undefined)
    return null;
  return this.arrRegion[p_strRegionName];
}

//  ---------------------------------------------------------------------------
//  Region Class
//  ---------------------------------------------------------------------------

// Constructor to create the Region object
function Region() {
  this.arrRegionItem = new Array();
}

Region.prototype.mobjAddRegionItem    = mobjAddRegionItem;
Region.prototype.mobjGetRegionItem    = mobjGetRegionItem;

// Create a Region object with name p_strRegionName and add to the
// Regions object
function mobjAddRegionItem(p_strRegionItemName, p_strLabel, p_iLength) {
  this.arrRegionItem[p_strRegionItemName] = 
    new RegionItem(p_strLabel, p_iLength);
  return this.arrRegionItem[p_strRegionItemName];
}

// Gets the Region object with the name p_strRegionName.  If the region
// does not exist then the function will return null.
function mobjGetRegionItem(p_strRegionItemName) {
var undefined;
  if (this.arrRegionItem[p_strRegionItemName] == undefined)
    return null;
  return this.arrRegionItem[p_strRegionItemName];
}

//  ---------------------------------------------------------------------------
//  RegionItem Class
//  ---------------------------------------------------------------------------

// Constructor to create the RegionItem object
function RegionItem(p_strLabel, p_iLength) {
  this.strLabel = p_strLabel;
  this.iLength = p_iLength;
}

RegionItem.prototype.mGetstrLabel    = mGetstrLabel;
RegionItem.prototype.mGetiLength     = mGetiLength;

// Get the label for the RegionItem object
function mGetstrLabel() {
  return this.strLabel;
}

// Gets the length for the RegionItem object
function mGetiLength() {
  return this.iLength;
}

//-------------------------------------------------------------
// Message class
//-------------------------------------------------------------

// Constructor to create the Message object.
function Message() {
  this.arrMessage = new Array();
}

Message.prototype.mstrAddMsg     = mstrAddMsg;
Message.prototype.mstrGetMsg     = mstrGetMsg;
Message.prototype.fstrReplToken   = fstrReplToken;

function mstrAddMsg(p_strMsgName, p_strMsgText)
{
  this.arrMessage[p_strMsgName] = p_strMsgText;
}

function mstrGetMsg(p_strMsgName)
{
  if ( this.arrMessage[p_strMsgName] == null )
    return p_strMsgName;
  else
    return this.arrMessage[p_strMsgName];
}

function fstrReplToken(p_strOrig, p_strToken, p_strValue)
{
  var strNew = "";
  var strRemaining = p_strOrig; 
  var index, count;

  while (( index = strRemaining.indexOf(p_strToken)) >=0 )
  {
    strNew = strNew + strRemaining.substring(0, index) + p_strValue;
    strRemaining =  strRemaining.substring(index+p_strToken.length, strRemaining.length);
  }

 strNew = strNew + strRemaining;

 return strNew;
}

//  ---------------------------------------------------------------------------
//  LookupTypes Class
//  ---------------------------------------------------------------------------

// Constructor to create the LookupTypes object
function LookupTypes() {
  this.arrLookupType = new Array();
}

LookupTypes.prototype.mobjAddLookupType    = mobjAddLookupType;
LookupTypes.prototype.mobjGetLookupType    = mobjGetLookupType;

// Create a LookupType object with name p_strLookupTypeName and add to the
// LookupTypes object
function mobjAddLookupType(p_strLookupTypeName) {
  this.arrLookupType[p_strLookupTypeName] = new LookupType();
  return this.arrLookupType[p_strLookupTypeName];
}

// Gets the LookupType object with the name p_strLookupTypeName.  If the LookupType
// does not exist then the function will return "".
function mobjGetLookupType(p_strLookupTypeName) {
  var undefined;
  if (this.arrLookupType[p_strLookupTypeName] == undefined)
    return "";
  return this.arrLookupType[p_strLookupTypeName];
}

//  ---------------------------------------------------------------------------
//  LookupType Class
//  ---------------------------------------------------------------------------

// Constructor to create the LookupType object
function LookupType() {
  this.arrLookupCode = new Array();
}

LookupType.prototype.mobjAddLookupCode    = mobjAddLookupCode;
LookupType.prototype.mobjGetLookupCode    = mobjGetLookupCode;

// Create a LookupCode object with name p_strLookupCodeCode and add to the
// LookupType object
function mobjAddLookupCode(p_strLookupCodeCode, p_strLookupCodeDesc) {
  var objLookupCode;

  objLookupCode = new LookupCode(p_strLookupCodeCode, p_strLookupCodeDesc);
  this.arrLookupCode[this.arrLookupCode.length] = objLookupCode;
  return objLookupCode;
}

// Gets the LookupType object with the name p_strLookupTypeName.  If the LookupType
// does not exist then the function will return null.
function mobjGetLookupCode(p_strLookupCodeCode) {

  for(var i = 0; i<this.arrLookupCode.length; i++) {
    if (this.arrLookupCode[i].mGetstrCode() == p_strLookupCodeCode)
      return(this.arrLookupCode[i]);
  }
  return "";
}

// Returns the number of lookup codes defined for the lookup type.
function miGetNumLookupCode() {
  return this.arrLookupCode.length;
}

//  ---------------------------------------------------------------------------
//  LookupCode Class
//  ---------------------------------------------------------------------------

// Constructor to create the LookupCode object
function LookupCode(p_strCode, p_strDesc) {
  this.strCode = p_strCode;
  this.strDesc = p_strDesc;
}

LookupCode.prototype.mGetstrCode    = mGetstrCode;
LookupCode.prototype.mGetstrDesc     = mGetstrDesc;

// Get the internal code for the LookupCode object
function mGetstrCode() {
  return this.strCode;
}

// Gets the description for the LookupCode object
function mGetstrDesc() {
  return this.strDesc;
}


//  ---------------------------------------------------------------------------
//  AuditLine Class
//  ---------------------------------------------------------------------------

// Constructor to create the AuditLine object
function AuditLine(p_iDenormID, p_dEntryDate, p_strProjectNum, 
              p_strTaskNum, 
              p_objType, p_nQuantity, p_strAuditType,
              p_strAuditReason, p_strAuditComment, p_strCreationDate){
  this.iDenormID       = p_iDenormID;
  this.dEntryDate      = p_dEntryDate;
  this.strProjectNum   = p_strProjectNum;
  this.strTaskNum      = p_strTaskNum;
  this.objType         = p_objType; 
  this.nQuantity       = p_nQuantity;
  this.strAuditType    = p_strAuditType;
  this.strAuditReason  = p_strAuditReason;
  this.strAuditComment = p_strAuditComment;
  this.strCreationDate   = p_strCreationDate;
}

AuditLine.prototype.mGetnDenormID       = mGetnDenormID;
AuditLine.prototype.mGetdEntryDate      = mGetdEntryDate;
AuditLine.prototype.mGetstrProjectNum   = mGetstrProjectNum;
AuditLine.prototype.mGetstrTaskNum      = mGetstrTaskNum;
AuditLine.prototype.mGetobjType         = mGetobjType;
AuditLine.prototype.mGetnQuantity       = mGetnQuantity;
AuditLine.prototype.mGetstrAuditType    = mGetstrAuditType;
AuditLine.prototype.mGetstrAuditReason  = mGetstrAuditReason;
AuditLine.prototype.mGetstrAuditComment = mGetstrAuditComment;
AuditLine.prototype.mGetstrCreationDate   = mGetstrCreationDate;

AuditLine.prototype.mSetdEntryDate      = mSetdEntryDate;
AuditLine.prototype.mSetstrProjectNum   = mSetstrProjectNum;
AuditLine.prototype.mSetstrTaskNum      = mSetstrTaskNum;
AuditLine.prototype.mSetobjType         = mSetobjType;
AuditLine.prototype.mSetnQuantity       = mSetnQuantity;
AuditLine.prototype.mSetstrAuditType    = mSetstrAuditType;
AuditLine.prototype.mSetstrAuditReason  = mSetstrAuditReason;
AuditLine.prototype.mSetstrAuditComment = mSetstrAuditComment;

// Get the Denorm ID associated with this Audit Line object
function mGetnDenormID(){
  return this.iDenormID;
}

// Get the Entry Date ID associated with this Audit Line object
function mGetdEntryDate(){
  return this.dEntryDate;
}

// Get the project number associated with this Audit Line object
function mGetstrProjectNum(){
  return this.strProjectNum;
}

// Get the task number associated with this Audit Line object
function mGetstrTaskNum(){
  return this.strTaskNum;
}

// Get the type object associated with this Audit Line object
function mGetobjType(){
  return this.objType;
}

// Get the hours quantity associated with this Audit Line object
function mGetnQuantity(){
  return this.nQuantity;
}

// Get the Audit Type associated with this Audit Line object
function mGetstrAuditType(){
  return this.strAuditType;
}

// Get the Audit Reason associated with this Audit Line object
function mGetstrAuditReason(){
  return this.strAuditReason;
}

// Get the Audit Comment associated with this Audit Line object
function mGetstrAuditComment(){
  return this.strAuditComment;
}

// Get the Creation date of this Audit Line object
function mGetstrCreationDate(){
  return this.strCreationDate;
}

// Set the entry date of this Audit Line object with the given value
function mSetdEntryDate(p_dEntryDate){
  this.dEntryDate = p_dEntryDate;
}

// Set the project number of this Audit Line object with the given value
function mSetstrProjectNum(p_strProjectNum){
  this.strProjectNum = p_strProjectNum;
}

// Set the task number of this Audit Line object with the given value
function mSetstrTaskNum(p_strTaskNum){
  this.strTaskNum = p_strTaskNum;
}

// Set the type object of this Audit Line object with the given value
function mSetobjType(p_objType){
  this.objType = p_objType;
}

// Set the quantity of this Audit Line object with the given value
function mSetnQuantity(p_nQuantity){
  this.nQuantity = p_nQuantity;
}

// Set the audit type of this Audit Line object with the given value
function mSetstrAuditType(p_strAuditType){
  this.strAuditType = p_strAuditType;
}

// Set the audit reason of this Audit Line object with the given value
function mSetstrAuditReason(p_strAuditReason){
  this.strAuditReason = p_strAuditReason;
}

// Set the audit comment of this Audit Line object with the given value
function mSetstrAuditComment(p_strAuditComment){
  this.strAuditComment = p_strAuditComment;
}


//  ---------------------------------------------------------------------------
//  Audit History Class
//  ---------------------------------------------------------------------------

// Constructor used to create a lines array
function AuditHistory(){
  this.arrAuditLine = new Array();
}

AuditHistory.prototype.mGetarrAuditLine       = mGetarrAuditLine;
AuditHistory.prototype.miGetNumAuditLines     = miGetNumAuditLines;
AuditHistory.prototype.mAddAuditLine          = mAddAuditLine;
AuditHistory.prototype.mstrSerializeAuditHistory = mstrSerializeAuditHistory;

function mGetarrAuditLine() {
  return this.arrAuditLine;
}

// Get the length of the array
function miGetNumAuditLines(){
  return(this.arrAuditLine.length);
}

// Add one line
function mAddAuditLine(p_iDenormID, p_dEntryDate, p_strProjectNum, 
              p_strTaskNum, 
              p_objType, p_nQuantity, p_strAuditType,
              p_strAuditReason, p_strAuditComment, p_strCreationDate){

  var l_iIndex = this.arrAuditLine.length;

  this.arrAuditLine[l_iIndex] = 
        new AuditLine(p_iDenormID, p_dEntryDate, p_strProjectNum, 
              p_strTaskNum, 
              p_objType, p_nQuantity, p_strAuditType,
              p_strAuditReason, p_strAuditComment, p_strCreationDate);

  return this.arrAuditLine[l_iIndex];
}


// Serialize the Audit History lines
function mstrSerializeAuditHistory()
{
  var C_strFIELD_DELIMITER = '@att@';
  var C_strLINE_DELIMITER  = '@line@';

  var l_strSerializeString;	
  var l_bSerialize = false;

  var l_arrTemp = new Array();

  l_strSerializeString = '';
  for (i=0; i < this.miGetNumAuditLines(); i++) {
    var l_objLine = this.arrAuditLine[i];
    if (l_objLine.iDenormID == '')
      l_bSerialize = false;
    else 
      l_bSerialize = true;
    
    if (l_bSerialize) {

      l_strSerializeString += C_strFIELD_DELIMITER +
        l_objLine.mGetnDenormID() 
		+ C_strFIELD_DELIMITER +
  	fDateToLongString(g_strSessDateFormat, 
                          l_objLine.mGetdEntryDate(),true)
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetstrProjectNum()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetstrTaskNum()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetobjType().mGetstrExpenditureType()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetobjType().mGetstrSystemLinkage()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetnQuantity()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetstrAuditType()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetstrAuditReason()
		+ C_strFIELD_DELIMITER +
     	l_objLine.mGetstrAuditComment()
		+ C_strFIELD_DELIMITER +
        l_objLine.mGetstrCreationDate()
                + C_strFIELD_DELIMITER;

      l_strSerializeString += C_strLINE_DELIMITER;
   } // If serialize == true
  }
  return l_strSerializeString;
}
