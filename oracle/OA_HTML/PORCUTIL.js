/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================+
| FILENAME                                                         |
|   PORCUTIL.js                                             |
|                                                                  |
| HISTORY                                                          |
|   03-DEC-98       marychu       Created                          |
|                                                                  | 
+==================================================================*/
/* $Header: PORCUTIL.js 115.14 99/10/21 11:29:12 porting ship $ */

submitInProcess = false;

function roundDecimal(expr, digits)
{
  var str= "" + Math.round(eval(expr) * Math.pow(10, digits));
  while (str.length <= digits)
  {
    str= "0"+str;
  }
  var decpoint= str.length- digits;
  return str.substring(0, decpoint) + "." + str.substring(decpoint, str.length);
}

function isPositiveNumber(num)
{
  var f= parseFloat(num);
  return (!isNaN(f) && f>=0);
}

/*********************************************
 *  Object manipulation functions
 *********************************************/
function getObject(obj)
{
  var theObj;
  if(typeof obj == "string") 
  {
    theObj= eval("document."+coll+obj+styleObj);
  }
  else 
  {
    theObj= obj;
  }
  return theObj;
}

function getLayer(name)
{
  var ret= searchLayer(top, name);
  return ret;
}

function findLayer(layerArray, lName) 
{
  var ret=layerArray[lName];
  if(ret!=null)
    return ret;
  else
  {
    for(var i=0; i<layerArray.length; i++)
    {
      ret= findLayer(layerArray[i].layers, lName);
      if(ret != null)
        return ret;
    }
  }
  return null;
}

function searchLayer(win, lName)
{
  var ret;
  if(IS_NAV)
  {
    ret= findLayer(win.document.layers, lName);
    if (ret!=null) 
      return ret;
  }
  else 
  {
    ret= win.document.all[lName];
    if (ret!=null) 
    {
      return ret;
    }
  }
  
  for(var i=0; i<win.frames.length; i++)
  {
    ret= searchLayer(win.frames[i], lName);
    if (ret!=null) 
      return ret;
  }
  if (win.mWin!=null && !win.mWin.closed) 
  {
    ret=searchLayer(win.mWin, lName);
    if (ret!=null) 
      return ret;
  }
}


function getObjWidth(obj) {
  if (IS_NAV)
  {
    return obj.clip.width;
  } else {
    return getStyle(obj).clientWidth;
  }
}

function getObjHeight(obj) {
  if (IS_NAV) 
  {
    return obj.clip.height;
  } else {
    return obj.style.height;
  }
}

function moveObject(obj, x, y)
{
  //  var theObj= getObject(obj);
  if (IS_NAV)
  {
    obj.moveTo(x,y);
  }
  else
  {
    obj.style.pixelLeft=x;
    obj.style.pixelTop=y;
  }
}

function getObjLeft(obj)  {
  if (IS_NAV) {
    return obj.left;
  } else {
    return obj.style.pixelLeft;
  }
}
// Utility function returns the y coordinate of a positionable object
function getObjTop(obj)  
{
  if (IS_NAV) {
    return obj.top;
  } else {
    return obj.style.pixelTop;
  }
}

function getObjRight(obj)  {
  return getObjLeft(obj) + getObjWidth(obj);
}

function getObjBottom(obj)  {
  return getObjTop(obj) + getObjHeight(obj);
}
function getStyle(obj)
{
  if(IS_NAV)
  {
    return obj;
  }
  else
  {
    return obj.style;
  }
}

function show(obj) {
  getStyle(obj).visibility = "visible";
}

function hide(obj) {
  getStyle(obj).visibility = "hidden";
}

function setBgcolor(obj, color)
{
  if (IS_NAV) 
  {
    obj.bgColor=color;
  }
  else 
  {
    obj.style.backgroundColor= color;
  }
}

function setContent(lyr,text) 
{
  if (IS_NAV) 
  {
    lyr.document.open();
    lyr.document.write(text);
    lyr.document.close();
  }
  else 
  {
    lyr.innerHTML = text;
  }
}
/*
function setTooltips(theWin)
{
  for(var i=0; i<theWin.document.links.length; i++)
  {
    var alink= theWin.document.links[i];
    alink.onMouseOver= new Function("showHelp(event, '" + alink.name
                                    +"')");
    alink.onMouseOut= new Function("hideHelp()");
  }
}
*/
function findFrame(aWin, aName)
{
  var ret= aWin.frames[aName];
  if(ret!=null)
    return ret;
  else
  {
    for(var i=0; i<aWin.frames.length; i++)
    {
      ret= findFrame(aWin.frames[i], aName);
      if(ret != null)
        return ret;
    }
  }
  if(aWin.mWin !=null && !aWin.mWin.closed)
  {
    ret=findFrame(aWin.mWin, aName);
    if(ret!=null)
      return ret;
  }
  return null;
}

function clearForms(doc)
{
  for(var i=0; i<doc.forms.length; i++)
  {
    allForms[doc.forms[i].name]=null;
  }

  if(IS_NAV)
  {
    for(var i=0; i<doc.layers.length; i++)
    {
      clearForms(doc.layers[i].document);
    }
  }
  doc.formsCleaned=true;
}

function initDocument(doc, frameName) 
{
  if(!doc.formsCleaned)
  {
    clearForms(doc);
  }
  
  for(var i=0; i<doc.forms.length; i++)
  {
    if(doc.forms[i].elements.length>0)
      initForm(doc.forms[i]);
  }

  if(IS_NAV)
  {
    for(var i=0; i<doc.layers.length; i++)
    {
      initDocument(doc.layers[i].document, frameName);
    }
  }

  if(doc.errorMessage)
  {
    openModalWindow("","errorWin");
    var errorWin=mWin;
    while(errorWin.mWin)
    {
      errorWin=errorWin.mWin;
    }
    errorWin.document.write(doc.errorMessage);
    errorWin.document.close();
  }
  
  changeImages(doc);
  changeLinks(doc);
  window.onclick= clickOrFocus;


  window.onfocus= clickOrFocus;

  top.noSave= false;

  clearSubmitFlag();

  setTrapEscape(top);


} // end of function initDocument()

function changeImages(doc)
{
  if(IS_IE)
  {
    for(var i=0; i<doc.images.length; i++)
    {
      doc.images[i].alt=replaceEntity(doc.images[i].alt);
    }
  }
}

function changeLinks(doc)
{
  if(IS_IE)
  {
    for(var i=0; i<doc.links.length; i++)
    {
      doc.links[i].href=replaceEntity(doc.links[i].href);
    }
  }
}

function initForm(fm)
{
  var arr= allForms[fm.name];
  if(arr==null)
  {
    allForms[fm.name]= new Array();
  }
  var len= allForms[fm.name].length;
  allForms[fm.name][len]= fm;
  fm.updated=false;

  for(var i=0; i<fm.elements.length; i++)
  {
    var elem= fm.elements[i];
    // check to see if each element has any dependencies; eg requester
    // && requester_id; if so, null out dependents onChange of dependee
    var dependents= ATTRIBUTE_DEPENDENCY[elem.name];
    var cmd="";
    if(dependents)
    {
      for(var d=0; d<dependents.length; d++)
      {
        if(fm.elements[dependents[d]])
        {
          cmd+="this.form."+ dependents[d]+".value='';";
        }
      }
    }
    fm.elements[i][getHandlerName("onChange")]= new Function(cmd+"this.form.updated=true");

    // Bug 952184, onChange event doesnt fire unless you tab out of the field
    // adding handler for onFocus just for POR_LINES_R region
    if (fm.name == 'POR_LINES_R')
      fm.elements[i][getHandlerName("onFocus")] = new Function("this.form.updated=true");


    if(fm.elements[i].type!="button" && fm.elements[i].type!="submit")
      fm.elements[i][getHandlerName("onClick")]= new Function("this.form.updated=true");
  } // matches   for(var i=0; i<fm.elements.length; i++)
}
/*  -- obsolete: to be removed
function SubmitInfo(u, a,f, data, fn, tar)
{
  this.url=u;
  this.action=a;
  this.func=f;
  this.generalData=data;
  this.formNames=fn;
  this.target=tar;
}
*/
//currentTemplate=null;

function getReqToken(func, act, temp)
{
  // Pocurement team also uses this function, but does not pass in any
  // parameters.  Default the parameters as if this is being called by
  // Pocurement server.
  func= (func==null) ? "addToOrder" : func;
  act= (act==null) ? "addToOrder" : act;
  temp= (temp==null) ? "por_search" : temp;

  var ret=
    "template"+ VALUE_DELIMITER + temp + FIELD_DELIMITER+
    "function"+ VALUE_DELIMITER + func + FIELD_DELIMITER+
    "action" + VALUE_DELIMITER + act + FIELD_DELIMITER+
    "reqSessionId" + VALUE_DELIMITER + reqSessionId + FIELD_DELIMITER+
    "reqHeaderId" + VALUE_DELIMITER + reqHeaderId + FIELD_DELIMITER+
    "emergencyReq" + VALUE_DELIMITER + emergencyReqFlag;

  return ret;
}

function submitRequest(currentTemplate, //
                       url,     // the url to submit the form to 
                       action,  // used to lookup the next template in
                                // por_master.xml
                       func,    // used by Apps layer to determine
                                // what to so with the submission
                       generalData,// concatenate name value pair of all data, other than
                       // REQ_TOKEN, that does not belong in any
                       // region, eg currentSelectedLine
                       // this field must start with FIELD_DELIMITER
                       formNames,// array of names of forms that needs to be submited
                       target,  //  name of target window that gets result of submission
                       method)  // request method for form.  This
                                // parameter is optional and has a
                                // default value of POST
{
  // first, check that all mandatory fields are filled out, if not,
  // return without doing anything

  setSubmitFlag();



  if(formNames!=null)
  {
    for(var i=0; i<formNames.length; i++)
    {
      if(!checkMandatory(formNames[i]))
      { 
        clearSubmitFlag();
        return;
      }
    }
  }
  
  var result= allForms["POR_RESULT_FORM"][0];

  result.action= url;
  result.target= target;

  //  Use post method to submit the form to middle tier, unless
  //  otherwise specified by caller.
  // For por_main, since this is the frame that gets reloaded when the
  // reload button is clicked, the method must be get.
  result.method= ((method=="GET" || target=="por_main") ? "GET" : "POST");
  //  alert(result.method);
  
  //clear out previous names and values of the form 
  for(var i=0; i<result.length; i++)
  {
    result[i].value= result[i].name="";
  }
  
  var data= 
    getReqToken(func, action,currentTemplate)+ generalData;

  result.elements[0].name= "REQ_TOKEN";
  result.elements[0].value= data; //getReqToken()

  if(formNames!=null)
  {
    for(var i=0; i<formNames.length; i++)
    {
      if(formNames[i]=="selectedLinesList" && allForms["POR_LINES_R"])
      {
        var str="";
        var lines=allForms["POR_LINES_R"];
        for(var j=0; j<lines.length; j++)
        {
          if(lines[j].POR_SELECT_BOX.checked)
          {
//            str= str+ j + FIELD_DELIMITER;
            str= str + lines[j].POR_LINE_NUM.value + FIELD_DELIMITER;
          }
        }
        result.elements[1+i].name = "selectedLinesList";
        result.elements[1+i].value = str.substring(0, str.length-1);
      }
      else
      {
        var form= allForms[formNames[i]];
        if(form)
        {
          result.elements[1+i].name= formNames[i];
          result.elements[1+i].value= packForm(form);
        }
      }
    } // end for(...i<formNames.length..)
  }
  result.submit();
} // end of submitRequest

function packForm(form)
{
  var ret="";

  // Bug 903689
  // if form is an array (more than one), check if all are to be submitted
  // even if only one is changed
  var frmSubmitAll = false;
  if (form.length > 0)
  {
    for(var i=0; i<FORM_SUBMIT_ALL.length; i++)
	{
      if (FORM_SUBMIT_ALL[i] == form[0].name)
	    frmSubmitAll = true;
	}
  }
  // set their updated property to true to force all forms to be submitted
  if (frmSubmitAll)
  {
    for(var i=0; i<form.length; i++)
    {
      form[i].updated = true;
	}
  }


  for(var i=0; i<form.length; i++)
  {
    if(form[i].updated && form[i].elements[0])
    {
      for(var j=0; j<form[i].elements.length; j++)
      {
        ret+= getNameValue(form[i].elements[j]);
      }
      ret= ret.substring(0, ret.length-FIELD_DELIMITER.length);
      ret+=LINE_DELIMITER;
    }
  }
  //  alert(ret);
  
  return ret.substring(0, ret.length-LINE_DELIMITER.length);
}


function unpackForm(aForm, aString)
{
  var elemStart=0;
  var elemEnd=0;
  
  for(elemStart=0; 
      elemStart<=aString.length && elemStart>=0;
      elemStart=elemEnd+top.FIELD_DELIMITER.length)
  {
    //alert("elemStart="+elemStart+"\nelemEnd="+elemEnd);
    elemEnd= aString.indexOf(top.FIELD_DELIMITER, elemStart);
    elemEnd= (elemEnd>=0) ? elemEnd : aString.length;
    var delimiter= aString.indexOf(top.VALUE_DELIMITER, elemStart);
    var elemName= aString.substring(elemStart, delimiter);
    var elemVal= aString.substring(delimiter+top.VALUE_DELIMITER.length, 
                                   elemEnd);
    var elem=aForm.elements[elemName];
    if(elem)
    {
      if(elem.type=="select-one")
      {
        for(var i=0; i<elem.options.length; i++)
        {
          elem.options[i].selected=(elem.options[i].value==elemVal);
        }
      } 
      else if(elem.type=="checkbox")
      {
        elem.checked= (elemVal=='Y');
      }
      else if(elem.type=="radio")
      {
        for(var i=0; i<elem.length; i++)
        {
          elem[i].checked=(elem[i].value==elemVal);
        }
      }
      else  // text elements
      {
        elem.value=elemVal;
      }
    }
  }
}

function getNameValue(elem)
{
  if(elem.type=="radio" && !elem.checked)
  {
    return "";
  }

  if(elem.type=="checkbox")
  {
    return elem.name+ VALUE_DELIMITER+ 
      (elem.checked ? 'Y' : 'N') + FIELD_DELIMITER;
  }

  if(elem.type=="select-one")
  {
    return elem.name+ VALUE_DELIMITER+ 
      elem.options[elem.selectedIndex].value + FIELD_DELIMITER;
  }    
  // other wise (text, hidden, text area) 
  return elem.name+ VALUE_DELIMITER+ elem.value+ FIELD_DELIMITER;
}

function checkMandatory(formName) 
{
  var forms= allForms[formName];
  var names= MANDATORY_FIELDS[formName];
  
  if(!names /*|| names.length==0*/)
    return true;

  if(!forms /*|| forms.length==0*/)
    return true;
  
  for(var j=0; j<forms.length; j++)
  {
    var form= forms[j];

    // Bug 942623 - check if form contains any elements - if condition added
    if(form.elements[0])
	{

      for(var i=0; i<form.elements.length; i++)
      {
        var elem=form.elements[i];
        if((elem.type== "text" || elem.type== "textarea") &&
           isEmpty(elem.value) &&
           names[elem.name]== "Y")
        {
          getActiveWindow().alert(FND_MESSAGES["ICX_POR_ALRT_IS_MANDATORY"]);
          return false;
        }
        
      }

    }
    /*    
          for(var i=0; i<names.length; i++)
          {
          var elem= form.elements[names[i]];
          if(elem &&
          isEmpty(elem.value) &&
          (elem.type=="text" || elem.type=="textarea"))
          {
          alert(FND_MESSAGES["ICX_POR_ALRT_IS_MANDATORY"]);
          return false;
          }
          }
    */
    
  }
  return true;
}

function isEmpty(str)
{
  for(var i=0; i<str.length; i++)
  {
    var ch= str.charAt(i);
    if(ch != '\n' && ch!=' ' && ch!='\t')
      return false;
  }
  return true;
}

/* gets the specified element value from the specified form
   if ind>=0; it would be used as the index of the form to look into,
   otherwise, the forms array would be searched until the element is
   found (useful for single line forms that spans multiple forms)
 */
function getElementValue(ind, formName, elementName)
{
  if (ind<0)
    ind= findIndex(formName, elementName);
  return (ind>=0) ? allForms[formName][ind][elementName].value :null;
}

function findIndex(formName, elementName)
{
  var forms=allForms[formName];
  for(var i=0; i<forms.length; i++)
  {
    if(forms[i][elementName])
      return i;
  }
  return -1;
}

function replaceEntity(str, env)
{
  var entityExp= /(&\{.*};)/;
  var ret="";
  str= str.replace(entityExp, '"+eval("$1".substring(2,"$1".length-2))+"');
  return eval('"' + str + '"');
}
  
/************************* Functions for tabs *********************/
function Tab(t, s, so, si, inst)
{
    this.title=t;
this.selected=s;
this.instruction= inst;
this.onSwitchOut= so;
this.onSwitchIn=si;
 
this.bgcolor= (this.selected ? "#cccccc" : "#6699CC");
this.drawBorder= drawTabBorder;
this.draw=drawTab;
this.drawBottom= drawTabBottom;
}

function Index(ti, tt)
{
  this.title=ti;
  this.tabs=tt;
  this.draw=drawIndex;
  this.switchTab=switchIndexTab;
  for(var i=0; i<this.tabs.length; i++)
  {
    this.tabs[i].tabNumber=i;
    if(this.tabs[i].selected)
    {
      this.selectedIndex=i;
    }
  }
}

function drawTabBorder(d)
{
  var str= (this.selected ?
            ("<td rowspan=2><img src=/OA_MEDIA/FNDGTL.gif></td>" +
             "<td bgcolor=#ffffff height=1><img src=/OA_MEDIA/FNDPX6.gif></td>" +
             "<td rowspan=2><img src=/OA_MEDIA/FNDGTR.gif></td>") :
            ("<td rowspan=2><img src=/OA_MEDIA/FNDIPTL.gif></td>" +
             "<td bgcolor=#6699CC height=1><img src=/OA_MEDIA/FNDPX4.gif></td>" +
             "<td rowspan=2><img src=/OA_MEDIA/FNDIPTR.gif></td>"));
  
  d.write(str);
}

function drawTabBottom(d)
{
  var str= (this.selected ?
            "<td colspan=3 bgcolor=#cccccc><img src=/OA_MEDIA/FNDPX6.gif></td>" :
            "<td colspan=3 bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td>");
            
  d.write(str);
}

            
function drawIndex(d)
{
  // var str= 
    //"<LINK REL=STYLESHEET HREF='/OA_HTML/PORSTYLE.css' TYPE='text/css'>" +
  d.write(  "<html>" +
    "<head>" +
    "<LINK REL=STYLESHEET HREF=/OA_HTML/" + top.LANGUAGE_CODE + "/PORSTYLE.css TYPE=text/css>");
  d.write("</head><!-- james -->" +
    "<body bgcolor=#336699>" +
    "<Table width=100% cellpadding=0 cellspacing=0 border=0>" + 
    "<tr bgcolor=#336699>" +
    "<td rowspan=3><font color=#336699>.</font></td>" +
    "<td rowspan=3 nowrap><font class=containertitle>" + this.title + "</font></td>" +
    "<td rowspan=3 width=100%><font color=#336699>.</font></td>" ) ;

  //d.write(str);

  for(var i=0; i<this.tabs.length; i++)
  {
    d.write("<td colspan=3><img src=/OA_MEDIA/FNDPX3.gif height=6></td>");


  }
  // spacer??
  d.write("<td rowspan=3><img src=/OA_MEDIA/FNDPX3.gif></td>");


  d.write("</tr><tr bgcolor=#336699>");
  for(var i=0; i<this.tabs.length; i++)


  {
    this.tabs[i].drawBorder(d);
  }
  d.write("</tr><tr>");
  
  for(var i=0; i<this.tabs.length; i++)


  {
    this.tabs[i].draw(d);
  }
  d.write("</tr>");

  str= "<tr>"+
    "<td rowspan=2 bgcolor=#cccccc valign=top><img src=/OA_MEDIA/FNDCGTL.gif></td>" +
    "<td colspan=2 bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td>";
  d.write(str);
  
  for(var i=0; i<this.tabs.length; i++)


  {
    this.tabs[i].drawBottom(d);
  }
  
  str="<td align=right rowspan=2 bgcolor=#cccccc valign=top><img src=/OA_MEDIA/FNDCGTR.gif></td></TR>" +
    "<tr bgcolor=#cccccc>" +
    "<td colspan=" + (4+3*this.tabs.length-1) +" height=200 valign=top>" +
    "<FONT CLASS=helptext>" + this.tabs[this.selectedIndex].instruction+
    "</td></tr></table></body></html>";
  d.write(str);
}

function drawTab(d)
{
  if(this.selected) 
  {
    var str=  
      "<td bgcolor=#cccccc nowrap><FONT CLASS=graytab>" + this.title + 
      "</font></td>";
  }
  else
  {
    str= 
      "<td bgcolor=#6699CC nowrap><a href=\"javascript:top.currentIndex.switchTab(" +
      this.tabNumber+ ");\"><FONT CLASS=purpletab>" + this.title + 
      "</font></a></td>";
  }
  d.write(str);


}

function switchIndexTab(i) 
{
  if(!this.tabs[this.selectedIndex].onSwitchOut())
    return;
  
  var prev= this.selectedIndex;
  this.tabs[this.selectedIndex].selected=false;
  this.tabs[i].selected=true;
  this.selectedIndex=i;

   // bug 1040140

   if(IS_IE4) 
   {
       findFrame(top, "por_top").location.reload();
   }
   else
   {
      findFrame(top, "por_top").document.open();
   // findFrame(top, "por_top").document.clear();
      currentIndex.draw(findFrame(top,"por_top").document);
      findFrame(top, "por_top").document.close();

   }
   // else 
   //{
   //  findFrame(top, "por_top").location.reload();
   //}
  
  this.tabs[i].onSwitchIn();
}
/* functions for drawing buttons -- to be removed  
function Button(t, l, rl, rr)
{
  this.text=t;
  this.link=l;
  this.roundLeft=rl;
  this.roundRight=rr;
  this.draw=new Function("doc", "drawButton(this.text,this.link,this.roundLeft,this.roundRight,doc)");
}

function ButtonPanel(t, b)
{
  this.topB=t;
  this.bottom=b;
  this.draw= drawTopPanel;
}

function drawTopPanel(doc)
{
  var str;
  doc.write(
//    "<LINK REL=STYLESHEET HREF='PORSTYLE.css' TYPE='text/css'>"+

    "<html> <LINK REL=STYLESHEET HREF=/OA_HTML/" + windows.top.LANGUAGE_CODE + "/PORSTYLE.css TYPE="text/css">"+
    "<body bgcolor=#336699>"+
    "<table bgcolor=#336699 width=100% cellpadding=0 cellspacing=0 border=0> <tr>"+
    "<td rowspan=3><img src=/OA_MEDIA/FNDCGBL.gif></td>" +
    "<td colspan=4 "+ 
    " bgcolor=#cccccc height=5 width=1000><img src=/OA_MEDIA/FNDPXG5.gif height=2></td>"+
    "<td rowspan=3><img src=/OA_MEDIA/FNDCGBR.gif></td></tr><tr>" +
    "<td bgcolor=#cccccc nowrap><img src=/OA_MEDIA/FNDPXG5.gif height=33 width=1></td>"+
    "<td bgcolor=#cccccc>"+
    "<table cellpadding=0 cellspacing=0 border=0><tr>");
  for(var i=0; i<this.topB.length; i++)
  {
    doc.write("<td>");
    this.topB[i].draw(doc);
    doc.write("</td><td width=2 rowspan=5></td>");
  }
  doc.write("</tr></table>");
  str="</td><td bgcolor=#cccccc align=right nowrap>"+
    "<font class=promptblack>Total Lines Added: </font>"+
    "<font class=datablack>4&nbsp;&nbsp;&nbsp;</font>"+
    "<font class=promptblack>Requisition Total: </font>"+
    "</td>"+
    "<td bgcolor=#cccccc><img name='spaceHolder' align=top src=/OA_MEDIA/FNDPX3.gif>"+
    "<font color=cccccc> __________</font></td>"+
    "</tr><tr>"+
    "<td colspan=4 bgcolor=#cccccc><img src=/OA_MEDIA/FNDPXG5.gif height=1></td>"+
    "</tr></TABLE>"+
    "<TABLE width=100% cellpadding=0 cellspacing=0 border=0>"+
    "<TR><td height=3></td></TR>"+
    "<TR>"+
    "<TD align=right>"+
    '<div id="help" style="position:absolute; left:0; top:0; background-color:yellow; visibility:show">'+
    "help div."+
    "</div>"+
    '<div id="flowing" style="position:absolute; left:0; top:0; visibility:show">'+
    'flowing Div'+
    '</div>'+
    "<table cellpadding=0 cellspacing=0 border=0> <tr>";
  doc.write(str);
  
    for(var i=0; i<this.bottom.length; i++)
    {
      doc.write("<td>");
      this.bottom[i].draw(doc);
      doc.write("</td><td width=2 rowspan=5></td>");
    }
  doc.write("</tr> </table></td></tr></table></body></html>");
}

function drawButton(text, link, roundLeft, roundRight, doc)
{
  var leftImg= (roundLeft ? "FNDBRNDL.gif" : "FNDBSQRL.gif");
  var rightImg= (roundRight ? "FNDBRNDR.gif" : "FNDBSQRR.gif");
  
  var str=
    "<table cellpadding=0 cellspacing=0 border=0> <tr>" +
    "<td rowspan=5> <img src=" +"/OA_MEDIA"+ leftImg + "></td>" +
    "<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif></td>" +
    "<td rowspan=5> <img src=" +"/OA_MEDIA"+ rightImg + "></td></tr>" +
    "<tr><td bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td></tr>" +
    "<tr><td bgcolor=#cccccc height=20 nowrap>" +
    "<a "+ link + ">"+
    "<font class=button>" +
    text + "</font></td>" + 
    "</a>" +
    "</tr>" +
    "<tr><td bgcolor=#666666><img src=/OA_MEDIA/FNDPX3.gif></td></tr>" +
    "<tr><td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif></td>" +
    "</tr> </table>";
  doc.write(str);
}

function position() 
{
  if(IS_IE)
    return;
  
  var lyr=getLayer("flowing");
  var img,x,y;

  currentLayer= 
    (currentIndex.selectedIndex==0) ? 
    "lines_buttons"+suffix : "header_buttons" +suffix;
  
  img= 
    getLayer(currentLayer).document.images.spaceHolder; 
  x=img.x;
  y=img.y;
  
  setBgcolor(lyr, "#cccccc");
  moveObject(lyr, x,y);
  lyr.zIndex=2;
  show(lyr);
}
*/
// sets the total line in edit Req
function setTotal()
{
  var lines= allForms["POR_LINES_R"];
  if(lines)
  {
    var total=0;
    for(var i=0; i<lines.length; i++)
    {  
      total+= parseFloat(lines[i].POR_EXTENDED_PRICE.value);
    }
    setLineCount(lines.length);
    setReqTotal(total);

    // Bug 976316, IE5 tries to update the total and repaint the page at the same time
    // but findFrame and getLayer would fail since the frame doesn't exist anymore
    // we check to see if submit is in process, if so, we dont even bother updating the total
    if (top.getTop().submitInProcess) return;

    position("por_bottom");
  }
}

function resetForms(formName)
{
  var forms= allForms[formName];
  if(forms)
  {
    for(var i=0; i<forms.length; i++)
    {
      forms[i].reset();
      forms[i].updated=false;
    }
  }
}

function getLineCount()
{
//  var f = top.getTop().allForms["POR_MESSAGE_FORM"];
//  return (f ? f[0].POR_TOTAL_LINES.value : 0);
  return (top.lineCount) ? top.lineCount : 0;
}

function getReqTotal()
{
//  var f = top.getTop().allForms["POR_MESSAGE_FORM"];
//  return (f ? f[0].POR_REQ_TOTAL.value : 0);
    return (top.reqTotal) ? top.reqTotal : 0;
}
  
function setLineCount(lc)
{
//  var f = top.getTop().allForms["POR_MESSAGE_FORM"];
//  if (f) f[0].POR_TOTAL_LINES.value= lc;
 top.lineCount=lc;
 // top.noSave= (lc==0 && top.reqHeaderI==0);
}

function setReqTotal(rt)
{
  //  var f = top.getTop().allForms["POR_MESSAGE_FORM"];
  //  if (f) f[0].POR_REQ_TOTAL.value= rt;
 top.reqTotal=rt;
}

function copyForm(form1, form2)
{
  for(var i=0; i<form1.elements.length; i++)
  {
    var elem1=form1.elements[i];
    var elem2=form2.elements[elem1.name];    
    if(elem2)
    {
      elem2.value=elem1.value;
    }
  }
}


// The following functions are used to resize the window to maximum and re-center the window
function openMaxWindow()
{
	if (IS_NAV) {
		moveWindowToCenter();
	}
	if (IS_IE) {
	 	if (screen.availWidth < 800 || screen.availHeight < 600) {
		        window.moveTo(0,0);
			window.resizeTo(screen.availWidth, screen.availHeight);
		} else {
			moveWindowToCenter();
		}
	}

}

function moveWindowToCenter () {
	var posX = (screen.availWidth-800)/2; 
	var posY = (screen.availHeight-600)/2;
        window.moveTo(posX, posY);
}
