//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtsumm.js                       |
//  |                                                |
//  | Description:                                   |
//  |                                                |
//  | History:                                       |
//  |                                                |
//  +================================================+
/* $Header: pawtsumm.js 115.17 2001/03/23 17:40:50 pkm ship      $ */

// Local Constants

var C_strTYPE               = 'TYPE';

var C_iNAME     = 0;
var C_iHOURS    = 1;
var C_iROWS     = 2;
var C_iSUBARRAY = 3;
var C_iDAYSARRAY = 1;
var C_iINVALID  = -1;
var C_strNOTHING = '&nbsp';
var C_strEMPTYSTRING = "";
var C_strEMPTYVALUE  = "--";

//  ---------------------------------------------------------------------------
//  Function: fFormatHours
//  Description:   
//
//  ---------------------------------------------------------------------------
function fFormatHours(p_strInput){
var undefined;

 if (p_strInput == undefined) 
  return C_strNOTHING;
 else
   if (p_strInput == 0)
     return '0.00';
   else
     return fFormatDecimals(p_strInput); 
}

//  ---------------------------------------------------------------------------
//  Function: fFormatEmployee
//  Description:   
//
//  ---------------------------------------------------------------------------
function fFormatEmployee(p_objEmployee){
var undefined;

  if ((p_objEmployee.mGetstrName() == "") ||
     (p_objEmployee.mGetstrName() == undefined))
    return C_strNOTHING; 
  else
    return p_objEmployee.mGetstrName() + ' (' + p_objEmployee.mGetiNumber() +  ')';
}

//  ---------------------------------------------------------------------------
//  Function: fFormatString
//  Description:   
//
//  ---------------------------------------------------------------------------
function fFormatString(p_strString){
  if (p_strString == "") 
    return C_strNOTHING;
  else
    return p_strString;
}

//  ---------------------------------------------------------------------------
//  Function: fFormatString
//  Description:   
//
//  ---------------------------------------------------------------------------
function fCheckNullString(p_strString){
  if (p_strString == C_strEMPTYSTRING) 
    return C_strEMPTYVALUE;
  else
    return p_strString;
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fGetRegionPrompt(p_objRegion, p_strRegion_item) {
var l_str;
 l_str = p_objRegion.mobjGetRegionItem(p_strRegion_item).mGetstrLabel();
 return l_str;
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fGetProfileOption(p_strProfOptName) {
 if (top.g_objProfOpt.mvGetProfOpt(p_strProfOptName) == 'Y') 
   return true;
 else
   return false;
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fOnClickReturnToInquiry(p_strMode) {
  window.close();
}

function fOnClickEditTimecard() {
  open(top.g_strSessBaseHRef+top.g_strSessDAD+'/PA_SELF_SERVICE_INQUIRIES_PVT.EditTimecard?p_expenditure_id='+top.g_objTimecard.mGetobjHeader().mGetiExpenditureID()+'&p_sIsFromInquiry=N', "_top")
}
  
//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fonClickSaveSubmitOrFinalReview(
		p_objTimecard, 
		p_objForm, 
		p_strFromPage,
		p_strAction,
                p_objAuditHistory) {

  p_objForm.hidTCHeader.value = p_objTimecard.mstrSerializeHeader();
  p_objForm.hidTCLines.value  = p_objTimecard.mstrSerializeLines();
  p_objForm.hidAuditHistory.value = p_objAuditHistory.mstrSerializeAuditHistory();
  p_objForm.hidFromPage.value = p_strFromPage;
  p_objForm.hidAction.value   = p_strAction;

  if ((p_strAction == top.C_strSUBMIT) && 
      (top.g_boolMiddleOfSSF == false)) {
    if (confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_CONFIRM_SUBMIT"))){
      top.g_boolMiddleOfSSF = true;
      p_objForm.submit();
    }
    else
      return;
 }
 else
   if (top.g_boolMiddleOfSSF == false) {
     top.g_boolMiddleOfSSF = true;
     p_objForm.submit();
   }
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fControlSSToCSContinue(
		p_objTimecard, 
		p_objErrors, 
		p_strMode,
		p_strAction,
                p_objAuditHistory,
                p_objDeletedAuditHistory) {

// If errors are present, draw Error Window and Header/Lines Page
  if (p_objErrors.arrErrors.length > 0) { 
      top.fDrawTimeEntry();  
      top.fOpenErrorWin(p_objErrors, 'top.framMainBody.framBodyHeader.document.formEntryBodyHeader', 'top.framMainBody.framBodyContent.document.formEntryBodyContent');
  }  
  else if ( p_strMode == top.C_strAUDIT ) { // Draw Audit Information screen
    fDrawAuditInfo();
  }
  else {
      // Close Error window if opened earlier
      fCloseErrorWindow();
      // Repaint the different frames
      fDrawToolbarFrame(top.framToolbar, p_strMode);
      fDrawButtonFrame(top.framButtons, p_strMode, p_strAction);
      fDrawSummaryFrame(top.framMainBody, p_objTimecard, p_strMode, 
        p_objAuditHistory, p_objDeletedAuditHistory); 
  } 
  top.g_boolMiddleOfSSF = false;
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawSummaryFrame(
		p_framTarget,
		p_objTimecard,
		p_strMode, p_objAuditHistory, p_objDeletedAuditHistory) {

 var l_strClass;

 if ((p_strMode==top.C_strSAVE) || (p_strMode==top.C_strSUBMIT)) {
   l_strClass = 'printwindow';
 }
 else if ((p_strMode==top.C_strFINAL_REVIEW) || (p_strMode==top.C_strVIEW_ONLY)) {
   l_strClass = 'panel';
 }
 p_framTarget.document.open();
 p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
 p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
 p_framTarget.document.writeln('<HEAD>');
 p_framTarget.document.writeln('<LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></LINK>');
 p_framTarget.document.writeln('<LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawstime.css"></LINK></HEAD>');

 p_framTarget.document.writeln('<BODY class=panel>');
 p_framTarget.document.writeln('<TABLE class=panel CELLPADDING=5 BORDER=0 CELLSPACING=0 WIDTH="100%"><TR><TD ALIGN="MIDDLE">');
 p_framTarget.document.writeln('<TABLE CLASS=' + l_strClass + ' CELLPADDING=5 BORDER=0 CELLSPACING=0 WIDTH="100%">');

 if ((p_strMode==top.C_strSAVE) || (p_strMode==top.C_strSUBMIT)) {
   fDrawConfirmHeaderSummary(
	p_framTarget, 
	p_objTimecard.mGetnTotalHours(),
     	p_objTimecard.mGetobjHeader(), 
	p_strMode); 
 }
 else if ((p_strMode==top.C_strFINAL_REVIEW) || (p_strMode==top.C_strVIEW_ONLY)) {
   fDrawReviewHeaderSummary(
	p_framTarget, 
	p_objTimecard.mGetnTotalHours(),
     	p_objTimecard.mGetobjHeader(), 
	p_strMode); 
 }
 fDrawLinesSummary(
	p_framTarget, 
	p_objTimecard.objLines,
	p_strMode);

 fDrawAuditSummary(
        p_framTarget,
        p_objAuditHistory, 
        p_objDeletedAuditHistory);

  p_framTarget.document.writeln('</TABLE>');
  p_framTarget.document.writeln('</TD></TR></TABLE>');

  if (p_strMode==top.C_strFINAL_REVIEW) {  // Form only in Final Review
    p_framTarget.document.writeln('<FORM NAME="formSaveSubmitFReview" METHOD=post ACTION="' + top.g_strSessBaseHRef + top.g_strSessDAD  + '/PA_SELF_SERVICE_HANDLER_PVT.From_SaveSubmitFReview"> ');

    p_framTarget.document.writeln('<INPUT TYPE="hidden" NAME="hidTCHeader"> ');
    p_framTarget.document.writeln('<INPUT TYPE="hidden" NAME="hidTCLines"> ');
    p_framTarget.document.writeln('<INPUT TYPE="hidden" NAME="hidAuditHistory"> ');
    p_framTarget.document.writeln('<INPUT TYPE="hidden" NAME="hidFromPage"> ');
    p_framTarget.document.writeln('<INPUT TYPE="hidden" NAME="hidAction"> ');
    p_framTarget.document.writeln('</FORM>');
  }

  p_framTarget.document.writeln('</BODY></HTML>');
  p_framTarget.document.close();
}

// Functions needed to render the header region
//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawHeaderLine(
		p_framTarget,   
		p_nPairs,   //Is it a 3 pair line header (VIEW_ONLY) or 2 pair line header
		p_strPair1Prompt,
		p_strPair1Value,
		p_strPair2Prompt,
		p_strPair2Value,
		p_strPair3Prompt,
		p_strPair3Value) {
  
  var C_nSINGLE_ON_DOUBLE_PAIR = 3;     // one pair on a two pair line
  var C_nDOUBLE_ON_DOUBLE_PAIR = 1;     // two pairs on a two pair line
  var C_nSINGLE_ON_TRIPLE_PAIR = 5;     // two pairs on a two pair line
  var C_nDOUBLE_ON_TRIPLE_PAIR = 3;     // two pairs on a two pair line
  var C_nTRIPLE_ON_TRIPLE_PAIR = 1;     // two pairs on a two pair line

  var l_nColSpan;

  p_framTarget.document.writeln('<TR>');

  if (p_nPairs==2) {
    if (p_strPair2Prompt == null) 
      l_nColSpan= C_nSINGLE_ON_DOUBLE_PAIR;
    else
      l_nColSpan= C_nDOUBLE_ON_DOUBLE_PAIR;
  
    p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair1Prompt  + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap   COLSPAN=' +l_nColSpan+'>'+ p_strPair1Value + '</TD>');
    if (p_strPair2Prompt != null) {
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair2Prompt + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap  >'+   p_strPair2Value  + '</TD>');
    }
  }
  else if (p_nPairs==3) { 

	//Handle following cases 
	// 1. All 3 pairs available
	// 2. Only left and middle pair available
	// 3. Only left pair available

    if (p_strPair1Prompt != null && p_strPair2Prompt!=null && p_strPair3Prompt!=null) {
      l_nColSpan= C_nSINGLE_ON_TRIPLE_PAIR;
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair1Prompt  + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap  >'  + p_strPair1Value + '</TD>');
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair2Prompt + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap  >'+   p_strPair2Value  + '</TD>');
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair3Prompt + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap  >'+   p_strPair3Value  + '</TD>');
    }
    else if (p_strPair1Prompt != null && p_strPair2Prompt!=null && p_strPair3Prompt==null) {
      l_nColSpan= C_nDOUBLE_ON_TRIPLE_PAIR;
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair1Prompt  + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap  >'  + p_strPair1Value + '</TD>');
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair2Prompt  + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap   COLSPAN=' +l_nColSpan+'>'+ p_strPair2Value + '</TD>');
    }
    else if (p_strPair1Prompt != null && p_strPair2Prompt==null && p_strPair3Prompt==null) {
      l_nColSpan= C_nSINGLE_ON_TRIPLE_PAIR;
      p_framTarget.document.writeln(
	  '<TD CLASS=PROMPTBLACKRIGHT nowrap >' + p_strPair1Prompt  + '</TD>' +
	  '<TD CLASS=DATABLACK   nowrap   COLSPAN=' +l_nColSpan+'>'+ p_strPair1Value + '</TD>');
    }
  }  //End 3 pair line

  p_framTarget.document.writeln('</TR>');
}



//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawApprAndBMesg(
	p_framTarget, 
	p_objHeader,
	p_nPairs, 
	p_strMode) {

  var l_bDisplayOApprover;    	//Display in all pages if 
			   	//profile option is set (regardless of value)
  var l_bDisplayBusinessMesg;  	//Display in all pages EXCEPT SAVE 
			   	//if profile option is set (regardless of value)
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");


  //Determine if Approver and Business Message willl be displayed
  if (p_strMode == top.C_strSAVE) { 
    l_bDisplayBusinessMesg 	= false; 
  }
  else { 
    l_bDisplayBusinessMesg 	= fGetProfileOption('PA_SST_ENABLE_BUS_MSG'); 
  }
  l_bDisplayOApprover = fGetProfileOption('PA_ONLINE_OVERRIDE_APPROVER'); // Overriding approver flag

// Approver and / or Business Message
  if (l_bDisplayOApprover  && l_bDisplayBusinessMesg) {
    fDrawHeaderLine(p_framTarget,
	p_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_OVERRIDING_APPROVER'),
	fFormatEmployee(p_objHeader.mGetobjApprover()),
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_BUSINESS_MESSAGE'),
	fFormatString(p_objHeader.mGetstrBusinessMesg()));
  }
  else if (l_bDisplayOApprover){
    fDrawHeaderLine(p_framTarget,
	p_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_OVERRIDING_APPROVER'),
	fFormatEmployee(p_objHeader.mGetobjApprover()));
  }
  else if (l_bDisplayBusinessMesg) {
    fDrawHeaderLine(p_framTarget,
	p_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_BUSINESS_MESSAGE'),
	fFormatString(p_objHeader.mGetstrBusinessMesg()));
  }
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawConfirmInstructions(
		p_framTarget, 
		l_strGif, 
		l_strInstructions){
  p_framTarget.document.writeln('<TR><TD><TABLE class=redborder CELLPADDING=1 CELLSPACING=1 BORDER=0 WIDTH="100%">');
  p_framTarget.document.writeln('<TR><TD><TABLE class=panel CELLSPACING=0 CELLPADDING=0 BORDER=0 WIDTH="100%">');

  p_framTarget.document.writeln('<TR><TD CLASS=panel><TABLE BORDER=0 WIDTH="100%">');
  p_framTarget.document.writeln('<TR><TD nowrap height=30 align=middle>'+
					'<IMG src='+g_strImagePath+l_strGif+'  align=middle border=0></TD>');
  p_framTarget.document.writeln('    <TD  CLASS=INFOBLACK><B>' + l_strInstructions + '</B></TD></TR>');
  p_framTarget.document.writeln('</TABLE></TD></TR>');

  p_framTarget.document.writeln('</TABLE></TD></TR>');
  p_framTarget.document.writeln('</TABLE></TD></TR>');
}

//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawConfirmHeaderSummary(
		p_framTarget,
		p_nTotalHours,
		p_objHeader,
		p_strMode) {

  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");
  var l_strHeaderTitle;   // "CONFIRMATION" or "SAVED TIMECARD"
  var l_strUpdateDatePrompt;  // "Submit Date" or "Save Date"
  var l_strUpdatedByPrompt;   // "Submitted By" or "Saved By"
  var l_strInstructions;
  var l_strGif;
  var l_nPairs = 2;

  // The following values are different between the 2 confirmation screens
  if (p_strMode == top.C_strSAVE) { 
    l_strHeaderTitle 		= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SAVE_CONF_TITLE');
    l_strUpdateDatePrompt 	= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SAVE_DATE');
    l_strUpdatedByPrompt 	= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SAVED_BY');
    l_strInstructions		= top.g_objFNDMsg.mstrGetMsg('PA_WEB_SAVE_CONF_INSTRUCTION');
    l_strGif 			= "PAISVCF.gif";
  }
  else if (p_strMode == top.C_strSUBMIT) { 
    l_strHeaderTitle 		= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SUBMIT_CONF_TITLE');
    l_strUpdateDatePrompt 	= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SUBMIT_DATE');
    l_strUpdatedByPrompt 	= fGetRegionPrompt(l_objCurRegion, 'PA_WEB_SUBMITTED_BY');
    l_strInstructions		= fstrReplToken(top.g_objFNDMsg.mstrGetMsg('PA_WEB_SUBMIT_CONF_INSTRUCTION'), "&TC_NUMBER",p_objHeader.mGetstrTimecardNum());
    l_strGif 			= "PAISBCF.gif";
  }

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH="100%">');
  // "Oracle Time" and "SAVED TIMECARD" / "CONFIRMATION"
  p_framTarget.document.writeln('<TR><TD  CLASS=INFOBLACK><B>  ' + 
				fGetRegionPrompt(l_objCurRegion, 'PA_WEB_CONFIRM_TITLE') + '</B></TD>');
  p_framTarget.document.writeln('<TD CLASS=INFOBLACKRIGHT><B>  ' + 
				l_strHeaderTitle + '</B></TD></TR>');

  //Timecard Number and Update Date
  p_framTarget.document.writeln('<TR><TD CLASS=PROMPTBLACK nowrap  >' +
				fGetRegionPrompt(l_objCurRegion, 'PA_WEB_TIMECARD_NUMBER') + '<B>  ' +  
				p_objHeader.mGetstrTimecardNum() + '</B></TD>');

  p_framTarget.document.writeln('<TD CLASS=PROMPTBLACKRIGHT nowrap >' +
				l_strUpdateDatePrompt + '<B>  ' + 
				fDateToLongString(top.g_strSessDateFormat, p_objHeader.mGetdLastUpdateDate(),true) + '</B></TD></TR>');

  // Employee and Updated By
  p_framTarget.document.writeln('<TR><TD CLASS=PROMPTBLACK nowrap >' +
				fGetRegionPrompt(l_objCurRegion, 'PA_WEB_EMPLOYEE')  + '<B>  ' +
				fFormatEmployee(p_objHeader.mGetobjEmployee()) + '</B></TD>');

  p_framTarget.document.writeln('<TD CLASS=PROMPTBLACKRIGHT nowrap >' +
				l_strUpdatedByPrompt  + '<B>  ' +
				fFormatEmployee(p_objHeader.mGetobjLastUpdatedBy()) + '</B></TD></TR>');

  // Week Ending
  p_framTarget.document.writeln('<TR><TD CLASS=PROMPTBLACK nowrap  >' +
				fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_ENDING')  + '<B>  ' +
				fDateToLongString(top.g_strSessDateFormat, p_objHeader.mGetdWeekEndingDate(),true) +'</B></TD></TR>');
  p_framTarget.document.writeln('</TABLE></TD></TR>');

  fDrawConfirmInstructions(p_framTarget, l_strGif, l_strInstructions);
 
  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0 WIDTH="100%">');
// Comment , Week Total ( and Timecard Number for VIEW ONLY)
    fDrawHeaderLine(p_framTarget,
	l_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_HEADER_COMMENT'),
	fFormatString(p_objHeader.mGetstrComment()),
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_TOTAL'),
	fFormatHours(p_nTotalHours));
  fDrawApprAndBMesg(p_framTarget, p_objHeader, l_nPairs, p_strMode);
  p_framTarget.document.writeln('     </TABLE></TD></TR>');
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawReviewHeaderSummary(
		p_framTarget,
		p_nTotalHours,
		p_objHeader,
		p_strMode) {

  var l_nPairs;
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0 WIDTH="100%">');

  if (p_strMode == top.C_strFINAL_REVIEW) { 
    l_nPairs = 2;  //Pairs per line in the header region
  }
  else { 
    l_nPairs = 3; //Pairs per line in the header region
  }

// Employee, Week Ending (and Timecard Status for VIEW ONLY)
    fDrawHeaderLine(p_framTarget,
	l_nPairs,
	fGetRegionPrompt(l_objCurRegion, 'PA_WEB_EMPLOYEE'),
	fFormatEmployee(p_objHeader.mGetobjEmployee()),
	fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_ENDING'),
	fDateToLongString(top.g_strSessDateFormat, p_objHeader.mGetdWeekEndingDate(),true),
	fGetRegionPrompt(l_objCurRegion, 'PA_WEB_TIMECARD_STATUS'),
	p_objHeader.mGetstrStatus());

// Comment , Week Total ( and Timecard Number for VIEW ONLY)
    fDrawHeaderLine(p_framTarget,
	l_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_HEADER_COMMENT'),
	fFormatString(p_objHeader.mGetstrComment()),
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_TOTAL'),
	fFormatHours(p_nTotalHours),
	fGetRegionPrompt(l_objCurRegion, 'PA_WEB_TIMECARD_NUMBER'),
	p_objHeader.mGetstrTimecardNum());

// Approver and / or Business Message
  fDrawApprAndBMesg(p_framTarget, p_objHeader, l_nPairs, p_strMode);

  p_framTarget.document.writeln('<TR><TD COLSPAN=' + eval(l_nPairs*2) + '><HR></TD></TR>');
  // Draw Instructions for Review Page
  if (p_strMode == top.C_strFINAL_REVIEW) {
    p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK COLSPAN=' + eval(l_nPairs*2) + '>' + 
		top.g_objFNDMsg.mstrGetMsg('PA_WEB_REVIEW_INSTRUCTION')  + '</TD></TR>');
  }
  else if (p_strMode == top.C_strREVERSE) {
    p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK COLSPAN=' + eval(l_nPairs*2) + '>' +
                top.g_objFNDMsg.mstrGetMsg('PA_WEB_REVERSAL_PROMPT')  + '</TD></TR>');
  }

  p_framTarget.document.writeln('     </TABLE></TD></TR>');

}



// Functions needed to render the lines region
//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fFindElementIndex(p_arrArray, p_strName) {
var i;
  for (i=0; i<p_arrArray.length; i++)
    if (p_arrArray[i][C_iNAME] == p_strName)
      return i;
  return -1;
}

//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fAddElementToArray(p_arrArray, p_strName, p_strType) {
  var l_iIndex;
  l_iIndex = p_arrArray.length;
  if (p_strType == C_strTYPE) {
    p_arrArray[l_iIndex] = new Array (p_strName, new Array(7)); 
     
  }  
  else
    p_arrArray[l_iIndex] = new Array (p_strName, 0, 0, new Array());
  return l_iIndex;

}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fCaptureDataForSummaryByDay(p_objLines, p_arrstrTypes) {

var l_iIndex;
var l_strType;
var i;
var j;

  for (i=0; i<p_objLines.arrLine.length; i++) {
    if ((p_objLines.arrLine[i].bIsEmpty == false) &&
	(p_objLines.arrLine[i].bIsDeleted == false) ) {
      l_strType = fCheckNullString(p_objLines.arrLine[i].mGetobjType().mGetstrName());
      l_iIndex = fFindElementIndex(p_arrstrTypes, l_strType);
      if (l_iIndex == C_iINVALID) {
        l_iIndex = fAddElementToArray(p_arrstrTypes, l_strType, C_strTYPE);
      }
      for (j=0; j<C_iWEEKDAYS; j++) {
        if (p_objLines.arrLine[i].mGetobjDays().mobjGetDay(j).mGetnHours() != "") {
          if (p_arrstrTypes[l_iIndex][C_iDAYSARRAY][j] == null) 
            p_arrstrTypes[l_iIndex][C_iDAYSARRAY][j] = 0;
          p_arrstrTypes[l_iIndex][C_iDAYSARRAY][j] += eval(p_objLines.arrLine[i].mGetobjDays().mobjGetDay(j).mGetnHours());
        }
      } //end Days loop
    } //End If not empty line
  } //End Line loop

}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fPrintSummaryByDay(p_framTarget, p_arrstrTypes) {

  var l_arrnWeekTotal		= new Array();
  var l_arrnDayTotal		= new Array();
  var l_nGrandTotal = 0;
  var i;
  var j;
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");

  for (j=0; j<C_iWEEKDAYS; j++)
    l_arrnDayTotal[j] =0;

  for (i=0; i< p_arrstrTypes.length ; i++) {
    l_arrnWeekTotal[i] = 0;

//  Draw Entries in table
    p_framTarget.document.writeln(
	'<TR><TD CLASS=TABLECELL>' + p_arrstrTypes[i][C_iNAME]  + '</TD>');  

    for (j=0; j<g_dWeekEndingWeek.length; j++) {

      p_framTarget.document.writeln(
	'<TD CLASS=TABLECELLRIGHT>' + fFormatHours(p_arrstrTypes[i][C_iDAYSARRAY][top.g_dWeekEndingWeek[j].getDay()]) + '</TD>');        

      if (p_arrstrTypes[i][C_iDAYSARRAY][top.g_dWeekEndingWeek[j].getDay()] != null) {
        l_arrnWeekTotal[i]     += p_arrstrTypes[i][C_iDAYSARRAY][top.g_dWeekEndingWeek[j].getDay()];
        l_arrnDayTotal[top.g_dWeekEndingWeek[j].getDay()]      += p_arrstrTypes[i][C_iDAYSARRAY][top.g_dWeekEndingWeek[j].getDay()];
      }
    }

    p_framTarget.document.writeln(
	'<TD CLASS=TABLECELLRIGHT>' + fFormatHours(l_arrnWeekTotal[i]) + '</TD></TR>');  
  }

  p_framTarget.document.writeln('<TR><TD height=1 colspan=9 class=color6><IMG src=' + g_strImagePath + 'FNDPX6.gif></TD></TR>');
// Draw last line in table - total
  p_framTarget.document.writeln(
	'<TR><TD CLASS=TABLECELL>' +
	'<B>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_LINE_TOTAL') + '</B></TD>');  

  for (j=0; j<g_dWeekEndingWeek.length; j++) {
    p_framTarget.document.writeln('<TD CLASS=TABLECELLRIGHT>' + fFormatHours(l_arrnDayTotal[top.g_dWeekEndingWeek[j].getDay()]) + '</TD>');
    l_nGrandTotal += l_arrnDayTotal[top.g_dWeekEndingWeek[j].getDay()];
  }
  p_framTarget.document.writeln('<TD CLASS=TABLECELLRIGHT>' + fFormatHours(l_nGrandTotal) + '</TD></TR>');  

}



//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawSummaryByDay(p_framTarget,p_objLines) {

  var l_arrstrTypes 		= new Array();
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");
  var j;

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 BORDER=0  CELLSPACING=0 width="100%">');
  fDrawTableLabel(p_framTarget, fGetRegionPrompt(l_objCurRegion,'PA_WEB_SUMMARY_BY_DAY'));

  p_framTarget.document.writeln('   <TR><TD>');
  fDrawTableTopBorder(p_framTarget, "LEFT")  // draw table with align=left

  p_framTarget.document.writeln('<TABLE cellpadding=2 cellspacing=1 border=0 WIDTH=100%>');
  p_framTarget.document.writeln( '<TR>' +
	    '<TD CLASS=TABLEROWHEADER>&nbsp</TD>');
  for (var i=0; i<g_dWeekEndingWeek.length; i++){
    p_framTarget.document.writeln('<TD CLASS=TABLEROWHEADER>' + fIntToDayOfWeek(top.g_dWeekEndingWeek[i].getDay()) + '</TD>' );
  }
  p_framTarget.document.writeln( '<TD CLASS=TABLEROWHEADER>&nbsp</TD>');
  p_framTarget.document.writeln( '</TR>');
  p_framTarget.document.writeln( '<TR><TD CLASS=TABLEROWHEADER>' + 
		fGetRegionPrompt(l_objCurRegion,'PA_WEB_EXP_TYPE') + '</TD>' );
  for (var i=0; i<g_dWeekEndingWeek.length; i++){
    p_framTarget.document.writeln('<TD CLASS=TABLEROWHEADERLIGHT>' + fDateToLongString(top.g_strSessDateFormat, top.g_dWeekEndingWeek[i], true) + '</TD>' );
  }
  p_framTarget.document.writeln('<TD CLASS=TABLEROWHEADER>' + 
		fGetRegionPrompt(l_objCurRegion,'PA_WEB_LINE_TOTAL') + '</TD></TR>');


  fCaptureDataForSummaryByDay(p_objLines, l_arrstrTypes);
  fPrintSummaryByDay(p_framTarget, l_arrstrTypes);  

  p_framTarget.document.writeln('</TABLE>');
  fDrawTableBottomBorder(p_framTarget);
  p_framTarget.document.writeln('   </TD></TR>');
  p_framTarget.document.writeln('</TABLE></TD></TR>');

}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fCaptureDataForSummaryByProject(p_objLines, p_arrSummary) {
  
  var l_strProject;
  var l_strTask;
  var l_strType;
  var l_iProject;
  var l_iTask;
  var l_iType;
  var l_nHours = 0;
  var i;
  var j;

  for (i=0; i<p_objLines.arrLine.length; i++) {

    if ((p_objLines.arrLine[i].bIsEmpty == false) &&
	(p_objLines.arrLine[i].bIsDeleted == false) ) {

    l_strProject = fCheckNullString(p_objLines.arrLine[i].mGetstrProjectName()) + ' / ' +
		   fCheckNullString(p_objLines.arrLine[i].mGetstrProjectNum());
    l_strTask    = fCheckNullString(p_objLines.arrLine[i].mGetstrTaskName()) + ' / ' +
		   fCheckNullString(p_objLines.arrLine[i].mGetstrTaskNum());
    l_strType    = fCheckNullString(p_objLines.arrLine[i].mGetobjType().mGetstrName());
    l_nHours = 0;
    for (j=0; j<C_iWEEKDAYS; j++) {
      if (p_objLines.arrLine[i].mGetobjDays().mobjGetDay(j).mGetnHours() != "") 
        l_nHours     += eval(p_objLines.arrLine[i].mGetobjDays().mobjGetDay(j).mGetnHours());
    }
//    l_nHours     = p_objLines.arrLine[i].mGetnWeekTotal();

    l_iProj = fFindElementIndex(p_arrSummary, l_strProject);
    if (l_iProj == C_iINVALID) {
      l_iProj = fAddElementToArray(p_arrSummary, l_strProject);
    }

    l_iTask = fFindElementIndex(p_arrSummary[l_iProj][C_iSUBARRAY], l_strTask);
    if (l_iTask == C_iINVALID) {
      l_iTask = fAddElementToArray(p_arrSummary[l_iProj][C_iSUBARRAY], l_strTask);
    }

    l_iType = fFindElementIndex(p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY], l_strType);
    if (l_iType == C_iINVALID) {
      l_iType = fAddElementToArray(p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY], l_strType);
      p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY][l_iType][C_iROWS]++;
      p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iROWS]++;
      p_arrSummary[l_iProj][C_iROWS]++;
    }

    if (l_nHours != "") { 
      if (p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY][l_iType][C_iHOURS] == null) 
        p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY][l_iType][C_iHOURS] = 0;
      if (p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iHOURS] == null) 
        p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iHOURS] = 0;
      if (p_arrSummary[l_iProj][C_iHOURS] == null) 
        p_arrSummary[l_iProj][C_iHOURS] = 0;
      
      p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iSUBARRAY][l_iType][C_iHOURS] += l_nHours;
      p_arrSummary[l_iProj][C_iSUBARRAY][l_iTask][C_iHOURS] 			  += l_nHours;
      p_arrSummary[l_iProj][C_iHOURS] 						  += l_nHours;  
    }
   } //End If
  } //End For Loop

}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fPrintLineForProjectSummary(p_framTarget, p_strRowSpan, p_strName, p_strValue) {   
  p_framTarget.document.writeln('<TD CLASS=TABLECELL ' + 'ROWSPAN="' + p_strRowSpan + '"   valign="TOP">' + p_strName + '</TD>');
  p_framTarget.document.writeln('<TD CLASS=TABLECELLRIGHT ' + 'ROWSPAN="' + p_strRowSpan + '"  valign="TOP">' + fFormatHours(p_strValue) + '</TD>');  
}


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawSummaryByProject(
                p_framTarget,
		p_objLines) {

  var l_arrSummary = new Array();
  var i;
  var j;
  var k;
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 BORDER=0  CELLSPACING=0 width="100%">');
  fDrawTableLabel(p_framTarget, fGetRegionPrompt(l_objCurRegion,'PA_WEB_SUMMARY_BY_PROJECT'));
  p_framTarget.document.writeln('   <TR><TD>');
  fDrawTableTopBorder(p_framTarget, "LEFT")  // draw table with align=left

  p_framTarget.document.writeln('<TABLE cellpadding=2 cellspacing=1 border=0 WIDTH=100%>');
  p_framTarget.document.writeln('<TR>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_PROJECT_NUMBER') + '</TD>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_HOURS') + '</TD>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_TASK_NUMBER') + '</TD>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_HOURS') + '</TD>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_EXP_TYPE') + '</TD>' +
    '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,'PA_WEB_HOURS') + '</TD>' +
    '</TR>');

  fCaptureDataForSummaryByProject(p_objLines, l_arrSummary);
  
  for (i=0; i<l_arrSummary.length; i++) {
    p_framTarget.document.writeln('<TR>'); 
    fPrintLineForProjectSummary(p_framTarget,
				l_arrSummary[i][C_iROWS], 
				l_arrSummary[i][C_iNAME], 
				l_arrSummary[i][C_iHOURS]);

    for (j=0; j<l_arrSummary[i][C_iSUBARRAY].length; j++) {
      fPrintLineForProjectSummary(p_framTarget,
				  l_arrSummary[i][C_iSUBARRAY][j][C_iROWS], 
		        	  l_arrSummary[i][C_iSUBARRAY][j][C_iNAME], 
		        	  l_arrSummary[i][C_iSUBARRAY][j][C_iHOURS]);

      for (k=0; k<l_arrSummary[i][C_iSUBARRAY][j][C_iSUBARRAY].length; k++) {
        fPrintLineForProjectSummary(p_framTarget,
				    l_arrSummary[i][C_iSUBARRAY][j][C_iSUBARRAY][k][C_iROWS], 
		          	    l_arrSummary[i][C_iSUBARRAY][j][C_iSUBARRAY][k][C_iNAME], 
		          	    l_arrSummary[i][C_iSUBARRAY][j][C_iSUBARRAY][k][C_iHOURS]);
        p_framTarget.document.writeln('</TR>'); 
      }
      p_framTarget.document.writeln('</TR>');
    }
    p_framTarget.document.writeln('</TR>'); 
  } // End for loop
  p_framTarget.document.writeln('</TABLE>');
  fDrawTableBottomBorder(p_framTarget);
  p_framTarget.document.writeln('   </TD></TR>');

  if (l_arrSummary.length==0) {
    p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK>' + g_objFNDMsg.mstrGetMsg("PA_WEB_SAVE_WITH_NO_LINES") + '</TD></TR>');  
  }
  p_framTarget.document.writeln('</TABLE></TD></TR>');

} // End function


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawProjectDetails(
                p_framTarget,
		p_objLines) {

  var l_nHours = 0; 
  var i;
  var j;
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_CONFIRMATION");
  var l_bIsDFlexDisplayed = ((fbIsDFlexUsed()) && (fbIsDFlexVisible()));
  var l_bIsHoursEntered = false;

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 BORDER=0  CELLSPACING=0 width="100%">');
  fDrawTableLabel(p_framTarget, fGetRegionPrompt(l_objCurRegion,'PA_WEB_PROJECT_DETAILS'));

  p_framTarget.document.writeln('   <TR><TD>');
  fDrawTableTopBorder(p_framTarget, "LEFT")  // draw table with align=left

  p_framTarget.document.writeln('<TABLE cellpadding=2 cellspacing=1 border=0 WIDTH=100%>');
  p_framTarget.document.writeln('<TR>' +
  	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_PROJECT_NUMBER') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_TASK_NUMBER') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_EXP_TYPE') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_DATE') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_DAY') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_HOURS') + '</TD>' +
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_LINE_COMMENT') + '</TD>');

  // DFlex column header
  if (l_bIsDFlexDisplayed) {
    p_framTarget.document.writeln(
	  '<TD CLASS=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion, 'PA_WEB_ADDITIONAL_INFO') + '</TD>');
  }

  p_framTarget.document.writeln('</TR>');

  for (i=0; i<p_objLines.arrLine.length; i++) {
    var objDays = p_objLines.arrLine[i].mGetobjDays();
    for (j=0; j<g_dWeekEndingWeek.length; j++) {
      var objDay = objDays.mobjGetDay(top.g_dWeekEndingWeek[j].getDay());
      l_nHours = objDay.mGetnHours();
      if ((l_nHours != "" ) && (p_objLines.arrLine[i].bIsDeleted == false)) {
        l_bIsHoursEntered = true;
        p_framTarget.document.writeln(
		'<TR>' + 
                  '<TD CLASS=TABLECELL VALIGN="TOP">' + fCheckNullString(p_objLines.arrLine[i].mGetstrProjectName())	+ ' / '   +
			   fCheckNullString(p_objLines.arrLine[i].mGetstrProjectNum()) 	+ '</TD>' +
          	  '<TD CLASS=TABLECELL VALIGN="TOP">' + fCheckNullString(p_objLines.arrLine[i].mGetstrTaskName())   	+ ' / '   +
			   fCheckNullString(p_objLines.arrLine[i].mGetstrTaskNum())    	+ '</TD>' +
        	  '<TD CLASS=TABLECELL VALIGN="TOP">' + fCheckNullString(p_objLines.arrLine[i].mGetobjType().mGetstrName()) + '</TD>' +
        	  '<TD CLASS=TABLECELL VALIGN="TOP">' + fDateToLongString(
				top.g_strSessDateFormat,
				top.g_dWeekEndingWeek[j], 
				true) 					+ '</TD>' +
        	  '<TD CLASS=TABLECELL VALIGN="TOP">' + fIntToDayOfWeek(top.g_dWeekEndingWeek[j].getDay()) 				+ '</TD>' +
        	  '<TD CLASS=TABLECELLRIGHT VALIGN="TOP">' + fFormatHours(l_nHours) 			+ '</TD>' +
        	  '<TD CLASS=TABLECELL VALIGN="TOP">' + fFormatString(objDay.mGetstrComment())	+ '</TD>');

        // DFlex values
        if (l_bIsDFlexDisplayed) {
          p_framTarget.document.writeln('<TD CLASS=TABLECELL VALIGN="TOP">');
          fDisplayDFlex(p_framTarget, fGetContSensField(p_objLines.arrLine[i]), objDay.arrDFlex);
          p_framTarget.document.writeln('</TD>');
        }

        p_framTarget.document.writeln('</TR>'); 
      }
     }
  }
  p_framTarget.document.writeln('</TABLE>');
  fDrawTableBottomBorder(p_framTarget);
  p_framTarget.document.writeln('   </TD></TR>');
  if (!l_bIsHoursEntered) {
    p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK>' + g_objFNDMsg.mstrGetMsg("PA_WEB_SAVE_WITH_NO_HOURS") + '</TD></TR>');  
  }
  p_framTarget.document.writeln('   </TABLE></TD></TR>');
} // End function


//-------------------------------------------------------------------
// Function :
// Description :
//
//-------------------------------------------------------------------
function fDrawLinesSummary(p_framTarget, p_objLines) {

  fDrawSummaryByDay(p_framTarget, p_objLines);
  fDrawSummaryByProject(p_framTarget, p_objLines);
  fDrawProjectDetails(p_framTarget, p_objLines); 
}


//  ----------------------------------------------------
//  Function: fDrawAuditSummary
//  Description: Draw the Audit summary table
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fDrawAuditSummary (p_framTarget, p_objAuditHistory, p_objDeletedAuditHistory)
{
  var l_objCurRegion;
  var l_strAuditOpt = g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT");
  var undefined;

  if (l_strAuditOpt != C_strFULL && l_strAuditOpt != C_strYES) {
    return;
  }

  l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_SUMMARY");

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=2 BORDER=0  CELLSPACING=0 width="100%">');
  fDrawTableLabel(p_framTarget, fGetRegionPrompt(l_objCurRegion,'PA_WEB_AUDIT_SUMMARY'));

  p_framTarget.document.writeln('   <TR><TD>');                                 
  fDrawTableTopBorder(p_framTarget);

  fDrawAuditSummaryHeader (p_framTarget, p_objAuditHistory, l_strAuditOpt);

  // Draw audit history lines
  if (p_objAuditHistory != undefined && p_objAuditHistory.miGetNumAuditLines() > 0) {
    fDrawAuditSummaryLines (p_framTarget, p_objAuditHistory, l_strAuditOpt);
  }

  // Draw deleted audit history lines
  if (p_objDeletedAuditHistory != undefined && p_objDeletedAuditHistory.miGetNumAuditLines() > 0) {
    fDrawAuditSummaryLines (p_framTarget, p_objDeletedAuditHistory, l_strAuditOpt);
  }

  p_framTarget.document.writeln('   </TABLE>');     
  fDrawTableBottomBorder(p_framTarget);               

  
  p_framTarget.document.writeln('   </TD></TR>');     

  if (p_objAuditHistory != undefined && 
      p_objAuditHistory.miGetNumAuditLines() == 0 && 
      p_objDeletedAuditHistory != undefined && 
      p_objDeletedAuditHistory.miGetNumAuditLines()==0) {
    p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK>' + g_objFNDMsg.mstrGetMsg("PA_WEB_NO_AUDIT_INFO") + '</TD></TR>');
  }
  p_framTarget.document.writeln('</TABLE></TD></TR>');

}


//  ----------------------------------------------------
//  Function: fDrawAuditSummaryHeader
//  Description: Draw the header of the Audit summary table
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditSummaryHeader (p_framTarget, p_objAuditHistory, p_strAuditOpt)
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_SUMMARY");

  p_framTarget.document.writeln('      <TABLE cellpadding=2 cellspacing=1 border=0 width=100%>');
  p_framTarget.document.writeln('      <TR>');
  if (p_strAuditOpt == C_strFULL)
    p_framTarget.document.writeln('        <TD colspan=8></TD>');
  else
    p_framTarget.document.writeln('        <TD colspan=6></TD>');

  p_framTarget.document.writeln('      </TR>');
  p_framTarget.document.writeln('      <TR>');

  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_PROJECT") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_TASK") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_EXP_TYPE") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_DATE") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_HOURS") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_AUDIT_TYPE") + '</TD>');

  if (p_strAuditOpt == C_strFULL) {
    p_framTarget.document.write('          <TD class=TABLEROWHEADER>');
    p_framTarget.document.writeln(fGetRegionPrompt(l_objCurRegion,"PA_WEB_AUDIT_REASON") + '</TD>');
    p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_AUDIT_COMMENT") + '</TD>');
  }

  p_framTarget.document.writeln('      </TR>');
}


//  ----------------------------------------------------
//  Function: fDrawAuditSummaryLines
//  Description: Draw the lines of the Audit summary table
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditSummaryLines (p_framTarget, p_objAuditHistory, p_strAuditOpt)
{
  var l_objAuditLine;

  for (i=0; i<p_objAuditHistory.arrAuditLine.length; i++) {
    l_objAuditLine = p_objAuditHistory.arrAuditLine[i];    
    p_framTarget.document.write('<TR>' +

      // Project Number
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fCheckNullString(l_objAuditLine.mGetstrProjectNum()) +
      '</TD>' +

      // Task Number
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fCheckNullString(l_objAuditLine.mGetstrTaskNum()) +
      '</TD>' +

      // Type
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fCheckNullString(l_objAuditLine.mGetobjType().mGetstrName()) +
      '</TD>' +

      // Date
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fDateToLongString(top.g_strSessDateFormat,l_objAuditLine.mGetdEntryDate(),true) +
      '</TD>' +

      // Hours
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fFormatHours(l_objAuditLine.mGetnQuantity()) +
      '</TD>' +

      // Audit Type
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fAuditPrintAuditType(l_objAuditLine.mGetstrAuditType()) +
      '</TD>');

    // Check if Audit profile option is full or not
    if (p_strAuditOpt == C_strFULL) {
      p_framTarget.document.write(

      // Audit Reason
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fFormatString(l_objAuditLine.mGetstrAuditReason()) +
      '</TD>' +

      // Audit Comment
      '<TD CLASS=TABLECELL VALIGN="TOP">' + 
      fFormatString(l_objAuditLine.mGetstrAuditComment()) +
      '</TD>');
    }
    p_framTarget.document.writeln('</TR>');
  }
}

