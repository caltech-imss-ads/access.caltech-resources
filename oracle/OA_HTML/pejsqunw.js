<!-- hide the script's contents from feeble browsers
<!-- $Header: pejsqunw.js 115.0 99/07/18 15:29:44 porting sh $
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date        Changed by   Bug #   Reason
<!-- ------- ----------- ------------ ------  ---------------------------------
<!-- 110.0   28-AUG-1998 flemonni	      Created	
<!-- 110.1   18-SEP-1998 flemonni             Working File
<!-- 110.2   22-SEP-1998 flemonni             Working File		
<!-- 110.3   08-Mar-1999 fychu                No content change. Just removed
<!--                                          Microsoft DOS carriage control
<!--                                          character.
<!-- 
<!-----------------------------------------------------------------------------
var separator = "!#"
<!-- --------------------------------------------------------------------------
<!-- ------------------------<NVL>---------------------------------------------
<!-- --------------------------------------------------------------------------
function NVL (Instring, AltString) {
  if (Instring == null) {
    return AltString
  }
  else {
    if (Instring.length > 0) {
    return Instring
    }
    else return AltString
    }
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------<TransferToSend>----------------------------------
<!-- --------------------------------------------------------------------------
function TransferToSend (ArrayVar) {
  var theform = window.parent.frames[2].document.TransmitForm
  if (theform.p_count.value == 1) {
    theform.p_answer_value.value = ArrayVar[0]
  }
  else {
    var AssignToFormArr = theform.p_answer_value
    var x = AssignToFormArr.length
    for 
      ( i=0;
        i<x;
        i++
      ) 
    {
      AssignToFormArr[i].value = NVL(ArrayVar[i], '')
    }
  }
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------<AddToSeparatedString>----------------------------
<!-- --------------------------------------------------------------------------
function AddToSeparatedString (addstring, storestring, position) {
  if (position == null) {
    return (storestring  + addstring + separator)
  }
  else
    return (separator + addstring + storestring)
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------<GetAnswers>--------------------------------------
<!-- --------------------------------------------------------------------------
function GetAnswers (form) {
  var TrueLength = window.parent.frames[2].document.TransmitForm.p_count.value
  var theindex = 0
  var SendArray = new Array()
// initialize the array
  for (i=0;i<TrueLength;i++) {
    SendArray[i] = ""
  }
    for (count=0; count<form.length; count++) {
      var thefield = form.elements[count]
      var thetype = thefield.type 
      var thename = thefield.name
// name will already be a separated string
      SendString = thename
      if (thetype == "select-one") {
        var theselectval = ""
        if (form.elements[count].options.length != 0) {
          for 
            ( counter=0;
	      counter < form.elements[count].options.length;
	      counter++
            ) {
            if (form.elements[count].options[counter].selected) {
              theselectval = form.elements[count].options[counter].value
            }
          }
              SendString = 
                AddToSeparatedString 
                  ( NVL(theselectval, 'NOVALUE')
                  , SendString
                  )

        }
      SendArray[theindex++] = SendString
      }
      else if (thetype == "radio") {
        if (form.elements[count].checked) {
            SendString = 
              AddToSeparatedString 
                ( form.elements[count].value
                , SendString
                )
        SendArray [theindex++] = SendString
        }
      }
      else if (thetype == "text") {
        SendString = 
          AddToSeparatedString 
            ( NVL(form.elements[count].value, 'NOVALUE')
            , SendString
            )
        SendArray [theindex++] = SendString
      }
      else if (thetype == "textarea") {
        SendString = 
          AddToSeparatedString 
            ( NVL(form.elements[count].value, 'NOVALUE')
            , SendString
            )
        SendArray [theindex++] = SendString
      }
      else if (thetype == "checkbox") {
        if (form.elements[count].checked) {
          SendString = 
            AddToSeparatedString 
              ( 'Y'
              , SendString
              )
        }
      else {
        SendString = 
          AddToSeparatedString 
            ( 'N'
            , SendString
            )
      }
      SendArray [theindex++] = SendString        
    }
  }
  TransferToSend (SendArray)
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------<GetAnswers>--------------------------------------
<!-- --------------------------------------------------------------------------
function TransmitAnswers () {
  GetAnswers (window.parent.frames[2].document.QuestionnaireForm)
SubmitProcess(2)
}

