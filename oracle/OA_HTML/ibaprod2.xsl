<?xml version="1.0"?>
<!--$Header: ibaprod2.xsl 115.2 2000/09/08 14:09:59 pkm ship   $-->

<!--This stylesheet is for rendering Product list. The properties of Product object -->
<!--accessed here are ProductDetailLink, ShortDescription and ThumbNail image.-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/">
  <table border="0" cellpadding="1" cellspacing="2">
    <xsl:apply-templates select="//Product"/>
  </table>
</xsl:template>

<xsl:template match="Product">
  <tr>
    <td>
      <table border="0" cellpadding="2" cellspacing="0" align="center">
        <tr>
          <td> 
            <xsl:element name="a">
              <xsl:attribute name="href">
                <xsl:value-of select="child::ProductDetailLink"/> 
              </xsl:attribute>
              <xsl:value-of select="child::ShortDescription"/>
            </xsl:element>
          </td>
          <td>
            <xsl:element name="a">
              <xsl:element name="img">
                <xsl:attribute name="src">
                  <xsl:value-of select="child::ThumbNail"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                  <xsl:text></xsl:text>
                </xsl:attribute>
                <xsl:attribute name="border">
                  <xsl:text>0</xsl:text>
                </xsl:attribute>
              </xsl:element>
            </xsl:element>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</xsl:template>

<xsl:template match="*"></xsl:template>

</xsl:stylesheet>





