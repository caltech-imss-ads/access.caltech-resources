<!-- $Header: asfinctb.js 115.12 2001/04/05 14:42:10 pkm ship     $ -->
function updateRow(formName, rowName) {
  document.forms[formName].elements[rowName + "asfAction"].value
    = "UPD";
}

function createRow(formName, rowName) {
  document.forms[formName].elements[rowName + "asfAction"].value
    = "CRE";
}

function changeRadio(formName, thisRowName, lastRowName, lastRowAction, fieldName,
     checkValue, uncheckValue, recAction) {
  thisForm = document.forms[formName];
  if (thisForm.elements[thisRowName + "$RSCOUNT"]) {
    rowPrefix = thisRowName.substring(0, thisRowName.lastIndexOf('.ROW') + 4);
    rowNum = parseInt(thisRowName.substring(thisRowName.lastIndexOf('.ROW') + 4, thisRowName.length));
    for (i = 0; i < thisForm.elements[thisRowName + "$RSCOUNT"].value; i++) {
      thisForm.elements[rowPrefix + (rowNum + i) + '.' + fieldName].value = checkValue;
      thisForm.elements[rowPrefix + (rowNum + i) + '.asfAction'].value = recAction;
    }
  } else {
    thisForm.elements[thisRowName + fieldName].value = checkValue;
    thisForm.elements[thisRowName + "asfAction"].value = recAction;
  }
  if (lastRowName) {
    if (thisForm.elements[lastRowName + "$RSCOUNT"]) {
      rowPrefix = lastRowName.substring(0, lastRowName.lastIndexOf('.ROW') + 4);
      rowNum = parseInt(lastRowName.substring(lastRowName.lastIndexOf('.ROW') + 4, lastRowName.length));
      for (i = 0; i < thisForm.elements[lastRowName + "$RSCOUNT"].value; i++) {
        thisForm.elements[rowPrefix + (rowNum + i) + '.' + fieldName].value = uncheckValue;
        thisForm.elements[rowPrefix + (rowNum + i) + '.asfAction'].value = lastRowAction;
      }
    } else {
      thisForm.elements[lastRowName + fieldName].value = uncheckValue;
      thisForm.elements[lastRowName + "asfAction"].value = lastRowAction;
    }
  }
}

function navigate(formName, secName, start, pageName) {
  thisForm = document.forms[formName];
  thisForm.action = pageName;
  thisForm.elements[secName + ".OBJ.$START_ROW"].value = start;
  thisForm.elements["PAGE.OBJ.asfAction"].value = "N";;
  thisForm.submit();
}

function addRequiredColumn(secName, name, errMsg, focusName) {
  if (!self.reqSections) self.reqSections = new Array();
  secNum = -1;
  for (var i = 0; i < reqSections.length; i++) {
    if (reqSections[i].name == secName) {
      secNum = i;
      break;
    }
  }
  if (secNum == -1) {
    secNum = self.reqSections.length; 
    self.reqSections[secNum] = new Object();
    self.reqSections[secNum].name = secName;
    self.reqSections[secNum].cols = new Array();
  }
  colNum = self.reqSections[secNum].cols.length;
  self.reqSections[secNum].cols[colNum] = new Object();
  self.reqSections[secNum].cols[colNum].name = name;
  self.reqSections[secNum].cols[colNum].errMsg = errMsg;
  if(focusName) {
    self.reqSections[secNum].cols[colNum].focusName = focusName;
  } else {
    self.reqSections[secNum].cols[colNum].focusName = name;
  }
}

function asfTableErase(formName, prefix) 
 {
   var f = document.forms[formName];
   for(var i = 0; i < f.elements.length; i++)
     {
        var e = f.elements[i];
        var n=e.name;
        if(n.indexOf(prefix) == 0)    
          {
            if (e.type == "text")
               e.value = "";
            
            else if (e.type == "select-one")
               e.options[0].selected = true;

            else if (e.type == "checkbox")
               e.checked = false;

            if (f.elements[prefix + "asfAction"])
                f.elements[prefix + "asfAction"].value = "";             
          
          }   
     }
 }

function setSortColumn(formName, secName, colName) {
  var theForm = document.forms[formName];
  if (!theForm.elements[secName + '.CACHEDORDER.NAME1']) return;
  if (!theForm.elements[secName + '.CACHEDORDER.SEQUENCE1']) return;

  if (theForm.elements[secName + '.CACHEDORDER.NAME1'].value == colName) {
    if (theForm.elements[secName + '.CACHEDORDER.SEQUENCE1'].value.toUpperCase() == "ASC")
      theForm.elements[secName + '.CACHEDORDER.SEQUENCE1'].value = "desc";
    else theForm.elements[secName + '.CACHEDORDER.SEQUENCE1'].value = "asc";
  } else {
    theForm.elements[secName + '.CACHEDORDER.NAME1'].value = colName;
    theForm.elements[secName + '.CACHEDORDER.SEQUENCE1'].value = "asc";
  }

  for (i = 2; ; i++) {
    if (theForm.elements[secName + '.CACHEDORDER.NAME' + i])
      theForm.elements[secName + '.CACHEDORDER.NAME' + i].value = '';
    else break;
    if (theForm.elements[secName + '.CACHEDORDER.SEQUENCE' + i])
      theForm.elements[secName + '.CACHEDORDER.SEQUENCE' + i].value = '';
  }

  if (theForm.elements['PAGE.OBJ.asfAction'])
    theForm.elements['PAGE.OBJ.asfAction'].value = "";
  theForm.submit();
}
function getTableElementBaseColumn(tgt) {
  var nm = tgt.name;
  var retval = nm.substring(nm.lastIndexOf('.')+1);
  return retval;
}
function getTableElementPrefix(tgt) {
  var nm = tgt.name;
  var retval = nm.substring(0,nm.lastIndexOf('.')+1);
  return retval;
}




