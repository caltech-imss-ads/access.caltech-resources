CREATE OR REPLACE PACKAGE citsss_webenr_schdl_apprvl_pkg
IS
  -- ===========================================================================
  --  FILE NAME:    CITSSS_WEBENR_SCHDL_APPRVL_PKG.pls
  --  MODULE:       CITSSS
  --  AUTHOR:       Paul Yasumi
  --  DATE CREATED: 06/11/2003
  --  DESCRIPTION:  File to create Exeter scheme object for the Web Enrollment
  --                  Module Schedule Approval Menu.  This package contains the
  --                  program code and logic for the functionality of the
  --                  advisers schedule approval.  The 3 tabs that are
  --                  displayed that can be displayed by this menu are: the My
  --                  Advisee(s), Department Advisees, and Help tabs.
  --                This package calls the CITSSS_WEBENR_UPDATE_STATUS package.
  --                  The CITSSS_WEBENR_UPDATE_STATUS package contains the
  --                  program logic and code for the schedule status update and
  --                  automatic email notification for advisers portion of the
  --                  CITSSS Web Enrollment Module.
  --  CALL:
  --  CALLED BY:    This package is called by the CITSSS_WEBENR_ADVISERS_PKG.
  --  OBJECTS:
  --  PARAMETERS:
  --  OUTPUT:
  -- ===========================================================================
  --  REVISION HISTORY:
  --  Date         Name            HDT/Chg Req#  Description
  --  06/04/2003   Paul Yasumi     CHG001960     Initial Creation.
  --  09/21/2003   Paul Yasumi     CHG001960     Sunday's Work.
  --  10/08/2003   Paul Yasumi     CHG001960     More Changes.
  --  10/22/2003   Paul Yasumi     CHG001960     Modified search facility to
  --                                               query for single quotation
  --                                               marks.
  --  10/27/2003   Paul Yasumi     HD043137      Disabled buttons after
  --                                               approval or not approval and
  --                                               moved alert to this package
  --                                               from citsss_webenr_updat_
  --                                               status_pkg.  Added message
  --                                               to indicate underload
  --                                               condition.
  --  11/20/2003   Paul Yasumi     HD043588      Removed check box and logic
  --                                               for verifying that it has
  --                                               been checked.
  --  11/20/2003   Paul Yasumi     HD043675      Modified search function so
  --                                               that a % sign is not
  --                                               included before the last
  --                                               name and first name literal
  --                                               value entered by the user.
  --  11/20/2003   Paul Yasumi                   Added the sss_enroll_prog_map
  --                                               and sss_student_instance
  --                                               tables and joins to limit
  --                                               courses to correct program.
  --  11/26/2003   Paul Yasumi                   Modified package to query
  --                                               citsss_webenr_profiles table
  --                                               instead of sss_student_
  --                                               instance.attribute08 for
  --                                               student schedule status.
  --  11/26/2003   Paul Yasumi                   Modified package by adding
  --                                               condition on sss_student_
  --                                               instance table.
  --  01/16/2004   Paul Yasumi                   Added urlencode to p_option_
  --                                               major, UASH link to student
  --                                               course schedule, and changed
  --                                               user name in automatic email
  --                                               notification to first name
  --                                               and last name from full name.
  --  01/21/2004   Paul Yasumi                   Added tags to search results
  --                                               table if no advisees are
  --                                               found.  Added the literal 
  --                                               Professor to user name in
  --                                               automatic email notification.
  --  01/22/2004   Dennis Lo                     Added replace on p_restore_url.
  --  01/30/2004   Paul Yasumi                   Added urlencode to p_schedule_
  --                                               status.
  -- ===========================================================================
  -- ---------------------------------------------------------------------------
  --  Global variables
  -- ---------------------------------------------------------------------------
  g_session_info citfnd_web_sec_exe_pkg.g_session_info%TYPE;
  g_fill VARCHAR2(1) := citfnd_web_sec_exe_pkg.get_fill;
  g_debug VARCHAR2(1) := 'Y';
  g_academic_info citsss_webenr_global_pkg.g_academic_info%TYPE;
  g_non_breaking_spaces VARCHAR2(18) := '';
  g_alignment VARCHAR2(6) := 'LEFT';
  -- ===========================================================================
  --  Option Representative Advisee Search Page (Department Advisees Tab)
  -- ===========================================================================
  PROCEDURE opt_rep_advisee_search
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_user_person_id IN NUMBER
    );
  -- ===========================================================================
  --  Option Representative Advisee Search Results Page or
  --  Advisor Advisee List Page (My Advisee(s) Tab)
  -- ===========================================================================
  PROCEDURE advisee_search_results
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_last_name IN VARCHAR2,
    p_first_name IN VARCHAR2,
    -- p_program_degree_id IN NUMBER,
    p_option_major IN VARCHAR2,
    p_schedule_status IN VARCHAR2,
    p_advisor_person_id IN NUMBER,
    p_page_number IN NUMBER,
    p_option_rep_or_advisor IN VARCHAR2,
    p_user_person_id IN NUMBER
    );
  -- ===========================================================================
  --  Advisee Course Schedule Frame Page
  -- ===========================================================================
  PROCEDURE advisee_course_schedule_frame
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_enrollment_term_id IN NUMBER,
    p_current_period IN VARCHAR2,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_student_year_of_study IN VARCHAR2,
    -- p_student_email_address IN VARCHAR2,
    p_student_option IN VARCHAR2,
    p_student_schedule_status IN VARCHAR2,
    p_user_person_id IN NUMBER,
    p_restore_url IN VARCHAR2
    );
  -- ===========================================================================
  --  Advisee Course Schedule Page
  -- ===========================================================================
  PROCEDURE advisee_course_schedule
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_enrollment_term_id IN NUMBER,
    p_current_period IN VARCHAR2,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_student_year_of_study IN VARCHAR2,
    -- p_student_email_address IN VARCHAR2,
    p_student_option IN VARCHAR2,
    p_student_schedule_status IN VARCHAR2,
    p_user_person_id IN NUMBER,
    p_restore_url IN VARCHAR2
    );
  -- ===========================================================================
  --  Advisor Approval Log Page
  -- ===========================================================================
  PROCEDURE advisor_approval_log
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_term_id IN NUMBER,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_dummy IN VARCHAR2
    );
  -- ===========================================================================
  --  Advisor Instructions Page (Help Tab)
  -- ===========================================================================
  PROCEDURE help
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2
    );
END citsss_webenr_schdl_apprvl_pkg;
/
CREATE OR REPLACE PACKAGE BODY citsss_webenr_schdl_apprvl_pkg
IS
  -- ===========================================================================
  --  Option Representative Advisee Search Page (Department Advisees Tab)
  -- ===========================================================================
  PROCEDURE opt_rep_advisee_search
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_user_person_id IN NUMBER
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
    -- -------------------------------------------------------------------------
    --  Other Local Variables
    -- -------------------------------------------------------------------------
    -- v_term_name exeter.cmn_terms.term_name%TYPE;
    -- v_majors_and_degrees citsss_webenr_adviser_info_pkg.v_results_table_type;
    -- -------------------------------------------------------------------------
    --  Cursor for Advisers
    -- -------------------------------------------------------------------------
    TYPE t_cursor IS REF CURSOR;
    c_cursor t_cursor;
    v_select_statement VARCHAR2(32767) :=
      'SELECT /*+ Rule */ 0 advisor_person_id,' ||
             'NULL advisor_full_name,' ||
             '0 sortFirst' ||
       ' FROM dual' ||
      ' UNION ' ||
      'SELECT DISTINCT cp_advisor.person_id advisor_person_id,' ||
             'cp_advisor.full_name advisor_full_name,' ||
             '1 sortFirst' ||
       ' FROM cmn_people cp_advisor,' ||
             'cmn_advisors ca,' ||
             'cmn_advisor_advisee_rel caar,' ||
            '(SELECT ssi.student_instance_id,' ||
                    'cs_degree.status' ||
              ' FROM sss_student_instance ssi,' ||
                    'cmn_statuses cs_student,' ||
                    'cmn_programs cp,' ||
                    'sss_student_degrees ssd,' ||
                    'cmn_statuses cs_degree,' ||
                    'cmn_program_degrees cpd' ||
             ' WHERE cp.name IN (''Undergraduate'', ''Graduate'')' ||
               ' AND cp.program_id=ssi.program_id' ||
               ' AND cs_student.system=''SSS''' ||
               ' AND cs_student.status_type=''Student''' ||
               ' AND cs_student.status=''Enrolled''' ||
               ' AND cs_student.status_id=ssi.status_id' ||
               ' AND ssi.student_instance_id=ssd.student_instance_id' ||
               ' AND cs_degree.status_type=''Degree''' ||
               ' AND cs_degree.status like ''%Seeking''' ||
               ' AND cs_degree.status_id=ssd.status_id' ||
               ' AND ssd.program_degree_id=cpd.program_degree_id';
    v_where_clause_degree_major VARCHAR2(2000);
    v_row_count NUMBER := 0;
    v_advisor_person_id NUMBER;
    v_advisor_full_name cmn_people.full_name%TYPE;
    v_sortFirst NUMBER;
    v_closing_parenthesis VARCHAR2(6) := ') sub ';
    v_main_query_where_clause VARCHAR2(1000) :=
      ' WHERE cp_advisor.person_id=ca.person_id' ||
        ' AND ca.active=''Y''' ||
        ' AND ca.advisor_id=caar.advisor_id' ||
        ' AND sub.student_instance_id=caar.application_instance_id' ||
        ' AND caar.system=''SSS''' ||
        ' AND DECODE(sub.status,''Seeking'',''Option 1 (Seeking)'',' ||
                               '''Double Major - Seeking'',''Option 2-Double Major(Seeking)'')=caar.advisor_type';
                               -- '''Double-Major - Seeking'',''Option 2-Double Major(Seeking)'',' ||
                               -- '''Minor - Seeking'',''Option 2-Double Major(Seeking)'')=caar.advisor_type';
    v_order_by_clause VARCHAR2(100) :=
      ' ORDER BY sortfirst, advisor_full_name';
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      --  Open HTML Page
      -- -----------------------------------------------------------------------
      htp.htmlOpen;
      htp.headOpen;
      -- -----------------------------------------------------------------------
      --  JavaScript Begin
      -- -----------------------------------------------------------------------
      htp.print( '
                 <SCRIPT LANGUAGE="JavaScript">
                 <!-- Disable right mouse button functionality - Begin -->
                 var rightButtonDisabled="Right-mouse button disabled!";
                 function clickIE4() {
                   if (event.button==2) {
                     alert(rightButtonDisabled);
                     return false;
                   }
                 }
                 function clickNS4(e){
                   if (document.layers||document.getElementById&&!document.all) {
                     if (e.which==2||e.which==3) {
                       alert(rightButtonDisabled);
                       return false;
                     }
                   }
                 }
                 if (document.layers) {
                   document.captureEvents(Event.MOUSEDOWN);
                   document.onmousedown=clickNS4;
                 }
                 else if (document.all&&!document.getElementById) {
                   document.onmousedown=clickIE4;
                 }
                 document.oncontextmenu=new Function("alert(rightButtonDisabled);return false")
                 <!-- Disable right mouse button functionality - End -->
                 function verifyIt() {
                   var form = document.forms[0]
                   var good = 1
                   if ((form.p_last_name.value.indexOf("%") != -1) ||
                       (form.p_last_name.value.indexOf("_") != -1)) {
                         form.p_last_name.focus()
                         alert("Pattern matching character is not allowed.");
                         good = 0
                   }
                   if (good == 1) {
                     if ((form.p_first_name.value.indexOf("%") != -1) ||
                         (form.p_first_name.value.indexOf("_") != -1)) {
                           form.p_first_name.focus()
                           alert("Pattern matching character is not allowed.");
                           good = 0
                     }
                   }
                   if (good == 1) {
                     good = 0
                     for (i = 0; i < form.elements.length; i++) {
                       if ((form.elements[i].type == "text" && form.elements[i].value != "") ||
                           (form.elements[i].type == "select-one" && form.elements[i].value != 0)) {
                             good = 1
                             <!-- alert("Please specify at least one search item.") -->
                             <!-- form.elements[i].focus() -->
                             break
                       }
                       // more tests
                     }
                     if (good == 0) {
                       form.elements[0].focus()
                       alert("Please specify at least one search criterion.")
                     }
                   }
                   if (good == 1) {
                     document.forms[0].submit()
                   }
                 // more statements
                 }
                 </SCRIPT>
                 ' );
      -- -----------------------------------------------------------------------
      --  JavaScript End
      -- -----------------------------------------------------------------------
      htp.headClose;
      htp.bodyOpen( CATTRIBUTES=>'BGCOLOR="#CCCCCC", TEXT="#000000"' );
      htp.formOpen( 'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' );
      --------------------------------------------------------------------------
      -- htp.header( 4,'Department Advisee Search','CENTER' );
      /*
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                     'COLOR="#336699"><B><I>' ||
                     'Department Advisee Search' ||
                     '</I></B></FONT>',
                     CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      --
      htp.print( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                       'COLOR="#336699"><B><I>' ||
                 'Department Advisee Search' ||
                 '</I></B></FONT>' );
      */
      -- -----------------------------------------------------------------------
      --  Obtain Academic Term
      -- -----------------------------------------------------------------------
      -- v_term_name := citsss_webenr_global_pkg.current_enrollment_period_name;
      citsss_webenr_global_pkg.get_enrollment_term_periods
        (
        g_academic_info.enrollment_term_id,
        g_academic_info.enrollment_term_name,
        g_academic_info.enrollment_period_begins,
        g_academic_info.enrollment_period_ends,
        g_academic_info.view_only_period_begins,
        g_academic_info.view_only_period_ends,
        g_academic_info.add_day,
        g_academic_info.drop_day
        );
      -- -----------------------------------------------------------------------
      --  Open Table - Academic Term
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="45%"'); -- 40
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       -- 'Schedule Approval for Academic Term:&nbsp;' || -- Literal to be displayed
                       'Academic Term:&nbsp;' || -- Literal to be displayed
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR=#FFFFFF">' ||
                       '&nbsp;' ||
                       -- v_term_name || -- Variable to be displayed
                       g_academic_info.enrollment_term_name ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="50%"' || -- 25
                                      'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Academic Term
      -- -----------------------------------------------------------------------
      --  Place formOpen before all search items
      --  Moved inside table since statement was causing a break
      --  htp.formOpen( 'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' );
      -- -----------------------------------------------------------------------
      --  Open Table
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
      -- htp.formOpen( 'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' );
      -- -----------------------------------------------------------------------
      --  Outer Most Table Row
      -- -----------------------------------------------------------------------
      /*
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                     'Advisee Search' ||
                     '</B></FONT>',
                     CATTRIBUTES => 'ALIGN = "CENTER" ROWSPAN = "5"' ||
                                    'WIDTH="20%"' ||
                                    'BGCOLOR="#336699"' );
      */
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Advisee Search' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "CENTER" COLSPAN = "2"' ||
                                      -- 'WIDTH="20%"' ||
                                      'BGCOLOR="#336699"' );
      htp.tableRowClose;
      htp.tableRowOpen;
      -- -----------------------------------------------------------------------
      --  Advisee Last Name Table Row
      -- -----------------------------------------------------------------------
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Last Name:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT"' ||
                                      -- 'WIDTH="35%"' ||
                                      'BGCOLOR="#336699"' );
      htp.print( '<TD ALIGN="LEFT">' );
      -- htp.formText( CNAME => 'p_last_name', CSIZE => 45);
      htp.formText( CNAME => 'p_last_name', CSIZE => 55);
      --  Next line not needed
      --               CVALUE => vFreeFormatLastName );
      htp.print( '</TD>' );
      htp.tableRowClose;
      -- -----------------------------------------------------------------------
      --  Advisee First Name Table Row
      -- -----------------------------------------------------------------------
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'First Name:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT"' ||
                                      'BGCOLOR="#336699"' );
      htp.print( '<TD ALIGN="LEFT">' );
      -- htp.formText( CNAME => 'p_first_name', CSIZE => 45);
      htp.formText( CNAME => 'p_first_name', CSIZE => 55);
      --  Next line not needed
      --               CVALUE => vFreeFormatFirstName );
      htp.print( '</TD>' );
      htp.tableRowClose;
      -- -----------------------------------------------------------------------
      --  Option Table Row
      -- -----------------------------------------------------------------------
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Option:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT"' ||
                                      'BGCOLOR="#336699"' );
      htp.print( '<TD WIDTH="80%">' );
      htp.formSelectOpen( 'p_option_major' );
      -- -----------------------------------------------------------------------
      --  This retrieves the options for the option representative's majors
      --    as defined in Exeter's Program/Degress tab
      -- -----------------------------------------------------------------------
      FOR option_rec
        IN ( SELECT /*+ Rule */ NULL option_major,
                    0 sort_first
               FROM dual
              UNION
             SELECT DISTINCT cpd.major option_major,
                    1 sort_first
               FROM cmn_program_degrees cpd
              WHERE TO_NUMBER(cpd.attribute01) = p_user_person_id
              ORDER BY sort_first, option_major )
      -- -----------------------------------------------------------------------
      --  This retrieves all options, regardless of the option representative's
      --    majors as defined in Exeter's Program/Degress tab
      -- -----------------------------------------------------------------------
      -- FOR option_rec
      --   IN ( SELECT /*+ Rule */ NULL option_major,
      --               0 sort_first
      --          FROM dual
      --         UNION
      --        SELECT DISTINCT cpd.major option_major,
      --               1 sort_first
      --          FROM exeter.cmn_program_degrees cpd
      --         ORDER BY sort_first, option_major )
        LOOP
          htp.formSelectOption( option_rec.option_major,
                                CATTRIBUTES => 'VALUE="' ||
                                option_rec.option_major || '"' );
        END LOOP;
      htp.FormSelectClose;
      htp.print( '</TD>' );
      htp.tableRowClose;
      -- -----------------------------------------------------------------------
      --  Schedule Status Table Row
      -- -----------------------------------------------------------------------
      htp.tableRowOpen;
      --  WIDTH of 20% is applied to longest table data value
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Schedule Status:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT"' ||
                                      'WIDTH="30%"' ||
                                      'BGCOLOR="#336699"' );
      htp.print( '<TD>' );
      htp.formSelectOpen( 'p_schedule_status' );
      htp.formSelectOption( '' );
      --                        CATTRIBUTES => 'VALUE=""' );
      htp.formSelectOption( 'Not Started',
                              CATTRIBUTES => 'VALUE="Not Started"' );
      htp.formSelectOption( 'In Progress',
                              CATTRIBUTES => 'VALUE="In Progress"' );
      htp.formSelectOption( 'Pending Approval',
                              CATTRIBUTES => 'VALUE="Pending Approval"' );
      htp.formSelectOption( 'Approved',
                              CATTRIBUTES => 'VALUE="Approved"' );
      htp.formSelectOption( 'Not Approved',
                              CATTRIBUTES => 'VALUE="Not Approved"' );
      htp.formSelectClose;
      htp.print( '</TD>' );
      htp.tableRowClose;
      -- -----------------------------------------------------------------------
      --  Build v_where_clause_degree_major for Option Representative
      -- -----------------------------------------------------------------------
      FOR degree_and_major_rec
        IN (SELECT /*+ Rule */ cpd.degree degree,
                   cpd.major major
              FROM cmn_program_degrees cpd
             WHERE TO_NUMBER(cpd.attribute01) = p_user_person_id) LOOP
          v_row_count := v_row_count + 1;
          IF v_row_count = 1 THEN
            v_where_clause_degree_major := ' AND (' || chr(010);
          ELSE
            v_where_clause_degree_major := v_where_clause_degree_major ||
                                             ' OR ' ||
                                             chr(010);
          END IF;
          v_where_clause_degree_major := v_where_clause_degree_major ||
                                           '(' ||
                                           'cpd.degree=''' ||
                                           degree_and_major_rec.degree ||
                                           ''' AND cpd.major=''' ||
                                           degree_and_major_rec.major ||
                                           ''')';
        END LOOP;
        IF v_row_count = 0 THEN
          v_where_clause_degree_major := v_where_clause_degree_major ||
                                           chr(010) ||
                                           ' AND 1=2' ||
                                           chr(010);

        ELSE
          v_where_clause_degree_major := v_where_clause_degree_major ||
                                           chr(010) ||
                                           ')' ||
                                           chr(010);
        END IF;
      v_select_statement := v_select_statement ||
                            v_where_clause_degree_major ||
                            v_closing_parenthesis ||
                            v_main_query_where_clause ||
                            v_order_by_clause;
      -- htp.print( v_select_statement );
      -- -----------------------------------------------------------------------
      --  Advisor Table Row
      -- -----------------------------------------------------------------------
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Adviser:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT"' ||
                                      'BGCOLOR="#336699"' );
      htp.print( '<TD>' );
      htp.formSelectOpen( 'p_advisor_person_id' );
      -- -----------------------------------------------------------------------
      --  This retrieves the advisers for the option representative's majors
      --    as defined in Exeter's Program/Degress tab
      -- -----------------------------------------------------------------------
      -- /*
      OPEN c_cursor FOR v_select_statement;
        LOOP
          FETCH c_cursor INTO v_advisor_person_id,
                              v_advisor_full_name,
                              v_sortFirst;
          IF c_cursor%NOTFOUND THEN
            EXIT;
          END IF;
          htp.formSelectOption( v_advisor_full_name,
                                cattributes => 'VALUE="' ||
                                v_advisor_person_id || '"' );
        END LOOP;
      -- */
      -- -----------------------------------------------------------------------
      --  This retrieves all advisers for all majors, regardless of the option
      --    representative's majors as defined in Exeter's Program/Degress tab
      -- -----------------------------------------------------------------------
      -- FOR advisor_rec IN
      --   ( SELECT /*+ Rule */ 0 advisor_person_id, NULL advisor_full_name,
      --            0 sortFirst
      --       FROM dual
      --      UNION
      --     SELECT DISTINCT cp_advisor.person_id advisor_person_id,
      --            cp_advisor.full_name advisor_full_name, 1 sortFirst
      --       FROM cmn_people cp_advisor,
      --            cmn_advisors ca
      --      WHERE cp_advisor.person_id = ca.person_id
      --        AND ca.active = 'Y'
      --      ORDER BY sortFirst, advisor_full_name )
      --   LOOP
      --     htp.formSelectOption( advisor_rec.advisor_full_name,
      --                           cattributes => 'VALUE="' ||
      --                           advisor_rec.advisor_person_id || '"' );
      --   END LOOP;
      htp.FormSelectClose;
      htp.print( '</TD>' );
      htp.br;
      htp.tableRowClose;
      htp.print( '<TFOOT>' );
      htp.print( '<TD COLSPAN=2 ALIGN=CENTER BGCOLOR=#336699>' );
      htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                   'Enter at least one search criterion and press the Search button.' ||
                 '</B></FONT>' );
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Buttons
      -- -----------------------------------------------------------------------
      htp.formHidden( 'p_key',p_key );
      htp.formHidden( 'p_uid',p_uid );
      htp.formHidden( 'p_page_number',1 );
      htp.formHidden( 'p_option_rep_or_advisor','O' );
      htp.formHidden( 'p_user_person_id',p_user_person_id );
      htp.tableOpen( 'NOBORDER', 'CENTER' );
      htp.tableRowOpen;
      htp.print( '<TD>' );
      htp.print( '<INPUT TYPE="button" NAME="search" VALUE="Search" ' ||
                 'onClick="verifyIt()">' ||
                 ' ' ||
                 '<INPUT TYPE="reset">' );
      htp.print( '</TD>' );
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Buttons
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Search Tips
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="65%"' );
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL><B>' ||
                       'Search tip:' ||
                     '</B></SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" NOWRAP="NOWRAP" VALIGN="TOP"' );
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL>' ||
                       'Fields are not case sensitive. ' ||
                       ' Partial entries are accepted.' ||
                     '</SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL><B>' ||
                       'Example:' ||
                     '</B></SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" VALIGN="TOP"' );
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL>' ||
                       'Typing <B>gal</B> in the Last Name field and' ||
                       ' clicking the Search button will return "Galilei"' ||
                       --  20031119 pyasumi
                       --  Begin - Modified search tips to be consistent will
                       --    its functionality.
                       ' and any students with their last name beginning with' ||
                       ' the letters "gal".' ||
                       --  End - Modified search tips to be consistent will
                       --    its functionality.
                     '</SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL><B>' ||
                       'Search tip:' ||
                     '</B></SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" NOWRAP="NOWRAP" VALIGN="TOP"' );
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL>' ||
                       'When a dropdown field is selected, type one letter' ||
                       ' to jump to that letter in the list of values.<BR>' ||
                     '</SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL><B>' ||
                       'Example:' ||
                     '</B></SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" VALIGN="TOP"' );
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#000000"><SMALL>' ||
                       'Clicking the down arrow next to the Adviser' ||
                       ' field and typing in an <B>s</B> will move the cursor to' ||
                       ' advisers whose last names begin with "S".' ||
                     '</SMALL></FONT>',
                       CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      -- -----------------------------------------------------------------------
      --  Table Closed - Search Tips
      -- -----------------------------------------------------------------------
      htp.formClose;
      htp.bodyClose;
      htp.htmlClose;
      -- -----------------------------------------------------------------------
      --  HTML Page Closed
      -- -----------------------------------------------------------------------
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      htp.print( 'SQLCODE: ' || SQLCODE );
      htp.print( '<BR>' );
      htp.print( 'SQLERRM: ' || SQLERRM );
  END opt_rep_advisee_search;
  -- ===========================================================================
  --  Option Representative Advisee Search Results Page or
  --  Advisor Advisee List Page
  -- ===========================================================================
  PROCEDURE advisee_search_results
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_last_name IN VARCHAR2,
    p_first_name IN VARCHAR2,
    -- p_program_degree_id IN NUMBER,
    p_option_major IN VARCHAR2,
    p_schedule_status IN VARCHAR2,
    p_advisor_person_id IN NUMBER,
    p_page_number IN NUMBER,
    p_option_rep_or_advisor IN VARCHAR2,
    p_user_person_id IN NUMBER
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
    -- -------------------------------------------------------------------------
    --  Search Results Record and Table
    -- -------------------------------------------------------------------------
    TYPE searchResultsRecordType IS
      RECORD (
             student_person_id cmn_people.person_id%TYPE,
             student_name cmn_people.full_name%TYPE,
             student_uid cmn_people.person_identifier01%TYPE,
             student_instance_id sss_student_instance.student_instance_id%TYPE,
             student_program cmn_programs.name%TYPE,
             student_degree VARCHAR2(50), -- cmn_program_degrees.degree%TYPE,
             student_option VARCHAR2(500), -- cmn_program_degrees.major%TYPE,
             student_year_of_study cmn_year_of_study.yos_name%TYPE,
             student_schedule_status cmn_people.attribute01%TYPE
             );
    searchResultsRecord searchResultsRecordType;
    TYPE searchResultsTableType IS
      TABLE OF searchResultsRecordType
        INDEX BY BINARY_INTEGER;
    searchResultsTable searchResultsTableType;
    TYPE htpAnchorRecordType IS
      RECORD (
             curl VARCHAR2(32767),
             ctext VARCHAR2(32767),
             cname VARCHAR2(32767),
             cattributes VARCHAR2(32767)
             );
    htpAnchorRecord htpAnchorRecordType;
    TYPE htpAnchorTableType IS
      TABLE OF htpAnchorRecordType
        INDEX BY BINARY_INTEGER;
    htpAnchorTable htpAnchorTableType;
    vAnchorCount BINARY_INTEGER := 0;
    vRowCount BINARY_INTEGER := 0;
    -- -------------------------------------------------------------------------
    --  Cursor for Advisees
    -- -------------------------------------------------------------------------
    -- Keep white space to a minimum in v_select_statement
    TYPE t_cursor IS REF CURSOR;
    c_cursor t_cursor;
    v_select_statement VARCHAR2(32767);
    v_select_statement_part_1a VARCHAR2(1000) :=
      'SELECT /*+ Rule */' ||
            ' cp_pe.person_id,' ||
             'cp_pe.full_name,' ||
             'cp_pe.person_identifier01,' ||
             'ssi.student_instance_id,' ||
             'cp_pr.name,' ||
             'citsss_webenr_concat_degrees (ssi.student_instance_id),' ||
             'citsss_webenr_concat_options (ssi.student_instance_id),' ||
             'cyos.yos_name,' ||
             -- 20031126
             -- Begin - Modify to get schedule status from citsss_webenr_profiles.
             -- 'NVL(ssi.attribute08,''Not Started'')' ||
             'NVL(citsss_webenr_global_pkg.get_schedule_status(cp_pe.person_identifier01,';
    v_select_statement_part_1b VARCHAR2(1000) :=
             ',ssi.student_instance_id),''Not Started'')' ||
             -- End - Modify to get schedule status from citsss_webenr_profiles.
       ' FROM cmn_people cp_pe,';
    v_main_query_table_cpd VARCHAR2(500) :=
           ' (SELECT ssd.student_instance_id student_instance_id,' ||
                   ' cpd.major major' ||
              ' FROM cmn_program_degrees cpd,' ||
                    'sss_student_degrees ssd,' ||
                    'cmn_statuses cs_d' ||
             ' WHERE cpd.program_degree_id=ssd.program_degree_id' ||
               ' AND cs_d.status_type=''Degree''' ||
               ' AND cs_d.status LIKE ''%Seeking%''' ||
               ' AND cs_d.status_id=ssd.status_id) cpd,';
    v_select_statement_part_2 VARCHAR2(1000) :=
             'cmn_programs cp_pr,' ||
             'cmn_statuses cs_s,' ||
             'cmn_year_of_study cyos,' ||
             'sss_student_instance ssi' ||
      ' WHERE cp_pe.person_id=ssi.person_id';
    v_where_clause_statement_cpd VARCHAR2(100) :=
        ' AND cpd.student_instance_id(+)=ssi.student_instance_id';
    v_where_clause_statement VARCHAR2(1000) :=
        ' AND cp_pr.name IN (''Undergraduate'',''Graduate'')' ||
        ' AND cp_pr.program_id=ssi.program_id' ||
        ' AND cs_s.system=''SSS''' ||
        ' AND cs_s.status_type=''Student''' ||
        ' AND cs_s.status=''Enrolled''' ||
        ' AND cs_s.status_id=ssi.status_id' ||
        ' AND cyos.year_of_study_id(+)=ssi.year_of_study_id';
    v_where_clause_degree_program VARCHAR2(10000);
    v_row_count NUMBER := 0;
    v_where_clause_last_name VARCHAR2(500) :=
         --  20031119 pyasumi
         --  Begin - Removed % sign after second '.
         ' AND UPPER(cp_pe.last_name) LIKE ''';
         --  End - Removed % sign after second '.
    v_where_clause_first_name VARCHAR2(500) :=
         --  20031119 pyasumi
         --  Begin - Removed % sign after second '.
         ' AND UPPER(cp_pe.first_name) LIKE ''';
         --  End - Removed % sign after second '.
    v_where_clause_option_major VARCHAR2(500) :=
         ' AND cpd.major=';
    v_where_clause_opt_rep_part_1 VARCHAR2(2000) :=
         ' AND ssi.student_instance_id IN (SELECT caar.application_instance_id' ||
                                           ' FROM cmn_statuses cs,' ||
                                                 'sss_student_instance ssi,' ||
                                                 'cmn_advisor_advisee_rel caar,' ||
                                                 'cmn_advisors ca' ||
                                          ' WHERE cs.system=''SSS''' ||
                                            ' AND cs.status_type=''Student''' ||
                                            ' AND cs.status=''Enrolled''' ||
                                            ' AND cs.status_id=ssi.status_id' ||
                                            ' AND caar.application_instance_id=ssi.student_instance_id' ||
                                            ' AND caar.advisor_type IN (''Option 1 (Seeking)'',' ||
                                                                       '''Option 2-Double Major(Seeking)'')' ||
                                            ' AND caar.advisor_id=ca.advisor_id' ||
                                            ' AND ca.active=''Y''' ||
                                            ' AND ca.person_id=';
    v_where_clause_opt_rep_part_2 VARCHAR2(2000) :=
                                          ' UNION ' ||
                                         ' SELECT caar.application_instance_id' ||
                                           ' FROM cmn_statuses cs,' ||
                                                 'sss_student_instance ssi,' ||
                                                 'cmn_advisor_advisee_rel caar,' ||
                                                 'cmn_advisors ca' ||
                                          ' WHERE cs.system=''SSS''' ||
                                            ' AND cs.status_type=''Student''' ||
                                            ' AND cs.status=''Enrolled''' ||
                                            ' AND cs.status_id=ssi.status_id' ||
                                            ' AND caar.application_instance_id=ssi.student_instance_id' ||
                                            ' AND caar.advisor_type = ''Freshman''' ||
                                            ' AND caar.advisor_id=ca.advisor_id' ||
                                            ' AND ca.active=''Y''' ||
                                            ' AND ca.person_id=';
    v_where_clause_opt_rep_part_3 VARCHAR2(2000) :=
                                            ' AND caar.application_instance_id NOT IN (SELECT caar2.application_instance_id' ||
                                                                                       ' FROM cmn_statuses cs,' ||
                                                                                             'sss_student_instance ssi,' ||
                                                                                             'cmn_advisor_advisee_rel caar2,' ||
                                                                                             'cmn_advisors ca' ||
                                                                                      ' WHERE cs.system=''SSS''' ||
                                                                                        ' AND cs.status_type=''Student''' ||
                                                                                        ' AND cs.status=''Enrolled''' ||
                                                                                        ' AND cs.status_id=ssi.status_id' ||
                                                                                        ' AND caar2.application_instance_id=ssi.student_instance_id' ||
                                                                                        ' AND ca.active=''Y''' ||
                                                                                        ' AND ca.advisor_id=caar2.advisor_id' ||
                                                                                        ' AND caar2.advisor_type IN (''Option 1 (Seeking)'',' ||
                                                                                                                    '''Option 2-Double Major(Seeking)'')' ||
                                                                                        ' AND caar2.primary = ''Y''' ||
                                                                                        ' AND caar2.application_instance_id=caar.application_instance_id' ||
                                                                                        ' AND ca.person_id<>';
    -- 20031126
    -- Begin - Modify to get schedule status from citsss_webenr_profiles.
    -- v_where_clause_schedule VARCHAR2(500) :=
    -- ' AND NVL(ssi.attribute08,''Not Started'')=';
    v_where_clause_schedule_a VARCHAR2(500) :=
      ' AND NVL(citsss_webenr_global_pkg.get_schedule_status(cp_pe.person_identifier01,';
    v_where_clause_schedule_b VARCHAR2(500) :=
      ',ssi.student_instance_id),''Not Started'')=';
      -- End - Modify to get schedule status from citsss_webenr_profiles.
    v_order_by_clause VARCHAR2(1000) :=
      ' ORDER BY DECODE(cp_pr.name,''Undergraduate'',1,''Graduate'',2,3),' ||
                'cp_pe.full_name,' ||
                'cp_pe.person_identifier01';
    -- -------------------------------------------------------------------------
    --  Paging PREV 1 2 3 NEXT
    -- -------------------------------------------------------------------------
    vLinesPerPage BINARY_INTEGER := 25;
    vWorkPageNumber NUMBER;
    v_true_false BOOLEAN := FALSE;
    v_preferred_email_address_type cmn_people.preferred_email_address_type%TYPE;
    v_student_email_address VARCHAR2(240);
    v_font_color VARCHAR2(7);
    v_current_url VARCHAR2(32767);
    v_current_period VARCHAR2(10);
    -- v_person_identifier01 cmn_people.person_identifier01%TYPE; -- 20031015
    -- v_full_name cmn_people.full_name%TYPE; -- 20031015
    v_student_option cmn_program_degrees.major%TYPE;
    v_enrollment_term_id cmn_terms.term_id%TYPE;
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      -- v_current_url := citsss_webenr_send_url
      v_current_url := citsss_webenr_global_pkg.urlencode
                         (
                         'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' ||
                         '?p_key=' || v_encrypted_session_id ||
                         '&p_uid=' || g_session_info.caltech_uid ||
                         '&p_last_name=' || p_last_name ||
                         '&p_first_name=' || p_first_name ||
                         -- '&p_program_degree_id=' || p_program_degree_id ||
                         '&p_option_major=' || p_option_major ||
                         '&p_schedule_status=' || p_schedule_status ||
                         '&p_advisor_person_id=' || p_advisor_person_id ||
                         '&p_page_number=' || p_page_number ||
                         '&p_option_rep_or_advisor=' || p_option_rep_or_advisor ||
                         '&p_user_person_id=' || p_user_person_id
                         );
      -- 20031126
      -- Begin - Modify to get schedule status from citsss_webenr_profiles.
      citsss_webenr_global_pkg.get_enrollment_term_periods
        (
        g_academic_info.enrollment_term_id,
        g_academic_info.enrollment_term_name,
        g_academic_info.enrollment_period_begins,
        g_academic_info.enrollment_period_ends,
        g_academic_info.view_only_period_begins,
        g_academic_info.view_only_period_ends,
        g_academic_info.add_day,
        g_academic_info.drop_day
        );
      v_enrollment_term_id := g_academic_info.enrollment_term_id;
      -- End - Modify to get schedule status from citsss_webenr_profiles.
      -- -----------------------------------------------------------------------
      --  Build v_where_clause_degree_major for Option Representative
      -- -----------------------------------------------------------------------
      IF p_option_rep_or_advisor = 'O' THEN
        BEGIN
          FOR degree_and_program_rec
            IN (SELECT cpd.program_degree_id program_degree_id,
                       cpd.program_id program_id
                  FROM cmn_program_degrees cpd,
                       cmn_programs cp
                 WHERE cpd.program_id = cp.program_id
                   -- AND ROWNUM < 4
                   AND cp.name IN ('Undergraduate', 'Graduate')
                   AND TO_NUMBER(cpd.attribute01) = p_user_person_id) LOOP
              v_row_count := v_row_count + 1;
              IF v_row_count = 1 THEN
                v_where_clause_degree_program :=
                  ' AND ssi.student_instance_id IN (SELECT ssi.student_instance_id';
              ELSE
                v_where_clause_degree_program := v_where_clause_degree_program ||
                                                     ' UNION' ||
                                                    ' SELECT ssi.student_instance_id';
              END IF;
                v_where_clause_degree_program := v_where_clause_degree_program ||
                                                      ' FROM cmn_statuses cs,' ||
                                                            'sss_student_instance ssi,' ||
                                                            'sss_student_degrees ssd' ||
                                                     ' WHERE ssi.program_id=' || TO_CHAR(degree_and_program_rec.program_id) ||
                                                       ' AND cs.system=''SSS''' ||
                                                       ' AND cs.status=''Enrolled''' ||
                                                       ' AND cs.status_id=ssi.status_id' ||
                                                       ' AND ssi.student_instance_id=ssd.student_instance_id' ||
                                                       ' AND ssd.program_degree_id=' || TO_CHAR(degree_and_program_rec.program_degree_id) ||
                                                       ' AND ssd.status_id IN (SELECT cs.status_id' ||
                                                                               ' FROM cmn_statuses cs' ||
                                                                              ' WHERE cs.system=''SSS''' ||
                                                                                ' AND cs.status_type=''Degree''' ||
                                                                                ' AND cs.status LIKE ''%Seeking%'')';
            END LOOP;
            IF v_row_count = 0 THEN
              v_where_clause_degree_program := v_where_clause_degree_program ||
                  ' AND 1=2';
            ELSE
              v_where_clause_degree_program := v_where_clause_degree_program ||
                  ')';
            END IF;
          -- v_select_statement := v_select_statement ||
          --                       v_where_clause_degree_program;
        END;
        -- htp.print( v_where_clause_degree_program );
      END IF;
      IF p_option_major IS NOT NULL THEN
        -- 20031126
        -- Begin - Modify to get schedule status from citsss_webenr_profiles.
        -- v_select_statement := v_select_statement_part_1 ||
        v_select_statement := v_select_statement_part_1a ||
                              g_academic_info.enrollment_term_id ||
                              v_select_statement_part_1b ||
        -- End - Modify to get schedule status from citsss_webenr_profiles.
                              v_main_query_table_cpd ||
                              v_select_statement_part_2 ||
                              v_where_clause_statement_cpd ||
                              v_where_clause_statement;
      ELSE
        -- 20031126
        -- Begin - Modify to get schedule status from citsss_webenr_profiles.
        -- v_select_statement := v_select_statement_part_1 ||
        v_select_statement := v_select_statement_part_1a ||
                              g_academic_info.enrollment_term_id ||
                              v_select_statement_part_1b ||
                              v_select_statement_part_2 ||
                              v_where_clause_statement;
      END IF;
      IF p_option_rep_or_advisor = 'O' THEN
        v_select_statement := v_select_statement ||
                              v_where_clause_degree_program;
      END IF;
      -- -----------------------------------------------------------------------
      --  Continue Building Dynamic Select Statement
      -- -----------------------------------------------------------------------
      IF p_last_name IS NOT NULL THEN
        v_select_statement := v_select_statement ||
                              v_where_clause_last_name ||
                              UPPER(REPLACE(p_last_name,'''','''''')) || '%''';
      END IF;
      IF p_first_name IS NOT NULL THEN
        v_select_statement := v_select_statement ||
                              v_where_clause_first_name ||
                              UPPER(REPLACE(p_first_name,'''','''''')) || '%''';
      END IF;
      IF p_option_major IS NOT NULL THEN
        v_select_statement := v_select_statement ||
                              v_where_clause_option_major || '''' ||
                              p_option_major || '''';
      END IF;
      IF p_schedule_status IS NOT NULL THEN
        v_select_statement := v_select_statement ||
        -- 20031126
        -- Begin - Modify to get schedule status from citsss_webenr_profiles.
        --                       v_where_clause_schedule || '''' ||
                              v_where_clause_schedule_a ||
                              g_academic_info.enrollment_term_id ||
                              v_where_clause_schedule_b ||
                              '''' || p_schedule_status || '''';
        -- End - Modify to get schedule status from citsss_webenr_profiles.
      END IF;
      IF p_advisor_person_id <> 0 THEN
        v_select_statement := v_select_statement ||
                              v_where_clause_opt_rep_part_1 || p_advisor_person_id ||
                              v_where_clause_opt_rep_part_2 || p_advisor_person_id ||
                              v_where_clause_opt_rep_part_3 || p_advisor_person_id ||
                              '))';
      END IF;
      v_select_statement := v_select_statement ||
                            v_order_by_clause;
      -- htp.print( v_select_statement );
      -- -----------------------------------------------------------------------
      --  Load option representative search results into PL/SQL table
      -- -----------------------------------------------------------------------
      vRowCount := 0;
      OPEN c_cursor FOR v_select_statement;
        LOOP
          FETCH c_cursor INTO searchResultsRecord.student_person_id,
                              searchResultsRecord.student_name,
                              searchResultsRecord.student_uid,
                              searchResultsRecord.student_instance_id,
                              searchResultsRecord.student_program,
                              searchResultsRecord.student_degree,
                              searchResultsRecord.student_option,
                              searchResultsRecord.student_year_of_study,
                              searchResultsRecord.student_schedule_status;
          IF c_cursor%NOTFOUND THEN
            EXIT;
          END IF;
          -- vRowCount := vRowCount + 1;
          IF vRowCount = 0 THEN
            -- v_person_identifier01 := searchResultsRecord.student_uid; -- 20031015
            -- v_full_name := searchResultsRecord.student_name; -- 20031015
            vRowCount := 1;
            searchResultsTable(vRowCount) := searchResultsRecord;
          -- IF vRowCount = 1 THEN
            --  Build hyper-link with record count as part of tag
            htpAnchorRecord.curl := 'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' ||
                                    '?p_key=' || v_encrypted_session_id ||
                                    '&p_uid=' || g_session_info.caltech_uid ||
                                    '&p_last_name=' || p_last_name ||
                                    '&p_first_name=' || p_first_name ||
                                    -- '&p_program_degree_id=' || p_program_degree_id ||
                                    '&p_option_major=' || citsss_webenr_global_pkg.urlencode(p_option_major) ||
                                    '&p_schedule_status=' || citsss_webenr_global_pkg.urlencode(p_schedule_status) ||
                                    '&p_advisor_person_id=' || p_advisor_person_id ||
                                    '&p_page_number=1' ||
                                    '&p_option_rep_or_advisor=' || p_option_rep_or_advisor ||
                                    '&p_user_person_id=' || p_user_person_id;
            --  THE <U> AND </U> TAG SHOULD BE REPLACED WITH A STYLE SHEET
            -- htp.print( 'everytwo' );
            htpAnchorRecord.ctext := '<U>' || 1 || '</U>';
            vAnchorCount := vAnchorCount + 1;
            -- htp.print( htpAnchorRecord.ctext || ' ' || htpAnchorRecord.curl || '<BR>' );
            htpAnchorTable(1) := htpAnchorRecord;
          -- ELSIF searchResultsRecord.student_uid <> v_person_identifier01 -- 20031015
          --   OR searchResultsRecord.student_name <> v_full_name THEN -- 20031015
          --   v_person_identifier01 := searchResultsRecord.student_uid; -- 20031015
          --   v_full_name := searchResultsRecord.student_name; -- 20031015
          ELSE
            vRowCount := vRowCount + 1;
            searchResultsTable(vRowCount) := searchResultsRecord;
            IF MOD(vRowCount - 1,vLinesPerPage) = 0 THEN
          -- ELSIF MOD(vRowCount - 1,vLinesPerPage) = 0 THEN
              vAnchorCount := vAnchorCount + 1;
              htpAnchorRecord.curl := 'citsss_webenr_schdl_apprvl_pkg.advisee_search_results' ||
                                      '?p_key=' || v_encrypted_session_id ||
                                      '&p_uid=' || g_session_info.caltech_uid ||
                                      '&p_last_name=' || p_last_name ||
                                      '&p_first_name=' || p_first_name ||
                                      -- '&p_program_degree_id=' || p_program_degree_id ||
                                      '&p_option_major=' || citsss_webenr_global_pkg.urlencode(p_option_major) ||
                                      '&p_schedule_status=' || citsss_webenr_global_pkg.urlencode(p_schedule_status) ||
                                      '&p_advisor_person_id=' || p_advisor_person_id ||
                                      '&p_page_number=' ||
                                       vAnchorCount ||
                                      '&p_option_rep_or_advisor=' || p_option_rep_or_advisor ||
                                      '&p_user_person_id=' || p_user_person_id;
              htpAnchorRecord.ctext := '<U>' ||TO_CHAR( vAnchorCount ) || '</U>';
              -- htp.print( htpAnchorRecord.ctext || ' ' || htpAnchorRecord.curl || '<BR>' );
              htpAnchorTable(vAnchorCount) := htpAnchorRecord;
            END IF;
          END IF;
          -- vRowCount := vRowCount + 1;
        END LOOP;
      -- htp.print( 'here' );
      -- -----------------------------------------------------------------------
      --  Open HTML Page
      -- -----------------------------------------------------------------------
      htp.htmlOpen;
      htp.headOpen;
      -- htp.print( '<META HTTP-EQUIV="Pragma" CONTENT="no-cache">' );
      -- htp.print( '<META HTTP-EQUIV="expires" CONTENT="0">' );
      -- -----------------------------------------------------------------------
      --  JavaScript Begin
      -- -----------------------------------------------------------------------
      --  This has to be modified
      htp.print( '
                 <SCRIPT LANGUAGE="JavaScript">
                 <!-- Disable right mouse button functionality - Begin -->
                 var rightButtonDisabled="Right-mouse button disabled!";
                 function clickIE4() {
                   if (event.button==2) {
                     alert(rightButtonDisabled);
                     return false;
                   }
                 }
                 function clickNS4(e){
                   if (document.layers||document.getElementById&&!document.all) {
                     if (e.which==2||e.which==3) {
                       alert(rightButtonDisabled);
                       return false;
                     }
                   }
                 }
                 if (document.layers) {
                   document.captureEvents(Event.MOUSEDOWN);
                   document.onmousedown=clickNS4;
                 }
                 else if (document.all&&!document.getElementById) {
                   document.onmousedown=clickIE4;
                 }
                 document.oncontextmenu=new Function("alert(rightButtonDisabled);return false")
                 <!-- Disable right mouse button functionality - End -->
                 function goToURL() {
                   window.location = "' || g_session_info.exeter_web_agent ||
                                           '/citsss_webenr_schdl_apprvl_pkg.opt_rep_advisee_search?p_key=' ||
                                           v_encrypted_session_id ||
                                           '&p_uid=' || g_session_info.caltech_uid ||
                                           '&p_user_person_id=' || p_user_person_id || '"' ||
                 '
                 }
                 <!-- The following 3 JavaScript functions open modal popup windows -->
                 function ShowWindow(url) {
                   var screenHeight = screen.height;
                   var screenWidth = screen.width;
                   var heightAdjustmentFactor;
                   var widthAdjustmentFactor;
                   heightAdjustmentFactor = screenHeight/768;
                   widthAdjustmentFactor = screenWidth/1024;
                   var winModalWindow
                 function IgnoreEvents(e) {
                   return false
                 }
                 function HandleFocus() {
                   if (winModalWindow) {
                     if (!winModalWindow.closed) {
                       winModalWindow.focus()
                     } else {
                       window.top.releaseEvents (Event.CLICK|Event.FOCUS)
                       window.top.onclick = ""
                     }
                   }
                   return false
                 }
				   				   
                   if (window.showModalDialog) {
                     window.showModalDialog(url,' ||
                                           'null,' ||
                                           '"dialogHeight=360px;' ||
                                           'dialogWidth=580px;' ||
                                           -- 'dialogLeft=68;' ||
                                           -- 'dialogTop=68;' ||
                                           'center=yes;' ||
                                           'scroll=no;' ||
                                           'status=no;' ||
                                           'resizable=no")
                   } else {
                     window.top.captureEvents(Event.CLICK|Event.FOCUS)
                     window.top.onclick=IgnoreEvents
                     window.top.onfocus=HandleFocus
                     winModalWindow = window.open(url,' ||
                                                  '"ModalChild",' ||
                                                  '"dependent=yes,' ||
                                                  'height=" + 500*heightAdjustmentFactor + ",' ||
                                                  'width=" + 700*widthAdjustmentFactor + ",' ||
                                                  'top=68,' || --  Increment tiling by 24
                                                  'left=68,' || --  Increment tiling by 24
                                                  'titlebar=yes,' ||
                                                  'scrollbars=yes,' ||
                                                  'menubar=yes,' ||
                                                  'resizable=yes");
                     winModalWindow.focus()
                   }
                 }
                 </SCRIPT>
                 ' );
      -- -----------------------------------------------------------------------
      --  JavaScript End
      -- -----------------------------------------------------------------------
      htp.headClose;
      htp.bodyOpen( CATTRIBUTES=>'BGCOLOR="#CCCCCC", TEXT="#000000"' );
      -- htp.formOpen( 'citsss_webenr_schdl_apprvl_pkg.advisee_course_schedule_frame' );
      -- htp.print( '<FORM>' );
      -- 20031126
      -- Begin - Modify to get schedule status from citsss_webenr_profiles.
      /*
      citsss_webenr_global_pkg.get_enrollment_term_periods
        (
        g_academic_info.enrollment_term_id,
        g_academic_info.enrollment_term_name,
        g_academic_info.enrollment_period_begins,
        g_academic_info.enrollment_period_ends,
        g_academic_info.view_only_period_begins,
        g_academic_info.view_only_period_ends,
        g_academic_info.add_day,
        g_academic_info.drop_day
        );
      */
      -- End - Modify to get schedule status from citsss_webenr_profiles.
      -- -----------------------------------------------------------------------
      --  Open Table - Academic Term
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="45%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Academic Term:&nbsp;' ||
                     '</B></FONT>',
                     CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                     'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       g_academic_info.enrollment_term_name ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="50%"' ||
                                      'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table -  Period Information Table
      -- -----------------------------------------------------------------------
      --  FOR TESTING
      -- IF SYSDATE - 0 BETWEEN g_academic_info.enrollment_period_begins AND
      IF SYSDATE BETWEEN g_academic_info.enrollment_period_begins AND
        g_academic_info.enrollment_period_ends THEN
          v_current_period := 'Enrollment';
      -- ELSIF SYSDATE - 0 BETWEEN g_academic_info.view_only_period_begins AND
      ELSIF SYSDATE BETWEEN g_academic_info.view_only_period_begins AND
        g_academic_info.view_only_period_ends THEN
          v_current_period := 'View-Only';
      END IF;
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Enrollment Period Begins:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      IF v_current_period = 'Enrollment' THEN
        htp.tableData( '<SMALL><FONT COLOR="#FF0000"' ||
                       'FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.enrollment_period_begins,
                                 -- 'DD-MON-YYYY HH24:MI:SS') ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         -- CATTRIBUTES => 'WIDTH="50%"' ||
                         --                'BGCOLOR="#FFFFFF"');
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      ELSE
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.enrollment_period_begins,
                                 -- 'DD-MON-YYYY HH24:MI:SS') ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         -- CATTRIBUTES => 'WIDTH="50%"' ||
                         --                'BGCOLOR="#FFFFFF"');
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      END IF;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Enrollment Period Ends:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      IF v_current_period = 'Enrollment' THEN
        htp.tableData( '<SMALL><FONT COLOR="#FF0000"' ||
                       'FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.enrollment_period_ends,
                                 -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      ELSE
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.enrollment_period_ends,
                                 -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      END IF;
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'View-Only Period Begins:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      IF v_current_period = 'View-Only' THEN
        htp.tableData( '<SMALL><FONT COLOR="#FF0000" ' ||
                       'FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.view_only_period_begins,
                                 -- 'DD-MON-YYYY HH24:MI') || ':00' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         -- CATTRIBUTES => 'WIDTH="50%"' ||
                         --                'BGCOLOR="#FFFFFF"');
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      ELSE
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.view_only_period_begins,
                                 -- 'DD-MON-YYYY HH24:MI') || ':00' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         -- CATTRIBUTES => 'WIDTH="50%"' ||
                         --                'BGCOLOR="#FFFFFF"');
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      END IF;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'View-Only Period Ends:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      IF v_current_period = 'View-Only' THEN
        htp.tableData( '<SMALL><FONT COLOR="#FF0000" ' ||
                       'FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.view_only_period_ends,
                                 -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      ELSE
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'BGCOLOR="#FFFFFF">' ||
                         '&nbsp;' ||
                         TO_CHAR(g_academic_info.view_only_period_ends,
                                 -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                                'DD-MON-YYYY HH:MI AM') ||
                       '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      END IF;
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Last Day to Add Courses:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                     'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       TO_CHAR(g_academic_info.add_day,
                               -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                              'DD-MON-YYYY HH:MI AM') ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Last Day to Drop Courses:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                     'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       TO_CHAR(g_academic_info.drop_day,
                               -- 'DD-MON-YYYY HH24:MI') || ':59' ||
                              'DD-MON-YYYY HH:MI AM') ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Enrollment Term and Period Information
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen( CATTRIBUTES => 'BGCOLOR="#336699"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'Student Name' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'UID' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'Program' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'Degree' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'Option' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                       g_non_breaking_spaces ||
                       'Year of Study' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       -- '<SMALL>' ||
                         g_non_breaking_spaces ||
                        'Schedule Status' ||
                       -- '</SMALL>' ||
                       '</FONT>',
                         CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableRowClose;
      FOR rowCount IN (p_page_number * vLinesPerPage - (vLinesPerPage - 1)) ..
        (p_page_number * vLinesPerPage) LOOP
          IF rowCount > vRowCount THEN
            EXIT;
          END IF;
          --  Build result table using HTML tags
          IF v_true_false THEN
            htp.tableRowOpen( CATTRIBUTES => 'BGCOLOR="LIGHTYELLOW"' );
          ELSE
            htp.tableRowOpen( CATTRIBUTES => 'BGCOLOR="WHITE"' ); -- , "NOWRAP"');
          END IF;
          v_true_false := NOT v_true_false;
          searchResultsRecord := searchResultsTable(rowCount);
          -- IF (searchResultsRecord.student_schedule_status = 'In Progress' OR
          IF (searchResultsRecord.student_schedule_status = 'Pending Approval' OR
            searchResultsRecord.student_schedule_status = 'Approved' OR
            searchResultsRecord.student_schedule_status = 'Not Approved') THEN
              IF searchResultsRecord.student_option LIKE '%<BR>%' THEN
                v_student_option := SUBSTR(searchResultsRecord.student_option,1,INSTR(searchResultsRecord.student_option,'<BR>')-1);
              ELSE
                v_student_option := searchResultsRecord.student_option;
              END IF;
              htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                               -- htf.anchor( 'citsss_webenr_schdl_apprvl_pkg.advisee_course_schedule' ||
                               htf.anchor( 'citsss_webenr_schdl_apprvl_pkg.advisee_course_schedule_frame' ||
                                 '?p_key=' || v_encrypted_session_id ||
                                 '&p_uid=' || g_session_info.caltech_uid ||
                                 '&p_enrollment_term_id=' || g_academic_info.enrollment_term_id ||
                                 '&p_current_period=' || v_current_period ||
                                 '&p_student_person_id=' || searchResultsRecord.student_person_id ||
                                 '&p_student_name=' || citsss_webenr_global_pkg.urlencode(searchResultsRecord.student_name) ||
                                 '&p_student_uid=' || searchResultsRecord.student_uid ||
                                 '&p_student_instance_id=' || searchResultsRecord.student_instance_id ||
                                 '&p_student_year_of_study=' || searchResultsRecord.student_year_of_study ||
                                 -- '&p_student_email_address=' || v_student_email_address ||
                                 '&p_student_option=' || REPLACE(REPLACE(v_student_option,'&','%2526'),' ','%20') ||
                                 -- '&p_student_option=' || REPLACE(searchResultsRecord.student_option,'&','') ||
                                 '&p_student_schedule_status=' || citsss_webenr_global_pkg.urlencode(searchResultsRecord.student_schedule_status) ||
                                 '&p_user_person_id=' || p_user_person_id ||
                                 -- '&p_restore_url=' || citsss_webenr_urlencode(v_current_url),
                                 '&p_restore_url=' || citsss_webenr_global_pkg.urlencode(v_current_url),
                               -- '<FONT COLOR="#FFFFFF">' ||
                                 g_non_breaking_spaces ||
                               -- '</FONT>' ||
                               searchResultsRecord.student_name ) ||
                             '</FONT></SMALL>', g_alignment );
          ELSE
              htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                               g_non_breaking_spaces ||
                               searchResultsRecord.student_name ||
                             '</FONT></SMALL>', g_alignment );
          END IF;
          IF searchResultsRecord.student_uid IS NOT NULL THEN
            htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                             g_non_breaking_spaces ||
                             searchResultsRecord.student_uid ||
                           '</FONT></SMALL>', g_alignment );
          ELSE
            htp.tableData( '&nbsp;' );
          END IF;
          htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                           g_non_breaking_spaces ||
                           searchResultsRecord.student_program ||
                         '</FONT></SMALL>', g_alignment );
          IF searchResultsRecord.student_degree IS NOT NULL THEN
            htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                             g_non_breaking_spaces ||
                             searchResultsRecord.student_degree ||
                           '</SMALL>', g_alignment );
          ELSE
            htp.tableData( '&nbsp;' );
          END IF;
          IF searchResultsRecord.student_option IS NOT NULL THEN
            htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                             g_non_breaking_spaces ||
                             searchResultsRecord.student_option ||
                           '</FONT></SMALL>', g_alignment );
          ELSE
            htp.tableData( '&nbsp;' );
          END IF;
          IF searchResultsRecord.student_year_of_study IS NOT NULL THEN
            htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                             g_non_breaking_spaces ||
                             searchResultsRecord.student_year_of_study ||
                           '</FONT></SMALL>', g_alignment );
          ELSE
            htp.tableData( '&nbsp;' );
          END IF;
          /*
          IF searchResultsRecord.student_schedule_status = 'Not Started' THEN
            v_font_color := '#FFFFFF';
          ELSIF searchResultsRecord.student_schedule_status = 'In Progress' THEN
            v_font_color := '#FFFFFF';
          ELSIF searchResultsRecord.student_schedule_status = 'Pending Approval' THEN
          */
          IF searchResultsRecord.student_schedule_status = 'Pending Approval' THEN
            v_font_color := '#FF0000';
          ELSE
            v_font_color := '#000000';
          END IF;
          /*
          ELSIF searchResultsRecord.student_schedule_status = 'Approved' THEN
            v_font_color := '#FFFFFF';
          ELSIF searchResultsRecord.student_schedule_status = 'Not Approved' THEN
            v_font_color := '#FFFFFF';
          END IF;
          */
          IF searchResultsRecord.student_schedule_status = 'Approved' OR
            searchResultsRecord.student_schedule_status = 'Not Approved' THEN
              htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                             -- 'COLOR="' || v_font_color || '">' ||
                               htf.anchor( 'javascript:ShowWindow(''' ||
                                            g_session_info.exeter_web_agent ||
                                           '/citsss_webenr_schdl_apprvl_pkg.advisor_approval_log' ||
                                           '?p_key=' || v_encrypted_session_id ||
                                           '&p_uid=' || g_session_info.caltech_uid ||
                                           '&p_term_id=' || g_academic_info.enrollment_term_id ||
                                           '&p_student_person_id=' || searchResultsRecord.student_person_id ||
                                           '&p_student_name=' || citsss_webenr_global_pkg.urlencode(searchResultsRecord.student_name) ||
                                           '&p_student_uid=' || searchResultsRecord.student_uid ||
                                           '&p_student_instance_id=' || searchResultsRecord.student_instance_id ||
                                           '&p_dummy=' || citsss_webenr_global_pkg.urlencode(TO_CHAR(SYSDATE,'DD-MON-YYYY HH:MI:SS AM')) ||										   

                                           ''')',
                             -- '<FONT COLOR="#FFFFFF">' ||
                               g_non_breaking_spaces ||
                             -- '</FONT>' ||
                               '<STYLE> A:link { color:#0033FF } A:visited { color:#0033FF } </STYLE>' ||
                               searchResultsRecord.student_schedule_status ) ||
                             '</FONT></SMALL>', g_alignment );
          ELSE
              htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif" ' ||
                             'COLOR="' || v_font_color || '">' ||
                               g_non_breaking_spaces ||
                               searchResultsRecord.student_schedule_status ||
                             '</FONT></SMALL>', g_alignment );
          END IF;
          htp.tableRowClose;
        END LOOP;
      IF vRowCount = 0 THEN
        IF p_option_rep_or_advisor = 'O' THEN
          --  20040121 pyasumi
          --  Begin - Added table row openning tag
          htp.print( '<TR> ' );
          --  End - Added table row openning tag
          htp.print( '<TD COLSPAN=7 ALIGN=CENTER BGCOLOR=#FFFFFF>' );
          htp.print( '<FONT COLOR="#FF0000"><B>' ||
                       'There are no advisees that meet your selection criteria.' ||
                     '</B></FONT>' );
          htp.print( '</TD>' );
          --  20040121 pyasumi
          --  Begin - Added table row closing tag
          htp.print( '</TR> ' );
          --  End - Added table row closing tag
          htp.print( '<TFOOT>' );
          htp.print( '<TD COLSPAN=7 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Click on the New Search button and then enter new selection criteria.' ||
                     '</B></FONT>' );
          --  20040121 pyasumi
          --  Begin - Added table data and footer closing tags
          htp.print( '</TD></TFOOT> ' );
          --  End - Added table data and footer closing tags
        ELSE
          --  20040121 pyasumi
          --  Begin - Added table row openning tag
          htp.print( '<TR> ' );
          --  End - Added table row openning tag
          htp.print( '<TD COLSPAN=7 ALIGN=CENTER BGCOLOR=#FFFFFF>' );
          htp.print( '<FONT COLOR="#FF0000"><B>' ||
                       'There are no advisees that are assigned to you.' ||
                     '</B></FONT>' );
          htp.print( '</TD>' );
          --  20040121 pyasumi
          --  Begin - Added table row closing tag
          htp.print( '</TR> ' );
          --  End - Added table row closing tag
          htp.print( '<TFOOT>' );
          htp.print( '<TD COLSPAN=7 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Please contact the Registrar''s Office if this is incorrect.' ||
                     '</B></FONT>' );
          --  20040121 pyasumi
          --  Begin - Added table data and footer closing tags
          htp.print( '</TD></TFOOT> ' );
          --  End - Added table data and footer closing tags
        END IF;
      ELSE
        htp.print( '<TR><TD COLSPAN=7 ALIGN=CENTER BGCOLOR=#336699>' );
        htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     -- 'Click on the student''s name to review their schedule.' ||
                     'If applicable, click on the student''s name to review their schedule or<BR>' ||
                     'click on the schedule status to view the approval log.' ||
                   '</B></FONT>' );
        htp.print( '</TD></TR>' );
      END IF;
        htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table
      -- -----------------------------------------------------------------------
      htp.tableOpen( CALIGN => 'CENTER' );
      htp.tableRowOpen;
      htp.tableData( 'Result page: ' );
      IF vRowCount = 0 THEN
        htp.tableData( '<B>0</B>' );
      ELSE
        IF NOT p_page_number = htpAnchorTable.FIRST THEN
          htpAnchorRecord := htpAnchorTable(p_page_number - 1);
          htp.tableData( htf.anchor( htpAnchorRecord.curl,
                                    '<U><B>Previous</B></U>' ) );
        END IF;
        htp.print( '<TD>' );
        FOR i IN 1 .. vAnchorCount LOOP
          IF i = p_page_number THEN
            htp.print('<B>' || i || '</B>' );
          ELSE
            htpAnchorRecord := htpAnchorTable(i);
            htp.anchor( htpAnchorRecord.curl, htpAnchorRecord.ctext );
          END IF;
        END LOOP;
        htp.print( '</TD>' );
        --  pyasumi 20040131 begin
        -- IF NOT p_page_number = htpAnchorTable.LAST THEN
        IF p_page_number < htpAnchorTable.LAST THEN
        --  pyasumi 20040131 end
          htpAnchorRecord := htpAnchorTable(p_page_number + 1);
          htp.tableData( htf.anchor( htpAnchorRecord.curl,
                                    '<U><B>Next</B></U>' ) );
        END IF;
      END IF;
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed
      -- -----------------------------------------------------------------------
      htp.br;
      IF p_option_rep_or_advisor = 'O' THEN
        -- ---------------------------------------------------------------------
        --  Open Table - Buttons
        -- ---------------------------------------------------------------------
        htp.formOpen(curl=>'javascript:void(0)'
                    ,cattributes=>'name="search_form"');
        htp.tableOpen( CALIGN => 'CENTER' );
        htp.tableRowOpen;
        -- htp.tableData( '<INPUT TYPE=button VALUE="New Search"' ||
        --                ' ONCLICK="history.go(-1)">' );
        htp.print( '<TD><INPUT TYPE=button VALUE="New Search" onClick="goToURL()"></TD>' );
        htp.tableRowClose;
        htp.tableClose;
        htp.formClose;		
        htp.br;
        -- ---------------------------------------------------------------------
        --  Table Closed - Buttons
        -- ---------------------------------------------------------------------
      END IF;
      htp.bodyClose;
      htp.htmlClose;
      -- -----------------------------------------------------------------------
      --  HTML Page Closed
      -- -----------------------------------------------------------------------
    END IF;
  EXCEPTION
    WHEN others THEN
      htp.print( 'SQLCODE: ' || SQLCODE );
      htp.print( '<BR>' );
      htp.print( 'SQLERRM: ' || SQLERRM );
  END;
  -- ===========================================================================
  --  Advisee Course Schedule Frame Page
  -- ===========================================================================
  PROCEDURE advisee_course_schedule_frame
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_enrollment_term_id IN NUMBER,
    p_current_period IN VARCHAR2,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_student_year_of_study IN VARCHAR2,
    -- p_student_email_address IN VARCHAR2,
    p_student_option IN VARCHAR2,
    p_student_schedule_status IN VARCHAR2,
    p_user_person_id IN NUMBER,
    p_restore_url IN VARCHAR2
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      --  07/29/03, Dennis Lo
      --  constuct frameset
      --  frame1: viewable
      --  frame2: hidden sql frame
      --  htp.print( 'p_restore_url: ' || p_restore_url );
      citsss_webenr_updat_status_pkg.set_frame(
         p_frame1_name => 'frame1'
        ,p_frame1_src  => 'citsss_webenr_schdl_apprvl_pkg.advisee_course_schedule?'||
                          'p_key='|| v_encrypted_session_id||'&'||
                          'p_uid='|| g_session_info.caltech_uid||'&'||
                          'p_enrollment_term_id='||p_enrollment_term_id||'&'||
                          'p_current_period='||p_current_period||'&'||
                          'p_student_person_id='||p_student_person_id||'&'||
                          'p_student_name='||citsss_webenr_global_pkg.urlencode(p_student_name)||'&'||
                          'p_student_uid='||p_student_uid||'&'||
                          'p_student_instance_id='||p_student_instance_id||'&'||
                          'p_student_year_of_study='||p_student_year_of_study||'&'||
                          -- 'p_student_email_address='||p_student_email_address||'&'||
                          --  20040122 dlo ZZZZZ
                          'p_student_option='||REPLACE(REPLACE(p_student_option,' ','%20'),'&','%2526')||'&'||
                          'p_student_schedule_status='||citsss_webenr_global_pkg.urlencode(p_student_schedule_status)||'&'||
                          'p_user_person_id=' ||p_user_person_id||'&'||
                          'p_restore_url='||p_restore_url
        ,p_frame1_pct  => '100%'
        ,p_frame2_name => 'frame2'
        ,p_frame2_src  => 'citsss_webenr_updat_status_pkg.hidden_frame'
        ,p_frame2_pct  => '0%'
      );
    END IF;
  END advisee_course_schedule_frame;
  -- ===========================================================================
  --  Advisee Course Schedule Page
  -- ===========================================================================
  PROCEDURE advisee_course_schedule
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_enrollment_term_id IN NUMBER,
    p_current_period IN VARCHAR2,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_student_year_of_study IN VARCHAR2,
    -- p_student_email_address IN VARCHAR2,
    p_student_option IN VARCHAR2,
    p_student_schedule_status IN VARCHAR2,
    p_user_person_id IN NUMBER,
    p_restore_url IN VARCHAR2
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
    -- -------------------------------------------------------------------------
    --  Other Local Variables
    -- -------------------------------------------------------------------------
    v_term_name exeter.cmn_terms.term_name%TYPE;
    v_row_count NUMBER := 0;
    v_true_false BOOLEAN := FALSE;
    v_total_units NUMBER := 0;
    v_html_input_type_buttons VARCHAR2(32767);
    v_html_hyper_links VARCHAR2(32767);
    v_partial_course_schedule_url VARCHAR2(9);
    -- -------------------------------------------------------------------------
    --  Variables for Automated Email Notification
    -- -------------------------------------------------------------------------
    -- v_preferred_email_address_type cmn_people.preferred_email_address_type%TYPE;
    v_student_email_address VARCHAR2(240);
    v_student_first_name cmn_people.first_name%TYPE;
    v_email_body VARCHAR2(32767);
    -- -------------------------------------------------------------------------
    --  Information returned by the citsss_webenr_adviser_info_pkg
    -- -------------------------------------------------------------------------
    v_user_person_id exeter.cmn_people.person_id%TYPE;
    v_user_full_name exeter.cmn_people.full_name%TYPE;
    v_user_email_address exeter.cmn_people.email%TYPE;
    -- -------------------------------------------------------------------------
    --  Advisee Course Schedule
    -- -------------------------------------------------------------------------
    CURSOR c1_course_schedule
    IS
      SELECT so.offering_name course_offering,
             /*
             DECODE( ss.unit_category,
                     'V', TO_CHAR(sse.variable_units),
                     'S', NVL( RTRIM( scc.attribute05 ), '0' ) || '-' ||
                          NVL( RTRIM( scc.attribute06 ), '0' ) || '-' ||
                          NVL( RTRIM( scc.attribute07 ), '0' )) units,

             --  For Calculating the Total Units
             DECODE( ss.unit_category,
                     'V', TO_NUMBER(NVL(sse.variable_units,'0')),
                     'S', TO_NUMBER(NVL(scc.attribute05,'0')) +
                          TO_NUMBER(NVL(scc.attribute06,'0')) +
                          TO_NUMBER(NVL(scc.attribute07,'0'))) for_total_units,
             */
             DECODE(
                   ss.unit_category,
                   -- 'V',DECODE(cs.status,'Enrolled - no credit','NC',NVL(sse.variable_units,'0')),
                   -- 'S',DECODE(cs.status,'Enrolled - no credit','NC',NVL(ss.default_units,'0'))
                   'V',TO_NUMBER(NVL(sse.variable_units,'0')),
                   'S',TO_NUMBER(NVL(ss.default_units,'0'))
                   ) units,
             --  For Calculating the Total Units
             DECODE(
                   ss.unit_category,
                   'V',TO_NUMBER(NVL(sse.variable_units,'0')),
                   'S',TO_NUMBER(NVL(ss.default_units,'0'))
                   ) for_total_units,
             cs.status offering_status,
             so.offering_title offering_title,
             so.attribute05 || so.attribute06 offering_notes,
             ss.name section,
             citsss_course_offering_pkg.get_instructor_name( ct.term_name, so.offering_id, ss.section_id ) instructor,
             citsss_webenr_global_pkg.concatenated_days_and_time( ss.section_id ) days_and_time,
             citsss_webenr_global_pkg.concatenated_locations( ss.section_id ) location,
             DECODE(sgs.grade_system_name,'LETTER','G','PASS-FAIL','P') grade_scheme
        FROM cmn_terms ct,
             sss_course_catalog scc,
             sss_unit_types sut,
             sss_offerings so,
             sss_sections ss,
             sss_grade_systems sgs,
             sss_section_grade_systems ssgs,
             cmn_statuses cs,
             --  20031120 pyasumi
             --  Begin - Added the sss_enroll_prog_map and sss_student_instance
             --    tables to limit courses to correct program.
             sss_enroll_prog_map sepm,
             sss_student_instance ssi,
             --  End - Added the sss_enroll_prog_map and sss_student_instance
             --    tables to limit courses to correct program.
             sss_student_enrollments sse
       WHERE ct.term_id = so.term_id
         AND ss.offering_id = so.offering_id
         AND ss.system = so.system
         AND scc.course_catalog_id = so.course_catalog_id
         AND sut.unit_type_id(+) = so.unit_type_id
         AND ss.section_id = sse.section_id
         AND sgs.grade_system_id = ssgs.grade_system_id
         AND ssgs.section_grade_system_id = sse.section_grade_system_id
         AND so.offering_id = sse.offering_id
         AND cs.status_id = sse.enrollment_status_id
         --  20031120 pyasumi
         --  Begin - Added joins to sss_enroll_prog_map and sss_student_instance
         --    tables to limit courses to correct program.
         AND sepm.enrollment_id = sse.enrollment_id
         AND sepm.program_hierarchy_id = ssi.program_hierarchy_id
         AND ssi.person_id = sse.person_id
         --  20031209 pyasumi
         --  Begin - Added condition on sss_student_instance table
         AND ssi.student_instance_id = p_student_instance_id
         --  End - Added condition on sss_student_instance table
         --  End - Added joins to sss_enroll_prog_map and sss_student_instance
         --    tables to limit courses to correct program.
         AND sse.person_id = p_student_person_id
         AND sse.term_id = p_enrollment_term_id
         -- AND sse.enrollment_status_id IN (150, 151)
       ORDER BY so.offering_name, ss.name;
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      /*
      citsss_webenr_adviser_info_pkg.get_adviser_info
        (
        p_uid,
        v_user_person_id,
        v_user_full_name,
        v_user_email_address
        );
      */
      --  pyasumi 20040116
      --  Begin - Commnented-out and added statements
      -- v_user_full_name := citsss_webenr_global_pkg.get_full_name(p_uid);
      --  pyasumi 20040121
      --  Begin - Added literal Professor
      v_user_full_name := 'Professor ' ||
      --  End - Added literal Professor
                          citsss_webenr_global_pkg.get_first_name(p_uid) || ' ' ||
                          citsss_webenr_global_pkg.get_last_name(p_uid);
      --  End - Commnented-out and added statements
      v_user_email_address := citsss_webenr_global_pkg.get_preferred_email_address(p_user_person_id);
      -- -----------------------------------------------------------------------
      --  Get Enrollment Term and Periods Information
      --  This routine was called before.  This may cause a timing issue.
      -- -----------------------------------------------------------------------
      citsss_webenr_global_pkg.get_enrollment_term_periods
        (
        g_academic_info.enrollment_term_id,
        g_academic_info.enrollment_term_name,
        g_academic_info.enrollment_period_begins,
        g_academic_info.enrollment_period_ends,
        g_academic_info.view_only_period_begins,
        g_academic_info.view_only_period_ends,
        g_academic_info.add_day,
        g_academic_info.drop_day
        );
      v_partial_course_schedule_url := REPLACE(g_academic_info.enrollment_term_name,' ','');
      -- -----------------------------------------------------------------------
      --  Get Advisee's First Name and Email Address
      -- -----------------------------------------------------------------------
      v_student_first_name := citsss_webenr_global_pkg.get_first_name(p_student_uid);
      v_student_email_address := citsss_webenr_global_pkg.get_preferred_email_address(p_student_person_id);
      --  FOR TESTING
      -- v_student_email_address := 'caltechstudent2003@yahoo.com';
      -- v_user_email_address := 'citadviser@yahoo.com';
      -- v_student_email_address := 'paul.yasumi@caltech.edu';
      -- v_user_email_address := 'paul.yasumi@caltech.edu';
      -- v_student_email_address := 'debra.tuttle@caltech.edu';
      -- v_user_email_address := 'debra.tuttle@caltech.edu';
      -- v_student_email_address := 'rosana.gatti@caltech.edu';
      -- v_user_email_address := 'rosana.gatti@caltech.edu';
      -- htp.print( 'overridden v_student_email_address: ' || v_student_email_address);
      -- htp.print( 'overridden v_user_email_address: ' || v_user_email_address);
      -- -----------------------------------------------------------------------
      --  Open HTML Page
      -- -----------------------------------------------------------------------
      htp.htmlOpen;
      htp.headOpen;
      -- htp.print( '<META HTTP-EQUIV="Pragma" CONTENT="no-cache">' );
      -- htp.print( '<META HTTP-EQUIV="expires" CONTENT="0">' );
      -- htp.print( '<link rel="stylesheet" type="text/css" href="/OA_HTML/US/CITSSS/webenr/webenr_styles.css">' );
      -- -----------------------------------------------------------------------
      --  JavaScript Begin
      -- -----------------------------------------------------------------------
      htp.print( '
                 <SCRIPT LANGUAGE="JavaScript">
                 <!-- Disable right mouse button functionality - Begin -->
                 var rightButtonDisabled="Right-mouse button disabled!";
                 function clickIE4() {
                   if (event.button==2) {
                     alert(rightButtonDisabled);
                     return false;
                   }
                 }
                 function clickNS4(e){
                   if (document.layers||document.getElementById&&!document.all) {
                     if (e.which==2||e.which==3) {
                       alert(rightButtonDisabled);
                       return false;
                     }
                   }
                 }
                 if (document.layers) {
                   document.captureEvents(Event.MOUSEDOWN);
                   document.onmousedown=clickNS4;
                 }
                 else if (document.all&&!document.getElementById) {
                   document.onmousedown=clickIE4;
                 }
                 document.oncontextmenu=new Function("alert(rightButtonDisabled);return false")
                 <!-- Disable right mouse button functionality - End -->
                 <!-- This JavaScript function opens modeless popup windows -->
                 function windowOpen(parameter) {
                   <!-- Get screen height and width and adjust popups accordingly using a base of 768 1024 -->
                   var screenHeight = screen.height;
                   var screenWidth = screen.width;
                   var heightAdjustmentFactor;
                   var widthAdjustmentFactor;
                   <!-- alert("screen.height: " + screenHeight); -->
                   <!-- alert("screen.width: " + screenWidth); -->
                   heightAdjustmentFactor = screenHeight/768;
                   widthAdjustmentFactor = screenWidth/1024;
                   <!-- alert("heightAdjustmentFactor: " + heightAdjustmentFactor); -->
                   <!-- alert("widthAdjustmentFactor: " + widthAdjustmentFactor); -->
                   <!-- alert("parameter: " + parameter); -->
                   if (parameter == ''units'') {
                     popup = window.open("citsss_webenr_crs_off_pkg.crs_off_abbrev_units",' ||
                                         '"",' ||
                                         '"height=226,' ||
                                         'width=600,' ||
                                         'top=48,' || --  Increment tiling by 24
                                         'left=48,' || --  Increment tiling by 24
                                         'location=no,' ||
                                         'titlebar=no,' ||
                                         'scrollbars=no");
                   } else if (parameter == ''days_and_time'') {
                     popup = window.open("citsss_webenr_crs_off_pkg.crs_off_abbrev_days_and_time",' ||
                                         '"",' ||
                                         '"height=315,' ||
                                         'width=600,' ||
                                         'top=48,' || --  Increment tiling by 24
                                         'left=48,' || --  Increment tiling by 24
                                         -- 'top=72,' || --  Increment tiling by 24
                                         -- 'left=72,' || --  Increment tiling by 24
                                         'titlebar=no,' ||
                                         'scrollbars=no");
                   } else if (parameter == ''buildings'') {
                     popup = window.open("citsss_webenr_crs_off_pkg.crs_off_abbrev_bldings_frames",' ||
                                         '"",' ||
                                         '"height=" + 500*heightAdjustmentFactor + ",' ||
                                         'width=" + 800*widthAdjustmentFactor + ",' ||
                                         'top=24,' || --  Begin tiling windows
                                         'left=24,' || --  Begin tiling windows
                                         -- 'status=yes,' ||
                                         'titlebar=no,' ||
                                         'scrollbars=no,' ||
                                         'resizable=no");
                   } else if (parameter == ''grade_schemes'') {
                     popup = window.open("citsss_webenr_crs_off_pkg.crs_off_abbrev_grade_schemes",' ||
                                         '"",' ||
                                         -- '"height=249,' || -- Each row occupies 23 pixels
                                         '"height=226,' ||
                                         'width=600,' ||
                                         'top=72,' || --  Increment tiling by 24
                                         'left=72,' || --  Increment tiling by 24
                                         -- 'top=96,' || --  Increment tiling by 24
                                         -- 'left=96,' || --  Increment tiling by 24
                                         'titlebar=no,' ||
                                         'scrollbars=no");
                   } else if (parameter == ''caltech_catalog'') {
                     popup = window.open("http://pr.caltech.edu/catalog/courses/courses.html",' ||
                                         '"",' ||
                                         '"height=" + 600*heightAdjustmentFactor + ",' ||
                                         'width=" + 800*widthAdjustmentFactor + ",' ||
                                         -- '"height=468,' ||
                                         -- 'width=624,' ||
                                         'top=20,' || --  Begin tiling windows
                                         'left=20,' || --  Begin tiling windows
                                         'titlebar=yes,' ||
                                         'scrollbars=yes,' ||
                                         'resizable=yes,' ||
                                         'menubar=yes");
                   } else if (parameter == ''course_schedule'') {
                     popup = window.open("http://www.registrar.caltech.edu/schedules/' ||
                                         v_partial_course_schedule_url || '.pdf",' ||
                                         '"",' ||
                                         '"height=" + 600*heightAdjustmentFactor + ",' ||
                                         'width=" + 900*widthAdjustmentFactor + ",' ||
                                         'top=44,' || --  Increment tiling by 24
                                         'left=44,' || --  Increment tiling by 24
                                         'titlebar=yes,' ||
                                         'scrollbars=yes,' ||
                   --  pyasumi 20040116
                   --  Begin - Commented-out and added statements 
                                         -- 'resizable=yes");'
                                         'resizable=yes");
                   } else if (parameter == ''uash_forms'') {
                     popup = window.open("http://www.registrar.caltech.edu/uash/index.htm",' ||
                                         '"",' ||
                                         '"height=" + 550*heightAdjustmentFactor + ",' ||
                                         'width=" + 900*widthAdjustmentFactor + ",' ||
                                         'top=96,' || --  Increment tiling by 24
                                         'left=96,' || --  Increment tiling by 24
                                         'titlebar=yes,' ||
                                         'scrollbars=yes,' ||
                                         'resizable=yes");'
                   --  pyasumi 20040116
                   --  End - Commented-out and added statements 
               );
      IF p_student_year_of_study = 'Freshman' OR
        p_student_year_of_study = 'Sophomore' OR
        p_student_year_of_study = 'Junior' OR
        p_student_year_of_study = 'Senior' OR
        p_student_year_of_study = 'Fifth Year +' THEN
          htp.print( '                  ' ||
                     ' } else if (parameter == ''institute_requirements'') {
                     popup = window.open("http://pr.caltech.edu/catalog/uginfo/gradreq.html",' ||
                                             '"",' ||
                                             '"height=" + 500*heightAdjustmentFactor + ",' ||
                                             'width=" + 755*widthAdjustmentFactor + ",' ||
                                             'top=68,' || --  Increment tiling by 24
                                             'left=68,' || --  Increment tiling by 24
                                             'titlebar=yes,' ||
                                             'scrollbars=yes,' ||
                                             'menubar=yes,' ||
                                             'resizable=yes");
                   }
                 }' );
      ELSE
        htp.print( '                  ' ||
                   ' } else if (parameter == ''institute_requirements'') {
                   popup = window.open("http://pr.caltech.edu/catalog/gradinfo/degreq.html",' ||
                                           '"",' ||
                                           '"height=" + 500*heightAdjustmentFactor + ",' ||
                                           'width=" + 755*widthAdjustmentFactor + ",' ||
                                           'top=68,' || --  Increment tiling by 24
                                           'left=68,' || --  Increment tiling by 24
                                           'titlebar=yes,' ||
                                           'scrollbars=yes,' ||
                                           'menubar=yes,' ||
                                           'resizable=yes");
                 }
               }' );
      END IF;
      IF p_current_period = 'Enrollment' AND
        (p_student_schedule_status = 'Pending Approval' OR
        p_student_schedule_status = 'Not Approved') THEN
          htp.print( '                 var button;
                 function verifyIt() {
                   //  20031120 pyasumi
                   //  Begin - Commented out edits for check box
                   //  20031120 pyasumi if (document.studentSchedule.discussed.checked == false) {
                     // alert(discussed.checked)
                     //  20031120 pyasumi alert("Have you discussed this schedule with your advisee?  If so, please mark the checkbox above the Approve button.")
                   //  20031120 pyasumi } else {
                   //  End - Commented out edits for check box
                     // alert(discussed.checked)
                     // alert(button)
                     // Which button did they press
                     //if (approve.onclick == true) {
      	             if (button == "approve") {' );
          IF p_student_schedule_status = 'Pending Approval' THEN
            htp.print( '                       document.studentSchedule.approve.disabled=true;
                       document.studentSchedule.do_not_approve.disabled=true;
                       document.studentSchedule.cancel.disabled=true;' );
          ELSE
            htp.print( '                       document.studentSchedule.approve.disabled=true;
                       document.studentSchedule.cancel.disabled=true;' );
          END IF;
          htp.print( '                       alert("Your approval has been accepted. Your advisee will be automatically notified of this action by email.")
                       parent.frame2.email(''email''' );
          htp.print( ',''' || v_student_email_address || '''' );
          htp.print( ',''' || 'regis@caltech.edu' || '''' );
          v_email_body :=
                     'Dear ' ||
                     REPLACE(v_student_first_name,'''','\''') ||
                     ',' || '\r\n' ||
                     '\n' ||
                     'Congratulations! Your student schedule has been approved by ' ||
                     REPLACE(v_user_full_name,'''','\''') ||
                     ' on ' || TO_CHAR(SYSDATE,'fmMonth DD, YYYY HH:fmMI AM') || '.' || '\r\n' ||
                     '\n' ||
                     'You may continue to make changes online to your schedule until the end of the enrollment period (' ||
                     TO_CHAR(g_academic_info.enrollment_period_ends,'DD-MON-YYYY HH:MI AM') || '); ' ||
                     'however, you will need to resubmit your modified schedule to your adviser and obtain approval.' || '\r\n' ||
                     '\n' ||
                     'Web Enrollment Logon: https://cookisland.caltech.edu:9030/OA_HTML/US/ICXINDEX.htm (Requires Microsoft Internet Explorer 5.5 or higher)' || '\r\n' ||
                     '\n' ||
                     'Any changes to your schedule after the online enrollment period has ended will require submission of an Add/Drop Card to the Registrar\''s Office.' || '\r\n' ||
                     '\n' ||
                     'The last day for adding courses and removing conditions and incompletes is ' ||
                     TO_CHAR(g_academic_info.add_day,'DD-MON-YYYY') || '.' || '\r\n' ||
                     'The last day for dropping courses, exercising pass/fail option, and changing sections is ' ||
                     TO_CHAR(g_academic_info.drop_day,'DD-MON-YYYY') || '.' || '\r\n' ||
                     '\n' ||
                     'If you have any questions regarding Web Enrollment, please contact the Registrar\''s Web Enrollment Hotline at x8866.' || '\r\n' ||
                     '\n' ||
                     '\n' ||
                     '-----------------------------------' || '\r\n' ||
                     'Office of the Registrar' || '\r\n' ||
                     'California Institute of Technology' || '\r\n' ||
                     'Room 125 Center for Student Affairs' || '\r\n' ||
                     '414 S. Holliston Ave.' || '\r\n' ||
                     'Pasadena CA 91125' ||
                     '''';
          htp.print( ',''' || v_email_body );
          htp.print( ',''Caltech Web Enrollment - Student Schedule' || '''' );
          htp.print( ',''' || p_enrollment_term_id || '''' );
          htp.print( ',''' || p_student_person_id || '''' );
          htp.print( ',''' || p_student_instance_id || '''' );
          htp.print( ',''' || p_user_person_id || '''' );
          htp.print( ',''' || 'Approved' || '''' );
          --  20040122 pyasumi									 
          htp.print( ',''' || replace(replace(p_restore_url,' & ',' %26 '),' ','%20') || '''' );
          htp.print( ',''' || v_user_email_address || '''' );
          htp.print( ',''' ||
                     'This is confirmation of the email sent to your advisee (' ||
                     v_student_email_address || ').' ||
                     '\r\n' ||
                     '\n' || v_email_body );
          htp.print( ');' );
          htp.print( '//document.forms[0].submit()
                     } else if (button == "do_not_approve") {
                       document.studentSchedule.approve.disabled=true;
                       document.studentSchedule.do_not_approve.disabled=true;
                       document.studentSchedule.cancel.disabled=true;
                       alert("Your decision to not approve this schedule has been accepted. Your advisee will be automatically notified of this action by email.")
                       parent.frame2.email(''email''' );
          htp.print( ',''' || v_student_email_address || '''' );
          htp.print( ',''' || 'regis@caltech.edu' || '''' );
          v_email_body :=
                     'Dear ' ||
                     REPLACE(v_student_first_name,'''','\''') ||
                     ',' || '\r\n' ||
                     '\n' ||
                     'Your student schedule was reviewed by your adviser, ' ||
                     REPLACE(v_user_full_name,'''','\''') ||
                     ', on ' || TO_CHAR(SYSDATE,'fmMonth DD, YYYY HH:fmMI AM') || ', and was not approved.' || '\r\n' ||
                     '\n' ||
                     'Please contact your adviser immediately to discuss your schedule.' || '\r\n' ||
                     '\n' ||
                     'You may continue to make changes online to your schedule until the end of the enrollment period (' ||
                     TO_CHAR(g_academic_info.enrollment_period_ends,'DD-MON-YYYY HH:MI AM') || '); ' ||
                     'however, you will need to resubmit your modified schedule to your adviser and obtain approval.' || '\r\n' ||
                     '\n' ||
                     'Web Enrollment Logon: https://cookisland.caltech.edu:9030/OA_HTML/US/ICXINDEX.htm (Requires Microsoft Internet Explorer 5.5 or higher)' || '\r\n' ||
                     '\n' ||
                     'Any changes to your schedule after the online enrollment period has ended will require submission of an Add/Drop Card to the Registrar\''s Office.' || '\r\n' ||
                     '\n' ||
                     'The last day for adding courses and removing conditions and incompletes is ' ||
                     TO_CHAR(g_academic_info.add_day,'DD-MON-YYYY') || '.' || '\r\n' ||
                     'The last day for dropping courses, exercising pass/fail option, and changing sections is ' ||
                     TO_CHAR(g_academic_info.drop_day,'DD-MON-YYYY') || '.' || '\r\n' ||
                     '\n' ||
                     'If you have any questions regarding Web Enrollment, please contact the Registrar\''s Web Enrollment Hotline at x8866.' || '\r\n' ||
                     '\n' ||
                     '\n' ||
                     '-----------------------------------' || '\r\n' ||
                     'Office of the Registrar' || '\r\n' ||
                     'California Institute of Technology' || '\r\n' ||
                     'Room 125 Center for Student Affairs' || '\r\n' ||
                     '414 S. Holliston Ave.' || '\r\n' ||
                     'Pasadena CA 91125' ||
                     '''';
          htp.print( ',''' || v_email_body );
          htp.print( ',''Caltech Web Enrollment - Student Schedule' || '''' );
          htp.print( ',''' || p_enrollment_term_id || '''' );
          htp.print( ',''' || p_student_person_id || '''' );
          htp.print( ',''' || p_student_instance_id || '''' );
          htp.print( ',''' || p_user_person_id || '''' );
          htp.print( ',''' || 'Not Approved' || '''' );
          --  20040122 pyasumi							 
          htp.print( ',''' || replace(replace(p_restore_url,' & ',' %26 '),' ','%20') || '''' );
          htp.print( ',''' || v_user_email_address || '''' );
          htp.print( ',''' ||
                     'This is confirmation of the email sent to your advisee (' ||
                     v_student_email_address || ').' ||
                     '\r\n' ||
                     '\n' || v_email_body );
          htp.print( ');' );
          htp.print( '// alert("1" + document.forms[1].name)
                           //document.forms[1].submit()
                         }
                         // alert("2" + document.forms[2].name)
                       }
                     //  20031120 pyasumi
                     //  Begin - Commented out ending } for verifying check box.
                     // }
                     //  Begin - Commented out ending } for verifying check box.
                     ' );
      END IF;
      htp.print( '
                 </SCRIPT>
                 ' );
      -- -----------------------------------------------------------------------
      --  JavaScript End
      -- -----------------------------------------------------------------------
      htp.headClose;
      htp.bodyOpen( CATTRIBUTES=>'BGCOLOR="#CCCCCC", TEXT="#000000"' );
      htp.print( '<FORM name="studentSchedule">' );
      -- htp.header( 4,'Student Schedule','CENTER' );
      /*
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                     'COLOR="#336699"><B><I>' ||
                     'Student Schedule' ||
                     '</I></B></FONT>',
                     CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      --
      */
      -- -----------------------------------------------------------------------
      --  Open Table - Academic Term
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="45%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Academic Term:&nbsp;' || -- Literal to be displayed
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                     'BGCOLOR=#FFFFFF">' ||
                      '&nbsp;' ||
                       -- v_term_name || -- Variable to be displayed
                       g_academic_info.enrollment_term_name ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="50%"' ||
                                      'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      -- -----------------------------------------------------------------------
      --  Obtain remaining student information not passed as parameters
      -- -----------------------------------------------------------------------
      citsss_webenr_global_pkg.get_academic_info
        (
        p_student_uid,
        g_academic_info
        );
      -- -----------------------------------------------------------------------
      --  Open Table - Student Information Table
      -- -----------------------------------------------------------------------
      -- IF g_academic_info.second_option = 'No option found' THEN
      IF g_academic_info.second_option IS NULL THEN
        htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                         'Student Name:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699" ' ||
                       'WIDTH="19%"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif" ' ||
                       'COLOR="#000000">' ||
                        '&nbsp;' ||
                         p_student_name ||
                       '</FONT></SMALL>',
                         CATTRIBUTES => 'WIDTH="31%" ' ||
                                        'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                         'Year of Study:&nbsp;' ||
                       '</B></FONT>',
                         CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                         '&nbsp;' ||
                         p_student_year_of_study ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableRowClose;
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'UID:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699" ' ||
                       'WIDTH="19%"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif" ' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_uid ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="31%" ' ||
                       'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Option:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_option ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableRowClose;
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Email:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '&nbsp;' ||
                       '<A HREF="mailto:' ||
                       g_academic_info.caltech_email ||
                       -- '?subject=Schedule Approval for ' || v_term_name ||
                       '?subject=Schedule Approval for ' || g_academic_info.enrollment_term_name ||
                       '">' ||
                       '<FONT SIZE=2 FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       g_academic_info.caltech_email ||
                       '</FONT></A>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Adviser:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        IF g_academic_info.first_advisor_email IS NOT NULL THEN
          htp.tableData( '&nbsp;' ||
		                 '<A HREF="mailto:' ||
                         g_academic_info.first_advisor_email ||
                         -- '?subject=Schedule Approval for ' || v_term_name ||
                         '?subject=Schedule Approval for ' || g_academic_info.enrollment_term_name ||
                         '">' ||
                         '<FONT SIZE=2 FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         g_academic_info.first_advisor ||
                         '</FONT></A>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        ELSE
          htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         '&nbsp;' ||
                         g_academic_info.first_advisor ||
                         '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        END IF;
        htp.tableRowClose;
        htp.tableClose;
      END IF;
      -- IF g_academic_info.second_option <> 'No option found' THEN
      IF g_academic_info.second_option IS NOT NULL THEN
        htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Student Name:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699" ' ||
                       'WIDTH="19%"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif" ' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_name ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="31%" ' ||
                       'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Option:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_option ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableRowClose;
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'UID:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699" ' ||
                       'WIDTH="19%"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif" ' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_uid ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="31%" ' ||
                       'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Adviser:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        IF g_academic_info.first_advisor_email IS NOT NULL THEN
          htp.tableData( '&nbsp;' ||
		                 '<A HREF="mailto:' ||
                         g_academic_info.first_advisor_email ||
                         -- '?subject=Schedule Approval for ' || v_term_name ||
                         '?subject=Schedule Approval for ' || g_academic_info.enrollment_term_name ||
                         '">' ||
                         '<FONT SIZE=2 FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         g_academic_info.first_advisor ||
                         '</FONT></A>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        ELSE
          htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         '&nbsp;' ||
                         g_academic_info.first_advisor ||
                         '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        END IF;
        htp.tableRowClose;
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Email:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '&nbsp;' ||
                       '<A HREF="mailto:' ||
                       g_academic_info.caltech_email ||
                       -- '?subject=Schedule Approval for ' || v_term_name ||
                       '?subject=Schedule Approval for ' || g_academic_info.enrollment_term_name ||
                       '">' ||
                       '<FONT SIZE=2 FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       g_academic_info.caltech_email ||
                       '</FONT></A>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Second Option:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       g_academic_info.second_option ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableRowClose;
        htp.tableRowOpen;
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Year of Study:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                       'COLOR="#000000">' ||
                       '&nbsp;' ||
                       p_student_year_of_study ||
                       '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Second Option Adviser:&nbsp;' ||
                       '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
        IF g_academic_info.second_advisor_email IS NOT NULL THEN
          htp.tableData( '&nbsp;' ||
		                 '<A HREF="mailto:' ||
                         g_academic_info.second_advisor_email ||
                         -- '?subject=Schedule Approval for ' || v_term_name ||
                         '?subject=Schedule Approval for ' || g_academic_info.enrollment_term_name ||
                         '">' ||
                         '<FONT SIZE=2 FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         g_academic_info.second_advisor ||
                         '</FONT></A>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        ELSE
          htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                         'COLOR="#000000">' ||
                         '&nbsp;' ||
                         g_academic_info.second_advisor ||
                         '</FONT></SMALL>',
                         CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
        END IF;
        htp.tableRowClose;
        htp.tableClose;
      END IF;
      -- -----------------------------------------------------------------------
      --  Table Closed - Student Information
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Advisee Course Schedule
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen( CATTRIBUTES => 'BGCOLOR="#336699"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Course<BR>' ||
                       g_non_breaking_spaces ||
                       'Offering' ||
                       g_non_breaking_spaces ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      /*
      htp.print( '<TH ALIGN="' || 'CENTER' || '">' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 '<A HREF="javascript:windowOpen(''units'');">' ||
                 '<STYLE> A:link { color:#FFFFFF } A:visited { color:#FFFFFF } </STYLE>' ||
                 'Units</A>' ||
                 g_non_breaking_spaces ||
                 '</TH>' );
      */
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Units' ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || 'CENTER' || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Status' ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || 'CENTER' || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Offering Title' ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Section' ||
                       g_non_breaking_spaces ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || 'CENTER' || '"' );
      htp.tableHeader( '<FONT COLOR="#FFFFFF">' ||
                       g_non_breaking_spaces ||
                       'Instructor' ||
                       g_non_breaking_spaces ||
                       '</FONT>',
                       CATTRIBUTES => 'ALIGN="' || g_alignment || '"' );
      htp.print( '<TH ALIGN="' || g_alignment || '">' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 '<A HREF="javascript:windowOpen(''days_and_time'');">' ||
                 '<STYLE> A:link { color:#FFFFFF } A:visited { color:#FFFFFF } </STYLE>' ||
                 'Days/Time</A>' ||
                 '</TH>' );
      htp.print( '<TH ALIGN="' || g_alignment || '">' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 '<A HREF="javascript:windowOpen(''buildings'');">' ||
                 '<STYLE> A:link { color:#FFFFFF } A:visited { color:#FFFFFF } </STYLE>' ||
                 'Location</A>' ||
                 '</TH>' );
      htp.print( '<TH ALIGN="' || 'CENTER' || '">' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 '<A HREF="javascript:windowOpen(''grade_schemes'');">' ||
                 '<STYLE> A:link { color:#FFFFFF } A:visited { color:#FFFFFF } </STYLE>' ||
                 'Grade' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '<BR>' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 'Scheme' ||
                 '<FONT COLOR="#336699">' ||
                 g_non_breaking_spaces ||
                 '</FONT>' ||
                 '</A>' ||
                 '</TH>' );
      htp.tableRowClose;
      FOR course_schedule_rec IN c1_course_schedule LOOP
         v_row_count := v_row_count + 1;
         IF v_true_false THEN
           htp.tableRowOpen(CATTRIBUTES => 'BGCOLOR="LIGHTYELLOW"' );
         ELSE
           htp.tableRowOpen(CATTRIBUTES => 'BGCOLOR="WHITE"' );
         END IF;
         v_true_false := NOT v_true_false;
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        g_non_breaking_spaces ||
                        course_schedule_rec.course_offering ||
                        '</FONT></SMALL>', g_alignment );
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        course_schedule_rec.units ||
                        '</FONT></SMALL>', 'CENTER' );
         IF course_schedule_rec.offering_status = 'Enrolled' THEN
           htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                          course_schedule_rec.offering_status ||
                          '</FONT></SMALL>', 'CENTER' );
         ELSE
           htp.tableData( '<SMALL><FONT COLOR="#FF0000"' ||
                          'FACE="Arial, Helvetica, sans-serif">' ||
                          course_schedule_rec.offering_status ||
                          '</FONT></SMALL>', 'CENTER' );
         END IF;
         IF course_schedule_rec.offering_notes IS NOT NULL THEN
           htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                          g_non_breaking_spaces ||
                          course_schedule_rec.offering_title ||
                          '<BR>' ||
                          g_non_breaking_spaces ||
                          '<SMALL><I>' ||
                          course_schedule_rec.offering_notes ||
                          '</I></SMALL>' ||
                          '</FONT></SMALL>', g_alignment );
         ELSE
           htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                          g_non_breaking_spaces ||
                          course_schedule_rec.offering_title ||
                          '</FONT></SMALL>', g_alignment );
         END IF;
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        course_schedule_rec.section ||
                        '</FONT></SMALL>', 'CENTER' );
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        g_non_breaking_spaces ||
                        course_schedule_rec.instructor ||
                        '</FONT></SMALL>', g_alignment );
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        g_non_breaking_spaces ||
                        course_schedule_rec.days_and_time ||
                        '</FONT></SMALL>', g_alignment,
                        CATTRIBUTES => 'WIDTH="17%"' );
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        g_non_breaking_spaces ||
                        course_schedule_rec.location ||
                        '</FONT></SMALL>', g_alignment,
                        CATTRIBUTES => 'NOWRAP="NOWRAP"' );
         htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                        g_non_breaking_spaces ||
                        course_schedule_rec.grade_scheme ||
                        '</FONT></SMALL>', 'CENTER' );
         htp.tableRowClose;
         -- --------------------------------------------------------------------
         --  If offerings status is enrolled or enrolled - no credit, add to
         --    total units
         -- --------------------------------------------------------------------
         IF course_schedule_rec.offering_status = 'Enrolled' OR
           course_schedule_rec.offering_status = 'Enrolled - no credit' THEN
             v_total_units := v_total_units + course_schedule_rec.for_total_units;
         END IF;
      END LOOP;
      IF v_row_count = 0 THEN
        htp.print( '<TR>');
        htp.print( '<TD COLSPAN=9 ALIGN="CENTER" BGCOLOR="#FFFFFF">' );
        htp.print( '<FONT COLOR="#FF0000"><B>' ||
                   'Student has no course offerings on their schedule.' ||
                   '</B></FONT>' );
        htp.print( '</TD>' );
        htp.print( '</TR>');
      ELSE
        htp.print( '<TR BGCOLOR="#FFFFFF">');
        htp.tableData( '<B>Total Units</B>',
                       CATTRIBUTES => 'ALIGN="LEFT" WIDTH="75"'  );
        IF v_total_units < 36 THEN
          -- htp.tableData( '<B><SMALL><FONT COLOR="#FF0000" FACE="Arial, Helvetica, sans-serif">' ||
          htp.tableData( '<B><SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                         v_total_units || '</FONT></SMALL></B>',
                         CATTRIBUTES => 'ALIGN="CENTER"' );
          --  20031120 pyasumi
          --  Begin - Expanded column span of table data from 5 to 7.
          htp.print( '<TD COLSPAN=7 ALIGN="CENTER">' ||
          --  End - Expanded column span of table data from 5 to 7.
                       '<B><FONT COLOR="#FF0000"> Advisee''s schedule is in an underload condition.</FONT></B>' ||
                       --  20031120 pyasumi
                       --  Begin - Added &nbsp;'s to correct alignment by changing
                       --    colspan from 5 to 7.
                       '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
                       '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
                       '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
                       '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ||
                       --  End - Added &nbsp;'s to correct alignment by changing
                       --    colspan from 5 to 7.
                     '</TD>' );
          --  20031120 pyasumi
          --  Begin - Removed last table data.
          -- htp.print( '<TD COLSPAN=2>' || '&nbsp;' || '</TD>' );
          --  End - Removed last table data.
        ELSE
          htp.tableData( '<B><SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                         v_total_units || '</FONT></SMALL></B>',
                         CATTRIBUTES => 'ALIGN="CENTER"' );
          htp.print( '<TD COLSPAN=7>' || '&nbsp;' || '</TD>' );
        END IF;
        htp.print( '</TR>');
      END IF;
      -- -----------------------------------------------------------------------
      --  Display appropriate table footer for student schedule status and
      --    enrollment period
      -- -----------------------------------------------------------------------
      /*
      IF p_student_schedule_status = 'In Progress' THEN
        htp.print( '<TFOOT>' );
        htp.print( '<TR>');
        htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
        htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                   'Advisee''s schedule is In Progress' ||
                   '</B></FONT>' );
      ELSIF p_student_schedule_status = 'Pending Approval' THEN
        htp.print( '<TFOOT>' );
        htp.print( '<TR>');
        htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
        htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                   'Remember to discuss this schedule with your advisee.' ||
                   '</B></FONT>' );
        htp.print( '</TD></TR></TFOOT>');
      ELSIF p_student_schedule_status = 'Approved' THEN
      */
      IF p_student_schedule_status = 'Approved' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'Advisee''s schedule is' ||
                     -- '</B></FONT>' ||
                     -- '<FONT COLOR="#FF0000"><B>' ||
                     ' Approved.' ||
                     '</B></FONT>' );
          htp.print( '</TD></TR></TFOOT>');
      END IF;
      IF p_current_period = 'Enrollment' THEN
        IF p_student_schedule_status = 'In Progress' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'Advisee''s schedule is In Progress.' ||
                     '</B></FONT>' );
        ELSIF p_student_schedule_status = 'Pending Approval' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'Remember to discuss this schedule with your advisee.' ||
                     '</B></FONT>' );
          htp.print( '</TD></TR></TFOOT>');
        ELSIF p_student_schedule_status = 'Not Approved' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'Advisee''s schedule is Not Approved.' ||
                     '</B></FONT>' );
          htp.print( '</TD></TR></TFOOT>');
        END IF;
      ELSIF p_current_period = 'View-Only' THEN
        IF p_student_schedule_status = 'In Progress' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'During the View-Only Period, all schedule status updates must be handled through the Registrar''s Office.' ||
                     '<BR>' ||
                     'Advisee''s schedule is In Progress.' ||
                     '</B></FONT>' );
        ELSIF p_student_schedule_status = 'Pending Approval' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'During the View-Only Period, all schedule status updates must be handled through the Registrar''s Office.' ||
                     '<BR>' ||
                     'Advisee''s schedule is Pending Approval.' ||
                     '</B></FONT>' );
          htp.print( '</TD></TR></TFOOT>');
        ELSIF p_student_schedule_status = 'Not Approved' THEN
          htp.print( '<TFOOT>' );
          htp.print( '<TR>');
          htp.print( '<TD COLSPAN=9 ALIGN=CENTER BGCOLOR=#336699>' );
          htp.print( '<FONT COLOR="#FFFFFF"><B>' ||
                     'During the View-Only Period, all schedule status updates must be handled through the Registrar''s Office.' ||
                     '<BR>' ||
                     'Advisee''s schedule is Not Approved.' ||
                     '</B></FONT>' );
          htp.print( '</TD></TR></TFOOT>');
        END IF;
      END IF;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Student Course Schedule
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Check Box
      -- -----------------------------------------------------------------------
      IF p_current_period = 'Enrollment' THEN
        IF p_student_schedule_status = 'Pending Approval' OR
          p_student_schedule_status = 'Not Approved' THEN
            htp.tableOpen( CALIGN => 'CENTER' );
            htp.tableRowOpen;
            htp.tableData( htf.formCheckBox( CNAME => 'discussed',
                                             CVALUE => 'check_on_off' ) ||
                           '&nbsp;I have discussed this schedule with my advisee.' );
            htp.tableRowClose;
            htp.tableClose;
            htp.br;
        END IF;
      END IF;
      -- -----------------------------------------------------------------------
      --  Table Closed - Check Box
      -- -----------------------------------------------------------------------
      -- htp.br;
      -- -----------------------------------------------------------------------
      --  Buttons for Page
      -- -----------------------------------------------------------------------
      -- IF p_student_schedule_status = 'Pending Approval' OR
      --   p_student_schedule_status = 'Not Approved' THEN
      --     htp.formOpen('citsss_webenr_schdl_apprvl_pkg.update_schedule_status');
      --     htp.formHidden('p_student_person_id',p_student_person_id);
      -- END IF;
      -- -----------------------------------------------------------------------
      --  Open Table - Buttons
      -- -----------------------------------------------------------------------
      htp.tableOpen( CALIGN => 'CENTER' );
      htp.tableRowOpen;
      v_html_input_type_buttons := '';
      IF p_current_period = 'Enrollment' THEN
        IF p_student_schedule_status = 'Pending Approval' OR
          p_student_schedule_status = 'Not Approved' THEN
            htp.formHidden('p_action','Approved');
            v_html_input_type_buttons := '<INPUT TYPE="button" NAME="approve" ' ||
                                         'VALUE="Approve" ' ||
                                         'ONCLICK="button=''approve'';verifyIt()">&nbsp;';
        END IF;
        IF p_student_schedule_status = 'Pending Approval' THEN
          htp.formHidden('p_action','Not Approved');
          v_html_input_type_buttons := v_html_input_type_buttons ||
                                       '<INPUT TYPE="button" NAME="do_not_approve" ' ||
                                       'VALUE="Do Not Approve" ' ||
                                       'ONCLICK="button=''do_not_approve'';verifyIt()">&nbsp;';
        END IF;
        IF p_student_schedule_status = 'Pending Approval' OR
          p_student_schedule_status = 'Not Approved' THEN
          v_html_input_type_buttons := v_html_input_type_buttons ||
                                       '<INPUT TYPE=BUTTON NAME="cancel" VALUE="Cancel" ' ||
--                                       'ONCLICK="history.go(-1)">';
--  20040122 dlo ZZZZZ
                                       'ONCLICK="window.location='''||replace(replace(p_restore_url,' & ',' %26 '),' ','%20')||'''">';
        ELSE
          v_html_input_type_buttons := v_html_input_type_buttons ||
                                       '<INPUT TYPE=BUTTON VALUE="Close Window"' ||
--                                       'ONCLICK="history.go(-1)">';
--  20040122 dlo ZZZZZ									   
                                       'ONCLICK="window.location='''||replace(replace(p_restore_url,' & ',' %26 '),' ','%20')||'''">';

        END IF;
      ELSE
        v_html_input_type_buttons := v_html_input_type_buttons ||
                                     '<INPUT TYPE=BUTTON VALUE="Close Window"' ||
--                                     'ONCLICK="history.go(-1)">';
--  20040122 dlo ZZZZZ									 
                                     'ONCLICK="window.location='''||replace(replace(p_restore_url,' & ',' %26 '),' ','%20')||'''">';
      END IF;
      htp.tableData( v_html_input_type_buttons );
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Buttons
      -- -----------------------------------------------------------------------
      /*
      -- -----------------------------------------------------------------------
      --  Open Table - Hyper-Links
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen;
      htp.tableData( '<A HREF="javascript:windowOpen(''caltech_catalog'');">' ||
                     -- '<STYLE> A:link { color:#000000 } A:visited { color:#000000 } </STYLE>' ||
                     '<SMALL><B><FONT COLOR="#0033CC" FACE="Arial, Helvetica, sans-serif"' ||
                     '>' ||
                     'Course Descriptions in Caltech Catalog' ||
                     '</FONT></B></SMALL>' ||
                     '</A>' );
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<A HREF="javascript:windowOpen(''course_schedule'');">' ||
                     -- '<STYLE> A:link { color:#000000 } A:visited { color:#000000 } </STYLE>' ||
                     '<SMALL><B><FONT COLOR="#0033CC" FACE="Arial, Helvetica, sans-serif"' ||
                     '>' ||
                     'Online Course Schedule' ||
                     '</FONT></B></SMALL>' ||
                     '</A>' );
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<A HREF="javascript:windowOpen(''institute_requirements'');">' ||
                     -- '<STYLE> A:link { color:#000000 } A:visited { color:#000000 } </STYLE>' ||
                     '<SMALL><B><FONT COLOR="#0033CC" FACE="Arial, Helvetica, sans-serif"' ||
                     '>' ||
                     'Institute Requirements' ||
                     '</FONT></B></SMALL>' ||
                     '</A>' );
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Hyper-Links
      -- -----------------------------------------------------------------------
      */
      -- -----------------------------------------------------------------------
      --  Open Table - Hyper-Links
      -- -----------------------------------------------------------------------
      v_html_hyper_links := '<A HREF="javascript:windowOpen(''caltech_catalog'');">' ||
                              '<FONT COLOR="#0033CC">' ||
                                'Course Descriptions in Caltech Catalog' ||
                              '</FONT>' ||
                            '</A>' ||
                          '&nbsp;|&nbsp;' ||
                            htf.img('/OA_MEDIA/CITSSS/webenr/pdf_icon.gif','absbottom') ||
                            '&nbsp;' ||
                            '<A HREF="javascript:windowOpen(''course_schedule'');">' || -- Add PDF Message
                              '<FONT COLOR="#0033CC">' ||
                                'Online Course Schedule' ||
                              '</FONT>' ||
                            '</A>' ||
                              '<FONT COLOR="#0033CC">' ||
                              -- '&nbsp(PDF format)' ||
                              -- '&nbsp;' ||
                              -- htf.img('/OA_MEDIA/CITSSS/webenr/pdf_icon.gif','BOTTOM') ||
                              '</FONT>' ||
                          '&nbsp;|&nbsp;' ||
                            '<A HREF="javascript:windowOpen(''institute_requirements'');">' ||
                              '<FONT COLOR="#0033CC">' ||
                                'Institute Requirements' ||
                              '</FONT>' ||
                            --  pyasumi 20040116
                            --  Begin - Commented-out and added statements 
                            -- '</A>';
                            '</A>' ||
                          '&nbsp;|&nbsp;' ||
                            '<A HREF="javascript:windowOpen(''uash_forms'');">' ||
                              '<FONT COLOR="#0033CC">' ||
                                'UASH Forms' ||
                              '</FONT>' ||
                            '</A>';
                            --  End - Commented-out and added statements 
      htp.br;
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen;
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif">' ||
                       v_html_hyper_links ||
                     '</FONT></SMALL>', 'CENTER'  );
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Hyper-Links
      -- -----------------------------------------------------------------------
      -- */
      htp.formClose;
      htp.bodyClose;
      htp.htmlClose;
      -- -----------------------------------------------------------------------
      --  HTML Page Closed
      -- -----------------------------------------------------------------------
    END IF;
  EXCEPTION
    WHEN others THEN
      htp.print( 'SQLCODE: ' || SQLCODE );
      htp.print( '<BR>' );
      htp.print( 'SQLERRM: ' || SQLERRM );
  END;
  -- ===========================================================================
  --  Advisor Approval Log Page
  -- ===========================================================================
  PROCEDURE advisor_approval_log
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2,
    p_term_id IN NUMBER,
    p_student_person_id IN NUMBER,
    p_student_name IN VARCHAR2,
    p_student_uid IN VARCHAR2,
    p_student_instance_id IN NUMBER,
    p_dummy IN VARCHAR2
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
    -- -------------------------------------------------------------------------
    --  Other Local Variables
    -- -------------------------------------------------------------------------
    v_term_name cmn_terms.term_name%TYPE;
    v_schedule_status citsss_webenr_schedule_status.schedule_status%TYPE;
    v_reviewed_by cmn_people.full_name%TYPE;
    v_reviewed_date VARCHAR2(11);
    v_reviewed_time VARCHAR2(8);
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      BEGIN
        SELECT ct.term_name
          INTO v_term_name
          FROM cmn_terms ct
         WHERE ct.term_id = p_term_id;
      EXCEPTION
        WHEN others THEN
          htp.print( 'SQLCODE: ' || SQLCODE );
          htp.print( '<BR>' );
          htp.print( 'SQLERRM: ' || SQLERRM );
      END;
      BEGIN
        SELECT cwss.schedule_status,
               cp.full_name,
               TO_CHAR(cwss.date_reviewed,'DD-MON-YYYY') reviewed_date,
               TO_CHAR(cwss.date_reviewed,'HH:MI AM') reviewed_time
          INTO v_schedule_status,
               v_reviewed_by,
               v_reviewed_date,
               v_reviewed_time
          FROM citsss_webenr_schedule_status cwss,
               cmn_people cp
         WHERE cp.person_id = cwss.reviewer_person_id
           AND cwss.term_id = p_term_id
           AND cwss.person_id = p_student_person_id
           AND cwss.student_instance_id = p_student_instance_id
           AND cwss.date_reviewed = (SELECT MAX(date_reviewed)
                                       FROM citsss_webenr_schedule_status cwss
                                      WHERE cwss.term_id = p_term_id
                                        AND cwss.person_id = p_student_person_id
                                        AND cwss.student_instance_id = p_student_instance_id);
      EXCEPTION
        WHEN others THEN
          htp.print( 'SQLCODE: ' || SQLCODE );
          htp.print( '<BR>' );
          htp.print( 'SQLERRM: ' || SQLERRM );
      END;
      htp.htmlOpen;
      htp.headOpen;
      -- htp.print( '<META HTTP-EQUIV="Pragma" CONTENT="no-cache">' );
      -- htp.print( '<META HTTP-EQUIV="expires" CONTENT="0">' );
      htp.br;
      htp.title( 'Adviser Approval Log' );
      htp.headClose;
      htp.bodyOpen( CATTRIBUTES=>'BGCOLOR="#CCCCCC", TEXT="#000000"' );
      htp.print( '<FORM>' );
      -- htp.header( 4,'Adviser Approval Log','CENTER' );
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                           'COLOR="#336699"><B><I>' ||
                       'Adviser Approval Log' ||
                     '</I></B></FONT>',
                       CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      --
      -- -----------------------------------------------------------------------
      --  Open Table - Academic Term
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Academic Term:&nbsp;' ||
                     '</B></FONT>',
                     CATTRIBUTES => 'ALIGN="RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       v_term_name ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="50%"' ||
                                      'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Academic Term
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Advisor Approval Log
      -- -----------------------------------------------------------------------
      htp.tableOpen( 'BORDER', 'CENTER', CATTRIBUTES => 'WIDTH="80%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Student Name:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       p_student_name ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'UID:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       p_student_uid ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'WIDTH="50%"' ||
                                      'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Schedule Status:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       -- '<B>' ||
                       v_schedule_status ||
                       -- '</B>' ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Schedule Reviewed by:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       '<B>' ||
                         v_reviewed_by ||
                       '</B>' ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Date:&nbsp;' ||
                     '</B></FONT>',
                       CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       '<B>' ||
                         v_reviewed_date ||
                       '</B>' ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableRowOpen;
      htp.tableData( '<FONT COLOR="#FFFFFF"><B>' ||
                       'Time:&nbsp;' ||
                     '</B></FONT>',
                     CATTRIBUTES => 'ALIGN = "RIGHT" BGCOLOR="#336699"' );
      htp.tableData( '<SMALL><FONT FACE="Arial, Helvetica, sans-serif"' ||
                                  'BGCOLOR="#FFFFFF">' ||
                       '&nbsp;' ||
                       '<B>' ||
                         v_reviewed_time ||
                       '</B>' ||
                     '</FONT></SMALL>',
                       CATTRIBUTES => 'BGCOLOR="#FFFFFF"');
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Advisor Approval Log
      -- -----------------------------------------------------------------------
      htp.br;
      -- -----------------------------------------------------------------------
      --  Open Table - Buttons
      -- -----------------------------------------------------------------------
      htp.tableOpen( CALIGN => 'CENTER' );
      htp.tableRowOpen;
      htp.tableData( '<INPUT TYPE=BUTTON VALUE="Close Window"' ||
                     ' ONCLICK="window.close();">' );
      htp.tableRowClose;
      htp.tableClose;
      -- -----------------------------------------------------------------------
      --  Table Closed - Buttons
      -- -----------------------------------------------------------------------
      htp.formClose;
      htp.bodyClose;
      htp.htmlClose;
      -- -----------------------------------------------------------------------
      --  HTML Page Closed
      -- -----------------------------------------------------------------------
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      htp.print( 'SQLCODE: ' || SQLCODE );
      htp.print( '<BR>' );
      htp.print( 'SQLERRM: ' || SQLERRM );
  END advisor_approval_log;
  -- ===========================================================================
  --  Advisor Schedule Approval Help Page
  -- ===========================================================================
  PROCEDURE help
    (
    p_key IN VARCHAR2,
    p_uid IN VARCHAR2
    )
  IS
    -- -------------------------------------------------------------------------
    --  Local Variables for Security
    -- -------------------------------------------------------------------------
    v_session_id NUMBER;
    v_encrypted_session_id VARCHAR2(100);
    v_check_session VARCHAR2(100);
  BEGIN
    -- -------------------------------------------------------------------------
    --  Security
    -- -------------------------------------------------------------------------
    --  Get session_id
    v_session_id := citfnd_web_sec_exe_pkg.decrypted_id( p_key );
    v_encrypted_session_id := citfnd_web_sec_exe_pkg.encrypted_id( v_session_id );
    --  Get session info from the exeter package (not the 11i side one)
    citsss_webenr_global_pkg.get_session_info( v_session_id,g_session_info );
    --  Check valid session
    v_check_session := citsss_webenr_global_pkg.check_session( v_session_id );
    IF v_check_session = 'EXPIRED' THEN
      citsss_webenr_global_pkg.relogon( g_session_info.home_url );
    ELSE
      -- -----------------------------------------------------------------------
      --  Passed Security
      -- -----------------------------------------------------------------------
      --  Open HTML Page
      -- -----------------------------------------------------------------------
      htp.htmlOpen;
      htp.bodyOpen( CATTRIBUTES=>'BGCOLOR="#CCCCCC", TEXT="#000000"' );
      -- htp.header( 4,'Help','CENTER' );
      /*
      htp.tableOpen( 'NOBORDER', 'CENTER', CATTRIBUTES => 'WIDTH="95%"');
      htp.tableRowOpen;
      htp.tableData( '<FONT FACE="Arial, Helvetica, sans-serif" ' ||
                     'COLOR="#336699"><B><I>' ||
                     'Help' ||
                     '</I></B></FONT>',
                     CATTRIBUTES => 'ALIGN = "LEFT"' );
      htp.tableRowClose;
      htp.tableClose;
      htp.br;
      --
      */
      -- -----------------------------------------------------------------------
      --  Open Table - Schedule Approval Help
      -- -----------------------------------------------------------------------
      -- -----------------------------------------------------------------------
      --  Table Closed - Schedule Approval Help
      -- -----------------------------------------------------------------------
      htp.br;
      htp.bodyClose;
      htp.htmlClose;
      -- -----------------------------------------------------------------------
      --  HTML Page Closed
      -- -----------------------------------------------------------------------
    END IF;
    EXCEPTION
      WHEN OTHERS THEN
        htp.print( 'SQLCODE: ' || SQLCODE );
        htp.print( '<BR>' );
        htp.print( 'SQLERRM: ' || SQLERRM );
  END help;
END citsss_webenr_schdl_apprvl_pkg;
/
