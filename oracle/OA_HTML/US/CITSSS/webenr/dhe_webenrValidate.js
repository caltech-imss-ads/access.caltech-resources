// ************************
// Declare global variables
// ************************

var offering_titleArray = new Array();
var unit_categoryArray = new Array();
var unit_type_idArray = new Array();
var instructorArray = new Array();
var days_and_timeArray = new Array();
var locationArray = new Array();
var unitsArray = new Array();
var for_total_unitsArray = new Array();
var gAdvisorEmail;
var gStudentEmail;
//**********
var gFreshmanOverloadLimit = 51;
var gNonFreshmanOverloadLimit = 54;
var gUGradUnderloadLimit = 36;
var gVariableUnitLimit = 18;

var gFrame;
var gForm;
var gField;
var gTmpID = "T_M_P";
var gReqID = "_reqd";

// ************************
// validation utility routines *
// ************************

// check if value entered is numeric
function isNumeric(){
 if (!isBlank(gField)) {
  //var strValidChars = "0123456789.-";
  var strValidChars = "0123456789.";  // move the "-".  Caltech 10/02/03
  var strChar;
  var blnResult = true;
  // NN6 and IE6 seem to have problems incrementing var i when var i initialized with zero and gField length is 1. 
  // Coded around the problem as follows
  //for (i = 0; i < gField.value.length && blnResult == true; i++){
  for (i = 1; i < gField.value.length+1 && blnResult; i++){
    strChar = gField.value.charAt(i-1);
    if (strValidChars.indexOf(strChar) == -1) {
      blnResult = false;
    }
  }
  if (!blnResult) {
    alert('Please input positive numbers only');
    //alert('Please input no decimal, and positive numbers only');  // move the "-".  Caltech 10/02/03
    gField.value = "";  // Caltech 04/29/04
    gField.focus();
    gField.select();
    return blnResult;
  } else {
    return blnResult;
  }
 }
 return true;
} // isNumeric

function stripBlanks(aString){
	 var newString =aString;
	 var blank = " ";
	 if(newString.indexOf(blank) != -1){
		var i = 0;
		var j = newString.length - 1;

		for(i = 0; i < newString.length; i++ ){
		  if(newString.charAt(i) != blank)
			 break;
		}

		if(i != newString.length){
		  for(j = newString.length - 1;j > -1; j--){
			 if(newString.charAt(j) != blank)
				break;
		  }
		  newString = newString.substring(i,j+1);
		}
		else {
		  // all blanks
		  newString = "";
		}
	}
 return newString;
} // stripBlanks

function isBlank(field){
  // 12-09-03, Dennis Lo, netscape 4.79 doesn't store selected value in select object
  if (jBrowser.nn && jBrowser.major == 4 && field.type == "select-one") { 
    if(! field[field.selectedIndex].text || stripBlanks(field[field.selectedIndex].text) == "") {
      return true;
    } else
      return false;
  } else {
   if(! field.value || stripBlanks(field.value) == "")
     return true;
   else
     return false;
  }
} // isBlank

function isNegativeOne(field){
	if(field.value == "-1")
	   return true;
	else
           return false;
} // isNegativeOne

function isNegative(field){
	if(field.value < "0")
	   return true;
	else
           return false;
} // isNegative

function isReqd(field){
   if (field.name.indexOf(gReqID) != -1) {
      return true;
   }else {
      return false;
   }
} // isReqd


//begin caltech webenr modification

function unitsCheckFailed(field){
        return false;
        //by pass the following.  they are handled by the pl/sql package
        if (field.name.toUpperCase() == "UNITS_REQD") { 
           //alert(field.form.program_name.value+ " - " +field.form.freshman.value+ "-" 
           //     +field.form.unit_category.value);
           if (field.value == "0") {
               alert("Units can not be zero");
               return true;
           //else if ((field.form.unit_category.value == 'V') &&
           //         (field.value > gVariableUnitLimit)) {
           //     alert("Variable Units Overload!\n\nYou may not overload with more than "
           //           +gVariableUnitLimit+
           //           " variable units.  \nPlease contact the Dean's Office.\n");
           //  return true;
           }
        } else {
           return false;
        }
} // unitsCheckFailed
//end caltech webenr modification

function isMissing() {
  var vFieldName = " ";
//   if (typeof gField == "undefined" || gField.length > 1) {
//     return false;
//   }

   //if (isReqd(gField) && (isBlank(gField) || isNegative(gField))) {
   if (isReqd(gField) && isBlank(gField)) {
      //caltech specific logic here
      if (gField.name.substring(0,5) == "depar") {
         vFieldName = "Department";
      }else if (gField.name.substring(0,5) == "offer") {
         vFieldName = "Offering";
      }else if (gField.name.substring(0,5) == "enrol") {
         vFieldName = "Status";
      }else if (gField.name.substring(0,5) == "secti") {
         vFieldName = "Section";
      }else if (gField.name.substring(0,5) == "grade") {
         vFieldName = "Grade Scheme";
      }else if (gField.name.substring(0,5) == "units") {
         vFieldName = "Units";
      }else {
         vFieldName = "This";
      }


      //replace the alert with caltech specific window message
        alert(vFieldName+" is a required field");

//      if (gField.name.indexOf(gReqID) != -1) {
//         alert(vFieldName+" is a required field");
//      }else{
//         alert(vFieldName+" is a required field");
//      }

      gField.value = "";  // Caltech 04/29/04
      //gField.select();
      gField.focus();
      return true;
   }else {
      return false;
   }
} // isMissing

function removeBlanks(){
  gField.value = stripBlanks(gField.value);
  return true;
} // removeBlanks

function dispatcher(validateFunc){
   this.doValidate = validateFunc;
} // dispatcher

// set up dispatchLookup functions array
var dispatchLookup = new Array();
dispatchLookup["isMissing"] = new dispatcher(isMissing);
dispatchLookup["removeBlanks"] = new dispatcher(removeBlanks);
dispatchLookup["isNumeric"] = new dispatcher(isNumeric);

// validate routine, one field at a time with multiple validations 
// also called by checkForm routine below
function validate(frame, field, method) {
   var frame_path = '';
   var win = frame; 
//alert("inside validate()");
   while (win.parent != top) {
      frame_path = win.parent.name + "." + frame_path;
      win = win.parent; 
   }
   frame_path = "top." + frame_path;
   gFrame = eval(frame_path+frame.name);
   gForm = eval(frame_path+frame.name+".document."+field.form.name);
   gField = eval(frame_path+frame.name+".document."+field.form.name+"."+field.name);

   if (!gField && field.name.indexOf(gReqID) != -1){ 
     var tmpField = field.name.substring(0,field.name.indexOf(gReqID))+
                    field.name.substring(field.name.indexOf(gReqID)+gReqID.length, field.name.length);
     gField = eval(frame_path+frame.name+".document."+field.form.name+"."+tmpField);
   }else if(!gField && field.name.indexOf(gReqID) == -1){ 
     var tmpField = field.name.substring(0,field.name.indexOf(gTmpID))+gReqID+
                    field.name.substring(field.name.indexOf(gTmpID),field.name.length);
     gField = eval(frame_path+frame.name+".document."+field.form.name+"."+tmpField);
   }

   //if (!gField || !gField.type) {
   //   gField  = eval(frame_path+frame.name+".document."+field.form.name+"."+field.name+'[0]');
   //}

   //if (!gField && field.name.indexOf(gReqID) != -1) {
   //   if (field.name.indexOf(gTmpID) != -1) {
   //      gField = eval(frame_path+frame.name+".document."+field.form.name+"."+field.name.substring(0,field.name.indexOf(gReqID))+gTmpID);
   //   }else{
   //      gField = eval(frame_path+frame.name+".document."+field.form.name+"."+field.name.substring(0,field.name.indexOf(gReqID)));
   //   }
   // }

   //if (typeof gField == "undefined") {
   //  return false;
   //} else {
     var args = validate.arguments;
     for (i=2; i<args.length; i++) {
        if (!dispatchLookup[args[i]].doValidate()){
          return false;
        }
     } 
   //}
   return true;
} // validate

// catch all form level validation. Scan and validate all at form submission
function checkForm(frame, form, offset, reset){
   // scan and valiate all fields on form
   for (var j=0; j<=form.length-1; j++){
       //if (form.elements[j].type == "text" && 
       if ((form.elements[j].name.indexOf(gReqID) != -1) && 
             validate(frame, form.elements[j], "isMissing")){
          return false;
       }

       if ((form.elements[j].name.toUpperCase().indexOf("_NUMERIC") != -1) && 
           (!isBlank(form.elements[j])) && 
           (!validate(frame, form.elements[j], "isNumeric"))){
             return false;
       } 

       //begin caltech webenr customization
       if ((form.elements[j].name.toUpperCase().indexOf("UNITS") != -1) && 
           (unitsCheckFailed(form.elements[j]))) {  
             return false;
       } 
       //end caltech webenr customization

   } 	

   if (reset) {
     resetFields(form);
   } 
   return true;
}  //checkform

function resetFields (form) {
  if (jBrowser.nn && jBrowser.major >= 5) { 
    var fieldNames = new Array();
    for (var j=0; j<=form.length-1; j++){
      fieldNames[j] = form.elements[j].name; 
    }

    for (var j=0; j<=fieldNames.length-1; j++){
      field = document.getElementsByName(fieldNames[j]).item(0);
      if (field && typeof field != "undefined") {
        var i = field.name.indexOf(gTmpID);
        if (i != -1){
          field.name = field.name.substring(0,i);
        } 
        var i = field.name.indexOf(gReqID);
        if (i != -1){
          field.name = field.name.substring(0, i);
        } 
      }
    }
  } else {
    for (var j=0; j<=form.length-1; j++){
      var i = form.elements[j].name.indexOf(gTmpID);
      if (i != -1){
        form.elements[j].name = form.elements[j].name.substring(0, i);
      } 
      var i = form.elements[j].name.indexOf(gReqID);
      if (i != -1){
        form.elements[j].name = form.elements[j].name.substring(0, i);
      } 
    } 
  } 
} 
