// *******************************
// Declare global variables
// 05/31/06 DHE Initial Creation
// *******************************
var gP1;   //exeter_web_agent
var gP2;   //p_key
var gP3;   //p_uid
var gP4;   //p_student_instance_id
var gVar
//*******************************

function goToAlert() {
 alert("You currently have a hold on your record.");
}

function goToURL() {
  //alert("Inside goToURL...gP1:"+gP1);
  gVar = gP1 + "citsss_webacadinf_students_pkg.format_transcript_student?p_fn_type=S"; 
  //alert("gVar:"+gVar);
  window.location = gVar; 
}

function goToDAURL() {
  //alert("Inside goToDAURL...gP1:"+gP1);
  gVar = gP1 + "citsss_webacadinf_students_pkg.format_DA_student?p_fn_type=S&p_delete_flag=Y"; 
  //alert("gVar:"+gVar);
  window.location = gVar; 
}

function goToDAUPDATE() {
  //alert("Inside goToDAUPDATE...gP1:"+gP1);
  gVar = gP1 + "citsss_webacadinf_students_pkg.update_DA?p_fn_type=S&p_delete_flag=Y"; 
  //alert("gVar:"+gVar);
  window.location = gVar; 
}

function goToEVURL() {
  //alert("Inside goToEVURL...gP1:"+gP1);
  gVar = gP1 + "citsss_webacadinf_students_pkg.format_enrl_verification?p_fn_type=S"; 
  //alert("gVar:"+gVar);
  window.location = gVar; 
}


