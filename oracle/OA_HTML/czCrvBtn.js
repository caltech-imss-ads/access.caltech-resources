/* $Header: czCrvBtn.js 115.21 2001/07/13 09:53:16 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash K MacClay Created.                       |
 |                                                                           |
 +===========================================================================*/
 
function CurvedButton()
{
  this.parentConstructor = Container;
  this.parentConstructor();

  this.height = 20;
  this.width = 100;
  this.curvedLeft = 11;
  this.curvedRight = 11;
  
  this.caption='';
  this.toggleButton = false;
  this.state = 0;
  this.button = true;
  this.built = false;
  this.toolTipText='';
  this.addListener('mousedownCallback',this);
  this.addListener('mouseupCallback',this);
  this.addListener('mousemoveCallback',this);
  this.enableKeyPressEvents();
  this.addListener('onKeyPressCallback', this);
}

function CurvedButton_buildControl ()
{
  if (this.built) return;

  var curWidth = 0;
  //create left curve image;
  if (this.curvedLeft) {
    var img = this.img1 = new ImageButton ();
    img.setName ('LImg-'+this.name);
    img.setDimensions (0, 0, this.curvedLeft, this.height);
    img.stretch = true;
    img.setImages (IMAGESPATH+'czButL0.gif',IMAGESPATH+'czButL0.gif'); 
    curWidth += this.curvedLeft;
  }
  //create right curve image;
  if (this.curvedRight) {
    var img = this.img2 = new ImageButton ();
    img.setName ('RImg-'+this.name);
    img.setDimensions (this.width - this.curvedRight, 0, this.curvedRight, this.height);
    img.stretch = true;
    img.setImages(IMAGESPATH+'czButR0.gif',IMAGESPATH+'czButR0.gif')
  }
  //create center image;
  var cenWd = this.width - (this.curvedLeft + this.curvedRight);
  var img = this.img3 = new ImageButton ();
  img.setName ('CImg-'+this.name);
  img.setDimensions (curWidth, 0, cenWd, this.height);
  img.stretch = true;
  img.setImages(IMAGESPATH+'czButC0.gif',IMAGESPATH+'czButC0.gif');
  
  //create a prompt for writing caption;
  var capt = this.capt = new Prompt ();
  this.setAlignment('center');
  capt.setName ('Captn-'+this.name);
  capt.setDimensions (curWidth, 4, cenWd, this.height-2);
  capt.setCaption (this.caption);
  if (this.spanClass) {
    capt.setSpanClass (this.spanClass);
  }
  //create a top layer;
  this.topLay = new ImageButton();
  this.topLay.setName ('TLay-'+this.name);
  this.topLay.setDimensions (0, 0, this.width, this.height);
  this.topLay.stretch = true;
  this.topLay.setImages (IMAGESPATH+'czblank.gif');
  this.topLay.setToolTipText (this.toolTipText);
  this.topLay.type = 'button';

  //add comps to container;
  if(this.img1)
    this.add(this.img1);

  if(this.img2)
    this.add(this.img2);

  this.add(this.img3);
  this.add(this.capt);
  this.add(this.topLay);

  this.built = true;
}

function CurvedButton_toggle(bVal)
{
  if(bVal) {
    if(this.img1) 
      this.img1.toggle(1);
    if(this.img2) 
      this.img2.toggle(1);
    if(this.img3) 
      this.img3.toggle(1);

    if(this.capt) {
      if(this.selectedStyle) {
        this.capt.spanClass = this.selectedStyle;
        this.setCaption(this.caption);
      }
    }
    this.state =1;
  } else {
    if(this.img1)
      this.img1.toggle(0);
    if(this.img2)
      this.img2.toggle(0);
    if(this.img3)
      this.img3.toggle(0);

    if(this.capt) {
      if(this.unselectedStyle) {
        this.capt.spanClass = this.unselectedStyle;
        this.setCaption(this.caption);
      }
    }
    this.state =0;
  }
}

function CurvedButton_setToolTipText(toolTipText)
{
  this.toolTipText=toolTipText;
}

function CurvedButton_setLeftImages(leftImg0,leftImg1)
{
  if(this.img1)
    this.img1.setImages(leftImg0,leftImg1);
}

function CurvedButton_setRightImages(rightImg0,rightImg1)
{
  if(this.img2)
    this.img2.setImages(rightImg0,rightImg1);
}

function CurvedButton_setCenterImages(img0,img1)
{
  if(this.img3)
    this.img3.setImages(img0,img1);
}

function CurvedButton_setLeftCurveWidth(val)
{
  if (isNaN(val)) {
    alert ("CurvedButton:setLeftCurveWidth:" + val + " Not a number");
    return;
  }
  this.curvedLeft = val;
}

function CurvedButton_setRightCurveWidth(val)
{
  if (isNaN(val)) {
    alert ("CurvedButton:setRightCurveWidth:" + val + " Not a number");
    return;
  }
  this.curvedRight = val;
}

function CurvedButton_moreLaunch(em)
{
  //Launch the child comps, but don't add it to EvtMgr;
  if(this.children) {
    for (var childName in this.children) 
      this.children[childName].launch();
  }  
}

function CurvedButton_mousemoveCallback(e)
{
  var em = this.eventManager;
  if(em.msdown && (this.states > 0)) {
    if(ie4)
      e.returnValue = false;
    else
      return false;
  }
  return true;
}

function CurvedButton_mousedownCallback(e, source)
{

  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;

  if(this.toggleButton)
    this.toggle(1-this.state);
  else if(this.button)
    this.toggle(1);		
  return true;
}

function CurvedButton_mouseupCallback(e, source)
{
  if ((navigator.platform.indexOf("MacPPC") == -1 ) && ((ns4 && e.which!=1) || (ie4 && e.button!=1))) 
    return true;

  if(this.toggleButton || this.button) {
    var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
    var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;
    var absx = this.getLeft('abs');
    var absy = this.getTop('abs');
    var inside = false;
    inside = this.isInside(x,y,absy,absx+this.width,absy+this.height,absx);

    if(! this.toggleButton)
      this.toggle(0);

    if(!inside && this.toggleButton)
      this.toggle(1-this.state);
    else 
      this.toggle(this.state);

    this.notifyListeners('onClick',e, this);
  }
  return true;
}

function CurvedButton_setCaption(v)
{
  if(v) {
    if (this.built)
      this.capt.setCaption(v);
    this.caption = v;
  }
}

function CurvedButton_getCaption()
{
  if(this.capt)
    return this.capt.getCaption();
}

function CurvedButton_setBackgroundColor(c)
{
  return true;
}

function CurvedButton_onKeyPressCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  if (key == 13) {
    if (ie4)
      e.returnValue = false;
    
    this.toggle(0);
    this.notifyListeners('onClick', e, this);
  }
  return true;
}

HTMLHelper.importPrototypes(CurvedButton,Container);

CurvedButton.prototype.constructor 	= CurvedButton;
CurvedButton.prototype.toggle 		= CurvedButton_toggle;
CurvedButton.prototype.setToolTipText 	= CurvedButton_setToolTipText;
CurvedButton.prototype.setCaption 	= CurvedButton_setCaption;
CurvedButton.prototype.getCaption 	= CurvedButton_getCaption;
CurvedButton.prototype.setLeftImages 	= CurvedButton_setLeftImages;
CurvedButton.prototype.setRightImages 	= CurvedButton_setRightImages;
CurvedButton.prototype.setCenterImages 	= CurvedButton_setCenterImages;
CurvedButton.prototype.mousedownCallback= CurvedButton_mousedownCallback;
CurvedButton.prototype.mouseupCallback	= CurvedButton_mouseupCallback;
CurvedButton.prototype.mousemoveCallback = CurvedButton_mousemoveCallback;
CurvedButton.prototype.moreLaunch = CurvedButton_moreLaunch;
CurvedButton.prototype.setBackgroundColor = CurvedButton_setBackgroundColor;
CurvedButton.prototype.setLeftCurveWidth = CurvedButton_setLeftCurveWidth;
CurvedButton.prototype.setRightCurveWidth = CurvedButton_setRightCurveWidth;
CurvedButton.prototype.onKeyPressCallback = CurvedButton_onKeyPressCallback;

//private method
CurvedButton.prototype.buildControl = CurvedButton_buildControl;
