<!-- $Header: amsincjs.js 115.5 2001/04/02 18:40:09 pkm ship        $ -->
<!-- This is the AMS common JavaScript library. Please feel free to add -->
<!-- any common JavaScript functions that you would like to be available to -->
<!-- all of AMS -->

function
getOpenerField(  p_strFormName
               , p_strObjName
              )
{
   var l_objField = eval("window.opener.document.forms['"+p_strFormName+"'].elements['"+p_strObjName+"']");
   return l_objField;
}

function
getOpenerFieldValue(  p_strFormName
                    , p_strObjName
                   )
{
   var l_objField = getOpenerField(  p_strFormName
                                   , p_strObjName
                                  );
   return l_objField.value;
}

function
getField(  p_strFormName
         , p_strObjName
        )
{
   var l_objField = eval("document.forms['"+p_strFormName+"'].elements['"+p_strObjName+"']");
   return l_objField;
}

function
getFieldValue(  p_strFormName
              , p_strObjName
             )
{
   var l_objField = getField(  p_strFormName
                             , p_strObjName
                            );
   return l_objField.value;
}

function
clearLov(  p_strFormName
         , p_strNameParamName
         , p_strIdParamName
        )
{
   var l_objNameField = getField(  p_strFormName
                                 , p_strNameParamName
                                );
   var l_objIdField = getField(  p_strFormName
                               , p_strIdParamName
                              );
   
   var l_strObjNameVal = trim(l_objNameField.value);
   
   if (0 == l_strObjNameVal.length)
   {
      l_objIdField.value = '';
   }
}

function
getWinFeatures()
{
   var l_strWinFeatures = 'scrollbars,width=600,height=500,left=100,top=100';
   return l_strWinFeatures;
}
