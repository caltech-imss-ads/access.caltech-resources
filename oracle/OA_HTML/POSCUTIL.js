/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================+
| FILENAME                                                         |
|   POSCUTIL.js                                                    |
|                                                                  |
| HISTORY                                                          |
|                                                                  | 
+==================================================================*/
/* $Header: POSCUTIL.js 115.0 99/10/14 16:19:53 porting shi $ */


/* We first need to determine what browser is being used */
if(parseInt(navigator.appVersion) >= 4) 
{
  if (navigator.appName == "Netscape")
  {
    // Web browser is a netscape navigator
    IS_NAV= true;
    IS_IE=false;
    coll = "";
    styleObj = "";
    window.captureEvents(Event.FOCUS|Event.CLICK |Event.RESIZE);
    suffix="_nav";
  }
  else // Internet Explorer
  {
    IS_NAV= false;
    IS_IE= true;
    coll = "all.";
    styleObj = ".style";
    layerTab= "div";
    suffix="_ie";
  }
}
else // invalid version
{
  alert("Invalid browser version");
}



function roundDecimal(expr, digits)
{
  var str= "" + Math.round(eval(expr) * Math.pow(10, digits));
  while (str.length <= digits)
  {
    str= "0"+str;
  }
  var decpoint= str.length- digits;
  return str.substring(0, decpoint) + "." + str.substring(decpoint, str.length);
}

function isPositiveNumber(num)
{
  var f= parseFloat(num);
  return (!isNaN(f) && f>=0);
}







