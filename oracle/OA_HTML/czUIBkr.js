/* $Header: czUIBkr.js 115.77 2001/08/10 10:48:48 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function UiBroker ()
{
  this.sessionId = null;
  this.proxyRef = 'parent.frames.czProxy';
  this.contentMode = 'empty';
  this.contentTarget = 'czContent';
  this.appExited = false;  
  this.pageHistory = new Stack ();
  this.curSeqNum = -1;
  this.readOnly = false;
  this.back= false;
  this.register=false;
  this.fromPublishedPage=true;
  this.isFirstTime=true;
  this.heartMax = null;
  this.atpShown = false;
  this.showOn = true;
  //used mainly in NS to refresh tree frame when things gets clipped
  this.numRefreshTree = 0;
  this.charSetEncoding = "UTF-8";

  //default logic state icons;
  this.bNoStateIcons = false;
  this.srcUTrue = 'czbolut.gif';
  this.srcUFalse = 'czboluf.gif';
  this.srcLTrue = 'czbollt.gif';
  this.srcLFalse = 'czbollf.gif';
  this.srcUnknown = 'czbolun.gif';
  this.imageWidth = 16;
  this.imageHeight = 16;

  //logic state color & boldness;
  this.bUseLogicColor = false;
  this.bUseLogicBold = false;
  
  this.msgTxt = "";
  //UI Frames related counters
  this.wndCnt = 0;
  this.ctrlCnt = 0;
  this.wndCol = "";
  this.ctrlCol = "";
  this.txtCol = "";
  this.uiLook = "blaf";
  this._3DLook = false;

  // back behavour for ie4
  this.cnt=0;
  this.backSeqNum = 0;

  this.tabindex = 1;

  // mac related variable
  this.needRefresh = false;
  this.alreadyPublished = false;
}

//static members added here 'cause it is used down below in functions
UiBroker.NO_BORDER_LOOK = 0;
UiBroker.FLAT_LOOK = 1;
UiBroker._3DLOOK = 2;

function UiBroker_setSessionId (Id)
{
  this.sessionId = Id;
}

function UiBroker_getSessionId ()
{
  return this.sessionId;
}

function UiBroker_readOnlySession (read)
{
  this.readOnly = read;
}

function UiBroker_getReadOnly ()
{
  return this.readOnly;
}

function UiBroker_terminate()
{
  top.close();
}

function UiBroker_setMaxHeartBeat(count)
{
  this.heartMax = count;
  this.heartBeat = 0;
  this.counter = 0;
  if ( self.heartBeatId != null){
    clearTimeout(self.heartBeatId);
    self.heartBeatId = null
  }
  if ( this.heartMax != 0)
    this.startHB();
}

function UiBroker_getMaxHeartBeat()
{
  return this.heartMax;
}

function UiBroker_stopHB()
{
  this.heartBeat = 0;
  this.counter = 0;
  clearTimeout(self.heartBeatId);
  self.heartBeatId = null;
}

function UiBroker_startHB()
{
  if ( this.heartMax != null)
    self.heartBeatId = setInterval("ub.setHeartBeat()",this.heartMax);
}

function UiBroker_setHeartBeat()
{
  this.log ("Idle Time :"+this.heartBeat, null, new Date());
  this.heartBeat = (++this.counter)*10000;
  this.postClientHeartBeat('<heartbeat src="CLIENT"/>');
}

function UiBroker_getHeartBeat()
{
  return this.heartBeat;
}

function UiBroker_reSetHeartBeat()
{
  this.stopHB()
  this.startHB();
}

function UiBroker_postClientHeartBeat(xml)
{
  if (!this.appExited) {
    this.log ("Sending message to server '" + xml + "'", new Date());
    if ( !this.isFirstTime ){
      this.lockEventManagers();
      this.isFirstTime = false;
    }

    if (( (!this.showOn) && (! this.isEventManagerLocked()) ) || (this.msgTxt != "") ) {
      this.log ("Sending message to server '" + xml + "'", new Date());
      this.lockEventManagers();

      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
    
      if ( ns4 )
      {
    	  var sessionId = parent.frames.czHeartBeat.document.form1.sessionId.value;
        if ( sessionId == "" )
	      sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
      }
      else if (ie4)
      {
    	  var sessionId = parent.frames.czHeartBeat.form1.sessionId.value;
        if ( sessionId == "" )
	      sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
      }  

      var msg = "<client-event session-id='" + sessionId + "'>";
      msg += xml + "</client-event>";
      if (ns4) {
        parent.frames.czHeartBeat.document.form1.XMLmsg.value = msg;
        parent.frames.czHeartBeat.document.form1.submit ();
      } else if (ie4) {
        parent.frames.czHeartBeat.form1.XMLmsg.value = msg;
        parent.frames.czHeartBeat.form1.submit ();
      }
      if ( this.heartMax != 0 )
        this.reSetHeartBeat();	      
      return true;
    } 
    else {
      return false;
    }
  }
  return false;
}

function UiBroker_setDefaultLogicImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
}

function UiBroker_setLogicColors (utCol, ltCol, ufCol, lfCol, unCol)
{
  this.bUseLogicColor = true;
  this.utColor = utCol;
  this.ltColor = ltCol;
  this.ufColor = ufCol;
  this.lfColor = lfCol;
  this.unColor = unCol;
}

function UiBroker_setLogicBold (bUtBld, bLtBld, bUfBld, bLfBld, bUnBld)
{
  this.bUseLogicBold = true;
  this.utBold = bUtBld;
  this.ltBold = bLtBld;
  this.ufBold = bUfBld;
  this.lfBold = bLfBld;
  this.unBold = bUnBld;
}

function UiBroker_setNoStateIcons (bVal)
{
  this.bNoStateIcons = bVal;
}

function UiBroker_setMessageTarget (wndName)
{
  this.messageTarget = wndName;
}

function UiBroker_getMessageTarget ()
{
  //In summary mode use czContent frame as the error message target;
  if ((this.contentMode == 'summary') || (this.contentMode == 'empty')) 
    return ('czContent');
  else if (this.contentMode == 'config')
    return (this.messageTarget);
}

function UiBroker_registerRuntimeObject (wndName, obj)
{
  var rtId = null;
  if (obj.getModelItem)
    rtId = obj.getModelItem().getName();
  else 
    rtId = obj.getName ();
  
  if ((obj!=null) && rtId && (wndName != null)) {   
    this.runtimeObjectHash[wndName][rtId] = obj;
    //Do not hook callbacks for tree;
    if (obj.type == 'tree') return;
    
    switch (obj.type) {
    case 'BomItem':
      obj.addListener('actionMouseUp', this);
      obj.addListener('actionOnChange', this);
      break;
      
    case 'CountedOptionItem':
      obj.addListener('actionOnChange', this);
      break;
      
    case 'LogicCheckBox':
      obj.addListener ('actionMouseUp', this);
      break;
      
    case 'text':
      if(!this.getReadOnly()){
        obj.addListener('onchangeCallback', this);
      }
      break;
    case 'numerictext':
      if(!this.getReadOnly()){
        obj.addListener('onchangeCallback', this);
      }
      break;
    case 'image':
      break;
    case 'button':
      if(!this.getReadOnly())
        obj.addListener('onClick', this);
      break;
    case 'TreeNode':
      obj.addListener('mouseupCallback', this);
      break;
    }
  }
}

function UiBroker_deleteComponent(runtimeId)
{
  var obj = this.getRuntimeObject(runtimeId);
  if (obj != null) {
    if (obj.type == 'TreeNode') {
      var parentNode = obj.getParent();
      if (parentNode) {
        parentNode.removeNode(obj);
      }
    }
  }
}

function UiBroker_formatRuntimeId (runtimeId)
{
  return (runtimeId.substring(1, runtimeId.length));
}

function UiBroker_showSummary()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
	this.showOn = true;
      this.clearContentFrame ("czSummary.htm");

	if (this.showOn)
    	   this.showOn = false;

      //this.contentMode = 'summary';
      //this.postClientMessage ('<show-summary/>');
    }
  }
}

function UiBroker_showConfiguration()
{
  if (!this.appExited) {
    this.showOn = true;
    this.cnt = 0;
    this.backSeqNum = 0;
    if (! this.isEventManagerLocked ()) {
      this.clearContentFrame ();
      //reset to the original proxy source...;
      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
      proxy.document.location = this.sessionStartURL;
      this.atpShown = false;

      //this.contentMode = 'config';
      //For future reference, make this post to the 
      //content frame or something.
      //this.postClientMessage ('<show-config/>');
    }
  }
}

function UiBroker_getContentMode()
{
  return this.contentMode;
}

function UiBroker_showATP()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      if ( this.contentMode == 'config' ){
      	this.postClientMessage ('<show-atp/>');
	}
	else if ( this.contentMode == 'summary'){
  		var parentStr = '';
  		for (var i=0; i<this.depthHash[this.contentTarget]; i++) {
    			parentStr += 'parent.';
  		}
  		var doc = eval(parentStr + this.contentTarget);	
		if ( !this.atpShown ){
			doc.showAtp();
			this.atpShown = true;
		}
	}
    }
  }
}

function UiBroker_completeConfiguration () 
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<submit/>');
    }
  }
}

function UiBroker_cancelConfiguration ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<cancel/>');
    }
  }
}

//Future implementation
function UiBroker_configurationHelp ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<help page='+this.navCmd+'/>');
    }
  }
}

function UiBroker_saveConfiguration ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<save/>');
    }
  }
}

function UiBroker_clearContentFrame (withSrc)
{
  var parentStr = '';
  for (var i=0; i<this.depthHash[this.contentTarget]; i++) {
    parentStr += 'parent.';
  }
  var doc = eval(parentStr + this.contentTarget + ".document");	
  //redirect the content form...
  //doc.location = '/html/czCntnt.htm';
  if (withSrc){
    doc.location = SOURCEPATH + withSrc;    
  }
  else 
    doc.location = SOURCEPATH + 'czCntnt.htm';
  this.contentMode = 'empty';

}

function UiBroker_navigateToScreen (newScreen)
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      var msg = "<navigate name='" + newScreen +  "'/>";
      this.postClientMessage(msg);
    }
  }
}

function UiBroker_mouseupCallback (e, eSrc)
{
  var rtId = null;
  if (eSrc.getModelItem)
    rtId = eSrc.getModelItem().getName();
  else
    rtId = eSrc.getName ();
 
  if (rtId) {
    var msg = "<click rtid='" + this.formatRuntimeId(rtId) +  "'/>";
    this.postClientMessage(msg);
  }
  return true;
}

function UiBroker_actionMouseUp (modelItem, modelView)
{
  return this.mouseupCallback (modelItem, modelView);
}

function UiBroker_onClick (e, eSrc)
{
  return this.mouseupCallback (e, eSrc);
}

function UiBroker_onchangeCallback(newValue, eSrc)
{
  var rtId = null;
  if (eSrc.getModelItem)
    rtId = eSrc.getModelItem().getName();
  else
    rtId = eSrc.getName ();
  
  if (rtId) {
    var msg = "<input rtid='" + this.formatRuntimeId(rtId) + "'>";
    if ((eSrc.type == 'BomItem') || (eSrc.type == 'CountedOptionItem'))
      msg += eSrc.getCount ();
    else 
      msg += newValue;
    msg += "</input>";
    this.postClientMessage(msg);
  }
  return true;
}

function UiBroker_actionOnChange (modelItem, modelView)
{
  this.onchangeCallback (modelItem, modelView);
}

function UiBroker_objectAddedCallback (item, itemParent)
{
  if (itemParent.windowName)
    this.registerRuntimeObject (itemParent.windowName, item);
}

function UiBroker_postClientMessage(xml, bOverRideLock)
{
  if (!this.appExited) {
    if ((! this.isEventManagerLocked()) || bOverRideLock) {
      this.log ("Sending message to server '" + xml + "'", new Date());
      this.lockEventManagers();
      
      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
      
      this.sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
        var msg = "<client-event session-id='" + this.sessionId + "'>";
      msg += xml + "</client-event>";
      if (ns4) {
        proxy.document.form1.XMLmsg.value = msg;
        proxy.document.form1.submit ();
      } else if (ie4) {
        proxy.form1.XMLmsg.value = msg;
        proxy.form1.submit ();
      }	      
      if ( this.heartMax != 0 )
        this.reSetHeartBeat();
      if (this.isEventManagerLocked ()) {
        this.timeOutId = setTimeout ("ub.sessionTimeout ();", 500000);
        this.intervalId = setInterval ("ub.pollProxy ();", 500);
      }	  	
      return true;
    } 
    else {
      return false;
    }
  }
  return false;
}

function UiBroker_unregisterRuntimeObject (wndName, runtimeId)
{
  if ((wndName != null) && (runtimeId != null)) {
    this.runtimeObjectHash[wndName][runtimeId] = null;
  }
}

function UiBroker_getRuntimeObject (runtimeId)
{
  var wndHash = null;
  var wndName = null;
  var runtimeObject = null;
  for (wndName in this.runtimeObjectHash) {
    wndHash = this.runtimeObjectHash[wndName];
    runtimeObject = wndHash[runtimeId];
    if (runtimeObject != null) 
      return runtimeObject;
  }
  return runtimeObject;
}

function UiBroker_setOptionValues (rtid, state, count, price, atp, visible)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if ( visible )
      ctl.setState (state);
    else
      ctl.setState ('unknown');
    ctl.setCount (count);
    ctl.setPrice (price);
    ctl.setATP (atp);
    ctl.setVisibility(visible);
  }
}

function UiBroker_setFeatureValues (rtid, state, count, price, atp, bUnSatisfied, visibility, captionId)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if (ctl.setState)
      ctl.setState (state);
    if (ctl.setCount)
      ctl.setCount (count);
    if (ctl.setPrice) {
      if (price != "") {
        ctl.setPriceVisible(true);
        ctl.setPrice (price);
      }
    }
    if (ctl.setATP){
      if(atp != ""){
        ctl.setATPVisible(true);
        ctl.setATP (atp);
      }
    }
    if (ctl.setUnSatisfied)
      ctl.setUnSatisfied (bUnSatisfied);

    ctl.hideFeature(!visibility);
    if (visibility){
     if(!ctl.isVisible())
       ctl.show();
    }
    else
     ctl.hide();

    ctl.updateDone();

    var capCtl = null;
    if (captionId != null){
       capCtl = this.getRuntimeObject (captionId);
       if(visibility)
         capCtl.show();
       else
         capCtl.hide();
    }
  }
}

function UiBroker_setUnSatisfactionValue (rtid, bUnSatisfied)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    ctl.setUnSatisfied (bUnSatisfied);
  }
}

//function to change the availability state of a tree node at runtime
function UiBroker_setAvailabilityState (rtid, bAvail)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if ((ctl != null) && ctl.setAvailability) {
    ctl.setAvailability(bAvail);
  }
}

function UiBroker_setNumericValue (rtid, val)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    ctl.setValue (val);
  }
}

//Function to change Image and caption of controls at runtime
function UiBroker_setDisplayValue (rtid, propName, val)
{
  var ctl = null;
  if ((propName != 'image') && (propName != 'caption')) 
    return;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if (propName == 'image' && val != "")
      ctl.setSrc (IMAGESPATH + val);
    else if (propName == 'caption')
      ctl.setCaption (val);
  }
}

function UiBroker_setAppearence (iapp)
{
  this.appearence = iapp;
}

function UiBroker_getAppearence()
{
  return (this.appearence);
}

function UiBroker_setUILook(style)
{
  this.uiLook = style;
}

function UiBroker_getUILook(){
  return this.uiLook;
}

function UiBroker_registerUIFrames(wnd,ref)
{
  if (!this.ctrlFrame) this.ctrlFrame = new Array();
  if (!this.wndFrame) this.wndFrame = new Array();
  
  if ( wnd.isWindow ){
    this.wndFrame[this.wndCnt] = ref;
    this.wndCnt++;
  } else if ( wnd.isControl ){
    this.ctrlFrame[this.ctrlCnt] = ref;
    this.ctrlCnt++;
  }
}

function UiBroker_setWndBGColor(bgCol)
{
  this.wndCol = bgCol;
  var len = this.wndFrame.length;
  for (i=0;i <len; i++)
  {
    eval(this.wndFrame[i]).document.bgColor = bgCol;
  }
}

function UiBroker_setCtrlBGColor(bgCol)
{	
  this.ctrlCol = bgCol;
  var len = this.ctrlFrame.length;
  for (i=0;i<len; i++)
  {
    eval(this.ctrlFrame[i]).document.bgColor = bgCol;
  }
}

function UiBroker_setTxtColor(bgCol)
{	
  this.txtCol = bgCol;
}

function UiBroker_setContextInfo(ctx)
{
  var len = this.wndFrame.length;
  for ( i=0; i< len; i++ ){
    if (eval(this.wndFrame[i]).setContextInformation)
      eval(this.wndFrame[i]).setContextInformation(ctx);
  }
}

function UiBroker_addTarget (wndName, publishRef, relDepth)
{
  if (!this.targetHash) this.targetHash = new Array ();
  if (!this.compMgrHash) this.compMgrHash = new Array ();
  if (!this.evtMgrHash) this.evtMgrHash = new Array ();
  if (!this.runtimeObjectHash) this.runtimeObjectHash = new Array();
  if (!this.loadedHash) this.loadedHash = new Array ();
  if (!this.depthHash) this.depthHash = new Array ();
  if (!this.srcWdwRefHash) this.srcWdwRefHash = new Array();

  this.targetHash[wndName] = publishRef;
  this.compMgrHash[wndName] = new ComponentManager (wndName);
  this.runtimeObjectHash[wndName] = new Array();
  if ((relDepth != null) || (relDepth != 'undefined'))
    this.depthHash[wndName] = relDepth;
  if (wndName != 'czContent') {
    this.loadedHash[wndName] = false;
  }
  this.hasTargetRef = true;

  var parentStr = '';
  for (var i=0; i < relDepth; i++)
    parentStr += 'parent.';

  this.srcWdwRefHash[wndName] = parentStr + "frames." + window.name;  
}

function UiBroker_removeTarget (wndName)
{
  if (!this.hasTargetRef) return;
  this.targetHash[wndName] = null;
  this.compMgrHash[wndName] = null;
  this.runtimeObjectHash[wndName] = null;
  this.evtMgrHash[wndName] = null;
  
  delete this.targetHash[wndName];
  delete this.compMgrHash[wndName];
  delete this.evtMgrHash[wndName];
  delete this.runtimeObjectHash[wndName];
}

function UiBroker_getDepth (wndName)
{
  return (this.depthHash[wndName]);
}

function UiBroker_setDepth (wndName, depth)
{
  if (!this.depthHash) return;
  this.depthHash[wndName] = depth;
}

function UiBroker_createLabel(wndName, rtid, caption, lt, tp, wd, ht)
{
  var cm = this.compMgrHash[wndName];
  var re = /\r\n/gi;
  var cap = caption.replace(re,'<br>');
  var lbl = new Prompt();
  lbl.setName (rtid);
  lbl.setDimensions (lt, tp, wd, ht);
  lbl.setCaption (cap);
  lbl.setReference (this.targetHash[wndName]);
  lbl.setWindowName (wndName);
  lbl.type = 'label';
  lbl.setCaptionWrapping (true);

  cm.addComponent (lbl);
  this.registerRuntimeObject (wndName, lbl);
  
  return lbl;
}

function UiBroker_createLogicComboBox(wndName, rtid, lt, tp, wd, ht,  bUnSatisfied ,visibility,capId)
{
  var cm = this.compMgrHash[wndName];
  var cbo = new LogicComboBox();
  cbo.setName (rtid);
  cbo.setDimensions (lt, tp, wd, ht);
  cbo.setReference (this.targetHash[wndName]);
  cbo.setWindowName (wndName);
  cbo.setEventManager (this.evtMgrHash[wndName]);
  cbo.setBackgroundColor ('white');
  cbo.setActiveBackColor ('lightgrey');
  cbo.setSelectedBackColor ('white');
  if (! this.bNoStateIcons) {
    cbo.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  cbo.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  //cbo.setLogicStateProps (true, true, true);
  cbo.setUnSatisfied (bUnSatisfied);

  cbo.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }
  cbo.type = 'ComboBox';
  cbo.setTabindex(this.tabindex++);

  cm.addComponent (cbo);
  this.registerRuntimeObject (wndName, cbo);

  cbo.addListener ('objectAddedCallback', this);
  cbo.addListener ('actionMouseUp', this);

  cbo.readOnly(this.getReadOnly());

  return cbo;
}

function UiBroker_createLogicList(wndName, rtid, lt, tp, wd, ht,bUnSatisfied, visibility,capId)
{
  var cm = this.compMgrHash[wndName];
  var lst = new LogicList();
  lst.setName (rtid);
  lst.setDimensions (lt, tp, wd, ht);
  lst.setReference (this.targetHash[wndName]);
  lst.setWindowName (wndName);
  lst.setEventManager (this.evtMgrHash[wndName]);
  lst.setBackgroundColor ('white');
  lst.setActiveBackColor ('lightgrey');
  lst.setSelectedBackColor ('white');
  if (! this.bNoStateIcons) {
    lst.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  lst.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  //lst.setLogicStateProps (true, true, true);
  lst.setUnSatisfied (bUnSatisfied);

  lst.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
    capCtl = this.getRuntimeObject (capId);
    if(visibility)
      capCtl.show();
    else
      capCtl.hide();
  }
  lst.type = 'list';
  lst.setTabindex(this.tabindex++);

  cm.addComponent (lst);
  this.registerRuntimeObject (wndName, lst);
  
  lst.addListener ('objectAddedCallback', this);
  lst.addListener ('actionMouseUp', this);
  lst.addListener ('actionOnChange', this);

  lst.readOnly(this.getReadOnly());
  
  return lst;
}

function UiBroker_createLogicCheckBox(wndName, rtid, caption, lt, tp, wd, ht, state, price, atp, bUnSatisfied, visibility, capId)
{
  var cm = this.compMgrHash[wndName];
  var chk = new LogicCheckBox();
  chk.setName (rtid);
  chk.setDimensions (lt, tp, wd, ht);
  chk.setReference (this.targetHash[wndName]);
  chk.setWindowName (wndName);
  chk.setCaption(caption);
  chk.setState (state);
  chk.setPrice (price);
  chk.setATP (atp);
  chk.setUnSatisfied (bUnSatisfied);

  chk.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }

  chk.type = 'LogicCheckBox';
  chk.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  chk.setTabindex(this.tabindex++);

  cm.addComponent (chk);
  this.registerRuntimeObject (wndName, chk);

  chk.readOnly(this.getReadOnly());
  
  return chk;
}

function UiBroker_createBomItem(wndName, rtid, caption, lt, tp, wd, ht, state, count, price, atp, bUnSatisfied, visibility)
{
  var cm = this.compMgrHash[wndName];
  var itm = new BomItem ();
  itm.setName (rtid);
  itm.setDimensions (lt, tp, wd, ht);
  itm.setReference (this.targetHash[wndName]);
  itm.setWindowName (wndName);
  itm.setCaption(caption);
  itm.setState (state);
  itm.setCount (count);
  itm.setPrice (price);
  itm.setATP (atp);
  itm.setUnSatisfied (bUnSatisfied);
  itm.setBackgroundColor('white');
  if (this.appearence == UiBroker.FLAT_LOOK) {
    itm.setFlatLook(true);
    itm.setBorderColor('black');
  } else if (this.appearence == UiBroker._3DLOOK) {
    itm.set3DLook(true);
    itm.setBorderColor('#c6c3c6');
    //itm.setBorderColor('gray');
  }
  if(!visibility){
    itm.hideFeature(true);
  }
  itm.type = 'BomItem';
  itm.setTabindex(this.tabindex++);
  if (! this.bNoStateIcons) {
    itm.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  itm.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  cm.addComponent (itm);
  this.registerRuntimeObject (wndName, itm);

  itm.readOnly(this.getReadOnly());
  
  return itm;
}

function UiBroker_createTree (wndName, rtid, iconsAry, nodeAry, lt, tp, wd, ht)
{
  var cm = this.compMgrHash[wndName];
  if (! lt) {
    if (this.getUILook() == UiBroker.FORMS_LOOK_AND_FEEL)
      lt = 2;
    else
      lt = 25;
  }
  if (! tp)
    tp = 0;
  //get the window width and height of the target window;
  if (this.targetHash) {
    var doc = eval (this.targetHash[wndName]);
    if (ns4) {
      var ref = this.targetHash[wndName];
      var index = ref.lastIndexOf (".document");
      var strWdw = ref.substr (0, index);
      var wdw = eval (strWdw);
    }
  }
  var wndWidth = (ns4) ? wdw.innerWidth : doc.body.clientWidth;
  var wndHeight = (ns4) ? wdw.innerHeight : doc.body.clientHeight;
  /*
  if (! wd){
    if (ns4 && wdw.outerWidth < 700)
      wd = wndWidth + Math.ceil(wdw.innerWidth*0.30);
    else
      wd = wndWidth-lt;
  }
  */
  if (! wd)
    wd = wndWidth
  if (! ht)
    ht = wndHeight;

  //make adjustments for NS browser when things gets clipped
  wd += this.numRefreshTree * wd/4;
  ht += this.numRefreshTree * 1000;

  var tree = new Tree ();
  tree.setName (rtid);
  tree.setDimensions (lt, tp, wd, ht);
  tree.setReference (this.targetHash[wndName]);
  tree.setWindowName (wndName);
  tree.type = 'tree';
  tree.setBackgroundColor ("#cecf9c");
  tree.setSelectedBackColor ("#336699");
  tree.setColor ("#003366");
  tree.setSelectedColor ("#ffffff");

  tree.addListener ('objectAddedCallback', this);
  for (var i=0; i<iconsAry.length; i++) {
    var src = iconsAry[i].src;
    tree.setNodeIcons (iconsAry[i].key, src);
  }
  cm.addComponent (tree);
  this.registerRuntimeObject (wndName, tree);
  if (nodeAry) {
    tree.addNodes (nodeAry);
  }
  var fnt = this.createFontObject("Times New Roman","12pt","normal","normal");
  tree.setFont(fnt);
  return tree;
}

function UiBroker_createImage(wndName, rtid, lt, tp, wd, ht, src)
{
  var cm = this.compMgrHash[wndName];
  var img = new ImageButton();
  img.setName (rtid);
  img.setDimensions (lt, tp, wd, ht);
  img.setReference (this.targetHash[wndName]);
  img.setWindowName (wndName);
  if ( src != "")
    img.setSrc (IMAGESPATH + src);
  img.type = 'image';
  cm.addComponent (img);
  this.registerRuntimeObject (wndName, img);
  
  return img;
}

function UiBroker_createTextInput(wndName, rtid, lt, tp, wd, ht, val, visibility, capId)
{
  var cm = this.compMgrHash[wndName];
  var txt = new InputText();
  txt.setName (rtid);
  txt.setDimensions (lt, tp, wd, ht);
  txt.setReference (this.targetHash[wndName]);
  txt.setWindowName (wndName);
  txt.setValue(val);
  if (this.getReadOnly())
    txt.EDITABLE = false;
  else
    txt.EDITABLE = true;
  txt.type = 'text';
  txt.setBackgroundColor ('white');
  txt.standalone = true;
  txt.hideFeature(!visibility);

  var capCtl = null;
  if (capId != null) {
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }
  txt.setTabindex(this.tabindex++);
  cm.addComponent (txt);
  this.registerRuntimeObject (wndName, txt);
  
  return txt;	
}

function UiBroker_createNumericInput(wndName, rtid, val, lt, tp, wd, ht)
{
  return (this.createTextInput (wndName, rtid, val, lt, tp, wd, ht));
}

function UiBroker_createButton(wndName, rtid, caption, lt, tp, wd, ht, imgSrc)
{
  var cm = this.compMgrHash[wndName];
  var btn;
  if ((imgSrc != null) && (imgSrc != '')) {
    btn = new ImageButton ();
    btn.setImages (IMAGESPATH + imgSrc);
  } else {
    btn = new CurvedButton();
    btn.setLeftCurveWidth (11);
    btn.setRightCurveWidth (11);
    btn.setCaption (caption);
  }
  btn.setName (rtid);
  btn.setDimensions (lt, tp, wd, ht);
  btn.setReference (this.targetHash[wndName]);
  btn.setWindowName (wndName);
  btn.type = 'button';
  btn.setTabindex(this.tabindex++);
  cm.addComponent (btn);
  this.registerRuntimeObject (wndName, btn);
  return btn;	
}

function UiBroker_createFontObject (family, size, style, weight)
{
  return (new Font (family, size, style, weight));
}

function UiBroker_outputToFrame (target, cmdStr, bIsJSOnly)
{
  var wdw = null;
  wdw = eval (target);
  if (wdw != null) {
    var doc = wdw.document;
    doc.open ("text/html");
    if (bIsJSOnly) {
      doc.writeln ("<HTML><HEAD></HEAD><BODY>");
      doc.writeln ("<SCRIPT language=javascript>");
      doc.writeln (cmdStr);
      doc.writeln ("<\/SCRIPT>");
      doc.writeln ("</BODY></HTML>");
    } else {
      doc.writeln (cmdStr);
    }
    doc.close ();
  }
}

function UiBroker_getEventManager (wndName)
{
  return (this.evtMgrHash[wndName]);
}

function UiBroker_getComponentManager (wndName)
{
  return (this.compMgrHash[wndName]);
}

function UiBroker_createMessage ()
{
  var msgTarget = this.getMessageTarget ();
  
  lbl = new Base();
  lbl.setName ('MSG1');
  lbl.setDimensions (0, 0, 0, 0);
  lbl.setBackgroundColor ('white');
  lbl.setZIndex (10000);
  lbl.setReference (this.targetHash[msgTarget]);
  lbl.hide ();
  if (msgTarget != null)
    this.compMgrHash[msgTarget].addComponent (lbl);

  this.messageBox= lbl;
}

var dlg = null;
var dlgOpen = false;

function checkDialog () {
  if (! dlgOpen) {
    if (ub.bShowingMessage) {
      ub.showMessage(ub.msgTxt);
    } else {
      dlgOpen = false;
    }
  }
}

function _check_msg_window()
{
  if (ub.bShowingMessage) {
    if (dlgOpen) {
      dlg.focus();
    } else {
      ub.showMessage(ub.msgTxt);
    }
  }
}

function UiBroker_setMsgTitle(title)
{
  this.messageType = title;
}

function UiBroker_showMessage (msg)
{
  //because of launch pad we have to handle in a spcial way
  if ( ns4 )
    dlg = window.open ("", "msgBox", "width=500,height=250,resizable=yes,scrollbars=yes", true);
  else
    dlg = window.open ("", "_blank", "width=500,height=250,resizable=yes,scrollbars=yes", true);

  dlgOpen = true;

  var doc = dlg.document;
  doc.open ("text/html");
  doc.writeln ("<html>");
  doc.writeln ("<title>"+ this.messageType +"</title>");
  if ( this.wndCol != "" )
    doc.writeln ("<body bgColor='" + this.wndCol + "' onUnload=\"javascript:opener.dlgOpen=false; opener.checkDialog();\">");
  else
    doc.writeln ("<body onUnload=\"javascript:opener.dlgOpen=false; opener.checkDialog();\">");
  if ( this.txtCol != "" )
    doc.writeln ("<FONT color='"+ this.txtCol + "'>"+ msg +"</FONT>");
  else
    doc.writeln (msg);
  doc.writeln ("</body></html>");
  doc.close ();
  dlg.focus();
  this.msgTxt = msg;
  this.lockEventManagers ();
  this.bShowingMessage = true;
  this.atpShown = false;
}

function UiBroker_launchWindow(url)
{
    _wURL = window.open(url,"");
}

function UiBroker_hideMessage ()
{
  this.bShowingMessage = false;
  if (dlgOpen) {
    dlg.close();
    this.msgTxt = "";
  }
}

function UiBroker_showContradiction (msg, idYes, idNo, yesCaption, noCaption)
{
  if (! yesCaption)
    yesCaption = "Yes";
  
  if (! noCaption)
    noCaption = "No";
  
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + yesCaption + '" id="' + idYes + '" name="' + idYes;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idYes + '\');">';
  sBuffer += '&nbsp;&nbsp;&nbsp;';
  sBuffer += '<INPUT type="button" value="' + noCaption + '" id="' + idNo + '" name="' + idNo;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idNo + '\');">';
  sBuffer += '</FORM>';
  sBuffer =  msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Contradiction";
  this.showMessage (sBuffer);
}

function UiBroker_showValidation (msg, idOk, okCaption)
{
  if (! okCaption)
    okCaption = "OK";
  
  //add OK button html tags with the message
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + okCaption + '" id="' + idOk + '" name="' + idOk;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idOk +  '\');">';
  sBuffer += '</FORM>';
  sBuffer = msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Validation";
  this.showMessage (sBuffer);
}

function UiBroker_showConfirmation (msg, idOk, idCancel, okCaption, cancelCaption)
{
  if (! okCaption)
    okCaption = "OK";
  
  if (! cancelCaption)
    cancelCaption = "Cancel";
  
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + okCaption + '" id="' + idOk + '" name="' + idOk;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idOk + '\');">';
  sBuffer += '&nbsp;&nbsp;&nbsp;';
  sBuffer += '<INPUT type="button" value="' + cancelCaption + '" id="' + idCancel + '" name="' + idCancel;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idCancel + '\');">';
  sBuffer += '</FORM>';
  sBuffer = msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Confirmation";
  this.showMessage (sBuffer);
}

function UiBroker_showFatalError (msg)
{
  if (! this.appExited) {
    this.showExitMessage(msg);
  }
}

function UiBroker_showExitMessage (msg)
{
  //this.clearContentFrame ();
  this.contentMode = 'empty';
  msg = '<DIV ALIGN="CENTER">' + msg + '</DIV>';
  this.publish('czContent', null, null, msg);
  this.appExited = true;
}

function UiBroker_messageCallback (id)
{
  var xmlMsg;
  xmlMsg = "<click rtid='" + this.formatRuntimeId(id) +  "'/>";
  this.bEvtMgrLocked = false;
  this.postClientMessage (xmlMsg);
  this.hideMessage ();
}

function UiBroker_showPromptInput (rtid, msg, defaultText, idInput, idOk, idCancel)
{
  var xmlMsg = '';
  //for now use regular browsers prompt to gather input;
  if (defaultText == null)
    defaultText = '';
  var result = window.prompt (msg, defaulText);
  if (result == null) {
    //user clicked cancel button send idCancel click to the server;
    xmlMsg = "<click rtid='" + this.formatRuntimeId(idCancel) +  "'/>";
  } else {
    //post input event message with value of the text;
    xmlMsg = "<input rtid='" + this.formatRuntimeId(idInput) + "'>";
    xmlMsg += result + "</input>";
  }
  //this.messageType="Prompt Input";
  this.postClientMessage (xmlMsg);
}

function UiBroker_publish(wndName, headContent, bodyAttributes, bodyContent)
{
  // mac related
  this.alreadyPublished = true;
  this.needRefresh = false;

  if(this.curSeqNum != -1) {
    if (wndName != null) {
      if ((this.getContentMode() == 'config') && (wndName == 'czContent')) {
        this.loadedHash[wndName] = true;
        return;
      }
      this.loadedHash[wndName] = false;
      if (!headContent) {
        if (this.defaultHeadContent)
          headContent = this.defaultHeadContent;
        else
          headContent = "";
      }
      if (!bodyAttributes) bodyAttributes = "";
      if (!bodyContent) bodyContent = "";
    
      //var parentStr = '';
      //for (var i=0; i < this.depthHash[wndName]; i++)
      //  parentStr += 'parent.';

      timeArray [0] = new Array ();
      timeArray[0][0] = new Date ();

      var doc = eval(this.targetHash[wndName]);
      //ToDo: Do some research. IE browser back button doesn't work properly
      //doc.open("text/html", "replace");
      doc.open("text/html");
      doc.writeln("<HTML>");
      doc.writeln("<HEAD>");
      doc.writeln('<META HTTP-EQIV="Content-Type" CONTENT="text/html; charset=' + this.charSetEncoding + '">');
      doc.writeln('<META HTTP-EQUIV="Expires" CONTENT="-1">');
      doc.writeln('<META HTTP-EQUIV="Pragma" CONTENT="no-cache">');
      doc.writeln(headContent);
      //create a message box for on the specified target or on the summary screen;
      //to display error message;
      if ((wndName == this.messageTarget) || (wndName == 'czContent')) {
        this.createMessage ();
      }
    
      timeArray [1] = new Array ();
      timeArray [1][0] = new Date ();
      this.compMgrHash[wndName].generateCSS(doc);
      timeArray[1][1] = new Date ();
      timeArray[1][2] = "Gen and Write Sytle sheet";

      if (this.bUseLogicColor) {
        doc.writeln ('<STYLE type="text/css">');
        if (this.utColor)
          doc.writeln ('.' + UT_COLOR + '{color:' + this.utColor + ';}');
        if (this.ltColor)
          doc.writeln ('.' + LT_COLOR + '{color:' + this.ltColor + '; }');
        if (this.ufColor)
          doc.writeln ('.' + UF_COLOR + '{color:' + this.ufColor + '; }');
        if (this.ufColor)
          doc.writeln ('.' + LF_COLOR + '{color:' + this.lfColor + '; }');
        if (this.unColor)
          doc.writeln ('.' + UN_COLOR + '{color:' + this.unColor + '; }');
       
        //boldness property will be there only if there is logic state color
        if (this.bUseLogicBold) {
          if (this.utBold)
            doc.writeln ('.' + UT_BOLD + '{font-weight:bold;}');
          if (this.ltBold)
            doc.writeln ('.' + LT_BOLD + '{font-weight:bold;}');
          if (this.ufBold)
            doc.writeln ('.' + UF_BOLD + '{font-weight:bold;}');
          if (this.lfBold)
            doc.writeln ('.' + LF_BOLD + '{font-weight:bold;}');
          if (this.unBold)
            doc.writeln ('.' + UN_BOLD + '{font-weight:bold;}');
        }
        doc.writeln ('</STYLE>');
      }
      this.cnt++;
      this.backSeqNum = this.cnt;
      doc.writeln("<script launguage=javascript>");
	doc.writeln("var seq="+ this.cnt +";");
      doc.writeln("</script>");
      doc.writeln("</HEAD>");
      doc.writeln("<BODY " + bodyAttributes + " tabindex=1" + 
                " onload='javascript:if (" + this.srcWdwRefHash[wndName] + ".isLoaded) {" + 
                " var ub = " + this.srcWdwRefHash[wndName] + ".getUiBroker ();" + 
                " ub.launch (\"" + wndName + "\", document, seq);" +
                " ub.loadingFinished (\"" + wndName + "\");" + 
                " ub = null;" +
                " delete ub;" +                
                "}' onfocus='javascript:" + this.srcWdwRefHash[wndName] + 
                ".window_onfocus(\"" + wndName + "\");'>");
      doc.writeln(bodyContent);

      timeArray [2] = new Array ();
      timeArray [2][0] = new Date ();
      this.compMgrHash[wndName].renderComponents(doc);
      timeArray[2][1] = new Date ();
      timeArray[2][2] = "Gen & Write Render String";

      doc.writeln ('<SCRIPT language=javascript>');
      if (ns4) {
        doc.writeln ('window.captureEvents (Event.RESIZE);');
        doc.writeln ('window.onresize='+ this.srcWdwRefHash[wndName] + '.window_resizeCallback;');
      }
      doc.write ("function doOnKeyDown (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyDown(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnKeyPress (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyPress(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnKeyUp (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyUp(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnFocus (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnFocus(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnBlur (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnBlur(e)');");
      doc.writeln ("return res;}");
      doc.writeln ('<\/SCRIPT>');
      doc.writeln("</BODY>");
      doc.writeln("</HTML>");
      this.fromPublishedPage = true;
      doc.close();

      timeArray[0][1] = new Date ();
      timeArray[0][2] = "Write new page";
      //writeTimes ();

    } else { 
        //write to our document;
        this.compMgr.generateCSS();
        this.compMgr.renderComponents();
    }
  }
  else
  {
    _hWnd = this.errorReport = window.open ("", "errorReport",
                                         "width=500,height=250,resizable=yes,scrollbars=yes", true);
    _hWnd.document.open ();
    if(bodyContent == "")
      bodyContent = "Please check the log files for detailed error";
    _hWnd.document.writeln('<html><head><title>Error</title></head><body>'+bodyContent+'</body></html>');
    _hWnd.document.close();
  }
  if ( this.contentMode == 'config' )
    this.contentMode = 'summary';

  if ( this.contentMode == 'summary')
    this.contentMode = 'config';
  else
    this.contentMode = 'summary';

  if (this.showOn)
    this.showOn = false;
}

function UiBroker_publishAll (bodyAttributes)
{
  //before start publishing to all frames first set the loadedHash of all frames as false;
  for (var wnd in this.targetHash)
    this.loadedHash[wnd] = false;
  
  if (this.hasTargetRef) {
    for (var wnd in this.targetHash)
      this.publish (wnd, null, bodyAttributes);
  } else
    this.publish ();
}

function UiBroker_launch(wndName, docObj, seq) 
{
  if ( ie4 ){
    if ( seq < 2 ){
      this.backSeqNum--;
    } else if ( seq < this.backSeqNum ){
      this.goBack();
      return true;
    } else
      this.backSeqNum = seq;
  }
  if (wndName != null) {
    var em = EventManager.instances[wndName];
    //for summary there is no event manager.
    if (em) {
      this.compMgrHash[wndName].launchComponents(em, true);
      em.init (docObj);
    }
  } else
    this.compMgr.launchComponents (this.evtMgr);
}

function UiBroker_loadingFinished (wndName)
{
  if (wndName != null) {
    this.loadedHash[wndName] = true;
    if (this.allFramesLoaded ()) {
      if (parent.frames.czProxy.doModelUpdate) {
        parent.frames.czProxy.doModelUpdate ();
      }
    } else
      this.bFinishedLoading = true;
  }
}

function UiBroker_allFramesLoaded ()
{
  if (this.hasTargetRef) {
    for (var wnd in this.targetHash) {
      if (this.loadedHash[wnd] != null)
        if (! this.loadedHash[wnd])
          return (false);
    }
  } else if (!this.bFinishedLoading)
    return (false);
  return (true);
}

function UiBroker_refresh(wndName) 
{
  if ((wndName != null) && this.hasTargetRef) {
    this.compMgrHash[wndName].destroy ();
    this.compMgrHash[wndName] = null;
    delete this.compMgrHash[wndName];
    
    if (this.evtMgrHash[wndName]) {
      this.evtMgrHash[wndName].destroy ();
      this.evtMgrHash[wndName] = null;
      delete this.evtMgrHash[wndName];
      EventManager.clearInstance (wndName);
    }
    Utils.clearCollection (this.runtimeObjectHash[wndName]);
    
    //TO DO: Redo when we make tree with our own scroll bar;
    //TO DO: fix for scroll drag in multiple frames;
    Drag.clearInstance ('BarDrag');
      
    this.compMgrHash[wndName] = new ComponentManager (wndName);
    this.runtimeObjectHash[wndName] = new Array ();
    
    var em = new EventManager (wndName);
    em.setCompMgr (this.compMgrHash[wndName]);
    this.evtMgrHash[wndName] = em;
  } else {
    this.compMgr = new ComponentManager ();
    this.evtMgr = new EventManager ();
  }
  this.tabindex = 1;
}

function UiBroker_setDefaultHeadContent (content)
{
  this.defaultHeadContent = content;
}

function UiBroker_lockEventManagers ()
{
  var item;
  for (item in this.evtMgrHash) {
    var em = this.evtMgrHash[item];
    em.lock ();
    this.bEvtMgrLocked = true;
  }
}

function UiBroker_unLockEventManagers ()
{
  //clear timeouts;
  this.clearTimeoutHandles ();

  //unlock event managers only when a message window is not shown;
  if (! this.bShowingMessage) {
    var item;
    for (item in this.evtMgrHash) {
      var em = this.evtMgrHash[item];
      em.unLock ();
      this.bEvtMgrLocked = false;
    }
    if (this.pendingEvt) {
      this.pendingEvt = false;
      this.postClientMessage(this.evtType);
      if(this.pendingObj != null)
        this.navigateTo(this.pendingObj.getName());
    }
  } else {
    //A message is shown. Clear the event queue;
    this.pendingEvt = false;
    this.evtType = null;
    this.pendingObj = null;
  }
  if ( this.msgTxt == "" && this.needRefresh && !this.alreadyPublished && this.contentMode == 'config' ){
    this.backSeqNum = 0;
    this.cnt = 0;
    this.refreshTreeFrame();
    this.needRefresh = false;
  }
  this.alreadyPublished = false;
}

function UiBroker_isEventManagerLocked ()
{
  return (this.bEvtMgrLocked);
}

/**
* Mostly this function will get called when showing messages
* @param - , separated list of window name/id
*/
function UiBroker_lockEventManagersExcept ()
{
  //First lock all the event manager, then go thru the argument list and;
  //unlock them;
  this.lockEventManagers ();
  var len = arguments.length;
  for (var i=0; i < len; i++) {
    var evtMgr = this.getEventManager (arguments[i]);
    if (evtMgr != null)
      evtMgr.unLock ();
  }
}

function UiBroker_setSessionStartURL (sessionUrl)
{
  this.sessionStartURL = sessionUrl;
}

function UiBroker_getSessionStartURL ()
{
  return (this.sessionStartURL);
}

function UiBroker_setDebugMode (bMode)
{
  var console;
  this.debugMode = bMode;
  if (!bMode) {
    if (this.dbgWnd != null) {
      console = this.dbgWnd;
      console.document.close ();
      console.close ();
    }
  }
}

function UiBroker_log (msg, stTime, endTime)
{
  var console;
  var consoleDoc;
  if (this.debugMode) {
    if ((this.dbgWnd == null) || (this.dbgWnd.closed)) {
      console = this.dbgWnd = window.open ("", "console", 
                                           "width=500,height=250,resizable=yes,scrollbars=yes", true);
      console.document.open ("text/plain");
    } else {
      console = this.dbgWnd;
    }
    consoleDoc = console.document;
    if (stTime && endTime) {
      //consoleDoc.writeln (this.formatTime (stTime) + "Begin " + msg);
      //consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
      var msec = endTime.valueOf() - stTime.valueOf();
      consoleDoc.writeln ('[' + msec + ']' + "Elapsed " + msg);
    } else if (stTime && (endTime == null)) {
      this.startTime = stTime;
      this.endTime = null;	
      consoleDoc.writeln (this.formatTime (this.startTime) + "Begin " + msg);
    } else if (endTime && (stTime == null)) {
      if (this.startTime) {
        consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
        var msec = endTime.valueOf() - this.startTime.valueOf();
        consoleDoc.writeln ('[' + msec + ']' + "Elapsed " + msg);
      } else {
        consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
      }
      this.startTime = null;
      this.endTime = null;	
    } else {
      var dt = new Date ();
      consoleDoc.writeln (this.formatTime (dt) + msg);
      this.startTime = null;
      this.endTime = null;	
    }
  }
}

function UiBroker_formatTime (dt)
{
  var timeStr = '[' + dt.getHours () + ':' + dt.getMinutes () + ':';
  timeStr += dt.getSeconds () + ':' + dt.getMilliseconds () + '] ';
  return (timeStr);
}

function UiBroker_registerPage (seqNum, navCmd, page)
{
  this.log ("Sending message to server", null, new Date())
  var retVal = true;
  if (navCmd != '') {
    navCmd = navCmd.replace (/&#60;/, '<');
    if ( navCmd == this.navCmd && seqNum < this.curSeqNum && page == 1){
 	this.showConfiguration();
    }else{
    	this.navCmd = navCmd;
    }
    //this.navCmd = navCmd.substring(navCmd.indexOf("rtid")+6,navCmd.indexOf("'/>"));
  }else
    this.navCmd = '';

  if ((seqNum == this.curSeqNum) && (page == 1))
  {
      this.goBack ();
      //this.back = true;
      this.register = false;
      retVal = false;
      return (retVal);
  }

  if (seqNum == this.curSeqNum)
  {
      //this.refreshCurrentPage ();
      retVal = false;
  }
  else if (seqNum < this.curSeqNum)
  {
      this.goBack ();
      //this.back = true;
      this.register = false;
      retVal = false;
  }
  else if (navCmd != '<show-summary/>')
  {
      //If the navCmd is an empty string, update seqNum and return true
      if (navCmd == '' || page == 1) {
          this.curSeqNum = seqNum;
          return (retVal);
      }
      var entry = this.pageHistory.peek ();
      if (entry)
      {
          if (navCmd != entry) {
            this.pageHistory.push (navCmd);
            this.register = true;
          }
     } else {
        this.pageHistory.push (navCmd);
        this.register = true;
     }
     this.curSeqNum = seqNum;
  }
  return (retVal);
}

function UiBroker_goBack ()
{
  if (this.contentMode == 'summary') {
	this.showConfiguration();
      this.back=false;
  }else{
  	if (this.pageHistory.entries.length > 1)
    	this.pageHistory.pop ();

  	var msg = this.pageHistory.peek ();
  	if (msg)
    		this.postClientMessage(msg);
		if ( this.pageHistory.entries.length > 1)
              this.back=true;
            else
		  this.back=false;
  }
}

function UiBroker_refreshCurrentPage ()
{
  var msg = this.pageHistory.peek ();
  this.postClientMessage(msg);	
}

function UiBroker_navigateTo (runtimeId)
{
  var obj = this.getRuntimeObject (runtimeId);
  if (obj != null) {
    //currently only tree node navigate is supported;
    if (obj.type == 'TreeNode') {
      var tree = obj.getParentObject (obj);
      tree.setSelectedNode (obj);
    }
  }
}

function UiBroker_showOutput(outputId)
{
  var servletUrl = parent.frames.czProxy.location;
  var url = servletUrl + "?generateOutput=" + outputId + "&sessionId=" + this.sessionId;
  var wd = 300;
  var ht = 300;
  if (ie4) {
    wd = top.window.document.body.clientWidth/2;
    ht = top.window.document.body.clientHeight/2;
  } else if (ns4) {
    wd = top.window.innerWidth/2;
    ht = top.window.innerHeight/2;
  }
  var newWindow = window.open(url, "czFcOutput", "menubar=no,resizable=yes,scrollbars=yes,width="+ wd + ",height="+ ht);
}

function UiBroker_showUrl(url)
{
  var newWindow = window.open(url, "czUserOutpu", "menubar=no");
}

function UiBroker_pollProxy ()
{
  var proxy = this.proxy;
  if (ie4) {
    if (proxy.document.readyState == "complete") {
      if (! proxy.doLayout) {
        var msg = proxy.document.body.innerHTML;
        // clear all time out;
        this.clearTimeoutHandles ();
        this.showExitMessage (msg);
      }
    }
  }
}

function UiBroker_sessionTimeout ()
{
  var msg = "Problem sending request or recieving response from the server.";
  this.showExitMessage (msg);
  this.clearTimeoutHandles ();
}

function UiBroker_clearTimeoutHandles ()
{
  if (this.timeOutId != null) {
    clearTimeout (this.timeOutId);
  }
  if (this.intervalId != null) {
    clearInterval (this.intervalId);
  }
}

function UiBroker_refreshTreeFrame ()
{
  this.numRefreshTree++;
  this.postClientMessage ('<refresh-frames/>', true);
}

function UiBroker_setCharacterSetProfile (charSetEnc)
{
  this.charSetEncoding = charSetEnc;
}

function UiBroker_setServletURL (url)
{
  this.servletURL = url;
}

function UiBroker_getServletURL ()
{
  return (this.servletURL);
}

function UiBroker_setStateToolTip(utTip, ufTip, ltTip, lfTip, unTip, unsatTip)
{
  self._utrueToolTip = utTip;
  self._ufalseToolTip = ufTip;
  self._ltrueToolTip = ltTip;
  self._lfalseToolTip = lfTip;
  self._unknownToolTip = unTip;
  self._unsatToolTip = unsatTip;
}

//static members
UiBroker.NO_BORDER_LOOK = 0;
UiBroker.FLAT_LOOK = 1;
UiBroker._3DLook = 2;
UiBroker.FORMS_LOOK_AND_FEEL = 'FORMS';

UiBroker.prototype.setSessionId = UiBroker_setSessionId;
UiBroker.prototype.getSessionId = UiBroker_getSessionId;
UiBroker.prototype.mouseupCallback = UiBroker_mouseupCallback;
UiBroker.prototype.onchangeCallback = UiBroker_onchangeCallback;
UiBroker.prototype.objectAddedCallback = UiBroker_objectAddedCallback;
UiBroker.prototype.actionMouseUp = UiBroker_actionMouseUp;
UiBroker.prototype.actionOnChange = UiBroker_actionOnChange;
UiBroker.prototype.onClick = UiBroker_onClick;

UiBroker.prototype.postClientMessage = UiBroker_postClientMessage;
UiBroker.prototype.addTarget = UiBroker_addTarget;
UiBroker.prototype.removeTarget = UiBroker_removeTarget;
UiBroker.prototype.setMessageTarget = UiBroker_setMessageTarget;
UiBroker.prototype.getMessageTarget = UiBroker_getMessageTarget;

UiBroker.prototype.setDebugMode = UiBroker_setDebugMode;
UiBroker.prototype.log = UiBroker_log;
UiBroker.prototype.formatTime = UiBroker_formatTime;

UiBroker.prototype.getRuntimeObject = UiBroker_getRuntimeObject;
UiBroker.prototype.navigateTo = UiBroker_navigateTo;

UiBroker.prototype.createLabel = UiBroker_createLabel;
UiBroker.prototype.createLogicList = UiBroker_createLogicList;
UiBroker.prototype.createLogicComboBox = UiBroker_createLogicComboBox;
UiBroker.prototype.createLogicCheckBox = UiBroker_createLogicCheckBox;
UiBroker.prototype.createBomItem = UiBroker_createBomItem;
UiBroker.prototype.createTextInput = UiBroker_createTextInput;
UiBroker.prototype.createNumericInput = UiBroker_createNumericInput;
UiBroker.prototype.createImage = UiBroker_createImage;
UiBroker.prototype.createButton = UiBroker_createButton;
UiBroker.prototype.createTree = UiBroker_createTree;
UiBroker.prototype.createBomItem = UiBroker_createBomItem;
UiBroker.prototype.createFontObject = UiBroker_createFontObject;
UiBroker.prototype.setDefaultLogicImages = UiBroker_setDefaultLogicImages;
UiBroker.prototype.setLogicColors = UiBroker_setLogicColors;
UiBroker.prototype.setLogicBold = UiBroker_setLogicBold;
UiBroker.prototype.setNoStateIcons = UiBroker_setNoStateIcons;
UiBroker.prototype.outputToFrame = UiBroker_outputToFrame;
UiBroker.prototype.deleteComponent = UiBroker_deleteComponent;

UiBroker.prototype.setOptionValues = UiBroker_setOptionValues;
UiBroker.prototype.setFeatureValues = UiBroker_setFeatureValues;
UiBroker.prototype.setUnSatisfactionValue = UiBroker_setUnSatisfactionValue;
UiBroker.prototype.setAvailabilityState = UiBroker_setAvailabilityState;
UiBroker.prototype.setNumericValue = UiBroker_setNumericValue;
UiBroker.prototype.setValue = UiBroker_setNumericValue;
UiBroker.prototype.setDisplayValue = UiBroker_setDisplayValue;

UiBroker.prototype.getComponentManager = UiBroker_getComponentManager;
UiBroker.prototype.getEventManager = UiBroker_getEventManager;
UiBroker.prototype.lockEventManagers = UiBroker_lockEventManagers;
UiBroker.prototype.unLockEventManagers = UiBroker_unLockEventManagers;
UiBroker.prototype.isEventManagerLocked = UiBroker_isEventManagerLocked;
UiBroker.prototype.lockEventManagersExcept = UiBroker_lockEventManagersExcept;

UiBroker.prototype.publish = UiBroker_publish;
UiBroker.prototype.publishAll = UiBroker_publishAll;
UiBroker.prototype.refresh = UiBroker_refresh;
UiBroker.prototype.launch = UiBroker_launch;
UiBroker.prototype.launchWindow = UiBroker_launchWindow;
UiBroker.prototype.loadingFinished = UiBroker_loadingFinished;
UiBroker.prototype.allFramesLoaded = UiBroker_allFramesLoaded;
UiBroker.prototype.hasAllFramesFinishedLoading = UiBroker_allFramesLoaded; //deprecated

UiBroker.prototype.setMsgTitle = UiBroker_setMsgTitle;
UiBroker.prototype.createMessage = UiBroker_createMessage;
UiBroker.prototype.showMessage = UiBroker_showMessage;
UiBroker.prototype.hideMessage = UiBroker_hideMessage;
UiBroker.prototype.showContradiction = UiBroker_showContradiction;
UiBroker.prototype.showValidation = UiBroker_showValidation;
UiBroker.prototype.showConfirmation = UiBroker_showConfirmation;
UiBroker.prototype.showPromptInput = UiBroker_showPromptInput;
UiBroker.prototype.showFatalError = UiBroker_showFatalError;
UiBroker.prototype.showExitMessage = UiBroker_showExitMessage;

UiBroker.prototype.showSummary = UiBroker_showSummary;
UiBroker.prototype.showConfiguration = UiBroker_showConfiguration;
UiBroker.prototype.completeConfiguration = UiBroker_completeConfiguration;
UiBroker.prototype.submitConfiguration = UiBroker_completeConfiguration;
UiBroker.prototype.saveConfiguration = UiBroker_saveConfiguration;
UiBroker.prototype.cancelConfiguration = UiBroker_cancelConfiguration;
UiBroker.prototype.configurationHelp = UiBroker_configurationHelp;
UiBroker.prototype.showATP = UiBroker_showATP;
UiBroker.prototype.getContentMode = UiBroker_getContentMode;
UiBroker.prototype.navigateToScreen = UiBroker_navigateToScreen;

UiBroker.prototype.registerPage = UiBroker_registerPage;
UiBroker.prototype.readOnlySession = UiBroker_readOnlySession;
UiBroker.prototype.getReadOnly = UiBroker_getReadOnly;

UiBroker.prototype.refreshTreeFrame = UiBroker_refreshTreeFrame;
UiBroker.prototype.setCharacterSetProfile = UiBroker_setCharacterSetProfile;
UiBroker.prototype.setServletURL = UiBroker_setServletURL;
UiBroker.prototype.getServletURL = UiBroker_getServletURL;
UiBroker.prototype.setStateToolTip = UiBroker_setStateToolTip;

// methods for heartbeat
UiBroker.prototype.postClientHeartBeat = UiBroker_postClientHeartBeat;
UiBroker.prototype.setMaxHeartBeat = UiBroker_setMaxHeartBeat;
UiBroker.prototype.getMaxHeartBeat = UiBroker_getMaxHeartBeat;
UiBroker.prototype.setHeartBeat = UiBroker_setHeartBeat;
UiBroker.prototype.getHeartBeat = UiBroker_getHeartBeat;
UiBroker.prototype.startHB = UiBroker_startHB;
UiBroker.prototype.stopHB = UiBroker_stopHB;
UiBroker.prototype.reSetHeartBeat = UiBroker_reSetHeartBeat;

//private methods
UiBroker.prototype.registerRuntimeObject = UiBroker_registerRuntimeObject;
UiBroker.prototype.unregisterRuntimeObject = UiBroker_unregisterRuntimeObject;
UiBroker.prototype.formatRuntimeId = UiBroker_formatRuntimeId;
UiBroker.prototype.messageCallback = UiBroker_messageCallback;

UiBroker.prototype.setDefaultHeadContent = UiBroker_setDefaultHeadContent;
UiBroker.prototype.setSessionStartURL = UiBroker_setSessionStartURL;
UiBroker.prototype.getSessionStartURL = UiBroker_getSessionStartURL;
UiBroker.prototype.clearContentFrame = UiBroker_clearContentFrame;

UiBroker.prototype.refreshCurrentPage = UiBroker_refreshCurrentPage;
UiBroker.prototype.goBack = UiBroker_goBack;

UiBroker.prototype.showOutput = UiBroker_showOutput;
UiBroker.prototype.showUrl = UiBroker_showUrl;

UiBroker.prototype.sessionTimeout = UiBroker_sessionTimeout;
UiBroker.prototype.pollProxy = UiBroker_pollProxy;
UiBroker.prototype.clearTimeoutHandles = UiBroker_clearTimeoutHandles;

//set context information 
UiBroker.prototype.setContextInfo = UiBroker_setContextInfo;

// set Background Color and Fore ground Color
UiBroker.prototype.setWndBGColor = UiBroker_setWndBGColor;
UiBroker.prototype.setCtrlBGColor = UiBroker_setCtrlBGColor;
UiBroker.prototype.setTxtColor = UiBroker_setTxtColor;

UiBroker.prototype.registerUIFrames = UiBroker_registerUIFrames;

// set and get method for ui Look
UiBroker.prototype.setUILook = UiBroker_setUILook;
UiBroker.prototype.getUILook = UiBroker_getUILook;
UiBroker.prototype.setAppearence = UiBroker_setAppearence;
UiBroker.prototype.getAppearence = UiBroker_getAppearence;

UiBroker.prototype.terminate = UiBroker_terminate;

timeArray = new Array ();
wdw = null;

function writeTimes (optTimeAry)
{
  if (! wdw) {
    wdw = window.open ("", "newconsole", "width=500,height=250,resizable=yes,scrollbars=yes", true);
    wdw.document.open ("text/plain");
  }
  var wdwDoc = wdw.document;
  if (optTimeAry) {
    var tAry = optTimeAry;
  } else {
    var tAry = timeArray;
  }
  var len = tAry.length;
  for (var i=0; i<len; i++) {
    var msec = tAry[i][1].valueOf() - tAry[i][0].valueOf (); 
    wdwDoc.writeln ('[' + msec + ']' + 'Elapsed ' + tAry[i][2]);
  }
  wdwDoc.writeln ('');
  tAry.length = 0;
  tAry = new Array ();
}

