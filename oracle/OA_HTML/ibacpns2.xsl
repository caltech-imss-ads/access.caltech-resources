<?xml version="1.0"?>
<!--$Header: ibacpns2.xsl 115.4 2000/11/01 11:47:27 pkm ship   $-->

<!--This stylesheet is for rendering Camapign by its short description, description-->
<!--and Micro Button image.The attachment types expected by this stylesheet are-->
<!--TXT_SHORT_DESC, TXT_DESC and IMG_MICRO_BUTTON-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/">
  <table border="0" cellpadding="1" cellspacing="2">
    <xsl:apply-templates select="//Campaign"/>
  </table>
</xsl:template>

<xsl:template match="Campaign">
  <tr>
    <td>
      <table border="0" cellpadding="2" cellspacing="0" align="center">
	   <tr></tr>
        <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'TXT_DESC')]">
          <tr>
            <td>
              <xsl:call-template name="SHORTDESC"/>
            </td>
          </tr>
        </xsl:for-each>
        <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'TXT_LONG_DESC')]">
          <tr>
            <td>
              <xsl:call-template name="DESC"/>
            </td>
          </tr>
        </xsl:for-each>
        <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'IMG_MICRO_BUTTON')]">
          <tr>
            <td>
              <xsl:call-template name="IMAGE"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </td>
  </tr>
</xsl:template>

<xsl:template name="IMAGE">
  <xsl:variable name="height">
    <xsl:value-of select="floor(child::DisplayHeight)"/>
  </xsl:variable>
  <xsl:variable name="width">
    <xsl:value-of select="floor(child::DisplayWidth)"/>
  </xsl:variable>
  <xsl:variable name="url">
    <xsl:value-of select="child::linkURL"/>
  </xsl:variable>
  <xsl:element name="a">
   <xsl:if test="boolean(normalize-space($url))">
     <xsl:attribute name="href">
       <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute>
   </xsl:if>
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:value-of select="child::FileLocation"/>
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="child::AlternateText"/>
      </xsl:attribute>
      <!--add height attribute if non-zero and number-->
      <xsl:if test="not($height='NaN')">
        <xsl:if test="not($height='0')">
          <xsl:attribute name="height">
           <xsl:value-of select="$height"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:if>
      <!--add width attribute if non-zero and number-->
      <xsl:if test="not($width='NaN')">
        <xsl:if test="not($width='0')">
          <xsl:attribute name="width">
           <xsl:value-of select="$width"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:if>
      <xsl:attribute name="border">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="SHORTDESC">
  <xsl:variable name="url">
    <xsl:value-of select="child::linkURL"/>
  </xsl:variable>
  <xsl:element name="a">
   <xsl:if test="boolean(normalize-space($url))">
     <xsl:attribute name="href">
       <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute>
   </xsl:if>
    <xsl:value-of select="child::DisplayText"/>
  </xsl:element>
</xsl:template>

<xsl:template name="DESC">
  <xsl:element name="a">
    <xsl:value-of select="child::DisplayText"/>
  </xsl:element>
</xsl:template>

<xsl:template match="*"></xsl:template>

</xsl:stylesheet>





