function _decimalFormat(
a0
)
{
return""+a0;
}
function _decimalParse(
a0
)
{
var a1=getLocaleSymbols();
if(a1)
{
var a2=new RegExp("\\"+a1.getGroupingSeparator(),"g");
a0=a0.replace(a2,"");
var a3=new RegExp("\\"+a1.getDecimalSeparator(),"g");
a0=a0.replace(a3,".");
}
var a4=a0.length-1;
while(a4>=0)
{
if(a0.charAt(a4)!=' ')
break;
a4--;
}
if(a4>=0)
{
if((a0.indexOf('e')<0)&&
(a0.indexOf('E')<0)&&
(((a0*a0)==0)||
((a0/a0)==1)))
{
var a5=parseFloat(a0);
if(!isNaN(a5))
return a5;
}
}
return(void 0);
}
function DecimalFormat()
{
this._class="DecimalFormat";
}
DecimalFormat.prototype=new Format();
DecimalFormat.prototype.format=_decimalFormat;
DecimalFormat.prototype.parse=_decimalParse;
