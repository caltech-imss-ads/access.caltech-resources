var _agent=new Object();
var _lastDateSubmitted;
function _atLeast(
a0,
a1
)
{
return(!a0||(a0==_agent.kind))&&
(!a1||(a1<=_agent.version));
}
function _atMost(
a0,
a1
)
{
return(a0==_agent.kind)&&(a1>=_agent.version);
}
function _agentInit()
{
var a0=navigator.userAgent.toLowerCase();
var a1=parseFloat(navigator.appVersion);
var a2=false;
var a3=false;
var a4=false;
var a5=false;
var a6="unknown";
var a7=false;
if(a0.indexOf("msie")!=-1)
{
a3=true;
var a8=a0.match(/msie (.*);/);
a1=parseFloat(a8[1]);
a6="ie";
}
else if(a0.indexOf("opera")!=-1)
{
a2=true
a6="opera";
}
else if((a0.indexOf('mozilla')!=-1)&&
(a0.indexOf('spoofer')==-1)&&
(a0.indexOf('compatible')==-1))
{
if(a1>=5.0)
{
a5=true;
a6="mozilla"
}
else
{
a4=true;
a6="nn";
}
}
if(a0.indexOf('win')!=-1)
a7=true;
_agent.isIE=a3;
_agent.isNav=a4;
_agent.isOpera=a2;
_agent.isMozilla=a5;
_agent.version=a1
_agent.kind=a6;
_agent.isWindows=a7;
_agent.atLeast=_atLeast;
_agent.atMost=_atMost;
}
_agentInit();
var _ieFeatures=
{
channelmode:1,
copyhistory:1,
directories:1,
fullscreen:1,
height:1,
location:1,
menubar:1,
resizable:1,
scrollbars:1,
status:1,
titlebar:1,
toolbar:1,
width:1
};
var _nnFeatures=
{
alwayslowered:1,
alwaysraised:1,
copyhistory:1,
dependent:1,
directories:1,
height:1,
hotkeys:1,
innerheight:1,
innerwidth:1,
location:1,
menubar:1,
outerwidth:1,
outerheight:1,
resizable:1,
scrollbars:1,
status:1,
titlebar:1,
toolbar:1,
width:1,
"z-lock":1
}
var _modelessFeatureOverrides=
{
};
var _modalFeatureOverrides=
{
};
var _featureDefaults=
{
document:
{
channelmode:false,
copyhistory:true,
dependent:false,
directories:true,
fullscreen:false,
hotkeys:false,
location:true,
menubar:true,
resizable:true,
scrollbars:true,
status:true,
toolbar:true
},
dialog:
{
channelmode:false,
copyhistory:false,
dependent:true,
directories:false,
fullscreen:false,
hotkeys:true,
location:false,
menubar:false,
resizable:true,
scrollbars:true,
status:true
}
}
var _signedFeatures=
{
alwayslowered:1,
alwaysraised:1,
titlebar:1,
"z-lock":1
};
var _booleanFeatures=
{
alwayslowered:1,
alwaysraised:1,
channelmode:1,
copyhistory:1,
dependent:1,
directories:1,
fullscreen:1,
hotkeys:1,
location:1,
menubar:1,
resizable:1,
scrollbars:1,
status:1,
titlebar:1,
toolbar:1,
"z-lock":1
};
function _getContentWidth(
a0,
a1,
a2
)
{
var a3=a0.childNodes;
var a4=_agent.isMozilla;
var a5=(a4)
?"tagName"
:"canHaveHTML"
var a6=0;
for(var a7=0;a7<a3.length;a7++)
{
var a8=a3[a7];
if(a8[a5]&&(a8.offsetWidth>0))
{
var a9=0;
var a10=a8["offsetWidth"];
if(a4)
{
if(a10==a1)
{
a9=_getContentWidth(a8,
a10,
a8.offsetLeft);
}
else
{
a9=a10;
}
}
else
{
a9=a8["clientWidth"];
if(a9==0)
{
a9=_getContentWidth(a8,
a10,
a8.offsetLeft);
}
}
if(a9>a6)
{
a6=a9;
}
}
}
if(a6==0)
a6=a1;
return a6+2*a2;
}
function _getTop(
a0
)
{
if(!_agent.isMozilla)
{
return top;
}
else
{
var a1=(a0)
?a0.window
:window;
while(a1.parent&&(a1.parent!=a1))
{
a1=a1.parent;
}
return a1;
}
}
function _sizeWin(
a0,
a1,
a2
)
{
var a3=_agent.isMozilla;
var a4=_agent.isIE;
if(!(a3||(a4&&_agent.isWindows)))
return;
var a5=a0.window.document.body;
if(a5)
{
var a6=(!a4&&(a5.scrollWidth>a5.clientWidth))
?a5.scrollWidth
:_getContentWidth(a5,a5.offsetWidth,a5.offsetLeft);
var a7=0;
if(a3)
{
a7=a5.offsetHeight+(window.outerHeight-window.innerHeight);
a7+=30;
a6+=(window.outerWidth-a5.offsetWidth);
}
else
{
a7=a5.scrollHeight+(a5.offsetHeight-a5.clientHeight);
a7+=21;
a6+=a5.offsetWidth-a5.clientWidth+16;
a7+=parseInt(a5.topMargin)+parseInt(a5.bottomMargin);
a6+=parseInt(a5.leftMargin)+parseInt(a5.rightMargin);
}
if(a1)
a6+=a1;
if(a2)
a7+=a2;
_getTop(a0).resizeTo(a6,a7);
}
}
function _onModalClickNN(
a0
)
{
if(_getValidModalDependent(self))
{
return false;
}
else
{
self.routeEvent(a0);
return true;
}
}
var _mozClickEH=new Object();
function _onModalClickMoz(
a0
)
{
dump(a0);
}
_mozClickEH["handleEvent"]=_onModalClickMoz;
function _onModalFocus()
{
var a0=self.document.body;
var a1=_getValidModalDependent(self);
var a2=_agent.atLeast("ie",5)&&_agent.isWindows;
if(a1)
{
if(!_agent.isMozilla)
a1.focus();
if(a2)
{
a0.setCapture();
}
}
else
{
if(a2)
{
a0.releaseCapture();
}
}
}
function openWindow(
a0,
a1,
a2,
a3,
a4,
a5,
a6
)
{
if(a0)
{
if(a4==(void 0))
a4=false;
if(!a5)
{
a5=(a4)?"dialog":"document";
}
if(!a2)
a2="_blank";
var a7=_featureDefaults[a5];
if(a7==(void 0))
{
a5="document";
a7=_featureDefaults[a5];
}
var a8=(a4)
?_modalFeatureOverrides
:_modelessFeatureOverrides;
var a9=(_agent.isIE)
?_ieFeatures
:_nnFeatures;
var a10="";
for(featureName in a9)
{
var a11=a8[featureName];
if(a11==(void 0))
{
if(a3)
a11=a3[featureName];
if(a11==(void 0))
a11=a7[featureName];
}
if(a11!=(void 0))
{
var a12=_booleanFeatures[featureName]!=(void 0);
if(a11||!a12)
{
a10+=featureName;
if(!a12)
{
a10+="="+a11;
}
a10+=",";
}
}
}
if(a10.length!=0)
{
a10=a10.substring(0,a10.length-1);
}
if(a6)
{
_setDependent(a0,a2,a6);
}
var a13=_agent.atMost("ie",4.99);
var a14=false;
var a15=a0.document.body;
if(a4&&!a13)
{
if(_agent.atLeast("ie",4))
{
a15.style.filter="alpha(opacity=50)";
a14=true;
}
if((_agent.atLeast("ie",5)&&_agent.isWindows))
{
a15.setCapture();
}
else if(_agent.isNav)
{
a0.captureEvents(Event.CLICK);
a0.onclick=_onModalClickNN;
}
else if(_agent.isMozilla)
{
a15.addEventListener(Event.CLICK,_mozClickEH,true);
}
a0.onfocus=_onModalFocus;
}
var a16=a0.open(a1,a2,a10);
if(a4&&!a13)
{
_setDependent(a0,"modalWindow",a16);
}
a16.focus();
if(a14)
{
a0.setTimeout("_clearAlphaFilter()",1000);
}
return a16;
}
else
{
return null;
}
}
function _getDependents(
a0,
a1
)
{
var a2;
if(a0)
{
a2=a0["_dependents"];
if(a2==(void 0))
{
if(a1)
{
a2=new Object();
a0["_dependents"]=a2;
}
}
}
return a2;
}
function _getDependent(
a0,
a1
)
{
var a2=_getDependents(a0);
var a3;
if(a2)
{
a3=a2[a1];
}
return a3;
}
function _setDependent(
a0,
a1,
a2
)
{
var a3=_getDependents(a0,true);
if(a3)
{
a3[a1]=a2;
}
}
function _getModalDependent(
a0
)
{
return _getDependent(a0,"modalWindow");
}
function _getValidModalDependent(
a0
)
{
var a1=_getModalDependent(a0);
if(a1)
{
if(a1.closed)
{
_setDependent(a0,"modalWindow",(void 0));
a1=(void 0);
}
}
return a1;
}
function _isModalDependent(
a0,
a1
)
{
return(a1==_getModalDependent(a0));
}
function _clearAlphaFilter()
{
if(_getValidModalDependent(self)!=null)
{
self.setTimeout("_clearAlphaFilter()",1000);
}
else
{
self.document.body.style.filter=null;
}
}
function _checkUnload(
a0
)
{
var a1=_getTop();
if(!a1)
return;
var a2=a1["opener"];
if(!a2)
return;
var a3=_getDependent(a2,self.name);
if(_isModalDependent(a2,self))
{
_setDependent(a2,"modalWindow",(void 0));
a2.onfocus=null;
var a4=a2.document.body;
if(_agent.atLeast("ie",4))
{
if(_agent.atLeast("ie",5)&&_agent.isWindows)
{
a4.releaseCapture();
}
a4.style.filter=null;
}
if(_agent.isNav)
{
a2.releaseEvents(Event.CLICK);
a2.onclick=null;
}
if(_agent.isMozilla)
{
a4.removeEventListener(Event.CLICK,
_mozClickEH,
true);
}
}
if(a3!=(void 0))
{
_setDependent(a2,self.name,(void 0));
if(a0==(void 0))
a0=self.event;
a3(a1,a0);
}
}
function _focusChanging()
{
if(_agent.isIE)
{
return(window.event.srcElement!=window.document.activeElement);
}
else
{
return true;
}
}
function _getKeyValueString(
a0,
a1,
a2
)
{
var a3=a0[a1];
if(typeof(a3)=="function")
{
a3="[function]";
}
var a4=(_agent.isMozilla)
?((a2+1)%3==0)
?'\n'
:'    '
:'\t';
return a1+':'+a3+a4;
}
function _dump(
a0
)
{
dump(a0,{innerText:1,outerText:1,outerHTML:1,innerHTML:1});
}
function dump(
a0,
a1,
a2
)
{
var a3="";
if(a0)
{
if(!a2)
{
a2=a0["name"];
}
var a4="return _getKeyValueString(target, key, index);";
if(_agent.atLeast("ie",5)||_agent.isMozilla)
a4="try{"+a4+"}catch(e){return '';}";
var a5=new Function("target","key","index",a4);
var a6=0;
var a7=new Array();
for(var a8 in a0)
{
if((!a1||!a1[a8])&&!a8.match(/DOM/))
{
a7[a6]=a8;
a6++;
}
}
a7.sort();
for(var a9=0;a9<a7.length;a9++)
{
a3+=a5(a0,a7[a9],a9);
}
}
else
{
a2="(Undefined)";
}
if(a3=="")
{
a3="No properties";
}
alert(a2+":\n"+a3);
}
function _validateForm(
a0
)
{
var a1='_'+a0.name+'Validater';
var a2=window[a1];
if(a2)
return a2(a0);
return false;
}
function _getNextNonCommentSibling(
a0,
a1
)
{
var a2=a0.children;
for(var a3=a1+1;a3<a2.length;a3++)
{
var a4=a2[a3];
if(a4&&(a4.tagName!="!"))
{
return a4;
}
}
return null;
}
function _valField(
formName,
nameInForm
)
{
if(nameInForm)
{
var target=document.forms[formName][nameInForm];
var blurFunc=target.onblur;
if(blurFunc)
{
var valFunc=blurFunc.toString();
var valContents=valFunc.substring(valFunc.indexOf("{")+1,
valFunc.lastIndexOf("}"));
var targetString="document.forms['"+
formName+
"']['"+
nameInForm+
"']";
valContents=valContents.replace(/this/,targetString);
var lastArg=valContents.lastIndexOf(",");
valContents=valContents.substring(0,lastArg)+")";
eval(valContents);
}
}
}
function _validateField(
a0,
a1,
a2,
a3,
a4
)
{
var a5=_agent.isNav;
if(a5&&a4)
{
return;
}
var a6=a3||(_getValue(a0)!=0);
if(a6&&!window._validating&&_focusChanging())
{
if(a4)
{
var a7=window.document.activeElement;
if(a7)
{
var a8=a0.parentElement;
if(a8==a7.parentElement)
{
var a9=a8.children;
for(var a10=0;a10<a9.length;a10++)
{
if(a0==a9[a10])
{
a6=(a7!=_getNextNonCommentSibling(a8,a10));
}
}
}
}
}
if(a6&&_hasValidationError(a0,a1))
{
window._validating=a0;
a0.select();
if(!a5)
{
a0.focus();
}
var a11=_getErrorString(a0,a2);
if(a11)
{
alert(a11);
}
if(a5)
{
a0.focus();
}
}
}
}
function _unvalidateField(
a0
)
{
if(window._validating==a0)
{
window._validating=void 0;
}
}
function submitForm(
a0,
a1,
a2
)
{
var a3=new Date();
if(_lastDateSubmitted)
{
var a4=a3-_lastDateSubmitted;
if((a4>=0)&&(a4<500))
return;
}
_lastDateSubmitted=a3;
if((typeof a0)=="string")
{
a0=document[a0];
}
if(!a0)
return false;
if(a1==(void 0))
a1=true;
var a5=true;
if(a1&&!_validateForm(a0))
a5=false;
var a6=window["_"+a0.name+"_Submit"];
if(a6!=(void 0))
{
var a7=new Function("doValidate",a6);
a0._tempFunc=a7;
var a8=a0._tempFunc(a1);
a0._tempFunc=(void 0);
if(a1&&(a8==false))
{
a5=false;
}
}
if(a5)
{
_resetHiddenValues(a0);
if(a2)
{
for(paramName in a2)
{
var a9=a2[paramName];
if(a9!=(void 0))
{
var a10=a0[paramName];
if(a10)
{
a10.value=a9;
}
}
}
}
a0.submit();
}
return a5;
}
function _resetHiddenValues(
a0
)
{
var a1=window["_reset"+a0.name+"Names"];
if(a1)
{
for(var a2=0;a2<a1.length;a2++)
{
var a3=a0[a1[a2]];
if(a3)
{
a3.value='';
}
}
}
}
function _getValue(a0)
{
var a1=a0.type
if(!a1&&a0.length)
{
a1=a0[0].type;
}
if(a1.substring(0,6)=="select")
{
var a2=a0.selectedIndex;
if(a2!=(void 0)&&
a2!=null&&
a2>=0)
{
return a0.options[a2].value;
}
return"";
}
else if(a1=="radio")
{
if(a0.length)
{
for(var a3=0;a3<a0.length;a3++)
{
if(a0[a3].checked)
{
return a0[a3].value;
}
}
}
else
{
if(a0.checked)
{
return a0.value;
}
}
return"";
}
else
{
return a0.value;
}
}
function _multiValidate(
a0,
a1
)
{
var a2="";
if(a1)
{
var a3=_getValidations(a0);
if(a3)
{
var a4=true;
for(var a5=0;a5<a1.length;a5+=4)
{
var a6=a0[a1[a5+1]];
var a7=a1[a5+3];
var a8=_getValue(a6);
if(!(a7&&(a8=="")))
{
if(_hasValidationError(a6,a1[a5],a3))
{
if(a4)
{
if(a6.focus)
a6.focus();
if(a6.type=="text")
a6.select();
a4=false;
}
var a9=_getErrorString(a6,
a1[a5+2]);
a2+='\n'+a9;
}
}
}
}
}
return a2;
}
function _getID(
a0
)
{
if(!_agent.isNav)
{
var a1=a0.id;
var a2=a0.type;
if(!a2&&a0.length)
a2=a0[0].type;
if(a2=="radio")
{
var a3;
if(a0.length)
{
a3=a0[0].parentNode;
}
else
{
a3=a0.parentNode;
}
a1=a3.id;
}
return a1;
}
else
{
var a4=_getForm(a0);
var a5=window["_"+a4.name+"_NameToID"];
if(a5)
{
var a6=_getName(a0);
return a5[a6];
}
}
}
function _getForm(
a0
)
{
var a1=a0.form;
if(a1==(void 0))
{
var a2=a0.type;
if(!a2&&a0.length)
a2=a0[0].type;
if(a2=="radio"&&a0.length)
{
a1=a0[0].form;
}
}
return a1;
}
function _getName(
a0
)
{
var a1=a0.name;
if(a1==(void 0))
{
var a2=a0.type;
if(!a2&&a0.length)
a2=a0[0].type;
if(a2=="radio"&&a0.length)
{
a1=a0[0].name;
}
}
return a1;
}
function _getErrorString(
a0,
a1
)
{
var a2=_getForm(a0);
var a3=_getValue(a0);
var a4=window["_"+a2.name+"_Formats"];
if(a4)
{
var a5=a4[a1];
if(a5)
{
var a6=window["_"+a2.name+"_Labels"];
var a7;
if(a6)
{
a7=a6[_getID(a0)];
}
var a8=_formatErrorString(a5,
{
"value":a3,
"label":a7
});
return a8;
}
}
}
function _getValidations(
a0
)
{
return window["_"+a0.name+"_Validations"];
}
function _hasValidationError(
input,
validationIndex,
validations
)
{
if(!validations)
{
validations=_getValidations(input.form);
}
if(validations)
{
var validator=validations[validationIndex];
if(validator)
{
var trueValidator=validator.replace(/%value%/g,"_getValue(input)");
return eval(trueValidator);
}
}
return false;
}
function _formatErrorString(
a0,
a1
)
{
var a2=a0;
for(currToken in a1)
{
var a3=a1[currToken];
if(!a3)
{
a3="";
}
var a4="%"+currToken+"%";
a2=a2.replace(a4,a3);
}
return a2;
}
function _chain(
a0,
a1,
a2,
a3,
a4
)
{
var a5=_callChained(a0,a2,a3);
if(a4&&(a5==false))
return false;
var a6=_callChained(a1,a2,a3);
return!((a5==false)||(a6==false));
}
function _callChained(
a0,
a1,
a2
)
{
if(a0&&(a0.length>0))
{
if(a2==(void 0))
{
a2=a1.window.event;
}
var a3=new Function("event",a0);
a1._tempFunc=a3;
var a4=a1._tempFunc(a2);
a1._tempFunc=(void 0);
return!(a4==false);
}
else
{
return true;
}
}
function _checkLength(a0,a1,a2)
{
elementLength=a0.value.length;
if(elementLength>a1)
{
a0.value=a0.value.substr(0,a1);
return false;
}
if(elementLength<a1)
return true;
if(a2&&(a2.which<32))
return true;
return false;
}
function _getElementById(
a0,
a1
)
{
if((_agent.kind!="ie")||(_agent.version>=5))
{
return a0.getElementById(a1);
}
else
{
return a0.all[a1];
}
}
function _addParameter(
a0,
a1,
a2
)
{
var a3=a0.indexOf('?');
if(a3==-1)
{
return a0+'?'+a1+'='+a2;
}
else
{
var a4=a0.indexOf(a1+'=',a3+1);
if(a4==-1)
{
return a0+'&'+a1+'='+a2;
}
else
{
var a5=a4+a1.length+1;
var a6=a0.substring(0,a5);
a6+=a2;
var a7=a0.indexOf('&',a5);
if(a7!=-1)
{
a6+=a0.substring(a7,a1.length);
}
return a6;
}
}
}
function _firePCUpdateMaster(
a0,
a1,
a2,
a3
)
{
var a4=a1+'_dt';
var a5=window[a4];
if(a5!=a0.id)
{
window[a4]=a0.id;
if(a5)
{
var a6=_getElementById(document,a5);
if(a6)
{
_updateDetailIcon(a6,'/marlin/cabo/images/cache/c-sdtl-1.gif');
}
}
_updateDetailIcon(a0,'/marlin/cabo/images/cache/c-dtl-1.gif');
_firePartialChange(a2,a3);
}
}
function _updateDetailIcon(
a0,
a1
)
{
a0.firstChild.src=a1;
}
function _firePartialChange(
a0,
a1
)
{
if(_agent.isIE||_agent.isMozilla)
{
var a2=_getElementById(document,'_'+a1);
var a3=a0;
if(_agent.isIE)
{
a2.src=a3;
}
else
{
a2.contentDocument.location=a3;
}
}
else
{
document.location=a0;
}
}
function _partialChange(
a0
)
{
var a1=document;
var a2=_getElementById(a1,a0);
var a3=_getElementById(parent.document,a0);
if(_agent.isIE)
{
a3.outerHTML=a2.outerHTML;
}
else
{
var a4=a3.parentNode;
var a5=parent.document;
var a6=a5.importNode(a2,true);
a4.replaceChild(a6,a3);
}
}
