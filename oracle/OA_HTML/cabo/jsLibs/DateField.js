function _returnCalendarValue(
a0,
a1
)
{
var a2=a0.returnValue;
if(a2!=(void 0))
{
var a3=new Date(a2);
var a4=a0._dateField;
if(a4==(void 0))
a4=_savedField1879034;
var a5=_getDateFieldFormat(a4);
a4.value=a5.format(a3);
a4.select();
a4.focus();
}
}
function _ldp(
a0,
a1,
a2,
a3,
a4
)
{
var a5=document.forms[a0][a1];
var a6;
if(a5.value!="")
{
a6=_getDateFieldFormat(a5).parse(a5.value);
if(!a6)
a6=new Date();
}
else
{
a6=new Date();
}
if(!a4)
{
a4=_jspDir+"a.jsp?_t=fred&_red=cd";
}
else
{
var a7=a4.lastIndexOf('?');
var a8="";
if(a7==-1)
{
a7=a4.length;
}
else
{
a8=a4.substr(a7);
}
var a9=a4.lastIndexOf('/',a7);
var a10=a4.substring(0,a9+1);
a10+=_jspDir+"a.jsp";
a10+=a8;
a10+=(a8.length==0)
?'?'
:'&';
a10+="_t=fred";
var a11=a4.substring(a9+1,a7);
a4=a10;
a4+="&redirect="+escape(a11);
}
a4+="&value="+a6.getTime();
if(_configName.length>0)
{
a4+="&configName="+escape(_configName);
}
a4+="&loc="+_locale;
if(window["_enc"])
{
a4+="&enc="+_enc;
}
if(window["_contextURI"])
{
a4+="&contextURI="+escape(_contextURI);
}
a4+="&tzOffset="+a6.getTimezoneOffset();
if(a2!=(void 0))
{
a4+="&minValue="+a2;
}
if(a3!=(void 0))
{
a4+="&maxValue="+a3;
}
var a12=openWindow(self,
a4,
'calendar',
{width:350,height:370},
true,
void 0,
_returnCalendarValue);
a12._dateField=a5;
_savedField1879034=a5;
}
function _getDateFieldFormat(a0)
{
var a1=a0.name;
if(a1&&_dfs)
{
var a2=_dfs[a1];
if(a2)
return new SimpleDateFormat(a2);
}
return new SimpleDateFormat();
}
var _savedField1879034;
