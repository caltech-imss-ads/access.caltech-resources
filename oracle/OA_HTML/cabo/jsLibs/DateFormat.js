var _AD_ERA=void 0;
function _getADEra()
{
if(_AD_ERA==(void 0))
{
_AD_ERA=new Date(0);
_AD_ERA.setFullYear(1);
}
return _AD_ERA;
}
function _simpleDateFormat(
a0
)
{
var a1=new Object();
a1.value="";
_doClumping(this._pattern,
this._localeSymbols,
_subformat,
a0,
a1);
return a1.value;
}
function _simpleDateParse(
a0
)
{
var a1=new Object();
a1.currIndex=0;
a1.parseString=a0;
a1.parsedHour=(void 0);
a1.parsedMinutes=(void 0);
a1.parsedSeconds=(void 0);
a1.parsedMilliseconds=(void 0);
a1.isPM=false;
a1.parsedBC=false;
a1.parsedFullYear=(void 0);
a1.parsedMonth=(void 0);
a1.parsedDate=(void 0);
a1.parseException=new ParseException();
var a2=new Date(0);
a2.setDate(1);
if(_doClumping(this._pattern,
this._localeSymbols,
_subparse,
a1,
a2))
{
var a3=a1.parsedFullYear;
if(a3!=(void 0))
{
if(a1.parsedBC)
{
a3=_getADEra().getFullYear()-a3;
}
a2.setFullYear(a3);
a1.parsedFullYear=a3;
}
var a4=a1.parsedMonth;
if(a4!=(void 0))
a2.setMonth(a4);
var a5=a1.parsedDate;
if(a5!=(void 0))
a2.setDate(a5);
var a6=a1.parsedHour;
if(a6!=(void 0))
{
if(a1.isPM&&(a6<12))
{
a6+=12;
}
a2.setHours(a6);
a1.parsedHour=a6;
}
var a7=a1.parsedMinutes;
if(a7!=(void 0))
a2.setMinutes(a7);
var a8=a1.parsedSeconds;
if(a8!=(void 0))
a2.setSeconds(a8);
var a9=a1.parsedMilliseconds;
if(a9!=(void 0))
a2.setMilliseconds(a9);
if(_dateFormatLenient==false&&
!_isStrict(a1,a2))
{
return(void 0);
}
return a2;
}
else
{
return(void 0);
}
}
function _isStrict(
a0,
a1)
{
var a2=["FullYear","Month","Date","Hours","Minutes",
"Seconds","Milliseconds"];
for(var a3=0;a3<a2.length;a3++)
{
var a4="parsed"+a2[a3];
if(a0[a4]!=(void 0)&&
a0[a4]!=a1["get"+a2[a3]]())
{
return false;
}
}
return true;
}
function _doClumping(
a0,
a1,
a2,
a3,
a4
)
{
var a5=a0.length;
var a6=false;
var a7=0;
var a8=void 0;
var a9=0;
for(var a10=0;a10<a5;a10++)
{
var a11=a0.charAt(a10);
if(a6)
{
if(a11=="\'")
{
a6=false;
if(a7!=1)
{
a9++;
a7--;
}
if(!a2(a0,
a1,
"\'",
a9,
a7,
a3,
a4))
{
return false;
}
a7=0;
a8=void 0;
}
else
{
a7++;
}
}
else
{
if(a11!=a8)
{
if(a7!=0)
{
if(!a2(a0,
a1,
a8,
a9,
a7,
a3,
a4))
{
return false;
}
a7=0;
a8=void 0;
}
if(a11=='\'')
{
a6=true;
}
a9=a10;
a8=a11;
}
a7++;
}
}
if(a7!=0)
{
if(!a2(a0,
a1,
a8,
a9,
a7,
a3,
a4))
{
return false;
}
}
return true;
}
function _subformat(
a0,
a1,
a2,
a3,
a4,
a5,
a6
)
{
var a7=null;
var a8=0;
var a9=false;
if((a2>='A')&&(a2<='Z')||
(a2>='a')&&(a2<='z'))
{
switch(a2)
{
case'D':
a7="(Day in Year)";
break;
case'E':
{
var a10=a5.getDay();
a7=(a4<=3)
?a1.getShortWeekdays()[a10]
:a1.getWeekdays()[a10];
}
break;
case'F':
a7="(Day of week in month)";
break;
case'G':
{
var a11=a1.getEras();
a7=(a5.getTime()<_getADEra().getTime())
?a11[0]
:a11[1];
}
break;
case'M':
{
var a12=a5.getMonth();
if(a4<=2)
{
a7=_getPaddedNumber(a12+1,a4);
}
else if(a4==3)
{
a7=a1.getShortMonths()[a12];
}
else
{
a7=a1.getMonths()[a12];
}
}
break;
case'S':
a7=_getPaddedNumber(a5.getMilliseconds(),a4);
break;
case'W':
a7="(Week in Month)";
break;
case'a':
{
var a13=a1.getAmPmStrings();
a7=(_isPM(a5.getHours()))
?a13[1]
:a13[0];
}
break;
case'd':
a7=_getPaddedNumber(a5.getDate(),a4);
break;
case'h':
a8=1;
case'K':
{
hours=a5.getHours();
if(_isPM(hours))
{
hours-=12;
}
a7=_getPaddedNumber(hours+a8,a4);
}
break;
case'k':
a8=1;
case'H':
a7=_getPaddedNumber(a5.getHours()+a8,a4);
break;
case'm':
a7=_getPaddedNumber(a5.getMinutes(),a4);
break;
case's':
a7=_getPaddedNumber(a5.getSeconds(),a4);
break;
case'w':
a7="(Week in year)";
break;
case'y':
{
var a14=a5.getFullYear();
var a15=(a4<=2)
?a4
:(void 0);
a7=_getPaddedNumber(a14,a4,a15);
}
break;
case'z':
{
var a16=a5.getTimezoneOffset()/60;
a7="GMT";
a7+=((a16<0)?"-":"+");
a7+=_getPaddedNumber(a16,2);
}
break;
default:
a7="";
}
}
else
{
a7=a0.substring(a3,a3+a4);
}
a6.value+=a7;
return true;
}
function _subparse(
a0,
a1,
a2,
a3,
a4,
a5,
a6
)
{
var a7=0;
var a8=a5.currIndex;
if((a2>='A')&&(a2<='Z')||
(a2>='a')&&(a2<='z'))
{
switch(a2)
{
case'D':
if(_accumulateNumber(a5)==(void 0))
{
return false;
}
break;
case'E':
{
var a9=_matchArray(a5,
(a4<=3)
?a1.getShortWeekdays()
:a1.getWeekdays());
if(a9==(void 0))
{
return false;
}
}
break;
case'F':
if(_accumulateNumber(a5)==(void 0))
{
return false;
}
break;
case'G':
{
var a10=_matchArray(a5,a1.getEras());
if(a10!=(void 0))
{
if(a10==0)
{
a5.isBC=true;
}
}
else
{
return false;
}
}
break;
case'M':
{
var a11;
var a12=0;
if(a4<=2)
{
a11=_accumulateNumber(a5);
a12=-1;
}
else
{
var a13=(a4==3)
?a1.getShortMonths()
:a1.getMonths();
a11=_matchArray(a5,a13);
}
if(a11!=(void 0))
{
a5.parsedMonth=(a11+a12);
}
else
{
return false;
}
}
break;
case'S':
{
var a14=_accumulateNumber(a5);
if(a14!=(void 0))
{
a5.parsedMilliseconds=a14;
}
else
{
return false;
}
}
break;
case'W':
if(_accumulateNumber(a5)==(void 0))
{
return false;
}
break;
case'a':
{
var a15=_matchArray(a5,
a1.getAmPmStrings());
if(a15==(void 0))
{
return false;
}
else
{
if(a15==1)
{
a5.isPM=true;
}
}
}
break;
case'd':
{
var a16=_accumulateNumber(a5);
if(a16!=(void 0))
{
a5.parsedDate=a16;
}
else
{
return false;
}
}
break;
case'h':
case'k':
a7=1;
case'H':
case'K':
{
var a17=_accumulateNumber(a5);
if(a17!=(void 0))
{
a5.parsedHour=a17-a7;
}
else
{
return false;
}
}
break;
case'm':
{
var a18=_accumulateNumber(a5);
if(a18!=(void 0))
{
a5.parsedMinutes=a18;
}
else
{
return false;
}
}
break;
case's':
{
var a19=_accumulateNumber(a5);
if(a19!=(void 0))
{
a5.parsedSeconds=a19;
}
else
{
return false;
}
}
break;
case'w':
if(_accumulateNumber(a5)==(void 0))
{
return false;
}
break;
case'y':
{
var a20=_accumulateNumber(a5);
if(a20!=(void 0))
{
if((a5.currIndex-a8>2)&&
(a4<=2)&&
(a20<=100))
{
return false;
}
else if((a4<=2)&&(a20>=0)&&(a20<=100))
{
var a21;
if(_dateFormatTwoDigitYearStart!=(void 0))
{
var a22=_dateFormatTwoDigitYearStart;
a21=a22-(a22%100);
a20+=a21;
if(a20<a22)
a20+=100;
}
else
{
var a23=new Date().getFullYear();
a21=a23-(a23%100)-100;
a20+=a21;
if(a20+80<a23)
{
a20+=100;
}
}
}
if(a20==0)
return false;
a5.parsedFullYear=a20;
}
else
{
return false;
}
}
break;
case'z':
{
if(!_matchText(a5,"GMT"))
{
return false;
}
if(_matchArray(a5,["-","+"])==(void 0))
{
return false;
}
if(_accumulateNumber(a5)==(void 0))
{
return false;
}
}
break;
default:
}
}
else
{
return _matchText(a5,
a0.substring(a3,a3+a4));
}
return true;
}
function _matchArray(
a0,
a1
)
{
for(var a2=0;a2<a1.length;a2++)
{
if(_matchText(a0,a1[a2]))
{
return a2;
}
}
return(void 0);
}
function _matchText(
a0,
a1
)
{
if(!a1)
return false;
var a2=a1.length;
var a3=a0.currIndex;
var a4=a0.parseString;
if(a2>a4.length-a3)
{
return false;
}
var a5=a4.substring(a3,a3+a2);
var a6=a5.toLowerCase();
var a7=a1.toLowerCase();
if(a6!=a7)
return false;
a0.currIndex+=a2;
return true;
}
function _accumulateNumber(
a0
)
{
var a1=a0.currIndex;
var a2=a1;
var a3=a0.parseString;
var a4=a3.length;
var a5=0;
while(a2<a4)
{
var a6=parseDigit(a3.charAt(a2));
if(!isNaN(a6))
{
a5*=10;
a5+=a6;
a2++;
}
else
{
break;
}
}
if(a1!=a2)
{
a0.currIndex=a2;
return a5;
}
else
{
return(void 0);
}
}
function _isPM(
a0
)
{
return(a0>=12);
}
function _getPaddedNumber(
a0,
a1,
a2
)
{
var a3=a0.toString();
if(a1!=(void 0))
{
var a4=a1-a3.length;
while(a4>0)
{
a3="0"+a3;
a4--;
}
}
if(a2!=(void 0))
{
var a5=a3.length-a2;
if(a5>0)
{
a3=a3.substring(a5,
a5+a2);
}
}
return a3;
}
function SimpleDateFormat(
a0,
a1
)
{
this._class="SimpleDateFormat";
this._localeSymbols=getLocaleSymbols(a1);
this._pattern=(a0==(void 0))
?this._localeSymbols.getShortDatePatternString()
:a0;
}
SimpleDateFormat.prototype=new Format();
SimpleDateFormat.prototype.format=_simpleDateFormat;
SimpleDateFormat.prototype.parse=_simpleDateParse;
