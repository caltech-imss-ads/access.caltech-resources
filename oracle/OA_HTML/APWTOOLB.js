//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWTOOLB.js                       |
//  |                                                |
//  | Description: Client-side Javascript functions  |
//  |              for generating Toolbar	     |     
//  |                                                |
//  | History:     Created 03/20/2000                |
//  |              David Tong                        |
//  |                                                |
//  +================================================+
/* $Header: APWTOOLB.js 115.14 2000/09/03 13:49:19 pkm ship     $ */

function fDrawToolbar(bDisableSave, bDisableReload, bDisableStop, bConfirmGotoMM, strPrintFrame, strTitle){
	var tRegion = top.g_objRegions.mobjGetRegion('AP_WEB_TOOLBAR');
	var d = eval(top.framToolbar.document);
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
	d.writeln("<HEAD>");
	d.writeln("<LINK REL=stylesheet TYPE=text/css href=\""+top.g_strCssDir+"apwscabo.css\">");
	d.writeln("<script language=javascript> ");
	d.writeln("  bName = navigator.appName;  ");
        d.writeln("  bVer = parseInt(navigator.appVersion); ");
        d.writeln("if (bVer >= 3) ");
        d.writeln("   {  var save1 = new Image; ");
        d.writeln("      save1.src = top.g_strImgDir+\"APWIWSA1.gif\"; ");
        d.writeln("      var print1 = new Image; ");
        d.writeln("      print1.src = top.g_strImgDir+\"APWIWPR1.gif\"; ");
        d.writeln("      var refresh1 = new Image; ");
        d.writeln("      refresh1.src = top.g_strImgDir+\"APWIWRL1.gif\"; ");
        d.writeln("      var help1 = new Image; ");
        d.writeln("      help1.src = top.g_strImgDir+\"APWIWHL1.gif\"; ");
        d.writeln("      var home1 = new Image; ");
        d.writeln("      home1.src = top.g_strImgDir+\"APWIWHO1.gif\"; ");
        d.writeln("      var down1 = new Image; ");
        d.writeln("      down1.src = top.g_strImgDir+\"APWIHLP1.gif\"; ");
	d.writeln("      var stop1 = new Image; ");
        d.writeln("      stop1.src = top.g_strImgDir+\"APWIWST1.gif\"; ");
        d.writeln("           } ");
  	d.writeln("var Restorable = false");
  	d.writeln("var Nav4 = ((navigator.appName == \"Netscape\") && (parseInt(navigator.appVersion) == 4))");
  	d.writeln("var Win32");
  	d.writeln("if (Nav4) {");
        d.writeln("Win32 = ((navigator.userAgent.indexOf(\"Win\") != -1) && (navigator.userAgent.indexOf(\"Win16\") == -1))");
  	d.writeln("} ");
  	d.writeln("else ");
  	d.writeln("{");
       	d.writeln(" Win32 = ((navigator.userAgent.indexOf(\"Windows\") != -1) && (navigator.userAgent.indexOf(\"Windows 3.1\") == -1))");
  	d.writeln("}");

  	d.writeln("function fprintFrame(wind) {");
        d.writeln("if (Win32) {");
        d.writeln("        if (Nav4) {");
        d.writeln("                wind.print()");
        d.writeln("        } else {");
	d.writeln("		 wind.onerror = fprintErr");
 	d.writeln("	         wind.focus()");
   	d.writeln("		IEControl.ExecWB(6, 1)");
        d.writeln("        }");
        d.writeln("} ");
	d.writeln("else {");
        d.writeln("        alert(\"Sorry. Printing is available only from Windows 95/98/NT.\")");
        d.writeln("}");
     	d.writeln("}");
	d.writeln("function fprintErr() {");
	d.writeln("return true");
     	d.writeln("}");
	d.writeln("<\/script> ");
	d.writeln("</HEAD>");
	d.writeln("<BODY class=appswindow>");
	d.writeln("<TABLE width=100% Cellpadding=0 Cellspacing=0 border=0>");
	d.writeln("<TR>");
	d.writeln("<TR></TR>   <TD width=10></TD>   <TD>");
	d.writeln("    <TABLE Cellpadding=0 Cellspacing=0 Border=0>");
	d.writeln("          <TD rowspan=3><IMG src="+top.g_strImgDir+"APWGTBL.gif></TD>");
	d.writeln("          <TD class=color6 height=1 colspan=3><IMG src="+top.g_strImgDir+"APWPX6.gif></TD>");
	d.writeln("          <TD rowspan=3><IMG src="+top.g_strImgDir+"APWGTBR.gif></TD>");
	d.writeln("  </TR>");
	d.writeln("  <TR>");
	d.writeln("          <TD class=colorg5 nowrap height=30 align=middle>");
	if ((bConfirmGotoMM) || (top.opener == null))
	d.writeln("          <A href=\"javascript:top.fGotoMainMenu();\"");
	else
	d.writeln("          <A href=\"javascript:parent.window.close();  parent.window.opener.focus();\" ");
	d.writeln("onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_GOTO_MM').mstrGetPrompt()+"&quot;; document.tbhome.src=home1.src; return true\"");
	d.writeln("onmouseout=\"window.status=''; document.tbhome.src='"+top.g_strImgDir+"APWIWHOM.gif'; return true\">");
	d.writeln("          <IMG name=tbhome src='"+top.g_strImgDir+"APWIWHOM.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_GOTO_MM').mstrGetPrompt()+"\" align=middle border=0></A>");
	d.writeln("          <IMG src='"+top.g_strImgDir+"APWIWDVD.gif' align=absmiddle>");
	d.writeln("          </TD>");
	d.writeln("          <TD class=toolbar nowrap height=30 align=middle>&nbsp;");
	d.writeln(tRegion.mobjGetRegionItem('AP_WEB_APP_NAME').mstrGetPrompt());
	d.writeln("          &nbsp;</TD>");
	d.writeln("          <TD class=colorg5 nowrap height=30 align=middle>");
	d.writeln("          <IMG src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	if (bDisableSave){
	d.writeln("          <IMG name=tbsave src='"+top.g_strImgDir+"APWIWSAD.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_SAVE').mstrGetPrompt()+"\" align=absmiddle border=0>");
	}
	else{
	d.writeln("          <A href=\"javascript:top.fSaveOrSubmit('save');\" ");
	d.writeln("onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_SAVE').mstrGetPrompt()+"&quot;; document.tbsave.src=save1.src; return true\"");
	d.writeln(" onmouseout=\"window.status=''; document.tbsave.src='"+top.g_strImgDir+"APWIWSAV.gif'; return true\">");
	d.writeln("          <IMG name=tbsave src='"+top.g_strImgDir+"APWIWSAV.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_SAVE').mstrGetPrompt()+"\" align=absmiddle border=0></A>");
	}
	d.writeln("          <A href=\"javascript:void fprintFrame(eval("+strPrintFrame+"));\" ");
	d.writeln(" onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt()+"&quot;; document.tbprint.src=print1.src; return true\"");
	d.writeln(" onmouseout=\"window.status=''; document.tbprint.src='"+top.g_strImgDir+"APWIWPRT.gif'; return true\">");
	d.writeln("          <IMG name=tbprint src='"+top.g_strImgDir+"APWIWPRT.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt()+"\" align=absmiddle border=0></A>");

	d.writeln("          <IMG src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	if (bDisableReload){
	d.writeln("          <IMG name=tbrefresh src='"+top.g_strImgDir+"APWIWRDD.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_RELOAD').mstrGetPrompt()+"\" align=absmiddle border=0>");
	}
	else{
	d.writeln("          <A href=\"javascript:top.location.reload();\" ");
	d.writeln(" onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_RELOAD').mstrGetPrompt()+"&quot;; document.tbrefresh.src=refresh1.src; return true\"");
	d.writeln(" onmouseout=\"window.status=''; document.tbrefresh.src='"+top.g_strImgDir+"APWIWRLD.gif'; return true\">");
	d.writeln("          <IMG name=tbrefresh src='"+top.g_strImgDir+"APWIWRLD.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_RELOAD').mstrGetPrompt()+"\" align=absmiddle border=0></A>");
	}
	if (bDisableStop){
	d.writeln("          <IMG name=tbstop src='"+top.g_strImgDir+"APWIWSTD.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_STOP').mstrGetPrompt()+"\" align=absmiddle border=0></A>");
	}
	else{
	d.writeln("          <A href=\"javascript:top.fStop();\" ");
	d.writeln(" onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_STOP').mstrGetPrompt()+"&quot;; document.tbstop.src=stop1.src; return true\"");
	d.writeln(" onmouseout=\"window.status=''; document.tbstop.src='"+top.g_strImgDir+"APWIWSTP.gif'; return true\">");
	d.writeln("          <IMG name=tbstop src='"+top.g_strImgDir+"APWIWSTP.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_STOP').mstrGetPrompt()+"\" align=absmiddle border=0></A>");
	}
	d.writeln("          <IMG src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");

	d.writeln("          <A href=\"javascript:top.help_window();\" ");
	d.writeln(" onmouseover=\"window.status=&quot;"+tRegion.mobjGetRegionItem('AP_WEB_HELP').mstrGetPrompt()+"&quot;; document.tbhelp.src=help1.src; return true\"");
	d.writeln(" onmouseout=\"window.status=''; document.tbhelp.src='"+top.g_strImgDir+"APWIWHLP.gif'; return true\">");
	d.writeln("          <IMG name=tbhelp src='"+top.g_strImgDir+"APWIWHLP.gif' alt = \""+tRegion.mobjGetRegionItem('AP_WEB_HELP').mstrGetPrompt()+"\" align=absmiddle border=0></A>");
	d.writeln("  </TR>");
	d.writeln("  <TR>");
	d.writeln("          <TD class=color3 height=1 colspan=3><IMG src="+top.g_strImgDir+"APWPX3.gif></TD>");
	d.writeln("  </TR>");
	d.writeln("</TABLE>");
	d.writeln("</TD>");
	d.writeln("<TD rowspan=5 width=100% align=right valign=top><IMG src="+top.g_strImgDir+"APWLWAPP.gif></TD>");
	d.writeln("</TR>");
	d.writeln("<TR><TD height=5></TD></TR> </TABLE>");

	if (strTitle){
	d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
	d.writeln("<TR bgcolor=#336699>");
	d.writeln("<TD><IMG src="+top.g_strImgDir+"APWPX3.gif></TD>");
	d.writeln("<TD nowrap><FONT class=containertitle>"+strTitle+"</FONT></TD>");
	d.writeln("</TR>");
	d.writeln("<TR>");
	d.writeln("<TD rowspan=2 bgcolor=#cccccc valign=top ><IMG src="+top.g_strImgDir+"APWCTTL.gif></TD>");
	d.writeln("<TD height=100 bgcolor=#cccccc><IMG src="+top.g_strImgDir+"APWDBPXW.gif></TD>");
	d.writeln("<TD rowspan=2 bgcolor=#cccccc valign=top align=right width=1><IMG src="+top.g_strImgDir+"APWCTTR.gif></TD>");
	d.writeln("</TR>");
	d.writeln("<TR bgcolor=#cccccc>");
	d.writeln("</TR>");
  	d.writeln("</TABLE>");
	}


	d.writeln("<OBJECT ID=\"IEControl\" WIDTH=0 HEIGHT=0 CLASSID=\"clsid:8856F961-340A-11D0-A96B-00C04FD705A2\">");
	d.writeln("</OBJECT>");
	d.writeln("</BODY>");
	d.writeln("</HTML>");
	d.close();

}

function fGotoMainMenu(){
	if (g_dcdName.charAt(g_dcdName.length-1) == '/') l_dcdName = g_dcdName.substring(0,g_dcdName.length-1);
	else l_dcdName = g_dcdName;
	top.g_closeWin = true;
	if ((!top.opener) || (top.opener.loaction == window.location)){ // for 11.0.3
	    if (top.g_strFunc == "newexp"){
	      if ((fGetObjExpReport().getTotalReceipts('OOP')==0) && (fGetObjExpReport().getTotalReceipts('CC')==0)){
		location = l_dcdName+"/OracleApps.DMM";
	      }
	      else
	    	if (fIsDataChanged()){
	      	  if (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_GOTOMM_CONFIRM"))){ 
	     		fSaveOrSubmit('save');
	      	  }
	      	  else { // donot want to save changed
 			location = l_dcdName+"/OracleApps.DMM";
	      	  }
            	}
	    	else{ // no chage has been made
			location = l_dcdName+"/OracleApps.DMM";
	    	}
	    }
	    else location = l_dcdName+"/OracleApps.DMM"; // for update, modify screens
	}
	else{ // for 11i
	  if (top.g_strFunc == "newexp"){
	    if ((fGetObjExpReport().getTotalReceipts('OOP')==0) && (fGetObjExpReport().getTotalReceipts('CC')==0)){
	  	top.windowunload();
	  	parent.window.close();  
	    } 
	    else
	      if (fIsDataChanged()){
	      	if (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_GOTOMM_CONFIRM"))){ 
	     		fSaveOrSubmit('save');
	        }
	        else {
	     		parent.window.close();  
	     		parent.window.opener.focus(); 
	      	}
              }
	      else{
	     	parent.window.close();  
	     	parent.window.opener.focus(); 
	      }
          }// no exp report exist
	  else{
		parent.window.close();  
	     	parent.window.opener.focus();
	  }
       }
}


function windowunload(){
	if (top && top.opener) {
	        if (top.opener.top.main){
                top.opener.top.main.document.functionwindowfocus.X.value = "FALSE";
                }
		setTimeout("",2000);
	} //end if
} //end function windowunload
// window.onfocus = self.focus;
window.onunload = new Function("windowunload()");

