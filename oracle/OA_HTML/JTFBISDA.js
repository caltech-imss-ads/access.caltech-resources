/* $Header: JTFBISDA.js 115.12 2001/02/20 16:20:41 pkm ship      $ */
/**********************************************
* JTFBISDA.js                                 *  
*                                             *
* History                                     *
* Added Date Validation and NLS compliance    *
*    - 11/22/2000 by sspurani                 *
* Added current date validation               *
*    - 12/06/2000 by sspurani                 *
* Added fix for case-insensitive date format  *
* Bug no 1518622 - 12/15/2000 by sspurani     *
* Added fix for the case when only one of the *
* date parameter exist ie.start date or end   *
* date                                        *
* Bug no 1518622 - 01/19/01 by sspurani       *
* Added code for date validation when         *
* date_format is 'MON-YYYY'                   *
* Bug no 1385557 - 01/23/01 by sspurani       *
**********************************************/

var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))

//Values here are all defaults, and can be overridden
//var l_mon 	= new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
//var l_mon 	= new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
//var l_mon 	= MON;
var l_days	= new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var l_len	= new Array(2, 3, 4);
var l_yearbreak = 75;
var m_pos = 1;
var d_pos = 0;
var y_pos = 2;
var delim = "-";
var m_type = "N";
var NLSformat = "DD-MON-RRRR";
var numericDate = 0;
var currdate=Dsysdate;
var today = new Date();
var day   = today.getDate();
var month = today.getMonth();
var year  = today.getFullYear();

var closeOnPick=true;
Calwindow = new Object;
Calwindow.closed=true;
var calwidth=250;
var calheight=245;
var calfile=OA_HTML+"JTFBISCL.htm";
//var dateErrorMsg="Sorry, the date you entered was not recognized.";
var invaliddatesErrMsg=MESSAGE_TEXT1;
var dateErrorMsg=MESSAGE_TEXT2;
var invalidEndDateErrMsg=MESSAGE_TEXT3;
var invalidStartDateErrMsg=MESSAGE_TEXT4;
var validdatesMsg = "Start and End dates are validated successfully.";
//var calWindowTitle = "Select a Date";
var calWindowTitle=CALWINDOW_TEXT;

function datecheck (P_value, P_NLS_format) {  

//exit if no date passed in 
	if (P_value == null|P_value == ''| P_value == ' ') return ' ';

	evalNLS(P_NLS_format);

	var l_length=P_value.length;
	var l_str = new Array(' ',' ',' ');
	var temp = new Array('','','');
	var l_swap='';
	var l_splitstr='';
	var l_index=0;
	var l_splitcount=1;
	var l_char='';
	var l_prevspace=true;
	var y_extra=0;
	var d_extra=0;
	var m_extra=0; 
	var l_delims = /\W/


//Splits and arranges an all numeric input string into numeric values ordered D M Y
//Assumes 2 characters for month and day, and assigns what is left over to year
	if (parseFloat(P_value) == P_value) {
		if (d_pos >= 0)
		{
		  if (P_value.length <5)
		  {
			P_value = P_value + "????????".substr(0,8-P_value.length);
		  }
		  else {
			if (P_value.length <= 6) {
			y_extra=0;
			d_extra=0;
			m_extra=0; 
			} 
			else {
				y_extra = P_value.length - 6 ;
				if (d_pos - y_pos > 0) d_extra = y_extra;
				if (m_pos - y_pos > 0) m_extra = y_extra;
			}
		}
		}
		else {
		if (d_pos == -1)
		{
		 if (P_value.length <3) 
		 {
		    P_value = P_value + "??????".substr(0,6-P_value.length);
		}
		else {
			if (P_value.length <= 4) {
			y_extra=0;
			d_extra=0;
			m_extra=0; 
			} 
			else {
				y_extra = P_value.length - 4 ;
				if (m_pos - y_pos > 0) m_extra = y_extra;
			}
		  }
		 }
		}

		if (d_pos == -1)
		{
		  l_str[0]='01';
		}
		else
		{
		  l_str[0]=P_value.substr(d_pos*2+d_extra,2);
		}
		l_str[1]=P_value.substr(m_pos*2+m_extra,2);
		l_str[2]=P_value.substr(y_pos*2,2+y_extra);
		if (l_str[1] > 12)
		{	l_swap = l_str[0];
			l_str[0]=l_str[1];
			l_str[1]=l_swap; 
		}	
	}
	else {

//Otherwise, splits the string using delimiters after compressing spaces 
//and common delimiter characters
		for (var count=0; count < l_length; ++count ) {
			l_char=P_value.charAt(count);
			if (l_char.search(l_delims)==0) {
			 	if (l_prevspace==false) { l_splitstr += l_char; }
			  	l_prevspace = true;
			}
			else	{ l_prevspace=false;
				  l_splitstr += l_char;
			}
		}
		l_length = l_splitstr.length;


//splits string by delimiter into 3 parts
		for( var count=0; count < l_length; ++count ) {
			l_char=l_splitstr.charAt(count);
			if (l_char.search(l_delims)==0) {
				l_index = l_index + 1; 
			}
			else { temp[l_index] += l_char;
		}	}
		if (d_pos == -1)
		{
		  l_str[0]='01';
		}
		else
		{
		  l_str[0]=temp[d_pos];
		}
		l_str[1]=temp[m_pos];
		l_str[2]=temp[y_pos];

//The array is now in the order of the NLS mask, but some sanity
//checking of the contents should still be done
//If the string already in the year slot is > 31, leave it - otherwise
//if any string is > 31 and numeric (probably the year), swap it into the year slot.

		if (!l_str[2] > 31) 
		{
			if (d_pos >=0)
			{
			
			 if (l_str[0] > 31 && !isNaN(l_str[0])) {
				l_swap = l_str[2];
				l_str[2] = l_str[0];
				l_str[0] = l_swap;
			 }
            }
			if (l_str[1] > 31 && !isNaN(l_str[1])) {
				l_swap = l_str[2];
				l_str[2] = l_str[1];
				l_str[1] = l_swap;
			}
		}	
		
//strips off non-numeric characters from elements
		var strip = ""
		if (d_pos >=0)
	    {
		 for (i=0; i<l_str[0].length; i++)
		 { if (!isNaN(l_str[0].charAt(i))) strip += l_str[0].charAt(i); }
		  //l_str[0] = (temp.length > 0)?temp + '':l_str[0] + '';
		  if (strip.length > 0) l_str[0] = parseFloat(l_str[0],10) + ''

		 strip = ""
        }
        
		for (i=0; i<l_str[1].length; i++)
		{ if (!isNaN(l_str[1].charAt(i))) strip += l_str[1].charAt(i); }
		if (strip.length > 0) l_str[1] = parseFloat(l_str[1],10) + ''
	
//swaps the first two strings if the first one is not numeric
//so the month string is placed into position 1 in the array, or
//swaps any value > 12 in the months slot into the day slot
		if (d_pos >=0)
	    {
		 if (isNaN(l_str[0])) { 
			l_swap=l_str[0];
			l_str[0]=l_str[1];
			l_str[1]=l_swap; 
		} 
		else { 
			if (l_str[1] > 12) {
				l_swap = l_str[0];
				l_str[0]=l_str[1];
				l_str[1]=l_swap; 
		}	}
		
       }
//If the month isn't numeric, checks the table of valid
//months to determine the numeric equivalent for the string.  Uses this to
//check whether the day is valid for the particular month later.
		if (isNaN(l_str[1])) {
			for (var count=0; count <= 11; ++count) {
				l_str[1] = l_str[1].substr(0,3);
				if (l_str[1].toUpperCase() == l_mon[count].toUpperCase() ) {
					l_str[1]=(count+1)+'';
				  	break; }
			}
			if (count == 12) { 
				l_str[1] = "??";
			}
			else { 
				if (l_str[1].length > l_len[1]) {
					l_str[1] = l_str[1].substr(0,l_len[1]);
				}
				else {
					while (l_str[1].length < l_len[1]) {
						l_str[1] = '0'+l_str[1];
			}	}	}
		} 
		else {
		if (l_str[1] > 12 || l_str[1] < 1) {
			l_str[1] = "??" }
		}

//zero fills the day if less than defined length and truncates if more
		if (l_str[0].length > l_len[0]) {
			l_str[0] = l_str[0].substr(0,l_len[0]);
		}
		else {
			while (l_str[0].length < l_len[0]) {
				l_str[0] = '0'+l_str[0]; 
		}	}	

//end of the "else" statement-->
}
//If the year isn't numeric, fill with ? and treat as unrecognizable, otherwise, convert to integer before
//filling to correct length.

if (isNaN(l_str[2])) l_str[2] = "????";
else l_str[2] = parseFloat(l_str[2]) + '';

//If the year comes in as 3 digits, just pads it with a "1", otherwise fills
//it to at least 2 digits.
    if (l_str[2].length > l_len[2]) 
    {
      l_str[2] = l_str[2].substr(0,l_len[2]);
    }
    else 
    {
      if (l_str[2].length == 3) 
	  {
        l_str[2] = '1' + l_str[2];
	  }
      else 
      {
        while (l_str[2].length < 2) 
        {
          l_str[2] = '0'+l_str[2]; 
        }	
      }	
    }

//adds century to a 2 digit year using an arbitrary split point
	if ((l_str[2].length <=2 || parseFloat(l_str[2])< 100 ) && l_str[2].indexOf("?") == -1 ) {
		l_str[2] = parseFloat(l_str[2])+'';
		while (l_str[2].length < 2) {
			l_str[2] = '0'+l_str[2];
		}
		if (l_str[2] < l_yearbreak) {
			l_str[2]='20'+l_str[2];
		}
		else {
			l_str[2]='19'+l_str[2]; 
	}	}

//Check if day of month is valid for month in year (check leapyear too)
   if (d_pos >=0)
   {
	if (!isNaN(l_str[2]) && !isNaN(l_str[1])) {
	   	if (((l_str[2] % 4 == 0) && (l_str[2] % 100 != 0)) || (l_str[2] % 400 == 0)) {
			l_days[1] = 29;
		} 
    		else {
     		    	l_days[1] = 28;
		}
		if (l_str[0] > l_days[l_str[1]-1]) l_str[0] = l_days[l_str[1]-1];
	}
 }
//If length of month > 2, just use the last two digits in the month.
    if (l_str[1].length > 2) l_str[1] = l_str[1].substring(l_str[1].length-2);
//If length of month < 2, zero fill it
	while (l_str[1].length < 2) { l_str[1] = '0'+l_str[1]; }	
	
//Store numeric date for future range checking. 	
	numericDate = parseFloat(l_str[2]+l_str[1]+l_str[0]);
	numericDate = isNaN(numericDate)? 0 : numericDate;

//Replaces the numeric month with string from the table if type is "C"
	if (m_type == "C") {
		if (!isNaN(l_str[1]) && l_str[1] <= 12) {
			l_str[1]=l_mon[l_str[1]-1]; 
		}
		else 	{
			for(var count=0; count <= 11; ++count) {
	 			if (l_str[1].toUpperCase() == l_mon[count].toUpperCase() ) {
					l_str[1]=l_mon[count];
			  		break;
			}	}
			if (count == 12) {
				l_str[1] = '???';
	}	}	}

//Finally, swap items around into output format and add delimiters
  if (d_pos >=0)
  {
	temp[d_pos] = l_str[0];
  }
	temp[m_pos] = l_str[1];
	temp[y_pos] = l_str[2];
  if (d_pos >=0)
  {
	l_splitstr=temp[0]+delim+temp[1]+delim+temp[2];
  }
  else
  {
    l_splitstr=temp[0]+delim+temp[1];
  }
	
	return l_splitstr;

//End of Datecheck Function
}

//This function checks whether P_value is in the date format
//provided in P_NLS_format
function datecheck_static(P_value, P_NLS_format)
{
   var l_val_length=P_value.length;
   var l_nls_length=P_NLS_format.length;
	var df = new Array('','','');
	var val = new Array('','','');
	var l_nls_splitstr='';
	var l_val_splitstr='';
	var l_index=0;
	var i=0;
	var l_splitcount=1;
	var l_char='';
	var l_prevspace=true;
	var y_extra=0;
	var d_extra=0;
	var m_extra=0; 
	var m_ind=0;
	var y_ind=0;
	var l_delims = /\W/
	var monthvalid = 0;
	var monthcnt = 0;


   //exit if no date passed in 
	if (P_value == null|P_value == ''| P_value == ' ') return ' ';


	evalNLS(P_NLS_format);

    //Added for NLS issue
    //In the French env date_validation was failing due to incorrect delimiter
    //Changed by sspurani 02/08/01
       if (delim)
        l_delims = delim ;


   //split the dateformat P_NLS_format into three char arrays

   //splits the NLS_format string using delimiters after compressing spaces 
   //and common delimiter characters
   for (var count=0; count < l_nls_length; count++ ) {
		l_char=P_NLS_format.charAt(count);
			if (l_char.search(l_delims)==0) {
			 	if (l_prevspace==false) { l_nls_splitstr += l_char; }
			  	l_prevspace = true;
			}
			else	{ l_prevspace=false;
				  l_nls_splitstr += l_char;
		}
	}


//splits the P_value string using delimiters after compressing spaces 
//and common delimiter characters
for (var count=0; count < l_val_length; count++ ) {
 l_char=P_value.charAt(count);
	if (l_char.search(l_delims)==0) {
		if (l_prevspace==false) { l_val_splitstr += l_char; }
			 l_prevspace = true;
		}
		else	{ l_prevspace=false;
			l_val_splitstr += l_char;
	}
}
		

	
//splits NLS string by delimiter into 2 or 3 parts

        var lnls = 0;
		for( var count=0; count < l_nls_length; count++ ) {
			l_char=l_nls_splitstr.charAt(count);
			if (l_char.search(l_delims)==0) {
				lnls = lnls + 1; 
			}
			else {
			    df[lnls] = df[lnls] + l_char;
          }
		}

 //split P_value into two or three char arrays

        var lval = 0;
		for( var count=0; count < l_val_length; count++ ) {
			l_char=l_val_splitstr.charAt(count);
			if (l_char.search(l_delims)==0) {
				lval = lval + 1; 
			}
			else {
			    val[lval] += l_char;
          }
		}
	

 //compare the length of corresponding arrays for date format and
 //date value
for (var i=0; i<=lval; i++)
{
 switch (df[i]){
   case "DD" : 
      
      if (!isNaN(parseFloat(val[i]))) 
	  {
       if (d_pos >=0)
       {
	   for(var j=0; j <= 2; ++j)
	   {
	     if (df[j] == 'MM' || df[j] == 'MON')
	     {
		  m_ind = j;
	     }
		 else {
	       if (df[j] == 'YY' || df[j] == 'YYYY' || df[j] == 'RR' || df[j] == 'RRRR')
	       {
		    y_ind = j;
	       }
		 }
		}

       //Get the numeric value of the month in char format
	    if (m_type == "C") {
		  var tmpval = val[m_ind];
		  for( var count=0; count < 12;  count++ ) {
		   if (l_mon[count].toUpperCase() == tmpval.toUpperCase()){
		   		     monthcnt = count + 1;
		    }
		   }
		  }
		 else
		    monthcnt = val[m_ind];


	     if ((!isNaN(val[y_ind])) && (!isNaN(monthcnt))) {
	   	   if (((val[y_ind] % 4 == 0) && (val[y_ind] % 100 != 0)) || (val[y_ind] % 400 == 0)) {
			l_days[m_ind] = 29;
		  } 
    	  else {
     		l_days[m_ind] = 28;
		  }
		  }
		  if (val[i] > l_days[monthcnt-1]) {
		    alert(dateErrorMsg);
			return false;
		  }
	    }
	  }

      break;
   case "MON" : 

        //Check if the date format is MON-YYYY
        if (d_pos <0)
        {
         val[2] = '01';
         }  

        //check if the the month value is non-numeric
      	//if (m_type == "C") {
		   
		   for(var count=0; count <= 11; count++) 
			{
	 		  if (val[i].toUpperCase() == l_mon[count].toUpperCase() ) 
			  {
			    monthvalid = 1;
			  }
			 }
			 if (monthvalid != 1)
			  {
			   alert(dateErrorMsg);
			   return false
			  }
				
		//  }

      break;
   case "MM" : 
      if ( isNaN(val[i]) || val[i].length >2 || (val[i] <= 0) || (val[i] > 12))
      {
	  alert(dateErrorMsg);
	  return false;
		break;
      }
	  break;
   case "YY" : 

      //adds century to a 2 digit year using an arbitrary split point
	if ((val[i].length <=2 || parseFloat(val[i])< 100 ) && val[i].indexOf("?") == -1 ) {
		val[i] = parseFloat(val[i])+'';
		while (val[i].length < 2) {
			val[i] = '0'+val[i];
		}
		if (val[i] < l_yearbreak) {
			val[i]='20'+val[i];
		}
		else {
			val[i]='19'+val[i]; 
	}	}


      if (parseInt(val[i]) < 0)
      {
	  alert(dateErrorMsg);
 	  return false;
		break;
      }
      break;
   case "RR" : 

   	if ((val[i].length <=2 || parseFloat(val[i])< 100 ) && val[i].indexOf("?") == -1 ) {
		val[i] = parseFloat(val[i])+'';
		while (val[i].length < 2) {
			val[i] = '0'+val[i];
		}
		if (val[i] < l_yearbreak) {
			val[i]='20'+val[i];
		}
		else {
			val[i]='19'+val[i]; 
	}	}


      if (parseInt(val[i]) < 0 )
      {
	  alert(dateErrorMsg);
  	  return false;
		break;
      }

      break;
   case "RRRR" : 

      	if ((val[i].length <=2 || parseFloat(val[i])< 100 ) && val[i].indexOf("?") == -1 ) {
		val[i] = parseFloat(val[i])+'';
		while (val[i].length < 2) {
			val[i] = '0'+val[i];
		}
		if (val[i] < l_yearbreak) {
			val[i]='20'+val[i];
		}
		else {
			val[i]='19'+val[i]; 
	}	}


      if (parseInt(val[i]) < 0)
      {
	  alert(dateErrorMsg);
  	  return false;
		break;
      }

      break;
   default : null;
 }
}
//Finally, swap items around into output format and add delimiters
  
	
       if (d_pos >=0)
       {
	l_val_splitstr=val[0]+delim+val[1]+delim+val[2];
       }
       else
       {
	l_val_splitstr=val[2]+delim+val[0]+delim+val[1];
       }
 
	return l_val_splitstr;

}

// End datecheck_static



function formatdate (P_Day, P_Month, P_Year, P_Dateformat) {
//This function returns the formatted output when fed the parts of the date.
//It relies on the presence of the same global javascript variables used by the
//evalNLS function.  It also assumes valid date components (as coming from the calendar)
	var temp = new Array('','','');
	var l_splitstr='';
	if (P_Dateformat) evalNLS (P_Dateformat);
	if (m_type == "C") P_Month=l_mon[parseFloat(P_Month)-1]; 
	if (d_pos >=0)
	{
	 temp[d_pos] = P_Day;
	}
	temp[m_pos] = P_Month;
	temp[y_pos] = P_Year;
	if (d_pos >=0)
	{
	l_splitstr=temp[0]+delim+temp[1]+delim+temp[2];
	}
	else
	{
	 l_splitstr=temp[0]+delim+temp[1];
	}

	return l_splitstr;
}
//Accepts the NLS format string and populates the variables necessary to apply it.
function evalNLS (P_Dateformat) {
	if (P_Dateformat) { NLSformat = P_Dateformat; }

	m_pos=NLSformat.indexOf('MON'); 
	m_type="C";
	l_len[1]=3;
	if (m_pos < 0) { m_pos=NLSformat.indexOf('MM');  m_type="N"; l_len[1]=2;}
	d_pos=NLSformat.indexOf('DD');
	if (d_pos<0)
	{
	 d_pos= -1;
	}
	y_pos=NLSformat.indexOf('RRRR');
        if (y_pos < 0)
         y_pos=NLSformat.indexOf('YYYY');
	if (d_pos == -1)
	{
	 delim=NLSformat.charAt(y_pos - 1);
	}
	 else
	 {
	  if (m_pos!=0) delim=NLSformat.charAt(m_pos - 1); 
		else delim = NLSformat.charAt(d_pos - 1);
	 }
	if (m_pos > 5) m_pos=2; else if (m_pos < 3) m_pos=0; else m_pos=1;
	if (d_pos >= 0)
	{
	 if (d_pos > 5) d_pos=2; else if (d_pos < 3) d_pos=0; else d_pos=1;
	}
	if (y_pos > 5) y_pos=2; else if (y_pos < 3) y_pos=0; else y_pos=1;

}
//Returns an alert if the date entered is in error.  This should be an official alert.
//If the date is OK, then the date string will be passed to an optionally included
//function which is custom written by the developer.
function checkForError (obj, func) 
{
	if (obj.value.indexOf('?') != -1) 
	{
		if (dateErrorMsg != null) alert(dateErrorMsg);
		obj.focus();
	} else
	{
		if (func) func(obj.value,numericDate);
	}
}
//Required to open and control the calendar
//function opencal(fieldobj,func) {
function opencal(fieldobj) {
	var openleft=100;
	var opentop=100;
	if (Calwindow.close && !Calwindow.closed) {
	// don't position calendar if already open
		var attr = "resizable=no,width=" + calwidth + ",height=" + calheight; } 
	else {
	if (Nav4) {
	// center on the main window
		openleft = parseInt(window.screenX + ((window.outerWidth - calwidth) / 2),10);
		opentop = parseInt(window.screenY + ((window.outerHeight - calheight) / 2),10);
		var attr = "screenX=" + openleft + ",screenY=" + opentop;
		attr += ",resizable=no,width=" + calwidth + ",height=" + calheight; } 
	else {
	// best we can do is center in screen
		openleft = parseInt(((screen.width - calwidth) / 2),10);
		opentop = parseInt(((screen.height - calheight) / 2),10);
		
		var attr = "left=" + openleft + ",top=" + opentop;
		attr += ",resizable=no,width=" + calwidth + ",height=" + calheight; }
 	}
	Calwindow=open(calfile, 'Calendar', attr);
	window.fillfield = fieldobj;
	//if (func) {window.afterfunc = func}
	if (Calwindow.opener == null) {
		Calwindow.opener = self; 
	}
}

function closecal() { 
	if (Calwindow.close && !Calwindow.closed) { 
		//delete window.afterfunc;
		delete window.fillfield;
		Calwindow.close();
}	}


function validate_date(sdate, edate, P_NLS_FORMAT){
  
	 
P_NLS_FORMAT = P_NLS_FORMAT.toUpperCase();
var currdate_arr = currdate.split(delim);
var sysday   = currdate_arr[0].toUpperCase();
var sysmonth = currdate_arr[1].toUpperCase();
var sysyear  = currdate_arr[2].toUpperCase();


//Start Date parameter exists
if (sdate != 'NOTEXIST') {

    sdate_str = datecheck_static(sdate, P_NLS_FORMAT);
    sdate_str = sdate_str.toUpperCase();
	
    if (!sdate_str)
    {
     return false;
    }
   //Split start date into day, month and year substrings
    sdate_arr = sdate_str.split(delim);
}

//End Date parameter exists
if (edate != 'NOTEXIST') {
    edate_str = datecheck_static(edate, P_NLS_FORMAT);
    edate_str = edate_str.toUpperCase();

    if (!edate_str)
    {
      return false;
    }
   //Split end date into day, month and year substrings
    edate_arr = edate_str.split(delim);
}
    
    //if (d_pos >= 0)
    //{
      if (sdate != 'NOTEXIST') {
	s_day   = sdate_arr[0];
	s_month = sdate_arr[1];
        s_year  = sdate_arr[2];
      }

      if (edate != 'NOTEXIST') {
	e_day   = edate_arr[0];
        e_month = edate_arr[1];
        e_year  = edate_arr[2];
      }
   /*}
   else
   {
      if (sdate != 'NOTEXIST') {
        s_month = sdate_arr[0];
        s_year  = sdate_arr[1];
      }

      if (edate != 'NOTEXIST') {
        e_month = edate_arr[0];
        e_year  = edate_arr[1];
      }
   }
*/


    //Replace charcter months by numeric months
   if (m_type == "C") {
     for( var count=0; count < 12;  count++ ) {
       if ((sdate != 'NOTEXIST') && (l_mon[count].toUpperCase() == s_month)){
	     s_month = count + 1;
        }
       if ((edate != 'NOTEXIST') && (l_mon[count].toUpperCase() == e_month)){
	     e_month = count + 1;
        }
       if (l_mon[count].toUpperCase() == sysmonth){
             sysmonth = count + 1;
        }
       }
      }

   //Current date validation for start date
   if (edate == 'NOTEXIST') {
       //if ((d_pos>=0) &&
       if (( s_year >  sysyear ) ||
          ((s_year == sysyear) && (s_month > sysmonth))  ||
          ((s_year == sysyear) && (s_month == sysmonth) && (s_day > sysday))
          )
       {
                alert(invalidStartDateErrMsg);
                return false;
       }
     /*
       else
       {
         if ((d_pos<0) &&
               (( s_year >  sysyear ) ||
               ((s_year == sysyear) && (s_month > sysmonth))
                  ) )
         {
                alert(invalidStartDateErrMsg);
                return false;
         }
      */
         //Start Date < Current Date
         else
         {
         return true;
        }

   //  }
   }

  //Current date validation for end date
   else
   { if (sdate == 'NOTEXIST'){
       //if ((d_pos>=0) &&
       if (( e_year >  sysyear ) ||
          ((e_year == sysyear) && (e_month > sysmonth))  ||
          ((e_year == sysyear) && (e_month == sysmonth) && (e_day > sysday))
          )
       {
                alert(invalidEndDateErrMsg);
                return false;
       }
      /*
       else
       {
         if ((d_pos<0) &&
               (( e_year >  sysyear ) ||
               ((e_year == sysyear) && (e_month > sysmonth))
                  ) )
         {
                alert(invalidEndDateErrMsg);
                return false;
         }
        */
         //End Date < Current Date
         else
         {
         return true;
        }
     //}
   }

    //Start and End Date Validation
    //Start Date > End Date
     else
     {

     if ((sdate_str.indexOf('?') != -1) 
      ||(edate_str.indexOf('?') != -1))
     {
       if (dateErrorMsg != null) alert(dateErrorMsg);
         return false;
     }
    else
    {
     if (((sdate_str == ' ') && (edate_str != ' '))  
     || ((edate_str == ' ') && (sdate_str != ' '))
     )
    {
	alert(invaliddatesErrMsg);
        return false;
    }
    else
    {
     //if ( (d_pos>=0) &&
       if     ( ( s_year > e_year) || 
	      ( (s_year == e_year) && (s_month > e_month) ) ||
	       ( s_year == e_year && s_month == e_month && s_day > e_day) 
	    ) 
	 
     {
	alert(invaliddatesErrMsg);
         return false;
     }
     else
     {
       //if ((d_pos>=0) &&
       if (( e_year >  sysyear ) ||
          ((e_year == sysyear) && (e_month > sysmonth))  ||
          ((e_year == sysyear) && (e_month == sysmonth) && (e_day > sysday))
	  )
       {
		alert(invalidEndDateErrMsg);
                return false;
       }
	 
    /*
     else 
     {
	   if  ( (d_pos<0) &&
	      ( ( s_year > e_year) || 
              ( (s_year == e_year) && (s_month > e_month) )
			) )
		
	    {
                alert(invaliddatesErrMsg);
                return false;
	    }
	 
	  else
	  {
	   if ((d_pos<0) && 
	       (( e_year >  sysyear ) ||
	       ((e_year == sysyear) && (e_month > sysmonth))
		  ) )
	   {
		alert(invalidEndDateErrMsg);
                return false;
	   }
          */

     //Start Date < End Date
	 else 
	 {
         return true;
        }
       //}
      //}
     }
    }
   }
  }
 }
}

