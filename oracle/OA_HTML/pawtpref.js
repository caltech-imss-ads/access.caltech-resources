//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtpref.js                       |
//  |                                                |
//  | Description:                                   |
//  |                                                |
//  | History:                                       |
//  |          03-06-2000 Start R11i implementation  |
//  |          by kwidjaja                           |
//  |          04-03-2000 Made Preferences a         |
//  |          primary page only by kwidjaja         |
//  |                                                |
//  +================================================+
/* $Header: pawtpref.js 115.26 2000/09/01 13:39:19 pkm ship        $ */


//----------------------------------------------------
// function: fTrimEndingSpace
// Description: Trims ending whitespace of a string.
//----------------------------------------------------

function fTrimEndingSpace(p_strValue)
{
  var l_strNew = p_strValue;
  while ( l_strNew.substring(l_strNew.length-1, l_strNew.length) == " "
       || l_strNew.substring(l_strNew.length-1, l_strNew.length) == "\n"
       || l_strNew.substring(l_strNew.length-1, l_strNew.length) == "\t"
       || l_strNew.substring(l_strNew.length-1, l_strNew.length) == "\r"  )
  {
    l_strNew = l_strNew.substring(0, l_strNew.length-1);
  }
  return l_strNew; 
}


//----------------------------------------------------
// function: fPrefCancel
// Description: Cancel routine called by the cancel
// button.
//----------------------------------------------------

function fPrefCancel(p_winDisplay) {
  var l_bCancel = false;
  var l_bConfirm = true;

  l_bConfirm = fPrefChanged (top.framMainBody, g_objPref);
/*
  fDrawConfirm(g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_CANCEL"),
              "window.close()",  null);
*/
  
  // If l_bConfirm is undefined or true, ask for confirmation 
  if (l_bConfirm != false) {
    if (confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_CANCEL"))) {
      l_bCancel = true;
    }
  }
  // if false, cancel without confirmation 
  else {
    l_bCancel = true;
  }

  if (l_bCancel) {
    if (  g_iProjLOVCurLine != null || g_iTaskLOVCurLine != null ) // if LOV is up                                                                              
    {                                                                           
      if ( ! lov_win.closed )                                                   
      {                                                                         
        lov_win.close();                                                        
        g_iProjLOVCurLine = null;                                               
        g_iTaskLOVCurLine = null;                                               
      }                                                                         
    }                                                                           
    window.close();
    if ( g_winError != null ) { g_winError.close(); }
  }
}


//----------------------------------------------------
// function: fPrefOnChangeDefAppr
// Description:
//----------------------------------------------------

function fPrefOnChangeDefAppr(p_formDisplay)
{
        p_formDisplay.hidDefApprID.value   = "";
	p_formDisplay.PA_WEB_APPR_ID.value = "";
}


//--------------------------------------------------------
// function: fPrefOnClickDefApprLOV
// Description: LOV function handler for Default approver
// text field's LOV.
//--------------------------------------------------------

function fPrefOnClickDefApprLOV() {
  top.g_formLOV = 
    top.g_framPref.framMainBody.framBodyHeader.document.formPrefBodyHeader;

  top.g_formLOV.PA_WEB_APPR_NAME.value = top.g_formLOV.txtDefApprName.value;

  top.LOV('275', 'PA_WEB_APPR_NAME', '601', 'PA_WEB_APPR_LOV_DUMMY', 
    'formPrefBodyHeader', 'top.g_framPref.framMainBody.framBodyHeader', '', ''); 
}


//--------------------------------------------------------
// function: fPrefUpdateDispSelectedAlias
// Description: Update the form by selecting the proper line.
//              Draw the arrow in the selected line.
//--------------------------------------------------------

function fPrefUpdateDispSelectedAlias(p_iLineNum, p_formDisplay) {
  g_iPrefPrevLine = g_iPrefCurrLine;
  g_iPrefCurrLine = p_iLineNum;

  fGetInputWidget (p_formDisplay, "imgCurrLineIndicator", g_iPrefPrevLine).src 
    = g_imgEmpty.src; 
  fGetInputWidget (p_formDisplay, "imgCurrLineIndicator", g_iPrefCurrLine).src 
    = g_imgArrow.src; 
}


//--------------------------------------------------------
// function: fPrefChanged
// Description: Determines whether the the user preferences
// have changed.
//--------------------------------------------------------
function fPrefChanged(p_framTarget, p_objPref)
{
  return (fPrefBodyHeaderChanged(p_framTarget.framBodyHeader.document.formPrefBodyHeader, p_objPref) ||
   
    fPrefBodyContentChanged(p_framTarget.framBodyContent.document.formPrefBodyContent, p_objPref));
}

//--------------------------------------------------------
// function: fPrefBodyHeaderChanged
// Description: Determines whether the body header
// has changed from the one given in the parameter.
//--------------------------------------------------------

function fPrefBodyHeaderChanged(p_formDisplay, p_objPref)
{
  // Check the Default comment value
  if (p_formDisplay.txaDefComment.value != p_objPref.mGetstrDefaultComment())
    return (true);

  // Check the Overriding Approver
  if ( g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER") == C_strYES )
    {
    if (p_formDisplay.txtDefApprName.value != 
      p_objPref.mGetobjDefaultApprover().mGetstrName() ||

        p_formDisplay.hidDefApprID.value != 
        p_objPref.mGetobjDefaultApprover().mGetiID())
      return (true);

    }
  // Check the working days
  if (p_formDisplay.cbxSunday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iSUNDAY) ||  
      p_formDisplay.cbxMonday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iMONDAY) ||
      p_formDisplay.cbxTuesday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iTUESDAY) ||
      p_formDisplay.cbxWednesday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iWEDNESDAY) ||
      p_formDisplay.cbxThursday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iTHURSDAY) ||
      p_formDisplay.cbxFriday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iFRIDAY) ||
      p_formDisplay.cbxSaturday.checked != 
      p_objPref.mGetobjWorkDays().bIsWorkDay(C_iSATURDAY)) 
    return (true);
  return (false);
}


//--------------------------------------------------------
// function: fPrefBodyContentChanged
// Description: Determines whether the values for the body
// contents, the alias lines, have changed or not.
//--------------------------------------------------------

function fPrefBodyContentChanged(p_formDisplay, p_objPref)
{
  var strAliasName, iProjID, strProjNum, strTaskNum, iTaskID, iTypeIndex;

  // If the number of lines in the form and the object are not the same
  // the form has changed.
  if (g_iPrefLineCount != p_objPref.mGetobjAliases().miGetNumAliases()) {
    return (true);
  }

  for ( var i=1; i<= p_objPref.mGetobjAliases().miGetNumAliases(); i++)
  {
    if ( p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mbIsEmpty() )
    {
      strAliasName = "";
      iProjID = "";
      strProjNum = "";
      iTaskID = "";
      strTaskNum = "";
      iTypeIndex = 0;
     }
     else
     {
       strAliasName =  
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrName();
       iProjID = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetiProjectID() ;
       strProjNum = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrProjectNum();
       strTaskNum = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrTaskNum();
       iTaskID = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetiTaskID();
     
       if ( p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetobjType() )
       {
         iTypeIndex = 
           g_objTypes.miGetTypeIndex(p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetobjType().mGetstrName());
       }
       else
       {
         iTypeIndex = 0;
       }
     }
     // Compare the line with values from the preferences object
     
     if (fGetInputWidget(p_formDisplay,"txtAliasName", i).value !=strAliasName 
       || fGetInputWidget(p_formDisplay, "hidProjID", i).value != iProjID
       || fGetInputWidget(p_formDisplay, "txtProjNum", i).value != strProjNum
       || fGetInputWidget(p_formDisplay, "hidTaskID", i).value != iTaskID
       || fGetInputWidget(p_formDisplay, "txtTaskNum", i).value != strTaskNum
     
     // Check expenditure type.
       || fGetInputWidget(p_formDisplay, "popType", i).options[iTypeIndex].selected != true)
       return (true);
  }
  return (false);
}


//--------------------------------------------------------
// function: fPrefUpdateBodyHeader
// Description: Updates the displayed values for the body
// header.
//--------------------------------------------------------

function fPrefUpdateBodyHeader(p_formDisplay, p_objPref)
{
  // Update the Default comment value
  p_formDisplay.txaDefComment.value = p_objPref.mGetstrDefaultComment();

  // Update the Overriding Approver
  if ( g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER") == C_strYES )
    {
    p_formDisplay.txtDefApprName.value = 
      p_objPref.mGetobjDefaultApprover().mGetstrName();

    p_formDisplay.hidDefApprID.value = 
      p_objPref.mGetobjDefaultApprover().mGetiID();

    }
  // Update the working days
  p_formDisplay.cbxSunday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iSUNDAY);   
  p_formDisplay.cbxMonday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iMONDAY); 
  p_formDisplay.cbxTuesday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iTUESDAY); 
  p_formDisplay.cbxWednesday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iWEDNESDAY); 
  p_formDisplay.cbxThursday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iTHURSDAY); 
  p_formDisplay.cbxFriday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iFRIDAY); 
  p_formDisplay.cbxSaturday.checked = 
    p_objPref.mGetobjWorkDays().bIsWorkDay(C_iSATURDAY); 
}


//--------------------------------------------------------
// function: fPrefUpdateBodyContent
// Description: Updates the displayed values for the body
// contents, the alias lines.
//--------------------------------------------------------

function fPrefUpdateBodyContent(p_formDisplay, p_objPref)
{
  var strAliasName, iProjID, strProjNum, strTaskNum, iTaskID, iTypeIndex;
  for ( var i=1; i<= p_objPref.mGetobjAliases().miGetNumAliases(); i++)
  {
    if ( p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mbIsEmpty() )
    {
      strAliasName = "";
      iProjID = "";
      strProjNum = "";
      iTaskID = "";
      strTaskNum = "";
      iTypeIndex = 0;
     }
     else
     {
       strAliasName =  
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrName();
       iProjID = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetiProjectID() ;
       strProjNum = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrProjectNum();
       strTaskNum = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetstrTaskNum();
       iTaskID = 
         p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetiTaskID();
     
       if ( p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetobjType() )
       {
         iTypeIndex = 
           g_objTypes.miGetTypeIndex(p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mGetobjType().mGetstrName());
       }
       else
       {
         iTypeIndex = 0;
       }
     }
     // Set display
     
     fGetInputWidget(p_formDisplay,"txtAliasName", i).value = strAliasName;
     
     fGetInputWidget(p_formDisplay, "hidProjID", i).value = iProjID;
     fGetInputWidget(p_formDisplay, "txtProjNum", i).value = strProjNum;
       
     fGetInputWidget(p_formDisplay, "hidTaskID", i).value = iTaskID;
     fGetInputWidget(p_formDisplay, "txtTaskNum", i).value = strTaskNum;
     
     // Set expenditure type.
     fGetInputWidget(p_formDisplay, "popType", i).options[iTypeIndex].selected = true;

  }
}


//--------------------------------------------------------
// function: fPrefUpdateDisp
// Description: Updates the displayed values for all 
// preferences.
//--------------------------------------------------------
function fPrefUpdateDisp(p_framTarget, p_bNewPref)
{

// New to set Time out for Macintosh to avoid racing conditions.
// When setting timeout, cannot pass object parameters to the function, Hence hard coding parameter values

  if (p_bNewPref) {
    setTimeout("fPrefUpdateBodyHeader(top.framMainBody.framBodyHeader.document.formPrefBodyHeader, top.g_objNewPref)", 200);
    setTimeout("fPrefUpdateBodyContent(top.framMainBody.framBodyContent.document.formPrefBodyContent,top.g_objNewPref)", 200);
  }
  else {
    setTimeout("fPrefUpdateBodyHeader(top.framMainBody.framBodyHeader.document.formPrefBodyHeader, top.g_objCurPref)", 200);
    setTimeout("fPrefUpdateBodyContent(top.framMainBody.framBodyContent.document.formPrefBodyContent,top.g_objCurPref)", 200);
  }

}


//--------------------------------------------------------
// function: fPrefUpdateStructPref
// Description: Updates the preferences data structure 
// with the displayed value.
//--------------------------------------------------------

function fPrefUpdateStructPref(p_framMainBody, p_objPref)
{
  var l_formDisplayHeader = 
    p_framMainBody.framBodyHeader.document.formPrefBodyHeader;
  
  var l_formDisplayContent = 
    p_framMainBody.framBodyContent.document.formPrefBodyContent;
  
  var strComment = l_formDisplayHeader.txaDefComment.value;
  
  // Get rid of newlines in comment string
  strComment = strComment.replace(/\r\n/g, ' ');

  // truncate the Default comment string
  p_objPref.mSetstrDefaultComment( strComment.substring(0, 249) );

  // Set the default approver
  if ( g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER") == C_strYES )
  {
    p_objPref.mSetobjDefaultApprover( l_formDisplayHeader.txtDefApprName.value, 
      l_formDisplayHeader.hidDefApprID.value );
  }

  // Set the work days
  p_objPref.mGetobjWorkDays().mSetWorkDays(
    l_formDisplayHeader.cbxSunday.checked,
    l_formDisplayHeader.cbxMonday.checked,
    l_formDisplayHeader.cbxTuesday.checked,
    l_formDisplayHeader.cbxWednesday.checked,
    l_formDisplayHeader.cbxThursday.checked,
    l_formDisplayHeader.cbxFriday.checked,
    l_formDisplayHeader.cbxSaturday.checked);

  // Set all the aliases
  for ( i=1; i<=g_iPrefLineCount; i++)
  {
     strAliasName = fTrimEndingSpace(fGetInputWidget(l_formDisplayContent, 
       "txtAliasName", i).value); 
     iProjID = fTrimEndingSpace(fGetInputWidget(l_formDisplayContent, 
       "hidProjID", i).value);
     strProjNum = fTrimEndingSpace(fGetInputWidget(l_formDisplayContent, 
       "txtProjNum", i).value);
     strTaskNum = fTrimEndingSpace(fGetInputWidget(l_formDisplayContent, 
       "txtTaskNum", i).value);
     iTaskID =  fTrimEndingSpace(fGetInputWidget(l_formDisplayContent,
       "hidTaskID", i).value);

     for ( j=0; j<fGetInputWidget(l_formDisplayContent, 
        "popType", i).options.length; j++)
     {
       if ( fGetInputWidget(l_formDisplayContent, 
         "popType", i).options[j].selected )
       {
         // Options[j].value should be j.
         iTypeIndex = j;
         break;
       }
     }
     
//     alert(i+ " alias: \"" + strAliasName + "\" iProjID: \"" + iProjID + "\" iTaskID: \"" + iTaskID + "\" strProjNum: \"" + strProjNum + "\" strTaskNum: \"" + strTaskNum + "\"" + "\niTypeIndex = " + iTypeIndex);
 
     if ( i > p_objPref.mGetobjAliases().miGetNumAliases() )
     {
       // This is a new alias
       p_objPref.mGetobjAliases().mAddAlias(
            strAliasName, 
            iProjID, 
            strProjNum, 
            iTaskID, 
            strTaskNum,
            g_objTypes.arrType[iTypeIndex]); // This should be good as we have a dummy selection and will be selected by default 
     }
     else // old entry, update it
     {
       p_objPref.mGetobjAliases().mGetarrAlias()[i-1].mSetAlias(
            strAliasName,  // Alias name
            iProjID,  // Project ID
            strProjNum, // Project Num
            iTaskID,  // Task ID
            strTaskNum,   // Task Num
            g_objTypes.arrType[iTypeIndex]);
     }
  }
}


function fPrefOnChangeProject(p_formDisplay, p_iIndex) {
  fGetInputWidget(p_formDisplay, "hidProjID", p_iIndex).value = "";
  fGetInputWidget(p_formDisplay, "hidTaskID", p_iIndex).value = "";
}

function fPrefOnChangeTask(p_formDisplay, p_iIndex) {
  fGetInputWidget(p_formDisplay, "hidTaskID", p_iIndex).value ="";
}


//--------------------------------------------------------
// function: fPrefOnClickProjectLOV
// Description: Handler for clicking on the Project LOV 
// button.
//--------------------------------------------------------
                                                          
function fPrefOnClickProjectLOV(p_iLineNum) {

  top.g_formLOV = 
    top.g_framPref.framMainBody.framBodyContent.document.formPrefBodyContent;
  top.g_strFormLOV = C_strPREFERENCES;

  // Draw selection arrow
  fPrefUpdateDispSelectedAlias(p_iLineNum, top.g_formLOV);

  g_iProjLOVCurLine = p_iLineNum;  
  top.g_formLOV.PA_WEB_PROJECT_NUMBER.value = 
    fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNum).value;
  top.g_formLOV.PA_WEB_PROJECT_ID.value = 
    fGetInputWidget(top.g_formLOV, "hidProjID", p_iLineNum).value;

  top.LOV('275','PA_WEB_PROJECT_NUMBER','601', 'PA_WEB_PROJECTS_TASKS_LOV_DMY', 'formPrefBodyContent', 'top.g_framPref.framMainBody.framBodyContent', '', '');  // Need to work out parameters
}


//--------------------------------------------------------
// function: fPrefOnClickTaskLOV                       
// Description: Handler for clicking on the Task LOV   
// button.                                                
//--------------------------------------------------------

function fPrefOnClickTaskLOV(p_iLineNum) {
  var strWhereClause;

  top.g_formLOV = 
    top.g_framPref.framMainBody.framBodyContent.document.formPrefBodyContent;
  top.g_strFormLOV = C_strPREFERENCES;

  if ( fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNum).value.length == 0 ) // No project has been selected
  {
    //fDrawAlert(g_objFNDMsg.mstrGetMsg("PA_WEB_TASK_LOV_NO_PROJECT"), null);
    alert(g_objFNDMsg.mstrGetMsg("PA_WEB_TASK_LOV_NO_PROJECT"));
    
    return;
  }

  // Draw arrow for selected line
  fPrefUpdateDispSelectedAlias(p_iLineNum, top.g_formLOV);        
  g_iTaskLOVCurLine = p_iLineNum;  

  top.g_formLOV.PA_WEB_PROJECT_NUMBER.value = 
    fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNum).value;

  top.g_formLOV.PA_WEB_PROJECT_ID.value =
    fGetInputWidget(top.g_formLOV, "hidProjID", p_iLineNum).value;
   

  top.g_formLOV.PA_WEB_TASK_NUMBER.value
    = fGetInputWidget(top.g_formLOV, "txtTaskNum", p_iLineNum).value;
   

  top.g_formLOV.PA_WEB_TASK_ID.value = 
    fGetInputWidget(top.g_formLOV, "hidTaskID", p_iLineNum).value;

  strWhereClause = "PROJECT_NUMBER=\'" + 
    fReplaceSpaceWithPlus(fSafeURL(top.g_formLOV.PA_WEB_PROJECT_NUMBER.value)) 
    + "\'";

  LOV('275', 'PA_WEB_TASK_NUMBER', '601', 'PA_WEB_PROJECTS_TASKS_LOV_DMY', 
    'formPrefBodyContent', 'top.g_framPref.framMainBody.framBodyContent', '', 
    strWhereClause); // Need to work out the parameters

}


//--------------------------------------------------------------
// function: fPrefCopyALine
// Description: Copy line function copy all the contents of the 
// currently selected line to a new line
//--------------------------------------------------------------

function fPrefCopyALine(p_formDisplay, p_iCurLineNum, p_iNewLineNum)
{
  fGetInputWidget(p_formDisplay, "txtAliasName", p_iNewLineNum).value = 
    fGetInputWidget(p_formDisplay, "txtAliasName", p_iCurLineNum).value;

  fGetInputWidget(p_formDisplay, "txtProjNum", p_iNewLineNum).value = 
    fGetInputWidget(p_formDisplay, "txtProjNum", p_iCurLineNum).value;

  fGetInputWidget(p_formDisplay, "hidProjID", p_iNewLineNum).value = 
    fGetInputWidget(p_formDisplay, "hidProjID", p_iCurLineNum).value;

  fGetInputWidget(p_formDisplay, "txtTaskNum", p_iNewLineNum).value = 
    fGetInputWidget(p_formDisplay, "txtTaskNum", p_iCurLineNum).value;

  fGetInputWidget(p_formDisplay, "hidTaskID", p_iNewLineNum).value = 
    fGetInputWidget(p_formDisplay, "hidTaskID", p_iCurLineNum).value;

  // Unselect the old pop list value
  for ( iOldIndex=0; iOldIndex < fGetInputWidget(p_formDisplay,"popType", p_iNewLineNum).options.length; iOldIndex++ )
  {
    if (  fGetInputWidget(p_formDisplay, "popType", p_iNewLineNum).options[iOldIndex].selected )
    {
      fGetInputWidget(p_formDisplay, "popType", p_iNewLineNum).options[iOldIndex].selected = false;
      break;
    }
  }
  // Select the new pop list value
  for ( iIndex=0; iIndex < fGetInputWidget(p_formDisplay, "popType", p_iCurLineNum).options.length; iIndex++ )
  {
    if (  fGetInputWidget(p_formDisplay, "popType", p_iCurLineNum).options[iIndex].selected )
    {
      fGetInputWidget(p_formDisplay, "popType", p_iNewLineNum).options[iIndex].selected = true;
      break;
    }
  }    
}


//--------------------------------------------------------------
// function: fPrefOnClickClearLine
// Description: Handler for clicking on the Clear Line button.
// 
//--------------------------------------------------------------

function fPrefOnClickClearLine(p_framTarget) {
  var l_formDisplay = p_framTarget.document.formPrefBodyContent;


  // Get the selected line. Assume we are in stand alone mode for now
  var l_iCurLineNum = g_iPrefCurrLine;

  // Copy the lines up.
  for ( j = l_iCurLineNum; j<g_iPrefLineCount; j++)
  {
    fPrefCopyALine(l_formDisplay, j+1, j);
  }
  // Clear out the last line
  fGetInputWidget(l_formDisplay, "txtAliasName", g_iPrefLineCount).value = "";
  fGetInputWidget(l_formDisplay, "txtProjNum", g_iPrefLineCount).value = "";
  fGetInputWidget(l_formDisplay, "hidProjID", g_iPrefLineCount).value = "";
  fGetInputWidget(l_formDisplay, "txtTaskNum", g_iPrefLineCount).value = "";
  fGetInputWidget(l_formDisplay, "hidTaskID", g_iPrefLineCount).value = "";

  var l_iNumOptions = fGetInputWidget (l_formDisplay, "popType",
    g_iPrefLineCount).options.length;
  // Reset pop list, go through all options
  for ( var i=0; i< l_iNumOptions ; i++)
  {
    // Set to false if the option is selected
    if (fGetInputWidget(l_formDisplay, "popType", 
                   g_iPrefLineCount).options[i].selected ) {

      fGetInputWidget(l_formDisplay, "popType", 
                   g_iPrefLineCount).options[i].selected = false;
    }
  }
  fGetInputWidget(l_formDisplay, "popType", 
    g_iPrefLineCount).options[0].selected = true;
}
  

//--------------------------------------------------------------
// function: fPrefOnClickCopyLine
// Description: Handler for clicking on the Copy Line button.
// 
//--------------------------------------------------------------

function fPrefOnClickCopyLine(p_framTarget) {

  var l_formDisplay = p_framTarget.document.formPrefBodyContent;
  var l_iNewLineNum;

  // Get the current selected line.
  var l_iCurLineNum = g_iPrefCurrLine;
 
  // Find the next empty line
  for ( l_iNewLineNum=1; l_iNewLineNum < g_iPrefLineCount + 1; l_iNewLineNum++ ) {
    if ( fGetInputWidget(l_formDisplay, "txtAliasName", 
      l_iNewLineNum).value.length == 0 )  // We found the next empty alias
    { 
      break;
    }
  }
  if ( l_iNewLineNum <= top.g_iPrefLineCount ) // There is an empty line
  {
    fPrefCopyALine(l_formDisplay, l_iCurLineNum, l_iNewLineNum);   
  }
  else
  {
    // There is no room, need to add lines.
    fPrefLineOverflow(p_framTarget.parent, l_formDisplay, l_iCurLineNum, 
      g_objNewPref);
  }
}


//--------------------------------------------------------------
// function: fPrefOnClickAddLines
// Description: Handler for clicking on the Add Lines button.
// 
//--------------------------------------------------------------

function fPrefOnClickAddLines(p_framMainBody) {
  var l_formDisplay = p_framMainBody.framBodyContent.document.formPrefBodyContent;

  // Save data structure
  fPrefUpdateStructPref(p_framMainBody, g_objNewPref);
  
  // Add 4 more lines
  top.g_iPrefLineCount += C_iMinAliasLines;

  // Redraw the lines
  top.fDrawPrefBodyContent(p_framMainBody.framBodyContent);

  fPrefUpdateDisp(p_framMainBody, true);
}


//--------------------------------------------------------------
// function: fPrefLineOverflow
// Description: Takes care of overflowing alias lines.
// 
//--------------------------------------------------------------
function fPrefLineOverflow(p_framMainBody, p_formDisplay, p_iCurLineNum,
   p_objPref) {


  // Save data structure
  fPrefUpdateStructPref(p_framMainBody, g_objNewPref);

  // Add new line to the data structure
 
  var strAliasName = fTrimEndingSpace(fGetInputWidget(p_formDisplay, 
       "txtAliasName", p_iCurLineNum).value); 
  var iProjID = fTrimEndingSpace(fGetInputWidget(p_formDisplay, 
       "hidProjID", p_iCurLineNum).value);
  var strProjNum = fTrimEndingSpace(fGetInputWidget(p_formDisplay, 
       "txtProjNum", p_iCurLineNum).value);
  var strTaskNum = fTrimEndingSpace(fGetInputWidget(p_formDisplay, 
       "txtTaskNum", p_iCurLineNum).value);
  var iTaskID =  fTrimEndingSpace(fGetInputWidget(p_formDisplay,
       "hidTaskID", p_iCurLineNum).value);
  var iTypeIndex;

  for ( j=0; j<fGetInputWidget(p_formDisplay, 
        "popType", p_iCurLineNum).options.length; j++)
  {
    if ( fGetInputWidget(p_formDisplay, 
      "popType", p_iCurLineNum).options[j].selected )
    {
      // Options[j].value should be j.
      iTypeIndex = j;
      break;
    }
  }
     
//     alert(i+ " alias: \"" + strAliasName + "\" iProjID: \"" + iProjID + "\" iTaskID: \"" + iTaskID + "\" strProjNum: \"" + strProjNum + "\" strTaskNum: \"" + strTaskNum + "\"");
 
  // This is a new alias
  p_objPref.mGetobjAliases().mAddAlias(
            strAliasName, 
            iProjID, 
            strProjNum, 
            iTaskID, 
            strTaskNum,
            g_objTypes.arrType[iTypeIndex]); // This should be good as we have a dummy selection and will be selected by default 

  // Add 4 more lines
  top.g_iPrefLineCount += C_iMinAliasLines;

  // Redraw the lines
  top.fDrawPrefBodyContent(p_framMainBody.framBodyContent);

  fPrefUpdateDisp(p_framMainBody, true);
}


//--------------------------------------------------------
// function: fPrefSave
// Description: Saves the preferences when the OK button
// is clicked.
//--------------------------------------------------------

function fPrefSave(p_framMainBody, p_objOrigPref, p_objCurPref)
{
  var strAliasName, strProjNum, iProjID, strTaskNum, iTaskID;
  var l_forSubmit = p_framMainBody.framBodyContent.document.formSubmitPref;

  // Don't save if there are no changes
  if (!fPrefChanged(top.framMainBody, g_objPref))
    fPrefCancel (self);

  // Update data structure
  fPrefUpdateStructPref(p_framMainBody, p_objCurPref);

  //Validate the current Alias (client-side).
  g_objErrors = new Errors(); // Reset g_objErrors to eliminate previous errors.

  fPrefValidate(p_objCurPref, g_objErrors);
  // Serialize the preference object and submit
  
  if ( g_objErrors.miGetNumOfErrors() > 0 )
  {
    fOpenErrorWin(g_objErrors, 
      "top.framMainBody.framBodyHeader.document.formPrefBodyHeader",
      "top.framMainBody.framBodyContent.document.formPrefBodyContent");
  }
  else 
  {
    fPrefIdentifyDeletedAlias(p_objOrigPref.mGetobjAliases(), 
      p_objCurPref.mGetobjAliases());
   
    l_forSubmit.hidPrefString.value = p_objCurPref.mstrSerialize();
//    alert("save: " + p_objCurPref.mGetobjDefaultApprover().mGetstrName() + ", " + p_objCurPref.mGetobjDefaultApprover().mGetiID());
//    alert("String:"+l_forSubmit.hidPrefString.value);
    l_forSubmit.submit();
    
    if ( top.fPrefIsHeaderChanged(p_objOrigPref, p_objCurPref) && ! top.g_bIsStandAlone )
    {
      top.g_framPref.alert( g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_HEADER_CHANGE") );
    }
  }
}


//--------------------------------------------------------
// function: fPrefValidate
// Description: Validates general preferences
// 
// Raise an alert  upon error for now.
// To be integrated with Anitha's Error stuff.
//--------------------------------------------------------

function fPrefValidate(p_objCurPref, p_objError)
{
  // Need to clear error
  var arrstrAliasNames = new Array();
  var strExpType = "";
  var strProjNum = "";
  var strTaskNum = "";
  
  if ( ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iSUNDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iMONDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iTUESDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iWEDNESDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iTHURSDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iFRIDAY) &&
       ! p_objCurPref.mGetobjWorkDays().bIsWorkDay(C_iSATURDAY) )
  {
    p_objError.mobjAddGeneralError( g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_NO_WORKDAY", C_strPREFERENCES) );
  }

  for ( i=0; i<p_objCurPref.mGetobjAliases().miGetNumAliases(); i++)
  {
    // Check if line Alias name in line i already exists
    if ( p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrName().length == 0 ) // Empty line  
    {  
      strProjNum = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrProjectNum();
    strExpType = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetobjType().mGetstrName();
      if ( strProjNum.length > 0 || strExpType.length > 0 )
      {
        p_objError.mobjAddLineError(i+1, 
          g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_ALIAS_MISSING"),
          C_strAlias);
      }
      continue; 
    }
    for ( j=0; j<arrstrAliasNames.length; j++ )
    {
       if ( arrstrAliasNames[j] == p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrName() )
       {
          // This alias name already exists
          p_objError.mobjAddLineError( i+1, 
            g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_ALIAS_DUP"),
            C_strAlias);
          break;  // Only need to pop message for this alias once
       }
    }
    // Still add this in so the line number between i and j are sync-ed.
    arrstrAliasNames[arrstrAliasNames.length] = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrName();
    
    strProjNum = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrProjectNum();
    strExpType = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetobjType().mGetstrName();
    strTaskNum = p_objCurPref.mGetobjAliases().mGetarrAlias()[i].mGetstrTaskNum();

    // Check for Task without project
    if ( strProjNum.length == 0 &&  strTaskNum.length > 0 )
    {
      p_objError.mobjAddLineError(i+1, 
        g_objFNDMsg.mstrGetMsg("PA_WEB_TASK_LOV_NO_PROJECT"), 
        C_strProject); 
    }

    // Check if project or type are given
    if ( strProjNum.length == 0 &&  strExpType.length == 0 )
    {
      p_objError.mobjAddLineError(i+1, 
        g_objFNDMsg.mstrGetMsg("PA_WEB_PROJECT_TASK_INVALID"), 
        C_strProject); 
    }
  }
}


//---------------------------------------------------------
// function:    fCompactAlias
// Description: 
//---------------------------------------------------------

function fCompactAlias(p_objAliases)
{
  for ( i=0; i< p_objAliases.miGetNumAliases(); i++)
  {
     if ( p_objAliases.mGetarrAlias()[i].mGetbIsDeleted() ) // deleted alias
     {
       p_objAliases.mRemoveAlias( p_objAliases.mGetarrAlias()[i].mGetstrName());
     }
  }
  for ( i=0; i< p_objAliases.miGetNumAliases(); i++)
  {
    if ( p_objAliases.mGetarrAlias()[i].mbIsEmpty())  // Blank Alias
    {
      p_objAliases.mRemoveAlias( p_objAliases.mGetarrAlias()[i].mGetstrName());
    }
  }
}

function fPrefIsHeaderChanged(p_objOrigPref, p_objCurPref)
{
  return ( p_objOrigPref.mGetstrDefaultComment() != p_objCurPref.mGetstrDefaultComment() ||  p_objOrigPref.mGetobjDefaultApprover().mGetstrName() != p_objCurPref.mGetobjDefaultApprover().mGetstrName() );
}

function fPrefIdentifyDeletedAlias(p_objOrigAlias, p_objCurAlias)
{
  for (var index = 0; index < p_objOrigAlias.miGetNumAliases(); index++)
  {
    var strAliasName = p_objOrigAlias.mGetarrAlias()[index].mGetstrName();

    if ( p_objCurAlias.mobjFindAlias(strAliasName) == null )
    {  // Alias not find in Current array. Put it back and mark it as deleted.
         p_objCurAlias.mAddAlias(
           p_objOrigAlias.mGetarrAlias()[index].mGetstrName(), // Alias name
           p_objOrigAlias.mGetarrAlias()[index].mGetiProjectID(), // Project ID
           p_objOrigAlias.mGetarrAlias()[index].mGetstrProjectNum(), // Project Num
           p_objOrigAlias.mGetarrAlias()[index].mGetiTaskID(),
           p_objOrigAlias.mGetarrAlias()[index].mGetstrTaskNum(),
           p_objOrigAlias.mGetarrAlias()[index].mGetobjType());
         // Mark this one as deleted
//          alert("Mark this one deleted:" + eval(p_objCurAlias.miGetNumAliases()-1));
         p_objCurAlias.mGetarrAlias()[p_objCurAlias.miGetNumAliases()-1].mSetbIsDeleted(true);
     }
  }
}


//---------------------------------------------------------
// function:    fPrefInit
// Description: 
// Initialize global variables and draw preference window
//---------------------------------------------------------

function fPrefInit(p_framTarget, p_bIsStandAlone)
{
  // When open preference window in a stand alone mode, type object array
  // will be defined at the top. 
  if ( p_bIsStandAlone )
  {
    p_framTarget.g_bIsHelpWinOpenOnEntry = false;
    p_framTarget.g_framCodeLoc = top;
    p_framTarget.g_framPref = top;   // User preference window is top window

    fDrawFrameSetPage(p_framTarget, "auto", null, null, null, C_strPREFERENCES);
      
  }
}


//---------------------------------------------------------
// function:    fPrefControlSSToCSContinue
// Description: Controls the behavior of the window after
// saving the user preference on the server-side.
//---------------------------------------------------------

function fPrefControlSSToCSContinue(p_objCurPref, p_objErrors, p_bIsStandAlone, p_bIsHelpWinOpenOnEntry, p_winDisplay) 
{
//    alert("Task ID: \""+ p_objCurPref.mGetobjAliases().mGetarrAlias()[0].mGetiTaskID() + "\"");
  var l_framMainBody = p_winDisplay.g_framPref.framMainBody;

  // Check if there are any errors
  if ( p_objErrors.miGetNumOfErrors() > 0 )
  {
    fOpenErrorWin(p_objErrors, 
      "top.framMainBody.framBodyHeader.document.formPrefBodyHeader",
      "top.framMainBody.framBodyContent.document.formPrefBodyContent");
    fCompactAlias(p_objCurPref.mGetobjAliases());
    fDrawPrefBodyFrame(l_framMainBody);
    fPrefUpdateDisp(l_framMainBody, false);
  }
  else
  {
    // Close error window
    if (top.g_winError != null) {
      if (!top.g_winError.closed) {
        //top.fModalWinClose(top.g_winError);
        top.g_winError.close();
      }
    }
    window.close();
  }
}


//  --------------------------------------------------
//  Function: fDrawTableTitle
//  Description: Draws the title of the multi-row table. 
//
//  --------------------------------------------------
function fDrawTableTitle(p_framTarget){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_USER_PREFERENCE_BODY");

  p_framTarget.document.writeln('      <TABLE cellpadding=0 cellspacing=0 border=0 width=100%>');
  p_framTarget.document.writeln('      <TR>');
  p_framTarget.document.writeln('        <TD>&nbsp</TD>');

  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>&nbsp</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' +
    l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREF_ALIAS_NAME").mGetstrLabel() + '</TD>');
   
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + 
    l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT_NUMBER").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + 
    l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK_NUMBER").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + 
    l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetstrLabel() + '</TD>');

  p_framTarget.document.writeln('      </TR>');
}


//  --------------------------------------------------     
//  Function: fDrawPrefBodyFrame
//  Description: This function draws the user preferences
//              body frame. It will call the following   
//              functions: fDrawPrefBodyHeader, 
//                         fDrawPrefBodyContent,   
//                         fDrawPrefBodyButtons.                           
//  Author: kwidjaja                         
//  --------------------------------------------------     

function fDrawPrefBodyFrame(p_framTarget) {
  //alert ("Entering fDrawPrefBodyFrame");
  fDrawPrefBodyButtons (p_framTarget.framBodyButtons);
  fDrawPrefBodyHeader (p_framTarget.framBodyHeader);
  fDrawPrefBodyContent (p_framTarget.framBodyContent);
}


//  --------------------------------------------------     
//  Function: fDrawPrefBodyHeader
//  Description: This function draws the contents of  
//              the body header frame.
//  Author: kwidjaja                         
//  --------------------------------------------------     

function fDrawPrefBodyHeader(p_framTarget) 
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_USER_PREFERENCE_BODY");
  var strProfOpt = g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER");

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' +
    g_strHTMLLangPath + 'pawscabo.css"></LINK>');
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' +
    g_strHTMLLangPath + 'pawstime.css"></LINK>');
  p_framTarget.document.writeln('</HEAD>');

  p_framTarget.document.writeln('<BODY class=PANEL>');

  p_framTarget.document.write('<TABLE BORDER=0 CELLPADDING=0 ');
  p_framTarget.document.writeln('CELLSPACING=0 WIDTH=100%>');

  // Draw white space on the left border   
  p_framTarget.document.writeln('  <TR><TD WIDTH=5 ROWSPAN=4>&nbsp</TD>');

  p_framTarget.document.write('    <TD><TABLE BORDER=0 CELLPADDING=0 ');
  p_framTarget.document.writeln('CELLSPACING=0 WIDTH=98%>');

  // Draw the Icon
  p_framTarget.document.writeln('  <TR><TD ROWSPAN=3 VALIGN="TOP">');
  p_framTarget.document.writeln('      <IMG SRC="' + g_strImagePath + 
                                'PALINFO.gif" ALIGN="absmiddle"> </TD>'); 

  // Draw white space
  p_framTarget.document.writeln('      <TD WIDTH=20 ROWSPAN=3>&nbsp</TD>');

  // Draw the labels
  p_framTarget.document.write('<TD><TABLE BORDER=0 CELLPADDING=0 ');
  p_framTarget.document.writeln('CELLSPACING=0 WIDTH=100%>');

  // Draw the User Preferences label
  p_framTarget.document.write('<TD CLASS=INFOBLACK><B>');
  p_framTarget.document.writeln(l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREFERENCES").mGetstrLabel() + '</B></TD></TR>');

  
  // Draw a thin line
  p_framTarget.document.writeln('<TR><TD><IMG SRC="' + g_strImagePath +
                                'FNDPX3.gif" WIDTH=100% HEIGHT=2></TD></TR>'); 
  
  // Write Instructions Text
  p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK>');
  p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_USER_PREF_INFO")); 

  p_framTarget.document.writeln('</TD></TR></TABLE>');
  p_framTarget.document.writeln('</TD></TR></TABLE></TD></TR>');

  // Start form 
  p_framTarget.document.writeln('<FORM NAME="formPrefBodyHeader">');

  // Hidden fields for this form
  p_framTarget.document.writeln('<input type="hidden" name="hidDefApprID">');
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_APPR_NAME">');
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_APPR_ID">');

  p_framTarget.document.writeln('<TR><TD><TABLE CELLPADDING=0 CELLSPACING=0>');

  // Draw Overriding Approver box if profile option is set
  if ( strProfOpt == "Y")
  {
    p_framTarget.document.writeln('<TR>'); 
    p_framTarget.document.write('  <TR><TD class=PROMPTBLACKRIGHT width=200 ');
    p_framTarget.document.writeln(' nowrap>');
    p_framTarget.document.writeln(l_objCurRegion.mobjGetRegionItem("PA_WEB_OVERRIDING_APPROVER").mGetstrLabel() + '</TD>');

    // Draw box for default approver 
    p_framTarget.document.write('<TD><input type="text" ');
    p_framTarget.document.write('name="txtDefApprName" value="" size="'); 
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_OVERRIDING_APPROVER").mGetiLength()); 
    p_framTarget.document.writeln('"  onChange="javascript:top.g_framCodeLoc.fPrefOnChangeDefAppr(document.formPrefBodyHeader)">');
    p_framTarget.document.write(' <a href="Javascript:top.g_framCodeLoc.fPrefOnClickDefApprLOV();"');
    p_framTarget.document.write(' onMouseOver="window.status=\'');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
    p_framTarget.document.writeln('\';return true">');
    p_framTarget.document.writeln('<img src="' + g_strImagePath+'FNDILOV.gif"');
    p_framTarget.document.write('alt="');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
    p_framTarget.document.write('" width="16" height="18" border=0 ');
    p_framTarget.document.writeln('align="absmiddle"></a></TD></TR>');
  }

  // Default comment
  p_framTarget.document.write('<TR><TD class=PROMPTBLACKRIGHT width=200');
  p_framTarget.document.writeln(' nowrap valign="TOP">');
  p_framTarget.document.writeln(l_objCurRegion.mobjGetRegionItem("PA_WEB_HEADER_COMMENT").mGetstrLabel() + '</TD>');
            
  // text area for default comment
  p_framTarget.document.writeln(' <TD colspan=2 nowrap>'); 
  p_framTarget.document.write(' <textarea name="txaDefComment" rows=2 cols=');
  p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_HEADER_COMMENT").mGetiLength()); 
  p_framTarget.document.writeln(' wrap=VIRTUAL scrolling=no></textarea>');
  p_framTarget.document.writeln(' </TD></TR>');

  // Working days
  p_framTarget.document.writeln(' <TR><TD class=PROMPTBLACKRIGHT');
  p_framTarget.document.writeln(' valign="TOP" width="200">');
  p_framTarget.document.writeln(l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREF_WORKING_DAYS").mGetstrLabel());
  p_framTarget.document.writeln(' </TD>');
           
  // Days of the week
  p_framTarget.document.writeln(' <TD width="252">'); 
  p_framTarget.document.writeln(' <table BORDER=0 WIDTH="100%" cols=7>');
  p_framTarget.document.writeln('    <tr align="CENTER">'); 
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iSUNDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iMONDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iTUESDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iWEDNESDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iTHURSDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iFRIDAY] + '</B></td>');
  p_framTarget.document.writeln('<td valign="TOP" class=PROMPTBLACK><B>' 
    + g_arrstrWeekDayAbbr[C_iSATURDAY] + '</B></td></tr>');
   
  // Draw the checkboxes for the weekdays.
  p_framTarget.document.writeln('<tr align="CENTER">');
  p_framTarget.document.writeln('<td><input type="CHECKBOX" name="cbxSunday"></td> <td> <input type="CHECKBOX" name="cbxMonday">');
  p_framTarget.document.writeln('</td><td><input type="CHECKBOX" name="cbxTuesday">'); 
  p_framTarget.document.writeln('</td><td><input type="CHECKBOX" name="cbxWednesday"> </td><td>');
  p_framTarget.document.writeln(' <input type="CHECKBOX" name="cbxThursday"> ');
  p_framTarget.document.writeln('</td><td><input type="CHECKBOX" name="cbxFriday"> </td><td> ');
  p_framTarget.document.writeln('<input type="CHECKBOX" name="cbxSaturday"> ');

  p_framTarget.document.writeln('</tr></table></TD></TR>');
  p_framTarget.document.writeln('</TABLE></TD></TR></FORM>');

  // Label for Alias Setup
  p_framTarget.document.write('  <TR><TD class=PROMPTBLACK><B>');
  p_framTarget.document.writeln(l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREF_ALIAS_SETUP").mGetstrLabel());
  p_framTarget.document.writeln('</B></TD></TR>');
  p_framTarget.document.writeln('<TR><TD><IMG src="' + g_strImagePath + 
    'FNDPX3.gif" width=98% height=2></TD></TR>');

  p_framTarget.document.writeln('</TABLE>');
  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}


//  --------------------------------------------------     
//  Function: fDrawPrefBodyContent
//  Description: This function draws the contents of
//              the body content frame.
//  Author: kwidjaja                         
//  --------------------------------------------------     

function fDrawPrefBodyContent(p_framTarget) 
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_USER_PREFERENCE_BODY");
  var strProfOpt = g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER");

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' +
    g_strHTMLLangPath + 'pawscabo.css"></LINK>');
  p_framTarget.document.writeln('</HEAD>');

  p_framTarget.document.writeln('<BODY class=PANEL onFocus="javascript:top.fModalWinOnFocusPreventClick()">');

  p_framTarget.document.writeln('<FORM name="formPrefBodyContent">'); 

  // Put a table within another table to allow some gray space
  p_framTarget.document.write('<TABLE BORDER=0 CELLPADDING=0 ');
  p_framTarget.document.writeln('CELLSPACING=0 WIDTH=100%>');

  // Draw white space on the left border   
  p_framTarget.document.writeln('  <TR><TD WIDTH=5>&nbsp</TD><TD>');

  fDrawTableTopBorder(p_framTarget, "absmiddle");
  fDrawTableTitle(p_framTarget);


  // Determine number of Alias lines to display
  var l_iNumOfAliases = objAliases.arrAlias.length;

  // Make sure it can draw when there are more than 4 aliases.
  if (l_iNumOfAliases > top.g_iPrefLineCount) {
    top.g_iPrefLineCount = Math.ceil(l_iNumOfAliases / C_iMinAliasLines) 
      * C_iMinAliasLines;
  }
  
  // Loop through each displayed Alias line
  for ( var i=1; i<= top.g_iPrefLineCount; i++)
  {
    p_framTarget.document.write('<tr><td CLASS=TABLECELL>&nbsp;');

    // Draw the arrow picture, if any.
    if (i == g_iPrefCurrLine) {
      p_framTarget.document.writeln('<IMG NAME="imgCurrLineIndicator' + 
        i + '" SRC="' + g_imgArrow.src + '"></td>');  
    }
    else
    // Otherwise draw the blank picture.
    {
      p_framTarget.document.writeln('<IMG NAME="imgCurrLineIndicator' + 
        i + '" SRC="' + g_imgEmpty.src + '"></td>');
    }

    // Draw the alias number
    p_framTarget.document.writeln('<td CLASS=TABLECELL >' + i +'</td>'); 

    // Draw field for Name
    p_framTarget.document.write('<td nowrap CLASS=TABLECELL>');
    p_framTarget.document.writeln('<TABLE border=0><TR><TD CLASS=TABLECELL>');
    p_framTarget.document.write('<input type="text" maxlength=20 name="txtAliasName');
    p_framTarget.document.write(i + '" value="" size="');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREF_ALIAS_NAME").mGetiLength());
    p_framTarget.document.writeln('" onFocus="javascript:top.g_framCodeLoc.fPrefUpdateDispSelectedAlias('+i+', document.formPrefBodyContent);"></td> ');
    p_framTarget.document.writeln('</TR></TABLE></TD>');

    // Draw field for Project number
    p_framTarget.document.write('<td nowrap CLASS=TABLECELL>');
    p_framTarget.document.writeln('<TABLE border=0><TR><TD CLASS=TABLECELL>');
    p_framTarget.document.write('<input type="text" name="txtProjNum'+ i);
    p_framTarget.document.write('" value="" size="');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT_NUMBER").mGetiLength());
    p_framTarget.document.writeln('" onFocus="javascript:top.g_framCodeLoc.fPrefUpdateDispSelectedAlias('+i+', document.formPrefBodyContent);"');
    p_framTarget.document.write('onBlur="javascript:top.g_framCodeLoc.fPrefOnChangeProject(document.formPrefBodyContent, '+i+');">');
    p_framTarget.document.writeln(' <a href="javascript:top.g_framCodeLoc.fPrefOnClickProjectLOV('+i+');"');
    p_framTarget.document.write('onMouseOver="window.status=\'');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
    p_framTarget.document.write('\';return true">');
    p_framTarget.document.writeln('<img src="'+g_strImagePath+'FNDILOV.gif"');
    p_framTarget.document.write('alt="');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
    p_framTarget.document.writeln('" width="16" height="18" border=0 align="absmiddle"></a> </td>');
    p_framTarget.document.writeln('</TR></TABLE></TD>');

    // Draw field for Task
    p_framTarget.document.write('<td nowrap CLASS=TABLECELL>');
    p_framTarget.document.writeln('<TABLE border=0><TR><TD CLASS=TABLECELL>');
    p_framTarget.document.write('<input type="text" name="txtTaskNum'+i);
    p_framTarget.document.write('" value="" size="');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK_NUMBER").mGetiLength());
    p_framTarget.document.writeln('" onFocus="javascript:top.g_framCodeLoc.fPrefUpdateDispSelectedAlias('+i+', document.formPrefBodyContent);"');
    p_framTarget.document.write(' onBlur="javascript:top.g_framCodeLoc.fPrefOnChangeTask(document.formPrefBodyContent, '+i+');">');
    p_framTarget.document.writeln(' <a href="javascript:top.g_framCodeLoc.fPrefOnClickTaskLOV('+i+');"');
    p_framTarget.document.write('onMouseOver="window.status=\'');
    p_framTarget.document.write(l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
    p_framTarget.document.write('\';return true">');
    p_framTarget.document.writeln('<img src="'+g_strImagePath+'FNDILOV.gif"');
    p_framTarget.document.write('alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel());
   p_framTarget.document.writeln('" width="16" height="18" border=0 align="absmiddle" ></a></td>');
    p_framTarget.document.writeln('</TR></TABLE></TD>');

    // Draw field for Type
    p_framTarget.document.write('<td nowrap CLASS=TABLECELL>');
    p_framTarget.document.write('<select name="popType' + i);
    p_framTarget.document.write('" onFocus="javascript:top.g_framCodeLoc.fPrefUpdateDispSelectedAlias('+i + ', document.formPrefBodyContent);" >');
    for ( var j=0; j<g_objTypes.miGetNumTypes(); j++)
    {
      var l_strTypeName = g_objTypes.arrType[j].mGetstrName();
      p_framTarget.document.write('<option value='+j+'>');
      p_framTarget.document.writeln(l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()));
    }
    p_framTarget.document.writeln('</select></td>');

    p_framTarget.document.writeln('<input type="hidden" name="hidProjID' + i + '" value="">');
    p_framTarget.document.writeln('<input type="hidden" name="hidTaskID' + i + '" value="">');

    // Draw thin line
    p_framTarget.document.writeln('</tr><tr></tr>');
  } 

  p_framTarget.document.writeln('   </TABLE>');

  // Draw table bottom
  fDrawTableBottomBorder(p_framTarget, "absmiddle");

  // Close outer table
  p_framTarget.document.writeln(' </TD></TR></TABLE>');

  
  // Hidden fields for this form (to be used to store values from LOVs)
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_NUMBER">');
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_ID">');
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_NUMBER">');
  p_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_ID">');
  p_framTarget.document.writeln('</FORM>');
  
  // Draw hidden fields for submission
  p_framTarget.document.write('<form name="formSubmitPref" method=post ');
  p_framTarget.document.writeln('action="' + g_strSessBaseHRef + g_strSessDAD +
    '/PA_SELF_SERVICE_HANDLER_PVT.SavePreferences" > ');
  p_framTarget.document.writeln('<input type="hidden" name="hidPrefString"> '); 
  p_framTarget.document.writeln('</form>');                                     

  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}


//  --------------------------------------------------     
//  Function: fDrawPrefBodyButtons
//  Description: This function draws the contents of
//              the body button frame.
//  Author:     kwidjaja                         
//  --------------------------------------------------     

function fDrawPrefBodyButtons(p_framTarget) {
  var l_objCurRegion = g_objAKRegions.mobjGetRegion
    ("PA_WEB_USER_PREFERENCE_BODY");
                                                                         
  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');                                     
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' +
    g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('</HEAD>');

  p_framTarget.document.writeln('<BODY class="PANEL">');                   
  p_framTarget.document.writeln('<TABLE cellpadding=3 cellspacing=5 border=0>');

  p_framTarget.document.writeln('<TR VALIGN=TOP>');                            
  
  // Draw Clear Line button
  p_framTarget.document.writeln('<TD>');                            
  fDrawButton(p_framTarget, C_strROUND_ROUND,
    l_objCurRegion.mobjGetRegionItem('PA_WEB_CLEAR_LINE').mGetstrLabel(), 
    "top.g_framCodeLoc.fPrefOnClickClearLine(parent.framBodyContent)", 
    C_bBUTTON_ENABLED, C_strGRAYBG);             
  p_framTarget.document.writeln('</TD>');                            

  // Draw Copy Line button
  p_framTarget.document.writeln('<TD>');                            
  fDrawButton(p_framTarget, C_strROUND_ROUND,
    l_objCurRegion.mobjGetRegionItem('PA_WEB_COPY_LINE').mGetstrLabel(), 
    "top.g_framCodeLoc.fPrefOnClickCopyLine(parent.framBodyContent)", 
    C_bBUTTON_ENABLED, C_strGRAYBG);             
  p_framTarget.document.writeln('</TD>');                            

  p_framTarget.document.writeln('      <TD><IMG SRC="' + g_strImagePath + 
    'FNDPXG5.gif" width=25 height=1></TD>'); 

  // Draw Add More Lines button
  p_framTarget.document.writeln('<TD>');                            
  fDrawButton(p_framTarget, C_strROUND_ROUND,
    l_objCurRegion.mobjGetRegionItem('PA_WEB_ADD_LINES').mGetstrLabel(), 
    "top.g_framCodeLoc.fPrefOnClickAddLines(parent)", 
    C_bBUTTON_ENABLED, C_strGRAYBG);             
  p_framTarget.document.writeln('</TD>');                            
  
  p_framTarget.document.writeln('</TR></TABLE>');
                                                                                
  p_framTarget.document.writeln('</BODY>');                                    
  p_framTarget.document.writeln('</HTML>');                                    
  p_framTarget.document.close();
}

