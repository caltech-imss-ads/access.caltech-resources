/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSACKEJ.js 115.2 2001/05/14 15:19:40 pkm ship     $ */

function clearFields()
{
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SUPPLIER_SITE.value="";
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SR_SUPPLIER_SITE_ID.value="";
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SR_PO_NUMBER.value="";
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SR_DOC_TYPE.value="";
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SR_ACC_REQD_START_DATE.value="";
        top.criteria.document.POS_ACK_SEARCH.POS_ACK_SR_ACC_REQD_END_DATE.value="";
}

function SearchPOs()
{
        top.criteria.document.POS_ACK_SEARCH.submit();
}

function SubmitAcks()
{
	var msg   =  window.top.FND_MESSAGES["ICX_POS_ACK_SUBMIT"];
	var temp1 = confirm (msg);
        if (temp1 == true)
            top.result.document.POS_EDIT_POS.submit();
}


/*
function LoadCounter(p_start, p_end, p_total)
{
    var l_URL = top.scriptName + "/POS_ACK_SEARCH.COUNTER_FRAME?" +
                "p_first=" + p_start +
                "`&p_last=" + p_end +
                "`&p_total=" + p_total;

    top.counter.location.href = l_URL;

    var l_URL2 = top.scriptName + "/POS_ACK_SEARCH.ADD_FRAME?" +
                "p_total=" + p_total;

    top.add.location.href = l_URL2;
}
*/

function changePromDate(po_num)
{
  var winWidth = 800;
  var winHeight = 600;
  var winAttributes = "menubar=no,location=no,toolbar=no," +
                      "width=" + winWidth + ",height=" + winHeight +
                      ",screenX=" + (screen.width - winWidth)/2 +
                      ",screenY=" + (screen.height - winHeight)/2 +
                      ",resizable=yes,scrollbars=yes";

  var scriptName = top.getTop().scriptName;
  var url = scriptName + "/POS_UPD_DATE.SEARCH_PO";
  top.getTop().openModalWindow(url, "test", winAttributes, winWidth, winHeight);
}
