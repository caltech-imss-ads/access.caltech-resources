<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- $Header: czclient.xsl 115.6 2000/12/10 16:10:50 pkm ship     $-->
<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="update_ui">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="control">
    	<xsl:if test="not(@operation)">
		<xsl:apply-templates select="layout/layout_info"/>
		<!-- if a value is specified in the control, generate a setProperty command here -->
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@operation='insert'">
                <xsl:text>setProperty(</xsl:text>
	        <xsl:value-of select="./content/@ui_obj_id"/>
	        <xsl:text>, "</xsl:text>
	        <xsl:value-of select="./content/@value"/>
	        <xsl:text>", "</xsl:text>
	        <xsl:value-of select="./content/@target"/>
	        <xsl:text>")</xsl:text>
	        <xsl:text>
	        </xsl:text>
		<xsl:apply-templates select="layout/layout_info"/>
	</xsl:if>
	<xsl:if test="@operation='create'">
		<xsl:text>construct("</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>", </xsl:text>
		<xsl:value-of select="../content/@ui_obj_id"/>
		<xsl:text>, "</xsl:text>
		<xsl:value-of select="layout/ctl_type/text()"/>
		<xsl:text>", "</xsl:text>
		<xsl:value-of select="content/@value"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>

		<xsl:apply-templates select="layout/layout_info"/>

		<xsl:text>create(</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>)</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@operation='navigate'">
		<xsl:text>navigate(</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>)</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@operation='delete'">
		<xsl:text>destroy(</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>)</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>destruct(</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>)</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@operation='initialize'">
		<xsl:text>initialize(</xsl:text>
		<xsl:value-of select="content/@ui_obj_id"/>
		<xsl:text>)</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:apply-templates select="control"/>
</xsl:template>

<xsl:template match="layout_info">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../../content/@ui_obj_id"/>
	<xsl:text>, "</xsl:text>
	<xsl:value-of select="@name"/>
	<xsl:text>", "</xsl:text>
	<xsl:value-of select="@value"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

</xsl:stylesheet>