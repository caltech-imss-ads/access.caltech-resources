<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : pyusotfw.js
// Description : This file contains all the common JavaScript routines
//               used in the Online Tax Forms module.
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------- ----------   ------- ------- ---------------------------------
// dscully     1-MAR-2000 		    created.
//
// dscully     24-MAR-2000  115.0	    created from 11.0 version	
// dscully     07-APR-2000  115.1 	    updated on_next_gif method
// ==========================================================================
// $Header: pyusotfw.js 115.1 2000/04/11 18:29:16 pkm ship       $


// ------------------- Global Variables -------------------

// Detect  browser type and version
var Nav4 = ((navigator.appName == "Netscape") && 
            (parseInt(navigator.appVersion) == 4));
var browserVer = parseInt(navigator.appVersion);


// Place holder for sub window.
var subWindow;

var Win32;

if (Nav4) {
  Win32 = ((navigator.userAgent.indexOf("Win") != -1) && 
           (navigator.userAgent.indexOf("Win16") == -1));
} else {
  Win32 = ((navigator.userAgent.indexOf("Windows") != -1) && 
           (navigator.userAgent.indexOf("Windows 3.1") == -1));
}

// ------------------- Function Definitions -------------------

// Function which does nothing. Used in print function.
function do_nothing () {
  return true;
}


function print_gif_onClick () {
  // Single frame printing is not available for Mac.
  if (Win32) {
    if (Nav4) {
      top.container_middle.print();
    }
    else {
      // traps all script error messages hereafter until page reload
      window.onerror = do_nothing();
      // make sure desired frame has focus
      wind.focus();
      // change second parameter to 2 if you don't want the print dialog to appear
      IEControl.ExecWB(6, 1);
      }
  }
  else {
    alert (FND_MESSAGES['HR_CANNOT_PRINT_WEB']);
  }
}


// This function will reload the the container middle frame.
function reload_gif_onClick () {
  top.container_middle.history.go(0);
}


// This function will stop reloading the the container middle frame.
function stop_gif_onClick () {
  if (Nav4) {
    void top.container_middle.stop();
  }
  else if (Win32) {
    top.container_middle.focus();
    // requires same <OBJECT> as printing
    IEControl.ExecWB(23, 0);
  }
}


// This function will popup the help window.
var helpWindow;
function help_gif_onClick () {
 var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  p_url = '/OA_HTML/US/pyusothp.htm';
  p_scrolling='Y';

 if (!helpWindow||helpWindow.closed){
        helpWindow=window.open(p_url,"","status=yes,menubar=yes,scrollbars=yes,toolbar=yes,location=yes,resizable=yes,alwaysRaised=yes,HEIGHT=500,WIDTH=500");
       helpWindow.opener=window;
 }
 else {
     helpWindow.focus();
 }
}



// Function to generate a blank page with dark blue color.
function get_dark_blue_page () {
  return "<HTML><BODY BGCOLOR=#336699></BODY></HTML>"
}

// Function to generate a blank page with gray color.
function get_gray_page () {
  return "<HTML><BODY BGCOLOR=#CCCCCC></BODY></HTML>"
}

// Some of the core routines look for a function checkModal
// which we create as a null
function checkModal () {
  null;
}

// This function will clear the container bottom frame so that buttons are
// not displayed when processing is going on in the container middle frame.
function clear_container_bottom_frame () {
  parent.container_bottom.document.open();
  parent.container_bottom.document.write (
    "<HTML>" +
    "<HEAD>" +
    "</HEAD>" +
    "<BODY BGCOLOR=#336699>" +
    "  <TABLE  WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>" +
    "  <TR BGCOLOR=#CCCCCC>" +
    "    <TD ALIGN=LEFT><IMG SRC=" + FND_IMAGES['FNDCTBL'] + " BORDER=0></TD>" +
    "    <TD WIDTH=100%><IMG SRC=" + FND_IMAGES['FNDPXG5'] + " BORDER=0></TD>" +
    "    <TD ALIGN=RIGHT><IMG SRC=" + FND_IMAGES['FNDCTBR'] + " BORDER=0></TD>" +
    "  </TR>" +
    "</TABLE>" +
    "</BODY>" +
    "</HTML>"
  );
  parent.container_bottom.document.close();
}

// Some of the core routines look for a procedure blank_bottom_frame
// so we wrape the clear... function
function blank_container_bottom_frame() {
	clear_container_bottom_frame();
}


// This function will reload the the container middle frame.
function reload_gif_onClick () {
  top.container_middle.history.go(0);
}


// This function will stop reloading the the container middle frame.
function stop_gif_onClick () {
  if (Nav4) {
    void top.container_middle.stop();
  }
  else if (Win32) {
    top.container_middle.focus();
    // requires same <OBJECT> as printing
    IEControl.ExecWB(23, 0);
  }
}

// Function to refresh container_top and container_bottom frames.
function refresh_frames (p_container_top_frame, p_container_bottom_frame) {
  if (p_container_top_frame) {
    if (top.container_top.location.href != p_container_top_frame) {
      top.container_top.location.href = p_container_top_frame;
    }
  }
  if (p_container_bottom_frame) {
    if (top.container_bottom.location.href != p_container_bottom_frame) {
      top.container_bottom.location.href = p_container_bottom_frame;
    }
  }
}


// Function to check whether the given string is blank.
function is_blank (str) {
  for (var i = 0; i < str.length; i++) {
    var c = str.charAt(i);
    if ((c != ' ') && (c != '\n') && (c != '\t'))
      return false;
  }
  return true;
}


//Required to open a sub window
function openSubWindow(p_url, p_width, p_height, p_scrolling) {
  var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  if (p_scrolling == "Y")
    {
     scrollbar_attr = ",scrollbars=yes";
    }
  else
    {
     scrollbar_attr = ",scrollbars=no";
    }
    
  if (!subWindow || subWindow.closed) {
    // center on the main window
    openleft = parseInt(window.screenX + ((window.outerWidth - p_width) / 2));
    opentop = parseInt(window.screenY + ((window.outerHeight - p_height) / 2));
    var attr = "screenX=" + openleft + ",screenY=" + opentop;
    attr += ",resizable=yes,width=" + p_width + ",height=" + p_height +
            scrollbar_attr;

    subWindow = window.open(p_url, 'sub_window', attr);
    subWindow.opener = window;
  }
  else {
    subWindow.focus();
  }
}

// Roll over image functions for onMouseOver and onMouseOut JavaScript events.
function set_onMouse_gif (p_document_name, p_image_name, p_gif_file) {
  if (browserVer >= 3 ) {
    eval(p_document_name + '.' + p_image_name + '.src=' + '"' + p_gif_file+'"');
  }
  else {
    return true;
  }
}

// This function will reload the the container middle frame.
function reload_gif_onClick () {
  top.container_middle.history.go(0);
}

// The following function is invoked from the
// header frame when the user clicks on the
// ''Main Menu'' gif in the toolbar.
function main_menu_gif_onClick () {
      top.location.href = 'hr_util_disp_web.hr_main_menu_page';
    }

// Function to handle clicking of the cancel button
function cancel_gif_onClick(overview) {
      if(confirm(top.FND_MESSAGES['HR_EXIT_WEB'])) {
	clear_container_bottom_frame();
	parent.container_middle.location.href = overview; }
}

//Function to handle clicking next on the update page
function next_gif_onClick() {
      clear_container_bottom_frame();
      parent.container_middle.document.main_form.p_process.value = top.p_process;
      parent.container_middle.document.main_form.p_itemtype.value = top.p_itemtype;
      parent.container_middle.document.main_form.submit();
}

//Function to handle clicking back on the review page
function back_gif_onClick() {
      clear_container_bottom_frame();
      parent.container_middle.location.href = parent.container_middle.back_url;
}

//Function to handle clicking update on the overview page
function update_gif_onClick() {
      clear_container_bottom_frame();
      parent.container_middle.location.href = parent.container_middle.update_url;
}

//Function to handle clicking reset on update page
function reset_gif_onClick() {
      parent.container_middle.document.main_form.reset();
}

//Function to handle clicking submit on the review page
function submit_gif_onClick() {
      clear_container_bottom_frame();
      parent.container_middle.location.href = parent.container_middle.submit_url;
}

//Function to handle clicking return to overview on confirm page
function return_gif_onClick(url) {
	clear_container_bottom_frame();
	parent.container_middle.location.href = url; 
}

<!-- Done hiding from browsers which cannot handle JavaScript. -->











