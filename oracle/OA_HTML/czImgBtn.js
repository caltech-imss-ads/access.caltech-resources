/* $Header: czImgBtn.js 115.18 2001/07/13 09:53:18 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash, K MacClay  Created.                     |
 |                                                                           |
 +===========================================================================*/

function ImageButton()
{
  this.parentConstructor = Base;
  this.parentConstructor();
  this.srcs = new Array();
  this.states = 0;
  this.stretch = false;
  this.toolTipText = '';

  this.addListener('mousemoveCallback',this);
  this.addListener('mouseupCallback',this);
  this.addListener('mousedownCallback',this);
}

function ImageButton_setImages()
{
  for(var i=0; i<arguments.length;i++) {
    if(arguments[i]) {
      this.srcs[i] = arguments[i];
    }
  }
  this.src = this.srcs[0];	
}


function ImageButton_setSrc(s)
{
  if(s) 
    this.src = s;
  else 	
    return;

  if(this.launched && this.getHTMLObj()) {
    if (ns4)
      this.block.document.images[0].src = s;
    else if (ie4) 	
      this.block.all['IMG-' + this.name].src = s;
  }
}

function ImageButton_toggle(stateNum){

  if(stateNum!=null) {
    if (this.state == stateNum) 
      return;	
    else 
      this.state = stateNum;
    
    this.setSrc(this.srcs[this.state]);
  }
}

function ImageButton_setImgOnSrc(imgOnSrc)
{
  this.imgOnSrc = imgOnSrc;
}

function ImageButton_setImgOffSrc(imgOffSrc)
{
  this.imgOffSrc = imgOffSrc;	
  this.setSrc(imgOffSrc);
}

function ImageButton_setToolTipText(toolTipText)
{
  this.toolTipText=toolTipText;
  if(this.launched && this.getHTMLObj()) {
    if (ns4)
      this.block.document.images[0].alt = toolTipText;
    else if (ie4) 	
      this.block.all['IMG-' + this.name].alt = toolTipText;
  }    
}

function ImageButton_innerRender()
{
  var sBuffer;
  var sBuffTerm = '';
  if ((this.type == 'button') || (this.active && ie4) || 
      (this.sclBtn && ie4)) {
    sBuffer = '<A href="javascript:void(0)"';
    sBuffer += ' NAME="A-'+ this.objId + '"';
    if (this.imgTabindex)
      sBuffer += ' tabindex="' + this.imgTabindex + '"';
    else
      sBuffer += ' tabindex="-1"';

    eval(this.self +'=this');
    if (ns4)
      var e = "event";
    else
      var e = null;
    if (this.sclBtn && ie4) {
      //for handling scroll bar arrow buttons
      sBuffer += ' onkeydown="javascript:doOnKeyDown('+ e +',\'' + this.self + '\')"';
      sBuffer += ' onkeyup="javascript:doOnKeyUp('+ e +',\'' + this.self + '\')"';
    } else if ((this.type == 'button') || (this.active && ie4)) {
      //For handling active images like state icons
      sBuffer += ' onkeypress="javascript:doOnKeyPress('+ e +',\'' + this.self + '\')"';
    }
    if ((this.type == 'button') && ie4)
      this.addListener ('onKeyPressCallback', this);

    sBuffer += '><IMG ID=IMG-'+this.objId +' NAME=IMG-'+this.objId+' BORDER=0';
    sBuffTerm = '</A>';      
  } else
    sBuffer = '<IMG ID=IMG-'+this.objId +' NAME=IMG-'+this.objId;	

  if(this.src) 
    sBuffer+=' SRC="'+ this.src +'"';

  if (this.stretch)
    sBuffer += ' HEIGHT='+ this.height + ' WIDTH='+ this.width;

  if (this.toolTipText!='')
    sBuffer+=' ALT="'+this.toolTipText+'" ';

  sBuffer += '></IMG>'+sBuffTerm;

  return sBuffer;
}

function ImageButton_mousemoveCallback(e)
{
  var em = this.eventManager;
  if(em.msdown && (this.states > 0)) {
    if(ie4)
      e.returnValue = false;
    else
      return false;
  }
  return true;
}

function ImageButton_mousedownCallback(e)
{
  if ((ns4 && e.which!=1) || (ie4 && e.button!=1)) 
    return true;
  
  if(this.states>0) {
    this.out = false;
    if(this.states == 1) 
      this.currState = 1;
    else if(this.states >1) 
      this.currState = (this.state+1)%this.states;
    
    this.prevState = this.state;
    this.toggle(this.currState);
  }
  return true;
}

function ImageButton_mouseupCallback(e)
{
  if ((navigator.platform.indexOf("MacPPC") == -1 ) && ((ns4&& e.which!=1) || (ie4 && e.button!=1))) 
    return true;
  
  if(this.states>0) {
    var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
    var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;
    var absx = this.getLeft('abs');
    var absy = this.getTop('abs');
    var inside = false;
    inside = this.isInside(x,y,absy,absx+this.width,absy+this.height,absx);
    
    if(!inside) {
      this.toggle(this.prevState);
    } else {
      if(this.states == 1)
        this.toggle(0);
    }
  }
  this.notifyListeners('onClick', e, this);
  return true;
}

function ImageButton_onKeyPressCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  if (key == 13) {
    //IE window scrolls, this has to be there to prevent scrolling of the window
    if (ie4)
      e.returnValue = false;

    this.notifyListeners('onClick', e, this);
  }
  return true;
}

function ImageButton_setFocus()
{
  if (this.launched && this.getHTMLObj() && ie4) {
    if ((this.type == 'button') || this.active || this.sclBtn) {
      this.block.all['A-' + this.objId].focus();
    }
  }
}

function ImageButton_setTabindex(ind)
{
  this.tabindex = null;
  this.imgTabindex = ind;
}

function ImageButton_getTabindex()
{
  return this.imgTabindex;
}

//Extending from Base
HTMLHelper.importPrototypes(ImageButton,Base);

ImageButton.prototype.constructor 	 = ImageButton;
ImageButton.prototype.setSrc 		 = ImageButton_setSrc;
ImageButton.prototype.setToolTipText 	 = ImageButton_setToolTipText;
ImageButton.prototype.innerRender 	 = ImageButton_innerRender;
ImageButton.prototype.setImages 	 = ImageButton_setImages;
ImageButton.prototype.toggle 		 = ImageButton_toggle;
ImageButton.prototype.mousemoveCallback  = ImageButton_mousemoveCallback;
ImageButton.prototype.mouseupCallback 	 = ImageButton_mouseupCallback;
ImageButton.prototype.mousedownCallback  = ImageButton_mousedownCallback;
ImageButton.prototype.onKeyPressCallback = ImageButton_onKeyPressCallback;
ImageButton.prototype.setImgOnSrc 	 = ImageButton_setImgOnSrc;
ImageButton.prototype.setImgOffSrc 	 = ImageButton_setImgOffSrc;
ImageButton.prototype.setFocus           = ImageButton_setFocus;
ImageButton.prototype.setTabindex        = ImageButton_setTabindex;
ImageButton.prototype.getTabindex        = ImageButton_getTabindex;
