//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWRESTR.js                       |
//  |                                                |
//  | Description: Client-side Javascript functions  |
//  |              for Restoring Expense Report page |     
//  |                                                |
//  | History:     Created 03/20/2000                |
//  |              David Tong                        |
//  |                                                |
//  +================================================+
/* $Header: APWRESTR.js 115.8 2000/09/03 13:49:17 pkm ship     $ */


function fGenerateStyleCodes(targetFrame){
        var d = eval('targetFrame.document');
	d.writeln("<LINK REL=stylesheet TYPE=\"text/css\" href=\""+top.g_strCssDir+"apwscabo.css\">");

} // end of fGenerateStyleCodes()

function fDrawButtonsFrame(){
	var d = eval(top.framButtons.document);
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
	d.writeln("<HEAD>");
	d.writeln("<LINK REL=stylesheet TYPE=text/css href=\""+top.g_strCssDir+"apwscabo.css\">");
	d.writeln("</HEAD>");
	d.writeln("<BODY class=appswindow>");
	d.writeln("<table width=100% cellpadding=0 cellspacing=0 border=0>");
	d.writeln("<tr>");
	d.writeln("<td rowspan=3><img src="+top.g_strImgDir+"APWCGBL.gif></td>");
	d.writeln("<td colspan=3 bgcolor=#cccccc width=1000><img src="+top.g_strImgDir+"APWDBPXC.gif height=2></td>");
	d.writeln("<td rowspan=3><img src="+top.g_strImgDir+"APWCGBR.gif></td>");
	d.writeln("</tr>");
	
	d.writeln("</TABLE>");

	d.writeln("</body>");
	d.writeln("</html>");
	d.close();
	}

function fBlankPage(strColor,strText) {
	if (strText)
        return "<HTML dir=\""+top.g_strDirection+"\"><BODY BGCOLOR=#"+strColor+">"+strText+"</BODY></HTML>"
	else
	return "<HTML dir=\""+top.g_strDirection+"\"><BODY BGCOLOR=#"+strColor+"></BODY></HTML>"
}

function DeleteReport(ReportNum) {
      if(confirm(top.g_objMessages.mstrGetMesg("AP_WEB_DELETE_REPORT"))) {
        open(g_dcdName+"AP_WEB_RESTORE_PKG.DeleteReport?ReportID=" + ReportNum, "_top");
      }
}

function fGenHeader(V_ReportNum, V_ReportNumAlign, V_Employee, V_EmployeeAlign, V_ReportDate, V_ReportDateAlign, V_ReportReject, V_ReportRejectAlign, V_Currency, V_CurrencyAlign, V_ReportTotal, V_ReportTotalAlign, V_Purpose, V_PurposeAlign, V_Delete, V_DeleteAlign){
  var t = top.navigation.document;
      //t.writeln("<TR>");  
  //if (V_DisplayAuthorField == 'Y')
  //    t.writeln("<TD class=tableheader><DIV ALIGN="+V_EmployeeAlign+ ">"+ V_Employee+"</DIV></TD>");

      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_ReportNumAlign + ">" + V_ReportNum + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_ReportRejectAlign + ">" + V_ReportReject + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_ReportDateAlign + ">" + V_ReportDate + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_CurrencyAlign + ">" + V_Currency + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_ReportTotalAlign + ">" + V_ReportTotal + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_PurposeAlign + ">" + V_Purpose + "</DIV></TD>");
      t.writeln("<TD class=tableheader><DIV ALIGN=" + V_DeleteAlign + ">" + V_Delete + "</DIV></TD>");
     t.writeln("</TR>");	

}

function fGenRow(V_ReportNum, V_ReportNumAlign, V_Employee, V_EmployeeAlign, V_ReportDate, V_ReportDateAlign, V_ReportReject, V_ReportRejectAlign, V_Currency, V_CurrencyAlign, V_ReportTotal, V_ReportTotalAlign, V_Purpose, V_PurposeAlign, V_Delete, V_DeleteAlign, V_Style){

      var t = top.navigation.document;
      //t.writeln("<TR>");  
  //if (V_DisplayAuthorField)
  //    t.writeln("<TD class=tableheader><DIV ALIGN="+V_EmployeeAlign+">"+ V_Employee+"</DIV></TD>");
    
      t.writeln("<TD class=" + V_Style +"><DIV ALIGN=" + V_ReportNumAlign + "><FONT class=promptblack>" + V_ReportNum + "</FONT></DIV></TD>");
      t.writeln("<TD class=" + V_Style + "><DIV ALIGN=" + V_ReportRejectAlign + "><FONT class=promptblack>" + V_ReportReject + "</FONT></DIV></TD>");
      t.writeln("<TD class=" + V_Style + "><DIV ALIGN=" + V_ReportDateAlign + "><FONT class=promptblack>" + V_ReportDate + "</FONT></DIV></TD>");
      t.writeln("<TD class=" + V_Style + "><DIV ALIGN=" + V_CurrencyAlign + "><FONT class=promptblack>" + V_Currency + "</FONT></DIV></TD>");
      t.writeln("<TD class=" +V_Style + "><DIV ALIGN=" + V_ReportTotalAlign + "><FONT class=promptblack>" + V_ReportTotal + "</FONT></DIV></TD>");
      t.writeln("<TD class=" + V_Style +"><DIV ALIGN=" + V_PurposeAlign + "><FONT class=promptblack>" + V_Purpose + "</FONT></DIV></TD>");
       t.writeln("<TD class=" + V_Style + "><DIV ALIGN=" + V_DeleteAlign + "><FONT class=promptblack>" + V_Delete + "</FONT></DIV></TD>");
       t.writeln("</TR>");
}

function fGenDeleteReportTableEntry(ReportID){
   return "<A HREF=\"javascript:top.DeleteReport("+ ReportID + ")\"><IMG SRC=" + top.g_strImgDir + "APWIDELR.gif border=0></A>";
}



function fGenRestoreReportTableEntry(V_DCDName, PrefixedReportID,ActualReportID){
   return "<A HREF=\"" + top.g_dcdName + "AP_WEB_PARENT_PKG.fin_parent?P_ReportHeaderID=" +  ActualReportID + "\" target=\"_top\">" +  PrefixedReportID + "</A>";
}

function ReportList(){
	this.arrItems = new Array();
}

ReportList.prototype.mobjAddItem = mobjAddItem;
ReportList.prototype.mobjGetItem = mobjGetItem;
ReportList.prototype.miGetNumOfItems = miGetNumOfItems;

function mobjAddItem(p_strInvoiceNum, p_strExpReportID, p_strEmpName, p_bRejected, p_strWeekEndDate, p_strCurrency, p_strTotal, p_strDescription){
	this.arrItems[this.arrItems.length] = new ListItem(p_strInvoiceNum, p_strExpReportID, p_strEmpName, p_bRejected, p_strWeekEndDate, p_strCurrency, p_strTotal, p_strDescription);
}

function mobjGetItem(p_strExpReportID){
	for (var i=0;i<this.miGetNumOfItems;i++){
		if (this.arrItems[i].mstrGetExpReportID == p_strExpReportID){
			return this.arrItems[i];
		}
	}
	return null;

}

function miGetNumOfItems(){
	return this.arrItems.length;
}


function ListItem(p_strInvoiceNum, p_strExpReportID, p_strEmpName, p_bRejected, p_strWeekEndDate, p_strCurrency, p_strTotal, p_strDescription){
	this.strInvoiceNum = p_strInvoiceNum;
	this.strExpReportID = p_strExpReportID;
	this.strEmpName = p_strEmpName;
	this.bRejected = p_bRejected;
	this.strWeekEndDate = p_strWeekEndDate;
	this.strCurrency = p_strCurrency;
	this.strTotal = p_strTotal;
	this.strDescription = p_strDescription;
}

ListItem.prototype.mstrGetExpReportID = mstrGetExpReportID;
ListItem.prototype.mstrGetInvoiceNum = mstrGetInvoiceNum;
ListItem.prototype.mstrGetEmpName = mstrGetEmpName;
ListItem.prototype.mbGetRejected = mbGetRejected;
ListItem.prototype.mstrGetWeekEndDate = mstrGetWeekEndDate;
ListItem.prototype.mstrGetCurrency = mstrGetCurrency;
ListItem.prototype.mstrGetTotal = mstrGetTotal;
ListItem.prototype.mstrGetDescription = mstrGetDescription;

ListItem.prototype.mSetExpReportID = mSetExpReportID;
ListItem.prototype.mSetInvoiceNum = mSetInvoiceNum;
ListItem.prototype.mSetEmpName = mSetEmpName;
ListItem.prototype.mSetRejected = mSetRejected;
ListItem.prototype.mSetWeekEndDate = mSetWeekEndDate;
ListItem.prototype.mSetCurrency = mSetCurrency;
ListItem.prototype.mSetTotal = mSetTotal;
ListItem.prototype.mSetDescription = mSetDescription;

function mstrGetExpReportID(){
	return this.strExpReportID;
}

function mstrGetInvoiceNum(){
	return this.strInvoiceNum;
}

function mstrGetEmpName(){
	return this.strEmpName;
}

function mbGetRejected(){
	return this.bRejected;
}

function mstrGetWeekEndDate(){
	return this.strWeekEndDate;
}

function mstrGetCurrency(){
	return this.strCurrency;
}

function mstrGetTotal(){
	return this.strTotal;
}

function mstrGetDescription(){
	return this.strDescription;
}

function mSetExpReportID(strExpReportID){
	this.strExpReportID = strExpReportID;
}

function mSetInvoiceNum(strInvoiceNum){
	this.strInvoiceNum = strInvoiceNum;
}

function mSetEmpName(strEmpName){
	this.strEmpName = strEmpName;
}

function mSetRejected(bRejected){
	this.bRejected = bRejected;
}

function mSetWeekEndDate(strWeekEndDate){
	this.strWeekEndDate = strWeekEndDate;
}

function mSetCurrency(strCurrency){
	this.strCurrency = strCurrency;
}

function mSetTotal(strTotal){
	this.strTotal = strTotal;
}

function mSetDescription(strDescription){
	this.strDescription = strDescription;
}

function fDrawRestoreList(){
  var d = top.navigation.document;
  d.open();
  d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
  d.writeln("<html dir=\""+top.g_strDirection+"\"><head>");
  top.fGenerateStyleCodes(top.navigation);
  d.writeln("</head><body bgcolor=#cccccc>");
	
  if (g_arrReportList.miGetNumOfItems() > 0 ){

  d.writeln("<div align=center>");
  d.writeln("    <TABLE width=95% border=0 cellpadding=1 cellspacing=0><TR class=tablesurround><TD>");
  d.writeln("    <TABLE CELLSPACING=1 CELLPADDING=2 BORDER=0 width=100%>");
  d.writeln("    <TR>");
  fGenHeader(g_cReportNum_Prompt,g_cCenterAlign,
	g_cEmployee_Prompt,g_cCenterAlign,
	g_cRejected_Prompt,g_cCenterAlign,
	g_cReportDate_Prompt,g_cCenterAlign,
	g_cCurrency_Prompt,g_cCenterAlign,
	g_cReportTotal_Prompt,g_cCenterAlign,
	g_cPurpose_Prompt,g_cCenterAlign,
	g_cDelete_Prompt,g_cCenterAlign);
  var objItem = null;
  var strTABLE_DATA_MULTIROW = "";
  var strExpReportID = "";
  d.writeln("    </TR>");	
  

  for (var i=0;i<g_arrReportList.miGetNumOfItems();i++){
    d.writeln("<TR class=TABLEROWLBLUE>");
    if (i%2 == 1) strTABLE_DATA_MULTIROW = "tablecellshaded";
    else strTABLE_DATA_MULTIROW = "tablecell";
	
    strExpReportID = g_arrReportList.arrItems[i].mstrGetExpReportID();
    objItem = g_arrReportList.arrItems[i];

    if (objItem){
	fGenRow(fGenRestoreReportTableEntry(g_dcdName,objItem.mstrGetInvoiceNum(),strExpReportID),g_cCenterAlign,objItem.mstrGetEmpName(),g_cLeftAlign ,objItem.mbGetRejected(), g_cCenterAlign ,objItem.mstrGetWeekEndDate(), g_cCenterAlign ,objItem.mstrGetCurrency(), g_cCenterAlign ,objItem.mstrGetTotal(), g_cRightAlign ,objItem.mstrGetDescription(), g_cLeftAlign ,
        fGenDeleteReportTableEntry(strExpReportID), g_cCenterAlign ,strTABLE_DATA_MULTIROW);
	}
	d.writeln("    </TR>");	
  } //for var i loop




  d.writeln("</table></TD></TR></TABLE></div>");
  d.writeln("<FONT class=promptblack>"+g_strRecordsRange+"</FONT><BR>");


  d.writeln("</body></html>");
  d.close();
  } //miNumOfItems > 0
  else{ 
	d.writeln("<FONT class=promptblack>");
	if (top.g_strMessageText1) d.writeln(top.g_strMessageText1);
	if (top.g_strMessageText2) d.writeln(top.g_strMessageText2);
	d.writeln("</FONT></BODY></HTML>");
	d.close();
      }
} // end of fDrawRestoreList

