/*********************************************************************
 * Javascript functions for Notes 
 *********************************************************************/
 <!-- $Header: asfNotesJslib.js 115.3 2000/12/19 09:38:07 pkm ship  $ -->

/*
 * Check Notes Text field length 
 */
function checkNoteTextLen(formName, fieldName, errMsg)
{
  text = document.forms[formName].elements[fieldName].value;
  //alert("len=" + text.length);
  if (text.length >= 2000)
  {
    alert(errMsg);
    document.forms[formName].elements[fieldName].value = text.substring(0,1999);

    return false;
  }

  return true;
}

/*
 * Check Notes private flag 
 */
function checkPrivateFlag(formName, fieldName)
{
  privFlag = document.forms[formName].elements['privateFlag'];
  if (privFlag.checked)
  {
     document.forms[formName].elements[fieldName].value = 'P';
  }
  else
  {
     document.forms[formName].elements[fieldName].value = 'E';
  }
  //alert("privFlag=" + document.forms[formName].elements[fieldName].value);
}
