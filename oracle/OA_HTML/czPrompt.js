/* $Header: czPrompt.js 115.13 2001/06/14 15:34:19 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash, K MacClay  Created.                     |
 |                                                                           |
 +===========================================================================*/

function Prompt()
{
  this.parentConstructor = Base;
  this.parentConstructor();

  this.height = 25;
  this.width = 100;
  this.bCaptionWrap = false;
}

function Prompt_innerRender()
{
  var sBuffer="";
  if(this.caption) {
    if (this.bCaptionWrap) {
      sBuffer += '<SPAN ';
    } else {
      sBuffer +=  '<NOBR ' ;
    }
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.name+ '>'+this.caption;
    if (this.bCaptionWrap) {
      sBuffer += '</SPAN>';
    } else {
      sBuffer +=  '</NOBR>' ;
    }
  }
  return sBuffer;
}

function Prompt_setValue (v)
{
  this.setCaption (v);
}

function Prompt_getValue (v)
{
  return (this.caption);
}

function Prompt_setCaption(v)
{
  var oldValue = this.caption;
  this.caption = v;

  if(this.launched) {
    var sBuffer = '';
    if (this.bCaptionWrap) {
      sBuffer += '<SPAN ';
    } else {
      sBuffer +=  '<NOBR ' ;
    }
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.objId+ '>'+this.caption;

    if (this.bCaptionWrap) {
      sBuffer += '</SPAN>';
    } else {
      sBuffer +=  '</NOBR>' ;
    }
    if(this.block == null)
      this.connect();
    
    if(this.block)
      HTMLHelper.writeToLayer(this.block,sBuffer);
  }
  if (this.caption != oldValue) {
    this.notifyListeners ('captionChangeCallback', oldValue, this);
  }
}

function Prompt_getCaption()
{
  return this.caption;
}

function Prompt_setSpanClass (classId, update)
{
  this.spanClass = classId;
  if (this.launched && update) {
    this.setCaption (this.caption);
  }
}

function Prompt_getSpanClass ()
{
  return (this.spanClass);
}

function Prompt_setCaptionWrapping (bFlag)
{
  this.bCaptionWrap = bFlag;
  //to allow wrapping of caption do not set clip on layer
  this.noclip = false;
}

function Prompt_getCaptionWrapping ()
{
  return (this.bCaptionWrap);
}

//Extend from Base
HTMLHelper.importPrototypes(Prompt,Base);

Prompt.prototype.constructor = Prompt;
Prompt.prototype.innerRender = Prompt_innerRender;
Prompt.prototype.setValue = Prompt_setValue;
Prompt.prototype.getValue = Prompt_getValue;
Prompt.prototype.setCaption = Prompt_setCaption;
Prompt.prototype.getCaption = Prompt_getCaption;
Prompt.prototype.setCaptionWrapping = Prompt_setCaptionWrapping;
Prompt.prototype.getCaptionWrapping = Prompt_getCaptionWrapping;
Prompt.prototype.setSpanClass = Prompt_setSpanClass;
Prompt.prototype.getSpanClass = Prompt_getSpanClass;

