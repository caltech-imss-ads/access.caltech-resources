/* $Header: czLstItm.js 115.21 2001/06/14 15:34:17 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function ListItem ()
{
  this.baseConstructor = Prompt;
  this.baseConstructor ();

  this.type = 'ListItem';
  this.backgroundColor = 'white';
  this.activeBackColor = 'lightgrey';
  this.selectedBackColor = 'gray';
  this.modelItem = new ListItemModel ();

  //register listeners;
  this.addListener ('mouseoverCallback', this);
  this.addListener ('mouseoutCallback', this);
  this.addListener ('mouseupCallback', this);
}

function ListItem_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function ListItem_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function ListItem_getModelItem ()
{
  return this.modelItem;
}

function ListItem_setModelItem (model)
{
  if (! this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  if (model) {
    if(this.parentObj)
      this.readOnly(this.parentObj.read);
    this.clearModelListeners();
    this.modelItem = model;
    this.loadModelProperties(model);
    this.registerModelListeners();
  } else {
    // null model. Lock events on this view layer;
    this.modelItem=null;
    this.clearViewAndLockEvents ();
    //this.unhilite ();
  }
}

function ListItem_buildCells()
{
  // construct and position the cells from left to right;
  if (this.columnDescriptors) {
    var colDesc = null;
    this.cellsHash = new Array();
    this.cells = new Array();
    
    colDesc = this.columnDescriptors[0];
    this.cellsHash[colDesc.name] = this;
    this.cells[colDesc.index] = this;
    this.type = 'label';
    
    this.cellsBuilt = true;
  }
}

function ListItem_loadModelProperties(model)
{
  if (model) {
    //Unlock the view to listen for events;
    this.unlockEvents ();

    this.setCellValue ('text', model.getValue('text'));
    this.setSelected (model.isSelected());
  }
}

function ListItem_setCellValue (colName, colValue)
{
  this.setCaption (colValue);
}

function ListItem_setSelected (state)
{
  if (state) {
    //this.hilite (this.selectedBackColor);		
  } else {
    //this.unhilite ();
  }
}

function ListItem_setColumnDescriptors (colDescs, colDescHash)
{
  this.columnDescriptors = colDescs;
  this.colDescHash = colDescHash;
}

function ListItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {
    // default-- the ListItem will later interpret a single text column;
    // to mean that it should render to it's own layer;
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('text', 'label', 0, this.height, this.width, 0, 0, 'top', 'left', true, true);
    this.hashColumnDescriptor(this.columnDescriptors[0]);
  }
  return (this.columnDescriptors);
}

function ListItem_getColumnDescriptorsHash ()
{
  return (this.colDescHash);
}

function ListItem_hashColumnDescriptor(colDesc) 
{
  this.colDescHash[colDesc.name] = colDesc;
}

function ListItem_hashAllColumnDescriptors(colDescs) 
{
  var len = colDescs.length;
  for (var i = 0; i < len; i++) {
    this.hashColumnDescriptor(colDescs[i]);
  }
}

function ListItem_clearViewAndLockEvents()
{
  this.clearView();
  this.lockEvents();  
}

function ListItem_clearView()
{
  this.setValue ('');
}

function ListItem_lockEvents() 
{
  this.locked = true;
}

function ListItem_unlockEvents() 
{
  this.locked = false;
}

function ListItem_registerModelListeners()
{
  if (this.modelItem) {
    if(!this.read)
    {
      //register modelitem listener
      this.modelItem.addListener('propertyChangeCallback', this);
    }
    else
   {
      //remove listeners;
      this.removeListener ('mouseoverCallback', this);
      this.removeListener ('mouseoutCallback', this);
      this.removeListener ('mouseupCallback', this);
   }
  }
}

function ListItem_clearModelListeners()
{
  if (this.modelItem) {
    this.modelItem.removeListener('propertyChangeCallback', this);
  }
}

function ListItem_propertyChangeCallback (change)
{
  if (change.property == 'selected') {
    this.setSelected(change.value);
  } else {
    this.setCellValue(change.property, change.value);
  }
  return true;
}

function ListItem_mouseoverCallback (e, eSrc)
{
  if (this.locked) 
    return true;

  //this.hilite (this.activeBackColor);
  //if (this.modelItem && (!this.modelItem.isSelected())) {
    //this.hilite (this.activeBackColor);
  //}
  return true;
}

function ListItem_mouseoutCallback (e, eSrc)
{
  if (this.locked) 
    return true;
  
  //this.unhilite ();
  //if (this.modelItem && (!this.modelItem.isSelected())) {
    //this.unhilite ();
  //}
}

function ListItem_mouseupCallback (e, eSrc)
{
  if (this.locked)
    return true;

  if (this.modelItem) {
    this.modelItem.setSelected (!this.modelItem.isSelected());
  }
  this.notifyListeners ('actionMouseUp', e, this);
  return true;
}

function ListItem_fixDimensions ()
{
  if (this.cellsHash) {
    var wd = 0;
    var lt = 0;
    var curCell = null, colDesc = null;

    var len = this.columnDescriptors.length;    
    for (var i = 0; i < len; i++) {
      colDesc = this.columnDescriptors [i];
      wd = colDesc.getAvailWidth ();
      lt = colDesc.getLeft ();
      curCell = this.cellsHash[colDesc.name];
      curCell.setWidth (wd);
      curCell.setLeft (lt);
    }
  }
}

function ListItem_showColumn(colName, withLayout)
{
  if (this.cellsHash) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      curCell.show();
      if (withLayout) {
        this.layoutColumns ();
      }
    }
  }  
}

function ListItem_hideColumn(colName, withLayout)
{
  if (this.cellsHash) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      curCell.hide ();
      if (withLayout) {
        this.layoutColumns ();
      }
    }
  }
}

function ListItem_layoutColumns ()
{
  //empty function. Sub class will override
}

function ListItem_innerRender()
{
  var sBuffer = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  sBuffer += '<NOBR ID=SPAN-' + this.name;
  if (this.spanClass) {
    sBuffer += ' CLASS=' + this.spanClass;
  }
  sBuffer += '>';
  if (this.caption)
    sBuffer += this.caption;
  
  sBuffer += '</NOBR>';
  
  return sBuffer;
}

// Extend from Prompt
HTMLHelper.importPrototypes(ListItem, Prompt);

// Public Methods
ListItem.prototype.setActiveBackColor	= ListItem_setActiveBackColor;
ListItem.prototype.setSelectedBackColor = ListItem_setSelectedBackColor;
ListItem.prototype.setSelected = ListItem_setSelected;

// Protected methods
ListItem.prototype.setModelItem	= ListItem_setModelItem;
ListItem.prototype.getModelItem	= ListItem_getModelItem;
ListItem.prototype.innerRender = ListItem_innerRender;
ListItem.prototype.fixDimensions = ListItem_fixDimensions;
ListItem.prototype.setColumnDescriptors = ListItem_setColumnDescriptors;
ListItem.prototype.getColumnDescriptors = ListItem_getColumnDescriptors;
ListItem.prototype.getColumnDescriptorsHash = ListItem_getColumnDescriptorsHash;
ListItem.prototype.setCellValue = ListItem_setCellValue;
ListItem.prototype.layoutColumns = ListItem_layoutColumns;
ListItem.prototype.showColumn = ListItem_showColumn;
ListItem.prototype.hideColumn = ListItem_hideColumn;

// Private methods
ListItem.prototype.buildCells = ListItem_buildCells;
ListItem.prototype.registerModelListeners = ListItem_registerModelListeners;
ListItem.prototype.clearModelListeners = ListItem_clearModelListeners;
ListItem.prototype.loadModelProperties = ListItem_loadModelProperties;
ListItem.prototype.hashColumnDescriptor = ListItem_hashColumnDescriptor;
ListItem.prototype.hashAllColumnDescriptors = ListItem_hashAllColumnDescriptors;

ListItem.prototype.clearViewAndLockEvents = ListItem_clearViewAndLockEvents;
ListItem.prototype.clearView = ListItem_clearView;
ListItem.prototype.lockEvents = ListItem_lockEvents;
ListItem.prototype.unlockEvents = ListItem_unlockEvents;

// Event handlers
ListItem.prototype.mouseoverCallback = ListItem_mouseoverCallback;
ListItem.prototype.mouseoutCallback	= ListItem_mouseoutCallback;
ListItem.prototype.mouseupCallback = ListItem_mouseupCallback;
ListItem.prototype.propertyChangeCallback = ListItem_propertyChangeCallback;
