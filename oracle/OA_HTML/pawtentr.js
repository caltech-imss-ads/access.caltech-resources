//============================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtentr.js                       |
//  |                                                |
//  | Description: This is the standalone JavaScript |
//  |              function for SST timecard info and|
//  |              enter hours page.                 |
//  |                                                |
//  | History:     Created by Akita Ling             |
//  |                                                |
//  +================================================+
/* $Header: pawtentr.js 115.52 2001/03/23 17:40:46 pkm ship      $ */

//  --------------------------------------------------
//  Function: fAddToPopDate
//  Description:  This function add the option in the 
//                date pop list on the header page. 
//
//  --------------------------------------------------
function fAddToPopDate(p_formDisplay,
                       p_dWeekEndingDate, 
                       p_bDefaultSelected, 
                       p_bSelected, 
                       p_iIndex){

  if (p_dWeekEndingDate != null) {
    if (p_formDisplay.popWeekEndingDate.options.length == p_iIndex)
      p_formDisplay.popWeekEndingDate.options.length = p_formDisplay.popWeekEndingDate.options.length + 1 ;
    p_formDisplay.popWeekEndingDate.options[p_iIndex].value = p_dWeekEndingDate.getTime();
    p_formDisplay.popWeekEndingDate.options[p_iIndex].text  = fDateToLongString(g_strSessDateFormat, p_dWeekEndingDate, true);
  }

}


//  --------------------------------------------------
//  Function: fCheckDetail
//  Description: Ask the user to appy changes in the
//		 detail page if changes have been 
//		 detected.
//
//  --------------------------------------------------
function fCheckDetail(){
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

  if ((g_bDetailModified) || ((fbIsDFlexUsed()) && (g_bDetailDFlexModified))) {
    if(l_winDetail.confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_APPLY_DETAIL")))  
      fOnClickOkDetail();
    else
      fCloseDetail(false);
  }
  else fCloseDetail(false);

}
  

//  --------------------------------------------------
//  Function: fCheckPosNum
//  Description: Make sure the input value is positive. 
//
//  --------------------------------------------------
function fCheckPosNum(p_iInputValue, p_bNoZero){
  var l_strString = p_iInputValue + "";

  if (((l_strString < 0) && (!p_bNoZero)) || ((l_strString <= 0) && (p_bNoZero))){
    return false;
  }
  else{
    return true;
  }
} 

//  --------------------------------------------------
//  Function: fCloseDetail
//  Description: Remember the X and Y coordinate of the
//		 the detail window before closing.  
//
//  --------------------------------------------------
function fCloseDetail(p_bUpdatedDetail){
  var l_focusItem = eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.' + g_iCursorPosition);
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

  // Navigator code 
  if ( fIsNetscape() ) {
    g_iDetailWinXCoordinate = l_winDetail.screenX;
    g_iDetailWinYCoordinate = l_winDetail.screenY;
  }
  else {
    g_iDetailWinXCoordinate = 100;
    g_iDetailWinYCoordinate = 100
  }
  fModalWinClose();

  if (p_bUpdatedDetail) {
    setTimeout("fRefreshEntryBody()", 200);
  }


}

function fRefreshEntryBody() {

  fDrawEntryBodyContent();
  fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent);

  // Mark the current line with the arrow indicator and unmark the previous one
  eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.imgCurrLineIndicator'  + g_iPrevLine).src = g_imgEmpty.src;
  eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.imgCurrLineIndicator'  + g_iCurrLine).src = g_imgArrow.src;

}

//  --------------------------------------------------
//  Function: fCodeForExplorer
//  Description: Focus on one of the widget on each form to
//		 force the onchange handler to fire in IE
//		 even when the user doesn't tab out the
//		 the field after modifications.
//
//  --------------------------------------------------
function fCodeForExplorer(){

  top.framMainBody.framBodyHeader.document.formEntryBodyHeader.txaComment.focus();

/* Need to focus and blur on IE5 and win95 combination in order for the
   onchange handler to be triggered Bug 1544505 */
  if (fIsIE5() && fIsWindows95()) {
    top.framMainBody.framBodyHeader.document.formEntryBodyHeader.txaComment.blur();
  }

  fGetInputWidget(top.framMainBody.framBodyContent.document.formEntryBodyContent, 'popType', g_iCurrLine).focus();

}

//  --------------------------------------------------
//  Function: fCodeForExplorerDetail
//  Description: Because explorer doesn't fire the onChange
//		 handler if the user doesn't tab out the of
//		 field and click on a link, we use this 
//		 function as a workaround to force the 
//		 onChange handler to fire.
//
//  --------------------------------------------------
function fCodeForExplorerDetail(){
  var l_winDetail             = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formDetailBodyHeader  = l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader;
  var l_formDetailBodyContent = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;
  var l_iobjTempLinesIndex    = g_objTempLines.miFindLine(g_iCurrLine);

  if(fIsExplorer()){
    for (var i=0; i<C_iWEEKDAYS; i++){
      if(g_objPref.mGetobjWorkDays().bIsWorkDay(i) == true || g_bHourNotOnWorkDay){
        break;
      }
    }

    fGetInputWidget(l_formDetailBodyContent, 'txtDetailHoursY', i).focus();
    fGetInputWidget(l_formDetailBodyContent, 'txaCommentY', i).focus();
    l_formDetailBodyHeader.txtTaskNum.focus();
    l_formDetailBodyHeader.txtProjNum.focus();
  }
}


//  --------------------------------------------------
//  Function: fComputeDayTotal
//  Description: Compute the total of a given day.
//
//  --------------------------------------------------
function fComputeDayTotal(p_iDayOfWeek){
  var l_nDayTotal  = 0;
  var l_nGrandTotal= 0;

  for (var i=0; i<g_objTimecard.objLines.miGetNumLines(); i++){
    if ((g_objTimecard.objLines.arrLine[i].bIsDeleted != true) &&
        (g_objTimecard.objLines.arrLine[i].bIsEmpty != true)){
      l_nDayTotal += g_objTimecard.objLines.arrLine[i].objDays.arrDay[p_iDayOfWeek].nHours - 0;
    } 
  }
  l_nDayTotal = l_nDayTotal + '';
  return (l_nDayTotal);
}

//  --------------------------------------------------
//  Function: fComputeWeekTotal
//  Description: Compute the weektotal with the give 
//		 week.
//
//  --------------------------------------------------
function fComputeWeekTotal(p_iLineArrayNumber){
  var l_nWeekTotal = 0;
  var l_nGrandTotal= 0;

  for (var i=0; i<C_iWEEKDAYS; i++){
    l_nWeekTotal += g_objTimecard.objLines.arrLine[p_iLineArrayNumber].objDays.arrDay[i].nHours - 0;
  }

  return (l_nWeekTotal);
}

//  --------------------------------------------------
//  Function: fComputeGrandTotal
//  Description: Compute grand total of the timecard. 
//
//  --------------------------------------------------
function fComputeGrandTotal(){
  var l_nGrandTotal=0;
  for (var i=0; i<g_objTimecard.objLines.miGetNumLines(); i++){
    if ((g_objTimecard.objLines.arrLine[i].bIsDeleted != true) && 
       (g_objTimecard.objLines.arrLine[i].bIsEmpty != true)){
      l_nGrandTotal += g_objTimecard.objLines.arrLine[i].nWeekTotal - 0;
    } 
  }
  return (l_nGrandTotal);
}


//  --------------------------------------------------
//  Function: fDrawIE5CalendarBody
//  Description: Main function used to draw the calendar
//  
//  --------------------------------------------------
function fDrawIE5CalendarBody(p_iDayOfWeek, p_iMonth, p_iYear) {
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_CALENDAR");
  var l_strHtmlReturn = "";
  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody;
  var l_strCodeLoc = g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc();

  var l_iDayOfWeek = p_iDayOfWeek;
  var l_iMonth = p_iMonth;
  var l_iYear = p_iYear;
 

  l_framTarget.document.open();
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('\n<HEAD>');
  l_framTarget.document.writeln('\n<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('\n</HEAD>');
  l_framTarget.document.writeln('\n<BODY class=panel TOPMARGIN=5 LEFTMARGIN=5 onload="window.focus()">');
  l_framTarget.document.writeln('\n<TITLE>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_CALENDAR").mGetstrLabel()+ '</TITLE>');
  l_framTarget.document.writeln('\n<FORM NAME="formCalendar">');

  l_framTarget.document.writeln('\n<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>');
  l_framTarget.document.writeln('\n  <TR>');
  l_framTarget.document.writeln('\n    <TD width=1 class=highlight><IMG src=' + g_strImagePath + 'FNDPX6.gif></TD>');
  l_framTarget.document.writeln('\n    <TD width=4 class=panel><IMG src=' + g_strImagePath + 'FNDPX5.gif></TD>');
  l_framTarget.document.writeln('\n    <TD class=panel>');
  l_framTarget.document.writeln('\n      <TABLE border=0 width=100%> ');
  l_framTarget.document.writeln('\n        <TR>');
  l_framTarget.document.writeln('\n          <TD valign=middle width=10%><A href="javascript:' + l_strCodeLoc + 'fSkipBack();">');
  l_framTarget.document.writeln('\n          <IMG src=' + g_strImagePath + 'FNDIPRV.gif border=0></A></TD>');
  l_framTarget.document.writeln('\n          <TD align=center valign=middle>');

  l_framTarget.document.writeln('\n  <SELECT NAME="popMonth"  onChange="javascript:' + l_strCodeLoc + 'g_iCalMonth=popMonth.options[popMonth.selectedIndex].value;' + l_strCodeLoc +  'fDrawIE5CalendarBody(' + l_strCodeLoc + 'g_iCalDay, ' + l_strCodeLoc + 'g_iCalMonth, ' + l_strCodeLoc + 'g_iCalYear );">');  
  
  // Fill the month poplist
  for (month=0; month<C_iMONTHS; month++){
    if (month == l_iMonth){ 
      l_framTarget.document.writeln('\n    <OPTION VALUE="' + month + '" SELECTED>' + fIntToMonthFull(month) + '</OPTION>');
    }
    else {
      l_framTarget.document.writeln('\n    <OPTION VALUE="' + month + '">' + fIntToMonthFull(month) + '</OPTION>');
    }
  }
  l_framTarget.document.writeln('\n  </SELECT>');

  // Fill the year poplist
  l_framTarget.document.writeln('\n  <SELECT NAME="popYear"  onChange="javascript:' + l_strCodeLoc + 'g_iCalYear=popYear.options[popYear.selectedIndex].value;' + l_strCodeLoc + 'fDrawIE5CalendarBody(' + l_strCodeLoc + 'g_iCalDay, ' + l_strCodeLoc + 'g_iCalMonth, ' + l_strCodeLoc + 'g_iCalYear);">');  
  for (year=g_dWeekEndingDate.getFullYear()-5; year<g_dWeekEndingDate.getFullYear()+6; year++){
    if (year==l_iYear){
      l_framTarget.document.writeln('\n    <OPTION VALUE="' + year + '" SELECTED>' + year + '</OPTION>');
    }
    else {
      l_framTarget.document.writeln('\n    <OPTION VALUE="' + year + '"         >' + year + '</OPTION>');
    }
  } 
  l_framTarget.document.writeln('\n  </SELECT>');

  l_framTarget.document.writeln('\n          </TD>');
	  l_framTarget.document.writeln('\n          <TD valign=middle width=10%><A href="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fSkipForward();">');
	  l_framTarget.document.writeln('\n            <IMG src=' + g_strImagePath + 'FNDINXT.gif  border=0></A>');
	  l_framTarget.document.writeln('\n          </TD>');
	  l_framTarget.document.writeln('\n        </TR>');
	  l_framTarget.document.writeln('\n      </TABLE>');
	  l_framTarget.document.writeln('\n      <TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 width=98%>');
	  l_framTarget.document.writeln('\n<TR>');
	  l_framTarget.document.writeln('\n<TD height=5></TD>');
	  l_framTarget.document.writeln('\n</TR>');


	  l_framTarget.document.writeln('\n<TR>');
	  l_framTarget.document.writeln('\n<TD><IMG src=' + g_strImagePath + 'PACALTL.gif></TD>');
	  for (var i=0; i<C_iWEEKDAYS; i++){
	    l_framTarget.document.writeln('\n  <TD WIDTH=35 class=weekdaycell ALIGN=CENTER VALIGN=MIDDLE>' + fIntToDayOfWeek(i).substr(0,1) + '</TD>');
	  }
	  l_framTarget.document.writeln('\n<TD><IMG src=' + g_strImagePath + 'PACALTR.gif></TD>');
	  l_framTarget.document.writeln('\n</TR>');


	  fDrawIE5CalendarLine(l_iDayOfWeek, l_iMonth, l_iYear);


	  l_framTarget.document.writeln('\n<TR><TD><IMG src=' + g_strImagePath + 'PACALBL.gif></TD>');
  l_framTarget.document.writeln('\n<TD class=monthdaycell colspan=7><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');
  l_framTarget.document.writeln('\n<TD><IMG src=' + g_strImagePath + 'PACALBR.gif></TD></TR></TABLE></TD>');
  l_framTarget.document.writeln('\n<TD width=4 class=panel><IMG src=' + g_strImagePath + 'FNDPX5.gif></TD>');
  l_framTarget.document.writeln('\n<TD width=1></TD></TR>');

  l_framTarget.document.writeln('\n<TR><TD height=3></TD></TR></TABLE>');

  l_framTarget.document.writeln('\n</FORM>');
  l_framTarget.document.writeln('\n</BODY></HTML>');
  l_framTarget.document.close();
}

//  --------------------------------------------------
//  Function: fDrawCalendarBody
//  Description: Main function used to draw the calendar
//  
//  --------------------------------------------------
function fDrawCalendarBody() {
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_CALENDAR");
  var l_strHtmlReturn = "";

  var l_iDayOfWeek = g_iCalDay;
  var l_iMonth = g_iCalMonth;
  var l_iYear = g_iCalYear;
 
  l_strHtmlReturn += ('<HTML DIR="' + g_strDirection + '">');
  l_strHtmlReturn += ('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_strHtmlReturn += ('\n<HEAD>');
  l_strHtmlReturn += ('\n<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_strHtmlReturn += ('\n</HEAD>');
  l_strHtmlReturn += ('\n<BODY class=panel TOPMARGIN=5 LEFTMARGIN=5 onload="window.focus()">');
  l_strHtmlReturn += ('\n<TITLE>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_CALENDAR").mGetstrLabel()+ '</TITLE>');
  l_strHtmlReturn += ('\n<FORM NAME="formCalendar">');

  l_strHtmlReturn += ('\n<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>');
  l_strHtmlReturn += ('\n  <TR>');
  l_strHtmlReturn += ('\n    <TD width=1 class=highlight><IMG src=' + g_strImagePath + 'FNDPX6.gif></TD>');
  l_strHtmlReturn += ('\n    <TD width=4 class=panel><IMG src=' + g_strImagePath + 'FNDPX5.gif></TD>');
  l_strHtmlReturn += ('\n    <TD class=panel>');
  l_strHtmlReturn += ('\n      <TABLE border=0 width=100%> ');
  l_strHtmlReturn += ('\n        <TR>');
  l_strHtmlReturn += ('\n          <TD valign=middle width=10%><A href="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fSkipBack();">');
  l_strHtmlReturn += ('\n          <IMG src=' + g_strImagePath + 'FNDIPRV.gif border=0></A></TD>');
  l_strHtmlReturn += ('\n          <TD align=center valign=middle>');

//  l_strHtmlReturn += ('\n  <SELECT NAME="popMonth"  onChange="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'g_iCalMonth=popMonth.options[popMonth.selectedIndex].value;self.location.reload();">');  

  l_strHtmlReturn += ('\n  <SELECT NAME="popMonth"  onChange="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'g_iCalMonth=popMonth.options[popMonth.selectedIndex].value;self.document.close();self.document.write(' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fDrawCalendarBody());">'); 
  
  // Fill the month poplist
  for (month=0; month<C_iMONTHS; month++){
    if (month == l_iMonth){ 
      l_strHtmlReturn += ('\n    <OPTION VALUE="' + month + '" SELECTED>' + fIntToMonthFull(month) + '</OPTION>');
    }
    else {
      l_strHtmlReturn += ('\n    <OPTION VALUE="' + month + '">' + fIntToMonthFull(month) + '</OPTION>');
    }
  }
  l_strHtmlReturn += ('\n  </SELECT>');

  // Fill the year poplist
//  l_strHtmlReturn += ('\n  <SELECT NAME="popYear"  onChange="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'g_iCalYear=popYear.options[popYear.selectedIndex].value;self.location.reload();">');  

  l_strHtmlReturn += ('\n  <SELECT NAME="popYear"  onChange="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'g_iCalYear=popYear.options[popYear.selectedIndex].value;self.document.close();self.document.write(' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fDrawCalendarBody());">');  

  for (year=g_dWeekEndingDate.getFullYear()-5; year<g_dWeekEndingDate.getFullYear()+6; year++){
    if (year==l_iYear){
      l_strHtmlReturn += ('\n    <OPTION VALUE="' + year + '" SELECTED>' + year + '</OPTION>');
    }
    else {
      l_strHtmlReturn += ('\n    <OPTION VALUE="' + year + '"         >' + year + '</OPTION>');
    }
  } 
  l_strHtmlReturn += ('\n  </SELECT>');

  l_strHtmlReturn += ('\n          </TD>');
  l_strHtmlReturn += ('\n          <TD valign=middle width=10%><A href="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fSkipForward();">');
  l_strHtmlReturn += ('\n            <IMG src=' + g_strImagePath + 'FNDINXT.gif  border=0></A>');
  l_strHtmlReturn += ('\n          </TD>');
  l_strHtmlReturn += ('\n        </TR>');
  l_strHtmlReturn += ('\n      </TABLE>');
  l_strHtmlReturn += ('\n      <TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0 width=98%>');
  l_strHtmlReturn += ('\n<TR>');
  l_strHtmlReturn += ('\n<TD height=5></TD>');
  l_strHtmlReturn += ('\n</TR>');


  l_strHtmlReturn += ('\n<TR>');
  l_strHtmlReturn += ('\n<TD><IMG src=' + g_strImagePath + 'PACALTL.gif></TD>');
  for (var i=0; i<C_iWEEKDAYS; i++){
    l_strHtmlReturn += ('\n  <TD WIDTH=35 class=weekdaycell ALIGN=CENTER VALIGN=MIDDLE>' + fIntToDayOfWeek(i).substr(0,1) + '</TD>');
  }
  l_strHtmlReturn += ('\n<TD><IMG src=' + g_strImagePath + 'PACALTR.gif></TD>');
  l_strHtmlReturn += ('\n</TR>');


  l_strHtmlReturn += fDrawCalendarLine(l_iDayOfWeek, l_iMonth, l_iYear);


  l_strHtmlReturn += ('\n<TR><TD><IMG src=' + g_strImagePath + 'PACALBL.gif></TD>');
  l_strHtmlReturn += ('\n<TD class=monthdaycell colspan=7><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');
  l_strHtmlReturn += ('\n<TD><IMG src=' + g_strImagePath + 'PACALBR.gif></TD></TR></TABLE></TD>');
  l_strHtmlReturn += ('\n<TD width=4 class=panel><IMG src=' + g_strImagePath + 'FNDPX5.gif></TD>');
  l_strHtmlReturn += ('\n<TD width=1></TD></TR>');
//  l_strHtmlReturn += ('\n<TD width=1 class=darkshadow><IMG src=' + g_strImagePath + 'FNDPX2.gif></TD></TR>');

  l_strHtmlReturn += ('\n<TR><TD height=3></TD></TR></TABLE>');

  l_strHtmlReturn += ('\n</FORM>');
  l_strHtmlReturn += ('\n</BODY></HTML>');
  return (l_strHtmlReturn);
}

//  --------------------------------------------------
//  Function: fDrawIE5CalendarLine
//  Description: Draw the calendar line 
//
//  --------------------------------------------------
function fDrawIE5CalendarLine(p_iDayOfWeek, p_iMonth, p_iYear){
  var firstDay  = new Date(p_iYear,p_iMonth,1);
  var startDay  = firstDay.getDay();
  var column    = 0;
  var row       = 1;
  var lastMonth = p_iMonth - 1;
  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody;
  var l_strHtmlReturn = "";

  // Take care of the leap year here
  if (((p_iYear % 4 == 0) && (p_iYear % 100 != 0)) || (p_iYear % 400 == 0))
    g_arriCalendarDays[1] = 29; 
  else
    g_arriCalendarDays[1] = 28;

   l_framTarget.document.writeln('\n<TR ALIGN=CENTER VALIGN=MIDDLE>');
   l_framTarget.document.writeln('\n<TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');


  // Filling out the calendar days of the previous month

  if (lastMonth == -1) lastMonth = 11;
  for (var i=0; i<startDay; i++, column++){ 
    l_framTarget.document.writeln('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + (g_arriCalendarDays[lastMonth]-startDay+i+1) + '</A></TD>');
  }

  // Filling out the calendar days of the current month
  for (var i=1; i<=g_arriCalendarDays[p_iMonth]; i++, column++){
    if(column == p_iDayOfWeek){
      l_framTarget.document.writeln('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=daylink HREF="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickCalDay(' + i + ')">' + i + '</A></TD>');
    }
    else {
      l_framTarget.document.writeln('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + i + '</A></TD>');
    }
    if (column == 6){
      column = -1;
      l_framTarget.document.writeln('\n  <TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD></TR>');
      l_framTarget.document.writeln('\n<TR ALIGN=CENTER VALIGN=MIDDLE>');
      l_framTarget.document.writeln('\n<TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');
    }
  }
  
  // Filling out the calendar days of the next month
  if (column > 0){
    for (var i=1; column<C_iWEEKDAYS; i++, column++){
      l_framTarget.document.writeln('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + i  + '</A></TD>');
      if (column == 6){
        l_framTarget.document.writeln('\n  <TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD></TR>');
      }
    }
  }
}


//  --------------------------------------------------
//  Function: fDrawCalendarLine
//  Description: Draw the calendar line 
//
//  --------------------------------------------------
function fDrawCalendarLine(p_iDayOfWeek, p_iMonth, p_iYear){
  var firstDay  = new Date(p_iYear,p_iMonth,1);
  var startDay  = firstDay.getDay();
  var column    = 0;
  var row       = 1;
  var lastMonth = p_iMonth - 1;
  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_strHtmlReturn = "";

  // Take care of the leap year here
  if (((p_iYear % 4 == 0) && (p_iYear % 100 != 0)) || (p_iYear % 400 == 0))
    g_arriCalendarDays[1] = 29; 
  else
    g_arriCalendarDays[1] = 28;

   l_strHtmlReturn += ('\n<TR ALIGN=CENTER VALIGN=MIDDLE>');
   l_strHtmlReturn += ('\n<TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');


  // Filling out the calendar days of the previous month

  if (lastMonth == -1) lastMonth = 11;
  for (var i=0; i<startDay; i++, column++){ 
    l_strHtmlReturn += ('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + (g_arriCalendarDays[lastMonth]-startDay+i+1) + '</A></TD>');
  }

  // Filling out the calendar days of the current month
  for (var i=1; i<=g_arriCalendarDays[p_iMonth]; i++, column++){
    if(column == p_iDayOfWeek){
      l_strHtmlReturn += ('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=daylink HREF="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickCalDay(' + i + ')">' + i + '</A></TD>');
    }
    else {
      l_strHtmlReturn += ('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + i + '</A></TD>');
    }
    if (column == 6){
      column = -1;
      l_strHtmlReturn += ('\n  <TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD></TR>');
      l_strHtmlReturn += ('\n<TR ALIGN=CENTER VALIGN=MIDDLE>');
      l_strHtmlReturn += ('\n<TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD>');
    }
  }
  
  // Filling out the calendar days of the next month
  if (column > 0){
    for (var i=1; column<C_iWEEKDAYS; i++, column++){
      l_strHtmlReturn += ('\n  <TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=disableddaylink>' + i  + '</A></TD>');
      if (column == 6){
        l_strHtmlReturn += ('\n  <TD class=monthdaycell><IMG src=' + g_strImagePath + 'FNDDBPXE.gif></TD></TR>');
      }
    }
  }

  return (l_strHtmlReturn);
}


//  --------------------------------------------------
//  Function: fCopyToTempObj
//  Description: Copy values from g_objTimecards.objLines
//		 to a temporary Lines object.
//
//  --------------------------------------------------
function fCopyToTempObj() {
  var j=0;

  // Copy the lines from the main page to another temporary structure
  g_objTempLines = new Lines();
  for (var i=0; i<g_iTotalDisplayedLines; i++){
      g_objTempLines.mAddLine('',i+1,'','','','','','',new Type('','','','',''),new Days());
  }

  for (var i=0; i<g_objTimecard.objLines.miGetNumLines(); i++){
    if ((g_objTimecard.objLines.arrLine[i].bIsDeleted != true) && 
       (g_objTimecard.objLines.arrLine[i].bIsEmpty != true)){
      g_objTempLines.arrLine[j].iDenormID		   = g_objTimecard.objLines.arrLine[i].mGetnDenormID();
      g_objTempLines.arrLine[j].nOrigDenormID	   = g_objTimecard.objLines.arrLine[i].mGetnOrigDenormID();
      g_objTempLines.arrLine[j].iDisplayLineNum	   = g_objTimecard.objLines.arrLine[i].mGetiDisplayLineNum();
      g_objTempLines.arrLine[j].iProjectID	   = g_objTimecard.objLines.arrLine[i].mGetiProjectID();
      g_objTempLines.arrLine[j].strProjectNum	   = g_objTimecard.objLines.arrLine[i].mGetstrProjectNum();
      g_objTempLines.arrLine[j].strProjectName	   = g_objTimecard.objLines.arrLine[i].mGetstrProjectName();
      g_objTempLines.arrLine[j].iTaskID		   = g_objTimecard.objLines.arrLine[i].mGetiTaskID();
      g_objTempLines.arrLine[j].strTaskNum	   = g_objTimecard.objLines.arrLine[i].mGetstrTaskNum();
      g_objTempLines.arrLine[j].strTaskName	   = g_objTimecard.objLines.arrLine[i].mGetstrTaskName();

      g_objTempLines.arrLine[j].mSetobjType(new Type(g_objTimecard.objLines.arrLine[i].objType.mGetstrName(),g_objTimecard.objLines.arrLine[i].objType.mGetstrSystemLinkage(), g_objTimecard.objLines.arrLine[i].objType.mGetstrExpenditureType(), g_objTimecard.objLines.arrLine[i].objType.mGetdStartDate(), g_objTimecard.objLines.arrLine[i].objType.mGetdEndDate())); 

      for(var k=0; k<C_iWEEKDAYS; k++){
        g_objTempLines.arrLine[j].objDays.arrDay[k].mCopy(g_objTimecard.objLines.arrLine[i].objDays.arrDay[k]);
      }

      g_objTempLines.arrLine[j].nWeekTotal	= g_objTimecard.objLines.arrLine[i].mGetnWeekTotal();
      g_objTempLines.arrLine[j].bIsDeleted    = g_objTimecard.objLines.arrLine[i].mGetbIsDeleted();
      g_objTempLines.arrLine[j].bIsEmpty      = g_objTimecard.objLines.arrLine[i].mGetbIsEmpty();
      j++;
    }
  }

}


//  --------------------------------------------------
//  Function: fDrawDetailBodyHeader
//  Description: Draw detail Body header
//
//  --------------------------------------------------
function fDrawDetailBodyHeader(p_iLineNumber){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_DETAIL");

  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody.framBodyHeader;

  l_framTarget.document.open();

  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css">');
  l_framTarget.document.writeln('</HEAD>');
  l_framTarget.document.writeln('<BODY class=panel>');
  l_framTarget.document.writeln('<FORM name="formDetailBodyHeader">');

  l_framTarget.document.writeln('<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>');
  l_framTarget.document.writeln('<TR>');
  l_framTarget.document.writeln('  <TD class=colorg5 COLSPAN=3 valign=top align=center>');
  l_framTarget.document.writeln('  <TABLE CELLPADDING=2 CELLSPACING=1 BORDER=0 WIDTH=90%>');
  l_framTarget.document.writeln('  <TR>');
  l_framTarget.document.writeln('    <TD rowspan=8 width=2% valign=top><IMG SRC=' + g_strImagePath + 'PADETAIL.gif></TD>');
  l_framTarget.document.writeln('    <TD class=fielddata colspan=2>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_DETAILS").mGetstrLabel()+ '<IMG SRC=' + g_strImagePath + 'FNDPX3.gif height=2 width=98%></TD>');
  l_framTarget.document.writeln('  </TR>');
  l_framTarget.document.writeln('  <TR>');
  l_framTarget.document.writeln('    <TD class=PROMPTBLACKRIGHT>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT").mGetstrLabel()+ '</TD>');
  l_framTarget.document.writeln('    <TD><TABLE CELLSPACING=0 CELLPADDING=0><TR>');
  l_framTarget.document.writeln('      <TD><input type=text name="txtProjNum"');
  l_framTarget.document.writeln('  onFocus="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnFocusDetailHeader();"  onChange="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeDetailProject(top.framMainBody.framBodyHeader.document.formDetailBodyHeader, false);"</TD>');
  l_framTarget.document.writeln('            <TD> <A href="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickProjectLOV(' + p_iLineNumber + ', false)" ');

 l_framTarget.document.writeln('          onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '\';return true">');
 l_framTarget.document.writeln('          <IMG src="' + g_strImagePath + 'FNDILOV.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '" height=18 width=16 border=no></A></TD>');

  l_framTarget.document.writeln('    </TD></TR></TABLE>');
  l_framTarget.document.writeln('  </TR>');
  l_framTarget.document.writeln('  <TR>');
  l_framTarget.document.writeln('    <TD class=PROMPTBLACKRIGHT>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK").mGetstrLabel()+ '</TD>');
  l_framTarget.document.writeln('    <TD><TABLE CELLSPACING=0 CELLPADDING=0><TR>');
  l_framTarget.document.writeln('      <TD><input type=text name="txtTaskNum"  onFocus="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnFocusDetailHeader();" onChange="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeDetailTask(top.framMainBody.framBodyHeader.document.formDetailBodyHeader, false);"></TD>');
  l_framTarget.document.writeln('            <TD> <A href="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickTaskLOV(' + p_iLineNumber + ', false)" ');
  l_framTarget.document.writeln('           onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '\';return true">');
  l_framTarget.document.writeln('            <IMG SRC="' + g_strImagePath + 'FNDILOV.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '" border=0 width=16 height=18 align="absmiddle"></A></TD>');

  l_framTarget.document.writeln('    </TD></TR></TABLE>');
  l_framTarget.document.writeln('  </TR>');
  l_framTarget.document.writeln('  <TR> ');
  l_framTarget.document.writeln('    <TD class=PROMPTBLACKRIGHT>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetstrLabel()+ '</TD>');
  l_framTarget.document.writeln('    <TD><TABLE CELLSPACING=0 CELLPADDING=0><TR>');
  l_framTarget.document.writeln('      <TD colspan=2><SELECT name="popType"  onFocus="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnFocusDetailHeader();"  onChange="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeDetailType(top.framMainBody.framBodyHeader.document.formDetailBodyHeader)">');

  for (var j=0; j<g_objTypes.miGetNumTypes(); j++){
    if (g_objTypes.arrType[j].mGetdStartDate() == '') {
      if ((g_objTypes.arrType[j].mGetdEndDate() == '') ||
          (g_objTypes.arrType[j].mGetdEndDate().getTime() >= g_dWeekEndingDate.getTime())){
        l_strTypeName = g_objTypes.arrType[j].mGetstrName();
        l_strEndDate = g_objTypes.arrType[j].mGetdEndDate();
        // for debug only - p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '/' + l_strEndDate + '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
        l_framTarget.document.writeln('    <OPTION value="' + l_strTypeName +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
      }
    }
    else {
      if(g_objTypes.arrType[j].mGetdStartDate().getTime() <= g_dWeekEndingDate.getTime()){
        if ((g_objTypes.arrType[j].mGetdEndDate() == '') ||
           (g_objTypes.arrType[j].mGetdEndDate().getTime() >= g_dWeekEndingDate.getTime())){
          l_strTypeName = g_objTypes.arrType[j].mGetstrName();
          l_strEndDate= g_objTypes.arrType[j].mGetdEndDate();
          // for debug only - p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '/' + l_strEndDate +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
          l_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
        }
      }
    }
  }


  l_framTarget.document.writeln('        </SELECT></TD>');
  l_framTarget.document.writeln('    </TR></TABLE></TD>');
  l_framTarget.document.writeln('  </TR>');
  l_framTarget.document.writeln('  <TR><TD>&nbsp;&nbsp;&nbsp;&nbsp;</TD></TR>');

  l_framTarget.document.writeln('  </TABLE>');
  l_framTarget.document.writeln('  </TD>');
  l_framTarget.document.writeln('</TR>');
  l_framTarget.document.writeln('</table>');

  l_framTarget.document.writeln('<!--Hidden fields in this form.--> ');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_NUMBER">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_ID">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_NUMBER">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_ID">');
  l_framTarget.document.writeln('<input type="hidden" name="hidProjID">');
  l_framTarget.document.writeln('<input type="hidden" name="hidTaskID">');

  l_framTarget.document.writeln('</FORM>');
  l_framTarget.document.writeln('</BODY></HTML>');
  l_framTarget.document.close();
  
}

//  --------------------------------------------------
//  Function: fDrawDetailBodyContent
//  Description: Draw detail Body Content
//
//  --------------------------------------------------
function fDrawDetailBodyContent(){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_DETAIL");
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var i; 
  var j=-6;
  var l_objLine;

  var l_framTarget = l_winDetail.framMainBody.framBodyContent;

  l_framTarget.document.open();

  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css">');
  l_framTarget.document.writeln('</HEAD>');

  l_framTarget.document.writeln('<BODY class=panel>');
  l_framTarget.document.writeln('<FORM name="formDetailBodyContent">');
  l_framTarget.document.writeln('<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>');
  l_framTarget.document.writeln('<TR>');
  l_framTarget.document.writeln('  <TD class=colorg5 COLSPAN=3 valign=top>');
  l_framTarget.document.writeln('  <TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0 WIDTH=90%>');

  for (i=0; i<C_iWEEKDAYS; i++){

    l_iDayOfWeek = fDateAddDay(g_objTimecard.objHeader.mGetdWeekEndingDate(), j).getDay();
    l_dDayOfWeek = fDateAddDay(g_objTimecard.objHeader.mGetdWeekEndingDate(), j);

    if(g_objPref.mGetobjWorkDays().bIsWorkDay(l_iDayOfWeek) == true || g_bHourNotOnWorkDay){
      l_strDate = fIntToDayOfWeek(l_iDayOfWeek) + ', ' +  fDateToLongString(g_strSessDateFormat, l_dDayOfWeek, true);
      l_framTarget.document.writeln('  <TR>');
      l_framTarget.document.writeln('    <TD width=20%></TD>');
      l_framTarget.document.writeln('    <TD class=datablack colspan=2>' + l_strDate + '<IMG SRC=' + g_strImagePath + 'FNDPX3.gif height=2 width=100%></TD>');
      l_framTarget.document.writeln('  </TR>');
      l_framTarget.document.writeln('  <TR> ');
      l_framTarget.document.writeln('    <TD width=20%></TD>');
      l_framTarget.document.writeln('    <TD class=promptblackright valign=center>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetstrLabel()+ '</TD>');
      l_framTarget.document.writeln('    <TD class=promptblack><input type=text name="txtDetailHoursY' + l_iDayOfWeek + '" onChange="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeDetailHours(top.framMainBody.framBodyContent.document.formDetailBodyContent, top.framMainBody.framBodyContent.document.formDetailBodyContent.txtDetailHoursY' + l_iDayOfWeek + '.value, ' + l_iDayOfWeek + ');" onFocus="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnFocusLine(top.framMainBody.framBodyContent.document.formDetailBodyContent,' + g_iCurrLineInDetail + ',' + l_iDayOfWeek + ',\'txtDetailHoursY' + l_iDayOfWeek +'\', true);"></TD>');
      l_framTarget.document.writeln('  </TR>');
      l_framTarget.document.writeln('  <TR>');
      l_framTarget.document.writeln('    <TD width=35%></TD> ');
      l_framTarget.document.writeln('    <TD class=promptblackright valign=top>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LINE_COMMENT").mGetstrLabel()+ '</TD>');
      l_framTarget.document.writeln('    <TD class=promptblack><textarea name="txaCommentY' + l_iDayOfWeek + '" rows=3 cols=33 wrap=HARD onChange="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeDetailComment(top.framMainBody.framBodyContent.document.formDetailBodyContent,' + g_iCurrLine + ',' + l_iDayOfWeek +');"  onFocus="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnFocusLine(top.framMainBody.framBodyContent.document.formDetailBodyContent,' + g_iCurrLineInDetail + ',' + l_iDayOfWeek + ',\'txaCommentY' + l_iDayOfWeek +'\', true);"></textarea></TD>');
      l_framTarget.document.writeln('  </TR>');

      if (fbIsDFlexUsed()) {

        var l_bDisable = false;
        l_objLine = g_objTempLines.mobjFindLine(g_iCurrLineInDetail);

	if (l_objLine.mbIsReverseLine()) {
	  l_bDisable = true;
	}
        fPaintDFlex(fGetContSensField(l_objLine), l_framTarget, l_iDayOfWeek, l_bDisable);
      }
      l_framTarget.document.writeln('  <TR><TD>&nbsp;</TD></TR>');
    } 
    j++;
  }
  l_framTarget.document.writeln('  </TABLE>');
  l_framTarget.document.writeln('  </TD>');
  l_framTarget.document.writeln('</TR>');
  l_framTarget.document.writeln('</table>');

  l_framTarget.document.writeln('</FORM>');
  l_framTarget.document.writeln('</BODY></HTML>');
  l_framTarget.document.close();

}


//  --------------------------------------------------
//  Function: fDrawDetailBodyButtons
//  Description: Draw detail Body buttons
//
//  --------------------------------------------------
function fDrawDetailBodyButtons(){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_DETAIL");
  var l_popRow; 

  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody.framBodyButtons;

  l_framTarget.document.open();

  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css">');
  l_framTarget.document.writeln('</HEAD>');

  l_framTarget.document.writeln('<BODY class=panel>');
  l_framTarget.document.writeln('<FORM name="formDetailBodyButtons">');

  l_framTarget.document.writeln('<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>');
  l_framTarget.document.writeln('<TR>');

  l_framTarget.document.writeln('  <TD class=colorg5 width=80%>');
  l_framTarget.document.writeln('  </TD>');

  l_framTarget.document.writeln('  <TD class=colorg5 valign=top align=center>');
  l_framTarget.document.writeln('  <TABLE CELLPADDING=4 CELLSPACING=0 BORDER=0 WIDTH=20%>');
  l_framTarget.document.writeln('  <TR> ');
  l_framTarget.document.writeln('    <TD><A HREF="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickRowBackward();"><IMG SRC=' +  g_strImagePath + 'PAARRL.gif border=0></A></TD>');
  l_framTarget.document.writeln('    <TD class=infoblackright>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_ROW").mGetstrLabel()+ '</TD>');
  l_framTarget.document.writeln('    <TD class=promptblack>');
  l_framTarget.document.writeln('      <SELECT name="popRow" onChange="javascript:' +  g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnChangeRow(popRow.options[popRow.selectedIndex].value)">');
  
  for(var i=g_objTempLines.miGetNumValidReversedLines()+1; i<=g_iTotalDisplayedLines; i++){
    l_framTarget.document.writeln('        <OPTION value=' + i + '>' + i + '</OPTION>');
  }

  l_framTarget.document.writeln('      </SELECT>');
  l_framTarget.document.writeln('    </TD> ');
  l_framTarget.document.writeln('    <TD><A HREF="javascript:' + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 'fOnClickRowForward();"><IMG SRC=' + g_strImagePath + 'PAARRR.gif border=0></A></TD>');
  l_framTarget.document.writeln('  </TR>');

  l_framTarget.document.writeln('  </TABLE>');
  l_framTarget.document.writeln('  </TD>');
  l_framTarget.document.writeln('</TR>');
  l_framTarget.document.writeln('</table>');

  l_framTarget.document.writeln('</FORM>');
  l_framTarget.document.writeln('</BODY></HTML>');
  l_framTarget.document.close();

  l_popRow = g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;
  for(var j=0;  j<l_popRow.options.length; j++){
    if(l_popRow.options[j].value == g_iCurrLine)
      l_popRow.options[j].selected = true;
      g_iPrevLineInDetail = g_iCurrLine;
      g_iCurrLineInDetail = g_iCurrLine;
  }
}

//  --------------------------------------------------
//  Function: fDrawEntryBodyHeader
//  Description: Draws the header section of the Time
//		 Entry page.
//
//  --------------------------------------------------
function fDrawEntryBodyHeader(){
  var l_framTarget = top.framMainBody.framBodyHeader;
  var l_dWeekEndingDate;
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
  var l_objButtonRegion = g_objAKRegions.mobjGetRegion("PA_WEB_BUTTON");
  var l_strHourText;

  if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
    l_strHourText = g_objFNDMsg.mstrGetMsg("PA_WEB_HOURS_AUDIT");
  }
  else {
    l_strHourText = g_objFNDMsg.mstrGetMsg("PA_WEB_HOUR_TEXT");
  }

  l_framTarget.document.open();
  
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css">');
  l_framTarget.document.writeln('</HEAD>');

  l_framTarget.document.writeln('<BODY class=panel onMouseOver="javascript:top.fModalWinOnFocusPreventClick()">');
  l_framTarget.document.writeln('<FORM name="formEntryBodyHeader" method="POST">');

  l_framTarget.document.writeln('  <TABLE CELLSPACING=0 CELLPADDING=2 BORDER=0 WIDTH=98% align=absmiddle>');
  l_framTarget.document.writeln('  <TR>');
  l_framTarget.document.writeln('    <TD class=panel colspan=6 valign=top>');
  l_framTarget.document.writeln('    <TABLE CELLSPACING=0 CELLPADDING=2 BORDER=0 WIDTH=100% align=absmiddle>');
  l_framTarget.document.writeln('    <TR>');
  l_framTarget.document.writeln('      <TD colspan=4><FONT class=fielddata>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_TIMECARD_INFORMATION").mGetstrLabel() + '</FONT><IMG SRC="' + g_strImagePath + 'FNDPX3.gif" width=100% height=2></TD>');
  l_framTarget.document.writeln('    </TR>');
  l_framTarget.document.writeln('    </TABLE>');
  l_framTarget.document.writeln('    </TD>');
  l_framTarget.document.writeln('  </TR>');

  l_framTarget.document.writeln('    <TR>');
  l_framTarget.document.writeln('      <TD class=helptext VALIGN=TOP colspan=6>&nbsp;' + g_objFNDMsg.mstrGetMsg("PA_WEB_TIMECARD_TEXT") + '<IMG SRC="' + g_strImagePath + 'FNDREQUI.gif" align=absmiddle></TD>');
  l_framTarget.document.writeln('    </TR>');
  l_framTarget.document.writeln('    <TR>');
  l_framTarget.document.writeln('      <TD class=promptblackright valign=top><IMG SRC="' + g_strImagePath + 'FNDREQUI.gif">' + l_objCurRegion.mobjGetRegionItem("PA_WEB_NAME").mGetstrLabel() + '</TD>');
  l_framTarget.document.writeln('      <TD>');
  l_framTarget.document.writeln('      <TABLE CELLSPACING=0 CELLPADDING=1><TR>');

  if ((g_objEmployees.miGetNumEmployees() > 1) && (!g_objTimecard.mbIsReverseTimecard()))  {
    l_framTarget.document.writeln('    <TD><SELECT name="popEmployee" onChange="javascript:top.fOnChangeEmployee(top.framMainBody.framBodyHeader.document.formEntryBodyHeader,popEmployee.selectedIndex)" >');
    for(var i=0; i<g_objEmployees.miGetNumEmployees(); i++){
      l_framTarget.document.writeln('	<OPTION value=' + g_objEmployees.arrEmployee[i].mGetiID() + '>' + g_objEmployees.arrEmployee[i].mGetstrName() + ' (' + g_objEmployees.arrEmployee[i].mGetiNumber() + ')');
    }
    l_framTarget.document.writeln('        </SELECT>');
  }
  else {
//    l_framTarget.document.writeln('<TD bgcolor=white>' +  g_objEmployees.arrEmployee[0].mGetstrName() + ' (' + g_objEmployees.arrEmployee[0].mGetiNumber() + ')');
    var l_objEmployee = g_objEmployees.mobjFindEmployee(g_objTimecard.objHeader.mGetobjEmployee().mGetiID());
    l_framTarget.document.writeln('<TD bgcolor=white>' +  l_objEmployee.mGetstrName() + ' (' + l_objEmployee.mGetiNumber() + ')');
  }

  l_framTarget.document.writeln('        </TD></TR>');
  l_framTarget.document.writeln('      </TABLE>');
  l_framTarget.document.writeln('      </TD>');
  l_framTarget.document.writeln('      <TD class=promptblackright valign=top><IMG NAME=tmp SRC="' + g_strImagePath + 'FNDREQUI.gif">' + l_objCurRegion.mobjGetRegionItem("PA_WEB_WEEK_ENDING").mGetstrLabel() + '</TD>');

  l_framTarget.document.writeln('      <TD><TABLE CELLSPACING=0 CELLPADDING=1><TR>');
  if (!g_objTimecard.mbIsReverseTimecard()) {
   
    l_framTarget.document.writeln('      <TD><SELECT name="popWeekEndingDate" onChange="javascript:top.fOnChangeWeekEndingDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, false)">');
    l_framTarget.document.writeln('        <OPTION>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        <OPTION>');
    l_framTarget.document.writeln('        </SELECT></TD>');
    l_framTarget.document.writeln('        <TD><A HREF="javascript:top.fOpenCalWin();" ')
    l_framTarget.document.writeln('          onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_CALENDAR").mGetstrLabel() + '\';return true"> ');
    l_framTarget.document.writeln('         <IMG SRC="' + g_strImagePath + 'FNDICLDR.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_CALENDAR").mGetstrLabel() + '" border=no></A></TD>');
  }
  else {
    l_framTarget.document.writeln('<TD bgcolor=white>'+fDateToLongString(g_strSessDateFormat, g_dWeekEndingDate, true) + '</TD>');
  }

  l_framTarget.document.writeln('      </TR></TABLE></TD>');
  l_framTarget.document.writeln('    </TR>');
  l_framTarget.document.writeln('    <TR>');

  if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER") ==C_strYES) {
    l_framTarget.document.writeln('      <TD class=promptblackright valign=top>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_OVERRIDING_APPROVER").mGetstrLabel() + '</TD>');
    l_framTarget.document.writeln('      <TD valign=top>');
    l_framTarget.document.writeln('      <TABLE CELLSPACING=0 CELLPADDING=0>');
    l_framTarget.document.writeln('      <TR>');
    l_framTarget.document.writeln('        <TD class=promptblack><input type=text size='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_OVERRIDING_APPROVER").mGetiLength() + ' name="txtDefApprName" onChange="javascript:top.fOnChangeApprover(top.framMainBody.framBodyHeader.document.formEntryBodyHeader);"  value=""></TD>');
    l_framTarget.document.writeln('        <TD valign=center><A HREF="javascript:top.fOnClickApproverLOV();"');
    l_framTarget.document.writeln('          onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '\';return true">');
    l_framTarget.document.writeln('          <IMG src="' + g_strImagePath + 'FNDILOV.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '" height=18 width=16 border=no></A></TD>');
    l_framTarget.document.writeln('      </TR>');
    l_framTarget.document.writeln('      </TABLE>');
    l_framTarget.document.writeln('      </TD>');
  }
  else {
    l_framTarget.document.writeln('      <TD></TD>');
    l_framTarget.document.writeln('      <TD></TD>');
  }

  l_framTarget.document.writeln('      <TD class=promptblackright valign=top width=40%>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_GENERAL_COMMENT").mGetstrLabel() + '</TD>');
  l_framTarget.document.writeln('      <TD><TABLE><TR>');
  l_framTarget.document.writeln('        <TD><textarea name="txaComment" rows=2 cols='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_GENERAL_COMMENT").mGetiLength() + '  wrap=HARD onChange="javascript:top.fOnChangeHeaderComment(top.framMainBody.framBodyHeader.document.formEntryBodyHeader.txaComment.value)" ></textarea></TD>');
  l_framTarget.document.writeln('        <TD></TD>');
  l_framTarget.document.writeln('      </TR></TABLE></TD>');
  l_framTarget.document.writeln('    </TR>');
  
  l_framTarget.document.writeln('    <!-- Start of the Enter Hours section-->');
  l_framTarget.document.writeln('    <TR>');
  l_framTarget.document.writeln('      <TD colspan=4><FONT class=fielddata>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_ENTER_HOURS").mGetstrLabel() + '</FONT><IMG SRC="' + g_strImagePath + 'FNDPX3.gif" width=100% height=2></TD>');
  l_framTarget.document.writeln('    </TR>');

  l_framTarget.document.writeln('    <TR>');
  l_framTarget.document.writeln('      <TD colspan=4>');
  l_framTarget.document.writeln('      <TABLE width=100% cellpadding=2 cellspacing=0 border=0>');
  l_framTarget.document.writeln('      <TR>');
  l_framTarget.document.writeln('      <TD class=helptext VALIGN=TOP>' + l_strHourText + '</TD>');
  l_framTarget.document.writeln('      <TD width=10% valign=bottom>');

  fDrawButton(l_framTarget, C_strROUND_ROUND,
  l_objButtonRegion.mobjGetRegionItem('PA_WEB_DETAILS_COMMENT').mGetstrLabel(), "top.fOpenDetailWin()", C_bBUTTON_ENABLED, C_strGRAYBG, C_strNARROW_GAP);

  l_framTarget.document.writeln('      </TD>');
  l_framTarget.document.writeln('      </TR>');
  l_framTarget.document.writeln('      </TABLE>');
  l_framTarget.document.writeln('      </TD>');
  l_framTarget.document.writeln('    </TR>');
  l_framTarget.document.writeln('    </TABLE>');

  l_framTarget.document.writeln('<!--Hidden fields in this form.--> ');
  l_framTarget.document.writeln('<input type="hidden" name="hidDefApprID">');

  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_APPR_NAME">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_APPR_ID">');

  l_framTarget.document.writeln('</FORM>');
  l_framTarget.document.writeln('</BODY>');
  l_framTarget.document.writeln('</HTML>');
  
  l_framTarget.document.close();
}

//  --------------------------------------------------
//  Function: fDrawEntryBodyContent
//  Description: This is the wrapper function that draws 
//		 the line of the multi-row table 
//
//  --------------------------------------------------
function fDrawEntryBodyContent(){
  var l_framTarget = top.framMainBody.framBodyContent;
  var l_iTotalDisplayedLines = g_iTotalDisplayedLines;

  l_framTarget.document.open();
  
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css">');
  l_framTarget.document.writeln('</HEAD>');

  l_framTarget.document.writeln('<BODY class=panel onFocus="javascript:top.fModalWinOnFocusPreventClick()">');
  l_framTarget.document.writeln('<FORM name="formEntryBodyContent">');

  fDrawTableTopBorder(l_framTarget);
  fDrawTableTitle(l_framTarget);
  var l_iNumRevLines = g_objTimecard.objLines.miGetNumValidReversedLines();
  for (var i=1; i<=l_iTotalDisplayedLines; i++){
    if (i<=l_iNumRevLines)
      var l_bReverseLine = true;
    else 
      var l_bReverseLine = false;
    fDrawTableLine(l_framTarget, i, 'US', l_bReverseLine); 
  }
  fDrawTableTotal(l_framTarget);
  fDrawTableBottomBorder(l_framTarget);

  l_framTarget.document.writeln('<!--Hidden fields in this form.--> ');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_NUMBER">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_PROJECT_ID">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_NUMBER">');
  l_framTarget.document.writeln('<input type="hidden" name="PA_WEB_TASK_ID">');
  l_framTarget.document.writeln('</FORM>');
  l_framTarget.document.writeln('</BODY>');
  l_framTarget.document.writeln('</HTML>');

  l_framTarget.document.close();
}

//  --------------------------------------------------
//  Function: fDrawEntryBodyButtons
//  Description: Draw the total line of the multi-row
//		 table. This section is not scrollable. 
//
//  --------------------------------------------------
function fDrawEntryBodyButtons(){
  var l_framTarget      = top.framMainBody.framBodyButtons;
  var l_objCurRegion    = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
  var l_objButtonRegion = g_objAKRegions.mobjGetRegion("PA_WEB_BUTTON");
  var l_strAliasName;

  l_framTarget.document.open();
  
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD>');
  l_framTarget.document.writeln('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  l_framTarget.document.writeln('</HEAD>');

  //l_framTarget.document.writeln('<BASE HREF="' +  top.g_strSessBaseHRef  + '">');

  l_framTarget.document.writeln('<BODY class=panel onFocus="javascript:top.fModalWinOnFocusPreventClick()">');
  l_framTarget.document.writeln('<FORM name="formEntryBodyButtons" method="POST">');

  l_framTarget.document.writeln('<table width=100% cellpadding=3 cellspacing=0 border=0>');
  l_framTarget.document.writeln('<TR>');
  l_framTarget.document.writeln('<TD class=panel>');
  l_framTarget.document.writeln('  <TABLE cellpadding=0 cellspacing=0 border=0 width=50%>');
  l_framTarget.document.writeln('  <TR> ');
  l_framTarget.document.writeln('      <TD>&nbsp;&nbsp</TD>');
  l_framTarget.document.writeln('      <TD>');

  fDrawButton(l_framTarget, C_strROUND_ROUND, l_objButtonRegion.mobjGetRegionItem("PA_WEB_CLEAR_LINE").mGetstrLabel(), "top.fOnClickClearLine()",
  C_bBUTTON_ENABLED, C_strGRAYBG, C_strNO_GAP);

  l_framTarget.document.writeln('      </TD>');
  l_framTarget.document.writeln('      <TD>');

  fDrawButton(l_framTarget, C_strROUND_ROUND, l_objButtonRegion.mobjGetRegionItem("PA_WEB_COPY_LINE").mGetstrLabel(), "top.fOnClickCopyLine()",
  C_bBUTTON_ENABLED, C_strGRAYBG, C_strNO_GAP);

  l_framTarget.document.writeln('      </TD>');
  l_framTarget.document.writeln('      <TD><IMG SRC="' + g_strImagePath + 'FNDPXG5.gif" width=25 height=1></TD>');

  l_framTarget.document.writeln('          <TD>');
  l_framTarget.document.writeln('          <TABLE><TR><TD>');

  fDrawButton(l_framTarget, C_strROUND_ROUND, l_objButtonRegion.mobjGetRegionItem("PA_WEB_APPLY_ALIAS").mGetstrLabel(), "top.fOnClickApplyAlias(top.framMainBody.framBodyButtons.document.formEntryBodyButtons)", C_bBUTTON_ENABLED, C_strGRAYBG, C_strNARROW_GAP);

  l_framTarget.document.writeln('          </TD>');
        
  l_framTarget.document.writeln('          <TD>');

  l_framTarget.document.writeln('	  <SELECT name="popAlias">');

  for(var i=0; i<g_objPref.mGetobjAliases().miGetNumAliases(); i++) {
    l_strAliasName = g_objPref.mGetobjAliases().mGetarrAlias()[i].mGetstrName();
    if(l_strAliasName != '') {
      l_framTarget.document.writeln('    <option>' + l_strAliasName + '');
    }
  }

  l_framTarget.document.writeln('	</OPTION></SELECT>');
  l_framTarget.document.writeln('          </TD>');
  l_framTarget.document.writeln('          </TR>');
  l_framTarget.document.writeln('          </TABLE>');
  l_framTarget.document.writeln('          </TD>');

  l_framTarget.document.writeln('  </TR>');
  l_framTarget.document.writeln('  </TABLE>');
  
  l_framTarget.document.writeln('</TD>');
  l_framTarget.document.writeln('</TR>');
  l_framTarget.document.writeln('</table>');
  l_framTarget.document.writeln('</FORM>');

  if (fIsNetscape()) {
    l_framTarget.document.writeln('<LAYER ID=LayTotalText STYLE="visibility=hidden;"></LAYER>');
  } 
  else {
    l_framTarget.document.writeln('<DIV ID=LayTotalText STYLE="POSITION:absolute"></DIV>');
  }

  l_framTarget.document.writeln('<FORM Name=formSaveSubmitFReview method="POST" ACTION="' + top.g_strSessDAD  + '/PA_SELF_SERVICE_HANDLER_PVT.From_SaveSubmitFReview" TARGET="framMainBody">');

  l_framTarget.document.writeln('<input type="hidden" name="hidTCHeader">');
  l_framTarget.document.writeln('<input type="hidden" name="hidTCLines">');
  l_framTarget.document.writeln('<input type="hidden" name="hidAuditHistory">');
  l_framTarget.document.writeln('<input type="hidden" name="hidFromPage">');
  l_framTarget.document.writeln('<input type="hidden" name="hidAction">');
  l_framTarget.document.writeln('</FORM>');

  l_framTarget.document.writeln('</BODY>');
  l_framTarget.document.writeln('</HTML>');

  l_framTarget.document.close();

}


//  --------------------------------------------------
//  Function: fDrawTableLine
//  Description: Draws the line of the multi-row table 
//
//  --------------------------------------------------
function fDrawTableLine(p_framTarget, p_iLineNumber, p_strLanguageCode, p_bReadOnly){
  var l_bMarkCurrLine = false;
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
  var l_strExpTypes = '';
  var l_strReadOnly = "";
  var l_strDisabled = "";

  g_iPrevLine = g_objTimecard.mGetobjLines().miFindFirstNonReversalLine();
  if (g_iPrevLine == p_iLineNumber){
    l_bMarkCurrLine = true; 
    g_iCurrLine = p_iLineNumber;
  }

  if (p_bReadOnly) {
    l_strReadOnly = "READONLY";
    l_strDisabled = "DISABLED";
  }

  p_framTarget.document.writeln('        <TR>');

  if( l_bMarkCurrLine == true) {
    p_framTarget.document.writeln('          <TD class=tablecell>&nbsp<IMG NAME="imgCurrLineIndicator' + p_iLineNumber + '" SRC="' + g_strImagePath + 'PAFAROW.gif">&nbsp;' + p_iLineNumber + '&nbsp;</TD>');
  }
  else {
    p_framTarget.document.writeln('          <TD class=tablecell>&nbsp<IMG NAME="imgCurrLineIndicator' + p_iLineNumber + '" SRC="' + g_strImagePath + 'PANOAROW.gif">&nbsp;' + p_iLineNumber + '&nbsp;</TD>');
  }

  p_framTarget.document.writeln('          <TD  class=tablecell>');
  p_framTarget.document.writeln('          <TABLE border=0>');
  p_framTarget.document.writeln('          <TR>');
  p_framTarget.document.writeln('            <TD><INPUT name="txtProjNum' + p_iLineNumber + '" type=text size=' +  l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT").mGetiLength() + '  onFocus="javascript:top.fOnFocusLine(top.framMainBody.framBodyContent.document.formEntryBodyContent, ' + p_iLineNumber + ',\'\',\'txtProjNum' + p_iLineNumber + '\', false);" onChange="javascript:top.fOnChangeProject(top.framMainBody.framBodyContent.document.formEntryBodyContent, false);" ' + l_strReadOnly + '></TD>');
  p_framTarget.document.writeln('            <TD> <A href="javascript:top.fOnClickProjectLOV(' + p_iLineNumber + ', true)" ');
  p_framTarget.document.writeln('           onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '\';return true">');
  p_framTarget.document.writeln('            <IMG SRC="' + g_strImagePath + 'FNDILOV.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '" border=0 width=16 height=18 align="absmiddle"></A></TD>');
  p_framTarget.document.writeln('          </TR>');
  p_framTarget.document.writeln('          </TABLE>');
  p_framTarget.document.writeln('          </TD>');
  p_framTarget.document.writeln('          <TD class=tablecell> ');
  p_framTarget.document.writeln('          <TABLE border=0>');
  p_framTarget.document.writeln('          <TR>');
  p_framTarget.document.writeln('            <TD><INPUT name="txtTaskNum' + p_iLineNumber + '" size=' +  l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK").mGetiLength() + ' type=text onFocus="javascript:top.fOnFocusLine(top.framMainBody.framBodyContent.document.formEntryBodyContent,'  + p_iLineNumber + ',\'\', \'txtTaskNum' + p_iLineNumber + '\', false);" onChange="javascript:top.fOnChangeTask(top.framMainBody.framBodyContent.document.formEntryBodyContent, false)" ' + l_strReadOnly + '></TD>');

  p_framTarget.document.writeln('            <TD> <A href="javascript:top.fOnClickTaskLOV(' + p_iLineNumber + ', true)" ');
  p_framTarget.document.writeln('           onMouseOver="window.status=\'' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '\';return true">');
  p_framTarget.document.writeln('            <IMG SRC="' + g_strImagePath + 'FNDILOV.gif" alt="' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LIST_OF_VALUES").mGetstrLabel() + '" border=0 width=16 height=18 align="absmiddle"></A></TD>');
  p_framTarget.document.writeln('          </TR>');
  p_framTarget.document.writeln('          </TABLE>');
  p_framTarget.document.writeln('          </TD>');
  p_framTarget.document.writeln('          <TD class=tablecell>');
  p_framTarget.document.writeln('	  <SELECT name="popType' +  p_iLineNumber + '"  onFocus="javascript:top.fOnFocusLine(top.framMainBody.framBodyContent.document.formEntryBodyContent,' + p_iLineNumber + ',\'\',\'popType' + p_iLineNumber + '\',false);" onChange="javascript:top.fonChangeType(top.framMainBody.framBodyContent.document.formEntryBodyContent)" ' + l_strDisabled + '>');

  for (var j=0; j<g_objTypes.miGetNumTypes(); j++){
    if (g_objTypes.arrType[j].mGetdStartDate() == '') {
      if ((g_objTypes.arrType[j].mGetdEndDate() == '') ||
	  (g_objTypes.arrType[j].mGetdEndDate().getTime() >= g_dWeekEndingDate.getTime())){
        l_strTypeName = g_objTypes.arrType[j].mGetstrName();
        l_strEndDate = g_objTypes.arrType[j].mGetdEndDate();
        // for debug only - p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '/' + l_strEndDate +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
        //p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
        l_strExpTypes += '    <OPTION value="' + l_strTypeName +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>';
      }
    }
    else {
      if(g_objTypes.arrType[j].mGetdStartDate().getTime() <= g_dWeekEndingDate.getTime()){
        if ((g_objTypes.arrType[j].mGetdEndDate() == '') ||
	   (g_objTypes.arrType[j].mGetdEndDate().getTime() >= g_dWeekEndingDate.getTime())){
          l_strTypeName = g_objTypes.arrType[j].mGetstrName();
          l_strEndDate= g_objTypes.arrType[j].mGetdEndDate();
          // for debug only - p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '/' + l_strEndDate +  '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
          //p_framTarget.document.writeln('    <OPTION value="' + l_strTypeName + '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>');
          l_strExpTypes += '    <OPTION value="' + l_strTypeName + '">' + l_strTypeName.substring(0, l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetiLength()) + '</OPTION>';
        }
      }
    }
  }

  p_framTarget.document.writeln(l_strExpTypes);
  p_framTarget.document.writeln('    </SELECT></TD>');

  fDrawWorkDays(g_objPref, g_objTimecard.objHeader, p_framTarget, 'Table Line', p_iLineNumber, p_bReadOnly);

  p_framTarget.document.writeln('  <TD class=tablecell><INPUT type=text size=3 name="txtHourTotal' + p_iLineNumber + '" onFocus="this.blur();" READONLY></TD>');
  p_framTarget.document.writeln('<input type="hidden" name="hidProjID' + p_iLineNumber + '">');
  p_framTarget.document.writeln('<input type="hidden" name="hidTaskID' + p_iLineNumber + '">');
  p_framTarget.document.writeln('</TR><TR></TR>');

  l_bMarkCurrLine = false;  

}

//  --------------------------------------------------
//  Function: fDrawTableTitle
//  Description: Draws the title of the multi-row table. 
//
//  --------------------------------------------------
function fDrawTableTitle(p_framTarget){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");

  p_framTarget.document.writeln('      <TABLE cellpadding=0 cellspacing=0 border=0 width=100%>');
  p_framTarget.document.writeln('      <TR>');
  p_framTarget.document.writeln('        <TD colspan=4></TD>');

  fDrawWorkDays(g_objPref, g_objTimecard.objHeader, p_framTarget, 'Table Title1', '', false);

  p_framTarget.document.writeln('      </TR>');
  p_framTarget.document.writeln('      <TR>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER></TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetstrLabel() + '</TD>');

  fDrawWorkDays(g_objPref, g_objTimecard.objHeader, p_framTarget, 'Table Title2', '', false);

  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LINE_TOTAL").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('      </TR>');
}

//  --------------------------------------------------
//  Function: fDrawTableTotal
//  Description: Draws the total line of the multi-row table. 
//
//  --------------------------------------------------
function fDrawTableTotal(p_framTarget){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
  var l_objButtonRegion = g_objAKRegions.mobjGetRegion("PA_WEB_BUTTON");

  p_framTarget.document.writeln('        <!-- Start of the total line-->');
  p_framTarget.document.writeln('        <TR>');


  p_framTarget.document.writeln('      <TD class=tablecellright  colspan=2 >');

  fDrawButton(p_framTarget, C_strROUND_ROUND, l_objButtonRegion.mobjGetRegionItem("PA_WEB_ADD_LINES").mGetstrLabel(), "top.fOnClickAddMoreLines()", C_bBUTTON_ENABLED, C_strGRAYBG, C_strNO_GAP);

  p_framTarget.document.writeln('      </TD>');

  //p_framTarget.document.writeln('      <TD class=TABLECELL>');
  //p_framTarget.document.writeln('      </TD>');

  p_framTarget.document.writeln('          <TD class=tablecellright colspan=2><font class=fielddata>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_LINE_TOTAL").mGetstrLabel() + '&nbsp</font></TD>');

  fDrawWorkDays(g_objPref, g_objTimecard.objHeader, p_framTarget, 'Total Line', '', false)

  p_framTarget.document.writeln('          <TD class=tablecell><INPUT type=text size=3 name="txtGrandTotal" value="" onFocus="this.blur();" READONLY></TD>');
  p_framTarget.document.writeln('        </TR>');
  p_framTarget.document.writeln('        <!--End of the total line -->');
  p_framTarget.document.writeln('    </TABLE>');

}

function fDrawTimeEntry2(){
 
  fDrawTimeEntry(); 
  setTimeout('fPopulateData()', 200);

}

//  --------------------------------------------------
//  Function: fDrawTimeEntry
//  Description: It draws the Time Entry page
//
//  --------------------------------------------------
function fDrawTimeEntry(){
  var l_framTarget = top.framMainBody;

  fDrawToolbarFrame(top.framToolbar, C_strENTER_HOURS);

  l_framTarget.document.open();
  l_framTarget.document.writeln(fDrawContentFrameSet(C_strENTER_HOURS));
  l_framTarget.document.close();

  fDrawButtonFrame(top.framButtons, C_strENTER_HOURS, g_strCurrentAction);

  // Bug 1534664. Need to explicitly draw here, otherwise
  // we get "Action Cancelled" message on Win95/IE5.
  if (fIsIE5() && fIsWindows95()) {
    fDrawEntryBodyHeader();
    fDrawEntryBodyContent();
    fDrawEntryBodyButtons();
  }
}

//  --------------------------------------------------
//  Function: fPopulateData
//  Description: Populates all the necessary data after
//		 time entry page is rendered.
//
//  --------------------------------------------------
function fPopulateData(){

  if (!g_bFromTimeEntryToReversal) {
    if (!g_objTimecard.mbIsReverseTimecard()) {
      setTimeout("fPopulateWeekEndingDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, g_iPopWeekEndingDateIndex)", 200);
    }

    setTimeout("fPopulateTimeEntry(top.framMainBody.framBodyHeader.document.formEntryBodyHeader)", 200);
 
    if (g_bLinesEntered == true) {
      setTimeout("fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent)" , 200);
    }
    else {
      setTimeout("fSetLayer('LayTotalText', g_objTimecard.objLines.miGetNumEnteredLines(), '0.00')", 200);
    }
  }
}


//  ---------------------------------------------------------------------------------
//  Function: fDrawWorkDays
//  Description:
//
//  ---------------------------------------------------------------------------------
function fDrawWorkDays(p_objPref, p_objHeader, p_framTarget, p_strFromLocation, p_iLineNumber, p_bReadOnly){
var l_strTABLE_TITLE1 = 'Table Title1';
var l_strTABLE_TITLE2 = 'Table Title2';
var l_strTABLE_LINE   = 'Table Line';
var l_strTOTAL_LINE   = 'Total Line';
var j=-6;
var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
var l_strReadOnly = "";

  if (p_bReadOnly) {
    l_strReadOnly = "READONLY";
  }
  for (i=0; i<C_iWEEKDAYS; i++){
    l_iDayOfWeek = fDateAddDay(p_objHeader.mGetdWeekEndingDate(), j).getDay();
    l_dDayOfWeek = fDateAddDay(p_objHeader.mGetdWeekEndingDate(), j);
    if ( (p_objPref.mGetobjWorkDays().bIsWorkDay(l_iDayOfWeek) == true) ||
	g_bHourNotOnWorkDay ) {
      if(p_strFromLocation == l_strTABLE_TITLE1){
        l_strDayOfWeek = fIntToDayOfWeek(l_iDayOfWeek);   
        p_framTarget.document.writeln('  <TD class=TABLEROWHEADER size='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetiLength() + '>' + l_strDayOfWeek + '</TD>');
      }
      else if (p_strFromLocation == l_strTABLE_TITLE2){ 
        l_strDate = fDateToShortString(g_strSessDateFormat, l_dDayOfWeek, true);
        p_framTarget.document.writeln('  <TD class=TABLEROWHEADERLIGHT size='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetiLength() + '>' + l_strDate + '</TD>');
      }
      else if (p_strFromLocation == l_strTABLE_LINE){
        p_framTarget.document.writeln('  <TD class=tablecell ><input type=text size='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetiLength() + ' name="txtHourEntryX' + p_iLineNumber + 'Y' + l_iDayOfWeek + '"onchange="javascript:top.fOnChangeHourEntry(top.framMainBody.framBodyContent.document.formEntryBodyContent,top.framMainBody.framBodyContent.document.formEntryBodyContent.txtHourEntryX' + p_iLineNumber + 'Y' + l_iDayOfWeek + '.value, ' + p_iLineNumber + ',' + l_iDayOfWeek + ');" onFocus="javascript:top.fOnFocusLine(top.framMainBody.framBodyContent.document.formEntryBodyContent,' + p_iLineNumber + ',' + l_iDayOfWeek + ',\'txtHourEntryX' + p_iLineNumber + 'Y' + l_iDayOfWeek +'\', false);"  ' + l_strReadOnly + '></TD>');

      }
      else if(p_strFromLocation == l_strTOTAL_LINE ){
        p_framTarget.document.writeln('  <TD class=tablecell><input name="txtDayTotal' + l_iDayOfWeek + '"type=text size='+ l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetiLength() + ' value=" " onFocus="this.blur();" READONLY></TD>');
      }
    }
    j++;
  }
}

//  --------------------------------------------------
//  Function: fKeepAllLine
//  Description: Keep all the entered lines if the
//		 user chose to keep after confirmation
//  --------------------------------------------------
function fKeepAllLine() {

  var l_NumofLines = g_objTimecard.objLines.miGetNumLines();
  var l_popWeekEndingDate = top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate;

  g_iPopWeekEndingDateIndex = l_popWeekEndingDate.selectedIndex;
 
  // Save selected value to the data structure
  g_objTimecard.objHeader.dWeekEndingDate = new Date(l_popWeekEndingDate.options[l_popWeekEndingDate.selectedIndex].value * 1);

  fPopulateWeekEndingWeekArr();

  if (fIsNetscape() || fIsIE5()) {
    fRefreshEntryBody();
  } else {
    // Reload the display. See BUG 1289975
    top.framMainBody.location.reload();
  }
}


//  --------------------------------------------------
//  Function: fIsCellValid
//  Description: Validation routine used to check each
//               hour cell entry when either the save
//		 or final review button is clicked.
//
//  --------------------------------------------------
function fIsCellValid(p_objLine, p_bAllowNegative, p_objDay, p_objError, p_iDay){
  var l_iInputValue;
  var l_strText = top.g_objFNDMsg.mstrGetMsg('PA_WEB_POS_NUM');

  if(p_iDay != ''){
    for (var i=0; i<g_dWeekEndingWeek.length; i++){
      if(top.g_dWeekEndingWeek[i].getDay() == p_iDay){
        l_dDay = top.g_dWeekEndingWeek[i];
      }
    }
  }

  if (p_objDay.mGetnHours() != ''){
    if (fIsNum(p_objDay.mGetnHours())==false){
      p_objError.mobjAddCellError(p_objLine.mGetiDisplayLineNum(), l_dDay, g_objFNDMsg.mstrGetMsg("PA_WEB_NUM_FORMAT"), '');
    }
  }

  if (p_bAllowNegative == C_strNO){
    l_iInputValue = p_objDay.mGetnHours();
    if ( fCheckPosNum(l_iInputValue, false) == false){
    l_strText = top.g_objFNDMsg.fstrReplToken(
                        l_strText,
                        '&Number',
                        l_iInputValue);
      p_objError.mobjAddCellError(p_objLine.mGetiDisplayLineNum(), l_dDay, l_strText);
    }
  }

}

//  --------------------------------------------------
//  Function: fIsLineValid
//  Description: Validation routine used to check each
//               line entry when either the save
//		 or final review button is clicked.
//
//  --------------------------------------------------
function fIsLineValid(p_objLine, p_objError){
  var l_bHourFilled = false;
  var l_iLineNumber = p_objLine.mGetiDisplayLineNum();
  var k = -6;

  // Is the project field null?
  if (p_objLine.mGetstrProjectNum() == ''){
    p_objError.mobjAddLineError(l_iLineNumber, g_objFNDMsg.mstrGetMsg("PA_WEB_NO_PROJECT"), 'Project Number');
  }

  // Is the task field null?
  if (p_objLine.mGetstrTaskNum() == ''){
    p_objError.mobjAddLineError(l_iLineNumber, g_objFNDMsg.mstrGetMsg("PA_WEB_NO_TASK"), 'Task Number');
  }

  // Is the expenditure field null?
  if (p_objLine.objType.mGetstrName() == ''){
    p_objError.mobjAddLineError(l_iLineNumber, g_objFNDMsg.mstrGetMsg("PA_WEB_NO_TYPE"), 'Expenditure Type');
  }

  // At least one hour per line is required
  for (var i=0; i<C_iWEEKDAYS; i++){   
    if((g_objPref.mGetobjWorkDays().bIsWorkDay(i) == true) || g_bHourNotOnWorkDay) {
      if (p_objLine.mGetobjDays().arrDay[i].mGetnHours() != ''){   
        fIsCellValid( p_objLine, g_objProfOpt.mvGetProfOpt("PA_SST_ALLOW_NEGATIVE_TXN"), p_objLine.mGetobjDays().arrDay[i] , p_objError, i);
        l_bHourFilled = true;
      }
    }
  }

  if (l_bHourFilled == false){
    p_objError.mobjAddGeneralError(g_objFNDMsg.mstrGetMsg("PA_WEB_NO_HOUR"), C_strHEADER );
  }

  // Is the hours entered  within the active dates of the expenditure type?
  for (var j=0; j<C_iWEEKDAYS; j++){
    if (p_objLine.mGetobjDays().arrDay[j].nHours > 0){
      if (p_objLine.mGetobjType().mGetdStartDate() != ''){
        if (p_objLine.mGetobjType().mGetdStartDate().getTime() > 0) {
          if (p_objLine.mGetobjType().mGetdStartDate().getTime() >
             g_objTimecard.objHeader.dWeekEndingDate.getTime()){
             p_objError.mobjAddCellError(l_iLineNumber, top.g_dWeekEndingWeek[j], g_objFNDMsg.mstrGetMsg("PA_WEB_INVALID_DATE_RANGE"));
          }  
        }
      }
      if (p_objLine.mGetobjType().mGetdEndDate() != '') {
        if (p_objLine.mGetobjType().mGetdEndDate().getTime() > 0){
          if (p_objLine.mGetobjType().mGetdEndDate().getTime() < 
           g_objTimecard.objHeader.dWeekEndingDate.getTime()){
             p_objError.mobjAddCellError(l_iLineNumber, top.g_dWeekEndingWeek[j], g_objFNDMsg.mstrGetMsg("PA_WEB_INVALID_DATE_RANGE"));
          }
        }
      }
    }   
  }

  // Validate DFlex
  if (fbIsDFlexUsed()) {
    for (var j=0; j<C_iWEEKDAYS; j++) {
      var l_objDay = p_objLine.mGetobjDays().arrDay[j];
      if (l_objDay.mGetnHours() != "") 
        fValidateDFlex(fGetContSensField(p_objLine), l_objDay.arrDFlex, 
                       p_objError, l_iLineNumber, fJsIndexToGlobalWeekDate(j));

    }   
  }
}

//  --------------------------------------------------
//  Function: fIsFormValid
//  Description: Validation starts here when either the save
//		 or final review button is clicked.
//
//  --------------------------------------------------
function fIsFormValid(){
var l_iNumOfLines = 0;
var l_iMinNumOfLines = 1;


  // Start validation
  g_objErrors = new Errors();

  // 1. Check lines page
  for (var i=0; i<g_objTimecard.objLines.miGetNumLines(); i++){
    if ((g_objTimecard.objLines.arrLine[i].bIsDeleted != true) && 
       (g_objTimecard.objLines.arrLine[i].bIsEmpty != true)) {
      top.fIsLineValid(g_objTimecard.objLines.arrLine[i], g_objErrors);
      l_iNumOfLines += 1;
    }
  }

  if (l_iNumOfLines < l_iMinNumOfLines) {
    g_objErrors.mobjAddGeneralError(g_objFNDMsg.mstrGetMsg("PA_WEB_NO_HOUR"), C_strHEADER );
  }

  if (g_objErrors.miGetNumOfErrors() > 0 ) {
    fOpenErrorWin(g_objErrors, 'top.framMainBody.framBodyHeader.document.formEntryBodyHeader', 'top.framMainBody.framBodyContent.document.formEntryBodyContent');
  }
}


//  --------------------------------------------------
//  Function: fCancelChanges
//  Description: don't touch the entered lines if the
//		 user select to cancel from confirmation
//
//  --------------------------------------------------
function fCancelChanges() {

  var l_popWeekEndingDate = top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate;

  l_popWeekEndingDate.options[g_iPopWeekEndingDateIndex].selected = true;
}

//  --------------------------------------------------
//  Function: fOnChangeRow
//  Description: 
//
//  --------------------------------------------------
function fOnChangeRow(p_iSelectedLine){
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal(); 

  fCodeForExplorerDetail();

  g_iPrevLineInDetail = g_iCurrLineInDetail;
  g_iCurrLineInDetail = p_iSelectedLine;

  // Determine whether change made to DFlex fields
  fDetermineDFlexChanged();

  if (fGetContSensField(g_objTempLines.mobjFindLine(g_iCurrLineInDetail)) != fGetContSensField(g_objTempLines.mobjFindLine(g_iPrevLineInDetail))) 
    fDrawDetailBodyContent(l_winDetail.framMainBody.framBodyContent);

  fPopulateDetail();

}


//  --------------------------------------------------
//  Function: fPopulateWeekEndingWeekArr
//  Description: Populate global variable: g_dWeekEndingWeek
//
//  --------------------------------------------------
function fPopulateWeekEndingWeekArr(){
var j=-6;
var k=0;

  for (i=0; i<C_iWEEKDAYS; i++){
    l_iDayOfWeek = fDateAddDay(g_objTimecard.objHeader.mGetdWeekEndingDate(), j).getDay();
    l_dDayOfWeek = fDateAddDay(g_objTimecard.objHeader.mGetdWeekEndingDate(), j);
    g_dWeekEndingWeek[i] = l_dDayOfWeek;
    j++;
  }
}

//  --------------------------------------------------
//  Function: fSetLayer
//  Description: 
//miGetNumNotEmptyLines
//  --------------------------------------------------
function fSetLayer(p_strLayerName, p_iTotalLinesEntered, p_strGrandTotal){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_TIME_ENTRY");
  var l_strTotalLinesEntered =  l_objCurRegion.mobjGetRegionItem("PA_WEB_TOTAL_LINES_ENTERED").mGetstrLabel();
  var l_strTotal =  l_objCurRegion.mobjGetRegionItem("PA_WEB_LINE_TOTAL").mGetstrLabel();
  var l_strHours =  l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetstrLabel();

  if (p_strGrandTotal == '') p_strGrandTotal = '0.00';
  var l_layerText =  l_strTotalLinesEntered + ': ' + p_iTotalLinesEntered + l_strTotal +': '  + p_strGrandTotal;
  var l_strLayerText = '<font class=promptblack>' + l_strTotalLinesEntered + ': </font>';
      l_strLayerText += ' <font class=datablack>' + p_iTotalLinesEntered + '</font>';
      l_strLayerText += ' <font class=promptblack>&nbsp;&nbsp;&nbsp;'  + l_strTotal + ': </font>';
      l_strLayerText += ' <font class=datablack>' + p_strGrandTotal + ' ' +  l_strHours + '</font>';
  var l_iLayerTextWidth = l_layerText.length * 7 + 40;

  if ( fIsNetscape() ) {
    var l_iDocumentWidth = self.innerWidth;
    var obj = eval('framMainBody.framBodyButtons.document["'+ p_strLayerName +'"]'); 

    if (obj){
      obj.document.write(l_strLayerText);
      obj.document.close();
      obj.left=l_iDocumentWidth - l_iLayerTextWidth;
      obj.top = 12;
    }
  } 
  else {

    var obj = eval('framMainBody.framBodyButtons.document.all["'+p_strLayerName+'"].style'); 
    var l_iDocumentWidth = top.framMainBody.framBodyButtons.document.body.clientWidth;

    if (obj){
      obj.left= l_iDocumentWidth-l_iLayerTextWidth;
      obj.top = 12;
      framMainBody.framBodyButtons.document.all.LayTotalText.innerHTML = l_strLayerText;
    }
  }
}

//  --------------------------------------------------
//  Function: fOnChangeApprover
//  Description: Called from the onChange handler of 
//		 the approver text box. Since the 
//		 overriding approver is an LOV on the
//		 header page, clear the hidden ID field
//		 to signal the field is being changed 
//		 by the user manually(not using LOV).   
//
//  --------------------------------------------------
function fOnChangeApprover(p_formDisplay){

    g_objTimecard.objHeader.objApprover.mSetstrName(p_formDisplay.txtDefApprName.value);

    //clear the default preferences approver ID & Number automatically
    g_objTimecard.objHeader.objApprover.mSetiID('');
    g_objTimecard.objHeader.objApprover.mSetiNumber('');

}



//  --------------------------------------------------
//  Function: fOnChangeDetailComment
//  Description: 
//
//  --------------------------------------------------
function fOnChangeDetailComment(p_formDisplay, p_iCurrLine, p_iHourColumn){
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

  if( fGetInputWidget(p_formDisplay, 'txtDetailHoursY', p_iHourColumn).value == ''){
    l_winDetail.alert(g_objFNDMsg.mstrGetMsg("PA_WEB_HOURS_REQUIRED"));
    fGetInputWidget(p_formDisplay, 'txaCommentY', p_iHourColumn).value  = '';
  }
  else {
    g_bDetailModified = true;
    g_objTempLines.arrLine[g_objTempLines.miFindLine(g_iCurrLineInDetail)].objDays.arrDay[p_iHourColumn].mSetstrComment(fGetInputWidget(p_formDisplay, 'txaCommentY', p_iHourColumn).value);
  }
}


//  --------------------------------------------------
//  Function: fOnChangeDetailProject
//  Description: 
//
//  --------------------------------------------------
function fOnChangeDetailProject(p_formDisplay, p_bLOVSelect){
  var l_txtProjNum   = p_formDisplay.txtProjNum;
  var l_txtHidProjID = p_formDisplay.hidProjID;
  var l_txtTaskNum   = p_formDisplay.txtTaskNum;
  var l_iIndex;

  if (p_bLOVSelect) {
    g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetProject(l_txtHidProjID.value, l_txtProjNum.value, '');
  }
  else {
    g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetProject( '', l_txtProjNum.value, '');
  }

  //when the project is being changed manually, 
  //clear the task id automatically.
  g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetTask('', l_txtTaskNum.value, '');

  g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetbIsEmpty(false);
  g_bDetailModified = true;

}

//  --------------------------------------------------
//  Function: fOnChangeDetailTask
//  Description: 
//
//  --------------------------------------------------
function fOnChangeDetailTask(p_formDisplay, p_bLOVSelect){
  var l_txtTaskNum   = p_formDisplay.txtTaskNum;
  var l_txtHidTaskID = p_formDisplay.hidTaskID;

  if (p_bLOVSelect) {
    g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetTask(
      l_txtHidTaskID.value, l_txtTaskNum.value, '');
  }
  else {
    g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetTask(
      '', l_txtTaskNum.value, '');
  }
  g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetbIsEmpty(false);
  g_bDetailModified = true;
 
}  


//  --------------------------------------------------
//  Function: fOnChangeDetailType
//  Description: 
//
//  --------------------------------------------------
function fOnChangeDetailType(p_formDisplay){
  var l_popType = eval("p_formDisplay." + 'popType');
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_objType = g_objTypes.mobjGetTypeByName(l_popType.options[l_popType.selectedIndex].value);

  g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetobjType(l_objType);
  g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mSetbIsEmpty(false);
  g_bDetailModified = true;

  setTimeout('', 200);
  if (fbIsDFlexUsed()) {
    fDrawDetailBodyContent(g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody.framBodyContent);
    fPopulateDetailLines();
  }  

}  

//  --------------------------------------------------
//  Function: fOnChangeDetailHours
//  Description: 
//
//  --------------------------------------------------
function fOnChangeDetailHours(p_formDisplay, p_strHourEntry, p_iYValue){
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_iXValue   = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow.options[l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow.selectedIndex].value;
  var l_iLineNumber;
  var l_txtHourEntry = fGetInputWidget(p_formDisplay,'txtDetailHoursY', p_iYValue);
  var l_txtHourTotal = fGetInputWidget(p_formDisplay,'txtHourTotal', l_iXValue);
  var l_strText = g_objFNDMsg.mstrGetMsg("PA_WEB_POS_NUM");
  var l_objLine;

  if (g_objProfOpt.mvGetProfOpt("PA_SST_ALLOW_NEGATIVE_TXN") == C_strNO){
    if ( fCheckPosNum(p_strHourEntry, false) == false){
      l_strText = top.g_objFNDMsg.fstrReplToken(
                        l_strText,
                        '&Number',
                        p_strHourEntry);
      l_winDetail.alert(l_strText);
      return;
    }
  }

  if (fIsNum(p_strHourEntry )==false){
    l_winDetail.alert(g_objFNDMsg.mstrGetMsg("PA_WEB_NUM_FORMAT"));
    return;
  }

  for (var decimalAt=0; decimalAt<p_strHourEntry.length; decimalAt++) {
    if (p_strHourEntry.charAt(decimalAt)==".") break
  }
  p_strHourEntry = p_strHourEntry + '';
  l_iDecimals = (p_strHourEntry.length)-decimalAt;
  
  if (l_iDecimals>3){
    p_strHourEntry = p_strHourEntry.substr(0,decimalAt+3);
  }

  l_txtHourEntry.value =  fFormatDecimals(p_strHourEntry);

  // assign value to the Temporary data structure
  // 1. find the corresponding array by matching the X value with the display number
  
  l_iLineNumber = g_objTempLines.miFindLine(l_iXValue);
  l_objLine = g_objTempLines.arrLine[l_iLineNumber];
  l_objLine.objDays.arrDay[p_iYValue].mSetnHours(fFormatDecimals(l_txtHourEntry.value));
  l_objLine.mSetnWeekTotal();
  l_objLine.mSetbIsEmpty(false); 
  g_bDetailModified = true;

  g_objTempLines.arrLine[g_objTempLines.miFindLine(g_iCurrLine)].mSetnWeekTotal();

  // If hour is cleared, then clear out day info
  if (p_strHourEntry == "") {
    l_objLine.objDays.arrDay[p_iYValue].mSetstrComment('');
    fClearDFlexArray(l_objLine.objDays.arrDay[p_iYValue].arrDFlex);
  }

}


//  --------------------------------------------------
//  Function: fOnChangeEmployee
//  Description: Called by the onChange handler  
//		 Since the weekending date depends on 
//		 the employee, need to recalculate the
//		 value used in the weekending poplist
//
//  --------------------------------------------------
function fOnChangeEmployee(p_formDisplay){
  var l_popEmployee =p_formDisplay.popEmployee;
  var l_iOptionLength = 11;

  // Populate data structure:  timecard.employee & timecard.dateWeekEndingDate
  g_objTimecard.objHeader.mSetobjEmployee(g_objEmployees.mobjFindEmployee(l_popEmployee.options[l_popEmployee.selectedIndex].value));
  g_iPopEmployeeIndex = l_popEmployee.selectedIndex;

  // update popWeekEndingDate
  g_dWeekEndingDates =
   ['',
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, -5),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, -4),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, -3),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, -2),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, -1),
    g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate,
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, 1),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, 2),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, 3),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, 4),
    fDateAddWeek(g_objEmployees.arrEmployee[l_popEmployee.selectedIndex].dWeekEndingDate, 5),
    ''];

  //Reset the poplist length to ensure the added option from the previous employee doesn't
  //remain in the next employee's popWeekEndingDate.
  top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate.options.length = l_iOptionLength;

  fPopulateWeekEndingDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, 6);
  fOnChangeWeekEndingDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, false);

}


//  --------------------------------------------------
//  Function: fOnChangeHeaderComment
//  Description: update the data structure if the 
//		 comment has been modified.
//
//  --------------------------------------------------
function fOnChangeHeaderComment(p_strComment){

  g_objTimecard.objHeader.mSetstrComment(p_strComment);

}


//  --------------------------------------------------
//  Function: fOnChangeHourEntry
//  Description: Do the first round of validation when
//		 hours are being entered. 
//
//  --------------------------------------------------
function fOnChangeHourEntry(p_formDisplay, p_strHourEntry, p_iXvalue, p_iYvalue){
  var l_iLineNumber;
  var l_txtHourEntry = fGetLineWidget(p_formDisplay,'txtHourEntry',p_iXvalue, p_iYvalue);
  var l_txtHourTotal = fGetInputWidget(p_formDisplay,'txtHourTotal', p_iXvalue);
  var l_strText = g_objFNDMsg.mstrGetMsg("PA_WEB_POS_NUM");
  var l_objLine;

  if (g_objProfOpt.mvGetProfOpt("PA_SST_ALLOW_NEGATIVE_TXN") == C_strNO){
    if ( fCheckPosNum(p_strHourEntry, false) == false){
      l_strText = top.g_objFNDMsg.fstrReplToken(
                        l_strText,
                        '&Number',
                        p_strHourEntry);
      alert(l_strText);
      return;
    }
  }

  if (fIsNum(p_strHourEntry )==false){
    alert(g_objFNDMsg.mstrGetMsg("PA_WEB_NUM_FORMAT"));
    return;
  }

  for (var decimalAt=0; decimalAt<p_strHourEntry.length; decimalAt++) {
    if (p_strHourEntry.charAt(decimalAt)==".") break
  }
  p_strHourEntry = p_strHourEntry + '';
  l_iDecimals = (p_strHourEntry.length)-decimalAt;
  
  if (l_iDecimals>3){
    p_strHourEntry = p_strHourEntry.substr(0,decimalAt+3);
  }

  l_txtHourEntry.value =  fFormatDecimals(p_strHourEntry);

  // assign value to the data structure
  // 1. find the corresponding array by matching the X value with the display number
  
  l_iLineNumber = g_objTimecard.objLines.miFindLine(p_iXvalue);
  l_objLine = g_objTimecard.objLines.arrLine[l_iLineNumber];
  l_objLine.objDays.arrDay[p_iYvalue].mSetnHours(fFormatDecimals(l_txtHourEntry.value));
  l_objLine.mSetbIsEmpty(false); // why uses global lvariable to identify?
  g_bLinesEntered = true;

  fUpdateDispWeekTotal(framMainBody.framBodyContent.document.formEntryBodyContent, p_iXvalue, fComputeWeekTotal(l_iLineNumber));
  fUpdateDispDayTotal(framMainBody.framBodyContent.document.formEntryBodyContent, p_iYvalue, fComputeDayTotal(p_iYvalue));
  g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetnWeekTotal();
  fUpdateDispGrandTotal(framMainBody.framBodyContent.document.formEntryBodyContent, fComputeGrandTotal());
  fSetLayer('LayTotalText', g_objTimecard.objLines.miGetNumEnteredLines(), fFormatDecimals(fComputeGrandTotal()));

  // If hour is cleared, then clear out day info
  if (p_strHourEntry == "") {
    l_objLine.objDays.arrDay[p_iYvalue].mSetstrComment('');
    fClearDFlexArray(l_objLine.objDays.arrDay[p_iYvalue].arrDFlex);
  }
}

//  --------------------------------------------------
//  Function: fOnChangeProject
//  Description: Called from the onChange handler of 
//		 the project text box. Since the 
//		 project field is an LOV on the
//		 lines page, clear the hidden ID field
//		 to signal the field is being changed 
//		 by the user manually(not using LOV).   
//
//  --------------------------------------------------
function fOnChangeProject(p_formDisplay, p_bLOVSelect){
  var l_txtProjNum = fGetInputWidget(p_formDisplay,'txtProjNum', g_iCurrLine);
  var l_txtHidProjID = fGetInputWidget(p_formDisplay,'hidProjID', g_iCurrLine);
  var l_txtTaskNum = fGetInputWidget(p_formDisplay,'txtTaskNum', g_iCurrLine);
  var l_iIndex;

  if (p_bLOVSelect) {
    g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetProject(
	l_txtHidProjID.value, l_txtProjNum.value, '');
  }
  else {
    g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetProject(
      '', l_txtProjNum.value, '');
  }

  //when the project is being changed manually, 
  //clear the task id automatically.
  g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetTask(
	'', l_txtTaskNum.value, '');

  g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetbIsEmpty(false);
  g_bLinesEntered = true;
}

//  --------------------------------------------------
//  Function: fOnChangeTask
//  Description: Called from the onChange handler of 
//		 the task text box. Since the 
//		 task field is an LOV on the
//		 lines page, clear the hidden ID field
//		 to signal the field is being changed 
//		 by the user manually(not using LOV).   
//
//  --------------------------------------------------
function fOnChangeTask(p_formDisplay, p_bLOVSelect){
  var l_txtTaskNum = fGetInputWidget(p_formDisplay, 'txtTaskNum' , g_iCurrLine);
  var l_txtHidTaskID = fGetInputWidget(p_formDisplay, 'hidTaskID', g_iCurrLine);

  if (p_bLOVSelect) {
    g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetTask(
      l_txtHidTaskID.value, l_txtTaskNum.value, '' );
  }
  else {
    g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetTask(
      '', l_txtTaskNum.value, '');
  }
  g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetbIsEmpty(false);
  g_bLinesEntered = true;
}

//  --------------------------------------------------
//  Function: fonChangeType
//  Description: Update the data structure and mark 
//		 the line is no longer empty  
//
//  --------------------------------------------------
function fonChangeType(p_formDisplay){
  var l_popType = fGetInputWidget(p_formDisplay, 'popType' , g_iCurrLine);
  var l_objType = g_objTypes.mobjGetTypeByName(l_popType.options[l_popType.selectedIndex].value);

  g_objTimecard.objLines.mobjFindLine(g_iCurrLine).mSetobjType(l_objType);

  g_objTimecard.objLines.arrLine[g_objTimecard.objLines.miFindLine(g_iCurrLine)].mSetbIsEmpty(false);
  g_bLinesEntered = true;

  // Clear all context sensitive fields
  if (fbIsDFlexUsed()) {
    var l_objLine = g_objTimecard.objLines.mobjFindLine(g_iCurrLine);
    for(var i=0; i<C_iWEEKDAYS; i++) {
      var l_objDay = l_objLine.mGetobjDays().arrDay[i];
      if (l_objDay.mGetnHours() != "")
        fClearCSDFlex(l_objDay.arrDFlex);
    }
  }
}


//  --------------------------------------------------
//  Function: fOnChangeWeekEndingDate
//  Description: Whenever the week ending date is 
//		 changed, redefine what dates should be
//		 displayed on the lines page
// 
//  --------------------------------------------------
function fOnChangeWeekEndingDate(p_formDisplay, p_bInternalCall){
  var l_popWeekEndingDate = p_formDisplay.popWeekEndingDate;
  var l_NumofLines = g_objTimecard.objLines.miGetNumLines();

  g_dWeekEndingDate = new Date(l_popWeekEndingDate.options[l_popWeekEndingDate.selectedIndex].value * 1);

  if ((g_bLinesEntered == true) && (p_bInternalCall != true)){
    if (confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_ERASE_HOUR"))) 
      fKeepAllLine();
    else
      fCancelChanges(); 
  }
  else {

  g_iPopWeekEndingDateIndex = l_popWeekEndingDate.selectedIndex;

  // Save selected value to the data structure
  g_objTimecard.objHeader.dWeekEndingDate = new Date(l_popWeekEndingDate.options[l_popWeekEndingDate.selectedIndex].value * 1);

    fPopulateWeekEndingWeekArr();

    if (fIsNetscape() || fIsIE5()) {
      fRefreshEntryBody();
    } else {
      // Reload the display. See BUG 1289975
      top.framMainBody.location.reload();
    }

  }
}


//  --------------------------------------------------
//  Function: fOnClickAddMoreLines
//  Description: When the add lines button is clicked,   
//		 add 5 more lines to both the display
//		 and to the data structure.
//
//  --------------------------------------------------
function fOnClickAddMoreLines(){
var l_iNumOfLine = g_iTotalDisplayedLines + 4;

  // Add line to the data structure
  for (var i=g_iTotalDisplayedLines; i<l_iNumOfLine; i++){
    g_objTimecard.objLines.mAddLine('',g_iTotalDisplayedLines+1,'','','','','','',
        new Type('','','','',''),new Days());

    g_iTotalDisplayedLines++;
  }

  // Add line to the display
  fDrawEntryBodyContent();
 
  // Populate lines back to the display
  fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent);
}

//  --------------------------------------------------
//  Function: fOnClickApplyAlias
//  Description: when the apply alias button is clicked
//		 verify things are correct before proceed.
//
//  --------------------------------------------------
function fOnClickApplyAlias(p_formDisplay){
  var l_popAlias = p_formDisplay.popAlias; 
  var l_bMatchFound = false;
  var l_bInvalidType = false;
  var l_focusItem = eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.' + g_iCursorPosition);

  //1. if the alias array is empty, tell the user that nothing can be applied
  if (g_objPref.mGetobjAliases().miGetActualNumAliases() < 1) {
    alert(g_objFNDMsg.mstrGetMsg("PA_WEB_NO_ALIAS"));
    return;
  }

  //2. Assign the alias value to the selected line 
  //   ** p_formDisplay is formLine3 while the following input widgets
  //   ** are on formEntryBodyContent.
  l_txtProjNum = fGetInputWidget(framMainBody.framBodyContent.document.formEntryBodyContent,'txtProjNum',g_iCurrLine);
  l_txtTaskNum    = fGetInputWidget(framMainBody.framBodyContent.document.formEntryBodyContent,'txtTaskNum',g_iCurrLine);
  l_popType    = fGetInputWidget(framMainBody.framBodyContent.document.formEntryBodyContent,'popType',g_iCurrLine);

  l_txtProjNum.value = g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].mGetstrProjectNum();
  l_txtTaskNum.value    = g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].mGetstrTaskNum();


  // Make sure the selected type is still valid within this week before updating the data structure.  
  if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdStartDate() != ''){
    if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdStartDate().getTime() > 0) {
      if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdStartDate().getTime() > g_objTimecard.objHeader.dWeekEndingDate.getTime()){
        alert(g_objFNDMsg.mstrGetMsg("PA_WEB_BLANK_TYPE"));
        l_bInvalidType = true;
      }
    }
  }

  if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdEndDate() != ''){
    if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdEndDate() != ''){
      if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdEndDate().getTime() > 0) {

        if (g_objPref.mGetobjAliases().mGetarrAlias()[l_popAlias.selectedIndex].objType.mGetdEndDate().getTime() 
           < g_objTimecard.objHeader.dWeekEndingDate.getTime()){
          alert(g_objFNDMsg.mstrGetMsg("PA_WEB_BLANK_TYPE"));
          l_bInvalidType = true;
        }
      }
    }
  }

  // update data structure
  fOnChangeProject(top.framMainBody.framBodyContent.document.formEntryBodyContent, false);
  fOnChangeTask(top.framMainBody.framBodyContent.document.formEntryBodyContent, false);
  if ( l_bInvalidType == false){

    // Verify whether type is still the same.  Don't want to clear DFlex
    if (l_popType.options[l_popType.selectedIndex].value != 
        objAliases.arrAlias[l_popAlias.selectedIndex].objType.mGetstrName()) {

      // Update the display before working on the data structure. 
      // Can't swap the order because fonChangeType refer to the display value.
      for (var i=0; i<l_popType.options.length; i++){
        if (l_popType.options[i].value == objAliases.arrAlias[l_popAlias.selectedIndex].objType.mGetstrName()){
          l_popType.options[i].selected = true;
        }
      }
      fonChangeType(top.framMainBody.framBodyContent.document.formEntryBodyContent);
    }
  }
  fCodeForExplorer();
   
  // Update the layer information
  fSetLayer('LayTotalText', g_objTimecard.objLines.miGetNumEnteredLines(), fFormatDecimals(fComputeGrandTotal()));

}



//  --------------------------------------------------
//  Function: fOnClickApproverLOV
//  Description: LOV function handler for overriding 
//	 	 approver LOV 
//
//  --------------------------------------------------
function fOnClickApproverLOV(){ 

  top.g_formLOV = top.framMainBody.framBodyHeader.document.formEntryBodyHeader;
  top.g_strFormLOV = top.C_strHEADER;

  g_formLOV.PA_WEB_APPR_NAME.value =  top.g_formLOV.txtDefApprName.value;

  top.LOV('275', "PA_WEB_APPR_NAME", '601', 'PA_WEB_APPR_LOV_DUMMY', 'formEntryBodyHeader','top.framMainBody.framBodyHeader',  '', ''); 

}

//  --------------------------------------------------
//  Function: fOnClickCalDay
//  Description: When the date link on the calendar 
//		 page is being clicked, update the 
//		 value on data structure and on display. 
//		 If there are data in lines, confirm
//		 user action before proceed.
//
//  --------------------------------------------------
function fOnClickCalDay(p_iDay){
  var l_winCalendar    = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_iOptionLength  = top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate.options.length;
  var l_NumOfLines     = g_objTimecard.objLines.miGetNumLines(); 
  var l_iMaxArrayIndex = 12;
  var undefined;

  if (g_bLinesEntered == true){
    if (!l_winCalendar.confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_ERASE_HOUR"))) 
      fCancelChanges();
    else  
      fConstructPopWeekEnding(p_iDay);

    // Close the window for the user after we prompt for further instruction
    // using a confirm box for better UI
    if ((l_winCalendar.closed != true) && (l_winCalendar.close != undefined))
      fModalWinClose();
  }
  else
    fConstructPopWeekEnding(p_iDay);


}

//  --------------------------------------------------
//  Function: fConstructPopWeekEnding
//  Description: Reconstruct the poplist value of the
//               weekending date if the selected calendar
//               day doesn't fall within the list of 
//               existing options.
//
//  --------------------------------------------------
function fConstructPopWeekEnding(p_iDay){
  var l_winCalendar       = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formCalendar      = l_winCalendar.framMainBody.document.formCalendar;
  var l_popWeekEndingDate = top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate;
  var l_strMonthValue;
  var l_iMonthDigit;
  var l_iDayDigit;
  var l_bMatchFound = false;

  var l_iOptionLength = top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate.options.length;
  var l_iMaxArrayIndex = 12;
  var l_NumOfLines = g_objTimecard.objLines.miGetNumLines();

  g_dWeekEndingDate =
    new Date(l_formCalendar.popYear.options[l_formCalendar.popYear.selectedIndex ].value,
    l_formCalendar.popMonth.options[l_formCalendar.popMonth.selectedIndex].value, p_iDay);

  // Is the date part of the g_dWeekEndingDates array?
  for (j=0; j<l_iOptionLength-1; j++){
    if (g_dWeekEndingDate.getTime() == l_popWeekEndingDate.options[j].value){
        g_iPopWeekEndingDateIndex = j;
        l_bMatchFound = true;
        break;
    }
  }

  if (!l_bMatchFound) {
    if (g_dWeekEndingDate.getTime() < g_dWeekEndingDates[1].getTime()){
      fAddToPopDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, g_dWeekEndingDate, false, false, 0);
      if (g_dWeekEndingDates[0] == '') {
        for (i=1; i<=l_iMaxArrayIndex; i++){
          if (g_dWeekEndingDates[i] != '')
            fAddToPopDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, g_dWeekEndingDates[i], false, false, i);
          }
      }
      g_iPopWeekEndingDateIndex = 0;
      g_dWeekEndingDates[0] = g_dWeekEndingDate;
    }
    else {
      if (g_dWeekEndingDates[l_iMaxArrayIndex] != ''){
         l_iOptionLength = l_iOptionLength -1;
      }
      fAddToPopDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, g_dWeekEndingDate, false, false, l_iOptionLength);
      g_iPopWeekEndingDateIndex = l_iOptionLength;
      g_dWeekEndingDates[l_iMaxArrayIndex] = g_dWeekEndingDate;
    }
  }

  top.framMainBody.framBodyHeader.document.formEntryBodyHeader.popWeekEndingDate.options[g_iPopWeekEndingDateIndex].selected = true;
  fOnChangeWeekEndingDate(top.framMainBody.framBodyHeader.document.formEntryBodyHeader, true);

}

//  --------------------------------------------------
//  Function: fOnClickCancelDetail
//  Description: This is being called when the cancel
//		 button on the detail page is clicked.
//		 Prompts the user to apply changes if
//		 changes have been made on the detail
//		 page.
//
//  --------------------------------------------------
function fOnClickCancelDetail(){

  fCodeForExplorerDetail();

  // Determine whether change has been made to DFF
  fDetermineDFlexChanged();

  // Prompt user to save the changes
  fCheckDetail();
}

//  --------------------------------------------------
//  Function: fOnClickClearLine
//  Description: Delete the line from the data structure
//		 and update display.
//
//  --------------------------------------------------
function fOnClickClearLine(){
var undefined;
  // delete the line from the data structure 
  g_objTimecard.objLines.mobjDeleteLine(g_iCurrLine);
 
  // update display 
  fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent);

}


//  --------------------------------------------------
//  Function: fOnClickCopyLine
//  Description: Called when the copy line button on
//		 the lines page is being clicked.
//
//  --------------------------------------------------
function fOnClickCopyLine(){

 var l_iToIndex = g_objTimecard.objLines.miGetEmptyLineNum();
 var l_iFromIndex;

  fCodeForExplorer();

  l_iFromIndex = g_objTimecard.objLines.miFindLine(g_iCurrLine);

  //If the user is trying to copy an empty line, do nothing.
  if ((g_objTimecard.objLines.arrLine[l_iFromIndex].mGetbIsEmpty() == true) ||
	(g_objTimecard.objLines.arrLine[l_iFromIndex].mGetbIsDeleted() == true)){
    alert(g_objFNDMsg.mstrGetMsg("PA_WEB_NOTHING_TO_COPY"));
    return;
  }

  //Make sure there are still an empty row on display
  if (g_objTimecard.objLines.miGetNumEmptyLines() > 0){
    g_objTimecard.objLines.mobjCopyLine(l_iToIndex, l_iFromIndex);
    fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent);
  }
  else{
    //If there are not enough blank lines on display, call fDrawLineBodyContent to render more rows
    fOnClickAddMoreLines();
    l_iToIndex = g_objTimecard.objLines.miGetEmptyLineNum();
    g_objTimecard.objLines.mobjCopyLine(l_iToIndex, l_iFromIndex);
    fPopulateLines(top.framMainBody.framBodyContent.document.formEntryBodyContent);
  }

}
				   
//  --------------------------------------------------
//  Function: fOnClickFinalReview
//  Description: called final review button is clicked 
//		 After the entries have passed the 
//		 client-side validation, call to display
//		 the final review page.
//
//  --------------------------------------------------
function fOnClickFinalReview(){ 
var l_objForm;

  fCodeForExplorer();
  fIsFormValid();
  l_objForm = top.framMainBody.framBodyButtons.document.formSaveSubmitFReview;

  // send everything to server only after client validation has been passed.
  if (top.g_objErrors.miGetNumOfErrors() < 1){
    top.fonClickSaveSubmitOrFinalReview(top.g_objTimecard, l_objForm, C_strENTER_HOURS, C_strFINAL_REVIEW, top.g_objAuditHistory);
  }
}

//  --------------------------------------------------
//  Function: fOnClickOkDetail
//  Description: 
//
//  --------------------------------------------------
function fOnClickOkDetail() {
var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
var l_popRow    = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;

  fCodeForExplorerDetail();

  g_iPrevLineInDetail = l_popRow.options[l_popRow.selectedIndex].value;

  // Determine whether change made to DFlex fields
  fDetermineDFlexChanged();

  fSaveDetail();

}

function fOnFocusDetailHeader(p_iLineNumber) {
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_objLine = g_objTimecard.objLines.mobjFindLine(g_iCurrLineInDetail);

  if (l_objLine.mbIsReverseLine()) {
    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.txtTaskNum.blur();
    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.txtProjNum.blur();
    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.popType.blur();
  } 
}



//  --------------------------------------------------
//  Function: fOnClickProjectLOV
//  Description: LOV function handler for overriding
//               approver LOV
//
//  --------------------------------------------------
function fOnClickProjectLOV(p_iLineNumber, p_bFromMainPage) {
  var l_objLine;

  if (p_bFromMainPage) {
    l_objLine = g_objTimecard.objLines.mobjFindLine(p_iLineNumber);
  } 
  else {
    l_objLine = g_objTimecard.objLines.mobjFindLine(g_iCurrLineInDetail);
  }
  if (!l_objLine.mbIsReverseLine()) {
    top.g_formLOV = top.framMainBody.framBodyContent.document.formEntryBodyContent;

    if (p_bFromMainPage) {
      top.g_strFormLOV = C_strLINES;
      // mark the current line with an arrow image
      fOnFocusLine(top.g_formLOV, p_iLineNumber, '', 'txtProjNum' + p_iLineNumber, false);
    }
    else {
      top.g_strFormLOV = C_strDETAILS;
    }

    top.g_formLOV.PA_WEB_PROJECT_NUMBER.value = 
	fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNumber).value;
    top.g_formLOV.PA_WEB_PROJECT_ID.value     = 
	fGetInputWidget(top.g_formLOV, "hidProjID", p_iLineNumber).value;
    g_iProjLOVCurLine = p_iLineNumber;

    top.LOV('275','PA_WEB_PROJECT_NUMBER','601', 'PA_WEB_PROJECTS_TASKS_LOV_DMY', 'formEntryBodyContent', 'top.framMainBody.framBodyContent', '', '');  

  } 
  else {
    return;
  }  
}

//  --------------------------------------------------
//  Function: fOnClickRowBackward
//  Description: 
//
//  --------------------------------------------------
function fOnClickRowBackward(){ 
  var l_winDetail      = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_popRow         = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;
  var l_iSelectedIndex = l_popRow.selectedIndex;

  fCodeForExplorerDetail();

  if (l_iSelectedIndex - 1  >= 0) {
    g_iPrevLineInDetail = l_popRow[l_iSelectedIndex].value;
    g_iCurrLineInDetail = l_popRow[l_iSelectedIndex - 1].value;

    // Determine whether change made to DFlex fields
    fDetermineDFlexChanged();

    l_iSelectedIndex = l_popRow.selectedIndex - 1;
    l_popRow.options[l_iSelectedIndex].selected = true;

    if (fGetContSensField(g_objTempLines.mobjFindLine(g_iCurrLineInDetail)) != fGetContSensField(g_objTempLines.mobjFindLine(g_iPrevLineInDetail)))
      fDrawDetailBodyContent(l_winDetail.framMainBody.framBodyContent);

    fPopulateDetail(); 

  }
  
}


//  --------------------------------------------------
//  Function: fOnClickRowForward
//  Description: 
//
//  --------------------------------------------------
function fOnClickRowForward(){ 
  var l_winDetail      = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_popRow         = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;
  var l_iSelectedIndex = l_popRow.selectedIndex; 

  fCodeForExplorerDetail();

  if (l_iSelectedIndex + 1 < g_iTotalDisplayedLines) {
    g_iPrevLineInDetail = l_popRow[l_iSelectedIndex].value;
    g_iCurrLineInDetail = l_popRow[l_iSelectedIndex + 1].value;

    // Determine whether change made to DFlex fields
    fDetermineDFlexChanged();

    l_iSelectedIndex = l_popRow.selectedIndex + 1;
    l_popRow.options[l_iSelectedIndex].selected = true;

    if (fGetContSensField(g_objTempLines.mobjFindLine(g_iCurrLineInDetail)) != fGetContSensField(g_objTempLines.mobjFindLine(g_iPrevLineInDetail)))
      fDrawDetailBodyContent(l_winDetail.framMainBody.framBodyContent);
      
    fPopulateDetail();
  }

}


//  --------------------------------------------------
//  Function: fOnClickSave
//  Description: called whenever save button is clicked 
//		 After the entries have passed the 
//		 client-side validation, call to display
//		 the save confirmation page.
//
//  --------------------------------------------------
function fOnClickSave(){ 

  fCodeForExplorer();

  if (confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_CONFIRM_SAVE"))) {
    fonClickSaveSubmitOrFinalReview(top.g_objTimecard, top.framMainBody.framBodyButtons.document.formSaveSubmitFReview, C_strENTER_HOURS, C_strSAVE, top.g_objAuditHistory);
  }
  else {
    return;
  }
}

//  --------------------------------------------------
//  Function: fOnClickTaskLOV
//  Description: LOV function handler for Task LOV
//
//  --------------------------------------------------
function fOnClickTaskLOV(p_iLineNumber, p_bFromMainPage) {
var strWhereClause;
var l_currWin;
var l_currForm;

  var l_objLine;

  if (p_bFromMainPage) {
    l_objLine = g_objTimecard.objLines.mobjFindLine(p_iLineNumber);
  } 
  else {
    l_objLine = g_objTimecard.objLines.mobjFindLine(g_iCurrLineInDetail);
  }
  if (!l_objLine.mbIsReverseLine()) {
    top.g_formLOV = top.framMainBody.framBodyContent.document.formEntryBodyContent;

    if (p_bFromMainPage) {
      if ( fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNumber).value.length == 0 ) {
        alert(g_objFNDMsg.mstrGetMsg("PA_WEB_TASK_LOV_NO_PROJECT"));
        return;
      }
      top.g_strFormLOV = C_strLINES;
      // mark the current line with an arrow image
      fOnFocusLine(top.g_formLOV, p_iLineNumber, '', 'txtTaskNum' + p_iLineNumber, false);
      top.g_formLOV.PA_WEB_PROJECT_NUMBER.value = 
	fGetInputWidget(top.g_formLOV, "txtProjNum", p_iLineNumber).value;
      top.g_formLOV.PA_WEB_PROJECT_ID.value     = 
	fGetInputWidget(top.g_formLOV, "hidProjID", p_iLineNumber).value;
    }
    else {
      l_currWin = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
      l_currForm = g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody.framBodyHeader.document.formDetailBodyHeader;
      if ( l_currForm.txtProjNum.value.length == 0 ) {
        alert(g_objFNDMsg.mstrGetMsg("PA_WEB_TASK_LOV_NO_PROJECT"));
        return;
      }
      top.g_strFormLOV = C_strDETAILS;
      top.g_formLOV.PA_WEB_PROJECT_NUMBER.value = l_currForm.txtProjNum.value;
      top.g_formLOV.PA_WEB_PROJECT_ID.value     = l_currForm.hidProjID.value;
    }

    top.g_formLOV.PA_WEB_TASK_NUMBER.value    = 
	fGetInputWidget(top.g_formLOV, "txtTaskNum", p_iLineNumber).value;
    top.g_formLOV.PA_WEB_TASK_ID.value        = 
	fGetInputWidget(top.g_formLOV, "hidTaskID", p_iLineNumber).value;
    g_iTaskLOVCurLine = p_iLineNumber;

    strWhereClause = "PROJECT_NUMBER=\'"+fReplaceSpaceWithPlus(fSafeURL(top.g_formLOV.PA_WEB_PROJECT_NUMBER.value)) + "\'";

    top.LOV('275', 'PA_WEB_TASK_NUMBER', '601', 'PA_WEB_PROJECTS_TASKS_LOV_DMY', 'formEntryBodyContent', 'top.framMainBody.framBodyContent', '', strWhereClause); 
  } 
}


//  --------------------------------------------------
//  Function: fOnFocusLine
//  Description: This is called from the onFocus handler
//		 of the comment field in the lines page.   
//
//  --------------------------------------------------
function fOnFocusLine(p_formDisplay, p_iCurrLine, p_iHourColumn, p_iCursorPosition, p_bFromDetailWin){
  var undefined;

  var l_objLine = g_objTimecard.objLines.mobjFindLine(p_iCurrLine);

  if (l_objLine.mbIsReverseLine()) {   // Reverse Line ??
    if (p_bFromDetailWin) {    // From Details??

      var l_winDetail             = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
      var l_formDetailBodyContent = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;

      fGetInputWidget(l_formDetailBodyContent, 'txtDetailHoursY', p_iHourColumn).blur();
      fGetInputWidget(l_formDetailBodyContent, 'txaCommentY', p_iHourColumn).blur();
    }
    else {  // From Entry
      eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.' + p_iCursorPosition).blur();
//      eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.' + p_iCursorPosition).disabled = true;
    }
  }
  else {   // Regular Line

    g_iCurrLine       = p_iCurrLine;
    g_iCursorPosition = p_iCursorPosition;
    g_iHourColumn     = eval( '\'' + p_iHourColumn + '\'');; 

    eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.imgCurrLineIndicator'  + g_iPrevLine).src = g_imgEmpty.src;
    eval('top.framMainBody.framBodyContent.document.formEntryBodyContent.imgCurrLineIndicator'  + p_iCurrLine).src = g_imgArrow.src;

    g_iPrevLine = g_iCurrLine;

    //Update detail header
    if (g_iHourColumn != '') {

      // Default DFlex value
      if (fbIsDFlexUsed()) {
        var l_arrDFlex = l_objLine.mGetobjDays().arrDay[g_iHourColumn].arrDFlex;    
        if (!fIsDFlexArrayInitialized(l_arrDFlex)) {
          fInitDFlex(fGetContSensField(l_objLine), l_arrDFlex);
        }
      }
    }
  }
}  



//  --------------------------------------------------
//  Function: fOpenCalWin
//  Description: Open the calendar window and lay down
//		 the modal code hooks. 
//
//  --------------------------------------------------
function fOpenCalWin(){

  // The following globals are used in the calendar
  // to keep track of the current date. This is done
  // so that reload() could be used. BUG 1289975
  g_iCalDay = top.g_dWeekEndingDate.getDay();
  g_iCalMonth = top.g_dWeekEndingDate.getMonth();
  g_iCalYear = top.g_dWeekEndingDate.getFullYear();

  top.fModalWinOpen(280, 280, 200, 200, '', top.C_strCALENDAR, ' ');


}

//  --------------------------------------------------
//  Function: fOpenDetailWin
//  Description: Open the detail window with the X, Y
//		 coordinates stored before.
//
//  --------------------------------------------------
function fOpenDetailWin() {

   fCodeForExplorer();

   fCopyToTempObj();

   g_iCurrLineInDetail = g_iCurrLine;
   top.fModalWinOpen(800, 560, 50, 50, null, top.C_strDETAILS, '');

}

//  --------------------------------------------------
//  Function: fPadTimecardLines
//  Description: This function pads the number of lines 
// 		 objects so that there is at least
// 		 C_iTimecardLinesIncrement, and number of lines 
//		 is a multiple of C_iTimecardLinesIncrement.
//  --------------------------------------------------
function fPadTimecardLines(p_objLines) {

  var l_iNumOfLines = p_objLines.arrLine.length;
  var l_iNewNumOfLinesToAdd = 0;
  var l_iDisplayNum;

   // If no lines defined, add minimum
  if (l_iNumOfLines == 0){
    l_iNewNumOfLinesToAdd = C_iTimecardLinesIncrement;
    l_iDisplayNum = 1;
  }
  else {
    l_iDisplayNum = p_objLines.arrLine[l_iNumOfLines -1].mGetiDisplayLineNum() + 1;
  }

  // Set new number of lines so that it is a multiple
  l_iNewNumOfLinesToAdd = l_iNewNumOfLinesToAdd + 
    (C_iTimecardLinesIncrement - (l_iNumOfLines % C_iTimecardLinesIncrement));

  if ((l_iNumOfLines % C_iTimecardLinesIncrement) == 0)
    l_iNewNumOfLinesToAdd = l_iNewNumOfLinesToAdd - C_iTimecardLinesIncrement;


  // Add lines to p_objLines
  for(var i=0;i<l_iNewNumOfLinesToAdd;i++, l_iDisplayNum++)
    p_objLines.mAddLine("", l_iDisplayNum, 
                        "", "", "", 
                        "", "", "", g_objTypes.mobjFindType("", ""),
                        new Days());


  //Determine is there any hours on non-workdays
  for (var j=0; j < l_iNumOfLines; j++){
    for (var k=C_iSUNDAY; k<=C_iSATURDAY; k++){
      if ((p_objLines.arrLine[j].mGetobjDays().arrDay[k].mGetnHours() != '') &&
	    (g_objPref.mGetobjWorkDays().bIsWorkDay(k) == false)) {
        g_bHourNotOnWorkDay = true;
        break;
      }
    }
  }
    

}


function fPopulateDetailWithTimeout() {
 setTimeout("fPopulateDetail();", 200);
}

//  --------------------------------------------------
//  Function: fPopulateDetail
//  Description: Populate the values on the detail 
//		 header according to the hour cell the
//		 user is on in the lines page. 
//
//  --------------------------------------------------
function fPopulateDetail(){

  var l_winDetail          = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formDetailBodyHeader    = l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader;
  var l_formDetailBodyContent    = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;
  var l_popRow             = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;
  var l_iXValue            = l_popRow.options[l_popRow.selectedIndex].value;
  var l_iobjTempLinesIndex = g_objTempLines.miFindLine(l_iXValue);
  var l_objTempLine        = g_objTempLines.arrLine[l_iobjTempLinesIndex];
  var l_popType            = l_formDetailBodyHeader.popType;
  var l_strCSValue         = fGetContSensField(l_objTempLine);

  // Blank out everything first
    l_formDetailBodyHeader.txtProjNum.value = '';
    l_formDetailBodyHeader.txtTaskNum.value = '';
    l_popType.options[0].selected = true;

    for (var j=C_iSUNDAY; j<=C_iSATURDAY; j++){
      if(g_objPref.mGetobjWorkDays().bIsWorkDay(j) == true || g_bHourNotOnWorkDay){
        fGetInputWidget(l_formDetailBodyContent, 'txtDetailHoursY', j).value = '';
        fGetInputWidget(l_formDetailBodyContent, 'txaCommentY', j).value = '';
        fClearDFlex(l_formDetailBodyContent, l_strCSValue, j);
      }
    }

    // Repopulate the Detail Header 
    l_formDetailBodyHeader.txtProjNum.value = g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetstrProjectNum();
    l_formDetailBodyHeader.txtTaskNum.value = g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetstrTaskNum();

    for (var i=0; i<l_popType.options.length; i++){
      if (l_popType.options[i].value == 
		g_objTempLines.arrLine[l_iobjTempLinesIndex].objType.mGetstrName()){
        l_popType.options[i].selected = true;
        break;
      }
    }
   
    // Repopulate the Detail Lines
    fPopulateDetailLines();
}

//  --------------------------------------------------
//  Function: fPopulateDetailLines
//  Description: Populate the line values to the detail page.
//
//  --------------------------------------------------
function fPopulateDetailLines(){
  var l_winDetail          = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formDetailBodyContent    = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;
  var l_popRow             = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow;
  var l_iXValue            = l_popRow.options[l_popRow.selectedIndex].value;
  var l_iobjTempLinesIndex = g_objTempLines.miFindLine(l_iXValue);

    for (var j=C_iSUNDAY; j<=C_iSATURDAY; j++){
      if (g_objPref.mGetobjWorkDays().bIsWorkDay(j) == true || g_bHourNotOnWorkDay){

        if (g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetobjDays().arrDay[j].mGetnHours() != '') {
          fGetInputWidget(l_formDetailBodyContent, 'txtDetailHoursY', j).value = g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetobjDays().arrDay[j].mGetnHours();
        }

        if (g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetobjDays().arrDay[j].mGetstrComment() != ''){
          fGetInputWidget(l_formDetailBodyContent, 'txaCommentY', j).value = g_objTempLines.arrLine[l_iobjTempLinesIndex].mGetobjDays().arrDay[j].mGetstrComment();
        }

        // Populate DFlex values
        if (fbIsDFlexUsed()) {
          fPopulateDFlex(l_winDetail.framMainBody.framBodyContent, 
                fGetContSensField(g_objTempLines.mobjFindLine(g_iCurrLineInDetail)),
                g_objTempLines.mobjFindLine(g_iCurrLineInDetail).mGetobjDays().arrDay[j].arrDFlex, 
 		j);
        }
      }
    }

}

//  --------------------------------------------------
//  Function: fPopulateTimeEntry
//  Description: Populate the values in the header page. 
//           
//  --------------------------------------------------
function fPopulateTimeEntry(p_formDisplay){

  var l_popEmployee = p_formDisplay.popEmployee;
  var undefined;

  if (!g_objTimecard.mbIsReverseTimecard()) {
    var l_popWeekEndingDate = p_formDisplay.popWeekEndingDate;
    l_popWeekEndingDate[g_iPopWeekEndingDateIndex].selected = true;  
  }
  if (g_objEmployees.miGetNumEmployees() > 1) {
    l_popEmployee[g_iPopEmployeeIndex].selected = true;
  }

  if (g_objTimecard.objHeader.mGetstrComment() != undefined)
    p_formDisplay.txaComment.value = g_objTimecard.objHeader.mGetstrComment();

  if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_OVERRIDE_APPROVER") == C_strYES ){
    p_formDisplay.txtDefApprName.value = g_objTimecard.objHeader.objApprover.mGetstrName();
    p_formDisplay.hidDefApprID.value = g_objTimecard.objHeader.objApprover.mGetiID();    
  }
}

//  --------------------------------------------------
//  Function: fPopulateLines
//  Description: Populate the values on the lines page.  
//
//  -------------------------------------------------
function fPopulateLines(p_formDisplay){
  var j=1;

  // Blank out everything first
  for (var k=1; k<=g_iTotalDisplayedLines; k++){
     fGetInputWidget(p_formDisplay, 'txtProjNum', k).value = '';
     fGetInputWidget(p_formDisplay, 'txtTaskNum', k).value = '';
     fGetInputWidget(p_formDisplay, 'popType', k).options[0].selected = true;
     fGetInputWidget(p_formDisplay, 'txtHourTotal', k).value = '';
     for (var y=C_iSUNDAY; y<=C_iSATURDAY; y++){
       if(g_objPref.mGetobjWorkDays().bIsWorkDay(y) == true || g_bHourNotOnWorkDay) {
         fGetLineWidget(p_formDisplay,'txtHourEntry',k, y).value = '';
       }
     }
  } 

  // Repopulate the valid lines back to display
  for (var i=0; i<g_objTimecard.objLines.miGetNumLines(); i++){
    if ((g_objTimecard.objLines.arrLine[i].bIsDeleted != true) && (g_objTimecard.objLines.arrLine[i].bIsEmpty != true)) {
      j = g_objTimecard.objLines.arrLine[i].mGetiDisplayLineNum();
      l_popType	     = fGetInputWidget(p_formDisplay, 'popType', j);
      fGetInputWidget(p_formDisplay, 'txtProjNum', j).value = g_objTimecard.objLines.arrLine[i].mGetstrProjectNum();
      fGetInputWidget(p_formDisplay, 'txtTaskNum', j).value    = g_objTimecard.objLines.arrLine[i].mGetstrTaskNum();
      fGetInputWidget(p_formDisplay, 'txtHourTotal', j).value = top.fFormatDecimals(g_objTimecard.objLines.arrLine[i].mGetnWeekTotal());
      for (var k=0; k<l_popType.options.length; k++){
        if (l_popType.options[k].value == g_objTimecard.objLines.arrLine[i].objType.mGetstrName()){
          l_popType.options[k].selected = true;
        }
      }

      for (var y=C_iSUNDAY; y<=C_iSATURDAY; y++){
        if ( g_objPref.mGetobjWorkDays().bIsWorkDay(y) == true || g_bHourNotOnWorkDay) {
          fGetLineWidget(p_formDisplay,'txtHourEntry',j, y).value    = g_objTimecard.objLines.arrLine[i].objDays.arrDay[y].mGetnHours();
        }
      }
    fUpdateDispWeekTotal(framMainBody.framBodyContent.document.formEntryBodyContent, g_objTimecard.objLines.arrLine[i].mGetiDisplayLineNum(), fComputeWeekTotal(i));
    }
  }

  // Compute day total
  for (var i=C_iSUNDAY; i<=C_iSATURDAY; i++){
    if(g_objPref.mGetobjWorkDays().bIsWorkDay(i) == true || g_bHourNotOnWorkDay){
      fUpdateDispDayTotal(framMainBody.framBodyContent.document.formEntryBodyContent, i, top.fComputeDayTotal(i));
    }
  }

  // Compute grand total
  fUpdateDispGrandTotal(framMainBody.framBodyContent.document.formEntryBodyContent,g_objTimecard.mGetnTotalHours());

if (!g_bFromTimeEntryToReversal) {
  var l_nGrandTotal = fFormatDecimals(fComputeGrandTotal());
  setTimeout("fSetLayer('LayTotalText', g_objTimecard.objLines.miGetNumEnteredLines(),'" + l_nGrandTotal + "');", 200);
  }
}


//  --------------------------------------------------
//  Function: fPopulateWeekEndingDate
//  Description: Populate the option values of the 
//		 weekending poplist.  
//
//  --------------------------------------------------
function fPopulateWeekEndingDate(p_formDisplay, p_iPopWeekEndingDateIndex){
var l_bSelected;
var l_iPopWeekEndingDateIndex = 6;
var j;


  for (i=0,j=0; i<g_dWeekEndingDates.length; i++){
    if(g_dWeekEndingDates[i] != ''){
      fAddToPopDate(p_formDisplay, g_dWeekEndingDates[i], false, false, j);
      j++
    }
  }
  p_formDisplay.popWeekEndingDate.options[p_iPopWeekEndingDateIndex].selected =  true;
  g_iPopWeekEndingDateIndex = p_formDisplay.popWeekEndingDate.selectedIndex;

}
//  --------------------------------------------------
//  Function: fSaveDetail 
//  Description: save changes made in the detail page
//		 to the g_objTimedard data structure.
//
//  --------------------------------------------------
function fSaveDetail(){
  var l_winDetail       = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formDetailBodyHeader = l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader;
  var l_formDetailBodyContent = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;
  var l_iXValue   = l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow.options[l_winDetail.framMainBody.framBodyButtons.document.formDetailBodyButtons.popRow.selectedIndex].value;


  //Update the data structure
  if ((g_bDetailModified) || ((fbIsDFlexUsed()) && (g_bDetailDFlexModified))) {

  for (var i=1; i<=g_objTempLines.miGetNumLines(); i++){
      var j=g_objTempLines.miFindLine(i);
      var l=g_objTimecard.objLines.miFindLine(i);
      g_objTimecard.objLines.arrLine[l].iDenormID	   = g_objTempLines.arrLine[j].mGetnDenormID();
      g_objTimecard.objLines.arrLine[l].iProjectID	   = g_objTempLines.arrLine[j].mGetiProjectID();
      g_objTimecard.objLines.arrLine[l].strProjectNum	   = g_objTempLines.arrLine[j].mGetstrProjectNum();
      g_objTimecard.objLines.arrLine[l].strProjectName	   = g_objTempLines.arrLine[j].mGetstrProjectName();
      g_objTimecard.objLines.arrLine[l].iTaskID		   = g_objTempLines.arrLine[j].mGetiTaskID();
      g_objTimecard.objLines.arrLine[l].strTaskNum	   = g_objTempLines.arrLine[j].mGetstrTaskNum();
      g_objTimecard.objLines.arrLine[l].strTaskName	   = g_objTempLines.arrLine[j].mGetstrTaskName();

// Modify later
      g_objTimecard.objLines.arrLine[l].mSetobjType(new Type(g_objTempLines.arrLine[j].objType.mGetstrName(),g_objTempLines.arrLine[j].objType.mGetstrSystemLinkage(), g_objTempLines.arrLine[j].objType.mGetstrExpenditureType(), g_objTempLines.arrLine[j].objType.mGetdStartDate(), g_objTempLines.arrLine[j].objType.mGetdEndDate())); 

      for(var k=0; k<C_iWEEKDAYS; k++){
        g_objTimecard.objLines.arrLine[l].objDays.arrDay[k].mCopy(g_objTempLines.arrLine[j].objDays.arrDay[k]);
      }

      g_objTimecard.objLines.arrLine[l].nWeekTotal	= g_objTempLines.arrLine[j].mGetnWeekTotal();
      g_objTimecard.objLines.arrLine[l].bIsDeleted	= g_objTempLines.arrLine[j].mGetbIsDeleted();
      g_objTimecard.objLines.arrLine[l].bIsEmpty	= g_objTempLines.arrLine[j].mGetbIsEmpty();
      j++;
  }

    g_bDetailModified = false;
    if (fbIsDFlexUsed()) g_bDetailDFlexModified = false;

  }

  fCloseDetail(true);

}

//  --------------------------------------------------
//  Function: fSkipBack
//  Description: 
//
//  --------------------------------------------------
function fSkipBack(){
  var l_framCalendar = g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody;
  var l_formCalendar = l_framCalendar.document.formCalendar;
  var l_iMonthIndex;
  var l_iYearIndex;

  if (l_formCalendar.popMonth.selectedIndex == 0){
    l_iMonthIndex = 11; 
    l_iYearIndex  = l_formCalendar.popYear.selectedIndex - 1;
  }
  else { 
    l_iMonthIndex = l_formCalendar.popMonth.selectedIndex - 1;
    l_iYearIndex  = l_formCalendar.popYear.selectedIndex;
  }

  g_iCalDay   = top.g_dWeekEndingDate.getDay();
  g_iCalMonth = l_formCalendar.popMonth.options[l_iMonthIndex].value;
  g_iCalYear  = l_formCalendar.popYear.options[l_iYearIndex].value;

  // Reload the display. See BUG 1289975
  if (fIsIE5() && fIsWindows95())
    fDrawIE5CalendarBody(g_iCalDay, g_iCalMonth, g_iCalYear);
  else {
    l_framCalendar.document.close();
    l_framCalendar.document.open();
    l_framCalendar.document.write(fDrawCalendarBody());
    l_framCalendar.document.close();
  }
}

//  --------------------------------------------------
//  Function: fSkipForward
//  Description:
//
//  --------------------------------------------------
function fSkipForward(){
  var l_framCalendar = g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody;
  var l_formCalendar = l_framCalendar.document.formCalendar;
  var l_iMonthIndex;
  var l_iYearIndex;

  if (l_formCalendar.popMonth.selectedIndex == 11){
    l_iMonthIndex = 0;
    l_iYearIndex  = l_formCalendar.popYear.selectedIndex + 1;
  }
  else { 
    l_iMonthIndex = l_formCalendar.popMonth.selectedIndex + 1;
    l_iYearIndex  = l_formCalendar.popYear.selectedIndex;
  }

  g_iCalDay   = top.g_dWeekEndingDate.getDay();
  g_iCalMonth = l_formCalendar.popMonth.options[l_iMonthIndex].value;
  g_iCalYear  = l_formCalendar.popYear.options[l_iYearIndex].value;

  // Reload the display. See BUG 1289975
  if (fIsIE5() && fIsWindows95())
    fDrawIE5CalendarBody(g_iCalDay, g_iCalMonth, g_iCalYear);
  else {
    l_framCalendar.document.close();
    l_framCalendar.document.open();
    l_framCalendar.document.write(fDrawCalendarBody());
    l_framCalendar.document.close();
  }

}

//  --------------------------------------------------
//  Function: fStartTimeEntry
//  Description: The first function to start the Time Entry screen 
//
//  --------------------------------------------------
function fStartTimeEntry(){

 fDrawFrameSet(self,'AUTO', 'Javascript:top.fDrawTimeEntry()',null , C_strENTER_HOURS);
}

//  --------------------------------------------------
//  Function: fUpdateDispDayTotal
//  Description: Update display of the total field 
//
//  --------------------------------------------------
function fUpdateDispDayTotal(p_formDisplay, p_iDayOfWeek, p_nDayTotal){

  var l_txtDayTotal = fGetInputWidget(p_formDisplay, 'txtDayTotal', p_iDayOfWeek);
  p_nDayTotal = p_nDayTotal + '';
  
  l_txtDayTotal.value  = top.fFormatDecimals(p_nDayTotal);

}

//  --------------------------------------------------
//  Function: fUpdateDispGrandTotal
//  Description: Update display of the grand total  
//
//  --------------------------------------------------
function fUpdateDispGrandTotal(p_formDisplay, p_nGrandTotal){
  p_nGrandTotal = p_nGrandTotal + '';

  p_formDisplay.txtGrandTotal.value = top.fFormatDecimals(p_nGrandTotal);
}

//  --------------------------------------------------
//  Function: fUpdateDispWeekTotal
//  Description: Update display of the week total  
//
//  --------------------------------------------------
function fUpdateDispWeekTotal(p_formDisplay, p_iDisplayLineNumber, p_nWeekTotal){
  var l_txtHourTotal = fGetInputWidget(p_formDisplay, 'txtHourTotal', p_iDisplayLineNumber);
    p_nWeekTotal = p_nWeekTotal + '';
    l_txtHourTotal.value = fFormatDecimals(p_nWeekTotal);
}

 
