/* $Header: czDefStl.js 115.5 1999/11/15 12:09:32 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

//default style for Base component
var style = Styles.createStyle('BASE-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('background-color','transparent');

style = Styles.createStyle('COMBOBOXSTYLE');
style.setAttribute('position','absolute');
style.setAttribute('background-color','#99ccff');

//default style for Container component
style = Styles.createStyle('CONTAINER-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',100);
style.setAttribute('background-color','transparent');

//default style for HyperLink component
style = Styles.createStyle('HYPERLINK-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);
style.setAttribute('background-color','transparent');

style = Styles.createStyle('LINK-SPANSTYLE');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','12');

//default style for Prompt component
style = Styles.createStyle('PROMPT-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);
style.setAttribute('background-color','transparent');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
if(ie4)
{
	style.setAttribute('font-family','Arial');
	style.setAttribute('font-size','10pt');
	style.setAttribute('padding-left','5');
	style.setAttribute('padding-top','3');
}


style = Styles.createStyle('BaseGridClass');
style.setAttribute('position','absolute');

style = Styles.createStyle('ColObjectClass');
style.setAttribute('position','absolute');
style.setAttribute('background-color','#336699');


//default style for Image component
style = Styles.createStyle('IMAGE-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',100);
style.setAttribute('background-color','transparent');

//default style for Form Input component
style = Styles.createStyle('FORMINPUT-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);
style.setAttribute('background-color','white');

//default style for Form Input Button component
style = Styles.createStyle('FORMBUTTON-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);

//default style for Form Check Box component
style = Styles.createStyle('FORMCHECKBOX-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);
style.setAttribute('background-color','transparent');

//default style for Container component
style = Styles.createStyle('INPUTTEXT-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);

if(ie4)
{
	style.setAttribute('font-family','Arial');
	style.setAttribute('font-size','10pt');
	style.setAttribute('padding-left','5');
	style.setAttribute('padding-top','3');
}

//default style for Text Area component
style = Styles.createStyle('TEXTAREA-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',25);


//default style for SCROLLBAR component
style = Styles.createStyle('SCROLLBAR-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',10);
style.setAttribute('height',100);
style.setAttribute('background-color','#999999');


//default style for SCROLL component
style = Styles.createStyle('SCROLL-STYLE');
style.setAttribute('position','absolute');
style.setAttribute('width',100);
style.setAttribute('height',100);
//style.setAttribute('background-color','white');

//default style for Prompt component
style = Styles.createStyle('CBUTTONCAPTION-STYLE','PROMPT-STYLE');
style.setAttribute('text-align','center');
if(ie4)
	style.setAttribute('padding-top','1');

style = Styles.createStyle('CBUTTON-CAPTION-SPAN');
style.setAttribute('font-size','10pt');
style.setAttribute('font-family','Arial');
style.setAttribute('padding-top','1');

style = Styles.createStyle('CPICKER-BUTTON-SPAN');
style.setAttribute('font-size','10pt');
style.setAttribute('font-family','Arial');
style.setAttribute('padding-top','1');
style.setAttribute('padding-left','12');


//default style for SELECTEDTAB component
style = Styles.createStyle('SELECTEDTAB');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
style.setAttribute('text-align','center');
style.setAttribute('color','#000000');
style.setAttribute('font-weight','bold');

//default style for DESELECTEDTAB component
style = Styles.createStyle('DESELECTEDTAB');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
style.setAttribute('text-align','center');
style.setAttribute('color','#ffffff');
style.setAttribute('font-weight','bold');

//default style for SUBTAB-SPAN component
style = Styles.createStyle('SUBTAB-SPAN');
style.setAttribute('font-family','Arial');
style.setAttribute('text-align','center');
style.setAttribute('color','#ffffff');
style.setAttribute('font-weight','bold');
style.setAttribute('font-size','10pt');
style.setAttribute('background-color','transparent');

style = Styles.createStyle('SELECTED-SUBTAB-SPAN','SUBTAB-SPAN');
style.setAttribute('color','#000000');

//default style for DIV-SUBTAB component
style = Styles.createStyle('DIV-SUBTAB','PROMPT-STYLE');
style.setAttribute('text-align','center');
style.setAttribute('color','white');
style.setAttribute('font-weight','bold');
//style.setAttribute('background-color','transparent');

//default style for DIV-TITLE component
style = Styles.createStyle('DIV-TITLEBAR','PROMPT-STYLE');
style.setAttribute('padding-top',3);

//default style for SPAN-TITLE component
style = Styles.createStyle('SPAN-TITLEBAR');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
style.setAttribute('font-weight','bold');
style.setAttribute('color','#000000');

//default style for DIV-TITLE component
style = Styles.createStyle('DIV-TITLE','PROMPT-STYLE');
style.setAttribute('text-align','left');


//default style for SPAN-TITLE component
style = Styles.createStyle('SPAN-TITLE');
style.setAttribute('font-face','Arial, Helvetica, sans-serif');
style.setAttribute('font-size','14pt');
style.setAttribute('font-weight','bold');
style.setAttribute('color','#333333');


//default style for DIV-FIELD component
style = Styles.createStyle('DIV-FIELD','PROMPT-STYLE');
style.setAttribute('text-align','right');

//default style for SPAN-FIELD component
style = Styles.createStyle('SPAN-FIELD');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','12pt');

//default style for DIV-THREAD component
style = Styles.createStyle('DIV-THREAD','PROMPT-STYLE');
style.setAttribute('text-align','left');

//default style for SPAN-THREAD component
style = Styles.createStyle('SPAN-THREAD');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','12pt');
style.setAttribute('color','blue');

//default style for DIV-LINK component
style = Styles.createStyle('DIV-LINK','PROMPT-STYLE');
style.setAttribute('text-align','left');

//default style for SPAN-LINK component
style = Styles.createStyle('SPAN-LINK');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','12pt');
style.setAttribute('color','blue');

//default style for Date component
style = Styles.createStyle('DATESPAN');
style.setAttribute('text-align','center');
style.setAttribute('font-family','Arial');
style.setAttribute('color','#003366');
style.setAttribute('font-size','10pt');
style.setAttribute('font-weight','normal');


//default style for Selected Date component
style = Styles.createStyle('DATESELECTSPAN');
style.setAttribute('text-align','center');
style.setAttribute('color','yellow');
style.setAttribute('font-size','10pt');
style.setAttribute('font-family','Arial');
style.setAttribute('font-weight','bold');


style = Styles.createStyle('DATEHOVERSPAN');
style.setAttribute('text-align','center');
style.setAttribute('color','#003366');
style.setAttribute('font-family','Arial');
style.setAttribute('font-weight','bold');
style.setAttribute('font-size','12pt');


style = Styles.createStyle('MENUITEMSPAN');
style.setAttribute('text-align','center');
style.setAttribute('color','white');
style.setAttribute('font-family','Arial');
style.setAttribute('font-weight','normal');
style.setAttribute('font-size','9pt');
style.setAttribute('padding-left',5);
style.setAttribute('padding-right',5);

style = Styles.createStyle('MENUITEMSELECTED');
style.setAttribute('text-align','center');
style.setAttribute('color','white');
style.setAttribute('font-family','Arial');
style.setAttribute('font-weight','normal');
style.setAttribute('font-size','9pt');
style.setAttribute('padding-left',5);
style.setAttribute('padding-right',5);

style = Styles.createStyle('LISTITEMSPAN');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
style.setAttribute('padding-left',5);
style.setAttribute('padding-right',5);

style = Styles.createStyle('LISTITEMSELECTED');
style.setAttribute('font-family','Arial');
style.setAttribute('font-size','10pt');
style.setAttribute('padding-left',5);
style.setAttribute('padding-right',5);


style = Styles.createStyle('PROMPTSPAN');
style.setAttribute('font-family','Arial');
style.setAttribute('background-color','transparent');
style.setAttribute('font-size','10pt');

style = Styles.createStyle('INPUTTEXTSPAN','PROMPTSPAN')
style.setAttribute('padding-left',5);
style.setAttribute('padding-top',3);


style = Styles.createStyle('BOXSPAN');
style.setAttribute('font-family','Arial');
style.setAttribute('background-color','transparent');
style.setAttribute('font-size','10pt');
style.setAttribute('font-weight','bold');
style.setAttribute('padding-left',4);
style.setAttribute('padding-top',4);

style = Styles.createStyle('COLUMNHEADERSPAN');
style.setAttribute('font-family','Arial');
style.setAttribute('background-color','transparent');
style.setAttribute('font-size','10pt');
style.setAttribute('font-weight','bold');
style.setAttribute('padding-left',5);
style.setAttribute('padding-top',5);

style = Styles.createStyle('LISTCLASS');
style.setAttribute('position','absolute');

if(ns4) 
{
	var _paletteleft = 1;
	var _palettetop = 1;
}
else 
{
	if(ie4)
	{
		var _paletteleft = 2;	
		var _palettetop = 2;
	}
}

style = Styles.createStyle('colorPickerClass');
style.setAttribute('position','absolute');
style.setAttribute('border','inset 1px #ffffff');
style.setAttribute('left',_paletteleft);
style.setAttribute('top',_palettetop);

style = Styles.createStyle('TOOLTIP');
style.setAttribute('position','absolute');
style.setAttribute('background-color','#ffffcc');

style = Styles.createStyle('TOOLTIPSPAN');
style.setAttribute('border','solid black 1pt');
style.setAttribute('overflow','hidden');
style.setAttribute('padding','1pt');
