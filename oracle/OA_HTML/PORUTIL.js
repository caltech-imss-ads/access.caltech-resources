/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: PORUTIL.js 115.40.1156.2 2001/08/20 20:02:06 pkm ship      $ */

submitInProcess = false;
var tabHighlighted = -1;
var subTabHighlighted = -1;

function counterDummy(counter){}

function setReqSessionInfo() {
  reqSessionId = allForms["POR_REQ_INFO_FORM"][0].POR_REQ_SESSION_ID.value
  ordSessionId = allForms["POR_REQ_INFO_FORM"][0].POR_ORD_SESSION_ID.value
  reqNumber = allForms["POR_REQ_INFO_FORM"][0].POR_REQ_NUM.value
  reqTotal = allForms["POR_REQ_INFO_FORM"][0].POR_REQ_TOTAL.value
  reqLines = allForms["POR_REQ_INFO_FORM"][0].POR_TOTAL_LINES.value
}

function resetReqSessionInfo() {
  reqSessionId = 0;
  reqNumber = 0;
  reqTotal = 0;
  reqLines=0;
  emergencyReqFlag = 'N';
  setStateInfo('checkoutFlow', 'regular');  
}

function resetReqSessionAndStateInfo() {
  resetReqSessionInfo();
  resetStateInfo();
}

function roundDecimal(expr, digits)
{
  var str= "" + Math.round(eval(expr) * Math.pow(10, digits));
  while (str.length <= digits)
  {
    str= "0"+str;
  }
  var decpoint= str.length- digits;
  
  return str.substring(0, decpoint) + decSeparator + str.substring(decpoint, str.length);
}


function isPositiveNumber(num)
{
  var f= parseFloat(num);
  return (!isNaN(f) && f>=0);
}

function getStyle(obj)
{
  if(IS_NAV)
  {
    return obj;
  }
  else
  {
    return obj.style;
  }
}

function show(obj) {
  getStyle(obj).visibility = "visible";
}

function hide(obj) {
  getStyle(obj).visibility = "hidden";
}

function setBgcolor(obj, color)
{
  if (IS_NAV) 
  {
    obj.bgColor=color;
  }
  else 
  {
    obj.style.backgroundColor= color;
  }
}

function setContent(lyr,text) 
{
  if (IS_NAV) 
  {
    lyr.document.open();
    lyr.document.write(text);
    lyr.document.close();
  }
  else 
  {
    lyr.innerHTML = text;
  }
}

function clearForms(doc)
{
  for(var i=0; i<doc.forms.length; i++)
  {
    allForms[doc.forms[i].name]=null;
  }

  if(IS_NAV)
  {
    for(var i=0; i<doc.layers.length; i++)
    {
      clearForms(doc.layers[i].document);
    }
  }
  doc.formsCleaned=true;
}

function initDocument(doc, frameName) 
{
  if(!doc.formsCleaned)
  {
    clearForms(doc);
  }
  for(var i=0; i<doc.forms.length; i++)
  {
    if(doc.forms[i].elements.length>0)
      initForm(doc.forms[i]);
  }

  if(IS_NAV)
  {
    for(var i=0; i<doc.layers.length; i++)
    {
      initDocument(doc.layers[i].document, frameName);
    }
  }
  changeImages(doc);
  changeLinks(doc);
  changeButtonText(doc);
  window.onclick= clickOrFocus;

  window.onfocus= clickOrFocus;

  top.noSave= false;

if(getStateInfo("formIndex")){
   var formIndex = getStateInfo("formIndex");	
   var form_name = getStateInfo("formName");
//fix for the non-catalog based pages
if(form_name != 'POR_SPORD_GOODS_R' && form_name != 'POR_SPORD_RATE_R' && form_name != 'POR_SPORD_AMOUNT_R' ){	
   var rForm= top.getTop().allForms[form_name][formIndex];
   eval("rForm."+ getStateInfo("fieldName") +".focus();");
   setStateInfo("formName",null);
   setStateInfo("formIndex",null);
   setStateInfo("fieldName",null);
	}
 }

  clearSubmitFlag();

// aahmad 2/12/2000. moved this to end of PORUTIL.js,
// because need it in tabs before the initialize() function is run.
// initState();

} // end of function initDocument()

function changeImages(doc)
{
  if(IS_IE)
  {
   for(var i=0; i<doc.images.length; i++)
    {
      doc.images[i].alt=replaceEntity(doc.images[i].alt);
    }
  }
}

function changeLinks(doc)
{
  if(IS_IE)
  {
    for(var i=0; i<doc.links.length; i++)
    {
      doc.links[i].href=replaceEntity(doc.links[i].href);
    }
  }
}

function changeButtonText(doc)
{
  if(IS_IE)
  {

    for (var j=0; j<doc.forms.length; j++)
	{

      for(var i=0; i<doc.forms[j].elements.length; i++)
      {
        if (doc.forms[j].elements[i].type == 'button')
          doc.forms[j].elements[i].value=replaceEntity(doc.forms[j].elements[i].value);
      }
    }

  }
}


function initForm(fm)
{
  var arr= allForms[fm.name];
  if(arr==null)
  {
    allForms[fm.name]= new Array();
  }
  var len= allForms[fm.name].length;
  allForms[fm.name][len]= fm;
  fm.updated=false;

  for(var i=0; i<fm.elements.length; i++)
  {
    var elem= fm.elements[i];
    // check to see if each element has any dependencies; eg requester
    // && requester_id; if so, null out dependents onChange of dependee
    var dependents= ATTRIBUTE_DEPENDENCY[elem.name];
    var cmd="";
    if (dependents)
    {
      for(var d=0; d<dependents.length; d++)
      {
        if(fm.elements[dependents[d]])
          cmd += "this.form."+ dependents[d]+".value='';";
      }
      // moving here because it triggers if there are dependent 
      // fields that way
      fm.elements[i][getHandlerName("onChange")] = new Function(cmd);
    }

// removing because of onChange events that are defined in 
// indivual pages are get overritten. This cause problem with IE4 and IE5.
    // put it back because of bug 1259361.
//    fm.elements[i][getHandlerName("onChange")]= new Function(cmd);

    // Bug 952184, onChange event doesnt fire unless you tab out of the field
    // adding handler for onFocus just for POR_LINES_R region
    if (fm.name == 'POR_LINES_R')
      fm.elements[i][getHandlerName("onFocus")] = new Function("this.form.updated=true");


//    if(fm.elements[i].type!="button" && fm.elements[i].type!="submit")
//      fm.elements[i][getHandlerName("onClick")]= new Function("this.form.updated=true");
  } // matches   for(var i=0; i<fm.elements.length; i++)
}

function getReqToken(func, act, temp)
{
  // Pocurement team also uses this function, but does not pass in any
  // parameters.  Default the parameters as if this is being called by
  // Pocurement server.
  func= (func==null) ? "addToOrder" : func;
  act= (act==null) ? "addToOrder" : act;
  temp= (temp==null) ? "por_search" : temp;

  var ret=
    "currentUITemplate" + VALUE_DELIMITER + 
    allForms['POR_PAGE_INFO'][0].currentUITemplate.value + FIELD_DELIMITER +
    "template"+ VALUE_DELIMITER + temp + FIELD_DELIMITER+
    "function"+ VALUE_DELIMITER + func + FIELD_DELIMITER+
    "action" + VALUE_DELIMITER + act + FIELD_DELIMITER+
    "reqSessionId" + VALUE_DELIMITER + reqSessionId + FIELD_DELIMITER+
     "ordSessionId" + VALUE_DELIMITER + ordSessionId + FIELD_DELIMITER+
    "reqHeaderId" + VALUE_DELIMITER + reqHeaderId + FIELD_DELIMITER+
    "shoppingCartLines" + VALUE_DELIMITER + reqLines + FIELD_DELIMITER+
    "emergencyReq" + VALUE_DELIMITER + emergencyReqFlag;

  return ret;
}

function submitRequest(currentTemplate, //
                       url,     // the url to submit the form to 
                       action,  // used to lookup the next template in
                                // por_master.xml
                       func,    // used by Apps layer to determine
                                // what to so with the submission
                       generalData,// concatenate name value pair of all data, other than
                       // REQ_TOKEN, that does not belong in any
                       // region, eg currentSelectedLine
                       // this field must start with FIELD_DELIMITER
                       formNames,// array of names of forms that needs to be submited
                       target,  //  name of target window that gets result of submission
                       method)  // request method for form.  This
                                // parameter is optional and has a
                                // default value of POST
{
  // first, check that all mandatory fields are filled out, if not,
  // return without doing anything

  	generalData += FIELD_DELIMITER+"stateInfo"+VALUE_DELIMITER+makeArrayIntoString(stateInfo);

  generalData += FIELD_DELIMITER+"checkoutFlow"+VALUE_DELIMITER+ checkoutFlow;
  generalData += FIELD_DELIMITER+"homeURL"+VALUE_DELIMITER+homeURL;

  setSubmitFlag();

  if(formNames!=null)
  {
    for(var i=0; i<formNames.length; i++)
    {
      if(!checkMandatory(formNames[i]))
      {
	addToMessageStack("ICX_POR_ALRT_IS_MANDATORY");
//	 generalData += FIELD_DELIMITER +"javascriptMessage"+VALUE_DELIMITER+FND_MESSAGES["ICX_POR_ALRT_IS_MANDATORY"];
//        clearSubmitFlag();
//        return;
      }
    }
  }
  generalData += FIELD_DELIMITER+JAVASCRIPT_MESSAGE;
  generalData += FIELD_DELIMITER + ERROR_ATTR;

  var result= allForms["POR_RESULT_FORM"][0];

  result.action= url;
  result.target= target;

  //  Use post method to submit the form to middle tier, unless
  //  otherwise specified by caller.
  // For por_main, since this is the frame that gets reloaded when the
  // reload button is clicked, the method must be get.
  result.method= ((method=="GET" || target=="por_main") ? "GET" : "POST");
  
  //clear out previous names and values of the form 
  for(var i=0; i<result.length; i++)
  {
    result[i].value= result[i].name="";
  }
  
  var data= 
    getReqToken(func, action,currentTemplate)+ generalData;

  result.elements[0].name= "REQ_TOKEN";
  result.elements[0].value= data; //getReqToken()

  if(formNames!=null)
  {
    for(var i=0; i<formNames.length; i++)
    {
      if(formNames[i]=="selectedLinesList" && allForms["POR_LINES_R"])
      {
        var str="";
        var lines=allForms["POR_LINES_R"];
        for(var j=0; j<lines.length; j++)
        {
          if(lines[j].POR_SELECT_BOX.checked)
          {
//            str= str+ j + FIELD_DELIMITER;
            str= str + lines[j].POR_LINE_NUM.value + FIELD_DELIMITER;
          }
        }
        result.elements[1+i].name = "selectedLinesList";
        result.elements[1+i].value = str.substring(0, str.length-1);
      }
      else
      {
        var form= allForms[formNames[i]];
        if(form)
        {
          result.elements[1+i].name= formNames[i];
          result.elements[1+i].value= packForm(form);
        }
      }
    } // end for(...i<formNames.length..)
  }
  result.elements[1+i].name= "PRE_REQ_TOKEN";
  result.elements[1+i].value= OriginalReqToken;

  result.submit();
} // end of submitRequest

function packForm(form)
{
  var ret="";

  // Bug 903689
  // if form is an array (more than one), check if all are to be submitted
  // even if only one is changed
/*
  var frmSubmitAll = false;
  if (form.length > 0)
  {
    for(var i=0; i<FORM_SUBMIT_ALL.length; i++)
	{
      if (FORM_SUBMIT_ALL[i] == form[0].name)
	    frmSubmitAll = true;
	}
  }
*/
  // set their updated property to true to force all forms to be submitted
/*
  if (frmSubmitAll)
  {
    for(var i=0; i<form.length; i++)
    {
      form[i].updated = true;
	}
  }
*/

  for(var i=0; i<form.length; i++)
  {
  //  if(form[i].updated && form[i].elements[0])
    if(form[i].elements[0])
    {
      for(var j=0; j<form[i].elements.length; j++)
      {
        ret+= getNameValue(form[i].elements[j]);
      }
      ret= ret.substring(0, ret.length-FIELD_DELIMITER.length);
      ret+=LINE_DELIMITER;
    }
  }
  
  return ret.substring(0, ret.length-LINE_DELIMITER.length);
}


function unpackForm(aForm, aString)
{
  var elemStart=0;
  var elemEnd=0;
  
  for(elemStart=0; 
      elemStart<=aString.length && elemStart>=0;
      elemStart=elemEnd+top.FIELD_DELIMITER.length)
  {
    elemEnd= aString.indexOf(top.FIELD_DELIMITER, elemStart);
    elemEnd= (elemEnd>=0) ? elemEnd : aString.length;
    var delimiter= aString.indexOf(top.VALUE_DELIMITER, elemStart);
    var elemName= aString.substring(elemStart, delimiter);
    var elemVal= aString.substring(delimiter+top.VALUE_DELIMITER.length, 
                                   elemEnd);
    var elem=aForm.elements[elemName];
    if(elem)
    {
      if(elem.type=="select-one")
      {
        for(var i=0; i<elem.options.length; i++)
        {
          elem.options[i].selected=(elem.options[i].value==elemVal);
        }
      } 
      else if(elem.type=="checkbox")
      {
        elem.checked= (elemVal=='Y');
      }
      else if(elem.type=="radio")
      {
        for(var i=0; i<elem.length; i++)
        {
          elem[i].checked=(elem[i].value==elemVal);
        }
      }
      else  // text elements
      {
        elem.value=elemVal;
      }
    }
  }
}

function getNameValue(elem)
{
  if(elem.type=="radio" && !elem.checked)
  {
    return "";
  }

  if(elem.type=="checkbox")
  {
    return elem.name+ VALUE_DELIMITER+ 
      (elem.checked ? 'Y' : 'N') + FIELD_DELIMITER;
  }

  if(elem.type=="select-one")
  {
    return elem.name+ VALUE_DELIMITER+ 
      elem.options[elem.selectedIndex].value + FIELD_DELIMITER;
  }    
  // other wise (text, hidden, text area) 
     return elem.name+ VALUE_DELIMITER+ elem.value+ FIELD_DELIMITER;

}

function checkMandatory(formName) 
{

  var forms= allForms[formName];
  var names= MANDATORY_FIELDS[formName];
  var result = true;

  if(!names /*|| names.length==0*/)
    return true;

  if(!forms /*|| forms.length==0*/)
    return true;
  
  for(var j=0; j<forms.length; j++)
  {
    var form= forms[j];

    // Bug 942623 - check if form contains any elements - if condition added
    if(form.elements[0])
	{

      for(var i=0; i<form.elements.length; i++)
      {
        var elem=form.elements[i];
        if((elem.type== "text" || elem.type== "textarea") &&
           isEmpty(elem.value) &&
           names[elem.name]== "Y")
        {
	  addErrorAttribute(elem.name);
           result = false;
        }
        
      }
    }
    
  }
  return result;
}

function isEmpty(str)
{
  str = str+"";
  for(var i=0; i<str.length; i++)
  {
    var ch= str.charAt(i);
    if(ch != '\n' && ch!=' ' && ch!='\t')
      return false;
  }
  return true;
}

/* gets the specified element value from the specified form
   if ind>=0; it would be used as the index of the form to look into,
   otherwise, the forms array would be searched until the element is
   found (useful for single line forms that spans multiple forms)
 */
function getElementValue(ind, formName, elementName)
{
  if (ind<0)
    ind= findIndex(formName, elementName);
  return (ind>=0) ? allForms[formName][ind][elementName].value :null;
}

function findIndex(formName, elementName)
{
  var forms=allForms[formName];
  for(var i=0; i<forms.length; i++)
  {
    if(forms[i][elementName])
      return i;
  }
  return -1;
}

function replaceEntity(str, env)
{
  var entityExp= /(&\{.*};)/;
  var ret="";
  str= str.replace(entityExp, '"+eval("$1".substring(2,"$1".length-2))+"');
  return eval('"' + str + '"');
}

  
function resetForms(formName)
{
  var forms= allForms[formName];
  if(forms)
  {
    for(var i=0; i<forms.length; i++)
    {
      forms[i].reset();
      forms[i].updated=false;
    }
  }
}

function copyForm(form1, form2)
{
  for(var i=0; i<form1.elements.length; i++)
  {
    var elem1=form1.elements[i];
    var elem2=form2.elements[elem1.name];    
    if(elem2 && elem2.value == '')
    {
      elem2.value=elem1.value;
    }
  }
}

// The following functions are used to resize the window to maximum and re-center the window
function openMaxWindow()
{
	if (IS_NAV) {
		moveWindowToCenter();
	}
	if (IS_IE) {
	 	if (screen.availWidth < 800 || screen.availHeight < 600) {
		        window.moveTo(0,0);
			window.resizeTo(screen.availWidth, screen.availHeight);
		} else {
			moveWindowToCenter();
		}
	}

}

function moveWindowToCenter () {
	var posX = (screen.availWidth-800)/2; 
	var posY = (screen.availHeight-600)/2;
        window.moveTo(posX, posY);
}

function setElementHandler(form,        // form object
                           elementName, // string of element name
                           handlerName, // string of name of event handler ;
                                        // eg "onClick" (nav style)
                           handler,     // function eg new Function("this.blur");
                           setOldValue)
{
  var elem=form.elements[elementName];
  if(elem)
  {
    handlerName= getHandlerName(handlerName);
    elem[handlerName]= handler;
    if(setOldValue)
    {
      elem.oldVal= (isNaN(elem.value) || (elem.value<0)) ? 0 : elem.value;
    }
  }
}


function getHandlerName(navName)
{
  return (IS_IE) ? navName.toLowerCase() : navName;
}

/*
function replaceStateParemeterValue(stateName)
{
 	var j = 0;
	var i = 0;
        tmpStateInfo = new Array(); 
	tmpStateInfo = stateInfo;
	stateInfo = "";
 
        for (i=0; i<tmpStateInfo.length; i=i+2) { 
                if (tmpStateInfo[i] != stateName)
         	{
			stateInfo[j] = tmpStateInfo[i];
			stateInfo[j+1] = tmpStateInfo[i+1];
			j += 2;
		}
        }
         
}
*/

function setStateInfo(stateName, stateValue)
{
        var len = stateInfo.length;
        var i = 0;
        var found = false;

        for (i=0;i < len; i=i+2)
        {
                if (stateInfo[i] == stateName)
                {
                        stateInfo[i+1] = replaceComma(stateValue);
                        found = true;
                        break;
                }
        }

        if (found == false)
        {
                stateInfo[len] = stateName;
                stateInfo[len+1] = replaceComma(stateValue);
        }
}

function getStateInfo(stateName)
{
        var len = stateInfo.length;
        var i = 0;
        var found = false;

        for (i =0; i < len; i = i+2)
        {
                if (stateInfo[i] == stateName)
                        return placeComma(stateInfo[i+1]);
        }

        return null;
}


  // replaces comma characters of the given string with delimiter. 
  // Otherwise when we write to state_info we
  // can not read it correctly if value has comma in it.
  function replaceComma(fieldString)
  {
    result     = '';
    startPos   = 0;
    delimiter  = '_._';
    position   = 0;

    if ((fieldString == null) || (fieldString == '') ||  !(isNaN(fieldString)))
       return fieldString;

    // serach for comma 
    while ((position = fieldString.indexOf(',', startPos)) != -1)
    {
      result = result + fieldString.substring(startPos, position) + delimiter;
      startPos = position + 1;
    }
    result = result + fieldString.substring(startPos, fieldString.length);
    return result;
  }

  // return actual string value by getting rid of delimiter value
  function placeComma(fieldString)
  {
    result     = '';
    startPos   = 0;
    delimiter  = '_._';
    position   = 0;

    if ((fieldString == null) || (fieldString == '') ||  !(isNaN(fieldString)))
       return fieldString; 

    while ((position = fieldString.indexOf(delimiter, startPos)) != -1)
    {
      result = result + fieldString.substring(startPos, position) + ',';
      startPos = position + 3;
    }
    result = result + fieldString.substring(startPos, fieldString.length);
    return result;
  }


function makeArrayIntoString(state_Info)
{
        return state_Info.join(',');
}

function makeStringIntoArray()
{
        return STATE_INFO.split(',');
}

function initState()
{
	if (STATE_INFO!=null) {
		if (STATE_INFO.match('null') || STATE_INFO.length==0) {
			return;
		}
		stateInfo = makeStringIntoArray();
	}
}

function resetStateInfo()
{
	var length = stateInfo.length;
	for (var i=0; i<length; i++)
		stateInfo.pop();
	STATE_INFO = '';	
}

/* checkPositiveNumber 
 * ----------------------- 
 * Checks the value of the form field (elem) to see if it is a valid 
 * number. 
 */ 
function checkPositiveNumber(elem) 
{ 
  if(!isValidNum(elem.value))
  {
    return false;
  }

  var num = getBaseNumber(elem.value); 

  if (num <= 0 || isNaN(num)) 
  {
    return false; 
  } 
  return true; 
} 

/* checkProperNumberFormat 
 * ----------------------- 
 * Checks the value of the form field (elem) to see if it is a valid 
 * number. 
 */ 
function checkProperNumberFormat(elem) 
{ 
  if(!isValidNum(elem.value))
  {
    return false;
  }
  var num = getBaseNumber(elem.value); 

  if (num < 0 || isNaN(num)) 
  { 
    return false; 
  } 
  return true; 
} 

/* addToMessageStack 
 * ----------------- 
 * Appends a text message to the global error string JAVASCRIPT_MESSAGE

 * which will be displayed by the display manager. 
 */ 
function addToMessageStack(messageCode) 
{ 
  JAVASCRIPT_MESSAGE = JAVASCRIPT_MESSAGE + messageCode; 
} 
  
function addErrorAttribute(attribute)  
{
        if (ERROR_ATTR == ('errorAttr')+VALUE_DELIMITER)
                ERROR_ATTR += attribute;
        else
                ERROR_ATTR = ERROR_ATTR + ',' + attribute;
}

/*=================================================================*/

if(parseInt(navigator.appVersion) >= 4) 
{
  if (navigator.appName == "Netscape")
  {
    // Web browser is a netscape nevigator
    IS_NAV= true;
    IS_IE=false;
    coll = "";
    styleObj = "";
    window.captureEvents(Event.FOCUS|Event.CLICK |Event.RESIZE);
    suffix="_nav";
  }
  else // Internet Explorer
  {
    IS_NAV= false;
    IS_IE= true;
    coll = "all.";
    styleObj = ".style";
    layerTab= "div";
    suffix="_ie";
  }
}
else // invalid version
{
  alert(top.getTop().FND_MESSAGES['ICX_POR_ALRT_INVALID_BROWSER']);
}

mWin =null;


function setSubmitFlag()
{
  top.getTop().submitInProcess = true;
}

function clearSubmitFlag()
{
  top.getTop().submitInProcess = false;
}

function checkSubmitFlag()
{
  if (top.getTop().submitInProcess)
  {
    alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_DONT_CLICK_TWICE"]);
  }
  return top.getTop().submitInProcess;
}

function clickOrFocus(e)
{
  if (mWin&& !mWin.closed)
  {
    if(IS_IE && !window.top.objectsDisabled)
    {
      disableWindow(top);
      window.top.objectsDisabled=true;
    }
    // race condition problem exists when user closes modal window.
    // mWin may get closed after we check for mWin.closed (above).
    // The only remedy (not cure) we can come up with is to check for
    // mWin.closed again right before giving focus.

    if(mWin && !mWin.closed)
      mWin.focus();
    return false;
  }
  else
  {
    if (IS_NAV)
    {
      return routeEvent(e);
    }
    else if (window.top.objectsDisabled)
    {
      enableWindow(top);
      window.top.objectsDisabled=false;
      return true;
    }
  }
}

function disableWindow(win)
{
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)  
    {
      var elem=cForm.elements[e];
      elem.oldOnfocus= elem.onfocus;
      elem.oldOnclick=elem.onclick;
      elem.onclick= clickOrFocus;
      elem.onfocus= clickOrFocus;
    }
  }
  for (l=0; l<win.document.links.length; l++)
  {
    var cLink= win.document.links[l];
    cLink.oldOnclick= cLink.onclick;
    cLink.onclick=clickOrFocus;
    cLink.oldOnfocus= cLink.onfocus;
    cLink.onfocus=clickOrFocus;
  }
  win.onclick=clickOrFocus;
  win.onfocus=clickOrFocus;
  win.document.onfocus=clickOrFocus;
  win.document.onclick=clickOrFocus;
  for(var w=0; w<win.frames.length; w++)
    disableWindow(win.frames[w]);
}

function enableWindow(win)
{  
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)
    {
      var elem=cForm.elements[e];  
      if (elem.oldOnfocus)
            elem.onfocus= elem.oldOnfocus;
      if (elem.oldOnclick)
        elem.onclick=elem.oldOnclick;
    }
  }
  for (l=0; l<win.document.links.length; l++)
  {
    var cLink= win.document.links[l];
    if (cLink.oldOnclick)
      cLink.onclick=cLink.oldOnclick;
   
  }
  for(var w=0; w<win.frames.length; w++)
    enableWindow(win.frames[w]);
}

window.onClick= clickOrFocus;
window.onFocus= clickOrFocus;
window.onclick= clickOrFocus;
window.onfocus= clickOrFocus;

// opens a modal window  using attribute list as provided, or
// defaulted otherwise.  windowWidth and windowHeight are taken into
// consideration only if attributeList is not provided
function openModalWindow(url,   // url for the new window
                         name,  // name of the new window
                         attributeList, // attribute list for the new
                                        // window- by default, status=yes (all
                                        // else no) 
                         windowWidth,   // width for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 800 if null
                         windowHeight)  // height for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 600 if null
{
	var maxWindow = false;

	if(attributeList==null || attributeList=="") {
	    	var windowWidth= (windowWidth) ? windowWidth : 800;
    		var windowHeight= (windowHeight) ? windowHeight : 600;
		var attributeList = "";

		if (IS_IE) {
			if (screen.availWidth < 800 || screen.availHeight < 600) {
        	        	maxWindow = true;
				windowWidth = screen.availWidth;         
				windowHeight = screen.availHeight;
				attributeList=
					"menubar=no,location=no,toolbar=no"+
					",resizable=yes,scrollbars=yes,status=yes";
			} else {
                		attributeList=
                		"menubar=no,location=no,toolbar=no,width="+ windowWidth+
                		",height="+windowHeight+
                		",resizable=yes,scrollbars=yes,status=yes";
        		}
		} else {
			attributeList= 
				"menubar=no,location=no,toolbar=no,width="+ windowWidth+ 
				",height="+windowHeight+
				",resizable=yes,scrollbars=yes,status=yes";
		}
		if(IS_NAV) {
	      		var winLeft = window.screenX+((window.outerWidth -
                                       windowWidth) / 2);
		      	var winTop = window.screenY + ((window.outerHeight -
                                       windowHeight) / 2);
		      	attributeList +=",screenX=" +winLeft + ",screenY=" + winTop;
		}
		else {
		      	var winLeft = (screen.width - windowWidth) / 2;
		      	var winTop = (screen.height - windowHeight) / 2;
			if (maxWindow) {
				attributeList += ",left=0,top=0";
			} else {	
		      		attributeList += ",left=" + winLeft + ",top=" + winTop;
			}
    		}
  	}
  if(modalOpen())
  {
	mWin.openModalWindow(url, name, attributeList);
	if (IS_IE && maxWindow) {
		mWin.resizeTo(windowWidth, windowHeight);
	}
	return mWin;
  }
  else
  { 
    top.mWin= open(url, name, attributeList);

        if(IS_IE)
        {
		if (maxWindow)
			mWin.resizeTo(windowWidth, windowHeight);
		disableWindow(top);
		window.top.objectsDisabled=true;
        }
    return top.mWin;
  }
}

function getTop()
{
  if(top.IS_TOP)
  {
    return top;
  }
  else
  {
    return top.getTop();
  }
}

// returns true if this window has a modal window open
function modalOpen()
{
  return(mWin && !mWin.closed);
}

//  this function returns the upperMost modal window
function getActiveWindow()
{
  if(modalOpen())
  {
    return (mWin.getActiveWindow) ? mWin.getActiveWindow() : mWin;
  }
  else 
  {
    return window;
  }
}

// this function displays a message in the status bar
function showStatus(msg)
{
  window.status = msg;
  return true;
}

// closes all modal window in order, starting from top most window
function recursiveClose()
{
  window.onClick= null;
  window.onFocus= null;
  window.onclick= null;
  window.onfocus= null;
  var tempWin= mWin;
  mWin=null;
  
  if(tempWin && !tempWin.closed && tempWin.recursiveClose)
  {
    tempWin.recursiveClose();
  }
  else if(tempWin && !tempWin.closed)
  {
    tempWin.close();
  }
  window.close();
}


function getTabLevel2(param)
{
  var tabText = "<td class=tabinactivelevel2>&nbsp</td>";
  var label = "";
  var functionName = "";
  var level2Table = "";
  var numArgs = getTabLevel2.arguments.length;
  var j = 0;
  var count=0;
  if (!isNaN(getTabLevel2.arguments[0])) {

   subTabHighlighted =   getTabLevel2.arguments[0];
   j = 1; 
  }


  count =j;
  for (var i=j; i<numArgs; i=i+2,count++) {
    label = getTabLevel2.arguments[i];

    functionName = getTabLevel2.arguments[i+1] ? getTabLevel2.arguments[i+1] : "";

    if (functionName != "") {   

      if ( subTabHighlighted == count) {

        tabText = tabText + "<td class=tabactivelevel2><a class=linktabswhite href = javascript:" + functionName + ">" +
                          label + "</a></td>";

      }
      else
        tabText = tabText + "<td class=tabactivelevel2><a class=linktabs href = javascript:" + functionName + ">" +
                          label + "</a></td>";
    } else {
   
       tabText = tabText + "<td class=tabinactivelevel2>" + 
                label + 
                "</td>";
    }

    if (i < numArgs - 2) {
      tabText = tabText + "<td>&nbsp</td><td><img src=/OA_MEDIA/PORTBDIV.gif></td><td>&nbsp</td>";
    }
  }

  tableOpen = "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=100%>" +
              "<tr>" +
              "<TD align=right valign=bottom rowspan=2>" +
              "<IMG SRC=/OA_MEDIA/PORSTL.gif alt='' width=8 height=31 BORDER=0></TD>" +
              "<td bgcolor=#336699>" +
              "<img src=/OA_MEDIA/PORTRANS.gif alt='' border=0 height=1 width=665></td>" +
              "<TD align=left valign=bottom rowspan=2>" +
              "<IMG SRC=/OA_MEDIA/PORSTR.gif alt='' width=8 height=31 BORDER=0></TD>" +
              "</tr>" +
              "<TR>" +
              "<TD bgcolor=#336699 class=tabinactivelevel2>" +
              "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>" +
              "<TR>";

  if ((CURRENT_UI_TEMPLATE == "POROHREC") ||
      (CURRENT_UI_TEMPLATE == "PORSPLST") ||
      (CURRENT_UI_TEMPLATE == "PORRCVOD") ||
      (CURRENT_UI_TEMPLATE == "PORPBLST") ||
      (CURRENT_UI_TEMPLATE == "PORPILST")) {
    // show beige shadow
    tableClose = "</tr>" +
                 "</table>" +
                 "</td>" +
                 "</tr>" +
                 "<TR valign=top>" +
                 "<TD width=15 align=right nowrap>" +
                 "<IMG SRC=/OA_MEDIA/PORSTBBL.gif alt='' BORDER=0></TD>" +
                 "<td width=100% background=/OA_MEDIA/PORSTBL.gif>" +
                 "<IMG SRC=/OA_MEDIA/PORTRANS.gif alt='' width=1 height=8></TD>" +
                 "<TD width=15>" +
                 "<img src=/OA_MEDIA/PORSTBBR.gif alt='' BORDER=0></TD>" +
                 "</TR>" +
                 "</table>";

  } else {

    tableClose = "</tr>" +
                 "</table>" +
                 "</td>" +
                 "</tr>" +
                 "<TR valign=top>" +
                 "<TD width=15 align=right nowrap>" +
                 "<IMG SRC=/OA_MEDIA/PORSTBBL.gif alt='' BORDER=0></TD>" +
                 "<td width=100% background=/OA_MEDIA/PORSTBB.gif>" +
                 "<IMG SRC=/OA_MEDIA/PORTRANS.gif alt='' width=1 height=8></TD>" +
                 "<TD width=15>" +
                 "<img src=/OA_MEDIA/PORSTBBR.gif alt='' BORDER=0></TD>" +
                 "</TR>" +
                 "</table>"; 
  } 

  return (tableOpen + tabText + tableClose);
}


function getTabLevel1(tabName)
{

if (isRelease11i()) {


  tabImage = "PORTBHOM.gif"
 
  switch (tabName) {
    case "Home":
          tabHighlighted = 0;
          tabImage = "PORTBHOM.gif"
      break;

    case "Shop":
          tabHighlighted = 1;
          tabImage = "PORTBSHP.gif"
      break;
   
        case "Order":
          tabHighlighted = 2;
          tabImage = "PORTBREQ.gif"
      break;
 
    case "Profile":
          tabHighlighted = 3;
          tabImage = "PORTBPRF.gif"
      break;

    default:
      startImage = "PORTBNON.gif"
      break;
    
  }
          
  tabLevel1 = "<td align=right valign=bottom colspan=1 width=100%>" +
              "<MAP NAME=TabBar>" +
              " <AREA href='javascript:home();' ALT='" + FND_MESSAGES['ICX_POR_TT_HOME'] + "' " +
              " SHAPE=" + MAP_SHAPE[0] + " COORDS=" + MAP_COORDS[0] + ">" +
              "<AREA SHAPE=" + MAP_SHAPE[1] + " COORDS=" + MAP_COORDS[1] +
              " href='javascript:shop();' ALT='" + FND_MESSAGES['ICX_POR_TT_SHOP'] + "'>" +
              "<AREA SHAPE=" + MAP_SHAPE[2] + " COORDS=" + MAP_COORDS[2] +
              " href='javascript:orderHistory();' ALT='" + FND_MESSAGES['ICX_POR_TT_REQ_STATUS'] + "'>" +
              "<AREA SHAPE=" + MAP_SHAPE[3] + " COORDS=" + MAP_COORDS[3] +
              " href='javascript:myProfile();' ALT='" + FND_MESSAGES['ICX_POR_TT_MY_PROFILE'] + "'>" +
              "</MAP>" +
              "<img usemap=#TabBar src=/OA_MEDIA/" + LANGUAGE_CODE + "/" + tabImage + " border=no align=bottom>" +
              "</td>";
} else {

  startImage = "PORTSTRI.gif"
  tab1Image = "PORTHOMI.gif"
  filler1Image = "PORTFILI.gif"
  tab2Image = "PORTSHPI.gif"
  filler2Image = "PORTFILI.gif"
  tab3Image = "PORTREQI.gif"
  filler3Image = "PORTFILI.gif"
  tab4Image = "PORTPRFI.gif"
  endImage = "PORTENDI.gif"

  switch (tabName) {
    case "Home": 
          tabHighlighted = 0; 
          startImage = "PORTSTRA.gif"
	  tab1Image = "PORTHOMA.gif"
	  filler1Image = "PORTFLPA.gif"
	  tab2Image = "PORTSHPI.gif"
	  filler2Image = "PORTFILI.gif"
	  tab3Image = "PORTREQI.gif"
	  filler3Image = "PORTFILI.gif"
	  tab4Image = "PORTPRFI.gif"
	  endImage = "PORTENDI.gif"
      break;

    case "Shop":
          tabHighlighted = 1;  
          startImage = "PORTSTRI.gif"
	  tab1Image = "PORTHOMI.gif"
	  filler1Image = "PORTFLNA.gif"
	  tab2Image = "PORTSHPA.gif"
	  filler2Image = "PORTFLPA.gif"
	  tab3Image = "PORTREQI.gif"
	  filler3Image = "PORTFILI.gif"
	  tab4Image = "PORTPRFI.gif"
	  endImage = "PORTENDI.gif"
      break;
    
	case "Order": 
          tabHighlighted = 2; 
          startImage = "PORTSTRI.gif"
	  tab1Image = "PORTHOMI.gif"
	  filler1Image = "PORTFILI.gif"
	  tab2Image = "PORTSHPI.gif"
	  filler2Image = "PORTFLNA.gif"
	  tab3Image = "PORTREQA.gif"
	  filler3Image = "PORTFLPA.gif"
	  tab4Image = "PORTPRFI.gif"
	  endImage = "PORTENDI.gif"
      break;

    case "Profile": 
          tabHighlighted = 3; 
          startImage = "PORTSTRI.gif"
	  tab1Image = "PORTHOMI.gif"
	  filler1Image = "PORTFILI.gif"
	  tab2Image = "PORTSHPI.gif"
	  filler2Image = "PORTFILI.gif"
	  tab3Image = "PORTREQI.gif"
	  filler3Image = "PORTFLNA.gif"
	  tab4Image = "PORTPRFA.gif"
	  endImage = "PORTENDA.gif"
      break;

    default: 
      startImage = "PORTSTRI.gif"
	  tab1Image = "PORTHOMI.gif"
	  filler1Image = "PORTFILI.gif"
	  tab2Image = "PORTSHPI.gif"
	  filler2Image = "PORTFILI.gif"
	  tab3Image = "PORTREQI.gif"
	  filler3Image = "PORTFILI.gif"
	  tab4Image = "PORTPRFI.gif"
	  endImage = "PORTENDI.gif"
      break;

  }
 
  tabLevel1 = "<td align=right valign=bottom colspan=1 width=100%>" + 
              "<img src=/OA_MEDIA/" + startImage + " border=no>" + 
              "<A href='javascript:home();'><img src=/OA_MEDIA/" + LANGUAGE_CODE + "/" + tab1Image + " border=no ALT='" + FND_MESSAGES['ICX_POR_TT_HOME'] + "'></A>" + 
              "<img src=/OA_MEDIA/" + filler1Image + " border=no>" + 
              "<A href='javascript:shop();'><img src=/OA_MEDIA/" + LANGUAGE_CODE + "/" + tab2Image + " border=no ALT='" + FND_MESSAGES['ICX_POR_TT_SHOP'] + "'></A>" + 
              "<img src=/OA_MEDIA/" + filler2Image + " border=no>" + 
              "<A href='javascript:orderHistory();'><img src=/OA_MEDIA/" + LANGUAGE_CODE + "/" + tab3Image + " border=no ALT='" + FND_MESSAGES['ICX_POR_TT_REQ_STATUS'] + "'></A>" + 
              "<img src=/OA_MEDIA/" + filler3Image + " border=no>" + 
              "<A href='javascript:myProfile();'><img src=/OA_MEDIA/" + LANGUAGE_CODE + "/" + tab4Image + " border=no ALT='" + FND_MESSAGES['ICX_POR_TT_MY_PROFILE'] + "'></A>" + 
              "<img src=/OA_MEDIA/" + endImage + " border=no>" + 
              "</td>";

  }
  return tabLevel1;

}


/* getShoppingCartIcon
 * -------------------
 * if the user is currently in the page that is part of the shopping cart or
 * checkout flow, we use the active icon.  Otherwise, the enabled icon
 * icon is used.
 */
function getShoppingCartIcon()
{
  icon = "<a href=javascript:shoppingCart();><img src=/OA_MEDIA/PORSCEN.gif alt='"+ 
	 FND_MESSAGES['ICX_POR_TAB_SHOPPING_CART'] +
	 "' border=0></a>" + 
         "<br>" + 
         "<a href=javascript:shoppingCart();><font class=linkheader>" +
         FND_MESSAGES['ICX_POR_TAB_SHOPPING_CART'] +
         "</font></a>";
  if (isShoppingCartPage()) {
    icon = "<img src=/OA_MEDIA/PORSCAC.gif alt='"+ FND_MESSAGES['ICX_POR_TAB_SHOPPING_CART'] +
	"' border=0><br><font class=linkheader>" + 
	FND_MESSAGES['ICX_POR_TAB_SHOPPING_CART'] + "</font>";
  }

  return icon;

}


/* isShoppingCartPage
 * ------------------
 * compare user's current page to see if it belongs to the
 * SHOPPING_CART_PAGES array.
 */
function isShoppingCartPage()
{
  for(var i=0; i<SHOPPING_CART_PAGES.length; i++) {
    if (SHOPPING_CART_PAGES[i] == CURRENT_UI_TEMPLATE)
      return true;
  }
  return false;
}


function getHelpIcon()
{
  icon = "<a href=javascript:callHelp();><img src=/OA_MEDIA/PORHLPEN.gif alt='" +  
 	FND_MESSAGES['ICX_POR_PRMPT_LINKS_HELP'] +
	"' border=0></a>" + 
         "<br>" +
         "<a href=javascript:callHelp();><font class=linkheader>" +
         FND_MESSAGES['ICX_POR_PRMPT_LINKS_HELP'] +
         "</font></a>";
  if (CURRENT_UI_TEMPLATE == "PORHELP") {
    icon = "<img src=/OA_MEDIA/PORHLPAC.gif alt='" + 
	FND_MESSAGES['ICX_POR_PRMPT_LINKS_HELP'] +
	"' border=0><br><font class=linkheader>" +
           FND_MESSAGES['ICX_POR_PRMPT_LINKS_HELP'] + "</font>";
  }

  return icon;

}


function getHomeIcon()
{
  icon = "<a href=javascript:home()><img src=/OA_MEDIA/" + LANGUAGE_CODE + 
         "/PORHOME.gif alt='" + FND_MESSAGES['ICX_POR_PRMPT_LINKS_HOME'] +
	 "' border=0 ></a>";

  return icon;

}


/* displayInfoTemplate
 * -------------------
 * This function is called from saved carts, shopping cart, power checkout,
 * and approver's shopping cart when the info template link is clicked.
 */
function displayInfoTemplate(ind, formName) {
  var action="displayInfoTemplate";
  var func = "displayInfoTemplateSingle";
  var url = "/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

  var infoTemplateLineNum = top.getElementValue(ind, formName, "POR_LINE_NUM_HIDDEN");
  var reqHeaderId = top.getElementValue(ind, formName, "POR_REQ_HEADER_ID");
  var generalData = top.FIELD_DELIMITER + "infoTemplateLineNum" +
                    top.VALUE_DELIMITER + infoTemplateLineNum +
                    top.FIELD_DELIMITER + "reqHeaderId" +
                    top.VALUE_DELIMITER + reqHeaderId;

  setStateInfo("INFOTEMPLATE_PAGE_ORIGIN", CURRENT_UI_TEMPLATE);
  top.submitRequest("createReq",
                    url,
                    action,
                    func,
                    generalData,
                    new Array(""),
                    "_top");

}

/* getReturnToPortalIcon
 * ---------------------
 */
function getReturnToPortalIcon()
{
  if (directFlag=="true") {
	icon = "<a href=javascript:mainMenu();><img src=/OA_MEDIA/PORLGOFF.gif alt='"+
	 FND_MESSAGES['ICX_POR_TAB_LOG_OFF'] +
	"' border=0></a>" + "<br>"+"<a href=javascript:mainMenu();> <font class=linkheader>" +FND_MESSAGES['ICX_POR_TAB_LOG_OFF'] + "</font></a>";
  }
  else { 
  icon = "<a href=javascript:mainMenu();><img src=/OA_MEDIA/PORRTPEN.gif alt='" +
	 FND_MESSAGES['ICX_POR_TAB_MAIN_MENU'] +
	"' border=0></a>" + 
         "<br>" +
         "<a href=javascript:mainMenu();><font class=linkheader>" +
         FND_MESSAGES['ICX_POR_TAB_MAIN_MENU'] +
         "</font></a>";
  }
  return icon;

}




function mainMenu()
{
  //location.href = "http://" + getStateInfo('OAS_SERVER_NAME') + ":" + getStateInfo('OAS_SERVER_PORT') + mainMenuURL;
  location.href = returnURL;
}


tmpParamArray = window.location.search.substring(1).split('&');  

setStateInfo('OAS_SERVER_NAME', getOASServerName(tmpParamArray));
setStateInfo('OAS_SERVER_PORT', getOASServerPort(tmpParamArray));

function getOASServerName(tmpParamArray)
{
  for (i=0; i<tmpParamArray.length; i++) {
    param = tmpParamArray[i].split('=');
    if (param[0] == 'oasServerName')
      return param[1];
  }
  return getStateInfo('OAS_SERVER_NAME');
}

function getOASServerPort(tmpParamArray)
{
  for (i=0; i<tmpParamArray.length; i++) {
    param = tmpParamArray[i].split('=');
    if (param[0] == 'oasServerPort')
      return param[1];
  }
  return getStateInfo('OAS_SERVER_PORT');
}

initState();

function initResultForm(doc) 
{
  if(!doc.formsCleaned)
  {
    clearForms(doc);
  }

  initForm(doc.forms['POR_RESULT_FORM']);
  initForm(doc.forms['POR_PAGE_INFO']);
  initForm(doc.forms['POR_REQ_INFO_FORM']);

  changeImages(doc);
  changeLinks(doc);
  changeButtonText(doc);
  window.onclick= clickOrFocus;

  window.onfocus= clickOrFocus;

  top.noSave= false;

  clearSubmitFlag();

  setReqSessionInfo();
 
// aahmad 2/12/2000. moved this to end of PORUTIL.js,
// because need it in tabs before the initialize() function is run.
// initState();

} // end of function initDocument()


// The function converts a formatted number (e.g. 1,234.56) to 
// its based format (e.g. 1234.56).  The valid format is defined 
//by the decimal and group separator
function getBaseNumber(x) {
        x = x+"";
	var y = "";
	var decimal = x.length;
	var group = 0;

	for (var i = x.length; i >= 0; i--) {
		if (x.charAt(i) == decSeparator ) {
			y = "." + y;
			decimal = i;
		} else {
			if (x.charAt(i) != groupSeparator )
				y = x.charAt(i) + y;
		}
	}
	return y;
}


// The function formats a base number (e.g. 1234.56) to a formatted
// number (e.g. 1,234.56).  The number format is defined by the 
// decimal and group separator
function formatNumber(x) {
        x = x+"";
	var y = "";
	var group_flag = false;
	var count = 0;
	var group_num = 3;

	decimal = x.length;

	for (var i = x.length; i >= 0; i--) {
		if (x.charAt(i) == '.') {
			y = decSeparator + y;
			group_flag = true;
			count = 0;
		} else {
			if (count == group_num && group_flag == true) {
				y = groupSeparator + y;
				count = 0;
			}
			y = x.charAt(i) + y;
			count++;
		}
	}
	return y;
}


// This is for the tabs
                        
function initTabs()
{
	if (!top.TabMapShape || !top.TabMapCoords) {
		return;
        }
	if (TabMapShape)
	        MAP_SHAPE = TabMapShape.split(LINE_DELIMITER);
	if (TabMapCoords)
	        MAP_COORDS = TabMapCoords.split(LINE_DELIMITER);
}
         
initTabs();

// The following is for the train tracks
                        
        visited_station_l = '/OA_MEDIA/PORTRVSL.gif';
        visited_station_r = '/OA_MEDIA/PORTRVSR.gif';
        current_station_l = '/OA_MEDIA/PORTRCSL.gif';
        current_station_r = '/OA_MEDIA/PORTRCSR.gif';
        normal_station_l = '/OA_MEDIA/PORTRNSL.gif';
        normal_station_r = '/OA_MEDIA/PORTRNSR.gif';

        visited_track = '/OA_MEDIA/PORTRV.gif';
        normal_track = '/OA_MEDIA/PORTRN.gif';
        transparent = '/OA_MEDIA/PORTRTP.gif';

function drawTrainTrack(trainTrack, selectedIndex)
{
        startStation = 0;
        endStation = trainTrack.length-1;
        stationIndex = 0;

        print_station_l = normal_station_l;
        print_station_r = normal_station_r;
        print_track = normal_track

        document.write('<table border="0" cellpadding="0" cellspacing="0" width="100% align="center"><tr>');
        document.write('<tr><td height=10><img src=/OA_MEDIA/FNDITPNT.gif alt="" border=no></td></tr>');

        for (stationIndex=0; stationIndex < trainTrack.length; stationIndex++) {
        
        if (stationIndex<selectedIndex) {
                print_station_l = visited_station_l;
                print_station_r = visited_station_r;
        }else if (stationIndex==selectedIndex) {
                print_station_l = current_station_l;
                print_station_r = current_station_r;
        } else {
                print_station_l = normal_station_l;
                print_station_r = normal_station_r;
        }
        
        
        if (stationIndex==startStation) {
                document.write('<td align="right"><img src="'+print_station_l+'" alt="" width="12" height="23"></td>');
        } else {
                if (stationIndex <= selectedIndex)
                        print_track = visited_track;
                else
                        print_track = normal_track;

    document.write('<td>');
      document.write('<table border="0" cellpadding="0" cellspacing="0" width="100%" background="'+print_track+'" height="23">');
        document.write('<tr>');
          document.write('<td align="right"><img src="'+print_station_l+'" alt="" width="12" height="23"></td>');
        document.write('</tr>');
      document.write('</table>');
    document.write('</td>');
                
                
        } 
        if (stationIndex==endStation) {
                document.write('<td align="left"><img src="'+print_station_r+'" alt="" width="11" height="23"></td>');
        } else {
                if (stationIndex < selectedIndex)
                        print_track = visited_track;
                else
                        print_track = normal_track;
 
    document.write('<td>');
      document.write('<table border="0" cellpadding="0" cellspacing="0" width="100%" background="'+print_track+'" height="23">');
        document.write('<tr>');
          document.write('<td align="left"><img src="'+print_station_r+'" alt="" width="11" height="23"></td>');
        document.write('</tr>');
      document.write('</table>');
    document.write('</td>');
        
        }
        if (stationIndex!=endStation) {
                if (stationIndex < selectedIndex)
                        print_track = visited_track;
                else
                        print_track = normal_track;
    document.write('<td>');
      document.write('<table border="0" cellpadding="0" cellspacing="0" width="100%" background="'+print_track+'" height="23">');
        document.write('<tr>');
          document.write('<td><img src="'+transparent+'" alt="" width="20" height="1"></td>');
        document.write('</tr>');
      document.write('</table>');
    document.write('</td>');
                        
        }
    
        }

        document.write('</tr><tr>');

        for (stationIndex=0; stationIndex < trainTrack.length; stationIndex++) {
                var classStyle;
                if (stationIndex==selectedIndex)
                        classStyle = 'trainCurrent';
                else if (stationIndex < selectedIndex)
                        classStyle = 'trainVisited';
                else
                        classStyle = 'trainNormal';
                
                document.write('<td colspan="2" align=center class='+classStyle+'>' + FND_MESSAGES[trainTrack[stationIndex]] + '</td>');
                if (stationIndex!=endStation) {
                        document.write('<td></td>');
                }
        }
        document.write('</tr>'); 
        document.write('<tr><td height=10><img src=/OA_MEDIA/FNDITPNT.gif alt=""  border=no></td></tr>');
        document.write('</table>');
         
}   


function isRelease11i()
{
	if (appsVersion.indexOf('11.5', 0)==0)
		return true;
	else return false;
}


function isValidNum(x){
        x=x+"";
	var count = 0;
	var group_num = 3;

	var j = x.length;

	for (var i = x.length; i >= 0; i--) {
		if (x.charAt(i) == decSeparator ) {
		       j = i-1;
			break;

		}
         }
       
       for ( var k = j ; k>=0; k--){
               if ( x.charAt(k) == groupSeparator){ 
	              if ( count < group_num ){
	                   return false; 
                      }
	              else{
	                 count = 0;
	              }
                }
                else{ 
	             count++;
                }
      }

      return true;

}
