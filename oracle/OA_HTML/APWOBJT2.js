//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT2.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |                                                |
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJCT.js is split into 4 files |
//  |              APWOBJT1.js, APWOBJT2.js ,        |
//  |              APWOBJT3.js, APWOBJT4.js          |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT2.js 115.1 2000/11/09 13:53:02 pkm ship        $ */
/*===========================================================================*
 *  creditCardData Class                                                     *
 *===========================================================================*/

  //creditCardData constructor

  function creditCardData()
  {
    this.cardProgName = new Array();
    this.trxns        = new Array();
    this.paymtDueFrom = new Array();
    this.loaded       = false;
  }

  g_objCCardData = new creditCardData();

  creditCardData.prototype.dataLoaded            = CCD_dataLoaded;
  creditCardData.prototype.setLoaded             = CCD_setLoaded;
  creditCardData.prototype.add                   = CCD_add;
  creditCardData.prototype.getTrxns              = CCD_getTrxns;
  creditCardData.prototype.getPaymtDueFrom       = CCD_getPaymtDueFrom;
  creditCardData.prototype.getCardProgNames      = CCD_getCardProgNames;
  creditCardData.prototype.isDataChanged         = CCD_isDataChanged;
  creditCardData.prototype.getDataChanged        = CCD_getDataChanged;
  creditCardData.prototype.insertTrxn            = CCD_insertTrxn;
  creditCardData.prototype.packPersonalTrxns     = CCD_packPersonalTrxns;


  function CCD_dataLoaded() 
  { 
    return this.loaded; 
  }

  function CCD_setLoaded(loaded) 
  { 
    this.loaded = loaded; 
  }

  // add card program method
  function CCD_add(name, trxns, paymtDueFrom)
  {
    this.cardProgName[this.cardProgName.length] = name;
    this.trxns[this.trxns.length]               = trxns;
    this.paymtDueFrom[this.paymtDueFrom.length] = paymtDueFrom;
  }

  //CCD_getTrxns method to return the trxns for a card program
  function CCD_getTrxns(name)
  {
    for (var i=0; i<this.cardProgName.length; i++)
      if (this.cardProgName[i] == name) 
        return this.trxns[i];

    return null;
  }

  //CCD_getPaymtDueFrom method to return the paymentDueFrom for a card program
  function CCD_getPaymtDueFrom(name)
  {
    for (var i=0; i<this.cardProgName.length; i++)
      if (this.cardProgName[i] == name) 
        return this.paymtDueFrom[i];

    return null;
  }


  // method to return the array of card program name
  function CCD_getCardProgNames()
  { 
    return this.cardProgName; 
  }


  // To check if data(category) changed
  function CCD_isDataChanged()
  { var t;

    for (i=0; i< this.trxns.length; i++){   
      this.trxns[i].reset();
      t = this.trxns[i].nextTrxn();
      while (t != null){    
        if (t.isCategoryChanged()) return true;
          t = this.trxns[i].nextTrxn();
      }
    }

    return false;
  }

  // To return the trxns whose categories were changed. The format is:
  // <number>"@att@"<id>"@att@"<category>"@att@"<id>"@att@"<category>"@att@"...
  function CCD_getDataChanged()
  { 
    var s = "";
    var count=0;

    for (i=0; i< this.trxns.length; i++){   

      this.trxns[i].reset();
      t = this.trxns[i].nextTrxn();

      while (t != null){    

        if (t.isCategoryChanged()){   
          s += t.getId() + "@att@" + t.getCategory() + "@att@";
          count++;
        }
        t = this.trxns[i].nextTrxn();
      }

    }

    s = count + "@att@" + s;
    return s;
  }

  function CCD_insertTrxn(cardProgName, 
                          id, 
                          date, 
                          folioType, 
                          merchant, 
                          city, 
                          amt, 
                          trxCurrency, 
                          trxAmt, 
                          category)
  {
    for (i=0; i< this.cardProgName.length; i++){   

      if (this.cardProgName[i] == cardProgName){   

        this.trxns[i].insert(id, 
                             date, 
                             folioType, 
                             merchant, 
                             city, 
                             amt, 
                             trxCurrency, 
                             trxAmt, 
                             category);
         return;
       }
     }
  }


  // To return the trxns whose categories is PERSONAL. The format is:
  // <number>"@att@"<id>"@att@"<billed amount>"@att@"<id>"@att@"<billed amount>"@att@"...
  function CCD_packPersonalTrxns(cardProgName)
  { 
    var s = "";
    var count=0;

    for (i=0; i< this.trxns.length; i++){ 
  
      if (this.cardProgName[i] == cardProgName) {

        this.trxns[i].reset();
        t = this.trxns[i].nextTrxn();

        while (t != null){    

          if (t.getCategory() == g_cPersonal){   
            s += t.getId() + "@att@" + t.getAmt() + "@att@" + t.getTrxAmt() +
                  "@att@" + t.getDate() + "@att@" + t.getTrxCurr() + "@att@";
	    count++;
          }
               
          t = this.trxns[i].nextTrxn();
        }

        if (s != "") 
          s = count + "@att@" + s;
          return s;
        }
    }
    return s;
  }
