<!-- hide the script's contents from feeble browsers
<!-- $Header: hrtmempw.js 115.1 2001/01/17 11:48:47 pkm ship $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date         Name          Bug #    Reason
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 110.11  22-Dec-2000  lma           1414461  Validate numeric fields on
<!--                                             next_button_onClick 
<!-- 110.10  02-Sep-1999  jxtan         934577   changed cancel button target
<!--                                             to container_middle.
<!-- 110.9   30-Jun-1999  jxtan         920731   add newtarget para to back
<!--                                             button_onClick. 
<!--                                    921386   general fix. Always display msg
<!--                                             when cancel/back clicked.
<!-- 110.8   22-Jun-1999  jxtan                  Add code to clear bottom 
<!--                                             frames when button
<!--                                             clicked
<!-- 110.7   21-Jun-1999  jxtan                  Removed Save, help gif
<!--                                             handling.
<!-- 110.6   20-Jun-1999  jxtan                  Add comments too long alert to
<!--                                             next_button_onClick.
<!--                                             Delete Reload, Print, Stop
<!--                                             gif handling
<!-- 110.4   17-Jun-1999  jxtan                  correct syntax.
<!-- 110.0   17-Jun-1999  jxtan                  Created.
<!-----------------------------------------------------------------------------
<!-----------------------------------------------------------------------------
<!-- Termination module specific js file. 
<!--  
<!-----------------------------------------------------------------------------


// The following function is invoked from the
// header frame when the user clicks on the 
// 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.workspaceForm.p_result_code.value = 'MAIN_MENU';  
  if (confirm(msg)) {
       document.workspaceForm.target = '_top'; 
       document.workspaceForm.submit();
  }
}

//Set form has been changed
function set_form_changed () {
  document.workspaceForm.p_form_changed.value="Y";
}

// The following function is invoked from the 
// container_bottom frame when the user clicks
// on the 'Reset Page' button.
function reset_button_onClick () { 
//  alert ('reset_button_onClick');
  document.workspaceForm.reset();
}

//when user click cancel button
function back_button_onClick (msg, newtarget) {
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workspaceForm.p_result_code.value = 'PREVIOUS';
        document.workspaceForm.target = newtarget;
        document.workspaceForm.submit();
     }
}


// container_bottom frame when the user clicks
// on the 'Cancel' button.
function cancel_button_onClick (msg) {
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workspaceForm.p_result_code.value = 'CANCEL';
        document.workspaceForm.target = 'container_middle';
        document.workspaceForm.submit();
     }
}


// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Next' button.
function next_button_onClick (msg1, msg2,msg3,decimalPoint) {
  document.workspaceForm.p_redraw.value ="N";
  if(checkMandatoryFields(document.workspaceForm, requiredFieldList,
fieldPromptList, msg1)) {
    if(checkNumericFields(document.workspaceForm,numericFieldList,
                          numericPromptList, msg3,decimalPoint)) {
     if ( !is_blank(document.workspaceForm.p_comments.value)) {
         if ( document.workspaceForm.p_comments.value.length > 2000) {
            alert (msg2);
         } else {
            top.clear_container_bottom_frame();
            document.workspaceForm.p_result_code.value = 'NEXT';
            document.workspaceForm.submit();
         }
     }else {
            top.clear_container_bottom_frame();
            document.workspaceForm.p_result_code.value = 'NEXT';
            document.workspaceForm.submit();
         }
    }
  }
}

// This function is called to go back to the main menu
// pointed to by loc.
function goto_main_menu (msg,loc) {
  alert (msg);
  parent.container_middle.location.href = loc;
} 

function set_redraw_flag () {
  document.workspaceForm.p_redraw.value ="Y";
  document.workspaceForm.submit();
}
