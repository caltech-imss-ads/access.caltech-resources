/* $Header: czLList.js 115.22 2001/06/14 15:34:14 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function LogicList ()
{
  this.parentConstructor = List;
  this.parentConstructor ();

  this.type = 'LogicList';
  this.cellRenderClass = 'OptionItem';
  this.listModel.setSelectionMode (ListModel.SELECT_NONE);
  this.priceVisible = false;
  this.atpVisible = false;

  //by default Prompt will be used to display description
  this.bUseImg = false;
  //by default we show state icons
  this.bNoIcon = false;
  this.bLogClr = false;
  this.bLogBld = false;
}

function LogicList_addOptionItem (rtid, item, index, state, price, atp, visible, imgSrc)
{
  this.cellRenderClass = 'OptionItem';
  return this.addLogicItem (rtid, item, index, state, null, price, atp, visible, imgSrc);
}

function LogicList_addCountedOptionItem (rtid, item, index, state, count, price, atp, visible, imgSrc)
{
  this.cellRenderClass = 'CountedOptionItem';
  return this.addLogicItem (rtid, item, index, state, count, price, atp, visible, imgSrc);
}

function LogicList_addBomItem (rtid, item, index, state, count, price, atp, visible)
{
  this.cellRenderClass = 'BomItem';
  return this.addLogicItem (rtid, item, index, state, count, price, atp, visible);
}

function LogicList_addLogicItem (rtid, item, index, state, count, price, atp, visible, imgSrc)
{
  if (typeof (item) == 'string') {
    var str = item;
    item = new LogicItemModel ();
    item.setText (str);
  }
  if (rtid) item.setName (rtid);
  if (state) item.setState (state);
  if (count != null) item.setCount (count);
  if (price != null) item.setPrice (price);
  if (atp != null) item.setATP (atp);
  if (imgSrc != "") item.setDescImage (IMAGESPATH + imgSrc);
  item.setVisibility (visible);
  this.listModel.addItem (rtid, item, index);
  return item;
}

function LogicList_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
}

function LogicList_buildCellRenderers()
{
  if (this.visibleRowCount == 0) {
    this.visibleRowCount = Math.ceil(this.height / this.itemHt);
  } else {
    var count = this.listModel.getItemCount ();
    if ((count > 0) && (count < this.visibleRowCount)) {
      this.visibleRowCount = count;
    } else if ((count == 0) && (this.mode == List.MODE_DROPDOWN)) {
      this.visibleRowCount = 1;
    }
    this.setHeight (this.visibleRowCount * this.itemHt + 2);
  }
  
  var cell;
  //Cache the column Descriptor and set this one to all the List Items;
  cell = eval('new ' + this.cellRenderClass + '();');
  this.columnDescriptors = cell.getColumnDescriptors (this.bUseImg, this.bNoIcon);
  this.colDescHash = cell.getColumnDescriptorsHash ();

  for (var i = 0; i < this.visibleRowCount; i++) {
    cell = eval('new ' + this.cellRenderClass + '();');
    cell.setName(this.name + 'Cell' + i);
    cell.setColumnDescriptors (this.columnDescriptors, this.colDescHash);
    cell.setParentObject(this);
    if (ns4) {
      cell.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    } else if (ie4) {
      cell.setReference (this.reference);
    }
    cell.setDimensions (0, this.itemHt*i, this.width, this.itemHt);
    cell.setBackgroundColor (this.itemBackgroundColor);
    cell.setActiveBackColor (this.activeBackColor);
    cell.setSelectedBackColor (this.selectedBackColor);
    cell.setSpanClass (this.spanClass);
    cell.setColor (this.color);
    // logic specific implementation;
    if (cell.setImages) {
      cell.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight)
    }
    cell.setPriceVisible (this.priceVisible);
    cell.setATPVisible (this.atpVisible);
    //set logic state props like no icon, color and boldness
    cell.setLogicStateProps (this.bNoIcon, this.bLogCol, this.bLogBld);
    cell.index = i;
    cell.setWindowName (this.windowName);
    cell.setModelItem (this.listModel.getItemAtIndex(i));
    this.cells[i] = cell;
    cell.setWindowName (this.windowName);
    cell.addListener ('actionMouseUp', this);
    cell.addListener ('actionOnChange', this);
  }
  // we only do this ONCE;
  this.cellsBuilt = true;
}

function LogicList_setPriceVisible (bVis)
{
  this.priceVisible = bVis;
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].setPriceVisible (bVis, true);
  }
  if (this.launched) {
    if (! this.bPriceColUpdated) {
      this.updateNewColumnWidth (this.colDescHash['price']);
      this.updateColumnPosition ();
      this.fixItemDimensions ();
      this.bPriceColUpdated = true;

      this.adjustLastCellHeight ();
      this.updateScrollBarVisibility();
      this.updateHorizontalScrollValues();
      this.updateVerticalScrollValues();
    }
  }
}

function LogicList_setATPVisible (bVis)
{
  this.atpVisible = bVis;
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].setATPVisible (bVis);
  }
  if (this.launched) {
    if (! this.bAtpColUpdated) {
      this.updateNewColumnWidth (this.colDescHash['atp']);
      this.updateColumnPosition ();
      this.fixItemDimensions ();
      this.bAtpColUpdated = true;

      this.adjustLastCellHeight ();
      this.updateScrollBarVisibility();
      this.updateHorizontalScrollValues();
      this.updateVerticalScrollValues();
    }
  }
}

function LogicList_updateNewColumnWidth (colDesc)
{
  var charWidth, charHeight;
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  if (colDesc.name == 'atp') {
    prefWidth = 10 * charWidth + colDesc.pdgLeft;
    prefHeight = charHeight; 
  } else if  (colDesc.name == 'price') {
    prefWidth = 8 * charWidth + colDesc.pdgLeft;
    prefHeight = charHeight;
  }
  colDesc.setPrefWidth (prefWidth);
  colDesc.setPrefHeight (prefHeight);
  
  this.cellPrefWidth += prefWidth;
  if (this.cellPrefHeight < prefHeight) {
    this.cellPrefHeight = prefHeight;
  }
}

function LogicList_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function LogicList_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.getLeft() + this.getWidth() + 8;
    var tpos = this.getTop();
    uicon.setDimensions (lpos, tpos, 16, 16);
    var ref = this.getReference ();
    uicon.setReference (ref);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = ref.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = ref.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicList_setMaxColWidth (colName, colWd)
{
  this.listModel.setMaxColWidth (colName, colWd);
}

function LogicList_useImageForDesc (bVal)
{
  if (this.launched)
    return;

  this.bUseImg = bVal;
}

function LogicList_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;

  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld;
}

HTMLHelper.importPrototypes(LogicList,List);

//public methods
LogicList.prototype.addOptionItem = LogicList_addOptionItem;
LogicList.prototype.addCountedOptionItem = LogicList_addCountedOptionItem;
LogicList.prototype.addBomItem = LogicList_addBomItem;
LogicList.prototype.setImages = LogicList_setImages;
LogicList.prototype.setMaxColWidth = LogicList_setMaxColWidth;
LogicList.prototype.setUnSatisfied = LogicList_setUnSatisfied;
LogicList.prototype.hideFeature = LogicList_hideFeature;
LogicList.prototype.setPriceVisible = LogicList_setPriceVisible;
LogicList.prototype.setATPVisible = LogicList_setATPVisible;
LogicList.prototype.useImageForDesc = LogicList_useImageForDesc;
LogicList.prototype.setLogicStateProps = LogicList_setLogicStateProps;

//override
LogicList.prototype.buildCellRenderers = LogicList_buildCellRenderers;

//private
LogicList.prototype.addLogicItem = LogicList_addLogicItem;
LogicList.prototype.updateNewColumnWidth = LogicList_updateNewColumnWidth;
