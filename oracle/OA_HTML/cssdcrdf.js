//  $Header: cssdcrdf.js 115.3 2000/04/12 14:18:23 pkm ship  $ 
 var valuechanged = false;
 var ns4 = (document.layers)? true:false;
 var ie4 = (document.all)? true:false;



function MM_openBrWindow(ID, theURL,winName,features, frm) {
	var theFrm = document.forms[frm];
	var item = 'phone1='+ theFrm.elements["phone_number"+ID].value;
	item = item + '&type1='+ theFrm.elements["phone_type"+ID].value;
	item  = item + '&phone2='+theFrm.elements["cont"+ID+"_phone_num0"].value+ '&type2='+theFrm.elements["cont"+ID+"_phone_type0"].value;
	item = item + '&phone3='+theFrm.elements["cont"+ID+"_phone_num1"].value+ '&type3='+theFrm.elements["cont"+ID+"_phone_type1"].value;
	item = item + '&phone4='+theFrm.elements["cont"+ID+"_phone_num2"].value+ '&type4='+theFrm.elements["cont"+ID+"_phone_type2"].value;
	item = item + '&phone5='+theFrm.elements["cont"+ID+"_phone_num3"].value+ '&type5='+theFrm.elements["cont"+ID+"_phone_type3"].value;
	item = item + '&phone6='+theFrm.elements["cont"+ID+"_phone_num4"].value+ '&type6='+theFrm.elements["cont"+ID+"_phone_type4"].value;
	item = item + '&phone7='+theFrm.elements["cont"+ID+"_phone_num5"].value+ '&type7='+theFrm.elements["cont"+ID+"_phone_type5"].value;
	item = item + '&lineID=' +ID+ '&parentFrm=' + frm; 
	var x = window.open(theURL+"?"+item,winName,features);
	x.opener = self;
        x.focus();
}
function passback(lovform,prid,prdesc,prver,prever,cpid,cpdesc,cpver,subid,subdesc, plid,pldesc,plver,ecid1,ecid2,ecid3,ecdesc1,ecdesc2,ecdesc3,caller) {
  document.forms[lovform].elements["productID"].value = prid;
  document.forms[lovform].elements["productDesc"].value = prdesc;
  document.forms[lovform].elements["pVer"].value = prver;
  document.forms[lovform].elements["pEver"].value = prever;
  document.forms[lovform].elements["compID"].value = cpid;
  document.forms[lovform].elements["compDesc"].value = cpdesc;
  document.forms[lovform].elements["compVer"].value = cpver;
  document.forms[lovform].elements["subcompID"].value = subid;
  document.forms[lovform].elements["subcompDesc"].value = subdesc;
  document.forms[lovform].elements["platformID"].value = plid;
  document.forms[lovform].elements["platformDesc"].value = pldesc;
  document.forms[lovform].elements["platformVer"].value = plver;
  document.forms[lovform].elements["ecID1"].value = ecid1;
  document.forms[lovform].elements["ecID2"].value = ecid2
  document.forms[lovform].elements["ecID3"].value = ecid3;
  document.forms[lovform].elements["ecDesc1"].value = ecdesc1;
  document.forms[lovform].elements["ecDesc2"].value = ecdesc2;
  document.forms[lovform].elements["ecDesc3"].value = ecdesc3;
  document.forms[lovform].action=caller;
  document.forms[lovform].submit();
}


//****************************************************************************
// Select a product 
//****************************************************************************
function getPrLOV() {
	document.forms["create_def"].elements["fromLOV"].value = "product";
	document.forms["create_def"].action = "cssprlov.jsp";
        document.forms["create_def"].submit();
}

//****************************************************************************
// Select a product version
//****************************************************************************
function getPvLOV() {
  document.forms["create_def"].elements["fromLOV"].value = "productVer";
  document.forms["create_def"].action = "csspvlov.jsp";
  document.forms["create_def"].submit();
}                     

function getLOV(from,to){
  document.forms["create_def"].elements["fromLOV"].value = from;
  document.forms["create_def"].action = to;
  document.forms["create_def"].submit();
}

//*****************************************************************************
//check mandatory fileds. 
//*****************************************************************************
function isEmpty(name)  {
 if (name =="type" || name=="status" || name =="severity"){
  var pos = document.forms["create_def"].elements[name].selectedIndex;
  var str = document.forms["create_def"].elements[name].options[pos].value;
  }
 else  { 
 var str = document.forms["create_def"].elements[name].value; 
 }
 if (str == null || str =="") {
   alert("Please fill out "+"'"+ name+"'"+" field!"); 
   return true
  }
 return false 
}
function checkform(){
 if (!isEmpty("summary")) {
   if (!isEmpty("product")) {
     if (!isEmpty("prod_ver")) {
       if (!isEmpty("earliest_ver")) {
         if (!isEmpty("status")) {
           if (!isEmpty("type")) {
             if (!isEmpty("severity")) {
               document.forms["create_def"].submit();
               return true; 
             }
           }
         }
       }
     }
   }
 }
}
/*
function checkPID(ele) {
 var pid = document.forms["create_def"].elements[ele].value;
 if (pid="" ||pid == null ) {
    alert (ele+" not selected");
    document.forms["create_def"].elements[ele].focus();
    document.forms["create_def"].elements[ele].select();
    return false;
 }
 else {
    return true;
 }
} 
*/
function checkPID() {
 var pid = document.forms["create_def"].elements["productID"].value;
 var pdesc = document.forms["create_def"].elements["product"].value;
 if (pid=="" ||pid=="null" ||pid == null || pdesc==""|| pdesc ==null) {
    alert ("Product not selected");
    document.forms["create_def"].elements["product"].focus();
    document.forms["create_def"].elements["product"].select();
    return false;
 }
 else {
    return true;
 }
} 
function clearEC(m) {
document.forms["create_def"].elements["error_code"+m].value="";
document.forms["create_def"].elements["ecID"+m].value="";
}

//*****************************************************************************
// Since nescape browser will lose the hidden field value, the these two function 
// used the restore the value
//*****************************************************************************
function restoreHiddenValues(){
	var srcForm;
	var tarForm; 
	if(ns4){
		var hiddenDiv = document.HIDDENFIELDS;
		var srcForm = hiddenDiv.document.forms["create_opp_h"];
		var tarForm = document.forms["create_opp"];
   	}
  	if(ie4){
        	var hiddenDiv = document.all["HIDDENFIELDS"];
        	var srcForm = hiddenDiv.document.forms["create_opp_h"];
		var tarForm = document.forms["create_opp"];
   	}	  
	for(var i=0; i<srcForm.elements.length;i++){
		tarForm.elements[srcForm.elements[i].name+"_m"].value= srcForm.elements[i].value;
        }

}

function changeHiddenValue(frm,elem,value){
	if(ns4){
		var hiddenDiv = document.HIDDENFIELDS;
		hiddenDiv.document.forms[frm].elements[elem].value = value;	
   	}
   	if(ie4){
		document.all["HIDDENFIELDS"].document.forms[frm].elements[elem].value = value;
   	}
}


//******************************************************************************************
// Check if the input data is number
//******************************************************************************************
function isNumber(obj, invNum){
	var myval = obj.value;
	var numStr = "012345556789.,";
	var charStr;
	var counter = 0;
	var i =0;
	var error = invNum;
	
	for(i; i<myval.length; i++){
  		charStr = myval.substring(i, i+1);
  		if(numStr.indexOf(charStr) !=-1)
   	  		counter ++;
 		}
	if(counter !=myval.length){
  		alert(error);
  		obj.value="";
  		obj.focus();
   	}
} 



function remove_commas(mynum)
{    
 var temp= mynum;
 var newnum,idx;
 while ((idx=temp.indexOf(",")) != -1)
 {
   newnum=temp.substring(0,idx);
   newnum+=temp.substring(idx+1,temp.length);
   temp=newnum;
 }
 return temp;
}   


 
//***********************************************************************************************
// Used to track which field's value is changed on the screen
//***********************************************************************************************
function markRow(frm, elm, hiddenfrm, hiddenelm){
	valuechanged = true;
	document.forms[frm].elements[elm].value="1";
	var str = "1";
	changeHiddenValue(hiddenfrm, hiddenelm, str); 

}



//***********************************************************************************************
// check if want to save data when user is leaving the page 
//***********************************************************************************************
/*function checkUnsavedData(UnsavedErr){
	if(valuechanged){
		var UnsaveMessage = UnsavedErr;
		if(confirm(UnsaveMessage)){
			valuechanged = false;
			document.forms["create_opp"].submit();
		}
	return false;
   	}
}
*/




//***************************************************************************************************
// Do client validation before submit the data
//***************************************************************************************************


function Validation_validate(val, mask, valFunction){
        var res=true;
        if(val == "")
                return res;

        if(mask)
        {
                var regExp = new RegExp(mask);
                if(regExp.exec(val))
                        res = true;
                else
                        res = false;
        }
                   
        if(res)
        {
                if(valFunction)
                {
                        res = valFunction(val);
                        return res;
                }
        }

        return res;     
}

Validation = new Object();
//Validation['mm/yyyy'] = '^\\d{2}/\\d{4}\\s*$';
// = '^\\d{2}/\\d{2}\\s*$';


Validation['DD/MM/YYYY'] = '^\\d{2}/\\d{2}/\\d{4}\\s*$';
Validation['DD/MMM/YYYY']= '^\\d{2}/\\d{3}/\\d{4}\\s*$';
Validation['DD/MON/YYYY']= '^\\d{2}/\\d{3}/\\d{4}\\s*$';

Validation['MMM/DD/YYYY'] = '^\\d{3}/\\d{2}/\\d{4}\\s*$';
Validation['MON/DD/YYYY'] = '^\\d{3}/\\d{2}/\\d{4}\\s*$';

Validation['MN-DD-YY'] = '^(\\d{1,2})[\\s-,\\/](\\d{1,2})[\\s-,\\/](\\d{2})\\s*$';
Validation['DD-MN-YY'] = '^(\\d{1,2})[\\s-,\\/](\\d{1,2})[\\s-,\\/](\\d{2})\\s*$';

Validation['MN-DD-YYYY'] = '^(\\d{1,2})[\\s-,\\/](\\d{1,2})[\\s-,\\/](\\d{4})\\s*$'; 
Validation['DD-MN-YYYY'] = '^(\\d{1,2})[\\s-,\\/](\\d{1,2})[\\s-,\\/](\\d{4})\\s*$';

Validation['DD-MON-YY'] = '^(\\d{2})[\\s-,\\/]\\s*(\\w{3})\\s*[\\s-,\\/]\\s*(\\d{2})\\s*$';
Validation['DD-MON-YYYY'] = '^(\\d{2})[\\s-,\\/]\\s*(\\w{3})\\s*[\\s-,\\/]\\s*(\\d{4})\\s*$';

Validation['MON-DD-YY'] = '^\\s*(\\w{3})\\s*[\\s-,\\/]\\s*(\\d{2})\\s*[\\s-,\\/]\\s*(\\d{2})\\s*$';
Validation['MON-DD-YYYY'] = '^\\s*(\\w{3})\\s*[\\s-,\\/]\\s*(\\d{2})\\s*[\\s-,\\/]\\s*(\\d{4})\\s*$';

Validation['$n,nnn.nn']= '^\\$? *[\\d,\\,]*\\.?\\d?\\d?$';
Validation['$n,nnn.nn']= '^ *[\\d,\\,]*\\.?\\d?\\d?$';
Validation.validate = Validation_validate;

