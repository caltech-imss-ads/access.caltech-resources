/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: posaslet.js 115.0 99/10/14 16:20:38 porting shi $ */

/*------------------------- Toolbar Events ------------------------*/


/* cancelClicked
 * -------------
 * This event handler is triggered when the user clicks the cancel button
 * or the cancel icon on the toolbar.  It will open up a modal window called
 * 'Cancel' that will prompt the user to confirm the 'cancel' action.
 */
function AslCancelClicked()
{
  var winWidth = 400;
  var winHeight = 200;
  var winAttributes = "menubar=no,location=no,toolbar=no," + 
                      "width=" + winWidth + ",height=" + winHeight + 
	              ",screenX=" + (screen.width - winWidth)/2 + 
                      ",screenY=" + (screen.height - winHeight)/2 + 
                      ",resizable=yes,scrollbars=yes";
  var url = top.getTop().scriptName + "/pos_window_sv.dialogbox";
  top.openModalWindow(url, "Cancel", winAttributes, winWidth, winHeight);
}




/* refreshFrame
 * ------------
 * This is the event handler for the refresh/reload button on the
 * toolbar.  It will only refresh frames with names other than those
 * listed below.
 */
function AslRefreshFrame(p_frame)
{
  var i;
  var frameName;

  if (!p_frame)
    p_frame = top;

  if (p_frame.frames.length == 0)
  {  
    frameName = p_frame.name;
    if ((frameName != "toolbar") && (frameName != "borderLeft") &&
        (frameName != "upperbanner") && (frameName != "lowerbanner") &&
        (frameName != "borderRight") && (frameName != "controlregion"))
    {
      p_frame.location.reload();
    }
    return;
  }

  for (i=0; i<p_frame.frames.length; i++)
  {
     AslRefreshFrame(p_frame.frames[i]);
  }

}


/* stopLoading
 * -----------
 * This is the event handler for the stop button on the toolbar.  It stops
 * all frames in the current window from loading.
 * Note: this does not work on IE.
 */
function AslStopLoading(p_win)
{
  var i;

  if (top.IS_IE)
  {
    alert("This does not work on IE");
    return;
  }

  if (!p_win)
    p_win = top;

  if (p_win.frames.length == 0)
  {
    p_win.stop();
  }

  for (i=0; i<p_win.frames.length; i++)
  {
    AslStopLoading(p_win.frames[i]);
  }

}


/* printWindow
 * -----------
 * Don't know how print is supposed to work yet, so just
 * print out an error message.
 */
function AslPrintWindow()
{
  if(top.IS_IE)
  {
   alert();
   return;
  }

  // Nav
//  var content=top.findFrame(top, frameName);
//  if(content && content.frames.length==0)
//  {
    top.frames[5].print();
    return;
//  }
  alert("The current page cannot be printed.");
}



/* loadHelp
 * --------
 * this does not load anything yet...
 */
function AslLoadHelp()
{

  alert("no help available...");

}




/*------------------------- Control Region Events ------------------------*/

function toPosInt(str)
{
  var res = 1;
  var num = parseInt(str);

  if ((num ==0) && (str.length == 1))
    return num;

  for (var i=1; i<str.length; i++)
    res = 10 * res;

  if (isNaN(num) || (num < res))
   return -1;

  return num;
}

/* AslSubmitClicked
 * -------------
 */
function AslSubmitClicked(p_frame)
{  
   var msg1 = window.top.FND_MESSAGES["ICX_POS_VALID_NUMBER"];
   var msg2 = window.top.FND_MESSAGES["ICX_POS_ACK_SUBMIT"];
   var msg3 = window.top.FND_MESSAGES["ICX_POS_CAP_SUBMIT_CONFIRM"];

  if (p_frame.result.document.POS_ASL_RESULT.POS_ROWS.value == "1") {
    if (confirm(msg2)) {
      if (p_frame.result.document.POS_ASL_RESULT.POS_ASL_PROCESSING_LEAD_TI.value != "") { 
      var v = toPosInt(p_frame.result.document.POS_ASL_RESULT.POS_ASL_PROCESSING_LEAD_TI.value);
      if (v == -1) {
        alert (msg1);
        p_frame.result.document.POS_ASL_RESULT.POS_ERROR_ROW.value = "1";
        p_frame.result.document.POS_ASL_RESULT.submit(); 
        return;
      }
      }
      if (p_frame.result.document.POS_ASL_RESULT.POS_ASL_MIN_ORDER_QTY.value != "") {
      v = toPosInt(p_frame.result.document.POS_ASL_RESULT.POS_ASL_MIN_ORDER_QTY.value);
      if (v == -1) {
        alert (msg1);
        p_frame.result.document.POS_ASL_RESULT.POS_ERROR_ROW.value = "2";
        p_frame.result.document.POS_ASL_RESULT.submit(); 
        return;
      }
      }
      if (p_frame.result.document.POS_ASL_RESULT.POS_ASL_FIXED_LOT_MULTIPLE.value != "") {
      v = toPosInt(p_frame.result.document.POS_ASL_RESULT.POS_ASL_FIXED_LOT_MULTIPLE.value);
      if (v == -1) {
        alert (msg1);
        p_frame.result.document.POS_ASL_RESULT.POS_ERROR_ROW.value = "3";
        p_frame.result.document.POS_ASL_RESULT.submit(); 
        return;
      }
      }

      if (p_frame.capacity.check_before_finish() && 
          p_frame.tolerance.check_before_finish()) {
        p_frame.capacity.check();
      }
    }
  }
}


/*------------------------- Other Button Events ------------------------*/

/* discard
 * -------
 * This event is triggered in the 'Cancel' window when the user clicks on
 * the discard button.  It closes all windows without saving.
 */
function AslDiscard()
{
   var scriptName = top.getTop().scriptName;
   var url = scriptName + "/pos_asl_master_pkg.discard";
   self.location.href = url;
   top.getTop().recursiveClose();
}


/* continue
 * --------
 * This event is triggered in the 'Cancel' window when the user clicks on
 * the continue button.  It closes the modal window so the user can
 * continue working.
 */
function AslContinueWork(p_win)
{
  p_win.close();
}

function AslCreateEntry(p_win)
{
  p_win.close();
  self.content.criteria.document.POS_ASL_SEARCH.POS_ASN_SR_SUPPLIER_SITE.value="";
  self.content.criteria.document.POS_ASL_SEARCH.POS_ASL_ITEM_NUM.value="";
  self.content.criteria.document.POS_ASL_SEARCH.POS_ASL_ITEM_DESC.value="";
  self.content.result.document.POS_ASL_RESULT.POS_ROWS.value = "-1";
  self.content.result.document.POS_ASL_RESULT.submit();
  self.content.capacity.clear();
  self.content.tolerance.clear();
}


function SearchPOs()
{
  document.POS_ACK_SEARCH.submit();
}

function AslSearchConstraints()
{
 document.POS_ASL_SEARCH.submit();
}


function AslClearFields()
{
  var msg = window.top.FND_MESSAGES["ICX_POS_CLEAR_FIELDS"]; 

  if (parent.result.document.POS_ASL_RESULT.POS_ROWS.value == "1") {
    if (confirm(msg)) {
      document.POS_ASL_SEARCH.POS_ASN_SR_SUPPLIER_SITE.value="";
      document.POS_ASL_SEARCH.POS_ASN_SR_SUPPLIER_SITE_ID.value="";
      document.POS_ASL_SEARCH.POS_ASL_ITEM_NUM.value="";
      document.POS_ASL_SEARCH.POS_ASL_ITEM_DESC.value="";
      parent.result.document.POS_ASL_RESULT.POS_ROWS.value = "-1";
      parent.result.document.POS_ASL_RESULT.submit();
      parent.capacity.clear();
      parent.tolerance.clear();
   }
 } else {
   document.POS_ASL_SEARCH.POS_ASN_SR_SUPPLIER_SITE.value="";
   document.POS_ASL_SEARCH.POS_ASN_SR_SUPPLIER_SITE_ID.value="";
   document.POS_ASL_SEARCH.POS_ASL_ITEM_NUM.value="";
   document.POS_ASL_SEARCH.POS_ASL_ITEM_DESC.value="";
 }
}


function AslSubmitAcks()
{
  document.POS_EDIT_POS.submit();
}


function AsLLoadResult(p_msg, p_mode)
{
  if (p_msg != "")
    alert(p_msg);
  
  if (p_mode == "NORMAL" && document.POS_ASL_RESULT.POS_ASL_ID.value != "") {
      document.POS_ASL_CAPACITY.POS_ASL_ID.value = 
           document.POS_ASL_RESULT.POS_ASL_ID.value;
      document.POS_ASL_CAPACITY.submit();

      document.POS_ASL_TOLERANCE.POS_ASL_ID.value = 
           document.POS_ASL_RESULT.POS_ASL_ID.value;
      document.POS_ASL_TOLERANCE.submit();
  } 
  
  if (p_mode != "START"  && document.POS_ASL_RESULT.POS_ASL_ID.value == "") {
      parent.capacity.clear();
      parent.tolerance.clear();
   }
}







