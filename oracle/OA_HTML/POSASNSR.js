/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================+
| FILENAME                                                         |
|   POSASNSR.js                                                    |
|                                                                  |
| HISTORY                                                          |
|   05-AUG-99       dkfchan       Created                          |
|                                                                  | 
+==================================================================*/
/* $Header: POSASNSR.js 115.0 99/10/14 16:19:35 porting shi $ */

function searchShipments()
{
  document.POS_ASN_SEARCH_R.submit();
}

function setCriteria(p_empty_cart, p_advance_flag)
{
  var s_form = document.POS_ASN_SEARCH_R;

  top.is_cart_empty = p_empty_cart;

  top.advance_flag = p_advance_flag;

  if (top.is_cart_empty == 'Y')
  {

    if (top.sr_supplier_site_id != null)
      s_form.POS_VENDOR_SITE_ID.value = top.sr_supplier_site_id;

    if (top.sr_supplier_site != null)
      s_form.POS_VENDOR_SITE_NAME.value = top.sr_supplier_site;

    if (top.sr_ship_to_loc_id != null)
      s_form.POS_SHIP_TO_LOCATION_ID.value = top.sr_ship_to_loc_id;

    if (top.sr_ship_to_location != null)
      s_form.POS_SHIP_TO_LOCATION.value = top.sr_ship_to_location;

  }

  if (top.sr_supplier_item_num != null && s_form.POS_SUPPLIER_ITEM_NUMBER != null)
    s_form.POS_SUPPLIER_ITEM_NUMBER.value = top.sr_supplier_item_num;

  if (top.sr_item_description != null && s_form.POS_ITEM_DESCRIPTION != null)
    s_form.POS_ITEM_DESCRIPTION.value = top.sr_item_description;

  if (top.sr_po_number != null && s_form.POS_PO_NUMBER != null)
    s_form.POS_PO_NUMBER.value = top.sr_po_number;

  if (top.sr_item_number != null && s_form.POS_ITEM_NUMBER != null)
    s_form.POS_ITEM_NUMBER.value = top.sr_item_number;

  if (top.sr_date_start != null && s_form.POS_DATE_START != null)
    s_form.POS_DATE_START.value = top.sr_date_start;

  if (top.sr_date_end != null && s_form.POS_DATE_END != null)
    s_form.POS_DATE_END.value = top.sr_date_end;

}

function syncTopCriteria()
{
  var s_form = document.POS_ASN_SEARCH_R;

  top.sr_supplier_site_id = s_form.POS_VENDOR_SITE_ID.value;
  top.sr_supplier_site = s_form.POS_VENDOR_SITE_NAME.value;
  top.sr_ship_to_loc_id = s_form.POS_SHIP_TO_LOCATION_ID.value;
  top.sr_ship_to_location = s_form.POS_SHIP_TO_LOCATION.value;

  if (s_form.POS_SUPPLIER_ITEM_NUMBER != null)
    top.sr_supplier_item_num = s_form.POS_SUPPLIER_ITEM_NUMBER.value;

  if (s_form.POS_ITEM_DESCRIPTION != null)
    top.sr_item_description = s_form.POS_ITEM_DESCRIPTION.value;

  if (s_form.POS_PO_NUMBER != null)
    top.sr_po_number = s_form.POS_PO_NUMBER.value;

  if (s_form.POS_ITEM_NUMBER != null)
    top.sr_item_number = s_form.POS_ITEM_NUMBER.value;

  if (s_form.POS_DATE_START != null)
    top.sr_date_start = s_form.POS_DATE_START.value;

  if (s_form.POS_DATE_END != null)
    top.sr_date_end = s_form.POS_DATE_END.value;

}

function clearFields()
{
  var s_form = document.POS_ASN_SEARCH_R;

  if (top.is_cart_empty == 'Y')
  {
    s_form.POS_VENDOR_SITE_ID.value = "";
    s_form.POS_VENDOR_SITE_NAME.value = "";
    s_form.POS_SHIP_TO_LOCATION_ID.value = "";
    s_form.POS_SHIP_TO_LOCATION.value = "";
  }

  if (s_form.POS_SUPPLIER_ITEM_NUMBER != null)
    s_form.POS_SUPPLIER_ITEM_NUMBER.value = "";

  if (s_form.POS_ITEM_DESCRIPTION != null)
    s_form.POS_ITEM_DESCRIPTION.value = "";

  if (s_form.POS_PO_NUMBER != null)
    s_form.POS_PO_NUMBER.value = "";

  if (s_form.POS_ITEM_NUMBER != null)
    s_form.POS_ITEM_NUMBER.value = "";

  if (s_form.POS_DATE_START != null)
    s_form.POS_DATE_START.value = "";

  if (s_form.POS_DATE_END != null)
    s_form.POS_DATE_END.value = "";

  syncTopCriteria();
}

function SwitchSearch()
{
  var s_form = document.POS_ASN_SEARCH_R;
  var s_frame = parent.criteria;
  var l_advance_flag;

  var l_supplier_item_number = "";
  var l_item_description = "";
  var l_po_number = "";
  var l_item_number = "";
  var l_date_start = "";
  var l_date_end = "";

  if (top.advance_flag == 'Y')
    l_advance_flag = 'N';
  else
    l_advance_flag = 'Y';

  if (s_form.POS_SUPPLIER_ITEM_NUMBER != null)
    l_supplier_item_number = s_form.POS_SUPPLIER_ITEM_NUMBER.value;

  if (s_form.POS_ITEM_DESCRIPTION != null)
    l_item_description = s_form.POS_ITEM_DESCRIPTION.value;

  if (s_form.POS_PO_NUMBER != null)
    l_po_number = s_form.POS_PO_NUMBER.value;

  if (s_form.POS_ITEM_NUMBER != null)
    l_item_number = s_form.POS_ITEM_NUMBER.value;

  if (s_form.POS_DATE_START != null)
    l_date_start = s_form.POS_DATE_START.value;

  if (s_form.POS_DATE_END != null)
    l_date_end = s_form.POS_DATE_END.value;

  var l_URL = top.scriptName + "/POS_ASN_SEARCH_PKG.CRITERIA_FRAME?" + 
/*
              "pos_vendor_site_id=" + escape(s_form.POS_VENDOR_SITE_ID.value, 1) +
              "&pos_vendor_site_name=" + escape(s_form.POS_VENDOR_SITE_NAME.value, 1) +
              "&pos_ship_to_location_id=" + escape(s_form.POS_SHIP_TO_LOCATION_ID.value, 1) +
              "&pos_ship_to_location=" + escape(s_form.POS_SHIP_TO_LOCATION.value, 1) +
              "&pos_supplier_item_number=" + escape(l_supplier_item_number, 1) +
              "&pos_item_description=" + escape(l_item_description, 1) +
              "&pos_po_number=" + escape(l_po_number, 1) +
              "&pos_item_number=" + escape(l_item_number, 1) +
              "&pos_date_start=" + escape(l_date_start, 1) +
              "&pos_date_end=" + escape(l_date_end, 1) +
*/
              "p_advance_flag=" + escape(l_advance_flag, 1);

  syncTopCriteria();

  s_frame.location.href = l_URL;
}

function checkBlur(attr)
{
  if (top.is_cart_empty == "N")
    attr.blur();
}

function reset_hidden(c_attribute_code)
{
  
  var f = document.POS_ASN_SEARCH_R;

  if (c_attribute_code == "POS_VENDOR_SITE_NAME")
  {
    f.POS_VENDOR_SITE_ID.value = "";
  }
  else if (c_attribute_code == "POS_SHIP_TO_LOCATION")
  {
    f.POS_SHIP_TO_LOCATION_ID.value = "";
  }

}

function SwitchPage(p_value)
{

  var r_form = parent.result.document.POS_ASN_RESULT_R;
  var r_frame = parent.result;

  var l_start_row = (r_form.pos_start_row.value * 1) + (p_value * 1);

  if (l_start_row < 1) 
    l_start_row = 1;  

  var l_URL = top.scriptName + "/POS_ASN_SEARCH_PKG.SWITCHRESULTPAGE?p_start_row=" + l_start_row;

  r_frame.location.href = l_URL;

}

function check_all()
{

  if (document.RESULT_INFO.select_all.value == "Y")
  {
    if (document.POS_ASN_RESULT_R.POS_SELECT[0] == null)
      document.POS_ASN_RESULT_R.POS_SELECT.checked = true;
    else 
      { 
        for (var i=0; i < document.POS_ASN_RESULT_R.POS_SELECT.length; i++)
           document.POS_ASN_RESULT_R.POS_SELECT[i].checked = true;
      }

    document.RESULT_INFO.select_all.value = "N";
  }
  else
  {
    if (document.POS_ASN_RESULT_R.POS_SELECT[0] == null)
      document.POS_ASN_RESULT_R.POS_SELECT.checked = false;
    else 
      { 
        for (var i=0; i < document.POS_ASN_RESULT_R.POS_SELECT.length; i++)
           document.POS_ASN_RESULT_R.POS_SELECT[i].checked = false;
      }

    document.RESULT_INFO.select_all.value = "Y";
  }
}

function LoadResult(p_msg, p_empty_cart)
{
  var r_form = document.RESULT_INFO;
  var c_frame = parent.counter;
  var a_frame = parent.add;
  
  top.is_cart_empty =  p_empty_cart;

  var l_URL = top.scriptName + "/POS_ASN_SEARCH_PKG.COUNTER_FRAME?" + 
              "p_first=" + r_form.p_start.value +
              "&p_last=" + r_form.p_stop.value +
              "&p_total=" + r_form.p_total.value +
              "&p_msg=" + escape(p_msg, 1);

  c_frame.location.href = l_URL;

  a_frame.location.href = top.scriptName + "/POS_ASN_SEARCH_PKG.ADD_FRAME"

}

function call_LOV(c_attribute_code)
{
  var c_where_clause = "";

  if (top.is_cart_empty == "Y")
  {
    LOV("178", c_attribute_code, "178", "POS_ASN_SEARCH_R", "POS_ASN_SEARCH_R", 
        "criteria", "", c_where_clause);
  }
  else
      alert(window.top.FND_MESSAGES["ICX_POS_ASN_FIXED_FIELD"]);
}
