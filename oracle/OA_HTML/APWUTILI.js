//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWUTILI.js                       |
//  |                                                |
//  | Description: Client-side Javascript functions  |
//  |              and variables		     |     
//  |                                                |
//  | History:     Created 10/07/1999                |
//  |              David Tong                        |
//  |             Priyai
//  |             1. Removed the final review action from the function fsaveorsubmit.   
//  |             2. Added the final review action in the function fvalidateReport.
//  |             3. Renamed the function fValidateReport to fFinalReview
//		     and subsequently changed the name where ever it is called.
         
//  |                                                |
//  +================================================+
/* $Header: APWUTILI.js 115.166 2001/02/19 18:11:49 pkm ship   $ */
/* ------------------------
    Global Variables
   ------------------------ */
   var g_firstOpenDetail = 0;
   var g_reportSubmitted = 0;
   var g_winCal = new Object; 
   var g_winDetail = new Object;
   var g_winCurrency = new Object;
   var g_winItemize = new Object;
   var g_winError = new Object;
   var g_bHeaderFrameLoaded = false;
   var g_bButtonsFrameLoaded = false; 
   var g_bDetailFrameLoaded = false;
   var g_bFlexFrameLoaded = false;
   var g_bEuroCodeDefined = false;
   var g_bTaxEnable = false;
   var g_layDoc = "";
   var g_laySty = "";
   var g_layHtm = "";
   var g_arrCurrency = new fMakeArray(10);
   var g_arrReimbCurrency = new fMakeArray(10);
   var g_strGlobalContext = "";
   var g_arrExpenseTemplate = new Array();
   var g_strPreviousExpType = ""; // Keep track of previous Expense Type
   var g_dToday = new Date();
   var g_strDay   = g_dToday.getDate();
   var g_strMonth = g_dToday.getMonth();
   var g_strYear  = g_dToday.getFullYear();
   var g_strErrorContent = "";
   var g_arrError = new Array;
   var g_strCurrentTab = 'OOP';
   var g_strVMode = null;
   var g_arrCountry = new Array();
   var g_closeWin = false;
  
   var g_reimbEqualsRec = 0;
   var g_reimbEurRecFixed = 1;
   var g_reimbFixedRecEur = 2;
   var g_reimbFixedRecFixed = 3;

   var bVersion = new fBrowserVersion(); // remove later
   
     
   g_winCal = null;
   g_winDetail = null;
   g_winCurrency = null;
   g_winItemize = null;
   g_winError = null;


/* --------------------------
   Generic Functions
   -------------------------- */

var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4));
if (Nav4) {
  top.g_layDoc = "document";
  top.g_laySty = "";
  top.g_layHtm = ".document";
}
else{
  top.g_layDoc = "document.all";
  top.g_laySty = ".style";
  top.g_layHtm = "";
}

var isIE5 = ((navigator.appName == "Microsoft Internet Explorer") && (navigator.appVersion.indexOf("MSIE 5.0")!=-1));

var isWindows95 = (navigator.appVersion.indexOf("Windows 95") != -1);

var SunOS = (navigator.platform.indexOf("SunOS") != -1);

function closeAll()
{
   if (g_winCal != null)
       if (!g_winCal.closed) g_winCal.close();

   if (g_winDetail != null)
       if (!g_winDetail.closed) g_winDetail.close();

   if (g_winCurrency != null)
       if (!g_winCurrency.closed) g_winCurrency.close();
  
   if (g_winItemize != null)
       if (!g_winItemize.closed) g_winItemize.close();

   if (g_winError != null)
       if (!g_winError.closed) g_winError.close();

   if (g_winCCardTrxn != null)
       if (!g_winCCardTrxn.closed) g_winCCardTrxn.close();

}

/* 
Description: The user can enter Cost Center # as the prompt for a descriptive
flex field since the code will convert the widget name into 
Cost_Center.  However, in situations where two descriptive flexfields 
with prompts such as Cost Center # and Cost Center exist, 
the widget names for these two different flex fields are going 
to be identical.  Therefore, customers should avoid having prompts 
like the above. 
*/
function ReplaceInvalidChars(strPrompt) {
  var strWidgetName;
  if (strPrompt) {
     	 strWidgetName = strPrompt.replace(/ /g,"_");
	 strWidgetName = strWidgetName.replace(/\//g,"_");
	 strWidgetName = strWidgetName.replace("{","");
	 strWidgetName = strWidgetName.replace("}","");
	 strWidgetName = strWidgetName.replace(/'/g,"");
	 strWidgetName = strWidgetName.replace(/-/g,"_");
	 strWidgetName = strWidgetName.replace(/=/g,"");
	 strWidgetName = strWidgetName.replace(/\+/g,"_");
	 strWidgetName = strWidgetName.replace(/\*/g,"");
	 strWidgetName = strWidgetName.replace(/\?/g,"");
	 strWidgetName = strWidgetName.replace(/\</g,"_");
	 strWidgetName = strWidgetName.replace(/\>/g,"_");
	 strWidgetName = strWidgetName.replace(/\[/g,"_");
	 strWidgetName = strWidgetName.replace(/\]/g,"_");
	 strWidgetName = strWidgetName.replace(/\!/g,"");
	 strWidgetName = strWidgetName.replace(/\,/g,"");
	 strWidgetName = strWidgetName.replace(/\./g,"");
	 strWidgetName = strWidgetName.replace(/\#/g,"");
	 strWidgetName = strWidgetName.replace(/\&/g,"");
	 strWidgetName = strWidgetName.replace(/\@/g,"");
	 strWidgetName = strWidgetName.replace(/\$/g,"");
	 strWidgetName = strWidgetName.replace(/\%/g,"");
	 strWidgetName = strWidgetName.replace(/\(/g,"");
	 strWidgetName = strWidgetName.replace(/\)/g,"");
	 strWidgetName = strWidgetName.replace(/\:/g,"");
  }
  return strWidgetName;
}

function ReplaceSpaceWithPlus(str) {
           var newstr = "";
           var c = "";
           for(var i=0;i<str.length;i++) {
             c=str.charAt(i);

             if(c == " ") 
               newstr = newstr + "+";
             else if(c == "&")
               newstr = newstr + "%26";
             else
               newstr = newstr + str.charAt(i);
           }
           return newstr;
         }

function fGetReceiptObj(iRecIndex){
  if (iRecIndex < 0) return null;
  if (fIsOOP()) 
    return fGetObjExpReport().oopReceipts[iRecIndex];
  else
    return fGetObjExpReport().cCardReceipts[iRecIndex];
}

function fGenTabs(strTab){
if (framMain.tabs){
var d = framMain.tabs.document;
var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_EXP_TABS');
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
fGenerateStyleCodes(top.framMain.tabs);
d.writeln("<body onFocus = \"top.fCheckModal()\" class=color3 onResize = \"history.go(0);\" link=blue vlink=blue alink=#ff0000>");

if (strTab == 'CC'){
d.writeln("	<Table width=100% cellpadding=0 cellspacing=0 border=0>");
d.writeln("	  <tr class=color3>");
d.writeln("		<td rowspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=1 width=1></td>");
d.writeln("		<td rowspan=3 nowrap><font class=containertitle>Enter Expenses</font></td>");
d.writeln("		<td rowspan=3 width=10000></td>");
d.writeln("		<td colspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=6></td>");
d.writeln("		<td colspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=6></td>");
d.writeln("		<td rowspan=3><img src="+top.g_strImgDir+"APWPX3.gif></td>");
d.writeln("	  </tr>");
d.writeln("	  <tr class=color3>");
d.writeln("		<td rowspan=2><img src="+top.g_strImgDir+"APWIPTL.gif></td>");
d.writeln("		<td class=color4 height=1><img src="+top.g_strImgDir+"APWPX4.gif></td>");
d.writeln("		<td rowspan=2><img src="+top.g_strImgDir+"APWIPTR.gif></td>");
d.writeln("		<td rowspan=2><img src="+top.g_strImgDir+"APWGTL.gif></td>");
d.writeln("		<td class=color6 height=1><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("		<td rowspan=2><img src="+top.g_strImgDir+"APWGTR.gif></td>");
d.writeln("	  </tr>");
d.writeln("	  <tr>");
d.writeln("		<td class=color4 nowrap><a href=\"javascript:top.fSwitchTabs('OOP');\"><FONT CLASS=othertabtext>"+tempRegion.mobjGetRegionItem('AP_WEB_OOP').mstrGetPrompt()+"</font></a></td>");
d.writeln("		<td class=colorg5 nowrap><FONT CLASS=currenttabtext>"+tempRegion.mobjGetRegionItem('AP_WEB_CC').mstrGetPrompt()+"</font></td>");
d.writeln("	  </tr>");
d.writeln("	  <tr>");
d.writeln("		<td rowspan=2 class=colorg5 valign=top><img src="+top.g_strImgDir+"APWCTTL.gif></td>");
d.writeln("		<td colspan=2 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("		<td colspan=3 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("		<td colspan=3 class=colorg5><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("		<td rowspan=2 class=colorg5 valign=top width=3><img src="+top.g_strImgDir+"APWCTTR.gif></td>");
d.writeln("	  </tr>");
d.writeln("	  <tr class=colorg5>");
d.writeln("		<td colspan=8 height=200 valign=top>");
d.writeln("		<FONT CLASS=helptext>"+top.g_objMessages.mstrGetMesg('AP_WEB_CCARD_HEADER')+"</td>");
d.writeln("	  </tr>");
d.writeln("	</table>");
}
else{
d.writeln("<Table width=100% cellpadding=0 cellspacing=0 border=0>");
d.writeln("<tr class=color3>");
d.writeln("<td rowspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=1 width=1></td>");
d.writeln("<td rowspan=3 nowrap><font class=containertitle>"+tempRegion.mobjGetRegionItem('AP_WEB_ENTREXP').mstrGetPrompt()+"</font></td>");
d.writeln("<td rowspan=3 width=10000></td>");
d.writeln("<td colspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=6></td>");
d.writeln("<td colspan=3><img src="+top.g_strImgDir+"APWPX3.gif height=6></td>");
d.writeln("<td rowspan=3><img src="+top.g_strImgDir+"APWPX3.gif></td>");
d.writeln("</tr>");
d.writeln("<tr class=color3>");
d.writeln("<td rowspan=2><img src="+top.g_strImgDir+"APWGTL.gif></td>");
d.writeln("<td class=color6 height=1><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("<td rowspan=2><img src="+top.g_strImgDir+"APWGTR.gif></td>");
d.writeln("<td rowspan=2><img src="+top.g_strImgDir+"APWIPTL.gif></td>");
d.writeln("<td class=color4 height=1><img src="+top.g_strImgDir+"APWPX4.gif></td>");
d.writeln("<td rowspan=2><img src="+top.g_strImgDir+"APWIPTR.gif></td>");
d.writeln("</tr>");
d.writeln("<tr>");
d.writeln("<td class=colorg5 nowrap><FONT CLASS=currenttabtext>"+tempRegion.mobjGetRegionItem('AP_WEB_OOP').mstrGetPrompt()+"</font></td>");
d.writeln("<td class=color4 nowrap><a href=\"javascript:top.fSwitchTabs('CC');\"><FONT CLASS=othertabtext>"+tempRegion.mobjGetRegionItem('AP_WEB_CC').mstrGetPrompt()+"</font></a></td>");
d.writeln("</tr>");
d.writeln("<tr>");
d.writeln("<td rowspan=2 class=colorg5 valign=top><img src="+top.g_strImgDir+"APWCTTL.gif></td>");
d.writeln("<td colspan=2 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("<td colspan=3 class=colorg5><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("<td colspan=3 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
d.writeln("<td rowspan=2 class=colorg5 valign=top width=3><img src="+top.g_strImgDir+"APWCTTR.gif></td>");
d.writeln("</tr>");
d.writeln("<tr class=colorg5>");
d.writeln("<td colspan=8 height=200 valign=top>");
d.writeln("<FONT CLASS=helptext>"+top.g_objMessages.mstrGetMesg('AP_WEB_OOP_HEADER')+"</td>");
d.writeln("</tr>");
d.writeln("</table>");
}

d.writeln("</body>");
d.writeln("</html>");
d.close();
}
}

// this function to check if a charge should be available in the Import Window
function fTrxnIsAvailable(id)
{
    var r = objExpenseReport.cCardReceipts;
    for (var i=0; i<r.length ; i++)
       if (r[i] != null && r[i].cCardTrxnId == id) return false;

    return true;
}

function fGetAvailableTransactions(cardProgName)
{
     var trxns = g_objCCardData.getTrxns(cardProgName); 
     var availTrx = new creditCardTransactions();
     trxns.reset();
     var t = trxns.nextTrxn();
     while (t != null)
     {
           if ((t.getCategory() != g_cDispute) && (t.getCategory() != g_cCredit) && 
              (t.getCategory() != g_cClear) && fTrxnIsAvailable(t.getId()))
          {
              availTrx.add(t);
          }
          t = trxns.nextTrxn();
     }
     return availTrx;
}

// if there is not parameter passed then it used selected card program
function fGetPersonalTransactions(cardProgName)
{
     var temp = g_selectedCardProg;
     if (arguments.length == 1) temp = cardProgName;
    
     var trxns = g_objCCardData.getTrxns(temp); 
     var availTrx = new creditCardTransactions();
     if (trxns != null) {
         trxns.reset();
         var t = trxns.nextTrxn();
         while (t != null) {
            if (t.getCategory() == g_cPersonal) availTrx.add(t);
            t = trxns.nextTrxn();
         }
     }
     return availTrx;
}

// Note that this function returns an array of disputedCharges object
function fGetDisputedCharges(cardProgName)
{
     if (g_objCCardData.getTrxns(cardProgName) == null) 
         return null;
     else
         return (g_objCCardData.getTrxns(cardProgName)).fGetDisputedCharges();
}

function fChecked(b) {if (b) return "checked"; else return "";}

// Search for the index given the text
function fGetSelectedIndex(objOptions, text)
{  for (i=0; i<objOptions.length; i++)
        if (objOptions[i].text == text) return i;
   return -1;
}

function fGenCountryPoplist(selectedValue)
{
  var strResult = "<option value=''";
  if (selectedValue == null || selectedValue == "") strResult += " SELECTED";
  strResult += "> ";
  for (var i=0; i < g_arrCountry.length;i++){
     strResult += "<option value='"+g_arrCountry[i].getCode() + "'";
     if (g_arrCountry[i].getCode() == selectedValue) strResult += " SELECTED";
       strResult += ">" + g_arrCountry[i].getName();
  }
  return strResult;
}

function fGenPopTaxCode(selectedText)
{
  var strResult = "<option value=''";
  if (selectedText == null || selectedText == "") strResult += " SELECTED";
  strResult += "> ";
  for (var i=0;i<top.MyTaxes.arr.length;i++){
     strResult += "<OPTION value = '" + top.MyTaxes.arr[i].taxCode + "'";
     if (top.MyTaxes.arr[i].taxCode == selectedText) strResult += " SELECTED";
      strResult += ">" + top.MyTaxes.arr[i].taxCode;
  }
  return strResult;
}


function fIsOOP(){
	return (top.g_strCurrentTab == 'OOP'?true:false);
}

 function fBrowserVersion() {
        var agent = navigator.userAgent.toLowerCase();
        this.major = parseInt(navigator.appVersion);
        this.minor = parseFloat(navigator.appVersion);
         this.ns  = ((agent.indexOf('mozilla')!=-1) && ((agent.indexOf('spoofer')==-1) && (agent.indexOf('compatible') == -1)));
        this.ns2 = (this.ns && (this.major == 2));
        this.ns3 = (this.ns && (this.major == 3));
        this.ns4b = (this.ns && (this.minor < 4.04));
        this.ns4 = (this.ns && (this.major >= 4));
        this.ie   = (agent.indexOf("msie") != -1);
        this.ie3  = (this.ie && (this.major == 2));
        this.ie4  = (this.ie && (this.major >= 4));
        this.win   = (agent.indexOf("win")!=-1);
        this.mac   = (agent.indexOf("mac")!=-1);
        this.unix  = (agent.indexOf("x11")!=-1);
}

function fGenerateStyleCodes(targetFrame){
        var d = targetFrame.document;
	d.writeln("<LINK REL=stylesheet TYPE=\"text/css\" href=\""+top.g_strCssDir+"apwscabo.css\">");

} // end of fGenerateStyleCodes()

function fMakeArray(n)
{
  this.len = n;
  for (var i=1; i<=n; i++)
    this[i] = 0;
}

function fGetAmtDueValues() { 
	 var ccBusinessTotal = fGetFinalReviewSubtotal(g_cBusiness);
	 var oopTotal = fGetFinalReviewSubtotal('oop');
	 var ccPersonalTotal = fGetFinalReviewSubtotal(g_cPersonal);
	 if (fGetPaymentScenario() == g_cIndividual || fGetPaymentScenario() == null) {
		// dtong for bug #1337490
		fGetObjHeader().setAmtDueEmployee(eval(oopTotal) + eval(ccBusinessTotal));
		fGetObjHeader().setAmtDueccCompany(0);

	 } else if (fGetPaymentScenario() == g_cBoth) {
		fGetObjHeader().setAmtDueEmployee(oopTotal);
		fGetObjHeader().setAmtDueccCompany(ccBusinessTotal);
		
	 } else if (fGetPaymentScenario() == g_cCompany) {
		fGetObjHeader().setAmtDueEmployee(eval(oopTotal) - eval(ccPersonalTotal));
		fGetObjHeader().setAmtDueccCompany(ccBusinessTotal + eval(ccPersonalTotal));
	 } else {
		fGetObjHeader().setAmtDueEmployee(0);
		fGetObjHeader().setAmtDueccCompany(0);
	 }
}

function fSetReqImg() {
  top.framMain.lines.document.reqimg.src = top.reqimg1.src;
} //end function fSetReqImg


/* --------------------------
   Functions for Synchronization
   -------------------------- */
function fWaitUntilFrameLoaded(strFrameLoaded,strExpr){
           if (!eval(strFrameLoaded))
	       setTimeout("top.fWaitUntilFrameLoaded('"+strFrameLoaded+"','"+strExpr+"')",1000);
	     else eval(strExpr);
}




// --------------------------
// Functions for tax
// --------------------------

function fPopulateTaxValues(iRecIndex){
	var r = fGetReceiptObj(iRecIndex);
	var popTaxCode = fGetObjDetail().framReceiptDetail.document.formDetail.popTaxCode;
	popTaxCode.selectedIndex = 0;
	fGetObjDetail().fDetailSetAmtIncTax(false);

	var popExpType =  eval('top.framMain.lines.document.data.popExpType'+ top.framMain.lines.g_iCurrentLine);
	var objE = top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(popExpType.options[popExpType.selectedIndex].value);

	if (objE.strDefaultTaxCode != "" && r && r.getTaxCode() == "")
		r.setTaxCode(objE.strDefaultTaxCode);

	if (r){
        for (i=0;i<popTaxCode.options.length;i++){
	   if (popTaxCode.options[i].value == r.getTaxCode())
	      popTaxCode.selectedIndex = i;
	}
		
        fGetObjDetail().fDetailSetAmtIncTax(r.getAmountIncludesTax());
	if (g_bEnableNewTaxFields == 'Y'){
	
	var popSupplyCountry = fGetObjDetail().framReceiptDetail.document.formDetail.popSupplyCountry;
	popSupplyCountry.selectedIndex = 0;
        for (i=0;i<popSupplyCountry.options.length;i++){
	   if (popSupplyCountry.options[i].value == r.getSupCountry())
	   popSupplyCountry.selectedIndex = i;
	}
	fGetObjDetail().fDetailSetDocNum(r.getDocNum())
	fGetObjDetail().fDetailSetTaxRegNum(r.getTaxRegNum())
	fGetObjDetail().fDetailSetReference(r.getMerchRef())
	fGetObjDetail().fDetailSetMerchantTaxId(r.getTaxPayerID())
	if (r.getMerchant() && top.fIsOOP()) fGetObjDetail().fDetailSetMerchName(r.getMerchant())
	}
	
	}//end if (r)
    else {
	if (g_bEnableNewTaxFields == 'Y'){
	
	t.popSupplyCountry.selectedIndex = 0;
	t.txtDocNum.value = "";
	t.txtTaxRegNum.value = "";
	t.txtTaxPayerID.value = "";
	t.txtMerchRef.value = "";
	t.txtMerchName.value = "";
	}
    } //end else
}//end function fPopulateTaxValues


// --------------------------
// Functions for validation
// --------------------------


function fFinalReview(){

    if (!Nav4) fIEOnChangeTrigger();
    top.g_reportSubmitted = 0;
    var t = top.framSaveReport.document.formServerSerialize;
    
if ((fGetObjExpReport().getTotalReceipts("OOP") > 0) || (fGetObjExpReport().getTotalReceipts("CC") > 0)) {

    // Store flex field values in receipt object to make sure we have latest data
    
    if (fIsOOP()){
	var iRecIndex = fGetObjExpReport().getNthReceipt(top.framMain.lines.g_iCurrentLine);	
	if (iRecIndex>=0)
	fGetReceiptObj(iRecIndex).setFlexValues();
    }
    else{ 
	//top.fGetObjExpReport().cCardReceipts[iRecIndex].setFlexValues();
    }	    
    
     var orig_tab = g_strCurrentTab;
     var strErrorContent = (fRequiredFieldsEntered(top.g_objMessages.mstrGetMesg("AP_WEB_ERROR_HEADER"),true,-1));
     g_strCurrentTab = orig_tab;
         
if (fDrawErrorWindow(top.g_objMessages.mstrGetMesg("AP_WEB_ERROR_HEADER"),strErrorContent))
    { 
   	  fOpenErrorWindow(top.g_objMessages.mstrGetMesg("AP_WEB_WAIT_VALIDATION"),'');
          framSaveReport.fSetParseString(-1,eval(t.ParseThis));
	  t.submit(); 
    }
    else {
	top.g_winError.focus();
	fGenSaveFrame(false); // not for confirmation page

	}
  }
  else {
      alert(top.g_objMessages.mstrGetMesg('AP_WEB_ONE_RECEIPT_REQUIRED'));
    }
}


function fValidateCoreFields(){
  return true;
}


function fCheckNum(inputValue, nozero,showalert){
  var str = inputValue + "";
  var msg = str + top.g_objMessages.mstrGetMesg("AP_WEB_NUMBER_REQUIRED");;

  if ((inputValue == 0) && (nozero)) {
    return false;
  }

  if (top.fIsNum(str, showalert)){

    return true;
  } 
  else {
    return false;
  }
} 


function fIsNum(str, showalert){
  if (str){
   var ch=str.substring(0,1);
 
  if((ch<"0" || "9"<ch) && (ch != ".") && (ch != "-")) {
   if (showalert)
    alert(top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));
    return false;
  }
  for(var i=1; i<str.length; i++){
    var ch=str.substring(i,i+1)
    if((ch<"0" || "9"<ch) && ch != "."){
     if (showalert)
      alert(top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));
      return false;
    }
  }
  // xx.xx.xx and xx. case
  if ((str.indexOf(".") != str.lastIndexOf(".")) || (str.lastIndexOf(".") == (str.length - 1))) {
   if (showalert)
    alert(top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));
    return false;
  }
  // -. case
  if (str == "-.") {
   if (showalert)
    alert(top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));
    return false;
  }
  // 00xx case
  if ((str.length > 1) && (str.substring(0,1) == "0") && (str.indexOf(".") == -1)) {
   if (showalert)
    alert(top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));
    return false;
  }
  }// str is not defined
  return true;
}


function fIsInt(str) {

  var ch=str.substring(0,1);
  if ((ch <= "0") || (ch > "9")) {
    return false;
  }
  for (var i = 1; i < str.length; i++) {
    ch = str.substring(i, i+1);
    if ((ch < "0") || (ch > "9")) {
      return false;
    }
  }
  return true;
}

function fIsNumericOnly(inputValue) {
           var c;
           for(var index=0; index < inputValue.length; index++) {
             c=inputValue.charAt(index);
             if (!(((c>="0")&&(c<="9")) ||
                   (c=="+") ||
                   (c=="-") ||
                   (c==",") ||
                   (c==".")))
               return false;
           }
           return true;
         }

function fIsAlphaUpperCaseOnly(inputValue) {
           var c;
           for(var index=0; index < inputValue.length; index++) {
             c=inputValue.charAt(index);
             if((c>="a")&&(c<="z"))
               return false;
           }
           return true;
         }

function fCheckPosNum(inputvalue, nozero){
  var str = inputvalue + "";
  var msg = top.g_objMessages.mstrGetMesg("AP_WEB_NOTPOS_NUM");

  if (fIsNum(str, true)){

    if (((str < 0) && (!nozero)) || ((str <= 0) && (nozero))) {
      msg = msg.replace(/&VALUE/, str);
      alert(msg);
      return false;
    }

  }else
    return false;

  return true;
} 

function fSilentCheckPosNum(inputvalue, nozero){
  var str = inputvalue + "";

  if (fIsNum(str, false)){

    if (((str < 0) && (!nozero)) || ((str <= 0) && (nozero))) {
      return false;
    }

  }else
    return false;

  return true;
} 

function fCompareDate(date1, date2) {

  if ((date1 != null) && (date2 != null)) {
    date1.setHours(00);
    date1.setMinutes(00);
    date1.setSeconds(00);
    date2.setHours(00);
    date2.setMinutes(00);
    date2.setSeconds(00);

    return (date1.getTime() - date2.getTime());


  } else {

    if ((date1 != null) && (date2 == null)) {
      return 1;
    }
    if ((date1 == null) && (date2 != null)) {
      return -1;
    }
    if ((date1 == null) && (date2 == null)) {
      return 0;
    }
  }

}


function fSwitchTabs(strTabName){
	top.fIEOnChangeTrigger(); // for bug 1413154
	if (fGetObjDetail() != null)
	 if (!fGetObjDetail().closed){
	   fGetObjDetail().close();
	   g_winDetail = null;
	 }
        if (strTabName == 'OOP'){
	   top.g_strCurrentTab = 'OOP';
	   top.fGenTabs('OOP');
           if (fGetObjExpReport().getTotalReceipts('OOP') > 0)
               framMain.framButtons.setEnabledOOP();
           else
               framMain.framButtons.setDisabledOOP();
	   top.fCreateMoreLines(0, false);
	updateButtonFrame('OOP');
        }
        else if (strTabName == 'CC'){
	   top.fGenTabs('CC');
	   if (g_objCCardData.dataLoaded()){
	    fDrawCCardLines(true);
	   }
           else
           {
              framMain.lines.location = g_dcdName + 
                     "/AP_SSE_CODE_GEN_UTIL_PKG.genCreditCardLines?p_userId="+top.fGetObjHeader().getObjEmployee().mGetstrEmployeeID() + 
                     "&p_reimbCurr=" + 
                     top.fGetObjHeader().getReimbursCurr();
              g_objCCardData.setLoaded(true); 
           }
        }
 
	
}



function fBlankPage(strColor,strText) {
	if (strText)
        return "<HTML dir=\""+top.g_strDirection+"\"><BODY BGCOLOR=#"+strColor+">"+strText+"</BODY></HTML>"
	else
	return "<HTML dir=\""+top.g_strDirection+"\"><BODY BGCOLOR=#"+strColor+"></BODY></HTML>"
}

function fFlexHeader(strColor){
var strReturn = "";
var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_DETAIL');
	strReturn+="<BASE HREF=\""+top.g_strBaseHref+"\">";
	strReturn+= "<HTML dir=\""+top.g_strDirection+"\"><STYLE><!--";
	strReturn+= ".DATABLACK 	{font-family:Arial, sans-serif;color:black;font-weight:bold;font-size:10pt}--></STYLE>\n";
	strReturn+= "<BODY BGCOLOR=#"+strColor+">\n<center><table border=0 cellpadding=0 cellspacing=0 width=90%><tr><td><font class=datablack>"+  tempRegion.mobjGetRegionItem('AP_WEB_ADD_INFO').mstrGetPrompt()+"</font><br><hr width=100%></td></tr></table>\n</center></BODY></HTML>";

return strReturn;
}



function  fDynamicButton(P_ButtonText,
                        P_LImageFileName,
                        P_RImageFileName,
                        P_OnMouseOverText,
                        P_HyperTextCall,
                        P_LanguageCode,
                        P_JavaScriptFlag,
                        P_DisabledFlag,
                        P_TargetFrame)
{
  var l_ImagePath = top.g_strImgDir;
  var l_DisabledColor = '#999999';
  var result = '';
  result += '<table border=0 cellpadding=0 cellspacing=0>';

  if (P_ButtonText)
    result+='<tr><td rowspan=5>';
  else
    result += '<tr><td>';
  if (P_HyperTextCall){
    result += '<a href="'+P_HyperTextCall+'" ';
    result += 'onMouseOver="window.status=&quot;'+
        P_OnMouseOverText+'&quot; ; return true"';
    if (P_TargetFrame==''){
    result +=' target="_parent"';
    }
    result +='>';
    result +='<img src="'+l_ImagePath+P_LImageFileName+
        '" border=0></a></td>';
  }
  else{
    result +='<img src="'+l_ImagePath+P_LImageFileName+
        '" border=0></td>';
  }
  if (P_ButtonText)
     result += '<td height=1></td>';
    //result += '<td height=1 class=colorg2><img height=1 width=1 src="'+
          //l_ImagePath+'APWDBPXB.gif"></td>';
  if (P_ButtonText)
    result += '<td height=20 width=12 rowspan=5>';
  else
    result +='<td height=20 width=12>';
  if (P_HyperTextCall){
    result +='<a href="'+P_HyperTextCall+'" ';
    result +='onMouseOver="window.status=&quot;'+
        P_OnMouseOverText+'&quot; ; return true"';
    if (P_TargetFrame==''){
    result +=' target="_parent"';
    }
    result +='>';
    result += '<img src="'+l_ImagePath+P_RImageFileName+
          '" border=0></a></td>';
  }
  else{
    result += '<img src="'+l_ImagePath+P_RImageFileName+
          '" border=0></td>';
  }
  if (P_ButtonText) {
    result +='<tr><td height=1 class=color6>';
    result += '<img width=1 height=1 src="'+
          l_ImagePath+'APWDBPXW.gif" border=0></td></tr>';
    result+='<tr><td height=20 align=center valign=center'+
          ' class=colorg5 nowrap>';
    if (P_HyperTextCall){
      result+='<a href="'+P_HyperTextCall+'" ';
      result+='onMouseOver="window.status=&quot;'+
          P_OnMouseOverText+'&quot; ; return true"';
      if (P_TargetFrame==''){
      result +=' target="_parent"';
      }
      result +='>';
    }
   
    if (P_DisabledFlag)
        result+='<font class=disabledbuttontext>'+P_ButtonText+
          '</font></a></td></tr>';
    else
        result+='<font class=buttontext>'+P_ButtonText+
          '</font></a></td></tr>';

    result+='<tr><td height=1 class=colorg3>';
    result+='<img width=1 height=1 src="'+
          l_ImagePath+'APWDBPX6.gif" border=0></td></tr>';
    result += '<tr><td height=1></td></tr>';
   }

  result+='</table>';
  return result;
}


function fResetTotals()
{
  var total = 0;
  var strTotal = "";
  var strCount = 0;
  for (var j=0; j< fGetObjExpReport().oopReceipts.length; j++) {
    if (fGetReceiptObj(j) != null) {
     if (fIsNum(fCalcReimbursAmount(fGetObjExpReport().getNthReceipt(j))+"", false))
      total += (fCalcReimbursAmount(fGetObjExpReport().getNthReceipt(j)) - 0);
    }
  }
  strTotal = fMoneyFormat(eval(total), fGetObjHeader().getReimbursCurr());
  strCount = fGetObjExpReport().getTotalReceipts('OOP')+"";
  //top.fWaitUntilFrameLoaded('top.g_bButtonsFrameLoaded','');
  
  if (top.g_bButtonsFrameLoaded) {
  framMain.framButtons.setTotalInfo(strCount,strTotal);
  }
  
  
  
}

function fParseErrorString(strErrorString){
  var iStart, iEnd;
  iStart = strErrorString.indexOf("{");
  iEnd = strErrorString.indexOf("}");
  if (iStart >= 0 && iEnd >= 0) return strErrorString.substring(iStart+1,iEnd-1);

}


function fDisplayHelp(i) {
    var helpurl = "";
    var baseurl = self.location.protocol + "//" + self.location.host + top.g_strImgDir;
    if (i == 1)
      helpurl = baseurl + "/APWERINF.htm";
    else if (i == 2)
      helpurl = baseurl + "/APWENRCT.htm";
    else if (i == 2.1)
      helpurl = baseurl + "/APWENRCT.htm#Restoring Reports";
    else if (i == 3)
      helpurl = baseurl + "/APWRVWRT.htm";
    else if (i == 4)
      helpurl = baseurl + "/APWULRCT.htm";
    else if (i == 4.1)
      helpurl = baseurl + "/APWULRCT.htm#Reviewing Upload";
    else if (i == 5)
      helpurl = baseurl + "/APWMDRCT.htm";
    else if (i == 6)
      helpurl = baseurl + "/APWHLCC1.htm";

    // open(helpurl, "Expense_Report_Help");
    open(helpurl, "Expense_Report_Help", "scrollbars=yes,resizable=yes,menubar=no,location=no,width=800,height=600");
  }

function fRtrim(Str){
   var Str_len = Str.length;
   var status = true;

   while (status) {
     var i = Str.charAt(Str_len - 1);
     if (i != " ") {
       status = false;
     }
     else
       Str_len = Str_len - 1;
     if (Str_len == 0) {
      status = false;
     }
   }

   return Str.substring(0,Str_len);

}

// rlangi - money format helper functions
function fMoneyFormatDailyRate(receipt) {
	return top.fMoneyFormat(receipt.getDailyRate(), receipt.getReceiptCurr());
}
function fMoneyFormatReceiptAmount(receipt) {
	return top.fMoneyFormat(receipt.getReceiptAmount(), receipt.getReceiptCurr());
}
function fMoneyFormatExpensedAmount(receipt) {
	return top.fMoneyFormat(receipt.getExpensedAmount(), receipt.getReceiptCurr());
}
function fMoneyFormatExchRate(receipt) {
	return top.fMoneyFormat(receipt.getExchRate(), receipt.getReceiptCurr());
}
function fMoneyFormatReimbursAmount(receipt) {
	return top.fMoneyFormat(receipt.getReimbursAmount());
}

// rlangi - if currency is null use fGetObjHeader().getReimbursCurr()

function fMoneyFormat(input, currency) {
if (currency == null) currency = fGetObjHeader().getReimbursCurr();
 var index = top.fRetrieveCurrencyIndex(currency);
 var minimumAcctUnit = top.g_arrCurrency[index].minimumAcctUnit;
 var precision = top.g_arrCurrency[index].precision;

 var strInput = input + "";

 if (strInput == "")
   return("");
 if ((eval(strInput) == 0) || (eval(strInput) == 0.0) || (eval(strInput) == 0.00)){
  if (precision <= 0)
    return("0");
  else if (precision == 1)
    return("0.0");
  else if (precision == 2)
    return("0.00");
  else if (precision == 3)
    return("0.000");

 }
 var prefix;
 if ((eval(input) < 0) && (eval(input) > -1)) {
   prefix = "-";
 } else {
   prefix = "";
 }

 if (minimumAcctUnit != ""){
   var amount = Math.round(eval(strInput)/minimumAcctUnit) * minimumAcctUnit;
   return (amount);
 } else {
   if (precision == 0) {
     return Math.round(input);
   }
   var dollars;
   var tmp;
   var multiplier;

   if (eval(strInput) >= 0) {
       dollars = Math.floor(strInput);
   } else {
       dollars = Math.ceil(strInput);
   }
 }
   tmp = strInput + "0";
   multiplier = 10;

   for (var decimalAt = 0; decimalAt < tmp.length; decimalAt++) {
      if (tmp.charAt(decimalAt)==".") break;
   }
   for (var i= 0; i < precision-1; i++){
     multiplier = 10 * multiplier;
   }
   var cents  = "" + Math.round(Math.abs(strInput) * multiplier)
   cents = cents.substring(cents.length-precision, cents.length)
   if ((eval(strInput) < 1) && (eval(strInput) > -1)){
     cents = "" + eval(cents) / multiplier;
     if (cents.charAt(0)=="0")
       cents = cents.substring(2,cents.length);
     else
       cents = cents.substring(1,cents.length);
   }
   dollars += ((tmp.charAt(decimalAt+precision)=="9")&&(cents=="00"))? 1 : 0;

   return prefix + dollars + "." + cents;

}

//Shuh
// Returns the number of decimal digits of num
function fCalcDecimalPrecision(num) {
   var tmp = num + "";
   var dec_len = 0;

   for (var decimalAt = 0; decimalAt < tmp.length; decimalAt++) {
      if (tmp.charAt(decimalAt)==".") break
   }//end for
   if (decimalAt < tmp.length) 
       dec_len =  tmp.length - decimalAt - 1;

   return dec_len;
} //end function fCalcDecimalPrecision

function fMultiply( op1, op2 )
{
    var total_precision = fCalcDecimalPrecision(op1) + fCalcDecimalPrecision(op2);   
    var adjust = Math.pow( 10, (total_precision + 1) * -1 );
    var factor = Math.pow( 10, total_precision );

    return Math.round((op1 * op2 + adjust) * factor) / factor;
}

//Shuh
// Arguments:
//   num - number to be rounded
//   precision - precision of the currency, i.e., precision with which to
//   		 round num
function fRound(num, precision) 
{
  var factor = Math.pow( 10, precision );
  return Math.round( num * factor) / factor;
}


/* --------------------------
   Functions for Header
   -------------------------- */
function fChangeCostCenter(strCostCenter) { 
	top.fGetObjHeader().setCostCenter(strCostCenter); 
}

function fReGenExpTypePoplist(){
   var popExpType = null;
   if (fIsOOP()) { 
     for (var i=1;i<=top.fGetObjExpReport().getTotalReceipts('OOP');i++){
       popExpType = eval('top.framMain.lines.document.data.popExpType'+ i);
       popExpType.selectedIndex = 0;
      }
       fCreateMoreLines(0,false);// Re-populate lines with new expense type pop-list
 }
 else{ // Credit Card
    fDrawCCardLines(false);
 }
 
 if (fGetObjDetail()!=null)
      if (!fGetObjDetail().closed){
	/*
        var nActualLine = fGetObjExpReport().getNthReceipt(top.fGetObjExpReport().currentLine);
        fGenerateDetailWindow();
	fGenerateDetailBody();
	fGenerateDetailButtons();
		
	if (nActualLine>0){
	  top.g_winDetail.bRunPopDetail = true;
	  top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.fPopulateReceiptDetail('+nActualLine+')');
	}
	top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.g_winDetail.fDetailSetExpenseType(top.fGetETypeIndex(top.fGetObjExpReport().cCardReceipts[fGetObjDetail().iRecIndex].getObjExpenseType().mGetID(), null))');
	top.fUpdatePopShowReceipt();
	*/
	top.g_winDetail.close();
      }

}

function fChangeExpTemplate(strExpTemplateID){
 var popExpType = null;
 var nActualLine = 0;
 var popExpTemplate = top.framMain.header.document.reportHeaderForm.popExpTemplate;
 if (!Nav4) fIEOnChangeTrigger(); 

 if (top.fGetObjExpReport().getTotalReceipts('OOP')>0 || top.fGetObjExpReport().getTotalReceipts('CC')>0){

  if (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_EXP_TYPE_CHANGE_WRN"))){

   for (var i=0;i<top.g_arrExpenseTemplate.length; i++) {
   if (top.g_arrExpenseTemplate[i].id == strExpTemplateID)
 	top.fGetObjHeader().setExpTemplate(top.g_arrExpenseTemplate[i]);
   }

//Reset expense type for OOP
 var objReceipts = objExpenseReport.oopReceipts;
 for (var i=0; i<objReceipts.length; i++)
      if (objReceipts[i] != null) 
          objReceipts[i].setExpenseType("");

//Reset expense type for Credit Card
 objReceipts = objExpenseReport.cCardReceipts;
 var trxn, objTrxns = g_objCCardData.getTrxns(g_selectedCardProg);
 var objExpTemplate = objExpenseReport.header.getObjExpTemplate();
 for (var i=0; i<objReceipts.length; i++)
      if (objReceipts[i] != null) {
          trxn = objTrxns.getTrx(objReceipts[i].getcCardTrxnId());
	  var objE = top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(trxn.getFolioType());
          objReceipts[i].setExpenseType(objE);
      }
     fReGenExpTypePoplist();
 }// if confirm
 else
  popExpTemplate.selectedIndex = fGetExpTemplateIndex(fGetObjHeader().getExpTemplateID())
 } //if (receipt exist)
 else{
  for (var i=0;i<top.g_arrExpenseTemplate.length; i++) {
   if (top.g_arrExpenseTemplate[i].id == strExpTemplateID)
 	top.fGetObjHeader().setExpTemplate(top.g_arrExpenseTemplate[i]);
   }
  fReGenExpTypePoplist();
 }
  
} //end function fChangeExpTemplate


function fChangeEmpName(strEmpID){
   if (!Nav4) fIEOnChangeTrigger();
   var nOOPReceipt = fGetObjExpReport().getTotalReceipts('OOP');
   if (nOOPReceipt > 0 || (fGetObjExpReport().getTotalReceipts('CC')>0)){
       if (!confirm(g_objMessages.mstrGetMesg("AP_WEB_EMP_CHANGE_WRN"))) {
           top.fRecoverEmployee(top.fGetObjHeader().getObjEmployee().mGetstrEmployeeID());
           return;
       }
   }

   // Delete all the receipt objects
   fGetObjExpReport().deleteReceipts('OOP');
   fGetObjExpReport().deleteReceipts('CC');

   g_objCCardData = new creditCardData(); // Clear the credit card data

   fGetObjHeader().setEmployee(top.g_arrEmployee.mobjGetEmployee(strEmpID,""));
   fGetObjHeader().setCostCenter(top.g_arrEmployee.mobjGetEmployee(strEmpID,"").mGetstrCostCenter());

   if (fIsOOP()){
      if (nOOPReceipt > 0) {
          top.framMain.lines.count = 0;
          top.g_iTotalDisplayedLines = top.g_iInitialDisplayedLines;
	  if (!Nav4)
	     top.framMain.lines.location.reload();
          else fCreateMoreLines(0,true);
          var nTotalReceipts = top.objExpenseReport.getTotalReceipts('OOP');
          var nTotalAmount = top.fMoneyFormat(top.objExpenseReport.calculateTotal(), objExpenseReport.header.getReimbursCurr());
          top.framMain.framButtons.setTotalInfo(nTotalReceipts, nTotalAmount);
          if (fIsDetailOpen()) fOpenDetail('OOP',1);
      }
      else fCreateMoreLines(0,true);
   }
   else {// for Credit Card 
      // Get the credit card data and redraw the frame
      framMain.lines.location = g_dcdName + "/AP_SSE_CODE_GEN_UTIL_PKG.genCreditCardLines?p_userId="+
                                fGetObjHeader().getObjEmployee().mGetstrEmployeeID() + 
                                "&p_reimbCurr=" + fGetObjHeader().getReimbursCurr();
      g_objCCardData.setLoaded(true); // credit data was populated
      if (fIsDetailOpen()) fCloseDetail();
   }
   framMain.header.document.reportHeaderForm.txtCostCenter.value = fGetObjHeader().getObjEmployee().mGetstrCostCenter();
   framMain.header.document.reportHeaderForm.popEmployeeId.focus();
}

function fRecoverEmployee(strEmployeeID){
  var popEmployee = top.framMain.header.document.reportHeaderForm.popEmployeeId;
  if (top.g_arrEmployee.mnGetNumOfEmployees() > 1){
    for (var i=0;i<popEmployee.options.length;i++){
	if (popEmployee.options[i].value == strEmployeeID)
		popEmployee.selectedIndex = i;
  
    }
  }
}

function fChangeApprover(strFullName, strEmpID) { 
	  fGetObjHeader().setApprover(strFullName);
	  fGetObjHeader().setApproverID(strEmpID);
}

function fChangePurpose(strPurpose) { 
	fGetObjHeader().setPurpose(strPurpose);
}

// -------------------------------------------------------------
// invoked by onFocus event handler of EVERY frame's document
// -------------------------------------------------------------
function fCheckModal() {
  if (Nav4){
	// rlangi - itemize window
      
      if (top.g_winItemize) {if (!top.g_winItemize.closed)
              top.g_winItemize.focus();
      }
	if (top.g_winCal) {
	  if (!top.g_winCal.closed) top.g_winCal.focus();
	}
	if (top.g_winCurrency) {
	  if (!top.g_winCurrency.closed) top.g_winCurrency.focus();
	}
	if (top.g_winCCardTrxn) {
	  if (!top.g_winCCardTrxn.closed) top.g_winCCardTrxn.focus();
	}
   }
}



/* ---------------------------------
   Functions for line operation
   --------------------------------- */
function fFocusOnLine(nLine,field_name){
	// Make sure approver's name is saved
	var f = framMain.header.document.reportHeaderForm;
	if (f) if (f.AP_WEB_FULLNAME) fChangeApprover(f.AP_WEB_FULLNAME.value,f.AP_WEB_EMPID.value);
   	var nActualLine = top.fGetObjExpReport().getNthReceipt(nLine);
	if ((nActualLine == -1) && (nLine>top.fGetObjExpReport().getTotalReceipts(top.g_strCurrentTab)+1)){
               g_bFocusOnDiffField = true;
               fFocusOnLine(nLine-1,field_name);
        }
        if ((top.framMain.lines.g_iCurrentLine>=1) && (top.framMain.lines.g_iCurrentLine!=nLine)){     
                var unselect_box = eval("top.framMain.lines.document.data.cbxSelect"+top.framMain.lines.g_iCurrentLine);
                unselect_box.checked = false;
        }
                
        if ((top.framMain.lines.g_iCurrentLine!=nLine) || (nLine == 1)){
                        var select_box = eval("top.framMain.lines.document.data.cbxSelect"+nLine);
			select_box.checked = true;
			top.framMain.lines.g_iCurrentLine = nLine;
			
                        nActualLine = top.fGetObjExpReport().getNthReceipt(nLine);
                        if ((nActualLine == -1) && (nLine>top.fGetObjExpReport().getTotalReceipts(top.g_strCurrentTab))){ 
                            // focus on a new line
			    if (top.fIsOOP()){
                              framMain.framButtons.setDisabledOOP();
			      var popTemp = eval('top.framMain.lines.document.data.popExpType'+nLine);
			      var bIsReq = 'N';
			     
			      if (bIsReq == 'Y')
				    top.fSetReqImg();
			      else top.framMain.lines.document.reqimg.src = top.g_strImgDir+"APWPX4.gif";
			      if (top.framMain.lines.document.tax_reqimg) 
				 top.framMain.lines.document.tax_reqimg.src = top.g_strImgDir+"APWPX4.gif";
			    } //end if (top.fIsOOP())
			    if (fGetObjDetail() != null && !fGetObjDetail().closed){
				top.g_winDetail.iRecIndex = nActualLine;
				top.g_winDetail.iLineNum = nLine; 
		  		top.fGenerateDetailBody(-1,nLine);
			     	top.g_winDetail.bRunPopDetail = true;
			    	top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.fPopulateReceiptDetail(-1)');
				top.fUpdatePopShowReceipt();

			    } //end if detail != null

                          } //end if (nAcutalLine == -1) ..
                          else{
                            // focus on a existing line
			      var bIsReq = 'N';
			      var objE = top.fGetReceiptObj(nActualLine).getObjExpenseType();
			      if (top.fIsOOP())
                                 framMain.framButtons.setEnabledOOP();
                              if (top.fGetReceiptObj(nActualLine).getObjExpenseType())
				bIsReq = objE.mGetbJustifRequired();
			      if (bIsReq == 'Y')
				    top.fSetReqImg();
			      else top.framMain.lines.document.reqimg.src = top.g_strImgDir+"APWPX4.gif";
			      // for bug 1303435
			      if (framMain.lines.document.tax_reqimg){
			          if (top.fGetReceiptObj(nActualLine).getAmountIncludesTax()) framMain.lines.document.tax_reqimg.src = top.reqimg1.src;
			          else top.framMain.lines.document.tax_reqimg.src = top.g_strImgDir+"APWPX4.gif";
			      }
			      		      
			      if ((fGetObjDetail() != null) && (nActualLine >=0)){
			         if (!fGetObjDetail().closed){
					top.g_winDetail.iRecIndex = nActualLine;
					top.g_winDetail.iLineNum = nLine; 
					top.fGenerateDetailBody(nActualLine,nLine);
			        	// need to repopulate detail page
					top.g_winDetail.bRunPopDetail = true;
                                        if ((!top.fIsOOP()) && (top.g_firstOpenDetail==0))
                                           top.g_firstOpenDetail = 1;
					top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.fPopulateReceiptDetail('+nActualLine+')');
					top.fUpdatePopShowReceipt();

				 }
			      }//end if (fGetObjDetail() != null) 
                            } //end else
               } //end if (top.framMain.lines.g_iCurrentLine!=nLine) || (nLine == 1)

               if ((g_bFocusOnDiffField) && (field_name!="") && (nLine>=1)){
                var focus_field = eval("top.framMain.lines.document.data."+field_name+nLine);
                if (focus_field) focus_field.focus();
                top.g_bFocusOnDiffField = false;
                }
	if (top.fGetReceiptObj(nActualLine)){
	  var objE = top.fGetReceiptObj(nActualLine).getObjExpenseType();
	  if (objE)
	    if ((field_name == 'popTaxCode') && (objE.bUpdateDefaultTaxCode == 'N')){
		var f = eval("top.framMain.lines.document.data."+field_name+nLine);
                if (f) f.blur();
	    }
	    if ((field_name == 'AP_WEB_PA_PROJECT_NUMBER' || field_name == 'AP_WEB_PA_TASK_NUMBER') && (!top.fGetReceiptObj(nActualLine).isProjectEnabled())){
		var f = eval("top.framMain.lines.document.data."+field_name+nLine);
                if (f) f.blur();
	    }
	  }
        return;
}     


function fUpdateLineStatus(nLine,bPopulateDetail){
        if (!fIsOOP()) return  // This function is used by OOP code only
	var iRecIndex = top.fGetObjExpReport().getNthReceipt(nLine);
	if (iRecIndex <0){
	// Add new lines
	top.fGetObjExpReport().addReceipt("",1,0,0,false,top.fGetObjHeader().getReimbursCurr(),1,null,"",null,"",null,"",null,"","","","");
	iRecIndex=  top.fGetObjExpReport().getNthReceipt(top.fGetObjExpReport().getTotalReceipts('OOP'));
	var tempSelect = eval("top.framMain.lines.document.data.cbxSelect"+nLine);
	tempSelect.value = iRecIndex;
	fGetReceiptObj(iRecIndex).addTax('','','','','','','','N','N');
        framMain.framButtons.setEnabledOOP();
	 if (fGetObjDetail() != null){
	  if (!fGetObjDetail().closed){
	    fGetObjDetail().iRecIndex = fGetObjExpReport().getNthReceipt(nLine);
	    fGetObjDetail().iLineNum = nLine;
	    fGenerateDetailButtons();
	    fUpdatePopShowReceipt();
	    if (bPopulateDetail){
		top.g_winDetail.bRunPopDetail = true;
	       fPopulateReceiptDetail(fGetObjDetail().iRecIndex);
	    }
	  }
	 } 
        var nTotalReceipts = objExpenseReport.getTotalReceipts('OOP');
	var nTotalAmount = fMoneyFormat(top.objExpenseReport.calculateTotal('OOP'), objExpenseReport.header.getReimbursCurr());
  	framMain.framButtons.setTotalInfo(nTotalReceipts, nTotalAmount);

	}// iRecIndex<0
        
}

function fPopulateUndeleteLines(){
var l_counter = 0;
	 for (var i=0;i< fGetObjExpReport().getTotalReceipts('OOP');i++){
         if (fGetReceiptObj(fGetObjExpReport().getNthReceipt(i+1))!=null){
		l_counter ++;
                framMain.lines.fPopulateLines(fGetObjExpReport().getNthReceipt(i+1),l_counter);
         }
        }

}


function fGenExpPopOptions(strExpType){
  var strResult = "";
  var strExpTypeID = "";
  var strExpTypeName = "";
//sungha  
  if (!top.fGetObjHeader().getObjExpTemplate()) top.objExpenseReport.header.setExpTemplate(top.g_arrExpenseTemplate[0]);
  var expTempObj = top.fGetObjHeader().getObjExpTemplate();
  for (var i=0; i < expTempObj.arrExpTypes.length;i++){ //sungha
     strExpTypeID = expTempObj.arrExpTypes[i].mGetID(); // 1000
     strExpTypeName = expTempObj.getExpTypeObj(strExpTypeID).mGetstrName(); // Airfare
     strResult += "<option value='"+strExpTypeID+"'";
     if (strExpTypeName == strExpType) strResult += " SELECTED";
       strResult += ">"+strExpTypeName;
  } //end for
  return strResult;
} //end function fGenExpPopOptions


/* -------------------
   Error window
  -------------------- */
function fOpenErrorWindow(strTitle,strContent){
      top.g_winError = open('','ErrorDialog','resizable=yes,width=600,height=350,scrollbars=yes');
      if (top.g_winError.opener == null) {
                 top.g_winError.opener = self; 
      }
      top.fGenErrorFrames();
      if (isWindows95 && isIE5){
	top.fGenBlueBorder('g_winError','leftborder');
	top.fGenBlueBorder('g_winError','rightborder');
      }
      top.fGenErrorHeader(strTitle);
      top.fGenErrorBody(strContent);
      top.fGenErrorTail();
}

function fCloseErrorWindow() { 
        if (top.g_winError.open) top.g_winError.close();
	top.g_winError.opener.focus();
	top.g_winError = null;
        }
function fGenErrorFrames(){
	
	var d = eval('top.g_winError.document');
	d.open();
	d.writeln("<frameset cols=\"3,*,3\" border=0>");
	d.writeln("     <frame  ");
        if (!(isWindows95 && isIE5)) d.writeln("             src=\"javascript:top.opener.top.fBlankPage('336699')\""); 
        d.writeln("             frameborder=0");
        d.writeln("             name=leftborder");
        d.writeln("             marginwidth=0");
        d.writeln("             scrolling=no>");
        d.writeln("     <frameset rows = \"45,*,40\" border=0 framespacing=0>");
        d.writeln("             <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("		src=\"javascript:top.opener.top.fBlankPage('336699');\"");
        d.writeln("		        name=framErrorHeader");
        d.writeln("                     marginwidth=0");
        d.writeln("                     frameborder=0");
        d.writeln("                     scrolling=no>");
        d.writeln("             <frame ");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fBlankPage('336699');\"");
        d.writeln("                     frameborder=0");
        d.writeln("                     name=framErrorBody");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=auto>");
        d.writeln("             <frame ");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fBlankPage('336699');\"");
        d.writeln("                     frameborder=0");
        d.writeln("                     name=framErrorTail");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=no>");
        d.writeln("     </frameset>");
        d.writeln("     <frame  ");
        if (!(isWindows95 && isIE5)) d.writeln("             src=\"javascript:top.opener.top.fBlankPage('336699')\""); 
        d.writeln("             frameborder=0");
        d.writeln("             name=rightborder");
        d.writeln("             marginwidth=0");
        d.writeln("             scrolling=no>");
	d.writeln("</FRAMESET>");
        d.close();
	
}

function fGenErrorHeader(strTitle){
	var targetFrame = top.g_winError.framErrorHeader;
        var d = top.g_winError.framErrorHeader.document;
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
        d.writeln("<title>Validation Errors</title>");
        top.fGenerateStyleCodes(targetFrame);
        d.writeln("<body class=color3 onFocus=\"javascript:if (top.opener.top.bVersion.ns4){top.opener.top.fFocusOnErrorWindow();}return true;\">");
	
        // d.writeln("<form name=formError>");
        d.writeln("<!--beginning of no-tab container code-->");
	d.writeln("<CENTER>");
        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>");
        d.writeln("	<TR class=colorg5>");
        d.writeln("          <TD ROWSPAN=2 valign=top align=left width=1><IMG SRC="+top.g_strImgDir+"APWCTTL.gif></TD>");
        d.writeln("          <TD class=color6 Width=1000 HEIGHT=1><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD>");
        d.writeln("          <TD ROWSPAN=2 align=right width=1><IMG SRC="+top.g_strImgDir+"APWCTTR.gif></TD>");
        d.writeln("  	</TR>");
        d.writeln("  	<TR>");
        d.writeln("          <TD height=5 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif></TD>");
        d.writeln("  	</TR>");
        d.writeln("  	<TR>");
        d.writeln("          <TD class=colorg5 COLSPAN=3 valign=top> ");
        d.writeln("          <table width=95% cellpadding=0 cellspacing=0 border=0><tr><td>");
	d.writeln("	     <font class=datablack>&nbsp;&nbsp;&nbsp;"+strTitle+"</font><br><br>");
	d.writeln("	     </td></tr></table>");
        d.writeln("          <hr width=95%>");
        d.writeln("          </TD>");
        d.writeln("  	</TR>");
 	d.writeln("</TABLE></CENTER></BODY></HTML>");
	
	d.close();
}

function fGenBlueBorder(strWinName, strFrameName){
	var d = eval('top.'+strWinName+'.'+strFrameName+'.document');
	var f = eval('top.'+strWinName+'.'+strFrameName);
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
        top.fGenerateStyleCodes(f);
        d.writeln("<body class=color3>");
	d.writeln("</body></html>");
	d.close();
}

function fGenErrorTail(){
	var targetFrame = top.g_winError.framErrorTail;
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_ERROR_WIN');
        var d = top.g_winError.framErrorTail.document;
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
        //d.writeln("<title>Validation Errors</title>");



	d.writeln("<script language=javascript> ");
        d.writeln("<!-- Hide script from old browsers ");
 	d.writeln(" var Restorable = false");
  	d.writeln("var Nav4 = ((navigator.appName == \"Netscape\") && (parseInt(navigator.appVersion) == 4))");
  	d.writeln("var Win32");
  	d.writeln("if (Nav4) {");
        d.writeln("	Win32 = ((navigator.userAgent.indexOf(\"Win\") != -1) && (navigator.userAgent.indexOf(\"Win16\") == -1))");
  	d.writeln("} ");
  	d.writeln("else ");
  	d.writeln("{");
       	d.writeln(" Win32 = ((navigator.userAgent.indexOf(\"Windows\") != -1) && (navigator.userAgent.indexOf(\"Windows 3.1\") == -1))");
  	d.writeln("}");
  	d.writeln("function fprintFrame(wind) {");
        
        d.writeln("if (Win32) {");
        d.writeln("        if (Nav4) {");
        d.writeln("                wind.print()");
        d.writeln("        } else {");
        d.writeln("                         wind.onerror = fprintErr");
        d.writeln("                wind.focus()");
        d.writeln("                IEControl.ExecWB(6, 1)");
        d.writeln("        }");
        d.writeln("} ");
	d.writeln("else {");
        d.writeln("        alert(\"Sorry. Printing is available only from Windows 95/98/NT.\")");
        d.writeln("}");
      	d.writeln("}");
  	d.writeln("function fprintErr() {");
	d.writeln("return true");
      	d.writeln("}");
        d.writeln(" // end hiding sript from old browsers--> ");
        d.writeln(" </script>");

        top.fGenerateStyleCodes(targetFrame);
	d.writeln("<body class=color3 onFocus=\"javascript:if (top.opener.top.bVersion.ns4){top.opener.top.fFocusOnErrorWindow();}return true;\">");
	d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>");
	d.writeln("<TR class=colorg5>");
        d.writeln("  <TD><IMG SRC="+top.g_strImgDir+"APWCTBL.gif height=5 width=5></TD>");
        d.writeln("  <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=1></TD>");
        d.writeln("  <TD align=right><IMG SRC="+top.g_strImgDir+"APWCTBR.gif height=5 width=5></TD>");
        d.writeln("</TR></table>");

        d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");

        d.writeln("    <TR><td height=5></td></TR>");
        d.writeln("    <TR>");
        d.writeln("    <TD align=right>");

        d.writeln("  <table border=0 cellspacing=0 cellpadding=0 width=100%>");
        d.writeln("  <tr align=right valign=center>");
        d.writeln("  <td valign=center align=left>");
        d.writeln("  <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
        d.writeln("  document.write(top.opener.top.fDynamicButton(\""+tempRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt()+"\",");
        d.writeln("                                  \"APWBRNDL.gif\",");
        d.writeln("                                  \"APWBRNDR.gif\",");
        d.writeln("                                  \""+tempRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt()+"\",");
        d.writeln("                                  \"javascript:void fprintFrame(parent.framErrorBody)\",");
        d.writeln("                                  \""+top.g_strLangCode+"\",");
        d.writeln("                                  false,");
        d.writeln("                                  false,");
        d.writeln("                                  'top.framMain.lines'));");
        d.writeln("  <\/SCRIPT>");
        d.writeln("  </td>");
	d.writeln("  <td width=5>");
	d.writeln("  </td>");
        d.writeln("  <td valign=center align=right>");
        d.writeln("  <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
        d.writeln("  document.write(top.opener.top.fDynamicButton(\""+tempRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
        d.writeln("                                  \"APWBRNDL.gif\",");
        d.writeln("                                  \"APWBRNDR.gif\",");
        d.writeln("                                  \""+tempRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
        d.writeln("                                  \"javascript:top.opener.top.fCloseErrorWindow()\",");
	d.writeln("                                  \""+top.g_strLangCode+"\",");
        d.writeln("                                  false,");
        d.writeln("                                  false,");
        d.writeln("                                  'top.framMain.lines'));");
        d.writeln("  <\/SCRIPT>");
        d.writeln("  </td>");
        d.writeln("  </tr>");
        d.writeln("  </table>");

        d.writeln("  </TD>");
        d.writeln("  </TR>");
        d.writeln("</TABLE>");
	d.writeln("<OBJECT ID=\"IEControl\" WIDTH=0 HEIGHT=0 CLASSID=\"clsid:8856F961-340A-11D0-A96B-00C04FD705A2\">");
	d.writeln("</OBJECT>");
	d.writeln("</BODY></HTML>");
	d.close();
}


function fGenErrorBody(strContent){
        
        var targetFrame = top.g_winError.framErrorBody;
        var d = top.g_winError.framErrorBody.document;
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
        //d.writeln("<title>Validation Errors</title>");
        top.fGenerateStyleCodes(targetFrame);
        d.writeln("<body class=colorg5 onFocus=\"javascript:if (top.opener.top.bVersion.ns4){top.opener.top.fFocusOnErrorWindow();}return true;\">");
	
        d.writeln("<form name=formError>");
        d.writeln("<!--beginning of no-tab container code-->");
	d.writeln("<CENTER>");
        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=colorg5>");
      
        d.writeln("  <TR align=center>");
        d.writeln("  <TD class=colorg5 COLSPAN=3>");
        d.writeln("	<center>");
        d.writeln("	<table border=0 cellpadding=0 cellspacing=0 width=100%>");

        d.writeln("	<tr>");
        d.writeln("		<td align=center><table width=90% cellspadding=0 cellspacing=0 border=0>");
	d.writeln("		<font class=promptblack>"+strContent+"</font></table></td>");
        
        d.writeln("	</tr>");
        d.writeln("	<tr><td></td></tr>");

        
       
        d.writeln("	</table>");
        d.writeln("	</center>");

        d.writeln("  </TD></TR>");
	d.writeln("</TABLE>");
        d.writeln("</CENTER>");
        d.writeln("</form>");
        d.writeln("</body>");
	d.writeln("</html>");
	d.close();
}

function fFocusOnErrorWindow(){
  
    var strErrorContent = top.fRequiredFieldsEntered('Report contains following errors:',true,-1);
    if (fDrawErrorWindow('Report contains following errors:',strErrorContent)){
       
    if (top.framSaveReport.document.formServerSerialize.p_action.value == 'save'){
	if (top.g_winError!=null) if (!top.g_winError.closed) top.g_winError.close();
        top.framSaveReport.document.formServerSerialize.submit();}
     else
	top.fFinalReview();
   }
   if (bVersion.ie4) top.g_winError.onfocus = null;
   
}




/* -----------------------
    Currency window
   ----------------------- */
function fOpenCurrWin(nLine) {
	top.fFocusOnLine(nLine,'curr');
	var iRecIndex = top.fGetObjExpReport().getNthReceipt(nLine);
        top.g_winCurrency=open('','currency','resizable=no,width=550,height=240');
      
        if (top.g_winCurrency.opener == null) {
                 top.g_winCurrency.opener = self; 
        }
        else {
		
        }
        top.fGenCurrencyWindow(iRecIndex,nLine);
	top.fPopulateCurrencyInfo(nLine);
}

function fCloseCurrWin() { 
        if (top.g_winCurrency != null){
	  if (!top.g_winCurrency.closed) g_winCurrency.close();
	}
	top.g_winCurrency.opener.focus();
        }

function fSaveCurrencyInfo(nLine){
        top.fCurrWinChangeCurrency(nLine);
	top.fCurrWinStoreRate(nLine);
	top.fCloseCurrWin();
	
}

// for bug #1322661
function fRecoverReimbursCurrency(strReimbursCurr){
  var popReimbursCurr = top.framMain.header.document.reportHeaderForm.popReimbursCurrency;
  if (popReimbursCurr)
    for (var i=0;i<popReimbursCurr.options.length;i++){
	if (popReimbursCurr.options[i].value == strReimbursCurr)
		popReimbursCurr.selectedIndex = i;
  
    }
}

function fChangeReimbursCurrency(strReimbursCurr){
  var targetField;
  var r;
  if (fIsOOP()){
   if (fGetObjExpReport().getTotalReceipts('OOP')>0){
    if (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_CUR_CHANGE_WRN"))){
	fGetObjHeader().setReimbursCurr(strReimbursCurr);
	for (var i=1;i<=fGetObjExpReport().getTotalReceipts('OOP');i++){
	   r = top.fGetReceiptObj(fGetObjExpReport().getNthReceipt(i));
	   top.framMain.lines.fSetReimbursAmount(fCalcReimbursAmount(fGetObjExpReport().getNthReceipt(i)), i) ;
	   //fGetAndSetExchRate(fGetObjExpReport().getNthReceipt(i));
	   framMain.lines.fChangeCurrency(r.getReceiptCurr(), i);
	   if (fIsDetailOpen()) fOpenDetail(g_strCurrentTab, g_winDetail.iLineNum);
	}
        top.fResetTotals();
	framMain.lines.fUpdateButtonsFrame();
    }
    else{ // need to roll back to original reimbursement currency
	  // for bug #1322661
	fRecoverReimbursCurrency(fGetObjHeader().getReimbursCurr());
    }
   }
   else {
	top.fGetObjHeader().setReimbursCurr(strReimbursCurr);
        top.fResetTotals();
   }
  }
  else // 'CC'
  {
      fGetObjHeader().setReimbursCurr(strReimbursCurr);
      fGetObjExpReport().deleteReceipts('CC');
      g_objCCardData = new creditCardData();
      // Get the credit card data and redraw the frame
      framMain.lines.location = g_dcdName + "/AP_SSE_CODE_GEN_UTIL_PKG.genCreditCardLines?p_userId="+
                                fGetObjHeader().getObjEmployee().mGetstrEmployeeID() + 
                                "&p_reimbCurr=" + fGetObjHeader().getReimbursCurr();
      g_objCCardData.setLoaded(true); 
      if (fIsDetailOpen()) fCloseDetail();
  }
}

function fGetCurrencyIndex(strCurrency){
  for (var i=0;i<top.g_arrCurrency.len;i++){
     if (g_arrCurrency[i].currency == strCurrency) return i;
  }
  return -1;
}



function fRetrieveCurrencyIndex(currency)
{
  var high = top.g_arrCurrency.len - 1;
  var low = 0;
  var mid;

  while (low <= high) {
    mid = Math.floor((high + low)/2);
    if (top.g_arrCurrency[mid].currency < currency) {
        low = mid + 1;
    } else if (top.g_arrCurrency[mid].currency > currency) {
        high = mid - 1;
    } else {
        return mid;
    }
  }
  return 0;

}

function fGetEffectiveDate(currency) {
  var index = top.fRetrieveCurrencyIndex(currency);
  if (index == "") {
     return null;
  } else {
    var strDate = top.g_arrCurrency[index].datEffective;
    var objTempDate = top.fStringToDate(strDate, false);
    return objTempDate;
  }
}



function fGetFixedRate(currency) {
  var index = top.fRetrieveCurrencyIndex(currency);
  if (index == -1) {
     return -1;
  } else {
     return top.g_arrCurrency[index].euroRate;
  }
}


function fDetermineConversion(recCurr, reimbCurr,objR) {
   if (recCurr == reimbCurr) {
    return g_reimbEqualsRec;
  }
   var datCompare = top.fStringToDate(objR.date); 
   
  if (datCompare == null){
     return g_reimbEqualsRec;
  }

  if (recCurr == "EUR") {
    if (fGetFixedRate(reimbCurr) > 0) {
        if (top.fCompareDate(datCompare, fGetEffectiveDate(reimbCurr)) >= 0)
          return g_reimbFixedRecEur;
        else
          return g_reimbEqualsRec;
    } else {
        return g_reimbEqualsRec;
    }

  } else if (reimbCurr == "EUR") {
    if (fGetFixedRate(recCurr) > 0) {
        if (top.fCompareDate(datCompare, fGetEffectiveDate(recCurr)) >= 0)
          return 1;
        else
          return 0;
    } else {
        return 0;
    }
  } else {
        if ((fGetFixedRate(reimbCurr) > 0) &&
            (fGetFixedRate(recCurr) > 0)) {
          if ((top.fCompareDate(datCompare, fGetEffectiveDate(reimbCurr)) >= 0)
               &&
              (top.fCompareDate(datCompare, fGetEffectiveDate(recCurr)) >= 0)) {
                return 3;
          } else {
                return 0;
          }
        } else {
          return 0;
        }
  }
} //end function fDetermineConversion


function fPopulateCurrencyInfo(nLine){
     var reimbursCurr = top.fGetObjHeader().getReimbursCurr();
     var nReceiptNum = top.fGetObjExpReport().getNthReceipt(nLine);
     var targetForm = top.g_winCurrency.document.formCurrency;
     var popReceiptCurr = top.g_winCurrency.document.formCurrency.popReceiptCurr;
    
     if (nReceiptNum>=0){
     var r = top.fGetReceiptObj(nReceiptNum);
     popReceiptCurr.selectedIndex = top.fGetCurrencyIndex(r.getReceiptCurr());
     var action = top.fDetermineConversion(r.getReceiptCurr(), reimbursCurr,r);  
     if ((action == top.g_reimbEurRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbFixedRecFixed)) 
	targetForm.txtRate.value = top.g_objMessages.mstrGetMesg("AP_WEB_FIXED");
     else targetForm.txtRate.value = r.getExchRate();
     targetForm.txtTotal.value = top.fMoneyFormat((r.getReimbursAmount()+""),reimbursCurr);
     }
}


function fGenRecCurrPopOptions(bWithFullName){
   var strResult = "";
   if (bWithFullName){
    for(var index = 0; index < top.g_arrCurrency.len; index++) {
        strResult += "<OPTION VALUE=" + top.g_arrCurrency[index].currency;
	if (top.g_arrCurrency[index].currency == top.fGetObjHeader().getReimbursCurr()) strResult += " SELECTED";
	strResult += "> " + top.g_arrCurrency[index].currency + " - " + top.g_arrCurrency[index].name.substring(0,26); 
    }
    return strResult;
   }
   else{
     for(var index = 0; index < top.g_arrCurrency.len; index++) {
        strResult += "<OPTION VALUE=" + top.g_arrCurrency[index].currency;
	if (top.g_arrCurrency[index].currency == top.fGetObjHeader().getReimbursCurr()) strResult += " SELECTED";
	strResult += "> " + top.g_arrCurrency[index].currency; 
     }
    return strResult;
   }
               
}

function fGenReimbCurrPopOptions(bWithFullName,strDefaultCurr){
   var strResult = "";
   
   var recCurrencyIndex = 0;
   if (bWithFullName){
    for(var index = 0; index < top.g_arrReimbCurrency.len; index++) {
        recCurrencyIndex = top.fRetrieveCurrencyIndex(top.g_arrReimbCurrency[index]);
        strResult += "<OPTION VALUE=" + top.g_arrCurrency[recCurrencyIndex].currency;
	if (top.g_arrCurrency[recCurrencyIndex].currency == strDefaultCurr) strResult += " SELECTED";
	strResult += "> " + top.g_arrCurrency[recCurrencyIndex].currency + " - " + top.g_arrCurrency[recCurrencyIndex].name; 
    }
    return strResult;
   }
   else{
     for(var index = 0; index < top.g_arrReimbCurrency.len; index++) {
        recCurrencyIndex = top.fRetrieveCurrencyIndex(top.g_arrReimbCurrency[index]);
        strResult += "<OPTION VALUE=" + top.g_arrCurrency[recCurrencyIndex].currency;
	if (top.g_arrCurrency[recCurrencyIndex].currency == strDefaultCurr) strResult += " SELECTED";
	strResult += "> " + top.g_arrCurrency[recCurrencyIndex].currency; 
     }
    return strResult;
   }


}



/* ------------------------
    Calendar window 
   ------------------------ */


function fOpenCalWin(strDateField,strFunc) {
	
	    g_winCal = new Object;
            top.g_winCal=open('','Pickdate','resizable=no,width=250,height=240');
	   
            if (top.g_winCal.opener == null) {
                top.g_winCal.opener = self; 
            }
	    else {}
	Calendar(top.g_strMonth,top.g_strYear,strDateField,strFunc);
}

function fCloseCalWin() { 
        if ((top.g_winCal) && (!top.g_winCal.closed)) top.g_winCal.close();
}


function Calendar(Month,Year,strDateField,strFunc) {
   var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CALENDAR_WIN');
    firstDay = new Date(Year,Month,1);
    startDay = firstDay.getDay();
    if (((Year % 4 == 0) && (Year % 100 != 0)) || (Year % 400 == 0))
         days[1] = 29; 
    else
         days[1] = 28;

    var d = eval('top.g_winCal.document');
    if (d){
    d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<html dir=\""+top.g_strDirection+"\">");
	d.writeln("<Title>"+tempRegion.mobjGetRegionItem('AP_WEB_SELECT_DATE').mstrGetPrompt()+"</title>");
	d.writeln("<LINK REL=stylesheet TYPE=\"text/css\" href=\""+top.g_strCssDir+"apwscabo.css\">");

	d.writeln("<BODY class=color3 TOPMARGIN=5 LEFTMARGIN=5>");


    d.writeln("<FORM NAME=\"Cal\">");
    d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>");
    d.writeln("  <TR class=colorg5>");
    d.writeln("	     <TD ROWSPAN=2 colspan=2><IMG SRC="+top.g_strImgDir+"APWCTTL.gif></TD>");
    d.writeln("	     <TD WIDTH=1000 HEIGHT=1 class=color6><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD><TD ROWSPAN=2 colspan=2><IMG SRC="+top.g_strImgDir+"APWCTTR.gif ></TD>");
    d.writeln("  </TR>");
    d.writeln("  <TR>");
    d.writeln("      <TD height=5 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif></td></tr>");
    d.writeln("  <TR><td width=1 class=color6><img src="+top.g_strImgDir+"APWDBPXW.gif></td><td width=4 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif></td><td class=colorg5>");

     d.write("<table border=0 width=100%><tr><td align=left valign=middle width=15%><a href=\"javascript:top.opener.top.fSkipback('"+strDateField+"','"+strFunc+"');\"><img src="+top.g_strImgDir+"APWIPRV.gif border=0></a></td>");
    d.write("<td align=left valign=middle>");
    d.write("</center><SELECT NAME=\"Month\" onChange=\"top.opener.fChangeMonth('"+strDateField+"','"+strFunc+"');\">");

    for (month=0; month<12; month++) {
        if (month == Month) d.write("<OPTION VALUE=" + month + " SELECTED>" + names[month] + "</OPTION>");
        else                d.write("<OPTION VALUE=" + month + ">"  + names[month] + "</OPTION>");
    }
    d.write("</SELECT>&nbsp;");
    d.write("<SELECT NAME=\"Year\" onChange=\"top.opener.fChangeYear('"+strDateField+"','"+strFunc+"');\">'");

    for (year=1900; year<2101; year++) {
        if (year == Year) d.write("<OPTION VALUE=" + year + " SELECTED>" + year + "</OPTION>");
        else              d.write("<OPTION VALUE=" + year + ">" + year + "</OPTION>");
    }
    d.write("</SELECT></TD>");
    d.write("<td align=right valign=middle width=15%><a href=\"javascript:top.opener.fSkipforward('"+strDateField+"','"+strFunc+"');\"><img src="+top.g_strImgDir+"APWINXT.gif border=0></a>");
    d.write("</td></tr></table>");
    d.write("<TABLE CELLSPACING=0 CELLPADDING=0 BORDER=0><TR><TD height=5></td></tr><TR>");

    d.write("<TD class=weekdaycell><img src="+top.g_strImgDir+"APWCALTL.gif></td>");
    for (i=0; i<7; i++)
        d.write("<TD WIDTH=35  class=weekdaycell ALIGN=CENTER VALIGN=MIDDLE>" + dow[i] +"</TD>");
    d.write("<TD class=weekdaycell><img src="+top.g_strImgDir+"APWCALTR.gif></td>");

    d.write("</TR><TR ALIGN=CENTER VALIGN=MIDDLE><TD class=monthdaycell><img src="+top.g_strImgDir+"APWDBPXW.gif></td>");

    var column = 0;
    var row=1;
    var lastMonth = Month - 1;
    if (lastMonth == -1) lastMonth = 11;
    for (i=0; i<startDay; i++, column++)
        d.write("<TD WIDTH=35 HEIGHT=20 class=monthdaycell><a class=disableddaylink>" + (days[lastMonth]-startDay+i+1) + "</a></TD>");

    for (i=1; i<=days[Month]; i++, column++) {
	if ((i==top.g_dToday.getDate()) && (Month == top.g_dToday.getMonth()) && (Year == top.g_dToday.getFullYear()))
	 d.write("<TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=daylinkred HREF=\"javascript:top.opener.top.fChangeDay(" + i + ",'"+strDateField+"','"+strFunc+"'); \">" + i + "</A></TD>");
	else
        d.write("<TD WIDTH=35 HEIGHT=20 class=monthdaycell><A class=daylink HREF=\"javascript:top.opener.top.fChangeDay(" + i + ",'"+strDateField+"','"+strFunc+"');\">" + i + "</A></TD>");
        if (column == 6) {
		column = -1;
		d.write("<TD class=monthdaycell><img src="+top.g_strImgDir+"APWDBPXW.gif></TD></TR>");
		if (i<days[Month]) {
			row ++;
            	d.write("<TR ALIGN=CENTER VALIGN=MIDDLE><TD class=monthdaycell><img src="+top.g_strImgDir+"APWDBPXW.gif></TD>");
		}
        }
    }
    if (column > 0) {
        for (i=1; column<7; i++, column++) {
            d.write("<TD WIDTH=35 HEIGHT=20 class=monthdaycell><a class=disableddaylink>" + i + "</a></TD>");
	  }
	  d.write("<TD class=monthdaycell><img src="+top.g_strImgDir+"APWDBPXW.gif></TD></TR>");
    }

    d.writeln("<TR><TD class=monthdaycell><img src="+top.g_strImgDir+"APWCALBL.gif></td><td class=monthdaycell colspan=7><img src="+top.g_strImgDir+"APWDBPXW.gif></td><td class=monthdaycell><img src="+top.g_strImgDir+"APWCALBR.gif></td></tr>");

	while (row < 6) {
		d.writeln("<tr><td colspan=9 height=20></td></tr>");
		row++; }

    d.writeln("</TABLE>");

   d.writeln("</td><td width=4 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif width=4></td><td width=1 class=colorg4><img src="+top.g_strImgDir+"APWDBPX9.gif></td></tr>");
   d.writeln("<TR class=colorg5><TD ROWSPAN=2 colspan=2><IMG SRC="+top.g_strImgDir+"APWCTBL.gif></TD><TD HEIGHT=4 class=colorg5><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif></TD>");
   d.writeln("<TD ROWSPAN=2 colspan=2><IMG SRC="+top.g_strImgDir+"APWCTBR.gif></TD></TR><TR><TD height=1 class=colorg3><img src="+top.g_strImgDir+"APWDBPX6.gif></td></tr>");
   d.writeln("<tr><td height=3></td></TR></table>");

   d.writeln("<table border=0 width=100%><tr><td align=right valign=middle>");
   d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0><TR><TD ROWSPAN=5><IMG SRC="+top.g_strImgDir+"APWBRNDL.gif></TD><TD></TD>");
   d.writeln("<TD ROWSPAN=5><IMG SRC="+top.g_strImgDir+"APWBRNDR.gif></TD><TR><TD CLASS=COLOR6><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD></TR>");
    d.writeln("<TR><TD CLASS=COLORG5 HEIGHT=20 NOWRAP><A HREF=\"\" onClick=\"parent.calopen==false; window.close();\"><FONT CLASS=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CLOSE').mstrGetPrompt()+"</FONT></TD></TR>");
    d.writeln("<TR><TD CLASS=COLORG3><IMG SRC="+top.g_strImgDir+"APWDBPX6.gif></TD></TR><TR><TD></TD></TR></TABLE>");


    d.writeln("</td></tr></table>");

   d.writeln("</FORM>");
   d.writeln("</BODY>");
   d.writeln("</HTML>");
   d.close();
  }
}


function fChangeDay(day,strDateField,strFunc) {
    	top.g_strDay = day + '';
	var dateField = eval(strDateField);
       	var pickedDay = '' + fPadout(top.g_strDay) + '/' + fPadout(top.g_strMonth - 0 + 1) + '/' + top.g_strYear;
	var objTempDate = top.fStringToDate_DDMMRRRR(pickedDay);
	dateField.value = top.fDateToString(objTempDate);
	eval(strFunc);
	top.g_winCal.close();

	
}

// Move logic from fChangeDay to here
function fOnClickDate(nLine){
	if (top.fIsOOP()) fUpdateLineStatus(nLine,true);
	var dateField = eval('top.framMain.lines.document.data.txtDate'+nLine);
	var iRecIndex = top.fGetObjExpReport().getNthReceipt(nLine);
	r = top.fGetReceiptObj(iRecIndex);
	var objTempDate = top.fStringToDate(dateField.value);
	top.framMain.lines.fChangeDate(top.fDateToString(objTempDate),nLine);
	
	r.setDate(top.fDateToString(objTempDate));
	//if (top.fIsOOP()) fUpdateLineStatus(nLine,true);
	
	if (fGetObjDetail() !=null)
	  if (!fGetObjDetail().closed)
		fGetObjDetail().fDetailSetDate(dateField.value);
	
        if ((g_winCal != null) && (!g_winCal.closed)) g_winCal.close();
}

function fChangeMonth(strDateField,strFunc) {
    var tempCalWin = null;
    if (g_winCal) tempCalWin=g_winCal;

    top.g_strMonth = tempCalWin.document.Cal.Month.options[tempCalWin.document.Cal.Month.selectedIndex].value + '';
    
    if (strFunc)
      tempCalWin.href = Calendar(top.g_strMonth,top.g_strYear,strDateField,strFunc);
    else
      tempCalWin.href = Calendar(top.g_strMonth,top.g_strYear,strDateField,'');
}

function fChangeYear(strDateField,strFunc) {
    var tempCalWin = null;
    if (g_winCal) tempCalWin=g_winCal;

    top.g_strYear = tempCalWin.document.Cal.Year.options[tempCalWin.document.Cal.Year.selectedIndex].value + '';
    tempCalWin.href = Calendar(top.g_strMonth,top.g_strYear,strDateField,strFunc);
}

function fCalMakeArray() {
    for (i = 0; i<fCalMakeArray.arguments.length; i++)
        this[i] = fCalMakeArray.arguments[i];
}

function fSkipback(strDateField,strFunc) { 
    var tempCalWin = null;
    if (g_winCal) tempCalWin=g_winCal;

	var monthIndex=tempCalWin.document.Cal.Month.selectedIndex;
	var yearIndex= tempCalWin.document.Cal.Year.selectedIndex;
	if (monthIndex == 0 ) {
		if (yearIndex == 0) return; 
		else 	{yearIndex = yearIndex - 1;
			 monthIndex = 11;
			} 
	}
	else { monthIndex = monthIndex - 1; }
	tempCalWin.document.Cal.Month.selectedIndex=monthIndex;
	tempCalWin.document.Cal.Year.selectedIndex=yearIndex;
	top.g_strMonth = tempCalWin.document.Cal.Month.options[tempCalWin.document.Cal.Month.selectedIndex].value + '';
	top.g_strYear = tempCalWin.document.Cal.Year.options[tempCalWin.document.Cal.Year.selectedIndex].value + '';

	Calendar(top.g_strMonth,top.g_strYear,strDateField,strFunc);
}

function fSkipforward(strDateField,strFunc) { 
    var tempCalWin = null;
    if (g_winCal) tempCalWin=g_winCal;

	var monthIndex=tempCalWin.document.Cal.Month.selectedIndex;
	var yearIndex= tempCalWin.document.Cal.Year.selectedIndex;
	if (monthIndex == 11 ) {
		if (yearIndex == 200) return; 
		else 	{yearIndex = yearIndex + 1;
			 monthIndex = 0;
			} 
	}
	else { monthIndex = monthIndex + 1; }
	tempCalWin.document.Cal.Month.selectedIndex=monthIndex;
	tempCalWin.document.Cal.Year.selectedIndex=yearIndex;
	top.g_strMonth = tempCalWin.document.Cal.Month.options[tempCalWin.document.Cal.Month.selectedIndex].value + '';
	
	top.g_strYear = tempCalWin.document.Cal.Year.options[tempCalWin.document.Cal.Year.selectedIndex].value + '';
	
	tempCalWin.href = Calendar(top.g_strMonth,top.g_strYear,strDateField,strFunc);
}

function fPadout(number) { return (number < 10) ? '0' + number : number; }


/* --------------------------
   Expense Type Functions
   -------------------------- */
function fGetExpTypeName(strID){
   for (var i=0;i<top.expenseTemplateObject.arrAllExpenseType.length;i++){
      if (top.expenseTemplateObject.arrAllExpenseType[i].mGetID()+"" == strID+""){
       if (top.expenseTemplateObject.arrAllExpenseType[i].mGetstrName()!=null)
        return top.expenseTemplateObject.arrAllExpenseType[i].mGetstrName();
       else 
	return "";
      }
   }
   return "";
}

function fGetExpTypeCode(strID){
   for (var i=0;i<top.expenseTemplateObject.arrAllExpenseType.length;i++){
      if (top.expenseTemplateObject.arrAllExpenseType[i].mGetID()+"" == strID+""){
       if (top.expenseTemplateObject.arrAllExpenseType[i].mGetstrCode()!=null)
        return top.expenseTemplateObject.arrAllExpenseType[i].mGetstrCode();
       else 
	return "";
      }
   }
   return "";
}

function fGetExpTypeID(strExpenseType){
 var tempObj = top.fGetObjHeader().getObjExpTemplate();
 if (tempObj){
    for (var i=0;i<top.expenseTemplateObject.arrAllExpenseType.length;i++){
    if (top.expenseTemplateObject.arrAllExpenseType[i].mGetstrName() == strExpenseType){
	for (var j=0;j<tempObj.arrExpTypes.length;j++)
	  if (tempObj.arrExpTypes[j].mGetID() == top.expenseTemplateObject.arrAllExpenseType[i].mGetID())
		return tempObj.arrExpTypes[j].mGetID();
      }
   }
  }
   return "";
}


function fGetExpTemplateIndex(strExpTemplate){
  for (var i=0;i<top.g_arrExpenseTemplate.length;i++){
     if (g_arrExpenseTemplate[i].id == strExpTemplate) return i;
  }
  return 0;
}


function fGetETypeIndex(strID,strExpType){
    var objTemp = top.fGetObjExpReport().header.getObjExpTemplate();
     if (strID){ 
	if (objTemp)
    	  for (var i=0;i<objTemp.arrExpTypes.length;i++){
      		if (objTemp.arrExpTypes[i].mGetID()+"" == strID +""){
        	  return i-1;
      		}
    	  }
    	return 0;
    }
    else if (strExpType){
	if (objTemp)
    	  for (var i=0;i<top.expenseTemplateObject.arrAllExpenseType.length;i++){
      		if ((top.expenseTemplateObject.arrAllExpenseType[i].mGetstrName()+"") == (strExpType +""))
		{
		  for (var j=0;j<objTemp.arrExpTypes.length;j++){
		  if (objTemp.arrExpTypes[j].mGetID()+"" == top.expenseTemplateObject.arrAllExpenseType[i].mGetID())
		   {
		    return j-1;
		   }
		  }// inner for loop
      		} 
    	  }// for loop
    	  return 0;	
    }// if (strExpType)
    else return 0;
}


function fGetRequireReceiptAmount(strExpenseType){
    var tempObj = top.fGetObjHeader().getObjExpTemplate();
 if (tempObj){
   for (var i=0;i<top.expenseTemplateObject.arrAllExpenseType.length;i++){
	if (top.expenseTemplateObject.arrAllExpenseType[i].mGetstrName() == strExpenseType + ""){
	 for (var j=0;j<tempObj.arrExpTypes.length;j++)
	   if (tempObj.arrExpTypes[j].mGetID() == top.expenseTemplateObject.arrAllExpenseType[i].mGetID()){
	   return top.expenseTemplateObject.arrAllExpenseType[i].mGetRequireReceiptAmount();
	  }
      }
    }
  }
    return null;
}

function fGenExpTemplatePoplist(strTemplateID){
   var strResult = "";
   var objExpTypeID = null;
   strResult += "<SELECT name=popExpTemplate onChange = ''javascript:var d=eval(top.framMain.header.document.reportHeaderForm.popExpTemplate;top.fChangeExpTemplate(d.options[d.selectedIndex].value);''>";
   for (var i=0;i<top.g_arrExpenseTemplate.length;i++){
	  strResult += "<option value=" + top.g_arrExpenseTemplate[i].id;
	  if (strTemplateID == top.g_arrExpenseTemplate[i].id)
	    strResult += " SELECTED";
	  strResult +=  " > " +  top.g_arrExpenseTemplate[i].mstrGetTemplateName();
   }
   strResult += "</SELECT>";
   return strResult;
}

/* --------------------
   Print Functions
   -------------------- */

function fPrintFrame(wind) {
        var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))
	var Win32
	if (Nav4) {
        Win32 = ((navigator.userAgent.indexOf("Win") != -1) && (navigator.userAgent.indexOf("Win16") == -1))
	} 
	else {
        Win32 = ((navigator.userAgent.indexOf("Windows") != -1) && (navigator.userAgent.indexOf("Windows 3.1") == -1))
	}
        // no single frame printing available for Mac
        if (Win32) {
                if (Nav4) {
                        window.print()
                } else {
                        // traps all script error messages hereafter until page reload
                        window.onerror = printErr
                        // make sure desired frame has focus
                        wind.focus()
                        // change second parameter to 2 if you don't want the
                        // print dialog to appear
                        IEControl.ExecWB(6, 1)
                }
        } else {
                alert("Sorry. Printing is available only from Windows 95/98/NT.")
        }
}


function fGenSaveFrame(){
var targetFrame = eval("top.framSaveReport");
var d = eval("targetFrame.document");
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\"><HEAD>");
	top.fGenerateStyleCodes(top.framSaveReport);

	d.writeln("<SCRIPT LANGUAGE=Javascript>");


d.writeln("function fGetLastReceiptDate() {");
d.writeln("	var lastReceiptDate;");
d.writeln("	for ( var i = 0; i < top.fGetObjExpReport().oopReceipts.length; i++ ) {");
d.writeln("	  if (top.fGetObjExpReport().oopReceipts[i])");
d.writeln("		if (top.fGetObjExpReport().oopReceipts[i].getDate()) {" );
d.writeln("			var dateObj = top.fStringToDate(top.fGetObjExpReport().oopReceipts[i].getDate(), false);");
d.writeln("			if ( lastReceiptDate ) {");
d.writeln("				if ( dateObj > lastReceiptDate ) ");
d.writeln("					lastReceiptDate = dateObj;");
d.writeln("			}");
d.writeln("			else {");
d.writeln("				lastReceiptDate = dateObj;");
d.writeln("			}");
d.writeln("		}");
d.writeln("	}");
d.writeln("	for ( var i = 0; i < top.fGetObjExpReport().cCardReceipts.length; i++ ) {");
d.writeln("	  if (top.fGetObjExpReport().cCardReceipts[i])");
d.writeln("		if (top.fGetObjExpReport().cCardReceipts[i].getDate()) {" );
d.writeln("			var dateObj = top.fStringToDate(top.fGetObjExpReport().cCardReceipts[i].getDate(), false);");
d.writeln("			if ( lastReceiptDate ) {");
d.writeln("				if ( dateObj > lastReceiptDate ) ");
d.writeln("					lastReceiptDate = dateObj;");
d.writeln("			}");
d.writeln("			else {");
d.writeln("				lastReceiptDate = dateObj;");
d.writeln("			}");
d.writeln("		}");
d.writeln("	}");
d.writeln("	if ( lastReceiptDate ) ");
d.writeln("		return top.fDateToString(lastReceiptDate);");
d.writeln("	else");
d.writeln("		return '';");
d.writeln("}");


     d.writeln("function fSetReceiptParseString(objR){");
     d.writeln("var result = \"\";"); 
     d.writeln("result += objR.getDate() + \"@att@\";");

     // for bug#1295684
     d.writeln("if ((objR.getOccurs() != 1) && (objR.getDate()!=\"\")){");
     d.writeln(" var tmpobj = top.fStringToDate(objR.getDate(), false);");
     d.writeln(" tmpobj.setDate(tmpobj.getDate() + eval(objR.getOccurs()) -1 );");
     d.writeln(" result += top.fDateToString(tmpobj);");
     d.writeln(" }");
     d.writeln("result += \"@att@\"; //for xdate2");

     d.writeln("result += objR.getOccurs() + \"@att@\"; ");
     d.writeln("result += objR.getDailyRate()+ \"@att@\"; ");
     d.writeln("result += objR.getReceiptAmount() + \"@att@\";");
     d.writeln("result += objR.getExchRate() + \"@att@\";");
     d.writeln("result += top.fMoneyFormat(objR.getReimbursAmount()) + \"@att@\";");
     d.writeln("result += objR.getGroup() + \"@att@\";");
     d.writeln("result += objR.getJustification() + \"@att@\";");
     d.writeln("if ((objR.isMissing == 'Y')||(objR.isMissing == true)) result += \"Y@att@\";");
     d.writeln("else result += \"N@att@\";");

     d.writeln("if (objR.getObjExpenseType())");
     d.writeln("  result += objR.getObjExpenseType().mGetID() + \"@att@\";");
     d.writeln("else");
     d.writeln("result += \"null@att@\";");

     d.writeln("result += objR.getCurrency() + \"@att@\";");
     d.writeln("if (objR.itemizeId) result += objR.itemizeId + \"@att@\";");
     d.writeln("else result += \"null@att@\";");
     d.writeln("result += objR.getcCardTrxnId() + \"@att@\";");
     d.writeln("result += objR.getMerchant() + \"@att@\";");
     d.writeln("if (top.g_bEnableNewTaxFields == 'Y'){");
     d.writeln("result += objR.getDocNum() + \"@att@\";");
     d.writeln("result += objR.getMerchRef() + \"@att@\";");
     d.writeln("result += objR.getTaxRegNum() + \"@att@\";");
     d.writeln("result += objR.getTaxPayerID() + \"@att@\";");
     d.writeln("result += objR.getSupCountry() + \"@att@\";");
     d.writeln("}");
     d.writeln("else{");
     d.writeln("result += \"@att@\";");
     d.writeln("result += \"@att@\";");
     d.writeln("result += \"@att@\";");
     d.writeln("result += \"@att@\";");
     d.writeln("result += \"@att@\";");
     d.writeln("}");
     d.writeln("result += objR.getTaxCodeID() + \"@att@\";");
     d.writeln("result += objR.getOverrideFlag() + \"@att@\";");
     d.writeln("result += (objR.getAmountIncludesTax()?'Y':'N') + \"@att@\";");

     d.writeln("result += top.fSerializeDFlex(objR);");

     d.writeln("if (top.fGetObjHeader().getObjEmployee().mGetbIsProjectEnabled()){  ");
     d.writeln("	  result += objR.getProjectNumber() + \"@att@\";  // project number");
     d.writeln("  result += objR.getTaskNumber() + \"@att@\";  // task number");
	  
	 
     d.writeln("  result += \"Y\" + \"@att@\"; // is receipt visited");
	d.writeln("  result += \"@att@@att@@att@@att@@line@\";");
	d.writeln("}");
	d.writeln("else");
  	d.writeln("  result += \"Y@att@@att@@att@@att@@att@@line@\"; ");
     d.writeln(" return result;");
     d.writeln("  }");

d.writeln("function fSetParseString(iReceiptIndex,objTargetField) {");
d.writeln("  objTargetField.value = null;");
d.writeln("  var h = top.fGetObjHeader();");
d.writeln("  var result = null;");
d.writeln("  var arrParse = new Array();");
d.writeln("        result = h.reportHeaderID + \"@att@\" + ");
d.writeln("	top.g_strSysDate + \"@att@\" +");
d.writeln("	h.getApproverID() + \"@att@\"+");
d.writeln("	h.getApprover() + \"@att@\" +");
d.writeln("	h.getCostCenter() + \"@att@\" +");
d.writeln("	h.getObjEmployee().mGetstrEmployeeID() + \"@att@\" +");
d.writeln("	h.getExpTemplateID() + \"@att@\" +");
d.writeln("	h.getExpTemplateID() + \"@att@\" +");

d.writeln("	fGetLastReceiptDate() + \"@att@\" +");
//d.writeln("	top.g_strSysDate +\"@att@\" +");

d.writeln("	h.getReimbursCurr() + \"@att@\" +");
d.writeln("	top.g_arrCurrency[top.fGetCurrencyIndex(h.reimbursCurr)].mGetName() + \"@att@\" +");
d.writeln("	top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_NON_BASE_REIMB') + \"@att@\" +");

d.writeln("	h.getPurpose() + \"@att@\" +");
d.writeln("	top.g_iNumMaxFlexField + \"@att@\" + ");
d.writeln("	h.getAmtDueEmployee() + \"@att@\" + ");
d.writeln("	h.getAmtDueccCompany() + \"@att@\" + \"@line@\";");

    
/* ----------------------
    Receipt Information
   ---------------------- */

d.writeln("  if (!iReceiptIndex>=0){");
d.writeln("  for (var j=0; j<top.fGetObjExpReport().oopReceipts.length; j++) {");
d.writeln("   if (top.fGetObjExpReport().oopReceipts[j]){    ");   
d.writeln("         arrParse[arrParse.length] = \"\"+fSetReceiptParseString(top.fGetObjExpReport().oopReceipts[j]);       }");
d.writeln("   } // end of for loop");

d.writeln("  for (var j=0; j<top.fGetObjExpReport().cCardReceipts.length; j++) {");
d.writeln("   if (top.fGetObjExpReport().cCardReceipts[j]){      ");
d.writeln("         arrParse[arrParse.length] = \"\"+fSetReceiptParseString(top.fGetObjExpReport().cCardReceipts[j]);");

d.writeln("    }");
d.writeln("  }// end of for loop");
d.writeln(" }// (!iReceiptIndex>=0)");
d.writeln(" else{");
d.writeln("	if (top.fGetReceiptObj(iReceiptIndex))");
d.writeln("     arrParse[arrParse.length] = \"\"+fSetReceiptParseString(top.fGetReceiptObj(iReceiptIndex));");
	
d.writeln(" }//iReceiptIndex>=0");
d.writeln("	if (iReceiptIndex>=0){");
d.writeln("	   document.formCalculateAmount.ParseThis.value = \"\"+result;");
d.writeln("	   for (var i=0;i<arrParse.length;i++){");
d.writeln(" 	  	document.formCalculateAmount.ParseThis.value += arrParse[i];");
d.writeln("	   }");
d.writeln("	}");
d.writeln("	else{");
d.writeln("	   document.formServerSerialize.ParseThis.value = \"\"+result;");
d.writeln("	   for (var i=0;i<arrParse.length;i++){");
d.writeln(" 	  	document.formServerSerialize.ParseThis.value += arrParse[i];");
d.writeln("	   }");
d.writeln("	}");
d.writeln("}");

	d.writeln("<\/SCRIPT>");
    	d.writeln("</HEAD>");
	d.writeln("<BODY class=color3 onFocus=\"top.fCheckModal();\">");
	d.writeln("<FORM name=\"formServerSerialize\" ACTION=\""+top.g_dcdName+"/AP_WEB_SUBMIT_PKG.saveOrSubmit\" target=\"framSaveReport\" METHOD=\"POST\" resizable=no menubar=no width=400 height=400 scrolling = auto location=no>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"ParseThis\" cols=60 rows=60>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"p_savedCategories\" cols=60 rows=10>");
	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"p_personalTrxns\" cols=60 rows=10>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"P_EmployeeID\" value=0>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"p_action\">");
	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"CalculateReceiptIndex\" VALUE=\"-1\">");
	d.writeln("</FORM>");

	d.writeln("<FORM name=\"formSaveCategory\" ACTION=\""+top.g_dcdName+"/AP_WEB_PARENT_PKG.saveCategory\" target=\"framSaveReport\" METHOD=\"POST\" resizable=no menubar=no width=400 height=400 scrolling = auto location=no>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"p_savedCategories\" cols=60 rows=10>");
    	d.writeln("</FORM>");
    	
	d.writeln("<FORM name=\"formCalculateAmount\" ACTION=\""+top.g_dcdName+"/AP_WEB_SUBMIT_PKG.saveOrsubmit\" target=\"framSaveReport\" METHOD=\"POST\" resizable=no menubar=no width=400 height=400 scrolling = auto location=no>");
	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"ParseThis\" cols=60 rows=60 value=\"\">");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"P_EmployeeID\" value=0>");
    	d.writeln("<INPUT TYPE=\"hidden\" NAME=\"CalculateReceiptIndex\" VALUE=\"-1\">");
    	d.writeln("</FORM>");
 	
    	d.writeln("</BODY></HTML>");
	d.close();
}

function updateButtonFrame(strTab) {
   var temp = strTab;
   if (!strTab) 
       if (fIsOOP())
           temp = 'OOP';
       else
           temp = 'CC';
   
   var nTotalReceipts = objExpenseReport.getTotalReceipts(temp);
   var nTotalAmount = fMoneyFormat(objExpenseReport.calculateTotal(temp), objExpenseReport.header.getReimbursCurr());
   framMain.framButtons.setTotalInfo(nTotalReceipts, nTotalAmount);
   if (temp == 'OOP'){
        if (nTotalReceipts > 0){
            if (top.framMain.framButtons) 
		top.framMain.framButtons.setEnabledOOP();
	}
        else{
            if (top.framMain.framButtons)
		top.framMain.framButtons.setDisabledOOP();
	}
   }
   else {
        if (top.framMain.framButtons) top.framMain.framButtons.setCreditCard();
   }
}


function fGenOOPFrameset(bUserHasCC,isInitial, isNavBack){
var strReturn = "";
    if (bUserHasCC){
    strReturn +="<frameset rows=\"155,50,*,40\" border=0 framespacing=0 frameborder=no onLoad=\"\">";
    strReturn +="			<frame";
    strReturn +="            			src=\"javascript:parent.top.fGenHeaderFrame(true)\"";
    strReturn +="            			name=header";
    strReturn +="            			marginwidth=0";
    strReturn +="                            	frameborder=no";
    strReturn +="                            	framespacing=0";
    if (top.SunOS)
       strReturn +="                                    scrolling=auto";
    else
       strReturn +="                                    scrolling=no";
    strReturn +="				noresize>";
    strReturn +="                    	<frame";
    if (fIsOOP())
        strReturn +="                            	src=\"javascript:parent.top.fGenTabs('OOP')\"";
    else
        strReturn +="                            	src=\"javascript:parent.top.fGenTabs('CC')\"";

    strReturn +="                            	name=tabs";
    strReturn +="                            	marginwidth=0";
    strReturn +="                            	frameborder=no";
    strReturn +="                            	framespacing=0";
    strReturn +="                            	scrolling=no";
    strReturn +="				noresize>";
    strReturn +="                    	<frame";
    if (fIsOOP())
        strReturn +="                            	src=\"javascript:top.fCreateMoreLines(0,"+isInitial+")\"";
    else
        strReturn +="                            	src=\"javascript:top.fDrawCCardLines(false)\"";

    strReturn +="                            	name=lines";
    strReturn +="                            	marginwidth=0";
    strReturn +="                            	frameborder=no";
    strReturn +="                            	framespacing=0";
    strReturn +="                            	scrolling=auto";
    strReturn +="				noresize>";
    strReturn +="                    	<frame";
    if ((!isNavBack) || (!(isWindows95 && isIE5))) strReturn +="                            	src=\"javascript:top.fDrawLinesBtn()\"";
    strReturn +="                            	name=framButtons";
    strReturn +="                            	marginwidth=0";
    strReturn +="                            	frameborder=no";
    strReturn +="                            	framespacing=0";
    strReturn +="                            	scrolling=no";
    strReturn +="				noresize>";
    strReturn +="            	</frameset> ";
}
    else{
    strReturn +="<frameset rows=\"155,*,40\" border=0 framespacing=0 onLoad=\"top.fDrawLinesBtn();\">";
    strReturn +="			<frame";
    strReturn +="            			src=\"javascript:parent.top.fGenHeaderFrame(false)\"";
    strReturn +="            			name=header";
    strReturn +="                            	frameborder=0";
    strReturn +="            			marginwidth=0";
    if (top.SunOS)
       strReturn +="                                    scrolling=auto";
    else
       strReturn +="                                    scrolling=no";
    strReturn +="				noresize>";
    strReturn +="                    	<frame";
    strReturn +="                            	src=\"javascript:parent.top.fCreateMoreLines(0,"+isInitial+")\"";
    strReturn +="                            	name=lines";
    strReturn +="                            	marginwidth=0";
    strReturn +="                            	frameborder=0";
    strReturn +="                            	scrolling=auto";
    strReturn +="				noresize>";
    strReturn +="                    	<frame";
    if (!(isWindows95 && isIE5)) strReturn +="                            	src=\"javascript:top.fBlankPage('336699')\"";
    strReturn +="                            	name=framButtons";
    strReturn +="                            	marginwidth=0";
    strReturn +="                            	frameborder=0";
    strReturn +="                            	scrolling=no noresize>";
    strReturn +="            	</frameset> ";
    }
return strReturn;

}

function fStop(){
	window.stop();
	fDrawToolbar(false,false,true,false,'parent.framMain.lines');
}

function fSaveOrSubmit(sAction){
    if ((sAction == 'save') && (top.g_strCurrentTab != 'finalReview'))
      if (!Nav4) fIEOnChangeTrigger();
 
    fGetAmtDueValues();  // to compute the amts in the header reports!

    var t = top.framSaveReport.document.formServerSerialize;
    var title = null;
     framSaveReport.fSetParseString(-1,eval(top.framSaveReport.document.formServerSerialize.ParseThis));
     t.P_EmployeeID.value = fGetObjHeader().getObjEmployee().mGetstrEmployeeID();
     t.p_savedCategories.value = g_objCCardData.getDataChanged();
     t.p_action.value = sAction;

     // BUG: 1479799
     if ((sAction=='save') || (sAction=='SilentSave')){
        if ((sAction=='SilentSave') ||
            ((sAction=='save') && (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_CONFIRM_SAVE"))))) {
	fDrawToolbar(true,true,false,true,'parent.framMain.lines');

        fSetOldReportObject();
	title = 'The following information must be corrected before entering Final Review page.';
	var strErrorContent = fRequiredFieldsEntered(title,false,-1)
        if (fDrawErrorWindow(title,strErrorContent)){ 
	    if (top.g_winError!=null) if (!top.g_winError.closed) top.g_winError.close();
          	top.framSaveReport.document.formServerSerialize.submit();
	}
	else{
            if (fGetObjDetail() != null)
	      if (!fGetObjDetail().closed)
		fGetObjDetail().focus();
	    fDrawToolbar(false, false, true,true,'parent.framMain.lines');
	}
	} // confirm
     }
     else{ // submit	
       if (top.g_reportSubmitted == 1){
         alert(top.g_objMessages.mstrGetMesg("AP_WEB_PROCESSING_IN_PROGRESS"));
         return;
       }
       else{
	 if (confirm(top.g_objMessages.mstrGetMesg("AP_WEB_CONFIRM_SUBMIT"))){
         top.g_reportSubmitted = 1;
	 fDrawToolbar(true,true,false,false,'parent.framMain.body'); 
	 if ( fGetPaymentScenario() != g_cIndividual)
             t.p_personalTrxns.value = g_objCCardData.packPersonalTrxns(g_selectedCardProg);
         else
             t.p_personalTrxns.value = "";
	     t.submit();
	 }
       } // top.g_reportSubmitted
     }
}

/*---------------------
     Navigation Frame    
  ---------------------*/
function fNavBack(){
	var d = eval(top.framMain.document);

        // BUG: 1479799.  After implicit save, will restore report
        if (isWindows95 && isIE5) {
           top.fSaveOrSubmit('SilentSave');
           return;
        }

	top.g_strCurrentTab = 'OOP'; 
        d.open();
	if (top.g_bCCEnabled){
	    d.writeln(fGenOOPFrameset(true,false,true));
	    if (isWindows95 && isIE5){
	      top.fGenHeaderFrame(true);
	      top.fGenTabs('OOP');
	      top.fCreateMoreLines(0,false);
		top.fDrawLinesBtn();
	    }
	}
	else{
            d.writeln(fGenOOPFrameset(false,false,true));
	    if (isWindows95 && isIE5){
	      top.fGenHeaderFrame(true);
	      top.fCreateMoreLines(0,false);
		top.fDrawLinesBtn();
	    }
	}
        d.close();
	top.framNavigation.src = top.fDrawNavFrame("multiRow");
	fDrawToolbar(false,false,true,false,'parent.framMain.lines');
	fGenSaveFrame();
}

function fCancelReport(){
   if (confirm(top.g_objMessages.mstrGetMesg('AP_WEB_CANCEL_REPORT'))) {
       if (g_objCCardData.isDataChanged()) {
           if (confirm(g_objMessages.mstrGetMesg('AP_WEB_CCARD_SAVE_CATEGORY'))){
               fSaveCategory();
               alert(g_objMessages.mstrGetMesg('AP_WEB_CCARD_CATEGORY_SAVED'));
           }
       }
       if (top.opener){
         window.parent.focus();
         window.close();
       }
       else location = g_dcdName+"/OracleApps.DMM";
   }
}

function fDrawNavFrame(strPage,byExpenseType){
	var d = eval(top.framNavigation.document);
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_EXP_NAVIGATION');
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
	d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
	d.writeln("<HEAD>");
	top.fGenerateStyleCodes(top.framNavigation);
	d.writeln("</HEAD>");
	if (strPage == "multiRow"){
	d.writeln("  <BODY onLoad=\"parent.g_bButtonsFrameLoaded=true; self.defaultStatus=top.OAW_message; return true;\" class=color3 onResize = \"history.go(0)\" onFocus=\"parent.top.fCheckModal();\">");
	d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
	d.writeln("    <TR><td height=5></td></TR>");
	d.writeln("    <TR>");
	d.writeln("    <TD align=right>");
	d.writeln("    <!-- button table for the lower buttons.  -->");
	d.writeln("    <table cellpadding=0 cellspacing=0 border=0>");
	d.writeln("      <tr>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("        <td></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("        <td width=15 rowspan=5></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("        <td></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBSQRR.gif></td>");
	d.writeln("        <td width=3 rowspan=5></td>");
	d.writeln("<!-- Where-you-are-bar structure -->");
	d.writeln("        <td rowspan=5 ><img src="+top.g_strImgDir+"APWBWHRL.gif></td>");
	d.writeln("        <td class=colorg2><img src="+top.g_strImgDir+"APWPXG2.gif></td>");
	d.writeln("        <td rowspan=5 ><img src="+top.g_strImgDir+"APWBWHRR.gif></td>");
	d.writeln("        <td width=3 rowspan=5></td>");
   	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBSQRL.gif></td>");
	d.writeln("        <td></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
        d.writeln("        <td width=10 rowspan=5></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("  <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln("  <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln("<!-- where-you-are-bar has no second edge color -->");
	d.writeln("  <td></td>");
	d.writeln("  <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("  <td class=colorg5 height=20 nowrap><a href=\"javascript:top.fCancelReport()\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"</font></td>");
	d.writeln("  <td class=colorg5 height=20 nowrap><A href=\"javascript:top.fGotoMainMenu();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_RETURN_MENU').mstrGetPrompt()+"</font></td>");
	d.writeln("  <td height=20 nowrap><font class=promptwhite><b>"+tempRegion.mobjGetRegionItem('AP_WEB_NEW_EXPREPORT').mstrGetPrompt()+"</b> &gt; "+tempRegion.mobjGetRegionItem('AP_WEB_FINAL_REVIEW').mstrGetPrompt()+"</font></td>");
	d.writeln("  <td class=colorg5 height=20 nowrap><a href=\"javascript:top.fFinalReview();\">");
	d.writeln("<font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_NEXT').mstrGetPrompt()+"</font></td>");
	d.writeln(" </tr>");
	d.writeln("  <tr>");
	d.writeln("  <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln("  <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln("<!-- second edge shadow for where-you-are bar is empty -->");
	d.writeln("  <td></td>");
	d.writeln("  <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("  <td></td>");
	d.writeln("  <td></td>");
	d.writeln("  <td class=color4><img src="+top.g_strImgDir+"APWPX4.gif></td>");
	d.writeln("    <td></td>");
	d.writeln("</tr>");
	d.writeln("  </table></td>");
	d.writeln("</tr>");
	d.writeln("</table>");
	d.writeln("<FORM name=\"buttonsForm\" onSubmit=\"return(false)\">");
	d.writeln("<TABLE width=100% height=100% cellspacing=0 cellpadding=0 border=0>");
	d.writeln("	<TR><td height=30></td></tr><tr>");
	d.writeln("	    <TD>");
	d.writeln("		<INPUT TYPE='hidden' NAME='Count' VALUE=' 0' SIZE=3>");
	d.writeln("		<INPUT TYPE='hidden' NAME='Total' VALUE=' 0.00' SIZE=12></TD></TR></TABLE></FORM>");
	d.writeln("</BODY>");
	d.writeln("</HTML>");
	}


	else if (strPage == "finalReview"){

	d.writeln("<body class=color3>");
	d.writeln("<form name=data>");
        d.writeln("<CENTER>");
        d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
        d.writeln("	<TR><td height=5></td></TR>");
        d.writeln("	<TR>");
        d.writeln("		<TD align=left>");
	 if (byExpenseType) {
	 d.writeln(			top.fDynamicButton(strAP_WEB_FR_VIEW_EG,
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						strAP_WEB_FR_VIEW_EG,
						'javascript:top.fShowWeeklySummary(false,false);',
						'US',
						false,
						false,
						'top.g_winFinalReview.finalReviewButtons'));
	 } else {
	 d.writeln(			top.fDynamicButton(strAP_WEB_FR_VIEW_ET,
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						strAP_WEB_FR_VIEW_ET,
						'javascript:top.fShowWeeklySummary(false,true);',
						'US',
						false,
						false,
						'top.g_winFinalReview.finalReviewButtons'));
	 }
        d.writeln("		</td>");


        d.writeln("		<TD align=right>");
        d.writeln("		<table cellpadding=0 cellspacing=0 border=0>");
        d.writeln("			<tr>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
        d.writeln("				<td></td>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
        d.writeln("				<td width=15 rowspan=5></td>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
        d.writeln("				<td></td>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBSQRR.gif></td>");
        d.writeln("				<td width=3 rowspan=5></td>");
        d.writeln("				<!-- Where-you-are-bar structure -->");
        d.writeln("				<td rowspan=5 ><img src="+top.g_strImgDir+"APWBWHRL.gif></td>");
        d.writeln("				<td class=colorg2><img src="+top.g_strImgDir+"APWPXG2.gif></td>");
        d.writeln("				<td rowspan=5 ><img src="+top.g_strImgDir+"APWBWHRR.gif></td>");
        d.writeln("				<td width=3 rowspan=5></td>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBSQRL.gif></td>");
        d.writeln("				<td></td>");
        d.writeln("				<td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
        d.writeln("				<td width=10 rowspan=5></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
        d.writeln("				<td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
        d.writeln("				<td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
        d.writeln("				<!-- where-you-are-bar has no second edge color -->");
        d.writeln("				<td></td>");
        d.writeln("				<td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
        d.writeln("				<td class=colorg5 height=20 nowrap><a href=\"javascript:top.fCancelReport();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"</font></td>");
        d.writeln("				<td class=colorg5 height=20 nowrap><a href=\"javascript:top.fNavBack();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_BACK').mstrGetPrompt()+"</font></td>");
        d.writeln("				<td height=20 nowrap><font class=promptwhite>"+tempRegion.mobjGetRegionItem('AP_WEB_NEW_EXPREPORT').mstrGetPrompt()+"&gt; <b>"+tempRegion.mobjGetRegionItem('AP_WEB_FINAL_REVIEW').mstrGetPrompt()+"</b></font></td>");
	d.writeln("				<td class=colorg5 height=20 nowrap><a href=\"javascript:top.fSaveOrSubmit('submit');\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_FR_SUBMIT').mstrGetPrompt()+"</font></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
        d.writeln("				<td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
        d.writeln("				<td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
        d.writeln("				<!-- second edge shadow for where-you-are bar is empty -->");
        d.writeln("				<td></td>");
        d.writeln("				<td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
        d.writeln("				<td></td>");
        d.writeln("				<td></td>");
        d.writeln("				<td class=color4><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("				<td></td>");
        d.writeln("			</tr>");
        d.writeln("		</table>");
        d.writeln("		</td>");
        d.writeln("	</tr>");
        d.writeln("</table>");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");

	}

	else if (strPage == "confirmation"){
	d.writeln("<body class=color3>");
	d.writeln("<CENTER>");
        d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
        d.writeln("	<TR><td height=5></td></TR>");
        d.writeln("	<TR>");
        d.writeln("		<TD align=left>");
	 
	d.writeln(			top.fDynamicButton(tempRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt(),
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						tempRegion.mobjGetRegionItem('AP_WEB_PRINT').mstrGetPrompt(),
						'javascript:top.framToolbar.fprintFrame(top.framMain.body);',
						'US',
						false,
						false,
						'top.g_winFinalReview.finalReviewButtons'));
	
        d.writeln("		</td>");
	d.writeln("		<TD align=right>");
	if (top.opener){ 
	  d.writeln(			top.fDynamicButton(tempRegion.mobjGetRegionItem('AP_WEB_RETURN_MENU').mstrGetPrompt(),
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						tempRegion.mobjGetRegionItem('AP_WEB_RETURN_MENU').mstrGetPrompt(),
						'javascript:parent.window.close();parent.window.opener.focus();',
						'US',
						false,
						false,
						'top.g_winFinalReview.finalReviewButtons'));
	}
	else{
	  d.writeln(			top.fDynamicButton(tempRegion.mobjGetRegionItem('AP_WEB_RETURN_MENU').mstrGetPrompt(),
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						tempRegion.mobjGetRegionItem('AP_WEB_RETURN_MENU').mstrGetPrompt(),
						'javascript:parent.location=\''+g_dcdName+'/OracleApps.DMM\';',
						'US',
						false,
						false,
						'top.g_winFinalReview.finalReviewButtons'));
	}
        d.writeln("		</td>");
	d.writeln("	</tr></table>");
	d.writeln("</CENTER>");
        d.writeln("</BODY></HTML>");

 	}
	d.close();

}



function fGetObjExpReport(){
  return top.objExpenseReport;
}


function fGetObjHeader(){
  return top.objExpenseReport.header;
}

function fGetObjDetail(){
  return top.g_winDetail;
}

function fIsDetailOpen(){
  if ((fGetObjDetail() != null) && (!fGetObjDetail().closed))
	return true;
  else return false;
}


//Function to draw the button frame for BOTH OOP and Credit Card tab
//The frame has 3 layers for buttons. One for credit card, and the other two are 
// for OOP. To display the desired buttons, use the provide API.
function fDrawLinesBtn()
{
var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_TAB_BUTTONS');
var objRegion2 = top.g_objRegions.mobjGetRegion('AP_WEB_OOP_TAB_BUTTONS');
var d = top.framMain.framButtons.document;
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\"><HEAD>");
d.writeln("<link rel=stylesheet type=text/css href=" + top.g_strCssDir + "apwscabo.css>");
d.writeln("<SCRIPT LANGUAGE=\"JavaScript\">");
d.writeln("var Nav4 = top.Nav4;");
d.writeln("var navTop = 12;");
d.writeln("var ieTop = 12;");
d.writeln("var objExpRep = top.objExpenseReport;");
d.writeln("var strLayDoc = top.g_layDoc;");
d.writeln("var strLaySty = top.g_laySty;");
d.writeln("var linePrompt = \"" + objRegion2.mobjGetRegionItem('AP_WEB_TOTAL_LINES').mstrGetPrompt() + "\";");
d.writeln("var totalPrompt = \"" + objRegion2.mobjGetRegionItem('AP_WEB_REPORT_TOTAL').mstrGetPrompt() + "\";");

d.writeln("function setTotalInfo(numLines, totalAmt) {");
d.writeln("     var msgText = linePrompt + totalPrompt + numLines + totalAmt + \" \"+objExpRep.header.getReimbursCurr();");
d.writeln("     var msg = \"<FONT CLASS='PROMPTBLACK'>\" + linePrompt + \"</FONT>\";");
d.writeln("	msg +=\"<FONT CLASS='DATABLACK'> \" + numLines + \"&nbsp;&nbsp;&nbsp;</FONT>\" ;");
d.writeln("     msg += \"<FONT CLASS='PROMPTBLACK'>\" + totalPrompt;");
d.writeln("     msg += \"</FONT><FONT CLASS='DATABLACK'> \" + totalAmt + \"&nbsp;&nbsp;&nbsp;</FONT>\";");
d.writeln("     msg += \"<FONT CLASS='DATABLACK'>\"+objExpRep.header.getReimbursCurr()+ '</FONT>';");
d.writeln("     var obj = eval(strLayDoc + '[\"totaltext\"]' + strLaySty); ");
d.writeln("     var objWidth=msgText.length * 7 + 10;");
d.writeln("     if (Nav4) {");
d.writeln("                obj.document.write(msg);");
d.writeln("                obj.document.close();");
d.writeln("                obj.left=self.innerWidth - objWidth;");
d.writeln("		   obj.top = navTop;");
d.writeln("     } else {");
d.writeln("                document.all[\"totaltext\"].innerHTML = msg;");
d.writeln("                obj.pixelLeft= document.body.clientWidth - objWidth;");
d.writeln("		   obj.pixelTop = ieTop;");
d.writeln("		   obj.visibility = 'visible';");
d.writeln("     }");
d.writeln("}");
d.writeln("function enableLayer(strLayerId, bVisible) {");
d.writeln("     var obj = eval(strLayDoc + '[\"'+ strLayerId +'\"]'+ strLaySty);");
d.writeln("     if (Nav4) ");
d.writeln("        if (bVisible) obj.visibility = 'show'; else  obj.visibility = 'hide';");
d.writeln("     else");
d.writeln("        if (bVisible) obj.visibility = 'visible'; else  obj.visibility = 'hidden';");
d.writeln("}");

d.writeln("function setCreditCard() {");
d.writeln("        enableLayer('cCardButtons', true);");
d.writeln("        enableLayer('disabledOOPButtons', false);");
d.writeln("        enableLayer('enabledOOPButtons', false);");
d.writeln("}");

d.writeln("function setDisabledOOP() {");
d.writeln("        enableLayer('cCardButtons',false);");
d.writeln("        enableLayer('disabledOOPButtons', true);");
d.writeln("        enableLayer('enabledOOPButtons', false);");
d.writeln("}");

d.writeln("function setEnabledOOP() {");
d.writeln("        enableLayer('cCardButtons',false);");
d.writeln("        enableLayer('disabledOOPButtons', false);");
d.writeln("        enableLayer('enabledOOPButtons', true);");
d.writeln("}");

d.writeln("<\/SCRIPT></HEAD>");
d.writeln("<BODY onLoad=\"parent.top.g_bButtonsFrameLoaded=true; self.defaultStatus=top.OAW_message; top.updateButtonFrame(); return true;\" class=color3 onFocus=\"top.fCheckModal();\">");
d.writeln("<TABLE width=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>");
d.writeln("<TR class=colorg5>");
d.writeln("<TD align=left valign=bottom><IMG SRC=" + g_strImgDir + "APWCTBL.gif height=5 width=5></TD>");
d.writeln("<TD><table width=100% cellpadding=0 cellspacing=0 border=0>");
d.writeln("    <tr><td rowspan=3 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=5 width=5></td>");
d.writeln("      <td colspan=3 class=colorg5 height=5 width=1000><img src=" + g_strImgDir + "APWPXG5.gif height=2></td>");
d.writeln("      <td rowspan=3 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=5 width=5></td></tr>");
d.writeln("    <tr><td class=colorg5 nowrap><img src=" + g_strImgDir + "APWPXG5.gif height=33 width=1></td>");
d.writeln("      <td class=colorg5></td>");
d.writeln("<!-- This cell contains the totals, and is justified to the right-->");
d.writeln("      <td class=colorg5 align=right nowrap>");
d.writeln("      <font class=promptblack>&nbsp; </font>");
d.writeln("      <font class=datablack>&nbsp;&nbsp;&nbsp;</font>");
d.writeln("      <font class=promptblack>&nbsp; </font>");
d.writeln("      <font class=datablack>&nbsp;&nbsp;&nbsp;</font></td></tr>");
d.writeln("    <tr><td colspan=3 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=1></td></tr>");
d.writeln("    </TABLE></TD>");
d.writeln("<TD align=right valign=bottom><IMG SRC=" + g_strImgDir + "APWCTBR.gif height=5 width=5></TD>");
d.writeln("</TR>");
d.writeln("</TABLE>");

d.writeln("<DIV ID=totaltext STYLE=\"position:absolute;\">&nbsp;</DIV>");

// Draw Credit Card Buttons Layer
if (Nav4) 
    d.writeln("<LAYER ID=cCardButtons top=10 left=10 width=100 height=30 z-index=1 visibility=hide\" >");
else
    d.writeln("<DIV ID=cCardButtons STYLE = \"position: absolute; top:10; left:10; width:100px; height: 30 px; z-index: 1; visibility:hidden\" >");
d.writeln("<table border=0 cellspacing=0 cellpadding=0>");
d.writeln("<tr><td>");
d.writeln("    <TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("    <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("     document.writeln(top.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_REMOVE_SELECTED_LINES').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion.mobjGetRegionItem('AP_WEB_REMOVE_SELECTED_LINES').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fRemoveReceipts(false,true);\",");
d.writeln("                   parent.v_lang,");
d.writeln("                   false,");
d.writeln("                   false,parent.buttonsFrame));");
d.writeln("    <\/SCRIPT></TD></TR></TABLE></td>");
d.writeln("    <td>&nbsp;</td>");
d.writeln("    <td>");
d.writeln("    <TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("    <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("    document.writeln(top.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_GET_CCARD_TRXNS').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion.mobjGetRegionItem('AP_WEB_GET_CCARD_TRXNS').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fOpenCCardTrxnWindow();\",");
d.writeln("                   parent.v_lang,");
d.writeln("                   false,");
d.writeln("                   false,parent.buttonsFrame));");
d.writeln("    <\/SCRIPT></TD></TR></TABLE></td>");
d.writeln("</tr>");
d.writeln("</table>");
if (Nav4) 
    d.writeln("</LAYER>");
else
    d.writeln("</DIV>");

// Draw disabled OOP buttons
if (Nav4) 
d.writeln("<LAYER ID=disabledOOPButtons top=10; left=10; width=100px; height=30 px; z-index=2; visibility:hide >");
else
d.writeln("<DIV ID=disabledOOPButtons STYLE = \"position: absolute; top:10; left:10; width:100px; height: 30 px; z-index: 2; visibility:hidden\" >");
d.writeln("<table border=0 cellspacing=0 cellpadding=0>");
d.writeln("	<tr><td>");
d.writeln("	<TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("	document.writeln(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_DUPLICATE').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                  \""+objRegion2.mobjGetRegionItem('AP_WEB_DUPLICATE').mstrGetPrompt()+"\",");
d.writeln(" 		   \"\",");
d.writeln("                   top.v_lang,");
d.writeln("                    false,");
d.writeln("                    true,top.buttonsFrame));");
d.writeln(" 	<\/SCRIPT></TD></TR></TABLE>");
d.writeln(" 	</td><td>&nbsp;</td><td>");
d.writeln(" <TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln(" 	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln(" 	document.writeln(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_CLEAR').mstrGetPrompt()+"\",");
d.writeln("                    \"APWBRNDL.gif\",");
d.writeln("                    \"APWBRNDR.gif\",");
d.writeln("                    \""+objRegion2.mobjGetRegionItem('AP_WEB_CLEAR').mstrGetPrompt()+"\",");
d.writeln(" 		  \"\",");
d.writeln("                    top.v_lang,");
d.writeln("                    false,");
d.writeln("                    true,top.buttonsFrame));");
d.writeln(" 	<\/SCRIPT></TD></TR></TABLE>");
d.writeln(" 	</td><td>&nbsp;&nbsp;</td><td>");

d.writeln("	<TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");

d.writeln("	document.writeln(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_MORE_LINES').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion2.mobjGetRegionItem('AP_WEB_MORE_LINES').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fCreateMoreLines(top.g_iInitialDisplayedLines,false);\",");
d.writeln("                   top.v_lang,");
d.writeln("                   false,");
d.writeln("                   false,top.buttonsFrame));");
d.writeln("	<\/SCRIPT></TD></TR></TABLE></td>");
d.writeln("</tr>");
d.writeln("</TABLE>");
if (Nav4) 
    d.writeln("</LAYER>");
else
    d.writeln("</DIV>");

// Draw enabled OOP buttons
if (Nav4)
	d.writeln("<LAYER ID=\"enabledOOPButtons\" top=10 left=10 width=100; height = 30; visibility=hidden;\" >");
else
d.writeln("<DIV ID=\"enabledOOPButtons\" STYLE = \"position: absolute; top:10; left:10; width:100px; height: 30 px; z-index: 1; visibility:'hidden';\" >");

d.writeln("<table border=0 cellspacing=0 cellpadding=0>");
d.writeln("<tr><td>");
d.writeln("	<TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("	document.write(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_DUPLICATE').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion2.mobjGetRegionItem('AP_WEB_DUPLICATE').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fDuplicateLines(false);\",");
d.writeln("                   top.v_lang,");
d.writeln("                   false,");
d.writeln("                   false,top.buttonsFrame));");
d.writeln("	<\/SCRIPT>");
d.writeln("	</TD></TR></TABLE></td>");
d.writeln("	<td>&nbsp;</td>");
d.writeln("	<td>");
d.writeln("	<TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("	document.write(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_CLEAR').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion2.mobjGetRegionItem('AP_WEB_CLEAR').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fClearLines();\",");
d.writeln("                   top.v_lang,");
d.writeln("                   false,");
d.writeln("                  false,top.buttonsFrame));");
d.writeln("	<\/SCRIPT>");
d.writeln("	</TD></TR></TABLE></td>");
d.writeln("	<td>&nbsp;&nbsp;</td>");
d.writeln("	<td>");
d.writeln("	<TABLE BORDER=0 cellspacing=0 cellpadding=0><TR align=LEFT valign=CENTER><TD valign=CENTER>");
d.writeln("	<SCRIPT LANGUAGE=\"JAVASCRIPT\">");
d.writeln("	document.write(parent.top.fDynamicButton(\""+objRegion2.mobjGetRegionItem('AP_WEB_MORE_LINES').mstrGetPrompt()+"\",");
d.writeln("                   \"APWBRNDL.gif\",");
d.writeln("                   \"APWBRNDR.gif\",");
d.writeln("                   \""+objRegion2.mobjGetRegionItem('AP_WEB_MORE_LINES').mstrGetPrompt()+"\",");
d.writeln("		   \"javascript:top.fCreateMoreLines(top.g_iInitialDisplayedLines,false);\",");
d.writeln("                   top.v_lang,");
d.writeln("                   false,");
d.writeln("                   false,top.buttonsFrame));");
d.writeln("	<\/SCRIPT></TD></TR></TABLE>");
d.writeln("	</td></tr></table>");
if (Nav4) d.writeln("</LAYER>");
else d.writeln("</DIV>");

d.writeln("</BODY>");
d.writeln("</HTML>");
        d.close();
}


function fShowSavingConfirm(){
  var strMsg = top.g_objMessages.mstrGetMesg('AP_WEB_EXP_REPORT_SAVE');
  strMsg = strMsg.replace(/&INVOICENUM/,g_strRepNum_prefix + objExpenseReport.header.reportHeaderID);
  var nTotal = top.objExpenseReport.calculateTotal('OOP');
  if (g_bCCEnabled) nTotal += top.objExpenseReport.calculateTotal('CC');

  strMsg = strMsg.replace(/&REPORTTOTAL/,fMoneyFormat(nTotal,top.objExpenseReport.header.getReimbursCurr()));
  alert(strMsg);
}

function fCalcReimbursAmount(iRecIndex) {
   var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
   var l_reimbcurr_precision =0;
   var objR = fGetReceiptObj(iRecIndex);
   var reimb_amount = 0;
   if (objR){
   var action = top.fDetermineConversion(objR.getReceiptCurr(), fGetObjHeader().getReimbursCurr(),objR);



   if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;

   if (action == g_reimbEqualsRec) {
	 if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y')
     	   reimb_amount = fRound( objR.getExpensedAmount() / objR.getExchRate(), l_reimbcurr_precision);
   	 else
           reimb_amount = fRound( fMultiply(objR.getExpensedAmount(), objR.getExchRate()), l_reimbcurr_precision);
    } 
    else if (action == g_reimbEurRecFixed) {
         fixed_rate = fGetFixedRate(objR.getReceiptCurr());
         reimb_amount = eval(objR.getExpensedAmount()) / eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecEur) {
         fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         reimb_amount = eval(objR.getExpensedAmount()) *  eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecFixed) {
         var rate1 = fGetFixedRate(objR.getReceiptCurr());
         var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         reimb_amount = fMultiply((eval(objR.getExpensedAmount())/rate1), rate2);
            }
    }
    return reimb_amount;
}

var g_oldObjReport = null;

function fIsDataChanged(){
  var r = fGetObjExpReport();
  var o = g_oldObjReport;
  if (o){
   if (r.header.getObjEmployee().strEmployeeName != o.header.getObjEmployee().strEmployeeName) return true;
   if (r.header.getObjEmployee().strEmployeeID != o.header.getObjEmployee().strEmployeeID) return true;
   if (r.header.getObjEmployee().strCostCenter != o.header.getObjEmployee().strCostCenter) return true;
   if (r.header.getObjEmployee().bProjectEnabled != o.header.getObjEmployee().bProjectEnabled) return true;
   if (r.header.getObjEmployee().strEmployeeNum != o.header.getObjEmployee().strEmployeeNum) return true;
   if (r.header.getObjExpTemplate().id != o.header.getObjExpTemplate().id) return true;
   if (r.header.getObjExpTemplate().name != o.header.getObjExpTemplate().name) return true;
   if (r.oopReceipts.length != o.oopReceipts.length) return true;
   if (r.cCardReceipts.length != o.cCardReceipts.length) return true;
   if (r.oopReceipts.length != o.oopReceipts.length) return true;
   for (var i=0;i<r.oopReceipts.length;i++){
      if (r.oopReceipts[i]){
    	 if (r.oopReceipts[i].getObjExpenseType() && o.oopReceipts[i].getObjExpenseType()){
      	    if (r.oopReceipts[i].getObjExpenseType().ID != o.oopReceipts[i].getObjExpenseType().ID) return true;
    	   }
    	 else if ((r.oopReceipts[i].getObjExpenseType() && !o.oopReceipts[i].getObjExpenseType()) ||
	     (!r.oopReceipts[i].getObjExpenseType() && o.oopReceipts[i].getObjExpenseType())) return true;
    

    	 if (r.oopReceipts[i].date != o.oopReceipts[i].date) return true;
    	 if (r.oopReceipts[i].occurs != o.oopReceipts[i].occurs) return true;
    	 if (r.oopReceipts[i].expensedAmount != o.oopReceipts[i].expensedAmount) return true; 
    	 if (r.oopReceipts[i].receiptAmount != o.oopReceipts[i].receiptAmount) return true;
    	 //if (r.oopReceipts[i].isMissing != o.oopReceipts[i].isMissing) return true;
    	 if (r.oopReceipts[i].receiptCurr != o.oopReceipts[i].receiptCurr) return true;
    	 if (r.oopReceipts[i].exchRate != o.oopReceipts[i].exchRate) return true;
    	 if (r.oopReceipts[i].justification != o.oopReceipts[i].justification) return true;
    	 if (r.oopReceipts[i].cCardTrxnId != o.oopReceipts[i].cCardTrxnId) return true;
    	 if (r.oopReceipts[i].merchant != o.oopReceipts[i].merchant) return true;
    	 if (r.oopReceipts[i].itemizeId != o.oopReceipts[i].itemizeId) return true;
    	 if (r.oopReceipts[i].expenseGroup != o.oopReceipts[i].expenseGroup) return true;
    	 if (r.oopReceipts[i].category != o.oopReceipts[i].category) return true;
    	 if (r.oopReceipts[i].projectNumber != o.oopReceipts[i].projectNumber) return true;
   	 if (r.oopReceipts[i].taskNumber != o.oopReceipts[i].taskNumber) return true;
    	 if (r.oopReceipts[i].flexFields && o.oopReceipts[i].flexFields){
    	    for (var j=0;j<r.oopReceipts[i].flexFields.length;j++){
      	    	if (r.oopReceipts[i].flexFields[j].name != o.oopReceipts[i].flexFields[j].name) return true;
      	    	if (r.oopReceipts[i].flexFields[j].value != o.oopReceipts[i].flexFields[j].value) return true;
     	    }
         }
         else return true;
       }// if (oopReceipt)
   }// for loop

  if (r.cCardReceipts.length != o.cCardReceipts.length) return true; 

  for (var i=0;i<r.cCardReceipts.length;i++){
    if (r.cCardReceipts[i]){
    	if (r.cCardReceipts[i].getObjExpenseType() && o.cCardReceipts[i].getObjExpenseType()){
      		if (r.cCardReceipts[i].getObjExpenseType().ID != o.cCardReceipts[i].getObjExpenseType().ID) return true;
    	}
    	else if ((r.cCardReceipts[i].getObjExpenseType() && !o.cCardReceipts[i].getObjExpenseType()) ||
	     (!r.cCardReceipts[i].getObjExpenseType() && o.cCardReceipts[i].getObjExpenseType())) return true;
    
    	if (r.cCardReceipts[i].date != o.cCardReceipts[i].date) return true;
    	if (r.cCardReceipts[i].occurs != o.cCardReceipts[i].occurs) return true;
    	if (r.cCardReceipts[i].expensedAmount != o.cCardReceipts[i].expensedAmount) return true; 
    	if (r.cCardReceipts[i].receiptAmount != o.cCardReceipts[i].receiptAmount) return true;
    	//if (r.cCardReceipts[i].isMissing != o.cCardReceipts[i].isMissing) return true;
    	if (r.cCardReceipts[i].receiptCurr != o.cCardReceipts[i].receiptCurr) return true;
    	if (r.cCardReceipts[i].exchRate != o.cCardReceipts[i].exchRate) return true;
    	if (r.cCardReceipts[i].justification != o.cCardReceipts[i].justification) return true;
    	if (r.cCardReceipts[i].cCardTrxnId != o.cCardReceipts[i].cCardTrxnId) return true;
    	if (r.cCardReceipts[i].merchant != o.cCardReceipts[i].merchant) return true;
    	if (r.cCardReceipts[i].itemizeId != o.cCardReceipts[i].itemizeId) return true;
    	if (r.cCardReceipts[i].expenseGroup != o.cCardReceipts[i].expenseGroup) return true;
    	if (r.cCardReceipts[i].category != o.cCardReceipts[i].category) return true;
    	if (r.cCardReceipts[i].projectNumber != o.cCardReceipts[i].projectNumber) return true;
    	if (r.cCardReceipts[i].taskNumber != o.cCardReceipts[i].taskNumber) return true;
    
    	if (r.cCardReceipts[i].flexFields && o.cCardReceipts[i].flexFields){
     	   for (var j=0;j<r.cCardReceipts[i].flexFields.length;j++){
      		if (r.cCardReceipts[i].flexFields[j].name != o.cCardReceipts[i].flexFields[j].name) return true;
     		 if (r.cCardReceipts[i].flexFields[j].value != o.cCardReceipts[i].flexFields[j].value) return true;
     	   }
    	}
    	else return true;
      }
  }
  return false;
 }
 return false;
}

function fSetOldReportObject(){
  var r = fGetObjExpReport();
  g_oldObjReport = new expenseReportObject();

  var objEmployee = new empObject(r.header.getObjEmployee().strEmployeeName,
				  r.header.getObjEmployee().strEmployeeID,
				  r.header.getObjEmployee().strCostCenter,
				  r.header.getObjEmployee().bProjectEnabled,
				  r.header.getObjEmployee().strEmployeeNum);
  var objExpTemplate = new expenseTemplateObject(r.header.getObjExpTemplate().id,
						 r.header.getObjExpTemplate().name,
						 null);
  g_oldObjReport.header = new headerObject(r.reportHeaderID,objEmployee,r.approver,r.approverID,r.purpose,r.reimbursCurr, objExpTemplate, r.amtDueEmployee, r.amtDueccCompany, r.costCenter);
  if (r.oopReceipts.length > 0)  g_oldObjReport.oopReceipts = new Array();
  for (var i=0;i<r.oopReceipts.length;i++){
    if (r.oopReceipts[i]){
     if (r.oopReceipts[i].getObjExpenseType())
      objExpType = new expenseTypeObject(r.oopReceipts[i].getObjExpenseType().ID,
					r.oopReceipts[i].getObjExpenseType().name,
					r.oopReceipts[i].getObjExpenseType().folioType,
					r.oopReceipts[i].getObjExpenseType().nRequireReceiptAmount,
					r.oopReceipts[i].getObjExpenseType().bJustifRequired,
					r.oopReceipts[i].getObjExpenseType().bCalAmtFlag);
    else objExpType = null;
    
    g_oldObjReport.oopReceipts[i] = new receiptObject(r.oopReceipts[i].date,
						r.oopReceipts[i].occurs,
						r.oopReceipts[i].expensedAmount,
						r.oopReceipts[i].receiptAmount,
						r.oopReceipts[i].isMissing,
						r.oopReceipts[i].receiptCurr,
						r.oopReceipts[i].exchRate,
						objExpType,
						r.oopReceipts[i].justification,
						r.oopReceipts[i].cCardTrxnId,
						r.oopReceipts[i].merchant,
						r.oopReceipts[i].itemizeId,
						r.oopReceipts[i].expenseGroup,
						r.oopReceipts[i].category,
						r.oopReceipts[i].projectNumber,
						r.oopReceipts[i].taskNumber);
    if (r.oopReceipts[i].flexFields) g_oldObjReport.oopReceipts[i].flexFields = new Array();
     for (var j=0;j<r.oopReceipts[i].flexFields.length;j++){
      g_oldObjReport.oopReceipts[i].flexFields[j] = new flexFieldValues(r.oopReceipts[i].flexFields[j].name,r.oopReceipts[i].flexFields[j].value);
     }
   }
   else g_oldObjReport.oopReceipts[i] = null;
  }
 
  if (r.cCardReceipts.length > 0)  g_oldObjReport.cCardReceipts = new Array();
  for (var i=0;i<r.cCardReceipts.length;i++){
   if (r.cCardReceipts[i]){
    if (r.cCardReceipts[i].getObjExpenseType())
      objExpType = new expenseTypeObject(r.cCardReceipts[i].getObjExpenseType().ID,
					r.cCardReceipts[i].getObjExpenseType().name,
					r.cCardReceipts[i].getObjExpenseType().folioType,
					r.cCardReceipts[i].getObjExpenseType().nRequireReceiptAmount,
					r.cCardReceipts[i].getObjExpenseType().bJustifRequired,
					r.cCardReceipts[i].getObjExpenseType().bCalAmtFlag);
    else
      objExpType = null;
    
    g_oldObjReport.cCardReceipts[i] = new receiptObject(r.cCardReceipts[i].date,
						r.cCardReceipts[i].occurs,
						r.cCardReceipts[i].expensedAmount,
						r.cCardReceipts[i].receiptAmount,
						r.cCardReceipts[i].isMissing,
						r.cCardReceipts[i].receiptCurr,
						r.cCardReceipts[i].exchRate,
						objExpType,
						r.cCardReceipts[i].justification,
						r.cCardReceipts[i].cCardTrxnId,
						r.cCardReceipts[i].merchant,
						r.cCardReceipts[i].itemizeId,
						r.cCardReceipts[i].expenseGroup,
						r.cCardReceipts[i].category,
						r.cCardReceipts[i].projectNumber,
						r.cCardReceipts[i].taskNumber);
    if (r.cCardReceipts[i].flexField) g_oldObjReport.cCardReceipts[i].flexFields = new Array();
     for (var j=0;j<r.cCardReceipts[i].flexFields.length;j++){
      g_oldObjReport.cCardReceipts[i].flexFields[j] = new flexFieldValues(r.cCardReceipts[i].flexFields[j].name,r.cCardReceipts[i].flexFields[j].value);
     } // for loop
   }// if (r.cCardReceipts[i])
   else g_oldObjReport.cCardReceipts[i] = null;
  }
  


}

// This is the function to force onChange event got triggered for IE
function fIEOnChangeTrigger(){
    if (fIsDetailOpen()){
	 fGetObjDetail().framReceiptDetail.document.formDetail.txtDate.focus();
	 fGetObjDetail().framReceiptDetail.document.formDetail.txtJustification.focus();
	 fCloseDetail();
    }
    var f = framMain.header.document.reportHeaderForm;
    f.txtCostCenter.focus();
    f.txtPurpose.focus();
    if (f) if (f.AP_WEB_FULLNAME) fChangeApprover(f.AP_WEB_FULLNAME.value,f.AP_WEB_EMPID.value);
    if (fIsOOP()){
	 framMain.lines.document.data.txtDate1.focus();
	 framMain.lines.document.data.txtJustification1.focus();
    }
    else if (fGetObjExpReport().getTotalReceipts('CC')>0){
	 framMain.lines.document.data.txtDate1.focus();
	 framMain.lines.document.data.txtJustification1.focus();
    }
}


/* ----------------------------------
-- Recover Expense Type
---------------------------------- */
function fRecoverExpenseType(id, popExpType){
  if (id == ""){
	 popExpType.selectedIndex = 0; 
	 return;
  }
  else
   for (var i=0;i<popExpType.options.length;i++){
	 if (popExpType.options[i].value == id){
	   popExpType.selectedIndex = i;
	   return;
	 }
    }
}
