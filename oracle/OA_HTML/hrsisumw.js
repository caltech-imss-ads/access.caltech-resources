<!-- hide the script's contents from feeble browsers
<!-- $Header: hrsisumw.js 115.0 2000/01/20 12:49:40 pkm ship       $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date         Name          Bug #    Reason
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 110.0   20-Jun-1999  arundell               Created.
<!-- 110.1   01-Sep-1999  lma           934577   Replace _top with
<!--                                             container_middle in 
<!--                                             cancel_button_onClick function
<!-----------------------------------------------------------------------------
<!-----------------------------------------------------------------------------
<!-- Javascript file for Special Information Types
<!--  
<!-----------------------------------------------------------------------------
top.PRINT_FLAG = 'Y';
// The following function is invoked from the
// header frame when the user clicks on the 
// 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.workflowForm.p_result_code.value = 'MAIN_MENU';  
  if (document.workflowForm.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
       top.clear_container_bottom_frame();
       document.workflowForm.target = '_top'; 
       document.workflowForm.submit();
     }
  }
  else {
       top.clear_container_bottom_frame();
       document.workflowForm.target = '_top';
       document.workflowForm.submit();
  }
}
//when user click cancel button
function back_button_onClick (msg,newtarget) {
  if (document.workflowForm.p_form_changed.value=="Y") {
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'PREVIOUS';
        document.workflowForm.target = newtarget;
        document.workflowForm.submit();
     }
  } else {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'PREVIOUS';
        document.workflowForm.target = newtarget;
        document.workflowForm.submit();
  }
}


// container_bottom frame when the user clicks
// on the 'Cancel' button.
function cancel_button_onClick (msg) {
  if (document.workflowForm.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'CANCEL';
        document.workflowForm.target = 'container_middle';
        document.workflowForm.submit();
     }
  } else {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'CANCEL';
        document.workflowForm.target = 'container_middle';
        document.workflowForm.submit();
  }
}


// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Next' button.
function next_button_onClick () {
  top.clear_container_bottom_frame();
  document.workflowForm.p_result_code.value = 'NEXT';
  document.workflowForm.submit();
  }



