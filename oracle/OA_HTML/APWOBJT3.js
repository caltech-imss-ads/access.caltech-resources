//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT3.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |                                                |
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJCT.js is split into 4 files |
//  |              APWOBJT1.js, APWOBJT2.js ,        |
//  |              APWOBJT3.js, APWOBJT4.js          |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT3.js 115.0 2000/11/02 18:06:22 pkm ship        $ */
/*===========================================================================*
 *  FlexFieldValues Class                                                    *
 *===========================================================================*/

function flexFieldValues(name,value){
       this.name = name + "";
       this.value = value;
}

flexFieldValues.prototype.mGetName = mGetName;
flexFieldValues.prototype.mGetValue = mGetValue;
flexFieldValues.prototype.mSetName = mSetName;
flexFieldValues.prototype.mSetValue = mSetValue;


function mGetName(){	
	return this.name;
}

function mGetValue(){	
	return this.value;
}

function mSetName(name){	
	this.name = name;
}

function mSetValue(value){	
	this.value = value;
}


/*===========================================================================*
 *  Env Class - Store Misc Environment variables                             *
 *===========================================================================*/

function envObject(bThirdPartyEnabled, bEmployeeNameReq, bCostCenterReq, bDescFlexEnabled, bTaxEnabled){
	this.bThirdPartyEnabled = bThirdPartyEnabled;
	this.bEmployeeNameReq = bEmployeeNameReq;
	this.bCostCenterReq = bCostCenterReq; 
	this.bDescFlexEnabled = bDescFlexEnabled;
	this.bTaxEnabled = bTaxEnabled;
}

envObject.prototype.mGetbThirdPartyEnabled = mGetbThirdPartyEnabled;
envObject.prototype.mGetbEmployeeNameRequired = mGetbEmployeeNameRequired;
envObject.prototype.mGetbCostCenterRequired = mGetbCostCenterRequired;
envObject.prototype.mGetbDescFlexEnabled = mGetbDescFlexEnabled;
envObject.prototype.mGetbTaxEnabled = mGetbTaxEnabled;
envObject.prototype.mSetThirdPartyEnabled = mSetThirdPartyEnabled;
envObject.prototype.mSetEmployeeNameRequired = mSetEmployeeNameRequired;
envObject.prototype.mSetCostCenterRequired = mSetCostCenterRequired;
envObject.prototype.mSetDescFlexEnabled = mSetDescFlexEnabled;
envObject.prototype.mSetTaxEnabled = mSetTaxEnabled;

function mGetbThirdPartyEnabled(){	
	return this.bThirdPartyEnabled;
}

function mGetbEmployeeNameRequired(){	
	return this.bEmployeeNameReq;
}

function mGetbCostCenterRequired(){	
	return this.bCostCenterReq;
}

function mGetbDescFlexEnabled(){	
	return this.bDescFlexEnabled;
}

function mGetbTaxEnabled(){	
	return this.bTaxEnabled;
}

function mSetThirdPartyEnabled(bThirdPartyEnabled){	
	this.bThirdPartyEnabled = bThirdPartyEnabled;
}

function mSetEmployeeNameRequired(bEmployeeNameReq){	
	this.bEmployeeNameReq = bEmployeeNameReq;
}

function mSetCostCenterRequired(bCostCenterReq){	
	this.bCostCenterReq = bCostCenterReq;
}

function mSetDescFlexEnabled(bDescFlexEnabled){	
	this.bDescFlexEnabled = bDescFlexEnabled;
}

function mSetTaxEnabled(bTaxEnabled){	
	this.bTaxEnabled = bTaxEnabled;
}

/*===========================================================================*
 *  ExpenseReport Class                                                      *
 *===========================================================================*/

function expenseReportObject(){
        this.header = null;
        this.oopReceipts = new Array();
        this.cCardReceipts = new Array();  
        this.creditCardProg = null; 
 }

expenseReportObject.prototype.getCreditCardProg = ER_getCreditCardProg;
expenseReportObject.prototype.setCreditCardProg = ER_setCreditCardProg;
expenseReportObject.prototype.deleteReceipts = ER_deleteReceipts
expenseReportObject.prototype.addCCardReceipt = ER_addCCardReceipt;
expenseReportObject.prototype.addReceipt = ER_addReceipt;
expenseReportObject.prototype.getReceiptCurrencyName = ER_getReceiptCurrencyName;
expenseReportObject.prototype.getNthReceipt = ER_getNthReceipt;
expenseReportObject.prototype.getTotalReceipts = ER_getTotalReceipts;
expenseReportObject.prototype.calculateTotal = ER_calculateTotal;
expenseReportObject.prototype.getNthLine = ER_getNthLine;
expenseReportObject.prototype.getHighestItemizeId = ER_getHighestItemizeId;

function ER_getCreditCardProg() { 
	return this.creditCardProg;
}

function ER_setCreditCardProg(sCreditCardProg) { 
	this.creditCardProg = sCreditCardProg;
}


function ER_deleteReceipts(receiptType)
{   if (receiptType == 'OOP')
        this.oopReceipts.length = 0;
    else
        this.cCardReceipts.length = 0;
}


function ER_addReceipt(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
        this.oopReceipts[this.oopReceipts.length] = new receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber);
        }


function ER_addCCardReceipt(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
        this.cCardReceipts[this.cCardReceipts.length] = new receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber);
        }


function ER_getReceiptCurrencyName(iIndex){
   if ((iIndex>=0) && (iIndex<this.currencies.length))
   return(this.currencies[iIndex].shortName);
   else return null;
}

function ER_getNthLine(nIndex){
var i=0;
var r = null;
	if (top.fIsOOP()) 
	r = this.oopReceipts;
	else 
	r = this.cCardReceipts;

       for (var j=0;j< r.length;j++){
         if (r[j] != null) i++;
         if (j==nIndex) return i;
	}
       return -1;

}

function ER_getNthReceipt(n){
var i=0;
var r = null;
	if (top.fIsOOP()) 
	r = this.oopReceipts;
	else 
	r = this.cCardReceipts;

       for (var j=0;j< r.length;j++){
         if (r[j] != null) i++;
         if (i==n) return j;
	}
       return -1;
}


function ER_getTotalReceipts(strType){
var counter = 0; 
      if (strType == 'OOP'){
        for (var i=0;i< this.oopReceipts.length;i++){
          if (this.oopReceipts[i] != null){
          counter ++;
	  }
         }
       return counter;
      }
      else {
        for (var i=0;i< this.cCardReceipts.length;i++){
          if (this.cCardReceipts[i] != null){
          counter ++;
	  }
         }
       return counter;
      }
      return 0;
}

function ER_calculateTotal(strType){
var total = 0;
      if (strType == 'OOP'){
        for (var i=0;i< this.oopReceipts.length;i++){
          if (this.oopReceipts[i] != null){
          total += eval(this.oopReceipts[i].getReimbursAmount());
	  }
         }
       return total;
      }
      else {
        for (var i=0;i< this.cCardReceipts.length;i++){
          if (this.cCardReceipts[i] != null){
          total += eval(this.cCardReceipts[i].getReimbursAmount());
	  }
         }
       return total;
      }
      return 0;
}

function ER_getHighestItemizeId(){
  var tempId = 1000;
  for (var i=0;i< this.oopReceipts.length;i++){
	if (this.oopReceipts[i])
	  if (this.oopReceipts[i].itemizeId > tempId)
 	    tempId = this.oopReceipts[i].itemizeId;
  }

  for (var i=0;i< this.cCardReceipts.length;i++){
	if (this.cCardReceipts[i])
    	  if (this.cCardReceipts[i].itemizeId > tempId)
 		tempId = this.cCardReceipts[i].itemizeId;
  }
  
  return tempId;
}

/*===========================================================================*
 *  Header Class - Expense Report Header Cleass                              *
 *===========================================================================*/

function headerObject(reportHeaderID,objEmployee,approver,approverID,purpose,reimbursCurr,objExpTemplate, costCenter){
	this.reportHeaderID = reportHeaderID;
	this.objEmployee = objEmployee;;
        this.approver = approver;
	this.approverID = approverID;
        this.purpose = purpose;
	this.reimbursCurr = reimbursCurr;
	this.objExpTemplate = objExpTemplate;
	this.amtDueEmployee = 0;
	this.amtDueccCompany = 0;
	this.costCenter = costCenter;
}

headerObject.prototype.getApprover = H_getApprover;
headerObject.prototype.getApproverID = H_getApproverID;
headerObject.prototype.setApproverID = H_setApproverID;
headerObject.prototype.setApprover = H_setApprover;
headerObject.prototype.setCostCenter = H_setCostCenter;
headerObject.prototype.getCostCenter = H_getCostCenter;
headerObject.prototype.setAmtDueEmployee = H_setAmtDueEmployee;
headerObject.prototype.getAmtDueEmployee = H_getAmtDueEmployee;
headerObject.prototype.setAmtDueccCompany = H_setAmtDueccCompany;
headerObject.prototype.getAmtDueccCompany = H_getAmtDueccCompany;
headerObject.prototype.getPurpose = H_getPurpose;
headerObject.prototype.setPurpose = H_setPurpose;
headerObject.prototype.setReimbursCurr = H_setReimbursCurr;
headerObject.prototype.getReimbursCurr = H_getReimbursCurr;
headerObject.prototype.setExpTemplate = H_setExpTemplate;
headerObject.prototype.getObjExpTemplate = H_getObjExpTemplate;
headerObject.prototype.getExpTemplateID = H_getExpTemplateID;
headerObject.prototype.setEmployee = H_setEmployee;
headerObject.prototype.getObjEmployee = H_getObjEmployee;

function H_setCostCenter(costCenter){
	this.costCenter = costCenter;
}

function H_getCostCenter(){ 
	return this.costCenter;
}

function H_setAmtDueEmployee(amtDueEmployee){	
	this.amtDueEmployee = amtDueEmployee;
}

function H_getAmtDueEmployee(){	
	return (this.amtDueEmployee);
}

function H_setAmtDueccCompany(amtDueccCompany){	
	this.amtDueccCompany = amtDueccCompany;
}

function H_getAmtDueccCompany(){	
	return(this.amtDueccCompany);
}

function H_getPurpose(){        
	return (this.purpose);
}

function H_setPurpose(strPurpose){        
	this.purpose = strPurpose;
}

function H_setReimbursCurr(reimbursCurr){        
	this.reimbursCurr = reimbursCurr;
}

function H_getReimbursCurr(){        
	return (this.reimbursCurr);
}

function H_getExpTemplateID(){   
	if (this.objExpTemplate)
	  return this.objExpTemplate.id;
	else
	  return "";
}

function H_setExpTemplate(objExpTemplate){	
	  this.objExpTemplate = objExpTemplate;
}

function H_getObjExpTemplate(){
	return this.objExpTemplate;
}

function H_getReimbursCurr(){        
	return this.reimbursCurr;
}


function H_setEmployee(objEmployee){	
	  this.objEmployee = objEmployee;
}

function H_getObjEmployee()
{	
	return this.objEmployee;
}

function H_getApproverID(){
	if (top.fIsNum(this.approverID,false))
	return this.approverID;
	else return "";
}

function H_getApprover(){
	return this.approver;
}

function H_setApproverID(strApproverID){
	this.approverID = strApproverID;
}

function H_setApprover(strApprover){
	this.approver = strApprover;
}


/*===========================================================================*
 *  ExpenseTemplate Class                                                    *
 *===========================================================================*/

function expenseTemplateObject(p_id, p_name,expTypeArr){
  this.id = p_id;  //ID == .value ==strExpenseTemplateID
  this.name = p_name;
  this.arrExpTypes = new Array(); 
 }
expenseTemplateObject.arrAllExpenseType = new Array();  

expenseTemplateObject.prototype.mstrGetTemplateID = getTemplateID();
expenseTemplateObject.prototype.mstrGetTemplateName = getTemplateName();
expenseTemplateObject.prototype.addExpType = addExpType;
expenseTemplateObject.prototype.getExpTypeName = getExpTypeName;
expenseTemplateObject.prototype.getExpTypeID = getExpTypeID;
expenseTemplateObject.prototype.getExpTypeObj = getExpTypeObj;
expenseTemplateObject.prototype.getJustifReq = getJustifReq; 

function getTemplateID(){  
	return this.id;
}

function getTemplateName(){  
	return this.name;
}

function addExpType(id) {
  for (var j=0;j<expenseTemplateObject.arrAllExpenseType.length;j++){
	if (expenseTemplateObject.arrAllExpenseType[j].mGetID() == id){ 
	  this.arrExpTypes[this.arrExpTypes.length] = expenseTemplateObject.arrAllExpenseType[j];
	}
  }
}

function getExpTypeName(folioType) {
  for (var i=0; i< this.arrExpTypes.length;i++) {
    if ( this.arrExpTypes[i].mGetFolioType() == folioType) {
	return this.arrExpTypes[i].mGetstrName();
	}
    }//end for
   return "";
}//end getExpTypeName


function getExpTypeID(folioType) {
  for (var i=0; i< this.arrExpTypes.length;i++) {
    if ( this.arrExpTypes[i].mGetFolioType() == folioType) {
 	return this.arrExpTypes[i].mGetID();
	}
    }//end for
  return "";
}//end getID



function getJustifReq(ID) {
  if (this.getExpTypeObj(ID)) 
	return this.getExpTypeObj(ID).mGetbJustifRequried();
  else return 'N';
}

function getExpTypeObj(ID) {
  for (var i=0; i< expenseTemplateObject.arrAllExpenseType.length; i++) {
    if (expenseTemplateObject.arrAllExpenseType[i].mGetID() == ID)
       return expenseTemplateObject.arrAllExpenseType[i];
  } //end for
  return null;
} //end getExpTypeObj


/*===========================================================================*
 *  Expense Type Class                                                       *
 *===========================================================================*/

function expenseTypeObject(ID,name,code,folioType,nRequireReceiptAmount,bJustifRequired,bCalAmtFlag,bAITFlag, strDefaultTaxCode, bUpdateDefaultTaxCode, strExpenditureType){
  this.ID = ID;
  this.name = name; 
  this.code = code; 
  this.folioType = folioType;
  this.nRequireReceiptAmount = nRequireReceiptAmount;
  this.bJustifRequired = bJustifRequired;
  this.bCalAmtFlag = bCalAmtFlag;
  this.bAITFlag = bAITFlag;
  this.strDefaultTaxCode = strDefaultTaxCode;
  this.bUpdateDefaultTaxCode = bUpdateDefaultTaxCode;
  this.strExpenditureType = strExpenditureType;
}



expenseTypeObject.prototype.mGetID = ET_mGetID;
expenseTypeObject.prototype.mGetstrName = ET_mGetstrName;
expenseTypeObject.prototype.mGetstrCode = ET_mGetstrCode;
expenseTypeObject.prototype.mGetFolioType = ET_mGetCardExpType;
expenseTypeObject.prototype.mGetRequireReceiptAmount = ET_mGetRequireReceiptAmount;
expenseTypeObject.prototype.mGetbJustifRequired = ET_mGetbJustifRequired;
expenseTypeObject.prototype.mSetID = ET_mSetID;
expenseTypeObject.prototype.mSetName = ET_mSetName;
expenseTypeObject.prototype.mSetCode = ET_mSetCode;
expenseTypeObject.prototype.mSetFolioType = ET_mSetCardExpType;
expenseTypeObject.prototype.mSetRequireReceiptAmount = ET_mSetRequireReceiptAmount;
expenseTypeObject.prototype.mSetJustifRequired = ET_mSetJustifRequired;
expenseTypeObject.prototype.mGetbCalAmtFlag = ET_mGetbCalAmtFlag;
expenseTypeObject.prototype.mGetbAITFlag = ET_mGetbAITFlag;
expenseTypeObject.prototype.mGetstrDefaultTaxCode = ET_mGetstrDefaultTaxCode;

function ET_mGetstrDefaultTaxCode(){
   return this.strDefaultTaxCode;

}

function ET_mGetbAITFlag(){
   return this.AITFlag;
}


function ET_mGetID() {  
	return this.ID;
}

function ET_mGetstrName() {  
	return this.name;
}

function ET_mGetstrCode() {  
	return this.code;
}

function ET_mGetCardExpType() {  
	return this.folioType;
}

function ET_mGetRequireReceiptAmount() {  
	return this.nRequireReceiptAmount;
}

function ET_mGetbJustifRequired() {  
	return this.bJustifRequired;
}


function ET_mSetID(ID) {  
	this.ID = ID;
}

function ET_mSetName(strName) {  
	this.strName =strName;
}

function ET_mSetCode(strCode) {  
	this.strCode =strCode;
}

function ET_mSetCardExpType(folioType) {  
	this.folioType = folioType;
}

function ET_mSetRequireReceiptAmount(nRequireReceiptAmount) {  
	this.nRequireReceiptAmount = nRequireReceiptAmount;
}

function ET_mSetJustifRequired(bJustifRequired) {  
	this.bJustifRequired = bJustifRequired;
}

function ET_mGetbCalAmtFlag() {
  return this.bCalAmtFlag;
}


/*===========================================================================*
 *  Tax Class                                                                *
 *===========================================================================*/

// merchName has been moved to receipt object
function Tax(docNum, merchRef, taxRegNum, taxPayerID, supCountry, taxCode, taxID, overrideFlag, amountIncludesTax){
if (arguments.length > 0) {
	this.docNum = docNum;
	this.merchRef = merchRef;
	this.taxRegNum = taxRegNum;
	this.taxPayerID = taxPayerID;
	this.supCountry = supCountry;
	this.taxCode = taxCode;
	this.taxID = taxID;
	this.overrideFlag = overrideFlag;
	this.amountIncludesTax = amountIncludesTax;
  }
else{
	this.docNum = "";
	this.merchRef = "";
	this.taxRegNum = "";
	this.taxPayerID = "";
	this.supCountry = "";
	this.taxCode = "";
	this.taxID = "";
	this.overrideFlag = "";
	this.amountIncludesTax = "";
  }
}

Tax.prototype.mGetDocNum = Tax_mGetDocNum;
Tax.prototype.mGetMerchRef = Tax_mGetMerchRef;
Tax.prototype.mGetTaxRegNum = Tax_mGetTaxRegNum;
Tax.prototype.mGetTaxPayerID = Tax_mGetTaxPayerID;
Tax.prototype.mGetSupCountry = Tax_mGetSupCountry;
Tax.prototype.mGetTaxCode = Tax_mGetTaxCode;
Tax.prototype.mGetTaxID = Tax_mGetTaxID;
Tax.prototype.mGetOverrideFlag = Tax_mGetOverrideFlag;
Tax.prototype.mGetAmountIncludesTax = Tax_mGetAmountIncludesTax;
Tax.prototype.mSetDocNum = Tax_mSetDocNum;
Tax.prototype.mSetMerchRef = Tax_mSetMerchRef;
Tax.prototype.mSetTaxRegNum = Tax_mSetTaxRegNum;
Tax.prototype.mSetTaxPayerID = Tax_mSetTaxPayerID;
Tax.prototype.mSetSupCountry = Tax_mSetSupCountry;
Tax.prototype.mSetTaxCode = Tax_mSetTaxCode;
Tax.prototype.mSetTaxID = Tax_mSetTaxID;
Tax.prototype.mSetOverrideFlag = Tax_mSetOverrideFlag;
Tax.prototype.mSetAmountIncludesTax = Tax_mSetAmountIncludesTax;

function Tax_mGetDocNum(){	return this.docNum;}

function Tax_mGetMerchRef(){	return this.merchRef;}

function Tax_mGetTaxRegNum(){	
	return this.taxRegNum;
}

function Tax_mGetTaxPayerID(){	
	return this.taxPayerID;
}

function Tax_mGetSupCountry(){	
	return this.supCountry;
}

function Tax_mGetTaxCode(){	
	return this.taxCode;
}

function Tax_mGetTaxID(){	
	return this.taxID;
}

function Tax_mGetOverrideFlag(){	
	return this.overrideFlag;
}

function Tax_mGetAmountIncludesTax(){	
	return this.amountIncludesTax;
}

function Tax_mSetDocNum(docNum){	
	this.docNum = docNum;
}

function Tax_mSetMerchRef(merchRef){	
	this.merchRef = merchRef;
}

function Tax_mSetTaxRegNum(taxRegNum){	
	this.taxRegNum = taxRegNum;
}

function Tax_mSetTaxPayerID(taxPayerID){	
	this.taxPayerID = taxPayerID;
}

function Tax_mSetSupCountry(supCountry){	
	this.supCountry = supCountry;
}

function Tax_mSetTaxCode(taxCode){	
	this.taxCode = taxCode;
}

function Tax_mSetTaxID(taxID){	
	this.taxID = taxID;
}

function Tax_mSetOverrideFlag(overrideFlag){	
	this.overrideFlag = overrideFlag;
}

function Tax_mSetAmountIncludesTax(amountIncludesTax){	
	this.amountIncludesTax = amountIncludesTax;
}



























