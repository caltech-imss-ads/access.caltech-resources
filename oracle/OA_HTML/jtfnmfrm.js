 /*==========================================================================
       Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA
                          All rights reserved.
 ============================================================================
  FILENAME
     jtfnmfrm.js

  DESCRIPTION
     Created based on calendar_constructor.js in Cabo/JSUI version 0.5.

  HISTORY
     05/24/2000   pkanukol   Created.
 ========================================================================= */
<!-- $Header: jtfnmfrm.js 115.5 2000/12/20 13:39:21 pkm ship      $ -->
var g_curr = '$';
var g_numDecimal = 2;
var g_sign = '+';
var l_sign = false; //leading sign
var t_sign = false; // trailing sign
var pr_sign = false; //angle bracket for -ve nums
var g_delim = ','; // default delimiter
var delimarr = null;
var delim_flg = false;
var notNumeric = "Number Entered is Not Numeric";
var badFormat = "Number Format is Wrong";
var shortFormat = "Format Specified is shorter than the number of digits";

function parseNum(P_num, curr)
{
	var c = curr;
	var n = P_num; n = n.toString();	
	if((n.search(/-|\+|</) == -1) || (n.search(/-|\+|</) ==0))
	{
		if((n.search(/-|\+|</) ==0)) n = n.replace(/\+|-/g,''); 
		if((n.search(/-|\+|>/) == -1)||(n.search(/-|\+|>/)==(n.length-1)))
		{
			n = n.replace(/\+|-|<|>|\,/g,''); 
			n = parseFloat(n); 
			return n;
		}
		else
			return null;
	}
	else
		return null; 
}	

function formatCurrency(P_num, P_curr, P_format)
{
	if (P_num == null|P_num == ''| P_num == ' ') return ' ';
	var formatN;
	if(P_format != null)
		formatN = formatNum(P_num, P_format);
	else
		formatN = formatDefault(P_num);
	if (P_curr == null|P_curr == ''| P_curr == ' ') 
		P_curr = g_curr;
	if(formatN != null)
	 	return (P_curr + formatN );
	else
		return null;
}

function formatDefault(P_num) 
{
	var result = null; 
	var numcheck = parseNum(P_num);
	if(numcheck != null && !isNaN(numcheck) )
	{
	if(P_num.toString().indexOf('+') != -1)
		l_sign = true; 
	if(P_num.toString().indexOf('-') != -1)
	{
		g_sign = '-';
		l_sign = true;
	}
	P_num = P_num.toString().replace(/\,|-/g,'');
	if(isNaN(P_num)) P_num = "0";
	if(P_num != 0)
	{	
		var P_pow = Math.pow(10,g_numDecimal); 
		var dec = Math.floor((P_num*P_pow+0.5)%P_pow); 
		P_num = Math.floor(P_num).toString();
		if(dec < (P_pow/10)) dec = "0" + dec; 
		for (var i = 0; i < Math.floor((P_num.length-(1+i))/3); i++) 
			P_num = P_num.substring(0,P_num.length-(4*i+3))+','+P_num.substring(P_num.length-(4*i+3)); 
		if(l_sign)
			result = g_sign + P_num + '.' + dec; 
		else
			result = P_num + '.' + dec; 
	}
	else
	{
		alert(notNumeric);
		return null;
	}
	g_sign = '+';
	return result;
	}
	else 
	return numcheck;
}

function formatNum(P_num, P_format)
{
	var P_int =0;
	var P_dec = 0;
	var f_int = null;
	var f_dec = null;
	var numcheck = parseNum(P_num);
	if(numcheck != null && !isNaN(numcheck) )
	{
	if(P_format == null|P_format == ''| P_format == ' ')
	{
		var result = formatDefault(P_num);
		return result;
	}
	P_num = P_num.toString();
	if(P_num.indexOf('<') != -1)
	{
		if(P_num.indexOf('>') != -1)
			g_sign = '-';
		else
		{
			alert(badFormat);
			return null;
		}
	}	
	if(P_num.indexOf('-') != -1)
		g_sign = '-'; 
	P_num = P_num.replace(/<|>|\,|-|\+/g,''); 
	if(P_num.indexOf('.') < 0)
		P_int = P_num;
	else
	{	
		var numarr = P_num.split('.');		
		P_int = numarr[0];
		P_dec = numarr[1]; 
	}
	if(P_format.indexOf('D') < 0)
	{
		f_int = P_format;
		f_int = trimAlpha(f_int);
	}
	else
	{
		var frmarr = P_format.split('D');		
		f_int = frmarr[0]; 
		f_dec = frmarr[1]; 

		f_int = trimAlpha(f_int); 
		f_dec = trimAlpha(f_dec); 

	}
	P_int = frm(P_int, f_int, 'rtl');

	if(f_dec != null)
		P_dec = frm(P_dec, f_dec, 'ltr'); 
	if(P_int == null || P_dec == null)
		return null;
	if(pr_sign && (g_sign == '-'))
	{
		P_int = '<' + P_int;
		if(f_dec!=null) P_dec = P_dec + '>';
		else P_int = P_int + '>';
		g_sign = '+';
		pr_sign = false;
	}
	if(l_sign)
		P_int = g_sign + P_int;
	if(t_sign)
	{
		if(P_dec != null)
			P_dec = P_dec + g_sign;
		else
			P_int = P_int + g_sign;
	}
	if(f_dec != null && P_dec != null && P_dec.length != 0)
		return(P_int + '.' + P_dec);
	else return(P_int);
	}
	else
	return numcheck;
}

function trimAlpha(P_value)
{
	s_pos = P_value.indexOf('S');
	if(s_pos != -1)
	{
		if(s_pos > (P_value.length/2))
			t_sign = true;
		else
			l_sign = true;
	}
	if(P_value.indexOf('MI') != -1)
		t_sign = true;
	if(P_value.indexOf('PR') != -1)
		pr_sign = true;
	P_value = P_value.replace(/[MISPR]/g,'');
	if(P_value.indexOf('G') != -1)
	{
		delim_flg = true;
		var frmarr = P_value.split('G');
		delimarr = new Array();
		for(var i=0; i < frmarr.length; i++)
			delimarr[i] = frmarr[i].length;
		P_value = P_value.replace(/G/g,'');
	}
	return P_value;
}

function frm(num, frm, dir)
{
	var numarr = new Array();
	var frmarr = new Array(); 
	num = num.toString(); 
	if(frm.length < num.length)
	{
		alert(shortFormat);
		return null;
	}
	// padding Zeros if the length of the number is less than the Format length.
	if(num.length < frm.length) 
	{
		for(; num.length < frm.length;)
		{
			if(dir == 'rtl')
				num = '0' + num;
			else
				num = num + '0';
		}
	} 
	for(var j=0; j < num.length; j++)
	{
		numarr[j] = num.substr(j,1);
 		frmarr[j] = frm.substr(j,1);
	}
	for(var k=0; k < numarr.length; k++)
	{
		if(numarr[k] == 0)
			if(frmarr[k] == 9)
				numarr[k] = '?';
	}
	num = numarr.join(); num = num.replace(/\,/g,''); 
	if(delimarr) delimarr = delimarr.reverse(); 
	if(dir == 'ltr')
		while(num.charAt(num.length-1)=='?') num = num.substring(0,num.length-1); //supress trailing spaces
	else
	{
		if(delim_flg)
		{
			var ind =0; var tot =0;  
			for (var i = 0; i < delimarr.length; i++) 
			{
				ind = num.length-(tot+delimarr[i]); 
				num = num.substring(0,ind)+g_delim+num.substring(ind); 
				tot = tot + delimarr[i] + 1; 
			}
		}	
		while(num.charAt(0)=='?' || num.charAt(0)==',') num = num.substring(1,num.length); // supress leading spaces and commas
	}
	num = num.replace(/\?/g,'0'); // replace the in between question marks to zeros
	delimarr = null; delim_flg = false;
	return num;
}

