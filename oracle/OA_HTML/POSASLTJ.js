/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSASLTJ.js 115.0 99/10/14 16:19:21 porting shi $ */

function clear()
{
  document.POS_TOL_UPDATE.POS_TOL_ACTION.value = "CLEAR";
  document.POS_TOL_UPDATE.submit();
}

function toPosInt(str)
{
  var res = 1;
  var num = parseInt(str);

  if ((num ==0) && (str.length == 1))
    return num;

  for (var i=1; i<str.length; i++)
    res = 10 * res;

  if (isNaN(num) || (num < res))
   return -1;

  return num;
}

function addrows()
{
  var more_rows = toPosInt(document.POS_TOL_UPDATE.POS_MORE_ROWS.value);
  var msg = window.top.FND_MESSAGES["ICX_POS_VALID_NUMBER"];  

  if (more_rows == -1) {
    alert(msg);
    return;
  }

  document.POS_TOL_UPDATE.POS_TOL_ACTION.value = "ADDROWS";
  document.POS_TOL_UPDATE.submit();
}

    
function check_before_finish()
{
  var msg1 = window.top.FND_MESSAGES["ICX_POS_VALID_NUMBER"];
  var msg2 = window.top.FND_MESSAGES["ICX_POS_UNIQUE_NUMBER"];
  var more_rows = toPosInt(document.POS_TOL_UPDATE.POS_MORE_ROWS.value);

  if (more_rows == -1) {
    alert(msg1);
    return false;
  }
 
  if (parseInt(document.POS_TOL_UPDATE.POS_TOL_ROWS.value) <= 0)
    return true;

  var e1 = document.POS_TOL_UPDATE.POS_DAYS_IN_ADVANCE;
  var e2 = document.POS_TOL_UPDATE.POS_TOLERANCE;

  for(var i = 0;i < e1.length; i++) {
    var v1 = toPosInt(e1[i].value);
    var v2 = toPosInt(e2[i].value);

    if ((!(e1[i].value == "" && e2[i].value == "")) && 
        ((v1 == -1) ||
         (v2 == -1))) {
      alert (msg1);
      document.POS_TOL_UPDATE.POS_ERROR_ROW.value = (i+1) + "";
      document.POS_TOL_UPDATE.POS_TOL_ACTION.value = "ERROR";
      document.POS_TOL_UPDATE.submit();
      return false;
    }
  }
  

  for(var i = 0;i < document.POS_TOL_UPDATE.POS_DAYS_IN_ADVANCE.length; i++) {
    for(var k = 0;k < document.POS_TOL_UPDATE.POS_DAYS_IN_ADVANCE.length; k++) { 
      var e = document.POS_TOL_UPDATE.POS_DAYS_IN_ADVANCE;
      if (e[i].type == "text" && i != k) 
        if ((e[i].value != "") && (e[i].value == e[k].value)) {
          alert (msg2);
          document.POS_TOL_UPDATE.POS_ERROR_ROW.value = (i+1) + "";
          document.POS_TOL_UPDATE.POS_TOL_ACTION.value = "ERROR";
          document.POS_TOL_UPDATE.submit();
          return false;
        }
    }
  }
  
  return true;
}

function finish()
{  
  document.POS_TOL_UPDATE.POS_TOL_ACTION.value = "SUBMIT";
  document.POS_TOL_UPDATE.submit();
}

function show_error(p_msg)
{
  if (p_msg != "")
    alert(p_msg);
}






