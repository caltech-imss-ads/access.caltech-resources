/* $Header: czLLIt.js 115.29 2001/06/14 15:34:11 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function LogicItem ()
{
  this.parentConstructor = ListItem;
  this.parentConstructor ();
  
  this.type = 'LogicItem';
  this.columnDescriptors = null;
  this.cells = null;
  this.cellsHash = null;
  this.cellsBuilt = false;
  //Border around cells;
  this.borderWidth = 1;
  this.borderColor = 'black';
  this.priceVisible = false;
  this.atpVisible = false;
  
  this.modelItem = new LogicItemModel ();
}

function LogicItem_moreSetName (nm)
{
  this.modelItem.setName (nm);
  if (this.cellsBuilt) {
    var len = this.cells.length;
    var cell = null;
    for (var i = 0; i < len; i++) {
      cell = this.cells[i];
      if (cell.type != 'inputtext') {
        cell.setName (cell.name + '-' + nm);
      } else {
        cell.setName (nm + 'xC' + i);
      }
    }
  }
}

function LogicItem_moreSetHeight(ht)
{ 
  if (this.cells) {
    var len = this.cells.length;
    var cell = null;
    for (var i = 0; i < len; i++) {
      cell = this.cells[i];  
      if (cell.type == 'icon') {
        cell.setTop (Math.round(Math.abs(this.height-cell.height)/2));
      } else {
        cell.setHeight(this.height);
      }
    }
  }
}

function LogicItem_setColor (col)
{
  this.color = col;
  if (this.launched) {
    if (ie4)
      HTMLHelper.setForegroundColor(this.getHTMLObj(), col);
    else if (ns4)
      return;
  }
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setColor (col);    
    }
  }
}

/*
function LogicItem_moreSetColor (col)
{
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setColor (col);    
    }
  }
}

function LogicItem_moreSetBackgroundColor (col)
{
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setBackgroundColor (col);    
    }
  }
}
*/

function LogicItem_setSpanClass (cls)
{
  this.spanClass = cls;
  if (this.cellsBuilt) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      if (this.cells[i].type != 'icon') {
        this.cells[i].setSpanClass (cls);
      }
    }
  }
}

function LogicItem_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
	for (var i=0; i < this.cells.length; i++ ) {
  	  var absLeftCell = this.cells[i].getLeft(true);
	  var absTopCell = this.cells[i].getTop(true);
	  var rtCell = absLeftCell + this.cells[i].getWidth();
	  var btCell = absTop + this.cells[i].getHeight();
	  if ( (xpos >= absLeftCell) && (xpos <= rtCell) && (ypos >= absTopCell) && (ypos <= btCell))
	    return this.cells[i].objId;
	}
      }
  }
  return (null);
}

function LogicItem_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
 // var icon = this.cellsHash['state'];
 // if (icon) {
 //   icon.setSrc (this.srcUnknown);
 // }
}

function LogicItem_setCount (cnt)
{
  this.modelItem.setCount(cnt);
}

function LogicItem_getCount ()
{
  return (this.modelItem.getCount());
}

function LogicItem_setCaption (val)
{
  this.modelItem.setText (val);
}

function LogicItem_getCaption ()
{
  return (this.modelItem.getText ());
}

function LogicItem_setState (logicState)
{
  this.modelItem.setState(logicState);
}

function LogicItem_getState ()
{
  return (this.modelItem.getState());
}

function LogicItem_setVisibility (visible)
{
  this.modelItem.setVisibility(visible);
}

function LogicItem_getVisibility ()
{
  return (this.modelItem.getVisibility());
}

function LogicItem_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.left + this.width + 8;
    var tpos = this.top + (this.height - 16)/2;
    uicon.setDimensions (lpos, tpos, 16, 16);
    uicon.setReference (this.reference);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = this.reference.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = this.reference.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicItem_setPriceVisible (bVis, bWithLayout)
{	
  this.priceVisible = bVis;
  if (bVis) {
    if (! this.columnDescriptors) {
      this.getColumnDescriptors ();
    }
    if (! this.colDescHash ['price']) {
      this.updateColumnDescriptors ();
    }
    if (this.cellsBuilt) {
      if (! this.cellsHash ['price']) {
        var colDesc = this.colDescHash['price'];
        var newCell = this.createCell(colDesc);
        newCell.setName (colDesc.name + '-' + this.name);
        newCell.index = colDesc.index;
        this.cells[colDesc.index] = newCell;
        this.cellsHash[colDesc.name] = newCell;
        // add listeners;
        this.registerCellListeners(colDesc, newCell);
        if (this.launched) {
          //create price layer dynamically;
          newCell.runtimeRender (null, this, this.eventManager);
        }
        /*
        if (! bWithLayout) {
          //set left pos;
          var prevCell = this.cells[colDesc.index-1];
          newCell.setLeft (prevCell.getLeft() + prevCell.getWidth());
        }
        */
        this.updateColumnPosition ();
        this.fixDimensions ();
      }
      this.showColumn('price', bWithLayout);
    }
  } else {
    this.hideColumn('price', bWithLayout);
  }
}

function LogicItem_isPriceVisible ()
{
  return this.priceVisible;
}

function LogicItem_setPrice (val)
{
  this.modelItem.setPrice(val);
}

function LogicItem_getPrice ()
{
  return (this.modelItem.getPrice());
}

function LogicItem_setATPVisible (bVis, bWithLayout)
{
  this.atpVisible = bVis;
  if (bVis) {
    if (! this.columnDescriptors) {
      this.getColumnDescriptors ();
    }
    if (! this.colDescHash ['atp']) {
      this.updateColumnDescriptors ();
    }
    if (this.cellsBuilt) {
      if (! this.cellsHash ['atp']) {    
        var colDesc = this.colDescHash['atp'];
        var newCell = this.createCell(colDesc);
        newCell.setName (colDesc.name + '-' + this.name);
        newCell.index = colDesc.index;
        this.cells[colDesc.index] = newCell;
        this.cellsHash[colDesc.name] = newCell;
        // add listeners;
        this.registerCellListeners(colDesc, newCell);
        if (this.launched) {
          //create atp layer dynamically;
          newCell.runtimeRender (null, this, this.eventManager);
          if (this.type == 'BomItem') {
            this.updatePreferredWidth ();
            this.updateColumnPosition();
            this.fixDimensions();
            return;
          }
        }
        if (! bWithLayout) {
          //set left pos;
          var prevCell = this.cells[colDesc.index-1];
          newCell.setLeft (prevCell.getLeft() + prevCell.getWidth() + 2);
        }
      }
    }
    this.showColumn('atp', bWithLayout);
  } else {
    this.hideColumn('atp', bWithLayout);
  }
}

function LogicItem_isATPVisible ()
{
  return (this.atpVisible);
}

function LogicItem_setATP (dat)
{
  this.modelItem.setATP(dat);
}

function LogicItem_getATP ()
{
  return (this.modelItem.getATP());
}

function LogicItem_getImageSrc (logicState)
{
  var src;
  switch (logicState) {
  case 'utrue':
    src = this.srcUTrue;
    break;
  case 'ufalse':
    src = this.srcUFalse;
    break;
  case 'ltrue':
    src = this.srcLTrue;
    break;
  case 'lfalse':
    src = this.srcLFalse;
    break;
  case 'unknown':
    src = this.srcUnknown;
    break;
  }
  return src;
}

function LogicItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    // params -name, type, index, ht, wd, padLt, padTop, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('text', 'label', 0, this.height, 50, 0, 0, 'top', 'left', true, true);

    //this.columnDescriptors[1] = new ColumnDescriptor ('price', 'label', 1, this.height, 20, 0, 0, 'top', 'right', true, false);
    //this.columnDescriptors[2] = new ColumnDescriptor ('atp', 'label', 2, this.height, 20, 0, 0, 'top', 'center', true, false);
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function LogicItem_updateColumnDescriptors ()
{
  if (this.columnDescriptors) {
    var index = this.columnDescriptors.length;
    if (! this.colDescHash['price']) {
      if (this.priceVisible) {
        this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, this.height, 10, 1, 2, 'center', 'right', true, false);
        this.hashColumnDescriptor (this.columnDescriptors[index]);
        index++;
      }
    }
    if (! this.colDescHash['atp']) {
      if (this.atpVisible) {
        this.columnDescriptors[index] = new ColumnDescriptor ('atp', 'label', index, this.height, 10, 1, 2, 'center', 'left', true, false);
        this.hashColumnDescriptor (this.columnDescriptors[index]);
      }
    }
  }
}

function LogicItem_updateColumnPosition ()
{
  var colDesc = null;
  var len = this.columnDescriptors.length;
  if (! this.springColDesc) {
    var i = 0;
    while ((! this.springColDesc) && (i<len)) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.spring) {
        this.springColDesc = colDesc;
      }
      i++;
    }
  }
  var gap = 0;
  var rtMargin = 0;
  if (this._3DLook) {
    gap = 2;
    rtMargin = 1;
  } else if (this._flatLook) {
    gap = 1;
    rtMargin = 1;
  }
  var totWidth = gap;
  var totWidthUpToSpringCell =0;
  var springColDesc = this.springColDesc;

  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors[i];
    if (colDesc.defVisible) {
      if (colDesc == springColDesc) {
        totWidthUpToSpringCell = totWidth;
      }
      colDesc.setLeft (totWidth);
      totWidth += colDesc.getPrefWidth () + gap;
    }
  }
  totWidth -= gap;
  if ((this.width > totWidth) || (this.type == 'BomItem')) {
    springColDesc.setAvailWidth (springColDesc.getPrefWidth() + (this.width - totWidth - rtMargin));

    //now shift everything else from spring cell;
    totWidth = totWidthUpToSpringCell + springColDesc.getAvailWidth () + gap;
    for (var i=springColDesc.index + 1; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.defVisible) {
        colDesc.setLeft (totWidth);
      }
      totWidth += colDesc.getAvailWidth () + gap;
    }
  } else {
    //use the preferred with for the spring cell as available widht;
    springColDesc.setAvailWidth (springColDesc.getPrefWidth ());
  }
}

function LogicItem_buildCells(colDescs)
{
  // construct and position the cells from left to right
  if (colDescs) {
    var colDesc = null;
    var newCell = null;
    var curCell = null;
    var gap = 0;
    var rtMargin = 0;
    if (this._3DLook) {
      gap = 2;
      rtMargin = 1;
    } else if (this._flatLook) {
      gap = 1;
      rtMargin = 1;
    }
    var cellsWidth = gap;
    var len = this.columnDescriptors.length;
    this.cellsHash = new Array();
    this.cells = new Array();
    if(this.parentObj){
      if(this.parentObj.read)
        this.readOnly(this.parentObj.read);
    }    
    for (var i = 0; i < len; i++) {
      colDesc = this.columnDescriptors[i];
      // maintain the last cell for positioning;
      curCell = newCell; 
      newCell = this.createCell(colDesc);

      if ((this.type == 'BomItem') && (newCell.type == 'label') &&
          (! this.priceVisible)) {
        newCell.setWidth(this.getWidth() - (this.cells[i-1].getLeft() + this.cells[i-1].getWidth() + gap) - rtMargin);
      }

      newCell.index = colDesc.index;
      this.cells[colDesc.index] = newCell;
      this.cellsHash[colDesc.name] = newCell;
      if (newCell.type == 'inputtext') {
        newCell.setName (this.name + 'x' + colDesc.name);
      } else {
        newCell.setName (colDesc.name + '-' + this.name);
      }
      newCell.setLeft (cellsWidth);
      // add listener;
      this.registerCellListeners(colDesc, newCell);
    
      if (colDesc.defaultVisible) {
        newCell.show();
      }
      cellsWidth += newCell.getWidth () + gap;
      if (this.windowName) {
        newCell.windowName = this.windowName;
      }
    }
    this.cellsBuilt = true;
  }
}

function LogicItem_createCell(colDesc)
{
  // cell factory;
  var newCell;
  //var wd = colDesc.defWidth + colDesc.pdgLeft;
  switch (colDesc.type)
  {
    case 'inputtext':
      newCell = new InputText();
      if(!this.read)
        newCell.addListener('mouseupCallback',newCell);
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight(this.height);
      newCell.setTop(0);
      newCell.EDITABLE = true;
      newCell.setBackgroundColor(this.backgroundColor);
      if ((this.type != 'BomItem') || (! (this._3DLook || this._flatLook) ))
        newCell.createBorder(this.borderWidth, this.borderColor);
      //for a flat look bom item create a 0 pixel border in ie4 to hide 3D
      if (ie4 && (this.type == 'BomItem') && this._flatLook)
        newCell.createBorder ('0', this.borderColor);
      if (this._3DLook && (this.type == 'BomItem'))
        newCell.alwaysShowInpTxt = true;
      newCell.setValue('');
      newCell.setSpanClass (this.spanClass);
      newCell.standalone = false;
      newCell.setTabindex (-1);
      break;
    case 'icon':
      // for now,  we only support 16x16 icons
      newCell = new ImageButton();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (colDesc.defHeight);
      newCell.setTop (Math.round ((this.height - colDesc.defHeight)/2));
      newCell.setBackgroundColor(this.backgroundColor);
      // set to a default source
      newCell.setSrc(IMAGESPATH + 'czblank.gif');
      newCell.active = true;
      newCell.addListener ('onKeyPressCallback', this);
      newCell.setWindowName (this.windowName);
      newCell.setPadding (colDesc.pdgLeft, colDesc.pdgTop);
      break;
    case 'label': 
      newCell = new Prompt();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (this.height);
      newCell.setTop (0);
      newCell.setCaption('');
      newCell.setBackgroundColor(this.backgroundColor);
      newCell.setSpanClass (this.spanClass);
      newCell.setWindowName (this.windowName);
      newCell.setPadding (colDesc.pdgLeft, colDesc.pdgTop);
      break;
    case 'descImg':
      newCell = new ImageButton();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (this.height);
      newCell.setTop (0);
      //create a blank image
      newCell.setSrc(IMAGESPATH + 'czblank.gif');
      break;
  }
  if (this._3DLook) {
    newCell.setTop(2);
    newCell.setHeight(this.height - 3);
    newCell.setBackgroundColor(this.cellBgColor);
  } else if (this._flatLook) {
    newCell.setTop(1);
    newCell.setHeight(this.height - 2);
    newCell.setBackgroundColor(this.cellBgColor);
  }
  newCell.setParentObject(this);
  newCell.type = colDesc.type;
  newCell.colName = colDesc.name;
  
  //TODO: Rework this later
  if (this.moreSetProperties) {
    this.moreSetProperties (newCell);
  }
  return newCell;
}

function LogicItem_loadModelProperties(model)
{
  if (model) {  
    //Unlock the view to listen for events;
    this.unlockEvents ();
    var ttip = null;
    if (ie4)
      ttip = eval("self._" + model.getState() + "ToolTip");
    this.setCellValue('state', this.getImageSrc(model.getState()), ttip);
    this.setCellValue('descImg', model.getDescImage());
    var desc = this.formDescStr(model.getText(), model.getState());
    this.setCellValue('text', desc);
    this.setCellValue('count', model.getCount());
    this.setCellValue('atp', model.getATP());
    this.setCellValue('price', model.getPrice());
    this.setSelected (model.isSelected());
  }
}

function LogicItem_formDescStr (desc, state)
{
  var strHtml = '';
  if (this.bLogCol) {
    strHtml += '<SPAN class="';
    if (state == 'utrue')
      strHtml += UT_COLOR + '" ';
    else if (state == 'ltrue')
      strHtml += LT_COLOR + '" ';
    else if (state == 'ufalse')
      strHtml += UF_COLOR + '" ';
    else if (state == 'lfalse')
      strHtml += LF_COLOR + '" ';
    else
      strHtml += UN_COLOR + '" ';
    
    strHtml += 'ID="c-' + this.objId + '">';
  }
  if (this.bLogBld) {
    strHtml += '<SPAN class="';
    if (state == 'utrue')
      strHtml += UT_BOLD + '" ';
    else if (state == 'ltrue')
      strHtml += LT_BOLD + '" ';
    else if (state == 'ufalse')
      strHtml += UF_BOLD + '" ';
    else if (state == 'lfalse')
      strHtml += LF_BOLD + '" ';
    else
      strHtml += UN_BOLD + '" ';

    strHtml += 'ID="b-' + this.objId + '">';
  }
  strHtml += desc;
  if (this.bLogCol)
    strHtml += '</SPAN>';
  if (this.bLogBld)
    strHtml += '</SPAN>';
  
  return strHtml;
}

function LogicItem_setCellValue (colName, colValue, ttip)
{
  if (this.cellsBuilt) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      if (! curCell.isVisible ()) {
        curCell.show ();
        //this.showColumn (colName, true);
      }
      switch (curCell.type) {
      case 'inputtext':
        curCell.setValue(colValue);
        break;
      case 'icon':
      case 'descImg':
        curCell.setSrc(colValue);
        if (ttip)
          curCell.setToolTipText(ttip);
        break;
      case 'label':
        curCell.setCaption(colValue);
        break;
      }
    }
  }
}

function LogicItem_moreCSS ()
{
  var CSSStr = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    CSSStr += this.cells[i].generateCSS() + '\n';
  }
  if (this.unSatIcon) {
    CSSStr += this.unSatIcon.generateCSS() + '\n';
  }
  if (this.bNoIcon) {
    this.createTopLayer();
    CSSStr += this.topLay.generateCSS();
  }
  return CSSStr;
}

function LogicItem_createTopLayer ()
{
  this.topLay = new ImageButton();
  this.topLay.setName ('TLay-'+this.name);
  this.topLay.setParentObject (this);
  this.topLay.setSrc (IMAGESPATH + 'czBlank.gif');
  this.topLay.active = true;
  this.topLay.addListener ('onKeyPressCallback', this);
  this.topLay.type = 'toplay';
  this.topLay.stretch = true;
  if (this.type == 'OptionItem') {
    //give the full width of the layer
    this.topLay.setDimensions (0, 0, this.width, this.height);
  } else if ((this.type == 'CountedOptionItem') ||
             (this.type == 'BomItem')) {
    //get the column descriptor of the input text box and then position this
    //top layer next to it
    var colDesc = this.colDescHash['count'];
    var offset = colDesc.getAvailWidth();
    this.topLay.setDimensions (offset, 0, this.width-offset, this.height);
  }
}

function LogicItem_render ()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    this.addListener('onKeyDownCallback', this, 'super');
    this.addListener('onKeyUpCallback', this, 'super');
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
  } else if (this.bEnableKeyPressEvt) {
    this.addListener('onKeyPressCallback', this, 'super');
    sBuffer += ' onkeypress="javascript:doOnKeyPress(null,\''+ this.self + '\')"';    
  }
  sBuffer +='>';

  if(this.innerRender)
    sBuffer += this.innerRender();

  sBuffer += '</DIV>' + '\n';
  
  if (this.border)
    sBuffer += '</DIV>\n';

  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';
  
  return (sBuffer);  
}

function LogicItem_innerRender ()
{
  var sBuffer = '';
  if (this.spanNormal) {
    this.setSpanClass (this.spanNormal);
  }
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    sBuffer += this.cells[i].render () + '\n';
  }
  if (this.topLay)
    sBuffer += this.topLay.render() + '\n';

  return (sBuffer);
}

function LogicItem_moreLaunch (em)
{
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].launch(em);
  }
  if (this.unSatIcon)
    this.unSatIcon.launch ();
  if (this.topLay)
    this.topLay.launch();
}

function LogicItem_hilite(hiliteColor)
{
  if (!this.launched) 
    return;
  if (!hiliteColor) 
    hiliteColor = 'lightgrey';
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), hiliteColor);
  if(!this.backgroundColor)
    this.backgroundColor ='transparent';
  
  if (this.cells) {	
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      if (this.cells[i].type != 'inputtext')
        this.cells[i].hilite(hiliteColor);
    }
  }
}

function LogicItem_unhilite()
{
  if (!this.launched) 
    return;
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), this.backgroundColor);
  
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].unhilite();
    }
  }
}

function LogicItem_clearView()
{
  if (this.cells) {
    var len  = this.cells.length;
    for (var i=0; i<len; i++) {
      var cell = this.cells[i];
      switch (cell.type) {
      case 'icon':
      case 'descImg':
        cell.setSrc (IMAGESPATH + 'czblank.gif');
        break;
      case 'label':
        cell.setValue ('');
        break;
      case 'inputtext':
        cell.setValue ('');
        if (this.type == 'CountedOptionItem') {
          this.hideColumn(cell.colName);
        }
        break;
      }
    }
  }
}

function LogicItem_layoutColumns() 
{
  var gap = 0;
  if (this._3DLook) {
    gap = 2;
  } else if (this._flatLook) {
    gap = 1;
  }
  if (this.cells) {
    var cellsWidth = gap;
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      var cell = this.cells[i];
      if (cell.isVisible ()) {
        cell.setLeft (cellsWidth);
        cellsWidth += cell.getWidth () + gap;
      }
    }
  }
}

function LogicItem_propertyChangeCallback (change)
{
  if(this.modelItem!=null){
    if (change.property == 'selected') {
      this.setSelected(change.value);
    } else if (change.property == 'state') {
      var ttip = eval("self._" + change.value + "ToolTip");
      this.setCellValue ('state', this.getImageSrc(change.value), ttip); 
      if (this.bLogCol) {
        var descStr = this.formDescStr (this.modelItem.getText(), change.value);
        this.setCellValue ('text', descStr);
      }
    }else {
        this.setCellValue(change.property, change.value);
    }

  }
  return true;
}

function LogicItem_mouseupCallback (e, eSrc)
{
  if (this.locked) {
    if (eSrc.type == 'inputtext')
      return false;
    else
      return true;
  }
  // grow the height of the item to allow for the rendering of;
  // the input form element of inputtext for NS;
  if (eSrc.type == 'inputtext') {
    this.oldHeight = this.height;
    if (ns4 && this.height < 28) {
      this.setHeight (28);
    }	
  } else {
    if ((this.modelItem.getState() != 'utrue') && (this.modelItem.getState() != 'ltrue')) {
      //'guess' the client state until we get server notification.;
      this.modelItem.setState('utrue');
    } else {
      this.modelItem.setState('unknown');
    }
    this.notifyListeners ('actionMouseUp', this.modelItem, this);
  }
  return true;
}

function LogicItem_onchangeCallback(newValue, eSrc)
{
  if (this.locked)
    return true;
  
  if (ns4 && this.oldHeight && (! this._flatLook)) {
    this.setHeight (this.oldHeight); 
  }
  var curCell = eSrc;
  var colDesc = null;
  colDesc = this.columnDescriptors[curCell.index];
  if (colDesc) {
    this.modelItem.setValue(colDesc.name, newValue);
  }
  this.notifyListeners ('actionOnChange', this.modelItem, this);
  return true;
}

function LogicItem_onKeyPressCallback (e, eSrc)
{
  if ((eSrc.type == 'icon') || (eSrc.type == 'toplay') ||
      (eSrc.type == 'LogicCheckBox')) {
    var key = (ns4)? e.which : e.keyCode;
    if (key == 32) {
      if (eSrc.states == 1)
        eSrc.toggle(0);
      //IE window scrolls, this has to be there to prevent scrolling of the window
      if (ie4)
        e.returnValue = false;
     this.mouseupCallback (e, eSrc);
    }
  }
  return true;
}

function LogicItem_registerCellListeners(colDesc, cell) 
{
  if (colDesc.type == 'inputtext') {
    if(!this.read)
    {
      cell.addListener ('mouseupCallback', this, 'super');
      cell.addListener ('onchangeCallback', this);
    }
  }
  //cell.addListener ('mouseoverCallback', this);
  //cell.addListener ('mouseoutCallback', this);
  //cell.addListener ('mouseupCallback', this, 'super');  
}

function LogicItem_setWindowName (wndName)
{
  this.windowName = wndName;
  if (this.cellsBuilt) {
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].windowName = wndName;
    }
  }
}

function LogicItem_moreDestroy ()
{
  if (this.cells) {
    Utils.clearCollection (this.cellsHash);    
    this.cellsHash = null;
    delete this.cellsHash;
    
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].destroy ();

      this.cells[i] = null;
      delete this.cells[i];
    }
    this.cells.length = 0;
    this.cells = null;
    delete this.cells;

    Utils.clearCollection (this.columnDescriptors);
    this.columnDescriptors = null;
    delete this.columnDescriptors;

    Utils.clearCollection (this.colDescHash);
    this.colDescHash = null;
    delete this.colDescHash;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
}

function LogicItem_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;
  
  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld
}

function LogicItem_hide()
{
  this.visibility = false;
  if (this.border)
    this.border.visibility = false;

  if (this.launched) {
    var curCell = this.cellsHash['count'];
    if (curCell) {
      curCell.hide();
    }
    if (this.border) {
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.visibility);
      return;
    }
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function LogicItem_show() 
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    var curCell = this.cellsHash['count'];
    if (curCell) {
      curCell.show();
    }
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function LogicItem_getMaxNumFocusCells()
{
  return 1;
}

function LogicItem_setFocus(index)
{
  if (index) {
    var maxFoc = this.getMaxNumFocusCells();
    if (this.bNoIcon && (index == maxFoc)) {
      this.topLay.setFocus();
      return;
    }
    var cell = this.cells[index-1];
    if (cell) {
      cell.setFocus();
    }
  } else {
    this.getHTMLObj();
    this.block.focus();
  }
}

// construction
HTMLHelper.importPrototypes(LogicItem,ListItem);

// public methods
LogicItem.prototype.moreSetName = LogicItem_moreSetName;
LogicItem.prototype.moreSetHeight = LogicItem_moreSetHeight;
LogicItem.prototype.setColor = LogicItem_setColor;
//LogicItem.prototype.moreSetColor = LogicItem_moreSetColor;
//LogicItem.prototype.moreSetBackgroundColor = LogicItem_moreSetBackgroundColor;
LogicItem.prototype.setSpanClass = LogicItem_setSpanClass;
LogicItem.prototype.hilite = LogicItem_hilite;
LogicItem.prototype.unhilite = LogicItem_unhilite;
LogicItem.prototype.setImages = LogicItem_setImages;
LogicItem.prototype.setCaption = LogicItem_setCaption;
LogicItem.prototype.getCaption = LogicItem_getCaption;
LogicItem.prototype.setState = LogicItem_setState;
LogicItem.prototype.getState = LogicItem_getState;
LogicItem.prototype.setVisibility = LogicItem_setVisibility;
LogicItem.prototype.getVisibility = LogicItem_getVisibility;
LogicItem.prototype.setCount = LogicItem_setCount;
LogicItem.prototype.getCount = LogicItem_getCount;
LogicItem.prototype.setPrice = LogicItem_setPrice;
LogicItem.prototype.getPrice = LogicItem_getPrice;
LogicItem.prototype.setATP = LogicItem_setATP;
LogicItem.prototype.getATP = LogicItem_getATP;
LogicItem.prototype.setUnSatisfied = LogicItem_setUnSatisfied;
LogicItem.prototype.setPriceVisible = LogicItem_setPriceVisible;
LogicItem.prototype.isPriceVisible = LogicItem_isPriceVisible;
LogicItem.prototype.setATPVisible = LogicItem_setATPVisible;
LogicItem.prototype.isATPVisible = LogicItem_isATPVisible;
LogicItem.prototype.setWindowName = LogicItem_setWindowName;
LogicItem.prototype.setLogicStateProps = LogicItem_setLogicStateProps;

// private methods
LogicItem.prototype.getImageSrc = LogicItem_getImageSrc;
LogicItem.prototype.layoutColumns = LogicItem_layoutColumns;
LogicItem.prototype.registerCellListeners = LogicItem_registerCellListeners;
LogicItem.prototype.updateColumnDescriptors = LogicItem_updateColumnDescriptors;
LogicItem.prototype.updateColumnPosition = LogicItem_updateColumnPosition;
LogicItem.prototype.formDescStr = LogicItem_formDescStr;
LogicItem.prototype.createTopLayer = LogicItem_createTopLayer;
LogicItem.prototype.setFocus = LogicItem_setFocus;
LogicItem.prototype.getMaxNumFocusCells = LogicItem_getMaxNumFocusCells;

// overrides
LogicItem.prototype.hide = LogicItem_hide;
LogicItem.prototype.show = LogicItem_show;
LogicItem.prototype.buildCells = LogicItem_buildCells;
LogicItem.prototype.createCell = LogicItem_createCell;
LogicItem.prototype.loadModelProperties = LogicItem_loadModelProperties;
LogicItem.prototype.setCellValue = LogicItem_setCellValue;
LogicItem.prototype.getColumnDescriptors = LogicItem_getColumnDescriptors;
LogicItem.prototype.moreCSS = LogicItem_moreCSS;
LogicItem.prototype.render = LogicItem_render;
LogicItem.prototype.innerRender = LogicItem_innerRender;
LogicItem.prototype.moreLaunch = LogicItem_moreLaunch;
LogicItem.prototype.clearView = LogicItem_clearView;
LogicItem.prototype.hitTest = LogicItem_hitTest;
LogicItem.prototype.moreDestroy = LogicItem_moreDestroy;

// event handlers
LogicItem.prototype.mouseupCallback = LogicItem_mouseupCallback;
LogicItem.prototype.propertyChangeCallback = LogicItem_propertyChangeCallback;
LogicItem.prototype.onchangeCallback = LogicItem_onchangeCallback;
LogicItem.prototype.onKeyPressCallback = LogicItem_onKeyPressCallback;
