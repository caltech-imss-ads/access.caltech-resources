/* $Header: czInpTxt.js 115.24 2001/06/14 15:34:06 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash, K MacClay  Created.                     |
 |                                                                           |
 +===========================================================================*/

function InputText()
{
  this.parentConstructor = Prompt;
  this.parentConstructor();

  this.value = '';
  this.EDITABLE = false;
  this.editMode = false;
}

function InputText_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function InputText_onLaunchCall()
{
  if (this.standalone) {
    if(this.hFeature)
      this.hide();
    else
      this.show();
  }
  eval(this.self +'=this');
}

function InputText_render ()
{
  if (this.EDITABLE)
    this.addListener ('mouseupCallback', this);

  var sBuffer = "";
  if (ie4) {
    sBuffer = '<INPUT ID=' + this.objId;
    if (this.tabindex)
      sBuffer += ' tabindex="'+ this.tabindex + '"';
    if (this.spanClass)
      sBuffer += ' CLASS=' + this.spanClass;
    sBuffer += this.renderHelper();
    sBuffer += '>';
    return sBuffer;
  }

  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if ( this.alignment != null )
    sBuffer += ' ALIGN="' +this.alignment+ '"';

  sBuffer +='>';
  sBuffer += this.innerRender();
  sBuffer += '</DIV>';
  
  if (this.border)
    sBuffer += '\n</DIV>';
  
  return (sBuffer);
}


function InputText_innerRender()
{
  var sBuffer="";
  if (this.alwaysShowInpTxt) {
    //always show the input text
    sBuffer += '<FORM NAME="editForm" onSubmit="javascript:return false;">';
    sBuffer += '<INPUT NAME="'+this.objId+'" TYPE="text"';
    if (this.width)
      sBuffer += ' SIZE=' + Math.floor((this.width-10)/8);
    sBuffer += this.renderHelper();
    sBuffer +='></INPUT></FORM>';
    return sBuffer;
  }
  if(this.value != null) {
    sBuffer =  '<NOBR ID="SPN-'+this.name+'"' ;
    if(this.spanClass)
      sBuffer += ' CLASS="'+this.spanClass+'"';
    sBuffer += '>'+this.value + '</NOBR>';  
  }
  return sBuffer;
}

function InputText_renderHelper()
{
  var sBuffer = '';
  sBuffer += ' TYPE="text" VALUE="';
  if (this.value != null)
    sBuffer += this.value + '"';
  else
    sBuffer += '""';
  
  if (this.EDITABLE) {
    this.addListener ('onFocusCallback', this);
    this.addListener ('onBlurCallback', this);
    this.addListener ('onKeyPressCallback', this);
    if (ns4)
      var e = "event";
    else
      var e = null;
    sBuffer += ' onfocus="javascript:doOnFocus('+ e +',\''+ this.self + '\')"';
    sBuffer += ' onblur="javascript:doOnBlur('+ e +',\''+ this.self + '\')"';
    sBuffer += ' onkeypress="javascript:doOnKeyPress('+ e + ',\''+ this.self + '\')"';
  } else
    sBuffer += ' readonly=true';

  return sBuffer;
}

function InputText_doFocus(e)
{
  var sBuffer="";
  self.editObj = this;

  this.oldVal = this.value;
  
  if(this.editMode)
    return;

  if(this.block ==null)
    this.connect();		
  
  var obj = this.block;
  
  if (ns4 && (! this.alwaysShowInpTxt)) {
    sBuffer +='<FORM NAME ="editForm" onSubmit="javascript:return false;" ><INPUT NAME="'+this.objId+'" TYPE="text" ';
    
    if (this.width) {
      this.size = Math.floor((this.width-20) / 8);
    }
  
    sBuffer += ' SIZE ='+this.size;
  
    if (! this.windowName)
      sBuffer += ' onblur="javascript:'+this.self+'.doBlur()" ';
  
    sBuffer +=' ></INPUT></FORM>';

    if (this.windowName) {
      this.eventManager.addListener('docMouseDown', this, 'super');
      this.eventManager.addListener('docEnterKeyUp',this);
    }
    HTMLHelper.writeToLayer(obj,sBuffer);
  }

  var txtelem = null;  
  if (ns4) 
    txtelem = eval("obj.document.editForm."+this.objId);
  else if (ie4)
    txtelem = this.block;
  
  if(txtelem) {
    if(this.value != null)
      txtelem.value = this.value;
    else
      txtelem.value = "";
    
    if(ns4) {
      var hwnd = null;
      if (this.windowName) {
        hwnd = self._hwnd[this.windowName];
      } else {
        hwnd = self;
      }
      if (hwnd) {
        var scrTop = hwnd.pageYOffset;
        var scrLeft = hwnd.pageXOffset;
        if (! this.alwaysShowInpTxt)
          txtelem.focus();
        hwnd.scroll(scrLeft,scrTop);
      }
    }
    txtelem.select();
    this.textElement = txtelem;
    this.editMode = true;
  }  
}

function InputText_doBlur()
{
  if(this.editMode && this.textElement) {
    this.editMode = false;
    var obj = this.block;
    var newValue = this.textElement.value;
    var setVal = newValue;
    var gAmp = /&/g;
    newValue = newValue.replace(gAmp,'&amp;');
    var gLess = /</g;
    newValue = newValue.replace(gLess,'&lt;');
    var gGreat = />/g;
    newValue = newValue.replace(gGreat,'&gt;');
    var gQuot = /"/g;
    newValue = newValue.replace(gQuot,'&quot;');

    var res = true;
    
    if(newValue != this.value)
      res = this.notifyListeners('onchangeCallback',newValue,this);
    
    if(res)
      this.setValue(setVal);
    else 
      this.setValue(this.value);
        
    if (ns4 && (! this.alwaysShowInpTxt)) {
      if (this.oldHeight) {
        this.setHeight (this.oldHeight);
      }
      if (this.windowName) {
        this.eventManager.removeListener('docEnterKeyUp',this);
        this.eventManager.removeListener ('docMouseDown', this);
      }
    }
    this.notifyListeners('onBlur', newValue, this);
    this.textElement = null;
    self.editObj = null;
  }
}

function InputText_onFocusCallback (e)
{
  this.doFocus(e);
  if (this.parentObj && (this.parentObj.type == 'CountedOptionItem')) {
    this.parentObj.parentObj.setFocussedItem(this.parentObj);
  }
  return true;
}

function InputText_onBlurCallback (e)
{
  this.doBlur(e);
  return true;
}

function InputText_onKeyPressCallback (e)
{
  var key = (ns4)? e.which :e.keyCode;
  if (key == 13) {
    this.doBlur (e);
  }
  return true;
}

function InputText_docEnterKeyUp(e)
{
  if(this.editMode == true)
    this.doBlur(e);

  return true;
}

function InputText_docMouseDown (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var id = eSrc.id;
    if (! id) {
      if (eSrc.name)
        id = eSrc.name;
    }
    if ((id == this.name) || (id == this.objId))
      return (true);
    else
      this.doBlur (e);
  }
  else
    this.doBlur (e);
  return (true);
}

function InputText_mouseupCallback(e,src)
{
  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;
  
  if(src.EDITABLE && src.editMode == false) {
    if (ns4) {
      var ht = this.getHeight ();
      if (ht < 28) {
        this.oldHeight = ht;
        this.setHeight (28);
      }
    }
    src.doFocus(e);
  }
  return true;
}

function InputText_setValue(v)
{
  this.value = v;
  if (this.launched) {    
    if(this.block == null)
      this.connect();
    
    if (ie4 && this.block) {
      this.block.value = v;
      return;
    } else if (ns4 && this.alwaysShowInpTxt) {
      if (! this.textElement)
        this.textElement = eval("this.block.document.editForm."+this.objId);
      this.textElement.value = v;
      return;
    }
    var sBuffer =  '<NOBR '; 
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.name+'>'+this.value + '</NOBR>';
    
    if(this.block)
      HTMLHelper.writeToLayer(this.block,sBuffer);
  }
}

function InputText_getValue()
{
  return this.value;
}

//Extend from Prompt
HTMLHelper.importPrototypes(InputText,Prompt);

InputText.prototype.constructor 	= InputText;
InputText.prototype.render 	        = InputText_render;
InputText.prototype.innerRender 	= InputText_innerRender;
InputText.prototype.mouseupCallback     = InputText_mouseupCallback;
InputText.prototype.doFocus 		= InputText_doFocus;
InputText.prototype.doBlur 		= InputText_doBlur;
InputText.prototype.onFocusCallback     = InputText_onFocusCallback;
InputText.prototype.onBlurCallback      = InputText_onBlurCallback;
InputText.prototype.onKeyPressCallback  = InputText_onKeyPressCallback;
InputText.prototype.docEnterKeyUp 	= InputText_docEnterKeyUp;
InputText.prototype.docMouseDown        = InputText_docMouseDown;
InputText.prototype.setValue 		= InputText_setValue;
InputText.prototype.setCount 		= InputText_setValue;
InputText.prototype.getValue 		= InputText_getValue;
InputText.prototype.hideFeature 	= InputText_hideFeature;
InputText.prototype.onLaunchCall 	= InputText_onLaunchCall;
InputText.prototype.renderHelper 	= InputText_renderHelper;

