<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- $Header: czcmdcvt.xsl 115.3 2000/12/10 16:10:51 pkm ship     $-->

<xsl:template name="getParent">
	<xsl:choose>
		<xsl:when test="from-parent(application)">
			<xsl:text>mainWindow</xsl:text>
		</xsl:when>
		<xsl:when test="from-parent(config-messages)">
			<xsl:text>mainWindow</xsl:text>
		</xsl:when>
		<xsl:when test="from-parent(cmd-add)">
			<xsl:value-of select="../@target"/>
		</xsl:when>
		<xsl:when test="from-parent(cmd-replace)">
			<xsl:value-of select="../@target"/>
		</xsl:when>
		<xsl:when test="from-parent(row-data)">
			<xsl:value-of select="../../@target"/><xsl:text>.</xsl:text><xsl:value-of select="../@rtid"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="../@rtid"/>
		</xsl:otherwise>
	</xsl:choose>
<!-- how to get the parent node
	<xsl:text>pseudoParent</xsl:text>
general case: ../@rtid
exceptions:
parent node is an application element
	in that case, parent is mainWindow
parent is cmd-add, cmd-replace, ...
	parent is ../@target (verify)
parent is one of those controls for which @rtid does not map to the client control id
	grid row, (grid cell, main window)
parent is config-messages
	for all messages, parent is mainWindow
that's how to get the parent node -->
</xsl:template>

<xsl:template match="/">
	<xsl:apply-templates select="config-ui"/>
</xsl:template>

<xsl:template match="config-ui">
<!--xsl:pi name="xml">version="1.0" standalone="yes"</xsl:pi-->
	<xsl:apply-templates select="config-layout|config-state|config-messages"/>
</xsl:template>


<xsl:template match="config-layout">
	<xsl:apply-templates select="cmd-add|cmd-navigate|cmd-remove|cmd-clear|cmd-replace|application|cmd-remove-row"/>
</xsl:template>

<xsl:template match="cmd-add">
	<xsl:apply-templates select="panel|treeview|treenode|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|grid-column|row-data" mode="add"/>
</xsl:template>

<xsl:template match="cmd-navigate">
	<xsl:text>navigate(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="cmd-remove">
	<xsl:text>destroy(</xsl:text>
	<xsl:value-of select="@remove-id"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>destruct(</xsl:text>
	<xsl:value-of select="@remove-id"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="cmd-clear">
	<xsl:text>initialize(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="cmd-replace">
	<xsl:text>initialize(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="panel|treeview|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|grid-column|row-data|find-control" mode="add"/>
</xsl:template>

<xsl:template match="config-state">
	<xsl:apply-templates select="object-update|row-update|model-update|cmd-navigate"/>
</xsl:template>

<xsl:template match="row-update">
	<xsl:apply-templates select="col-data" mode="update"/>
</xsl:template>

<xsl:template match="object-update">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>, "value", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="cmd-remove-row">
	<xsl:text>destroy(</xsl:text>
	<xsl:value-of select="@target"/><xsl:text>.</xsl:text><xsl:value-of select="@remove-id"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>destruct(</xsl:text>
	<xsl:value-of select="@target"/><xsl:text>.</xsl:text><xsl:value-of select="@remove-id"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="application">
	<xsl:text>setProperty(mainWindow, "width", "</xsl:text>
	<xsl:value-of select="@width"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>setProperty(mainWindow, "height", "</xsl:text>
	<xsl:value-of select="@height"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:if test="@bgcolor">
		<xsl:text>setProperty(mainWindow, "bgcolor", "</xsl:text>
		<xsl:value-of select="@bgcolor"/>
		<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	</xsl:if>

	<xsl:apply-templates select="frameset"/>
</xsl:template>

<xsl:template match="frameset">
	<xsl:choose>
		<xsl:when test="../system-frame or from-parent(application)">
			<xsl:text>construct("</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>", </xsl:text>
			<xsl:call-template name="getParent"/>
			<xsl:text>, "panel", "")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "fillParent", "true")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>create(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>)</xsl:text>
			<xsl:text>
			</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:apply-templates select="frameset|frame"/>
		</xsl:when>
		<xsl:when test="../frame/@sizable='false'">
			<xsl:text>construct("</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>", </xsl:text>
			<xsl:call-template name="getParent"/>
			<xsl:text>, "panel", "")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "top", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
					<xsl:value-of select="../@height1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "left", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
					<xsl:value-of select="../@width1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "width", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@width1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@width2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "height", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@height1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@height2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>create(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>)</xsl:text>
			<xsl:text>
			</xsl:text>
			<xsl:text>
			</xsl:text>
	
			<xsl:apply-templates select="frameset|frame"/>
		</xsl:when>
		<xsl:when test="../frame/@sizable='true' or count(../frameset)=2">
			<xsl:text>construct("</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>", </xsl:text>
			<xsl:call-template name="getParent"/>
			<xsl:text>, "pane", "")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "top", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
					<xsl:value-of select="../@height1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "left", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
					<xsl:value-of select="../@width1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "width", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@width1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@width2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "height", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@height1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@height2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>create(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>)</xsl:text>
			<xsl:text>
			</xsl:text>
			<xsl:text>
			</xsl:text>
	
			<xsl:apply-templates select="frameset|frame"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="frameset|frame"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="frame">
	<xsl:choose>
		<xsl:when test="@sizable='false'">
			<xsl:text>construct("</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>", </xsl:text>
			<xsl:call-template name="getParent"/>
			<xsl:text>, "panel", "")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "top", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
					<xsl:value-of select="../@height1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "left", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
					<xsl:value-of select="../@width1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "width", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@width1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@width2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "height", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@height1"/>
			</xsl:if>
			<xsl:if test="position()=2">
					<xsl:value-of select="../@height2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>create(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>)</xsl:text>
			<xsl:text>
			</xsl:text>
			<xsl:text>
			</xsl:text>
	
			<xsl:apply-templates select="panel" mode="add"/>
		</xsl:when>
		<xsl:when test="@sizable='true'">
			<xsl:text>construct("</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>", </xsl:text>
			<xsl:call-template name="getParent"/>
			<xsl:text>, "pane", "")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "top", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
					<xsl:value-of select="../@height1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "left", "</xsl:text>
			<xsl:choose>
				<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
					<xsl:value-of select="../@width1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>0</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "width", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@width1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@width2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>setProperty(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>, "height", "</xsl:text>
			<xsl:if test="position()=1">
				<xsl:value-of select="../@height1"/>
			</xsl:if>
			<xsl:if test="position()=2">
				<xsl:value-of select="../@height2"/>
			</xsl:if>
			<xsl:text>")</xsl:text>
			<xsl:text>
			</xsl:text>

			<xsl:text>create(</xsl:text>
			<xsl:value-of select="@rtid"/>
			<xsl:text>)</xsl:text>
			<xsl:text>
			</xsl:text>
			<xsl:text>
			</xsl:text>
	
			<xsl:apply-templates select="panel" mode="add"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="panel" mode="add"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="panel" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "panel", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:if test="alignment/@fill-parent='true'">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "fillParent", "true")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="position">
		<xsl:apply-templates select="position"/>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="panel|treeview|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|find-control" mode="add"/>
</xsl:template>

<xsl:template match="treeview" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "tree", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:if test="alignment/@fill-parent='true'">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "fillParent", "true")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="position">
		<xsl:apply-templates select="position"/>
	</xsl:if>
	<xsl:apply-templates select="model-tree-icons"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
	
	<xsl:apply-templates select="treenode" mode="add"/>
</xsl:template>

<xsl:template match="model-tree-icons">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "open", "</xsl:text>
	<xsl:value-of select="@open"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "closed", "</xsl:text>
	<xsl:value-of select="@closed"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "open_unsatisfied", "</xsl:text>
	<xsl:value-of select="@open-unsatisfied"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "closed_unsatisfied", "</xsl:text>
	<xsl:value-of select="@closed-unsatisfied"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="treenode" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "treeNode", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:if test="text">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="image">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "iconLocation", "</xsl:text>
		<xsl:value-of select="image/@src"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
	
	<xsl:apply-templates select="treenode" mode="add"/>
</xsl:template>

<xsl:template match="button" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "button", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="caption">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="caption/text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="image">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "iconLocation", "</xsl:text>
		<xsl:value-of select="image/@src"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="checkBox" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "checkBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>

	<xsl:if test="caption">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="caption/text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="radioButton" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "radioButton", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="caption">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="caption/text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="text-control" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "textBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="number-control" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "textBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="@spin-button">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "spinButton", "</xsl:text>
		<xsl:value-of select="@spin-button"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@min-val">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "minVal", "</xsl:text>
		<xsl:value-of select="@min-val"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@max-val">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "maxVal", "</xsl:text>
		<xsl:value-of select="@max-val"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="text-object" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "label", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="text">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@active">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "active", "</xsl:text>
		<xsl:value-of select="@active"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@multiline">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "multiLine", "</xsl:text>
		<xsl:value-of select="@multiline"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="value-display" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "label", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="@text">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="image-object" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "image", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="image">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "location", "</xsl:text>
		<xsl:value-of select="image/@src"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:if test="@active">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "active", "</xsl:text>
		<xsl:value-of select="@active"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="option-group" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "optionGroup", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="@control-type">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "optionStyle", "</xsl:text>
		<xsl:value-of select="@control-type"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:if test="areastyle/@bgcolor">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "bgColor", "</xsl:text>
		<xsl:value-of select="areastyle/@bgcolor"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="option" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "option", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>
	<xsl:if test="caption">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "caption", "</xsl:text>
		<xsl:value-of select="caption/text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="find-control" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "panel", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="position"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="text-control|button|image|text-object" mode="add"/>
</xsl:template>

<xsl:template match="grid" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "grid", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:if test="alignment/@fill-parent='true'">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "fillParent", "true")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="position">
		<xsl:apply-templates select="position"/>
	</xsl:if>
	<xsl:if test="@column-count">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "nCols", "</xsl:text>
		<xsl:value-of select="@column-count"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@col-headers">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "colHeader", "</xsl:text>
		<xsl:value-of select="@col-headers"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@row-headers">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "rowHeader", "</xsl:text>
		<xsl:value-of select="@row-headers"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:apply-templates select="logic-state-display"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="grid-column"/>
</xsl:template>

<xsl:template match="logic-state-display">
	<xsl:apply-templates select="logic-icons"/>
</xsl:template>

<xsl:template match="logic-icons">
	<xsl:if test="@usage='check-list'">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../../@rtid"/>
		<xsl:text>, "ufalse", "</xsl:text>
		<xsl:value-of select="@ufalse"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../../@rtid"/>
		<xsl:text>, "utrue", "</xsl:text>
		<xsl:value-of select="@utrue"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../../@rtid"/>
		<xsl:text>, "unknown", "</xsl:text>
		<xsl:value-of select="@unknown"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../../@rtid"/>
		<xsl:text>, "ltrue", "</xsl:text>
		<xsl:value-of select="@ltrue"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../../@rtid"/>
		<xsl:text>, "lfalse", "</xsl:text>
		<xsl:value-of select="@lfalse"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
</xsl:template>

<xsl:template match="grid-column">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "gridColumn", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:if test="@index">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "iCol", "</xsl:text>
		<xsl:value-of select="@index"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="caption">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "headerCaption", "</xsl:text>
		<xsl:value-of select="caption/text/text()"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@control-type">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "controlType", "</xsl:text>
		<xsl:value-of select="@control-type"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@width">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="@rtid"/>
		<xsl:text>, "colWidth", "</xsl:text>
		<xsl:value-of select="@width"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="row-data" mode="add">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="../@target"/><xsl:text>.</xsl:text><xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "gridRow", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="../@target"/><xsl:text>.</xsl:text><xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:apply-templates select="col-data"/>
</xsl:template>

<xsl:template match="col-data">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="../@rtid"/><xsl:text>.</xsl:text><xsl:value-of select="@col-id"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "gridCell", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/><xsl:text>.</xsl:text><xsl:value-of select="@col-id"/>
	<xsl:text>, "colObjId", "</xsl:text>
	<xsl:value-of select="@col-id"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="../@rtid"/><xsl:text>.</xsl:text><xsl:value-of select="@col-id"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="col-data" mode="update">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@row-id"/><xsl:text>.</xsl:text><xsl:value-of select="@col-id"/>
	<xsl:text>, "value", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="model-update">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>, "unsatisfied", "</xsl:text>
	<xsl:value-of select="unsatisfied/text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@target"/>
	<xsl:text>, "branch_unsatisfied", "</xsl:text>
	<xsl:value-of select="branch-unsatisfied/text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="config-messages">
	<xsl:apply-templates select="contradiction-message|validation-message|warning-message|prompt-confirm-message|prompt-input-message|information-message"/>
</xsl:template>

<xsl:template match="contradiction-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "contradiction")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idYes", "</xsl:text>
	<xsl:value-of select="@id-yes"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idNo", "</xsl:text>
	<xsl:value-of select="@id-no"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="prompt-text|title-text|message-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="validation-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "validation")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idOk", "</xsl:text>
	<xsl:value-of select="@id-ok"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="title-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="warning-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "warning")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idOk", "</xsl:text>
	<xsl:value-of select="@id-ok"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="warning-text|title-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="prompt-confirm-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "confirm")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idConfirm", "</xsl:text>
	<xsl:value-of select="@id-yes-ok"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idNegate", "</xsl:text>
	<xsl:value-of select="@id-no-cancel"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="prompt-text|title-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="prompt-input-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "input")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idDone", "</xsl:text>
	<xsl:value-of select="@id-ok"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idCancel", "</xsl:text>
	<xsl:value-of select="@idcancel"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="prompt-text|title-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="information-message">
	<xsl:text>construct("</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>", </xsl:text>
	<xsl:call-template name="getParent"/>
	<xsl:text>, "messageBox", "")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "msgType", "information")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "idOk", "</xsl:text>
	<xsl:value-of select="@id-ok"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>

	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>, "errText", "</xsl:text>
	<xsl:apply-templates select="error-text"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:apply-templates select="message-text|title-text"/>

	<xsl:text>create(</xsl:text>
	<xsl:value-of select="@rtid"/>
	<xsl:text>)</xsl:text>
	<xsl:text>
	</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="message-text">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "msgText", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="error-text">
	<xsl:if test="position() > 2">
		<xsl:text>|</xsl:text>
	</xsl:if>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="prompt-text">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "promptText", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="title-text">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "title", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="warning-text">
	<xsl:text>setProperty(</xsl:text>
	<xsl:value-of select="../@rtid"/>
	<xsl:text>, "warnText", "</xsl:text>
	<xsl:value-of select="text()"/>
	<xsl:text>")</xsl:text>
	<xsl:text>
	</xsl:text>
</xsl:template>


<xsl:template match="position">
	<xsl:if test="@width">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../@rtid"/>
		<xsl:text>, "width", "</xsl:text>
		<xsl:value-of select="@width"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@height">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../@rtid"/>
		<xsl:text>, "height", "</xsl:text>
		<xsl:value-of select="@height"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@top">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../@rtid"/>
		<xsl:text>, "top", "</xsl:text>
		<xsl:value-of select="@top"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
	<xsl:if test="@left">
		<xsl:text>setProperty(</xsl:text>
		<xsl:value-of select="../@rtid"/>
		<xsl:text>, "left", "</xsl:text>
		<xsl:value-of select="@left"/>
		<xsl:text>")</xsl:text>
		<xsl:text>
		</xsl:text>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>