<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : context_layer_pos.js
// Description : Display Horizontal scrolling Layer at the Left hand side of
//               the screen
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------  -----------  ------- ------- ---------------------------------
// pbrimble    02-Dec-1999  110.00          Created.
// ==========================================================================
// $Header: context_layer_pos.js 115.0 2000/03/14 12:15:44 pkm ship   $
//

//declare variable contextLayer
var contextLayer = getLayerObject("holdLayer")

var layerInterval
if (browser.isIE && browser.version < 5) {
  layerInterval = 100
} else {
  layerInterval = 10
}

// Margin Offset
var leftOffset = 0

// Document display variables
var PX = -1000

function holdLayer() {

    // Position Context Layer if document reload or document horizontal
    //   display has changed
    if (PX != getLeftScrollPos()) {
        //hide(contextLayer)
        shiftOnHorizScroll(contextLayer,leftOffset)
        show(contextLayer)
        PX = getLeftScrollPos()
    }
}

// Execute layer function at intervals of 10 milliseconds
setInterval("holdLayer()",layerInterval);
//-->