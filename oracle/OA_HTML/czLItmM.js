/* $Header: czLItmM.js 115.6 2000/12/10 16:10:30 pkm ship     $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function ListItemModel (rtid, desc, isSelected, isVisible)
{
  this.type = 'ListItemModel';
  this.columnHash = new Array ();
  
  if (rtid == null) rtid = 'z' + ListItemModel.count++;
  if (desc == null) desc = '';
  if (isSelected == null) isSelected = false;
  if (isVisible == null) isVisible = true;
  
  this.name = rtid;
  this.selected = isSelected;
  this.visible = isVisible;
  this.setValue ('text', desc);
  this.index = null;
}

function ListItemModel_setName (rtid)
{
  this.name = rtid;
}

function ListItemModel_getName()
{
  return this.name;
}

function ListItemModel_setText(val)
{
  if (val) {
    this.setValue ('text', val);
  } else {
    this.setValue ('text', '');
  }
}

function ListItemModel_setValue (colName, colValue)
{
  var oldVal = this.columnHash[colName];
  if (oldVal != colValue) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:colName, value:colValue})) {
      this.columnHash[colName] = colValue;
    }		
  }
}

function ListItemModel_getValue (colName)
{
  return (this.columnHash[colName]);
}

function ListItemModel_getText()
{
  return this.getValue('text');
}

function ListItemModel_setIndex(val)
{
  if (val != this.index) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'Index', value:val})) {
      this.index = val;
      //it's the container's responsibility to maintain indexing.
    }	
  }
}

function ListItemModel_getIndex()
{
  return this.index;
}

function ListItemModel_setSelected (sel)
{
  if (sel != this.selected) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'selected', value:sel})) {
      this.selected = sel;
    }	
  }
}

function ListItemModel_isSelected ()
{
  return this.selected;
}

function ListItemModel_setVisible (vis)
{
  if (vis != this.visible) {
    if (this.notifyListeners ('propertyChangeCallback',{source:this, property:'Visible', value:vis})) {
      this.visible = vis;
    }	
  }	
}

function ListItemModel_isVisible ()
{
  return this.visible;
}

function ListItemModel_destroy ()
{
  if (this.columnHash) {
    Utils.clearCollection (this.columnHash);
    this.columnHash = null;
    delete this.columnHash;
    
    this.clearQueue ();
  }
}

HTMLHelper.importPrototypes(ListItemModel,Listener);

//Class members
ListItemModel.count = 0;

//public methods
ListItemModel.prototype.setName = ListItemModel_setName;
ListItemModel.prototype.getName = ListItemModel_getName;
ListItemModel.prototype.setText = ListItemModel_setText;
ListItemModel.prototype.getText = ListItemModel_getText;
ListItemModel.prototype.setIndex = ListItemModel_setIndex;
ListItemModel.prototype.getIndex = ListItemModel_getIndex;
ListItemModel.prototype.setSelected = ListItemModel_setSelected;
ListItemModel.prototype.isSelected = ListItemModel_isSelected;
ListItemModel.prototype.setVisible = ListItemModel_setVisible;
ListItemModel.prototype.isVisible = ListItemModel_isVisible;
ListItemModel.prototype.setValue = ListItemModel_setValue;
ListItemModel.prototype.getValue = ListItemModel_getValue;
ListItemModel.prototype.destroy = ListItemModel_destroy;