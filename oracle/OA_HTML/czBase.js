/* $Header: czBase.js 115.22 2001/06/14 15:33:46 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash K MacClay Created.                       |
 |                                                                           |
 +===========================================================================*/

if(!self.HTMLHelper) {
  alert('HelperFuncs.js not included');
}

function Base() 
{
  this.self = "Base" + Base.baseCount;
  Base.baseCount++;
  this.visibility = true;

  this.htmlObj = null;
  this.block = null;
  this._3DLook = false;
}

function Base_setReference(r)
{
  this.reference = r;
  if (this.border) {
    this.border.setReference (r);
    if (ns4) {
      this.reference = r + '.layers["' + this.border.name +'"].document';
    }
  }
  if (this.moreSetReference) {
    this.moreSetReference(this.reference);
  }
}

function Base_readOnly(bFlag)
{
   this.read = bFlag;
}

function Base_updateDone()
{
  //empty function
}

function Base_hideFeature()
{
  //empty function
}

function Base_setNoBorder(bBorder)
{
   this.noBorder = bBorder;
}

function Base_setLeft(lt)
{
  var oldLt;
  if(lt!=null) {
    if(isNaN(lt)) {
      alert (lt + " Not a Number");
      return;
    }
    oldLt = this.left;
    this.left = lt;
    if (this.border) {
      oldLt = this.border.left;
      this.border.setLeft (lt);
      this.left = this.borderWidth;
    }
    if (this.launched) {
      if (this.glue) 
        this.glue.setLeft(this, this.left);
      else 
        HTMLHelper.setDimensions(this.getHTMLObj(), this.left, null);
    }
    if(this.prefSystem && oldLt!= lt)
      this.prefSystem.notify(this,'left',lt);
    
    if (this.moreSetLeft)
      this.moreSetLeft (lt);
  }
}

function Base_setTop(tp)
{
  var oldTp;
  if(tp!=null) {
    if(isNaN(tp)) {
      alert (tp + " Not a Number");
      return;
    }
    oldTp = this.top;
    this.top = tp;
    if (this.border) {
      oldTp = this.border.top;
      this.border.setTop (tp);
      this.top = this.borderWidth;
    }
    if (this.launched) {
      if (this.glue)
        this.glue.setTop(this, this.top);
      else 
        HTMLHelper.setDimensions(this.getHTMLObj(),null, this.top);
    }
    if(this.prefSystem && oldTp!= tp)
      this.prefSystem.notify(this,'top',tp);
    
    if (this.moreSetTop)
      this.moreSetTop (tp);
  }
}

function Base_setName (nm)
{
  //name cannot be changed after launch (design time property);
  if (this.launched) {
    alert ("Read only property");
    return;
  }
  this.objId = this.name = nm;
  
  if (this.border)
    this.border.setName ('BDR-' + this.name);
  
  if (this.moreSetName) {
    this.moreSetName(this.name); 
  }
}

function Base_setWindowName (wndName)
{
  this.windowName = wndName;
}

function Base_setHeight(ht)
{
  if(ht!=null) {
    if(isNaN(ht)) {
      alert (ht + " Not a Number");
      return;
    }
    //Height should not be negative;
    if (ht < 0) {
      ht = 0;
    }
    var oldht = this.height;
    this.height = ht;
    if (this.border) {
      this.height = ht - this.borderWidth*2;
      this.border.setHeight (ht);
    }
    if(this.launched) {
      var clipRgn;
      if(!this.noclip)
        clipRgn = "rect(0px "+this.width+"px "+this.height+"px 0px)";
      
      HTMLHelper.setDimensions(this.getHTMLObj(),null,null,null,ht,clipRgn);
      
      if(oldht != ht) {
        if(this.prefSystem)
          this.prefSystem.notify(this,'height',ht);
        if(this.onresizecall)
          this.onresizecall(this);
      }
    }
    if (this.moreSetHeight)
      this.moreSetHeight (this.height);
  }
}

function Base_setWidth(wd)
{
  if (wd!=null) {
    if(isNaN(wd)) {
      alert (wd + " Not a Number");
      return;
    }
    //Width should not be negative;
    if (wd < 0) {
      wd = 0;
    }
    var oldwd = this.width;
    this.width = wd;
    if (this.border) {
      this.width = wd - this.borderWidth * 2;
      this.border.setWidth (wd);
    } 
    if(this.launched) {
      var clipRgn;
      if(!this.noclip)
        clipRgn = "rect(0px "+this.width+"px "+this.height+"px 0px)";
      
      HTMLHelper.setDimensions(this.getHTMLObj(),null,null,wd,null,clipRgn);
      
      if(oldwd !=wd) {
        if(this.onresizecall) 
          this.onresizecall(this);
        if(this.prefSystem)
          this.prefSystem.notify(this,'width',wd);
      }
    }
    if (this.moreSetWidth)
      this.moreSetWidth (this.width);
  }
}

function Base_setDimensions(lt, tp, wd, ht)
{
  if(lt!=null)
    this.setLeft(lt);
  if(tp!=null)
    this.setTop(tp);
  if(wd!=null)
    this.setWidth(wd);
  if(ht!=null)
    this.setHeight(ht);
}

function Base_createBorder (borderWidth, borderColor)
{
  if (this.launched) return;
  this.bBorder = true;
  this.borderVisible = true;
  
  if (borderWidth != null)
    this.borderWidth = borderWidth;
  else
    this.borderWidth = 1;
  
  if (borderColor != null)
    this.borderColor = borderColor;
  else if (this.color)
    this.borderColor = this.color
  else
    this.borderColor = 'black';
  
  if (ns4) {
    this.border = new Base ();
    this.border.setName ('BDR-' + this.name);
    this.border.setBackgroundColor (this.borderColor);
    if (! this.visibility) {
      this.border.visibility = false;
    }
    this.visibility = true;
    if(this.border)
      this.bCaptionWrap = false;
    
    //resize this controls diemensions and properties;
    this.setDimensions (this.left, this.top, this.width, this.height);
    this.setParentObject (this.parentObj);
    this.setReference (this.reference);
  }
}

function Base_setBorderVisible (bVis)
{
  if (!this.bBorder) 
    return;
  this.borderVisible = bVis;
  return;
  
  if (ns4) {
    if (bVis)
      this.border.show ();
    else
      this.border.hide ();
    return;
  } else if (ie4) {
    if (this.launched) {
      this.getHTMLObj ();
      if (bVis) {
        this.htmlObj.borderWidth = this.borderWidth;
        this.htmlObj.borderColor = this.borderColor;
        this.htmlObj.borderStyle = 'solid';
      } else {
        this.borderWidth = 0;
        this.htmlObj.borderWidth = 0;
      }
    }
  }
}

function Base_setBorderWidth (wd)
{
  this.borderWidth = wd;
  if (this.launched && ie4) {
    this.getHTMLObj ();
    this.htmlObj.borderWidth = wd;
  }
}

function Base_setBorderColor (col)
{
  this.borderColor = col;
  if (this.launched && ie4) {
    this.getHTMLObj ();
    this.htmlObj.borderColor = col;
  }
}

function Base_setParentObject(pObj)
{
  this.parentObj = pObj;
  if (this.border) {
    this.border.setParentObject (pObj);
    this.parentObj = this.border;
  }
}

function Base_setBackgroundColor(c)
{
  if(c!=null) {
    this.backgroundColor = c;
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),c);
    
    if (this.moreSetBackgroundColor) {
      this.moreSetBackgroundColor (c);
    }
  }
}

function Base_setColor (c)
{
  this.color = c;
  if (this.launched) {
    if (ie4)
      HTMLHelper.setForegroundColor(this.getHTMLObj(), c);
    else if (ns4)
      return;
    
    if (this.moreSetColor) {
      this.moreSetColor(c);
    }
  }
}

function Base_setValue(v)
{
  this.value = v;
  
  var sBuffer =  '<SPAN ID="SPAN-'+this.objId+'"';
  if(this.spanClass)
    sBuffer += ' CLASS="'+this.spanClass+'"';
  sBuffer += ' >' + this.value + '</SPAN>';
  
  if (this.launched) {
    if (this.getHTMLObj())
      if(ns4){
        this.block.document.open();
        this.block.document.write(sBuffer);
        this.block.document.close();
      } else if(ie4) this.block.innerHTML = sBuffer;
  }
}

function Base_getValue()
{
  return this.value;
}

function Base_setZIndex (index)
{
  this.zIndex = index;
  if (this.border)
    this.border.setZIndex (index);
  
  if (this.launched)
    this.getHTMLObj ().zIndex = index;
}

function Base_getZIndex ()
{
  return (this.zIndex);
}

function Base_getBackgroundColor()
{
  return this.backgroundColor;
}

function Base_getColor (c)
{
  return this.color;
}

function Base_getName()
{
  return this.name;
}

function Base_getId()
{
  return this.objId;
}

function Base_getReference()
{
  if (this.border) {
    return (this.border.reference);
  } else
    return (this.reference);
}

function Base_getLeft(bAbs)
{
  var lt=0;
  if (this.border) {
    if(bAbs && this.border.parentObj) {
      lt = this.border.parentObj.getLeft(true);
      return (this.border.left + lt);
    } else
      return (this.border.left);
  } else if(bAbs && this.parentObj) 
    lt = this.parentObj.getLeft(true);
  
  return(this.left+lt);
}

function Base_getTop(bAbs)
{
  var tp = 0;
  if (this.border) {
    if(bAbs && this.border.parentObj) {
      tp = this.border.parentObj.getTop(true);
      return (this.border.top + tp);
    } else
      return (this.border.top);
  } else if(bAbs && this.parentObj) 
    tp = this.parentObj.getTop(true);
  
  return(this.top+tp);		
}

function Base_getHeight()
{
  if (this.border)
    return (this.border.height);
  else
    return (this.height);
}

function Base_getWidth()
{
  if (this.border)
    return (this.border.width);
  else
    return (this.width);
}

function Base_getParentObject()
{
  if (this.border)
    return (this.border.parentObj);
  else
    return this.parentObj;
}

function Base_getHTMLObj()
{
  if(this.htmlObj == null) 
    this.connect();

  return this.htmlObj;
}

function Base_generateCSS()
{
  var CSSStr = "";

  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";

  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";

  if(this.height != null)
    CSSStr += "height:" + this.height + "; ";
      
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height)
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";
  
  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";
  
  //write border style attribute only IE browser;
  if (ie4 && this.bBorder) {
    CSSStr += "border-style:solid; ";
    if (this.borderWidth)
      CSSStr += "border-width:" + this.borderWidth + "; ";
    else
      CSSStr += "border-width:1; ";
    
    if (this.borderColor)
      CSSStr += "border-color:" + this.borderColor + "; ";
    else
      CSSStr += "border-color:black; ";
  }
  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  
  CSSStr += "}";
  
  if (ns4 && this.bBorder) {
    CSSStr += "\n" + this.border.generateCSS ();
  }  
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();
  
  return(CSSStr);
}

function Base_render()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if ( this.alignment != null )
    sBuffer += ' ALIGN="' +this.alignment+ '"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
  } else if (this.bEnableKeyPressEvt) {
    sBuffer += ' onkeypress="javascript:doOnKeyPress(null,\''+ this.self + '\')"';    
  }
  sBuffer +='>';

  if(this.innerRender)
    sBuffer += this.innerRender();
  else if (this.value)
    sBuffer += this.value;

  sBuffer += '</DIV>';
  
  if (this.border)
    sBuffer += '\n</DIV>';
  
  return (sBuffer);
}

function Base_runtimeRender(target, parentObj, em)
{
  if(this.parentObj) {
    if(this.parentObj.block==null)
      this.parentObj.connect();
    var obj = this.parentObj.block;
  }
  else
    var obj = null;
  
  var lyr = HTMLHelper.createLayer(this.name, obj, target);
  
  if(ns4)
    this.objId = lyr.id;

  this.block = lyr;
  
  if(ns4)
    this.htmlObj = lyr;
  else if(ie4)
    this.htmlObj = this.block.style;
  
  this.launched = true;
  this.setDimensions(this.left, this.top, this.width, this.height);
  this.setBackgroundColor(this.backgroundColor);
  
  if(this.visibility == false)
    this.hide();	
  else if(this.parentObj)
    this.htmlObj.visibility = 'inherit';
  else
    this.show();

  var sBuffer;
  if(this.innerRender) {
    sBuffer = this.innerRender(true);
    if(sBuffer != null)
      HTMLHelper.writeToLayer(this.block, sBuffer);
  }
  if (em) {
    this.launch (em);
  } else if(self.EventManager) {
    //If this control is placed in a different window, then get the event;
    //manager associated to that window;
    if ((this.windowName != null) && (this.windowName != 'undefined'))
      this.launch (self.EventManager(wndName));
    else
      this.launch (self.EventManager ());
  } else
    alert('EventManager.js not included');
}

function Base_launch(em)
{
  if(em) {
    this.eventManager = em;
    em.add(this);

    if (this.tabindex)
      em.addTabComp (this);
  }
  this.launched = true;
  
  if (this.border)
    this.border.launch (em);
  
  if(this.moreLaunch)
    this.moreLaunch(em);
  
  if(this.onLaunchCall)
    this.onLaunchCall();
}

function Base_write(strHtml)
{
  if (!this.launched) 
    return;
  if (this.block == null) 
    this.connect();
  
  HTMLHelper.writeToLayer(this.block, strHtml);
}

function Base_doMouseOver(e)
{
  if(this.tip && self.Tooltip)
    Tooltip.show(this.tip);
  
  return this.notifyListeners('mouseoverCallback',e, this);
}

function Base_doMouseOut(e)
{
  return this.notifyListeners('mouseoutCallback', e, this);
}

function Base_doMouseDown(e)
{
  return this.notifyListeners('mousedownCallback', e, this);
}

function Base_doMouseUp(e)
{
  if(this.tabManager)
    this.tabManager.gotFocus(this);

  return this.notifyListeners('mouseupCallback', e, this);
}

function Base_doMouseMove(e)
{
  return this.notifyListeners('mousemoveCallback', e, this);
}

function Base_doDoubleClick (e)
{
  this.notifyListeners ('doubleclickCallback', e, this);
  return (this.notifyListeners('ondoubleclick', e, this));
}

function Base_doOnFocus (e)
{
  return this.notifyListeners ('onFocusCallback', e , this);
}

function Base_doOnBlur (e)
{
  return this.notifyListeners ('onBlurCallback', e, this);
}

function Base_doOnKeyDown (e)
{
  return this.notifyListeners ('onKeyDownCallback', e, this);
}

function Base_doOnKeyPress (e)
{
  return this.notifyListeners ('onKeyPressCallback', e, this);
}

function Base_doOnKeyUp (e)
{
  return this.notifyListeners ('onKeyUpCallback', e, this);
}

function Base_moveTo(x, y)
{
  this.setLeft(x);
  this.setTop(y);
}

function Base_moveBy(x, y)
{
  var newLeft= this.left;
  var newTop = this.top;
  
  if(x) 
    newLeft +=x;
  if(y) 
    newTop +=y;

  this.moveTo(newLeft, newTop);
}

function Base_show() 
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function Base_hide() 
{
  this.visibility = false;
  if (this.border)
    this.border.visibility = false;
  
  if (this.launched) {
    if (this.border) {
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.visibility);
      return;
    }
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function Base_isVisible()
{
  if(this.visibility == null) 
    return true;
  return (this.visibility);
}

function Base_clipTo(t,r,b,l) 
{
  if (null == t) 
    t = this.clipValues('t');
  if (null == r) 
    r = this.clipValues('r');
  if (null == b) 
    b = this.clipValues('b');
  if (null == l) 
    l = this.clipValues('l');
  if (ns4) {
    this.getHTMLObj().clip.top = t;
    this.getHTMLObj().clip.right = r;
    this.getHTMLObj().clip.bottom = b;
    this.getHTMLObj().clip.left = l;
  }
  else if (ie4) 
    this.getHTMLObj().clip = "rect("+t+"px "+r+"px "+b+"px "+l+"px)";
  this.clipMode = true;
}

function Base_clipBy(t,r,b,l) 
{
  this.clipTo(this.clipValues('t')+t,this.clipValues('r')+r,this.clipValues('b')+b,this.clipValues('l')+l)
}

function Base_clipValues(which) {
  var retVal;
  if (ie4)
    var clipv = this.getHTMLObj().clip.split("rect(")[1].split(")")[0].split("px");
  switch(which)
    {
    case "t": 
      retVal =  (ns4)? this.getHTMLObj().clip.top : Number(clipv[0]);
      break;
    case "r":
      retVal = (ns4)? this.getHTMLObj().clip.right : Number(clipv[1]);
      break;
    case "b":
      retVal = (ns4)? this.getHTMLObj().clip.bottom : Number(clipv[2]);
      break;
    case "l": 
      retVal = (ns4)? this.getHTMLObj().clip.left : Number(clipv[3]);
      break;
    }
  return retVal;
}

function Base_slideTo(endx,endy,inc,speed,fn) 
{
  var distx =0;
  var disty =0;
  if(endx != null) distx = endx-this.left;
  if(endy != null) disty = endy-this.top;
  Base.slideStart(this,endx,endy,distx,disty,inc,speed,fn);
}

function Base_slideBy(distx,disty,inc,speed,fn) 
{
  var endx = this.left;
  var endy = this.top;
  if(distx!=null) endx += distx;
  if(disty!=null) endy += disty;
  Base.slideStart(this,endx,endy,distx,disty,inc,speed,fn);
}

function Base_slideStop()
{
  this.slideActive = false;
  if(this.slideTimeoutId) {
    clearTimeout(this.slideTimeoutId);
    delete this.slideTimeoutId;
  }
}

//Hiliting the basewidget with a color
function Base_hilite(hiliteColor)
{
  if (!this.launched) 
    return;
  if (!hiliteColor) 
    hiliteColor = 'yellow';
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), hiliteColor);
  if(!this.backgroundColor)
    this.backgroundColor ='transparent';
}

//Dehiliting the basewidget with a color
function Base_unhilite()
{
  if (!this.launched) 
    return;
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), this.backgroundColor);
}

function Base_isInside(x, y, t, r, b, l) 
{
  var tp,right,bottom,left;

  tp = this.top;
  right = this.left+this.width;
  bottom = this.top+this.height;
  left = this.left;

  if (r !=null) 
    right = r;
  if (b !=null) 
    bottom = b;
  if (t !=null) 
    tp =t;
  if (l !=null) 
    left =l;

  if (x > left) 
    if(x < right) 
      if(y > tp) 
        if(y < bottom) 
          return true;
  return false;
}

function Base_positionTo(obj, side, gap)
{
  if (gap == null) 
    gap = 0;

  switch(side)
    {
    case 2:
      this.setLeft(obj.left + obj.width+gap);
      if(this.top==null) 
        this.setTop(obj.top);
      break;
    case 4:
      this.setLeft(obj.left - this.width-gap);
      if(this.top==null ) 
        this.setTop(obj.top);
      break;
    case 1:
      this.setTop(obj.top - this.height-gap);
      if(this.left==null) 
        this.setLeft(obj.left);
      break;
    case 3:
      this.setTop(obj.top + obj.height+gap);
      if(this.left==null) 
        this.setLeft(obj.left);
      break;
    }
}

function Base_connect(block)
{
  var ref;
  if(block) {
    this.htmlObj = this.block = block;
    if(ie4)
      this.htmlObj = block.style;
    return;
  }
  if(this.parentObj) {
    if(!this.parentObj.block) 
      this.parentObj.connect();
    
    ref = 'this.parentObj.block';
  }
  else 
    ref = this.reference;
  if (ns4) {
    this.block = eval(ref + ".layers['" + this.objId + "']");
    if(this.block == null)
      alert ("Connect:Invalid Reference");
    else
      this.htmlObj = this.block;
  } else if (ie4) {
    this.block = eval(ref + ".all['"+ this.objId + "']");
    if(this.block == null)
      alert ("Connect:Invalid Reference");
    else
      this.htmlObj = this.block.style;
  }
}

function Base_copy(obj)
{
  var nubase = new Object();

  for(prop in obj) {
    eval("nubase."+prop+" = obj."+prop);
  }
  nubase.name = nubase.name + 'c'+obj.numCopy;
  nubase.name = nubase.name + 'c'+obj.numCopy;
  obj.numCopy++;
  return nubase;
}

//private method to get style object from the styles collection
function Base_getStyleObject (styleName, bCreate)
{
  var styl;
  //Try to get the syle object from the styles collection for the specified;
  //style name. If not found and bCreate is true then create a new one;
  if (styleName) {
    styl = Styles(this.windowName).getStyle (styleName);
    if (styl != null)
      return (styl);
    else if (bCreate) {
      styl = Styles(this.windowName).createStyle (styleName);
      return (styl);
    }
  }
  if (! this.divClass) {
    styl = Styles(this.windowName).getStyle (this.name + 'Style');
    if (styl == null) 
      styl = Styles(this.windowName).createStyle (this.name + 'Style');
    
    this.divClass = this.name + '_Style';
  } else {
    styl = Styles(this.windowName).getStyle (this.divClass);
    if ((styl == null) && bCreate)
      styl = Styles(this.windowName).createStyle (this.divClass);
  }
  
  return styl;
}

function Base_setFont (font)
{
  if (this.launched)
    return;
  
  this.spanClass = this.name+'Style';
  var style = this.getStyleObject (this.spanClass, true);
  this.font = font;
  style.setAttribute ('font-family', font.family);
  style.setAttribute ('font-size', font.size);
  style.setAttribute ('font-style', font.style);
  style.setAttribute ('font-weight', font.weight);
}

function Base_setAlignment (align)
{
  if (this.launched)
    return;
  
  this.spanClass = this.name+'Style';
  var style = this.getStyleObject (this.spanClass, true);
  this.alignment = align; 
  style.setAttribute ('text-align', align);
}

function Base_getFont ()
{
  return (this.font);
}

function Base_setPadding (pdgLt, pdgTp, pdgRt, pdgBot)
{
  if (this.launched)
    return;
  this.divClass = this.name + 'DivStyle';
  var style = this.getStyleObject(this.divClass, true);
  if (pdgLt != null)
    style.setAttribute('padding-left', pdgLt);
  if (pdgTp != null)
    style.setAttribute('padding-top', pdgTp);
  if (pdgRt != null)
    style.setAttribute('padding-right', pdgRt);
  if (pdgBot != null)
    style.setAttribute('padding-bottom', pdgBto);
}

function Base_setStyleValue (styleAttr, val)
{
  if (this.launched)
    return;
  var style = this.getStyleObject ();
  if (style == null) {
    alert ("Style object not found");
    return;
  }
  style.setAttribute (styleAttr, val)
}

function Base_getStyleValue (styleAttr)
{
  if (!this.divClass) 
    return null;
  
  var style = this.getStyleObject ();
  if (style == null) {
    alert ("Style object not found");
    return;
  }
  return (style.getAttribute (styleAttr));
}

function Base_set3DLook (bVal)
{
  this._3DLook = bVal;
}

function Base_is3DLookSet ()
{
  return this._3DLook;
}

function Base_setFlatLook (bVal)
{
  this._flatLook = bVal;
}

function Base_isFlatLookSet ()
{
  return this._flatLook;
}

function Base_setTabindex (tabindex)
{
  this.tabindex = tabindex;
}

function Base_getTabindex()
{
  return this.tabindex;
}

function Base_enableKeyPressEvents()
{
  eval(this.self +'=this');
  this.bEnableKeyPressEvt = true;
}

function Base_enableKeyBoardEvents()
{
  eval(this.self +'=this');
  this.bEnableKeyBoardEvt = true;
}

function Base_setFocus()
{
  if (this.launched) {
    this.getHTMLObj();
    this.block.focus();
  }
}

function Base_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
      return (this.name);
    }
  }
  return (null);
}

function Base_destroy ()
{
  if (! this.bDestroyed) {
    this.htmlObj = null;
    delete this.htmlObj;
    
    this.block = null;
    delete this.block;
    
    this.eventManager = null;
    delete this.eventManger;
    
    this.clearQueue ();
    
    if (this.moreDestroy) {
      this.moreDestroy ();
    }
    this.bDestroyed = true;
  }
}

//Extend from Listener class
HTMLHelper.importPrototypes(Base,Listener);

//Class Method
Base.baseCount 	= 0; 
Base.slideStart	= Base_slideStart; 
Base.slide	= Base_slide;

//Instance Methods
Base.prototype.setReference 	= Base_setReference;
Base.prototype.setDimensions 	= Base_setDimensions;
Base.prototype.setLeft 		= Base_setLeft;
Base.prototype.setTop 		= Base_setTop;
Base.prototype.setWidth 	= Base_setWidth;
Base.prototype.setHeight 	= Base_setHeight;
Base.prototype.setParentObject 	= Base_setParentObject;
Base.prototype.setName 		= Base_setName;
Base.prototype.setWindowName = Base_setWindowName;
Base.prototype.getName 		= Base_getName;
Base.prototype.getId 		= Base_getId;
Base.prototype.getReference 	= Base_getReference;
Base.prototype.getLeft 		= Base_getLeft;
Base.prototype.getTop 		= Base_getTop;
Base.prototype.getWidth 	= Base_getWidth;
Base.prototype.getHeight	= Base_getHeight;
Base.prototype.getParentObject 	= Base_getParentObject;
Base.prototype.render 		= Base_render;
Base.prototype.runtimeRender 	= Base_runtimeRender;
Base.prototype.launch 		= Base_launch;
Base.prototype.doMouseOver 	= Base_doMouseOver;
Base.prototype.doMouseOut 	= Base_doMouseOut;
Base.prototype.doMouseDown	= Base_doMouseDown;
Base.prototype.doMouseUp 	= Base_doMouseUp;
Base.prototype.doMouseMove 	= Base_doMouseMove;
Base.prototype.doDoubleClick 	= Base_doDoubleClick;
Base.prototype.doOnFocus 	= Base_doOnFocus;
Base.prototype.doOnBlur 	= Base_doOnBlur;
Base.prototype.doOnKeyDown 	= Base_doOnKeyDown;
Base.prototype.doOnKeyPress 	= Base_doOnKeyPress;
Base.prototype.doOnKeyUp 	= Base_doOnKeyUp;
Base.prototype.generateCSS 	= Base_generateCSS;
Base.prototype.setBackgroundColor = Base_setBackgroundColor;
Base.prototype.getBackgroundColor = Base_getBackgroundColor;
Base.prototype.moveTo 		= Base_moveTo;
Base.prototype.moveBy 		= Base_moveBy;
Base.prototype.slideTo 		= Base_slideTo;
Base.prototype.slideBy 		= Base_slideBy;
Base.prototype.slideStop 	= Base_slideStop;
Base.prototype.clipTo 		= Base_clipTo;
Base.prototype.clipBy 		= Base_clipBy;
Base.prototype.clipValues	= Base_clipValues;
Base.prototype.show 		= Base_show;
Base.prototype.hide 		= Base_hide;
Base.prototype.hilite 		= Base_hilite;
Base.prototype.unhilite 	= Base_unhilite;
Base.prototype.isInside 	= Base_isInside;
Base.prototype.isVisible 	= Base_isVisible;
Base.prototype.positionTo 	= Base_positionTo;
Base.prototype.connect 		= Base_connect;
Base.prototype.setValue 	= Base_setValue;
Base.prototype.getValue 	= Base_getValue;
Base.prototype.getHTMLObj 	= Base_getHTMLObj;
Base.prototype.write 		= Base_write;
Base.prototype.setColor 	= Base_setColor;
Base.prototype.setNoBorder 	= Base_setNoBorder;
Base.prototype.getColor 	= Base_getColor;
Base.prototype.setZIndex 	= Base_setZIndex;
Base.prototype.getZIndex 	= Base_getZIndex;
Base.prototype.setFont 		= Base_setFont;
Base.prototype.setAlignment     = Base_setAlignment;
Base.prototype.getFont 		= Base_getFont;
Base.prototype.setPadding       = Base_setPadding;
Base.prototype.setStyleValue	= Base_setStyleValue;
Base.prototype.getStyleValue	= Base_getStyleValue;
Base.prototype.updateDone = Base_updateDone;
Base.prototype.hideFeature = Base_hideFeature;
Base.prototype.createBorder = Base_createBorder;
Base.prototype.setBorderVisible	= Base_setBorderVisible;
Base.prototype.setBorderWidth = Base_setBorderWidth;
Base.prototype.setBorderColor = Base_setBorderColor;
Base.prototype.set3DLook = Base_set3DLook;
Base.prototype.is3DLookSet = Base_is3DLookSet;
Base.prototype.setFlatLook = Base_setFlatLook;
Base.prototype.isFlatLookSet = Base_isFlatLookSet;
Base.prototype.setTabindex = Base_setTabindex;
Base.prototype.getTabindex = Base_getTabindex;
Base.prototype.enableKeyPressEvents = Base_enableKeyPressEvents;
Base.prototype.enableKeyBoardEvents = Base_enableKeyBoardEvents;

Base.prototype.hitTest = Base_hitTest;
Base.prototype.destroy = Base_destroy;
Base.prototype.readOnly = Base_readOnly;
Base.prototype.setFocus = Base_setFocus;

//Private Method
Base.prototype.getStyleObject	= Base_getStyleObject;

//actual sliding functions
function Base_slide(obj,dx,dy,endx,endy,num,i,speed,fn) 
{
  if(!obj.slideActive)
    return;
  if(i++ < num) 
    {
      obj.moveBy(dx,dy);


      var res = obj.notifyListeners('onSlide',speed);
      if(res>1)
        speed = res;
      if (obj.slideActive) 
        {
          eval(obj.self +'= obj');
          obj.slideTimeoutId = setTimeout("Base_slide("+obj.self+","+dx+","+dy+","+endx+","+endy+","+num+","+i+","+speed+",\""+fn+"\")",speed);

        }
    }
  else 
    {	
      obj.moveTo(endx,endy);
      obj.slideActive = false;
      obj.slideProgress = false;
      obj.notifyListeners('onSlide',speed);
      if(obj.slideTimeoutId)
        delete obj.slideTimeoutId;
      if(fn) 
        eval(fn);
      obj.notifyListeners('onSlideEnd',speed);
    }
}

//start sliding operation
function Base_slideStart(obj, endx,endy,distx,disty,inc,speed,fn) 
{
  obj.slideStop();
  if (!inc) 
    inc = 10;
  if (!speed) 
    speed = 20;
  var num = Math.sqrt(Math.pow(distx,2) + Math.pow(disty,2))/inc;
  if (num==0) 
    return;
  var dx =distx/num;
  var dy =disty/num;

  if (!fn) 
    fn = null;
  obj.slideActive = true;

  Base_slide(obj, dx,dy,endx,endy,num,1,speed,fn);
}

