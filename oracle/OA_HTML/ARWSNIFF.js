/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================+
| FILENAME                                                         |
|  ARWSNIFF.js  Javascript functions for sniffing browser type     |
|               and versions. Javascript code can differ based on  |
|               the above.                                         |
| HISTORY                                                          |
|   10-OCT-99       gjayanth       Created.			   |
|                                                                  | 
+==================================================================*/
/* $Header: ARWSNIFF.js 115.0.1150.1 2000/01/11 16:26:44 pkm ship      $ */
// global for brower version branching
var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))
