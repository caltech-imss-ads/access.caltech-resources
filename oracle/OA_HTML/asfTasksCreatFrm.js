/*********************************************************************
 * Javascript functions for To Do Create/Update
 *********************************************************************/
 <!-- $Header: asfTasksCreatFrm.js 115.7 2000/12/29 12:07:06 pkm ship   $ -->

/**
 * handle click on checkBox for Private Flag
 */

function setPrivateFlag(secObjName)
{
  var theForm = document.forms['TasksCreate'];

  var privateFlag = secObjName + 'privateFlag';

  var checkBox = 'privateFlag';

  if(theForm.elements[checkBox].checked)
  {
    theForm.elements[privateFlag].value = 'Y';
  }
  else
  {
    theForm.elements[privateFlag].value = 'N';
  }
}
