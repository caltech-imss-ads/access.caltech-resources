<!-- hide the script contents from feeble browsers
// ==========================================================================
// Change History
// Version  Date         Author      Bug #    Remarks
// -------  -----------  ----------- -------- -------------------------------
// 110.15   22-Dec-2000  lma         1414461  Validate numeric fields on
//                                            next_button_onClick 
// 110.08   10-NOV-2000  snachuri   1474955  Modified function next_button_onClick
//                                           for the html form name from hourForm to
//                                           hoursForm
// 110.07   26-SEP-2000  snachuri   1390539  Modified validate_work_hours function 
//                                           to allow decimal in the work hours.
// 110.06   01-NOV-1999  snachuri   1059523  Added New function work_hours_changed() 
// 110.05   26-AUG-1999  snachuri   934577   Commented out the code that set
//                                          the Target for cancel_button_onclick
//                                           The Target is set in hr_hours_web
//                                           display_container_bottom_frame
// 110.04   01-JUL-1999  jchu                 Commented out code that checked
//                                            if form values has been changed
//                                            , may reverted back later
//
// 110.03   25-JUN-1999  jchu                 Added function to check 
//                                            effective date
//
// 110.02   23-JUN-1999  jchu                 Added code to clear the bottom
//                                            frame when a button is pressed
//
// 110.01   19-Jun-1999  jchu                 Added chk_pay_basis function
//
// 110.00   14-Jun-1999  jchu                 Created
// --------------------------------------------------------------------------
// $Header: hrhrchgw.js 115.3 2001/01/17 11:48:44 pkm ship $


// Invoke by the menu icon on the top frame
// It checks to see if the values in the form has changed or not
// After user confirms the warning, it returns the user
// back to the main menu
function main_menu_gif_onClick (msg) {
		 document.hoursForm.p_result_code.value="MAIN_MENU";
        // if (document.hoursForm.p_form_changed.value=="Y")
        //{
           if (confirm(msg))
            {
			 document.hoursForm.target= '_top';
             document.hoursForm.submit();
            }
         //}
         //else
         //{
		 //    document.hoursForm.target= '_top';
         //    document.hoursForm.submit();
         //}
}

// Invoke by the reset button
// Reset the form values to its initial values
function reset_button_onClick () { 
  document.hoursForm.reset();
}


// container_bottom frame when the user clicks
// on the 'Cancel' button.
function cancel_button_onClick(msg) {
 
  //if (document.hoursForm.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
		document.hoursForm.p_result_code.value = 'CANCEL';
	//	document.hoursForm.target= '_top';
        document.hoursForm.submit();
     }
  //} else {
  //      top.clear_container_bottom_frame();
  //		document.hoursForm.p_result_code.value = 'CANCEL';
  //		document.hoursForm.target= '_top';
  //      document.hoursForm.submit();
// }
}


// Invoke by the back button
// Submits the form with p_result_code set to PREVIOUS
// Transitions to the previous activity
function back_button_onClick (msg,p_target) {
 

 //if (document.hoursForm.p_form_changed.value=="Y") { 
 
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
		document.hoursForm.p_result_code.value = 'PREVIOUS';
		document.hoursForm.target= p_target;
        document.hoursForm.submit();
     }
  //} else {
    //    top.clear_container_bottom_frame();
		//document.hoursForm.p_result_code.value = 'PREVIOUS';
		//document.hoursForm.target= p_target;
       // document.hoursForm.submit();
 // }
}


// Invoke by the next button
// Validates the form values on the client side
// the fields it checks are: p_work_hours, p_normal_time_start,
// p_normal_time_finish
function next_button_onClick(msg1, msg2, decimalPoint) {
	var error_msg = message_array[1] + "\n\n";
	var l_input;

  //if (document.hoursForm.p_form_changed.value == "Y") {
   if(checkMandatoryFields(document.hoursForm, requiredFieldList,
fieldPromptList, msg1)) {
        if(top.checkNumericFields(document.hoursForm,numericFieldList,
                          numericPromptList, msg2,decimalPoint))
        {
     
        if (document.hoursForm.P_PERSON_NAME == ' ' ||
		    document.hoursForm.P_PERSON_NAME == '') {
			document.hoursForm.p_person_id = '';
           }
		   
		if (!(validate_work_hours())) {
			error_msg += message_array[3] + "\n";
			l_input = document.hoursForm.p_work_hours;
			}
			
		if (!(validate_hours("p_normal_time_start"))) {
			error_msg += message_array[4] + "\n";
			if (!l_input) l_input= document.hoursForm.p_normal_time_start;
			}
			
		if (!(validate_hours("p_normal_time_finish"))) {
			error_msg += message_array[5] + "\n";
			if (!l_input) l_input= document.hoursForm.p_normal_time_finish;
			}
	
		if (!compare_time()) {
			error_msg += message_array[6] + "\n";
			if (!l_input) l_input= document.hoursForm.p_normal_time_start;
		}
		
		if (error_msg != (message_array[1]+"\n\n") ) {
			error_msg += "\n" + message_array[2];
			alert(error_msg );
			l_input.focus();
			l_input.select();
			}
		else {
			top.clear_container_bottom_frame();
			document.hoursForm.submit();
			}
          }
     }
//}
	
	//if (document.hoursForm.p_form_changed.value != "Y") {
	   // Please enter some data before proceeding.
	//   alert(message_array[9]);

	// }
		 
}

// Invoke when a value in the form has changed
// Works with the cancel_button_onClick function
function set_form_changed () {
     document.hoursForm.p_form_changed.value="Y";
}


// Invoke by next_button_onClick js function
// Checks the whether work hour value is entered as numbers only
function validate_work_hours(){
var answer = 1
var input_str=document.hoursForm.p_work_hours.value;
var input_len=input_str.length;
oneDecimal = false;

       for (var i = 0; i < input_len; i++) {
            var ch = input_str.substring(i, i + 1);
           // condition to check for decimal in the number format.
           if(ch=="." &&!oneDecimal){
             oneDecimal = true; 
             continue;
            }
            if ((ch < "0" )||( ch > "9" )) {
                answer = 0;
				return answer;
            }
    }
    return answer;
}


// Invoke by next_button_onClick js function
// Checks start time and end time format
function validate_hours(p_time) {
var answer = 1;
var time = eval("document.hoursForm." + p_time + ".value");
var len = time.length;
var re = /\d\d:\d\d/
	if (time != "") {
		if (!(re.test(time))) {
			var answer = 0;
			return answer;
		}
		
		if (re.test(time)) {
		    timeArr = time.split(":");
			
			if ( (timeArr[0] < 0) || (timeArr[0] > 23) || 
			     (timeArr[1] < 0) || (timeArr[1] > 59) ) {
			   var answer = 0;
			   return answer;
			}
	    }
	}
	return answer;
}


// Invoke by next_button_onClick js function
// Checks to see if end time is greater than start time
function compare_time() {
var start_time = document.hoursForm.p_normal_time_start.value;
var end_time = document.hoursForm.p_normal_time_finish.value;
var convert_start_time = "December 1, 1990 " + start_time + ":00";
var convert_end_time = "December 1, 1990 " + end_time + ":00";
m_start_time = new Date(convert_start_time);
m_end_time = new Date(convert_end_time);

var timeDiff = m_end_time - m_start_time;

	if (timeDiff <= 0) {
		return false;
	}
	else {
	 	return true;
	}
}


// Invoke by the ddf context field
// This redraws the page with the new flexfields
function set_redraw_flag() {
	document.hoursForm.p_redraw.value="Y";
	next_button_onClick();
}

// Check pay basis and hours
function chk_pay_basis() {
  var lv_pay_basis = document.hoursForm.p_pay_basis.value;
  
  if (lv_pay_basis == 'HOURLY') {
      alert(message_array[7]);
  }
}

// This function checks whether there is a future
// assignment record or a hiring date in reference 
// to the effective date that the user has inputed.
function chk_effective_date(error_code) { 
   
  if (error_code == 'terminated') {
    // The date you have entered is invalid 
	// because the person has been terminated.
    alert(message_array[11]);
    document.hoursForm.p_result_code.value = 'CANCEL';
	document.hoursForm.target= '_top';
    document.hoursForm.submit();
  }
  
  if (error_code == 'hire_date') {
    // The date you have chosen is before the hire date,
    // please choose a later date.
    alert(message_array[10]);
    document.hoursForm.p_result_code.value = 'CANCEL';
	document.hoursForm.target= '_top';
    document.hoursForm.submit();
	var error_flag = 1;
   }

   if (error_code == 'future_rec') {
    // A future change exists for this assignment,  
	// please choose a later date.
    alert(message_array[8]);
    document.hoursForm.p_result_code.value = 'CANCEL';
	document.hoursForm.target= '_top';
    document.hoursForm.submit();
	var error_flag = 1;
   }


   if (error_code == 'correction') {
	    // You are entering data in correction mode.
	   if (confirm(message_array[12])) {
	     void(0);
	   }
	   else {
	    document.hoursForm.p_result_code.value = 'CANCEL';
		document.hoursForm.target= '_top';
	    document.hoursForm.submit();
	      }
	  }
}

function lov_changed(msg) {
   set_form_changed();
}

// Invoke when a value in the P_WORK_HOURS is changed
function work_hours_changed () {
        alert(message_array[13]);
}

