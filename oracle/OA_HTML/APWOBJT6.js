//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT6.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |              				     |     
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJT2.js is split into 2 files |
//  |              APWOBJT2.js, APWOBJT6.js ,        |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT6.js 115.0 2000/11/09 13:53:06 pkm ship        $ */

/*===========================================================================*
 *  Receipt Class                                                            *
 *===========================================================================*/

function receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
if (arguments.length > 0) {
	this.date = date;
        if (fIsNum(occurs+"",false)) this.occurs = occurs; else this.occurs = 1;
        if (fIsNum(expensedAmount+"",false)) this.setExpensedAmount(expensedAmount); else this.setExpensedAmount(0);
        if (fIsNum(receiptAmount+"",false)) this.setReceiptAmount(receiptAmount); else this.setReceiptAmount(0);
	this.isMissing = ((isMissing) ? 'Y' : 'N');
        this.setReceiptCurr(receiptCurr);
        if (fIsNum(exchRate+"",false)) this.setExchRate(exchRate); else this.setExchRate(1);
	this.flexFields = new Array(); // of FlexFieldValues Object
        this.objExpenseType = objExpenseType; 
	this.justification = justification;
        this.cCardTrxnId = cCardTrxnId; 
        this.merchant = merchant;
	this.itemizeId = itemizeId; 
	this.expenseGroup = expenseGroup; 
        this.category = category;
        this.projectNumber = projectNumber;
        this.taskNumber = taskNumber;	
	this.taxInfo = new Tax();
 	this.reimbursAmount = this.calcReimbursAmount();
} else {
	this.date = null;
        this.occurs = null;
        this.setExpensedAmount(null); 
        this.setReceiptAmount(null);
	this.reimbursAmount = null;
	this.isMissing = 'N';
        this.setReceiptCurr(top.objExpenseReport.header.reimbursCurr);
        this.setExchRate(null);
	this.flexFields = new Array(); // of FlexFieldValues Object
        this.objExpenseType = null;
        this.taxInfo = new Tax();
	this.justification = "";
        this.cCardTrxnId = null; 
        this.merchant = null;
	this.itemizeId = null;
	this.expenseGroup = null; 
        this.category = "";
        this.projectNumber = "";
        this.taskNumber = "";
}

}



receiptObject.prototype.copy = R_copy;
receiptObject.prototype.getcCardTrxnId = R_getcCardTrxnId;
receiptObject.prototype.setJustification = R_setJustification;
receiptObject.prototype.getJustification = R_getJustification;
receiptObject.prototype.setIsMissing = R_setIsMissing;
receiptObject.prototype.getIsMissing = R_getIsMissing;
receiptObject.prototype.getGroup = R_getGroup;
receiptObject.prototype.setGroup = R_setGroup;
receiptObject.prototype.getCategory = R_getCategory;
receiptObject.prototype.getObjExpenseType = R_getObjExpenseType;
receiptObject.prototype.addFlexFields = R_addFlexFields;
receiptObject.prototype.setExpenseType = R_setExpenseType;
receiptObject.prototype.getCurrency = R_getCurrency;
receiptObject.prototype.setCurrency = R_setCurrency;
receiptObject.prototype.getOccurs = R_getOccurs;
receiptObject.prototype.setOccurs = R_setOccurs;
receiptObject.prototype.getDate = R_getDate;
receiptObject.prototype.setDate = R_setDate;
receiptObject.prototype.getReceiptCurr = R_getReceiptCurr;
receiptObject.prototype.setReceiptCurr = R_setReceiptCurr;
receiptObject.prototype.getReimbursAmount = R_getReimbursAmount;
receiptObject.prototype.setReimbursAmount = R_setReimbursAmount;
receiptObject.prototype.getExchRate = R_getExchRate;
receiptObject.prototype.setExchRate = R_setExchRate;
receiptObject.prototype.getDailyRate = R_getDailyRate;
receiptObject.prototype.getReceiptAmount = R_getReceiptAmount;
receiptObject.prototype.getNATaskNumber = R_getNATaskNumber;
receiptObject.prototype.getNAProjectNumber = R_getNAProjectNumber;
receiptObject.prototype.getTaskNumber = R_getTaskNumber;
receiptObject.prototype.getProjectNumber = R_getProjectNumber;
receiptObject.prototype.setTaskNumber = R_setTaskNumber;
receiptObject.prototype.setProjectNumber = R_setProjectNumber;
receiptObject.prototype.setReceiptAmount = R_setReceiptAmount;
receiptObject.prototype.getExpensedAmount = R_getExpensedAmount;
receiptObject.prototype.setExpensedAmount = R_setExpensedAmount;
receiptObject.prototype.setFlexValues = R_setFlexValues;
receiptObject.prototype.getAmountIncludesTax = R_getAmountIncludesTax;
receiptObject.prototype.setAmountIncludesTax = R_setAmountIncludesTax;
receiptObject.prototype.getOverrideFlag = R_getOverrideFlag;
receiptObject.prototype.setOverrideFlag = R_setOverrideFlag;
receiptObject.prototype.getTaxID = R_getTaxID;
receiptObject.prototype.setTaxID = R_setTaxID;
receiptObject.prototype.getTaxCode = R_getTaxCode;
receiptObject.prototype.getTaxCodeID = R_getTaxCodeID;
receiptObject.prototype.setTaxCode = R_setTaxCode;
receiptObject.prototype.getSupCountry = R_getSupCountry;
receiptObject.prototype.setSupCountry = R_setSupCountry;
receiptObject.prototype.getTaxPayerID = R_getTaxPayerID;
receiptObject.prototype.setTaxPayerID = R_setTaxPayerID;
receiptObject.prototype.getTaxRegNum = R_getTaxRegNum;
receiptObject.prototype.setTaxRegNum = R_setTaxRegNum;
receiptObject.prototype.getMerchRef = R_getMerchRef;
receiptObject.prototype.setMerchRef = R_setMerchRef;
receiptObject.prototype.getMerchant = R_getMerchant;
receiptObject.prototype.setMerchant = R_setMerchant;
receiptObject.prototype.getDocNum = R_getDocNum;
receiptObject.prototype.setDocNum = R_setDocNum;
receiptObject.prototype.addTax = R_addTax;
receiptObject.prototype.getObjFlexField = R_getObjFlexField
receiptObject.prototype.getNumOfFlexField = R_getNumOfFlexField;
receiptObject.prototype.calcReimbursAmount = R_calcReimbursAmount;
receiptObject.prototype.getExpTypeName = R_getExpTypeName;
receiptObject.prototype.getItemizeId = R_getItemizeId;
receiptObject.prototype.setItemizeId = R_setItemizeId;
receiptObject.prototype.isProjectEnabled = R_isProjectEnabled;

//
// copy all attributes of a receipt object
//
function R_copy(receipt) {
	var tempFFObj;
	this.date = receipt.date;
	this.occurs = receipt.occurs;
	this.setExpensedAmount(receipt.getExpensedAmount());
	this.setReceiptAmount(receipt.getReceiptAmount());
	this.reimbursAmount = receipt.getReimbursAmount();
	this.isMissing = receipt.isMissing;
	this.setReceiptCurr(receipt.getReceiptCurr());
	this.setExchRate(receipt.getExchRate());
	this.objExpenseType = receipt.getObjExpenseType();
	this.justification = receipt.justification;
	this.cCardTrxnId = receipt.cCardTrxnId;
	this.merchant = receipt.merchant;
	this.itemizeId = receipt.itemizeId;
	this.expenseGroup = receipt.expenseGroup;
	this.category = receipt.category;
	this.projectNumber = receipt.projectNumber;
	this.taskNumber = receipt.taskNumber;
	// this.flexFields has been created while setting expense type
	if (receipt.flexFields){
	  for (var i=0; i<receipt.flexFields.length; i++) {
			if (receipt.flexFields[i] != null) {
				tempFFObj = new flexFieldValues(receipt.flexFields[i].mGetName(),receipt.flexFields[i].mGetValue());
				this.addFlexFields(tempFFObj);
			}
	  }

	}
	else this.flexFields = null;
	if (this.taxInfo == null) this.taxInfo = new Tax();
		this.taxInfo.taxCodeID = receipt.taxInfo.taxCodeID;
		this.taxInfo.amountIncludesTax = receipt.taxInfo.amountIncludesTax;
		this.taxInfo.docNum = receipt.taxInfo.docNum;
		this.taxInfo.merchRef = receipt.taxInfo.merchRef;
		this.taxInfo.taxRegNum = receipt.taxInfo.taxRegNum;
		this.taxInfo.taxPayerID = receipt.taxInfo.taxPayerID;
		this.taxInfo.supCountry = receipt.taxInfo.supCountry;
		this.taxInfo.taxCode = receipt.taxInfo.taxCode;
		this.taxInfo.taxID = receipt.taxInfo.taxID;
		this.taxInfo.overrideFlag = receipt.taxInfo.overrideFlag;
}


function R_setExpensedAmount(expensedAmount){
        this.expensedAmount = expensedAmount;
	this.reimbursAmount = this.calcReimbursAmount();
}

function R_getExpensedAmount(){        
	return this.expensedAmount;
}

function R_setReceiptAmount(receiptAmount){        
	this.receiptAmount = receiptAmount;
}

function R_getExpTypeName(){        
	if (this.getObjExpenseType()) return this.getObjExpenseType().mGetstrName();
	else return "";
}

function R_setProjectNumber(projectNumber){        
	this.projectNumber = projectNumber;
}

function R_setTaskNumber(taskNumber){        
	this.taskNumber = taskNumber;
}

function R_getNAProjectNumber(){    
	if (this.projectNumber){ return this.projectNumber}
	else if (this.getObjExpenseType()){
	      if (this.getObjExpenseType().strExpenditureType == ''){
		  return top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');
	      }
	      else return '';
	     } 
	     else return '';
}

function R_getProjectNumber(){        
	return this.projectNumber;
}

function R_getNATaskNumber(){       
	if (this.taskNumber){ return this.taskNumber}
	else if (this.getObjExpenseType()){
	      if (this.getObjExpenseType().strExpenditureType == ''){
		  return top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');
	      }
	      else return '';
	     }
	     else return ''; 
}

function R_getTaskNumber(){        
	return this.taskNumber;
}

function R_isProjectEnabled(){
	if (this.getObjExpenseType() != null){
	  if (this.getObjExpenseType().strExpenditureType != "") return true;
	  else return false;
	}
	else return true;
}

function R_getReceiptAmount(){        
	return this.receiptAmount;
}

function R_getDailyRate(){
	if (this.occurs >0){
	return(this.getExpensedAmount()/this.occurs);
	}
	else return 0;
}

function R_setExchRate(exchRate){        
	this.exchRate = exchRate;
	this.reimbursAmount = this.calcReimbursAmount();
}

function R_getExchRate(){        
	return this.exchRate;
}

function R_getReimbursAmount(){      
   	return(this.reimbursAmount);
}

function R_setReimbursAmount(value){        
	this.reimbursAmount = value;
}

function R_setReceiptCurr(receiptCurr){        
	this.receiptCurr = receiptCurr;
}

function R_getReceiptCurr(){        
	return(this.receiptCurr);
}

function R_setDate(date){       
	this.date = date;
}

function R_getDate(){       
	return this.date;
}

function R_setOccurs(occurs){        
	this.occurs = occurs;
}

function R_getOccurs(){        
	return this.occurs;
}

function R_setCurrency(receiptCurr){    
	this.receiptCurr = receiptCurr;
}

function R_getCurrency(){    
	return this.receiptCurr;
}

function R_setExpenseType(objE){
        this.objExpenseType = objE;
}

function R_addFlexFields(objFlexField){
   this.flexFields[this.flexFields.length] = objFlexField;
}

function R_setFlexArrValue(i,val){
	if (this.flexFields[i])
        this.flexFields[i].value = val;
}
receiptObject.prototype.setFlexArrValue = R_setFlexArrValue;

function R_getFlexArrValue(i){
	if (this.flexFields[i])
          return this.flexFields[i].value;
	else
	  return "";
}
receiptObject.prototype.getFlexArrValue = R_getFlexArrValue;


function R_setFlexArrName(i,val){
        this.flexFields[i].name = val;
}
receiptObject.prototype.setFlexArrName = R_setFlexArrName;

function R_getFlexArrName(i){
        return this.flexFields[i].name;
}
receiptObject.prototype.getFlexArrName = R_getFlexArrName;


function R_setFlexArrElem(i,obj){
        this.flexFields[i] = obj;
}
receiptObject.prototype.setFlexArrElem = R_setFlexArrElem;

function R_getFlexArrElem(i){
        return this.flexFields[i];
}
receiptObject.prototype.getFlexArrElem = R_getFlexArrElem;

function R_getObjExpenseType()
{ 
       return this.ObjExpenseType;
}

function R_getObjExpenseType(){        
	return this.objExpenseType;
}

function R_getCategory(){        
	return this.category;
}

function R_setGroup(group){        
	this.expenseGroup = group;
}

function R_getGroup(){        
	return this.expenseGroup;
}

function R_getIsMissing(){        
	return ((this.isMissing == 'Y') ? true : false);
}

function R_setIsMissing(isMissing){        
	this.isMissing = ((isMissing) ? 'Y' : 'N');
}

function R_getJustification(){
	return this.justification;
}

function R_setJustification(strJustif){
	this.justification = strJustif;
}

function R_getcCardTrxnId(){	
	return this.cCardTrxnId;
}

function R_addTax(docNum,merchRef, regNum,taxPayerID,supCountry,taxCode,taxID,overrideFlag,amountIncludesTax){
	this.taxInfo = new Tax(docNum, merchRef, regNum,taxPayerID,supCountry,taxCode,taxID,overrideFlag,amountIncludesTax);
}


function R_setDocNum(docNum){	
	this.taxInfo.docNum = docNum;
}

function R_getDocNum(){	
	return this.taxInfo.docNum;
}

function R_setMerchant(strMerchant){	
	this.merchant = strMerchant;
}

function R_getMerchant(){	
	return this.merchant;
}

function R_setMerchRef(merchRef){	
	this.taxInfo.merchRef = merchRef;
}

function R_getMerchRef(){	
	return this.taxInfo.merchRef;
}

function R_setTaxRegNum(taxRegNum){	
	this.taxInfo.taxRegNum = taxRegNum;
}

function R_getTaxRegNum(){	
	return this.taxInfo.taxRegNum;
}

function R_setTaxPayerID(taxPayerID){	
	this.taxInfo.taxPayerID = taxPayerID;
}

function R_getTaxPayerID(){	
	return this.taxInfo.taxPayerID;
}

function R_setSupCountry(supCountry){	
	this.taxInfo.supCountry = supCountry;
}

function R_getSupCountry(){	
	return this.taxInfo.supCountry;
}

function R_setTaxCode(taxCode){	
	this.taxInfo.taxCode = taxCode;
}

function R_getTaxCodeID(){
	return this.taxInfo.taxID;
}

function R_getTaxCode(){	
	return this.taxInfo.taxCode;
}

function R_setTaxID(taxID){	
	this.taxInfo.taxID = taxID;
}
function R_getTaxID(){		
	return this.taxInfo.taxID;
}

function R_setOverrideFlag(overrideFlag){	
	this.taxInfo.overrideFlag = overrideFlag;
}

function R_getOverrideFlag(){
	if ((!this.taxInfo.overrideFlag) || (this.taxInfo.overrideFlag == null)) this.taxInfo.overrideFlag = 'N';
	else this.taxInfo.overrideFlag = 'Y';
	return this.taxInfo.overrideFlag;
}

function R_setAmountIncludesTax(amountIncludesTax){
	this.taxInfo.amountIncludesTax = (amountIncludesTax ? 'Y':'N');
}

function R_getAmountIncludesTax(){
  	return ((this.taxInfo.amountIncludesTax == 'Y') ? true:false);
}

function R_setItemizeId(ItemizeId){
	this.itemizeId = ItemizeId;
}

function R_getItemizeId(){
  	return (this.itemizeId);
}

function R_setFlexValues(strName,strValue){
	var objFlexField = this.getObjFlexField(strName);
	if (objFlexField)
	objFlexField.mSetValue(strValue);
}

function R_getObjFlexField(strName){
	for (var i=0;i<this.getNumOfFlexField();i++){
	  if (this.flexFields[i].mGetName() == strName)
	  return this.flexFields[i];
	}
	return null;
}

function R_getNumOfFlexField(){
	if (this.flexFields)
	return this.flexFields.length;
	else return 0;
}


function R_calcReimbursAmount() {
   var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
   var l_reimbcurr_precision =0;
   if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;
   if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y')
     return fRound( this.expensedAmount / this.exchRate, l_reimbcurr_precision);
   else
     return fRound( fMultiply(this.expensedAmount, this.exchRate), l_reimbcurr_precision);
}
