<?xml version="1.0"?> 
<!--
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  FILENAME
 |      xmltojs.xsl
 |  DESCRIPTION
 |
 |  NOTES
 |    All runtime ids should be prefixed by z.
 |
 |  DEPENDENCIES
 |
 |  HISTORY
 |        6-AUG-99 D Gosselin    Created
 +===========================================================================*/
$Header: czxml2js.xsl 115.17 2001/01/08 15:14:10 pkm ship      $
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:preserve-space elements="text"/>

<!-- ********************************************************************** -->
<xsl:template match="/">
   <xsl:apply-templates select="config-ui|terminate"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-ui">
  <xsl:choose>
    <xsl:when test="config-layout/application">
      <xsl:apply-templates select="config-layout"/>
    </xsl:when>
    <xsl:when test="config-summary">
      <xsl:element name="script">
        <xsl:attribute name="language">JavaScript</xsl:attribute>
          var nav_cmd = "<xsl:value-of select="nav-cmd"/>"
          var seq_num = <xsl:value-of select="seq-num"/>
          var um = null;
          if (parent.frames.czSrc.isLoaded) {
            um = parent.frames.czSrc.getUiBroker();
          }
          function doModelUpdate() {
          }
          function doLayout() {
            if (um != null) {
              var ret = um.registerPage (seq_num, nav_cmd);
              if (ret) {
                var startTime = new Date();
                var sum = '' +
                          <xsl:apply-templates select="config-summary"/>
                          '';
                um.publish('czContent', null, null, sum);
                var endTime = new Date();
                um.log("Summary Screen Render", startTime, endTime);
              }
              um.unLockEventManagers();
            }
          }
      </xsl:element>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="script">
        <xsl:attribute name="language">JavaScript</xsl:attribute>
          var nav_cmd = "<xsl:value-of select="nav-cmd"/>"
          var seq_num = <xsl:value-of select="seq-num"/>
          var um = null;
          if (parent.frames.czSrc.isLoaded) {
            um = parent.frames.czSrc.getUiBroker();
          }
          function doModelUpdate() {
            if (um != null) {
              var startTime = new Date();
              <xsl:apply-templates select="config-state"/>
              var endTime = new Date();
              um.log("Model Update", startTime, endTime);
            }
          }
          function doLayout() {
            if (um != null) {
              var ret = um.registerPage (seq_num, nav_cmd);
              if (ret) {
                var startTime = new Date();
                <xsl:apply-templates select="config-layout|config-messages"/>
                var endTime = new Date();
                um.log("Layout Rendering", startTime, endTime);
                if (um.allFramesLoaded()) {
                  doModelUpdate();
                }
              }
              um.unLockEventManagers();
            }
          }
      </xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="terminate">
      <xsl:element name="script">
        <xsl:attribute name="language">JavaScript</xsl:attribute>
          var um = parent.frames.czSrc.getUiBroker();
          function doModelUpdate() {
          }
          function doLayout() {
            if (parent.frames.czSrc.isLoaded) {
              if (um != null) {
                um.log ("Sending message to server", null, new Date());
                var startDate = new Date();
                var term = '<BR></BR>' +
          <xsl:if test="config_header_id">
                           '<p><b>Configuration Header ID:</b>  <xsl:value-of select="config_header_id"/></p>' +
          </xsl:if>
          <xsl:if test="config_rev_nbr">
                           '<p><b>Configuration Revision:</b>  <xsl:value-of select="config_rev_nbr"/></p>' +
          </xsl:if>
          <xsl:if test="valid_configuration">
                           '<p><b>Configuration Valid:</b>  <xsl:value-of select="valid_configuration"/></p>' +
          </xsl:if>
          <xsl:if test="complete_configuration">
                           '<p><b>Configuration Complete:</b>  <xsl:value-of select="complete_configuration"/></p>' +
          </xsl:if>
          <xsl:if test="exit">
                           '<p><b>Exit Status:</b>  <xsl:value-of select="exit"/></p>' +
          </xsl:if>
                '';
                um.showExitMessage(term);
                um.unLockEventManagers();
                um.log ("Display Terminate", startDate, new Date());
              }
            }
          }
      </xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-layout">
   <xsl:apply-templates select="application|cmd-navigate|cmd-remove|cmd-clear|cmd-add|cmd-replace"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-state">
   <xsl:apply-templates select="object-update|row-update|model-update|cmd-navigate"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-messages">
   <xsl:apply-templates select="contradiction-message|validation-message|warning-message|prompt-confirm-message|information-message|error-message"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="application">
  <xsl:apply-templates select="frameset"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="frameset">
   <xsl:element name="frameset">
      <xsl:attribute name="id">z<xsl:value-of select="@rtid"/></xsl:attribute>
      <xsl:attribute name="name">z<xsl:value-of select="@rtid"/></xsl:attribute>
      <xsl:choose>
         <xsl:when test="@rows-or-cols='rows'">
            <xsl:attribute name="rows"><xsl:value-of select="@allocations"/></xsl:attribute>
         </xsl:when>
         <xsl:otherwise>
            <xsl:attribute name="cols"><xsl:value-of select="@allocations"/></xsl:attribute>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="frameborder">0</xsl:attribute>
      <xsl:attribute name="framespacing">0</xsl:attribute>
      <xsl:attribute name="border">0</xsl:attribute>
      <xsl:apply-templates select="frameset|frame"/>
   </xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="frame">
  <xsl:element name="frame">
    <xsl:choose>
      <xsl:when test="@src">
        <xsl:attribute name="src"><xsl:value-of select="@src"/></xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="src">../blank.html</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:attribute name="id">z<xsl:value-of select="@rtid"/></xsl:attribute>
    <xsl:attribute name="name">z<xsl:value-of select="@rtid"/></xsl:attribute>
    <xsl:choose>
      <xsl:when test="@borders='true'">
        <xsl:attribute name="frameborder">1</xsl:attribute>
        <xsl:attribute name="framespacing">0</xsl:attribute>
        <xsl:attribute name="border">1</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="frameborder">0</xsl:attribute>
        <xsl:attribute name="framespacing">0</xsl:attribute>
        <xsl:attribute name="border">0</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="@margin-width">
      <xsl:attribute name="marginwidth"><xsl:value-of select="@margin-width"/></xsl:attribute>
    </xsl:if>
    <xsl:if test="@margin-height">
      <xsl:attribute name="marginheight"><xsl:value-of select="@margin-height"/></xsl:attribute>
    </xsl:if>
    <xsl:if test="@scrolling">
      <xsl:attribute name="scrolling"><xsl:value-of select="@scrolling"/></xsl:attribute>
    </xsl:if>
  </xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="panel">
<!-- For the moment panels are meaningless since we can only have one. -->
  <xsl:apply-templates select="panel|treeview|grid|option-group|bom-item-selector|checkbox|radiobutton|list-control|text-control|number-control|value-display|component-set-mgr|button|image-object|text-object|find-control"/>

  <xsl:if test="panel/areastyle/@bgcolor">
          bdyTags = 'bgcolor=<xsl:value-of select="panel/areastyle/@bgcolor"/>';
  </xsl:if>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="treeview">
              {
              var icons = new Array ();
    <xsl:if test="./model-tree-icons">
              icons[0] = {key:'open', src:'<xsl:value-of select="model-tree-icons/@open"/>'};
              icons[1] = {key:'closed', src:'<xsl:value-of select="model-tree-icons/@closed"/>'};
              icons[2] = {key:'open-unsatisfied', src:'<xsl:value-of select="model-tree-icons/@open-unsatisfied"/>'};
              icons[3] = {key:'closed-unsatisfied', src:'<xsl:value-of select="model-tree-icons/@closed-unsatisfied"/>'};
    </xsl:if>
    <xsl:choose>
      <xsl:when test="./position">
              var ctl = um.createTree(
                                    target,
                                    'z<xsl:value-of select="@rtid"/>',
                                    icons,
                                    null,
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
      </xsl:when>
      <xsl:otherwise>
              var ctl = um.createTree(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>',
                                     icons);
      </xsl:otherwise>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="@icons='true'">
         <xsl:choose>
           <xsl:when test="@treelines='true'">
              ctl.setTreeStyle(3);
           </xsl:when>
           <xsl:otherwise>
              ctl.setTreeStyle(2);
           </xsl:otherwise>
         </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
         <xsl:choose>
           <xsl:when test="@treelines='true'">
              ctl.setTreeStyle(1);
           </xsl:when>
           <xsl:otherwise>
              ctl.setTreeStyle(0);
           </xsl:otherwise>
         </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
              <xsl:apply-templates select="areastyle"/>
              <xsl:apply-templates select="treenode"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="treenode">
  <xsl:choose>
    <xsl:when test="../../treeview"> <!-- Our parent is treview we are a root -->
              {
      <xsl:choose>
        <xsl:when test="./text">
              var n = ctl.addNode (null, 0, 'z<xsl:value-of select="@rtid"/>', "<xsl:value-of select="./text"/>");
        </xsl:when>
        <xsl:otherwise>
              var n = ctl.addNode (null, 0, 'z<xsl:value-of select="@rtid"/>', "");
        </xsl:otherwise>       
      </xsl:choose>
              n.setUnSatisfied(true);
              }      
    </xsl:when>
    <xsl:otherwise>  <!-- We are a child of something -->
              {
      <xsl:choose>
        <xsl:when test="./text">
              var n = ctl.addNode ('z<xsl:value-of select="@parent-rtid"/>', 4, 'z<xsl:value-of select="@rtid"/>', "<xsl:value-of select="./text"/>");
        </xsl:when>
        <xsl:otherwise>
              var n = ctl.addNode ('z<xsl:value-of select="@parent-rtid"/>', 4, 'z<xsl:value-of select="@rtid"/>', "");
        </xsl:otherwise>       
      </xsl:choose>
              n.setUnSatisfied(true);
              }
    </xsl:otherwise>
 </xsl:choose>
 <xsl:apply-templates select="treenode"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="grid">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="grid-column">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="option-group|list-control">
<!-- This should ONLY use position and not alignment!!! -->
              {
      <xsl:choose>
         <xsl:when test="@control-type='list-single'">
              var ctl = um.createLogicList(
                                    target,
                                    'z<xsl:value-of select="@rtid"/>', 
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
              ctl.setMultiSelect(false);
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
         </xsl:when>
         <xsl:when test="@control-type='list-multi'">
              var ctl = um.createLogicList(
                                    target,
                                    'z<xsl:value-of select="@rtid"/>', 
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
              ctl.setMultiSelect(true);
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
         </xsl:when>
         <xsl:when test="@control-type='checkboxes'">
              var ctl = um.createLogicList(
                                    target,
                                    'z<xsl:value-of select="@rtid"/>', 
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
              ctl.setMultiSelect(true);
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
         </xsl:when>
         <xsl:when test="@control-type='radiobuttons'">
              var ctl = um.createLogicList(
                                    target,
                                    'z<xsl:value-of select="@rtid"/>', 
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
              ctl.setMultiSelect(false);
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
         </xsl:when>
         <xsl:otherwise> <!-- dropdown -->
              var ctl = um.createLogicComboBox (
                                    target,
                                    'z<xsl:value-of select="@rtid"/>', 
                                    <xsl:value-of select="position/@left"/>,
                                    <xsl:value-of select="position/@top"/>, 
                                    <xsl:value-of select="position/@width"/>,
                                    <xsl:value-of select="position/@height"/>);
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="option|list-item"/>
      <xsl:if test="logic-state-display/logic-icons">
              ctl.setImages('<xsl:value-of select="logic-state-display/logic-icons/@utrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ufalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ltrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@lfalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@unknown"/>',
                            16, 16); 
      </xsl:if>
      <xsl:if test="logic-state-display/logic-colors">
              ctl.setSelectedBackColor('<xsl:value-of select="logic-state-display/logic-colors/@utrue"/>');
      </xsl:if>
      <xsl:if test="option/caption/font">
        <xsl:if test="@color">
              ctl.setColor('<xsl:value-of select="option/caption/font/@color"/>');
        </xsl:if>
              var fnt = um.createFontObject('<xsl:value-of select="option/caption/font/@name"/>',
                                            '<xsl:value-of select="option/caption/font/@size"/>',
        <xsl:choose>
          <xsl:when test="option/caption/font/@italic='true'">
                                            'italic',
          </xsl:when>
          <xsl:otherwise>
                                            'normal',
          </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="option/caption/font/@bold='true'">
                                            'bold');
          </xsl:when>
          <xsl:otherwise>
                                            'normal');
          </xsl:otherwise>
        </xsl:choose>
              ctl.setFont(fnt);
      </xsl:if>
      <xsl:apply-templates select="areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="option">
      <xsl:choose>
         <xsl:when test="@countable='true'">
           <xsl:choose>
             <xsl:when test="caption/text">
              ctl.addCountedOptionItem("<xsl:value-of select="caption/text"/>",
             </xsl:when>
             <xsl:otherwise>
              ctl.addCountedOptionItem("",
             </xsl:otherwise>
           </xsl:choose>
                                       'unknown', 0,
                                       'z<xsl:value-of select="@rtid"/>');
         </xsl:when>
         <xsl:otherwise>
           <xsl:choose>
             <xsl:when test="caption/text">
              ctl.addOptionItem ("<xsl:value-of select="caption/text"/>", 
             </xsl:when>
             <xsl:otherwise>
              ctl.addOptionItem ("", 
             </xsl:otherwise>
           </xsl:choose>
                                 'unknown',
                                 'z<xsl:value-of select="@rtid"/>');
         </xsl:otherwise>
      </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="list-item">
  <xsl:choose>
    <xsl:when test="caption/text">
              ctl.addItem ("<xsl:value-of select="caption/text"/>");
    </xsl:when>
    <xsl:otherwise>
              ctl.addItem ("");
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="bom-item-selector">
              {
              var ctl = um.createBomItem(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
  <xsl:choose>
    <xsl:when test="caption/text">
                                     "<pre><xsl:value-of select="caption/text"/></pre>",
    </xsl:when>
    <xsl:otherwise>
                                     "",
    </xsl:otherwise>
  </xsl:choose>
                                     'false'); 
              ctl.setPriceVisible ('true');
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
      <xsl:if test="logic-state-display/logic-icons">
              ctl.setImages('<xsl:value-of select="logic-state-display/logic-icons/@utrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ufalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ltrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@lfalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@unknown"/>',
                            16, 16); 
      </xsl:if>
              <xsl:apply-templates select="caption|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="checkbox">
              {
              var ctl = um.createLogicCheckBox(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
  <xsl:choose>
    <xsl:when test="caption/text">
                                     "<xsl:value-of select="caption/text"/>",
    </xsl:when>
    <xsl:otherwise>
                                     "",
    </xsl:otherwise>
  </xsl:choose>
                                     'unknown'); 
              ctl.setImages('czUTrue.gif',
                            'czUFalse.gif',
                            'czLTrue.gif',
                            'czLFalse.gif',
                            'czUnknow.gif',
                            16, 16); 
      <xsl:if test="logic-state-display/logic-icons">
              ctl.setImages('<xsl:value-of select="logic-state-display/logic-icons/@utrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ufalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@ltrue"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@lfalse"/>',
                            '<xsl:value-of select="logic-state-display/logic-icons/@unknown"/>',
                            16, 16); 
      </xsl:if>
              <xsl:apply-templates select="caption|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="radiobutton">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="text-control">
              {
              var ctl = um.createTextInput(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     ''); 
              <xsl:apply-templates select="font|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="number-control">
              {
              var ctl = um.createNumericInput(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     ''); 
              <xsl:apply-templates select="font|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="value-display">
              {
              var ctl = um.createLabel(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     ''); 
              <xsl:apply-templates select="font|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="button">
              {
              var ctl = um.createButton(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     '<xsl:value-of select="image/@src"/>',
  <xsl:choose>
    <xsl:when test="caption/text">
                                     "<xsl:value-of select="caption/text"/>");
    </xsl:when>
    <xsl:otherwise>
                                     "");
    </xsl:otherwise>
  </xsl:choose>
              <xsl:apply-templates select="caption|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="text-object">
<!-- This should ONLY use position and not alignment!!! -->
              {
  <xsl:choose>
    <xsl:when test="@active='true'"> <!-- Because it's active it is button. -->
              var ctl = um.createButton(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     '<xsl:value-of select="image/@src"/>',
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
      <xsl:choose>
        <xsl:when test="./text">
                                     "<xsl:value-of select="./text"/>");
        </xsl:when>
        <xsl:otherwise>
                                     ""); 
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise> <!-- Not a button. -->
              var ctl = um.createLabel(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
      <xsl:choose>
        <xsl:when test="./text">
                                     "<xsl:value-of select="./text"/>"); 
        </xsl:when>
        <xsl:otherwise>
                                     ""); 
        </xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
              <xsl:apply-templates select="font|areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="image-object">
              {
  <xsl:choose>
    <xsl:when test="@active='true'"> <!-- Because it's active it is button. -->
              var ctl = um.createButton(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     '<xsl:value-of select="image/@src"/>',
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     ''); 

    </xsl:when>
    <xsl:otherwise> <!-- Not a button. -->
              var ctl = um.createImage(
                                     target,
                                     'z<xsl:value-of select="@rtid"/>', 
                                     <xsl:value-of select="position/@left"/>,
                                     <xsl:value-of select="position/@top"/>, 
                                     <xsl:value-of select="position/@width"/>,
                                     <xsl:value-of select="position/@height"/>,
                                     '<xsl:value-of select="image/@src"/>'); 
    </xsl:otherwise>
  </xsl:choose>
              <xsl:apply-templates select="areastyle"/>
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="find-control">
  <xsl:apply-templates select="text-control|button|text-object"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="component-set-mgr">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="component-node">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="model-update">
              {
              var ctl = um.getRuntimeObject('z<xsl:value-of select="@target"/>');
              if (ctl != null) {
  <xsl:apply-templates select="logic-state|quantity|unsatisfied|list-price|discount-pct|discount-amt|net-price|atp-date|ext-price|value"/>
              }
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="value">
  <xsl:choose>
    <xsl:when test=".">
              ctl.setValue("<xsl:value-of select="."/>");
    </xsl:when>
    <xsl:otherwise>
              ctl.setValue("");
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="logic-state">
              ctl.setState('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="quantity">
              ctl.setCount('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="branch-unsatisfied">
              ctl.setUnSatisfied(<xsl:value-of select="."/>);
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="list-price">
              ctl.setPrice('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="discount-pct">
<!--              ctl.setDiscountPct('<xsl:value-of select="."/>'); -->
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="discount-amt">
<!--              ctl.setDiscountAmt('<xsl:value-of select="."/>'); -->
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="net-price">
              ctl.setPrice('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="atp-date">
              ctl.setATP('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="ext-price">
              ctl.setPrice('<xsl:value-of select="."/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="row-update">
  <xsl:apply-templates select="col-data"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="object-update">
              {
              var ctl = um.getRuntimeObject('z<xsl:value-of select="@target"/>');
                if (ctl != null) {
  <xsl:choose>  
    <xsl:when test=".">
                  ctl.setValue("<xsl:value-of select="."/>");
    </xsl:when>
    <xsl:otherwise>
                  ctl.setValue("");
    </xsl:otherwise>
  </xsl:choose>
                }
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-add">
   <xsl:choose>
      <xsl:when test="./treenode">
              {
              var target = 'z<xsl:value-of select="@target"/>';
              var node = um.getRuntimeObject('z<xsl:value-of select="@target"/>');
              if (node != null) {
                var ctl = node.getParentObject();
                if (ctl != null) {
                  <xsl:apply-templates select="treenode"/>
                }
              }
              }
      </xsl:when>
      <xsl:otherwise>
              {
              var target = 'z<xsl:value-of select="@target"/>';
              um.refresh(target);
              var bdyTags = '';
   <xsl:apply-templates select="panel|treeview|grid|option-group|bom-item-selector|checkbox|radiobutton|list-control|number-control|value-display|component-set-mgr|button|image-object|text-object|find-control|treenode|model-runtime-data|row-data|text-control"/>
              um.publish(target, null, bdyTags, null);
              }
      </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-remove">
              um.deleteComponent('z<xsl:value-of select="@remove-id"/>');
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-replace">
              {
              var target = 'z<xsl:value-of select="@target"/>';
              um.refresh(target);
  <xsl:apply-templates select="panel|treeview|grid|option-group|bom-item-selector|checkbox|radiobutton|list-control|number-control|value-display|component-set-mgr|button|image-object|text-object|find-control|model-runtime-data|row-data"/>
              um.publish(target);
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-clear">
              {
              var target = 'z<xsl:value-of select="@target"/>';
              um.refresh(target);
              um.publish(target);
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-navigate">
             {
             um.navigateTo ('z<xsl:value-of select="@target"/>');
             }       
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="row-data">
  <xsl:apply-templates select="col-data"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="col-data">
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-summary">
  <xsl:choose>
    <xsl:when test="./model-runtime-data/ext-price">
        '<table border="0" width="100%" cellpadding="4">' +
        '  <caption><b>Summary Screen</b></caption>' +
        '  <tr bgcolor="lightblue">' +
        '    <th>Item</th>' +
        '    <th>Quantity</th>' +
        '    <th>List Price</th>' +
        '    <th>Price</th>' +
        '    <th>Total Price</th>' +
        '    <th>Available</th>' +
        '  </tr>' +
          <xsl:apply-templates select="model-runtime-data"/>
          <xsl:if test="./total-price">
        '    <tr>' +
        '      <td><br><xsl:text> </xsl:text></br></td>' +
        '      <td><br><xsl:text> </xsl:text></br></td>' +
        '      <td><br><xsl:text> </xsl:text></br></td>' +
        '      <td align="center"><b>Total</b></td>' +
        '      <td align="right"><xsl:value-of select="./total-price"/></td>' +
        '    </tr>' +
          </xsl:if>
        '</table>' +
    </xsl:when>
    <xsl:otherwise>
        '<table border="0" width="100%" cellpadding="4">' +
        '  <caption><b>Summary Screen</b></caption>' +
        '  <tr bgcolor="lightblue">' +
        '    <th>Item</th>' +
        '    <th>Quantity</th>' +
        '    <th>Price</th>' +
        '    <th>Total</th>' +
        '  </tr>' +
          <xsl:apply-templates select="model-runtime-data"/>
          <xsl:if test="./total-price">
        '    <tr>' +
        '      <td><br><xsl:text> </xsl:text></br></td>' +
        '      <td><br><xsl:text> </xsl:text></br></td>' +
        '      <td align="center"><b>Total</b></td>' +
        '      <td align="right"><xsl:value-of select="./total-price"/></td>' +
        '    </tr>' +
          </xsl:if>
        '</table>' +
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="model-runtime-data">
  <xsl:choose>
    <xsl:when test="./ext-price">
    
        <xsl:choose>
          <xsl:when test="./quantity">
        '  <tr bgcolor="lightgrey">' +
            <xsl:choose>
              <xsl:when test="./name">
        "    <td><pre><xsl:value-of select="./name"/></pre></td>" +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./quantity">
        '    <td align="center"><xsl:value-of select="./quantity"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./list-price">
        '    <td align="right"><xsl:value-of select="./list-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./net-price">
        '    <td align="right"><xsl:value-of select="./net-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
            <xsl:when test="./ext-price">
        '    <td align="right"><xsl:value-of select="./ext-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./atp-date">
        '    <td align="right" width="13%"><xsl:value-of select="./atp-date"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            
        '  </tr>' +
          </xsl:when>
          <xsl:otherwise>
         '  <tr>' +
            <xsl:choose>
              <xsl:when test="./name">
        "    <td><pre><xsl:value-of select="./name"/></pre></td>" +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./quantity">
        '    <td align="center"><xsl:value-of select="./quantity"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./list-price">
        '    <td align="right"><xsl:value-of select="./list-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./net-price">
        '    <td align="right"><xsl:value-of select="./net-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./ext-price">
        '    <td align="right"><xsl:value-of select="./ext-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./atp-date">
        '    <td align="right" width="13%"><xsl:value-of select="./atp-date"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            
        '  </tr>' +
          </xsl:otherwise>
        </xsl:choose>
            <xsl:apply-templates select="model-runtime-data"/>

    
    </xsl:when>
    <xsl:otherwise>
        <xsl:choose>
        <xsl:when test="./quantity">
        '  <tr bgcolor="lightgrey">' +
            <xsl:choose>
              <xsl:when test="./name">
        "    <td><pre><xsl:value-of select="./name"/></pre></td>" +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./quantity">
        '    <td align="center"><xsl:value-of select="./quantity"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./list-price">
        '    <td align="right"><xsl:value-of select="./list-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./line-total">
        '    <td align="right"><xsl:value-of select="./line-total"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
        '  </tr>' +
          </xsl:when>
          <xsl:otherwise>
        '  <tr>' +
            <xsl:choose>
              <xsl:when test="./name">
        "    <td><pre><xsl:value-of select="./name"/></pre></td>" +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./quantity">
        '    <td align="center"><xsl:value-of select="./quantity"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./list-price">
        '    <td align="right"><xsl:value-of select="./list-price"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="./line-total">
        '    <td align="right"><xsl:value-of select="./line-total"/></td>' +
              </xsl:when>
              <xsl:otherwise>
        '    <td><br><xsl:text> </xsl:text></br></td>' +
              </xsl:otherwise>
            </xsl:choose>
        '  </tr>' +
          </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="model-runtime-data"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="contradiction-message">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="message-text"/>
              '<ul>' +
  <xsl:apply-templates select="error-text"/>
              '</ul>' +
  <xsl:apply-templates select="prompt-text"/>
                        '</font></p>';
              um.showContradiction(msg, 
                                   'z<xsl:value-of select="@id-yes"/>',
                                   'z<xsl:value-of select="@id-no"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-confirm-message">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="prompt-text"/>
                        '</font></p>';
              um.showContradiction(msg, 
                                   'z<xsl:value-of select="@id-yes-ok"/>',
                                   'z<xsl:value-of select="@id-no-cancel"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="validation-message">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="error-text"/>
                        '</font></p>';
              um.showValidation(msg, 
                                'z<xsl:value-of select="@id-ok"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="warning-messsage">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="warning-text"/>
                        '</font></p>';
              um.showValidation(msg, 
                                'z<xsl:value-of select="@id-ok"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="information-message">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="message-text|information-text"/>
                        '</font></p>';
              um.showValidation(msg, 
                                'z<xsl:value-of select="@id-ok"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-confirm-text">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="prompt-text"/>
                        '</font></p>';
              um.showContradiction(msg, 
                                   'z<xsl:value-of select="@id-confirm"/>',
                                   'z<xsl:value-of select="@id-negate"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-input-message"> 
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="prompt-text"/>
                        '</font></p>';
              um.showContradiction(msg, 
                                   'z<xsl:value-of select="@id-confirm"/>',
                                   'z<xsl:value-of select="@id-cancel"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="error-message">
              {
              var msg = '<br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><br><xsl:text> </xsl:text></br><p><font style="font-family:arial;font-size:11pt;font-style:normal;font-weight:normal">' +
  <xsl:apply-templates select="error-text"/>
                        '</font></p>';
              um.showValidation(msg, 
                                'z<xsl:value-of select="@id-ok"/>');
              }
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="message-text|information-text">
  <xsl:choose>
    <xsl:when test=".">
              "<xsl:value-of select="."/>" +
    </xsl:when>
    <xsl:otherwise>
              "" +
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="error-text">
  <xsl:choose>
    <xsl:when test=".">
              "<li><xsl:value-of select="."/></li>" +
    </xsl:when>
    <xsl:otherwise>
              "" +
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="warning-text">
  <xsl:choose>
    <xsl:when test=".">
              "<xsl:value-of select="."/>" +
    </xsl:when>
    <xsl:otherwise>
              "" +
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-text">
  <xsl:choose>
    <xsl:when test=".">
              "<xsl:value-of select="."/>" +
    </xsl:when>
    <xsl:otherwise>
              "" +
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="areastyle">
  <xsl:if test="@bgstyle='opaque'">
    <xsl:if test="@bgcolor">
              ctl.setBackgroundColor('<xsl:value-of select="@bgcolor"/>');
    </xsl:if>
  </xsl:if>
  <xsl:if test="@borderwidth>0">
              ctl.createBorder('<xsl:value-of select="@borderwidth"/>',
                               '<xsl:value-of select="@bordercolor"/>');
  </xsl:if>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="font">
  <xsl:if test="@color">
              ctl.setColor('<xsl:value-of select="@color"/>');
  </xsl:if>
              var fnt = um.createFontObject('<xsl:value-of select="@name"/>',
                                            '<xsl:value-of select="@size"/>',
  <xsl:choose>
    <xsl:when test="@italic='true'">
                                            'italic',
    </xsl:when>
    <xsl:otherwise>
                                            'normal',
    </xsl:otherwise>
  </xsl:choose>
  <xsl:choose>
    <xsl:when test="@bold='true'">
                                            'bold');
    </xsl:when>
    <xsl:otherwise>
                                            'normal');
    </xsl:otherwise>
  </xsl:choose>
              ctl.setFont(fnt);
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="caption">
  <xsl:apply-templates select="font"/>
</xsl:template>

</xsl:stylesheet>

