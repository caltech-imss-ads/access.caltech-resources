/* $Header: czCursor.js 115.2 2000/04/17 13:30:21 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function Cursor()
{
	return null;
}

function Cursor_Create(src)
{

	if(src)
	{
		if(Cursor.htmlObj==null)
		{
			var lyr = HTMLHelper.createLayer('CursorObj',null,self, '<IMG ID=cursImg ></IMG>');
			
			Cursor.img = ns4?lyr.document.images[0]:lyr.all['cursImg'];
			Cursor.htmlObj = Cursor.block = lyr;
			if(ie4) 
				Cursor.htmlObj = lyr.style;
			HTMLHelper.setDimensions(Cursor.htmlObj,-100,-100);
			Cursor.htmlObj.zIndex = 10000;

		}
		Cursor.img.src = src;
	}

	var em = new EventManager();
	em.addListener('docMouseMove',Cursor_docMouseMoveCallback);
}

function Cursor_show(src,x,y)
{

	if(src == null)
	{
		Cursor.hide();
		return;
	}

	if(src == Cursor.src)
		return;
	else
		Cursor.src = src;

	if(Cursor.htmlObj==null)
		Cursor_Create(src);	
	else
		Cursor.img.src = src;	
	
	Cursor.offsetx= 5;
	Cursor.offsety= 5;
	
	if(x && y)
		HTMLHelper.setDimensions(Cursor.htmlObj,Cursor.offsetx+x,Cursor.offsety+y);
	Cursor.htmlObj.visibility = ns4?'show':'visible';
}

function Cursor_hide()
{
	if(Cursor.htmlObj) 
		Cursor.htmlObj.visibility = ns4?'hide':'hidden';
	Cursor.src = null;
}

function Cursor_docMouseMoveCallback(e,src)
{

	if(Cursor.src)
	{
		Cursor.htmlObj.left = Cursor.offsetx+(ns4?e.pageX:e.clientX+ document.body.scrollLeft);
		Cursor.htmlObj.top  = Cursor.offsety+(ns4?e.pageY:e.clientY+ document.body.scrollTop);
		
	}

return true;
}

Cursor.show = Cursor_show;
Cursor.hide = Cursor_hide;




