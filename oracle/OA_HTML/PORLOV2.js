/*===========================================================================+
 |      Copyright (c) 1998 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  FILENAME                                                                 |
 |      PORLOV.js                                                           | 
 |                                                                           |
 |  DESCRIPTION                                                              |
 |      Javascript rountines for LOV's                                       |
 |                                                                           |
 |  NOTES                                                                    |
 |     Must be included in every file that contains an LOV button            |
 |  DEPENDENCIES                                                             |
 |                                                                           |
 |  HISTORY                                                                  |
 |       23-NOV-98  marychu  created                                         |
 |                                                                           |
 +===========================================================================*/

/* $Header: PORLOV2.js 115.24 2001/08/10 16:43:21 pkm ship       $ */

function getOrgId(rForm)
{
  return ((rForm.POR_DELIVER_TO_ORG_ID && 
          rForm.POR_DELIVER_TO_ORG_ID.value != "") ? 
          "("+rForm.POR_DELIVER_TO_ORG_ID.value +")" :
     "(select nvl(hrl.inventory_organization_id,"+
                   "fsp.inventory_organization_id)"+
      " from hr_locations hrl," +
           "financials_system_parameters fsp " +
      "where hrl.location_code='" + rForm.POR_DELIVER_TO_LOCATION.value+
           "' and sysdate < nvl(hrl.inactive_date,sysdate+1))");
}

function getProjectId(rForm)
{
  return (rForm.POR_PROJECT_ID.value != "-9999") ? rForm.POR_PROJECT_ID.value :
     "(select project_id from pa_projects_expend_v "+
       "where project_number='" + rForm.POR_PROJECT.value + "' and rownum=1)";
}

function isInventory(rForm)
{
 return (rForm.POR_INV_REPL_REQUEST ? rForm.POR_INV_REPL_REQUEST.checked : 
   (rForm.POR_INV_REPLENISHMENT_FLAG ? rForm.POR_INV_REPLENISHMENT_FLAG.checked : false));
}
  
function callLOV(attr_id, attr_code, region_id, region_code,
                 form_name, frame_name, where_clause,
                 ind) 
{

  if(ind<0)
  {
    ind=top.getTop().findIndex(form_name, attr_code);
  }
  
  window.document.LOVReceiver= top.getTop().allForms[form_name][ind];
  var rForm= window.document.LOVReceiver;
  var js_where_clause= "";

  // set where clause
 
  // Bug 1575129: the hard wired where clause below eliminates expenditure orgs
  // whichare inactive (and are likely to be duplicate)
  if(attr_code=="POR_EXPENDITURE_ORG")
  {
     js_where_clause=" nvl(active_flag,'*') = 'Y' and trunc(sysdate) between date_from and nvl(date_to, trunc(sysdate)) " ;

  }
  else if(attr_code=="POR_DELIVER_TO_SUBINVENTORY")
  {

    if(top.getTop().isEmpty(rForm.POR_DELIVER_TO_LOCATION.value))
    {

      addToMessageStack(FND_MESSAGES['ICX_POR_ALRT_ENTER_LOCATION']);
      fieldEmptyCheck();

      return;
    }
    js_where_clause = "organization_id in " + getOrgId(rForm);
  }
  else if(attr_code=="POR_PROJECT" &&
          region_code!="POR_MASS_UPDATE_R")

  {
    js_where_clause= 
      (isInventory(rForm) ?
       "INVENTORY_ORGANIZATION_ID="+ getOrgId(rForm) :
       "INVENTORY_ORGANIZATION_ID=-9999");
  }
  else if (attr_code=="POR_TASK")
  {
    if(top.getTop().isEmpty(rForm.POR_PROJECT.value))
    {

       addToMessageStack(FND_MESSAGES['ICX_POR_ALRT_ENTER_PROJECT']);
       fieldEmptyCheck();

      return;
    }
    js_where_clause = "project_id="+ getProjectId(rForm);

    // if this is an inventory replenishment request, we also need to
    // check the task level of the deliver to org.
    if(isInventory(rForm) && region_code!='POR_MASS_UPDATE_R')
    {
      js_where_clause += 
        "and enabled_task_level in "+
         "(select MP.PROJECT_CONTROL_LEVEL FROM MTL_PARAMETERS MP"+
         " WHERE MP.ORGANIZATION_ID=" + getOrgId(rForm) + ")";    
    }
  }
  else if (attr_code=="POR_DELIVER_TO_LOCATION" && 
           region_code!='POR_MASS_UPDATE_R' &&
           rForm.POR_ITEM_ID && 0<rForm.POR_ITEM_ID.value)
  {
    if(isInventory(rForm))
    {
      js_where_clause= 
        " deliver_to_org_id in (select msi1.organization_id "+
        " from mtl_system_items msi1 " +
        " where msi1.inventory_item_id = "+ rForm.POR_ITEM_ID.value+
        " and msi1.stock_enabled_flag = 'Y') ";

    }
    else // line is for expense
    {
      js_where_clause =" deliver_to_org_id in (select msi2.organization_id "+
                                            " from mtl_system_items msi2 " +
                                            " where msi2.inventory_item_id ="+
                                                    rForm.POR_ITEM_ID.value +")";
    }

    if(rForm.POR_ITEM_REVISION && !top.getTop().isEmpty(rForm.POR_ITEM_REVISION.value))
    {
      js_where_clause += 
        " organization_id in (select mir.organization_id "+
        " from mtl_item_revisions mir "+
        " where mir.revision = "+ rForm.POR_ITEM_REVISION.value+
        " and mir.inventory_item_id = " + rForm.POR_ITEM_ID.value;
    }
  }
  else if(region_code == 'POR_RECEIVE_ORDERS_LINES_R' && attr_code =='POR_UNIT_OF_MEASURE' )
   {
     js_where_clause += " uom_class = '" + rForm.POR_UOM_CLASS.value + "'";
   }
 else if(attr_code== 'POR_CALL_CATEGORY_NAME')
  {
    js_where_clause = "type=2 ";
  }
  else if(region_code == 'POR_RCPT_EXP_SRCH_R' && attr_code =='POR_REQUESTER' )
  {
    //setting the where clause here but check for the function security in LOV.java
    //bug 1734333
    js_where_clause = "EMPLOYEE_ID  IN ( select employee_id from fnd_user where user_id = fnd_global.user_id)";
  }
  else 
  {
    js_where_clause = "";
  }

  // ideally would null out after user selects from LOV, but doing it
  // here for time 
  if(attr_code=="POR_DELIVER_TO_LOCATION")
  {
    if (rForm.POR_DELIVER_TO_SUBINVENTORY)
      rForm.POR_DELIVER_TO_SUBINVENTORY.value="";
  }
  else if(attr_code=="POR_PROJECT_ID")
  {
    if (rForm.POR_TASK)
    {
      rForm.POR_TASK= rForm.POR_TASK_ID="";
    }
  }

  //  if(document.debugLOV)
  //    top.getActiveWindow().alert(js_where_clause);
//  js_where_clause= escape(js_where_clause,1);
  //  top.getActiveWindow().alert(js_where_clause);
  //alert("js_where_clause="+js_where_clause);
  
  rForm.updated=true;

 /*
  LOV(attr_id, attr_code, region_id, region_code,"LOVReceiver",
      frame_name , where_clause,js_where_clause); 

 */

   if (!top.writeStateInfo || top.writeStateInfo == null) {
   } 
   else {
     if (ind > -1) {
       top.writeStateInfo(ind);
      }
     else {
       top.writeStateInfo();
     }
   }


   var action="displayLOVPage";
   var func="";

   var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";


   if (!top.trainTracksImage || top.trainTracksImage == null)
       top.trainTracksImage = null;

   setStateInfo("formName",form_name);
   setStateInfo("formIndex",ind);
   setStateInfo("fieldName",attr_code);

   top.submitRequest("createReq",
                    url,
                    action,
                    func,
                    top.FIELD_DELIMITER + "clickedLOV" + top.VALUE_DELIMITER + "true" + top.FIELD_DELIMITER + "attributeCode" + top.VALUE_DELIMITER + attr_code+ top.FIELD_DELIMITER + "attributeId" + top.VALUE_DELIMITER + attr_id+ top.FIELD_DELIMITER + "regionCode" + top.VALUE_DELIMITER + region_code + top.FIELD_DELIMITER + "regionId" + top.VALUE_DELIMITER + region_id + top.FIELD_DELIMITER + "formName" + top.VALUE_DELIMITER + form_name + top.FIELD_DELIMITER + "lineNumber" + top.VALUE_DELIMITER + ind + top.FIELD_DELIMITER + "trainTracksImage" + top.VALUE_DELIMITER + top.trainTracksImage + top.FIELD_DELIMITER + "attribute_default_value" + top.VALUE_DELIMITER +  top.getElementValue(ind,form_name,attr_code) + top.FIELD_DELIMITER + "highlighted_tab" + top.VALUE_DELIMITER + top.tabHighlighted + top.FIELD_DELIMITER + "highlighted_subtab" + top.VALUE_DELIMITER + (top.subTabHighlighted -1) + top.FIELD_DELIMITER + "js_where_clause" + top.VALUE_DELIMITER + js_where_clause + top.FIELD_DELIMITER + "where_clause" + top.VALUE_DELIMITER + where_clause,
                    new Array(form_name),
                    "_top"); 



}

function postUnNumberLOV(formName,index)
{
  if (top.allForms[formName][index].POR_HAZARD_CLASS_NAME != null) {
	top.allForms[formName][index].POR_HAZARD_CLASS_NAME.value=
	  top.allForms[formName][index].POR_HAZARD_CLASS_2.value; 
  }

}


/*
function postHazardClassLOV(formName,index)
{
  if (top.allForms[formName][index].POR_UN_NUMBER != null) {
	top.allForms[formName][index].POR_UN_NUMBER.value=
	  top.allForms[formName][index].POR_UN_NUMBER_2.value; 
  }

}
*/

function postRequesterLOV()
{
  if (window.document.LOVReceiver.POR_DELIVER_TO_LOCATION != null) {
  window.document.LOVReceiver.POR_DELIVER_TO_LOCATION.value=
    window.document.LOVReceiver.POR_DELIVER_TO_LOCATION_2.value;
// SGadkari Thu Jan 11 10:26:21 PST 2001 Changed region item for 
// bug id 1405523. POR_DELIVER_TO_LOCATION_ID_2 -> POR_DLVR_TO_LOCATION_ID_2
  window.document.LOVReceiver.POR_DELIVER_TO_LOCATION_ID.value=
    window.document.LOVReceiver.POR_DLVR_TO_LOCATION_ID_2.value;
  window.document.LOVReceiver.POR_DELIVER_TO_ORG_ID.value=
    window.document.LOVReceiver.POR_DELIVER_TO_ORG_ID_2.value;
  if(window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY) 
    window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY.value="";
  if(window.document.LOVReceiver.POR_SUBINVENTORY) 
    window.document.LOVReceiver.POR_SUBINVENTORY.value="";
  }
}



function postRequesterLOVForLine(formName,index)
{


  if (top.allForms[formName][index].POR_DELIVER_TO_LOCATION != null) {

  
  if (top.allForms[formName][index].POR_DELIVER_TO_LOCATION_2 != null
      && top.allForms[formName][index].POR_DELIVER_TO_LOCATION_2.value != "")
  top.allForms[formName][index].POR_DELIVER_TO_LOCATION.value=
    top.allForms[formName][index].POR_DELIVER_TO_LOCATION_2.value;


  // SGadkari Thu Jan 11 10:30:29 PST 2001 Bug 1405523 Changed region item
  // POR_DELIVER_TO_LOCATION_ID_2 to POR_DLVR_TO_LOCATION_ID_2
  //one was left out so changing now in this version
  if (top.allForms[formName][index].POR_DLVR_TO_LOCATION_ID_2 != null 
   && top.allForms[formName][index].POR_DLVR_TO_LOCATION_ID_2.value != "")
  top.allForms[formName][index].POR_DELIVER_TO_LOCATION_ID.value=
    top.allForms[formName][index].POR_DLVR_TO_LOCATION_ID_2.value;

  if (top.allForms[formName][index].POR_DELIVER_TO_ORG_ID_2 != null
   && top.allForms[formName][index].POR_DELIVER_TO_ORG_ID_2.value != "")
  top.allForms[formName][index].POR_DELIVER_TO_ORG_ID.value=
    top.allForms[formName][index].POR_DELIVER_TO_ORG_ID_2.value;

  if (top.allForms[formName][index].POR_DELIVER_TO_SUBINVENTORY) 
   top.allForms[formName][index].POR_DELIVER_TO_SUBINVENTORY.value="";

  if(top.allForms[formName][index].POR_SUBINVENTORY) 
    top.allForms[formName][index].POR_SUBINVENTORY.value="";

  }
}

function postLocationLOV()
{
  if(window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY) 
    window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY.value="";
}


function postLocationLOVForLine(formName,index)
{
  if(top.allForms[formName][index].POR_DELIVER_TO_SUBINVENTORY) 
   top.allForms[formName][index].POR_DELIVER_TO_SUBINVENTORY.value="";
}

function postLOVHandler(lovFunctionName,formName,index) {
  
 if (index >= 0) {
    if (lovFunctionName == "postRequesterLOV") {

          postRequesterLOVForLine(formName,index);
   }
   if (lovFunctionName == "postLocationLOV") {
          postLocationLOVForLine(formName,index);
   }
   if (lovFunctionName == "postUnNumberLOV") {
          postUnNumberLOV(formName,index);
   }

 }

}

function addApprover(approverId, approverName)
{
  top.getTop().submitRequest("requisition",  // should be approver_list
                             "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
                             window.functionName, // actionName
                             window.functionName, // functionName
                             top.getTop().FIELD_DELIMITER+ "approverName"+
                             top.getTop().VALUE_DELIMITER+ approverName +
                             top.getTop().FIELD_DELIMITER+ "approverId"+
                             top.getTop().VALUE_DELIMITER+ approverId,
                             (window.functionName=="addApprover" ?
                              new Array("POR_APPROVERS_R") : null),
                             "approvers_hidden");
}









