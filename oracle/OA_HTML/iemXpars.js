/*
<!-- 
 +============================================================================+
 |      Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA        |
 |                         All rights reserved.                               |
 +============================================================================+
 |  FILENAME:  iemXpars.js                                                    |
 +============================================================================+	
 |  DESCRIPTION:  Javascript that Parses XML.                                 |
 |	Called by: iemAddFd.jsp,iemRemFd.jsp,iemTree.jsp                        |
 |                                                                            |
 +============================================================================+
 |  HISTORY                                                                   |
 +============================================================================+
-->
<!-- $Header: iemXpars.js 115.1 2000/02/17 10:42:44 pkm ship      $ -->
*/

/** 
 * DOM object constructor
 * 
 * @return            	VOID
 */ 
function constructDom()
{
  this.type = "element";
  this.name = new String();
  this.attributes = new Array();
  this.contents = new Array();
  this.uid = _Xparse_count++;
  _Xparse_index[this.uid]=this;
}

/** 
 * character data type constructor
 * 
 * @return            	VOID
 */ 
function constructChar()
{
  this.type = "chardata";
  this.value = new String();
}

/** 
 * pi type constructor
 * 
 * @return            	VOID
 */ 

function constructPi()
{
  this.type = "pi";
  this.value = new String();
}

/** 
 * comment type constructor
 * 
 * @return            	VOID
 */ 
function constructComment()
{
  this.type = "comment";
  this.value = new String();
}

/** 
 * Holding object to encapsulate data structures sent between functions.
 * 
 * @return            	VOID
 */ 
function pObj()
{
  this.str = new String();
  this.ary = new Array();
  this.end = new String();
}

  var _Xparse_count = 0;
  var _Xparse_index = new Array();

/** 
 * Parses the xml.
 * 
 * @param pXml     passed in xml String
 * @return         Xml root object
 */ 
function XMLParse(pXml)
{
  var myStructs = new pObj();
  myStructs.str = fixLf(pXml);
  var root = new constructDom();
  root.name="ROOT";
  myStructs = XML2Array(myStructs);
  root.contents = myStructs.ary;
  root.index = _Xparse_index;
  _Xparse_index = new Array();
  return root;
}

/** 
 * Convert the xml string into an array.
 * 
 * @param pStructs  Data Structures passed in
 * @return          Converted Data Structure
 */ 
function XML2Array(pStructs)
{
  while(1)
	{  
	  if(pStructs.str.length == 0)
	    {
	      return pStructs;
	    }
	  var TagStart = pStructs.str.indexOf("<");
	  if (TagStart != 0)
	    {
	      var thisary = pStructs.ary.length;
	      pStructs.ary[thisary] = new constructChar();
		  if(TagStart == -1)
		    {
		     pStructs.ary[thisary].value = FixSpecChar(pStructs.str);
		     pStructs.str = "";
		    }
		  else
		    {
		     pStructs.ary[thisary].value = FixSpecChar(pStructs.str.substring(0,TagStart));
		     pStructs.str = pStructs.str.substring(TagStart,pStructs.str.length);
		    }
	    }
	  else
	    {
		  if(pStructs.str.substring(1,2) == "?")
		    {
		      pStructs = parsePi(pStructs);
		    }
		  else
		    {
		      if(pStructs.str.substring(1,4) == "!--")
			  {
			    pStructs = parseCom(pStructs);
			  }
			  else
			    {
			      if(pStructs.str.substring(1,9) == "![CDATA[")
			        {
				      pStructs = parseCd(pStructs);
				    }
			      else
			        {
                      if(pStructs.str.substring(1,pStructs.end.length + 3) == "/" + pStructs.end + ">" || WSremove(pStructs.str.substring(1,pStructs.end.length + 3)) == "/" + pStructs.end)
				        {
					      pStructs.str = pStructs.str.substring(pStructs.end.length + 3,pStructs.str.length);
					      pStructs.end = "";
					      return pStructs;
				        }
				      else
				        {
				          pStructs = parseElem(pStructs);
				        }
				    }
			   }
		    }
	}
}
  return "";
}

/** 
 * Processes element tags.
 * 
 * @param pStructs  Data Structures passed in
 * @return          Data Structures
 */ 
function parseElem(pStructs)
{
  var close = pStructs.str.indexOf(">");
  var empty = (pStructs.str.substring(close - 1,close) == "/");
  if(empty)
    {
      close -= 1;
    }
  var starttag = WSrestore(pStructs.str.substring(1,close));
  var nextspace = starttag.indexOf(" ");
  var attribs = new String();
  var name = new String();
  if(nextspace != -1)
    {
      name = starttag.substring(0,nextspace);
      attribs = starttag.substring(nextspace + 1,starttag.length);
    }
  else
    {
      name = starttag;
    }

  var thisary = pStructs.ary.length;
  pStructs.ary[thisary] = new constructDom();
  pStructs.ary[thisary].name = WSremove(name);
  if(attribs.length > 0)
    {
      pStructs.ary[thisary].attributes = parseElemVals(attribs);
    }
  if(!empty)
    {
      var contents = new pObj();
      contents.str = pStructs.str.substring(close + 1,pStructs.str.length);
      contents.end = name;
      contents = XML2Array(contents);
      pStructs.ary[thisary].contents = contents.ary;
      pStructs.str = contents.str;
    }
  else
    {
      pStructs.str = pStructs.str.substring(close + 2,pStructs.str.length);
    }
  return pStructs;
}

/** 
 * Processes pi tags.
 * 
 * @param pStructs  Data Structures passed in
 * @return          modified Data Structures
 */ 
function parsePi(pStructs)
{
  var close = pStructs.str.indexOf("?>");
  var val = pStructs.str.substring(2,close);
  var thisary = pStructs.ary.length;
  pStructs.ary[thisary] = new constructPi();
  pStructs.ary[thisary].value = val;
  pStructs.str = pStructs.str.substring(close + 2,pStructs.str.length);
  return pStructs;
}

/** 
 * Processes comment tags.
 * 
 * @param pStructs  Data Structures passed in
 * @return          modified Data Structures
 */ 
function parseCom(pStructs)
{
  var close = pStructs.str.indexOf("-->");
  var val = pStructs.str.substring(4,close);
  var thisary = pStructs.ary.length;
  pStructs.ary[thisary] = new constructComment();
  pStructs.ary[thisary].value = val;
  pStructs.str = pStructs.str.substring(close + 3,pStructs.str.length);
  return pStructs;
}

/** 
 * Processes cdata tags.
 * 
 * @param pStructs  Data Structures passed in
 * @return          modified Data Structures
 */ 
function parseCd(pStructs)
{
  var close = pStructs.str.indexOf("]]>");
  var val = pStructs.str.substring(9,close);
  var thisary = pStructs.ary.length;
  pStructs.ary[thisary] = new constructChar();
  pStructs.ary[thisary].value = val;
  pStructs.str = pStructs.str.substring(close + 3,pStructs.str.length);
  return pStructs;
}

/** 
 * Handles element attribute parsing
 * 
 * @param str     String
 * @return        Array of keys to values
 */ 
function parseElemVals(str)
{
  var all = new Array();
  while(1)
    {
      var eq = str.indexOf("=");
      if(str.length == 0 || eq == -1)
        {
	      return all;
        }
      var id1 = str.indexOf("\'");
      var id2 = str.indexOf("\"");
      var ids = new Number();
      var id = new String();
      if((id1 < id2 && id1 != -1) || id2 == -1)
        {
	      ids = id1;
	      id = "\'";
        }
      if((id2 < id1 || id1 == -1) && id2 != -1)
        {
	      ids = id2;
	      id = "\"";
        }
      var nextid = str.indexOf(id,ids + 1);
      var val = str.substring(ids + 1,nextid);

      var name = WSremove(str.substring(0,eq));
      all[name] = FixSpecChar(val);
      str = str.substring(nextid + 1,str.length);
     }
  return "";
}

/** 
 * Removes \r's from String
 * 
 * @param str      String
 * @return         xml String without \r's
 */ 
function fixLf(str)
{
  var A = new Array();
  A = str.split("\r\n");
  str = A.join("\n");
  A = str.split("\r");
  str = A.join("\n");
  var start = str.indexOf("<");
  if(str.substring(start,start + 3) == "<?x" || str.substring(start,start + 3) == "<?X" )
    {
      var close = str.indexOf("?>");
      str = str.substring(close + 2,str.length);
    }
  var start = str.indexOf("<!DOCTYPE");
  if(start != -1)
    {
      var close = str.indexOf(">",start) + 1;
      var dp = str.indexOf("[",start);
      if(dp < close && dp != -1)
        {
          close = str.indexOf("]>",start) + 2;
        }
      str = str.substring(close,str.length);
    }
return str;
}

/** 
 * Remove whitespace from String
 * 
 * @param str      String
 * @return         String w/o whitespace
 */ 
function WSremove(str)
{
  var A = new Array();
  A = str.split("\n");
  str = A.join("");
  A = str.split(" ");
  str = A.join("");
  A = str.split("\t");
  str = A.join("");
  return str;
}

/** 
 * Replace whitespace in String
 * 
 * @param str      String
 * @return         String w/whitespace
 */ 
function WSrestore(str)
{
  var A = new Array();
  A = str.split("\n");
  str = A.join(" ");
  A = str.split("\t");
  str = A.join(" ");
  return str;
}

/** 
 * Replaces special characters in String
 * 
 * @param str      String
 * @return         String w/ character escapes
 */ 
function FixSpecChar(str)
{
  var A = new Array();
  A = str.split("&lt;");
  str = A.join("<");
  A = str.split("&gt;");
  str = A.join(">");
  A = str.split("&quot;");
  str = A.join("\"");
  A = str.split("&apos;");
  str = A.join("\'");
  A = str.split("&amp;");
  str = A.join("&");
  return str;
}
