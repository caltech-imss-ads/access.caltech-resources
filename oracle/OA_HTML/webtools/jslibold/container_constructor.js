

tabcontrol.prototype.validargs = p_title+p_helptext+p_objectref+p_targetframe+p_initialtab;
tabcontrol.prototype.title = "No Title supplied";
tabcontrol.prototype.helptext = "";
tabcontrol.prototype.objectref = "";
tabcontrol.prototype.targetframe = "";
tabcontrol.prototype.currentTab = 1;
tabcontrol.prototype.initialtab = 1;
tabcontrol.prototype.titlehtml ="<Table width=100% cellpadding=0 cellspacing=0 border=0><tr class=color3>" +
				                "<td rowspan=3><img src=OA_HTML/webtools/images/pixel_color3.gif height=1 width=1></td>" +
				                "<td rowspan=3 nowrap><font class=tabtitle>TtTtT</font></td>" +
				                "<td rowspan=3 width=10000></td>";
tabcontrol.prototype.otherTab2 ="<td rowspan=2><img src=OA_HTML/webtools/images/tab_left_non_selected.gif></td>" +
                                "<td class=othertab height=1><img src='OA_HTML/webtools/images/pixel_color4.gif'></td>" +
                                "<td rowspan=2><img src='OA_HTML/webtools/images/tab_right_non_selected.gif'></td>";
tabcontrol.prototype.otherTab3a="<td class=othertab nowrap valign=middle>";
tabcontrol.prototype.otherTab3b="<a class=othertabtext title='HhHhH' " +
                                "href='javascript:parent.BbBbB.switchtab(NnNnN,window,top.FfFfF," +
                                '"UuUuU")' + "'>TtTtT</a>"
tabcontrol.prototype.otherTab3c="</td>";
tabcontrol.prototype.otherTab4= "<td colspan=3 class=highlight><img src='OA_HTML/webtools/images/pixel_color6.gif'></td>";
tabcontrol.prototype.anyTab1 =  "<td colspan=3><img src='OA_HTML/webtools/images/pixel_color3.gif' height=6></td>";
tabcontrol.prototype.curTab2 =  "<td rowspan=2><img src='OA_HTML/webtools/images/tab_left_selected.gif'></td>" +
                                "<td class=highlight height=1>" +
                                "<img src='OA_HTML/webtools/images/pixel_color6.gif'></td><td rowspan=2>" + 
                                "<img src='OA_HTML/webtools/images/tab_right_selected.gif'></td>";
tabcontrol.prototype.curTab3a = "<td class=currenttab nowrap valign=middle>";
tabcontrol.prototype.curTab3b = "<font class=currenttabtext>TtTtT</font>";
tabcontrol.prototype.curTab3c = "</td>";
tabcontrol.prototype.curTab3d = "<a class=currenttabtext " +
                                "href='javascript:parent.BbBbB.switchtab(NnNnN,window,top.FfFfF," +
                                '"UuUuU")' + "'>TtTtT</a>"
tabcontrol.prototype.curTab4 = 	"<td colspan=3 class=panel><img src='OA_HTML/webtools/images/pixel_gray5.gif'></td>";
tabcontrol.prototype.disTab3a =  "<td class=othertab nowrap valign=middle>";
tabcontrol.prototype.disTab3b = "<a class=disabledtabtext title='HhHhH'>TtTtT</a>"
tabcontrol.prototype.disTab3c = "</td>";
tabcontrol.prototype.helphtml = "<tr class=panel><td colspan=PpPpP height=200 valign=top><FONT CLASS=helptext>" +
                                "TtTtT</td></tr></table>";
//tabcontrol.prototype.iconhtml = "<a href='javascript:JjJjJ'><img src='IiIiI' align=absmiddle alt='HhHhH'></a>"
tabcontrol.prototype.render = rendertabs;
tabcontrol.prototype.renderbottom = renderbottom;
tabcontrol.prototype.addtab = addtab;
tabcontrol.prototype.switchtab = switchtab;
tabcontrol.prototype.modifytab = modifytab;

//**********************************************************************************
function tabcontrol ( P_args) //tabcontrol object constructor function
{ 
  if (P_args)
  {
    args = P_args.split(";"); 
    for (var i=0; i < args.length; i++) 
    {
      var arg = args[i];
      if (!arg) break; 
      parsevalues(arg,this,this.validargs);
    }
  }
	this.tabarray = new Array;
}
//**********************************************************************************
tab.prototype.validargs = p_name+p_text+p_hint+p_disabledhint+p_url+p_enabled+p_visible+p_alwaysactive+p_iconobj;
tab.prototype.name = ""
tab.prototype.text = "";
tab.prototype.hint = "";
tab.prototype.disabledhint = "";
tab.prototype.url = "#";
tab.prototype.alwaysactive = false;
tab.prototype.enabled = true;
tab.prototype.visible = true;
function tab(P_args)
{ 
  this.iconobj = null;
  if (!P_args) return;
  args = P_args.split(";"); 
  for (var i=0; i < args.length; i++) 
  {
    var arg = args[i];
    if (!arg) break; 
    parsevalues(arg,this,this.validargs);
  }
  if (this.iconobj != null) this.iconobj = eval(this.iconobj);
}

function addtab (tab_obj) 
{
	this.tabarray[this.tabarray.length] = tab_obj;
}
//this can also be accomplished by simply re-arranging or replacing the objects in the tab array
function modifytab ( tab, P_args, Frame_ref) {
	if (!P_args) return;
	if (isNaN(tab)) {
		tabnum = -1;
		for (var i=0; i<this.tabarray.length; i++) {
			if (tab == this.tabarray[i].name) tabnum = i;
		}
	} else {
		var tabnum = tab-1;
	}
	if (tabnum == -1) return;
	args = P_args.split(";"); 
	for (var i=0; i < args.length; i++) {
		parsevalues (args[i], this.tabarray[tabnum], this.tabarray[tabnum].validargs)
	}
	if (Frame_ref) Frame_ref.location.reload(); 
}

function rendertabs(window_Ref) {
    if (this.initialtab != null)
	{
	  this.currentTab = this.initialtab;
	  this.initialtab = null;
	}
	output = 	"";
	spancount = 2;
	tabrow1 = 	this.titlehtml.replace(reptxt,this.title);
	tabrow2 = 	"<tr class=color3>"
	tabrow3 = 	"<tr class=color3>";
	tabrow4 = 	"<tr><td rowspan=2 class=panel valign=top><img src='OA_HTML/webtools/images/container_top_left_tabs.gif'></td>" +
			"<td colspan=2 class=highlight><img src='OA_HTML/webtools/images/pixel_color6.gif'></td>";
	for (i=0;i<this.tabarray.length;i++) 
	{
		if (this.tabarray[i].visible == "false"||eval(this.tabarray[i].visible) == false) continue;
		spancount = spancount + 3
		tabrow1 += this.anyTab1;
		if (i == this.currentTab - 1) {
			tabrow2 += this.curTab2;
			tabrow3 += this.curTab3a;
			if (this.tabarray[i].iconobj != null) tabrow3 += this.tabarray[i].iconobj.render("left")
			if (this.tabarray[i].alwaysactive == "true" || this.tabarray[i].alwaysactive == true)
			{
				temptext = this.curTab3d.replace(reptxt,this.tabarray[i].text);
				temptext = temptext.replace(rephlp,this.tabarray[i].hint);
				temptext = temptext.replace(repnum,i+1);
				temptext = temptext.replace(repbar,this.objectref);
				temptext = temptext.replace(repurl,this.tabarray[i].url);
				temptext = temptext.replace(repfram,this.targetframe);
				tabrow3 += temptext;
			}
			else
			{
			  tabrow3 += this.curTab3b.replace(reptxt,this.tabarray[i].text);
			}
			if (this.tabarray[i].iconobj != null) tabrow3 += this.tabarray[i].iconobj.render("right")		
			tabrow3 += this.curTab3c;
			tabrow4 += this.curTab4;
		} else {
			tabrow2 += this.otherTab2;
			if (this.tabarray[i].enabled == "false"||eval(this.tabarray[i].enabled) == false) 
			{
			  tabrow3 += this.disTab3a;
			  if (this.tabarray[i].iconobj != null) tabrow3 += this.tabarray[i].iconobj.render("left",true)
			  tabrow3 += this.disTab3b.replace(reptxt,this.tabarray[i].text);
			  tabrow3 = tabrow3.replace(rephlp,this.tabarray[i].disabledhint); alert(this.tabarray[i].disabledhint);
			  if (this.tabarray[i].iconobj != null) tabrow3 += this.tabarray[i].iconobj.render("right",true)
			  tabrow3 += this.disTab3c;
			} 
			else 
			{
			    temptext = this.otherTab3a;
				if (this.tabarray[i].iconobj != null) temptext += this.tabarray[i].iconobj.render("left")
				temptext += this.otherTab3b.replace(reptxt,this.tabarray[i].text);
				temptext = temptext.replace(rephlp,this.tabarray[i].hint);
				temptext = temptext.replace(repnum,i+1);
				temptext = temptext.replace(repbar,this.objectref);
				temptext = temptext.replace(repurl,this.tabarray[i].url);
				temptext = temptext.replace(repfram,this.targetframe);
				if (this.tabarray[i].iconobj != null) temptext += this.tabarray[i].iconobj.render("right")
				temptext += this.otherTab3c;
			tabrow3 += temptext;
			}
			tabrow4 += this.otherTab4;
		}
	}
	tabrow1 += "<td rowspan=3><img src='OA_HTML/webtools/images/pixel_color3.gif'></td></tr>";
	tabrow2 += "</tr>";
	tabrow3 += "</tr>";
	tabrow4 += "<td rowspan=2 class=panel " + 
			"align=top><img src='OA_HTML/webtools/images/container_top_right_tabs.gif'></td></tr>";	
	output += 	tabrow1 + tabrow2 + tabrow3 + tabrow4;
	temptext = this.helphtml.replace(reptxt,this.helptext);
	temptext = temptext.replace(repspan,spancount);
	output +=	temptext

  if (window_Ref) 
  { 
    if (window_Ref == "HTML") 
	{ 
	  return output; 
	}
	else 
	{ 
	  window_Ref.document.write(output); 
	  return "";
    }
  }
  else 
  {
    document.write(output);
	return "";
  }
}
function switchtab ( currtab, window_Ref, frame_ref, newurl ) {
	this.currentTab = currtab;
	window_Ref.location.reload();
	if (frame_ref) frame_ref.location = newurl;
}

//**********************************************************************************
tabicon.prototype.validargs = p_iconname + p_disablediconname + p_iconposition + p_hint + p_disabledhint + p_actiontype + p_url + p_targetframe + p_action + p_enabled;
tabicon.prototype.iconname = null;
tabicon.prototype.disablediconname = null;
tabicon.prototype.iconposition = "right";
tabicon.prototype.hint = "";
tabicon.prototype.disabledhint = "";
tabicon.prototype.actiontype = "url"
tabicon.prototype.url = "#";
tabicon.prototype.targetframe = "_self";
tabicon.prototype.action = "";
tabicon.prototype.enabled = "true";
tabicon.prototype.calcHtml = calcTabiconHtml;
tabicon.prototype.render = renderTabicon;
tabicon.prototype.html = "<a AaAaA><img src='IiIiI' align=VvVvV border=no alt='HhHhH'></a>"
tabicon.prototype.disabledhtml = "<img src='IiIiI' align=VvVvV alt='HhHhH'>"
function tabicon(P_args)
{
  if (!P_args) return;
  args = P_args.split(";"); 
  for (var i=0; i < args.length; i++) 
  {
    var arg = args[i];
    if (!arg) break; 
    parsevalues(arg,this,this.validargs);
  }
  if (this.disablediconname == null) this.disablediconname = this.iconname;
  this.calcHtml();
}
function calcTabiconHtml()
{ 
  command = "href=";
  if (this.actiontype=="url") command += "'" + this.url + "'";
  else command += "'javascript:void " + this.action + "'";
  command += (this.targetframe=="")?"":" target=" + this.targetframe;
  
  this.html = this.html.replace(repact,command);
  this.html = this.html.replace(repicn,this.iconname)
  this.html = this.html.replace(rephlp,this.hint)
  if (anyNav)
    this.html = this.html.replace(repval,(this.iconposition=="right")?"absmiddle":"left hspace=0");
  else
    this.html = this.html.replace(repval,"absmiddle");
  this.disabledhtml = this.disabledhtml.replace(repicn,this.disablediconname)
  this.disabledhtml = this.disabledhtml.replace(rephlp,this.disabledhint)
  if (anyNav)
    this.disabledhtml = this.disabledhtml.replace(repval,(this.iconposition=="right")?"absmiddle":"left hspace=0");
  else
    this.disabledhtml = this.disabledhtml.replace(repval,"absmiddle");
}
  
function renderTabicon(p_position, p_disabled)
{  
	if (p_position != this.iconposition) return "";
	if (p_disabled) return this.disabledhtml;
	return this.html;
}




//**********************************************************************************
notabcontrol.prototype.validargs = p_title+p_helptext+p_longpage;
notabcontrol.prototype.title = "No Title supplied";
notabcontrol.prototype.helptext = "";
notabcontrol.prototype.longpage = "true";
notabcontrol.prototype.tophtml =  "<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%><TR><TD " +
                                  "class=paneltitle ROWSPAN=2 " +
                                  "valign=top width=1><IMG SRC='OA_HTML/webtools/images/container_top_left_notabs.gif'></TD>" + 
                                  "<TD class=highlight WIDTH=10000 HEIGHT=1><IMG SRC='OA_HTML/webtools/images/pixel_color6.gif' height=1>" +
                                  "</TD><TD ROWSPAN=2 class=paneltitle valign=top width=1>" +
                                  "<IMG SRC='OA_HTML/webtools/images/container_top_right_notabs.gif'></TD></TR>" + 
                                  "<TR><TD class=paneltitle valign=top>TtTtT</TD></TR><TR>" +
                                  "<TD class=highlight COLSPAN=3 HEIGHT=1><img src=OA_HTML/webtools/images/pixel_color6.gif></TD></TR><TR>" +
                                  "<TD class=panel colspan=3 " 
notabcontrol.prototype.render = rendernotabs;
notabcontrol.prototype.renderbottom = renderbottom;

function notabcontrol(P_args) 
{
  if (P_args)
  {
    args = P_args.split(";"); 
    for (var i=0; i < args.length; i++) 
    {
      var arg = args[i];
      if (!arg) break; 
      parsevalues(arg,this,this.validargs);
    }
  }
  this.tophtml += (this.longpage == "true")? " height=100 valign=top>" : "valign=top>";
  this.tophtml += "<font class=helptext>&nbsp;HhHhH</font></td></tr></table>" 
}
function rendernotabs(window_Ref, p_content) 
{
	output = this.tophtml;
	output = output.replace(reptxt,this.title);
	output = output.replace(rephlp,this.helptext);

  if (window_Ref) 
  { 
    if (window_Ref == "HTML") 
	{ 
	  return output; 
	}
	else 
	{ 
	  window_Ref.document.write(output); 
	  return "";
    }
  }
  else 
  {
    document.write(output);
	return "";
  }
}

panelbottom = new Object;
panelbottom.render = renderbottom;

function renderbottom (window_Ref) {
  output = "<table width=100% cellpadding=0 cellspacing=0 border=0>" +
           "<tr><td class=panel rowspan=3 align=left valign=bottom width=10>" +
			"<img src='OA_HTML/webtools/images/container_bottom_left.gif' height=5 width=5></td>" +
			"<td class=panel height=5 width=1000><img src='OA_HTML/webtools/images/pixel_gray5.gif' height=2></td>" +
			"<td class=panel rowspan=3 align=right valign=bottom width=10>" +
			"<img src='OA_HTML/webtools/images/container_bottom_right.gif' height=5 width=5></td></tr>" +
			"<tr><td class=panel nowrap><img src='OA_HTML/webtools/images/pixel_gray5.gif' width=1></td></tr>" +
			"<tr><td class=panel><img src='OA_HTML/webtools/images/pixel_gray5.gif' height=1></td></tr></TABLE>";

  if (window_Ref) 
  { 
    if (window_Ref == "HTML") 
	{ 
	  return output; 
	}
	else 
	{ 
	  window_Ref.document.write(output); 
	  return "";
    }
  }
  else 
  {
    document.write(output);
	return "";
  }
}
