/* $Header: czContr.js 115.10 2000/12/10 16:10:16 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash K MacClay Created.                       |
 |                                                                           |
 +===========================================================================*/
 
function Container()
{
  this.parentConstructor = Base;
  this.parentConstructor();

  this.children = null;
}

function Container_add() 
{
  if(arguments) {
    if (this.children == null) 
      this.children = new Array();
    
    for (var i=0; i<arguments.length; i++) {
      if(arguments[i]) {
        this.children[arguments[i].name] = arguments[i];
        if(arguments[i].setParentObject) 
          arguments[i].setParentObject(this);
        else
          arguments[i].parentObj = this;
        
        arguments[i].setReference(this.reference + (ns4?'.layers["'+this.objId+'"].document':''));
        if(this.launched)
          arguments[i].runtimeRender(null, this, this.eventManager);
      }	
    }
  }
}

function Container_contains(obj) 
{
  if(this.children) {
    for(var childName in this.children)
      if(this.children[childName]== obj)
        return true;
  }
  return false;
}

function Container_getComponent(name) 
{
  if(this.children)
    if(this.children[name])
      return this.children[name];
    else
      return null;
}

function Container_setReference(ref)
{
  if(ref) {
    this.reference = ref;
    if (this.children) {
      var sChildRef = this.reference + (ns4?'.layers["'+this.objId+'"].document':'');
      for(var i in this.children) 
        this.children[i].setReference(sChildRef); 
    }
  }
}

function Container_innerRender(bRuntime)
{
  var sBuffer;
  if(! bRuntime) {
    sBuffer ="";
    if(this.content)
      sBuffer = this.content;
    if(this.children) {
      for (var i in this.children) {
        this.children[i].parentType = this.type;
        sBuffer += '\n' + this.children[i].render();
      }
    }
  } else {
    if(this.content)
      HTMLHelper.writeToLayer(this.block,this.content);

    if ((! this.children) && this.buildControl) {
      this.buildControl ();
      if (this.launched) return;
    }
    if(this.children) {
      for (var i in this.children)
        this.children[i].runtimeRender(null, this, this.eventManager);
    }
  }
  return sBuffer;
}

function Container_moreLaunch(em) 
{
  if(this.children) {
    for (var childName in this.children) 
      this.children[childName].launch(em);
  }
}

function Container_moreCSS() 
{
  var CSSStr="";
  if ((! this.children) && this.buildControl) {
    this.buildControl ();
  }
  if(this.children) {
    for (var childName in this.children) {
      CSSStr += this.children[childName].generateCSS() + '\n'; 
    }
  }
  return(CSSStr);
}

function Container_cropToObjects(dir)
{
  var maxX=0;
  var maxY=0;
  var tempX;
  var tempY;

  if (this.children) {
    for(var i in this.children) {
      tempX = this.children[i].left+this.children[i].width;
      tempY = this.children[i].top+this.children[i].height;
      if (tempX > maxX) maxX = tempX;
      if (tempY > maxY) maxY = tempY;
    }
    if(dir == null) {
      if(maxX)
        this.setWidth(maxX);
      if(maxY)
        this.setHeight(maxY);
    } else if(dir == 'h') {
      if(maxX)
        this.setWidth(maxX);
    } else if(dir == 'v') {
      if(maxY)
        this.setHeight(maxY);
    }
  }
  if(this.launched) {
    if(this.block == null)
      this.connect();
    var wd = ns4?this.block.clip.width:this.block.scrollWidth;
    var ht = ns4?this.block.clip.height:this.block.scrollHeight;
    if(wd > this.width) 
      this.setWidth(wd);
    if(ht > this.height)
      this.setHeight(ht);
  }
}

function Container_moreDestroy ()
{
  if (this.children) {
    for (var id in this.children) {
      this.children[id].destroy ();
      
      this.children[id] = null;
      delete this.children[id];
    }
    this.children = null;
    delete this.children;
  }
}

HTMLHelper.importPrototypes(Container,Base);

Container.prototype.constructor 	= Container;
Container.prototype.innerRender 	= Container_innerRender;
Container.prototype.moreCSS 		= Container_moreCSS;
Container.prototype.moreLaunch 		= Container_moreLaunch;
Container.prototype.add 		= Container_add;
Container.prototype.addComponent 	= Container_add;
Container.prototype.getComponent 	= Container_getComponent;
Container.prototype.contains	 	= Container_contains;
Container.prototype.setReference 	= Container_setReference;
Container.prototype.cropToObjects 	= Container_cropToObjects;
Container.prototype.moreDestroy = Container_moreDestroy;
