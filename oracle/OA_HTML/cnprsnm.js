/**-=========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  FILENAME                                                                 |
 |        cnprsnm.js                                                          | 
 |                                                                           |
 |  DESCRIPTION                                                              |
 |    Library of javascript procedures used in Sales Comp JSP pages          |
 |    use to parse number                                                    |
 |  NOTES                                                                    |
 |                                                                           |
 |  DEPENDENCIES                                                             |
 |  HISTORY                                                                  |
 |                                                                           |
 +=========================================================================**/

 <!-- $Header: cnprsnm.js 115.5 2000/11/08 17:14:14 pkm ship      $ -->
 
var errInput = 'Invalid Number.';
var Deci = '.';
var Group =  ',';

 /*
 * This function takes a number and check if is a valid number
 * If valid, return a number without any format strings
 */
 function cn_parseNum(P_num)
 {
    var  n = new String(P_num);
    var negative = false;
    
    n = trim(n); // remove leading and tailing space
    
    // remove negative format (XXX)
    rmNegPR = /^\((.*)\)$/;
    if (n.search(rmNegPR) != -1) 
    {
      n = "-";
      n += RegExp.$1;
      negative = true;
      // alert('Remove negative (XXX) n = ' + n);
    } 

    // remove negative format <XXX>
    rmNegGT = /^<(.*)>$/;
    if (n.search(rmNegGT) != -1) 
    {
      n = "-";
      n += RegExp.$1;
      negative = true;  
      // alert('Remove negative <XXX> n = ' + n);
    } 
    
    // remove tailing '-'
    rmTailNeg = /(.*)-$/;
    if (n.search(rmTailNeg) != -1) 
    {
      n = "-";
      n += RegExp.$1;
      // alert('Remove tailing negative XXX- n = ' + n);
    } 
    
    // check for negative number 
    if (n.search(/^-/) == 0) negative = true;
    
    // check if more than 1 '+' occurred
    chkPositive = new RegExp("\\+.*\\+");
    if (n.search(chkPositive) != -1) {
     // alert ('Invalid number -- extra positive sign');
     return null;
    }		
    
    // remove leading and tailing '+' 
    rmPos = /\+/;
    if ( !negative && (n.search(rmPos) == 0 || n.search(rmPos) == (n.length-1)) ) 
    {
      n = n.replace(rmPos,'');
      // alert('Remove positive XXX+ or +XXX n = ' + n);
    } 
    
    // check if include non-digit character, 
    // except Decimal or Group delimiter, Negtive sign
    chkChar = new RegExp("[^0-9"+Deci+Group+"-]") ;
    if (n.search(chkChar) != -1 ) {
     // alert ('Invalid number -- Non digit char exist');
     return null;
    }	
    // remove Group Delimiter
    rmGroupDelimiter = new RegExp(Group,"g") ;	
    n = n.replace(rmGroupDelimiter,'');
    
    // check if more than 1 decimal point occurred
    chkDecimal = new RegExp("\\"+Deci+".*"+"\\"+Deci);
    if (n.search(chkDecimal) != -1) {
     // alert ('Invalid number -- extra decimal point');
     return null;
    }	
    // check if more than 1 '-' occurred
    chkNegative = new RegExp("-.*-");
    if (n.search(chkNegative) != -1) {
     // alert ('Invalid number -- extra negative sign');
     return null;
    }    

    // remove leading and tailing 0's if have decimal digit
    if (n.indexOf(Deci) != -1) {	
      n = trimzero(n); 
      if (n.charAt(n.length-1) == Deci) {
        rmDeci = new RegExp("\\.");
        n = n.replace(rmDeci,'');
      }
      if (n == "") { n = "0";}
    }  

    // alert('number = ' + n); 
    return n;  
 }
 
 
/**
* This function takes a string and trims white spaces
* from both front and end.
**/
function trim(str)
{
  // starting position
  var startPosition = 0;
  // end position
  var endPosition = 0;
  var i;

  for (i = 0; i < str.length; i++)
  {
    // check for first non-white space
    if (str[i] != ' ')
    {
      startPosition =  i;
      break;
    }
  }
  if (i == str.length)
    return "";

  // check for the end position
  for (i = str.length - 1; i > startPosition; i--)
  {
    if (str[i] != ' ')
      break;
  }
  endPosition = i + 1;
  // just return everything in between  
  return str.substring(startPosition, endPosition);
}

/**
* This function takes a string and trims 0's
* from both front and end.
**/
function trimzero(str)
{
  // starting position
  var startPosition = 0;
  // end position
  var endPosition = 0;
  var i;

  for (i = 0; i < str.length; i++)
  {
    // check for first non-white space
    if (str[i] != '0')
    {
      startPosition =  i;
      break;
    }
  }
  if (i == str.length)
    return "";

  // check for the end position
  for (i = str.length - 1; i > startPosition; i--)
  {
    if (str[i] != '0')
      break;
  }
  endPosition = i + 1;
  // just return everything in between  
  return str.substring(startPosition, endPosition);
}

// -------------------------------------------------------------------
// Function to check if user input is valid number
function isValidNumber(inNumber,origVal) 
{
  if ( (inNumber.value == "") || ( cn_parseNum(inNumber.value) == "" ) ) {
    inNumber.value = "0";
    return true;
  }
  if ( cn_parseNum(inNumber.value) == null ) {
    alert(errInput);
    inNumber.value=origVal;
    inNumber.focus();
    return false;
  } else {
    return true;
  }    
}
