/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: PORWUTIL.js 115.20 2000/05/02 17:15:45 pkm ship    $ */

if(parseInt(navigator.appVersion) >= 4) 
{
  // bug 1040140
  IS_IE4=false;

  if (navigator.appName == "Netscape")
  {
    // Web browser is a netscape nevigator
    IS_NAV= true;
    IS_IE=false;
    coll = "";
    styleObj = "";
    window.captureEvents(Event.FOCUS|Event.CLICK |Event.RESIZE);
    suffix="_nav";
  }
  else // Internet Explorer
  {
    IS_NAV= false;
    IS_IE= true;
    coll = "all.";
    styleObj = ".style";
    layerTab= "div";
    suffix="_ie";

    IS_IE4=(navigator.appVersion.indexOf("MSIE\ 4") !=-1);

  }
}
else // invalid version
{
  alert(top.getTop().FND_MESSAGES['ICX_POR_ALRT_INVALID_BROWSER']);
}

mWin =null;


function setSubmitFlag()
{
  top.getTop().submitInProcess = true;
}

function clearSubmitFlag()
{
  top.getTop().submitInProcess = false;
}

function checkSubmitFlag()
{
  if (top.getTop().submitInProcess)
  {
    alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_DONT_CLICK_TWICE"]);
  }
  return top.getTop().submitInProcess;
}

function trapEscapeAction(win)
{

	  if (win.event)
      {
        var charCode = win.event.keyCode;
        if (charCode == 27)
        {
          clearSubmitFlag();
          return;
        }
      }


    // loop thru all the frames determine which one generated the event
    for(var w=0; w<win.frames.length; w++)
    {
      trapEscapeAction(win.frames[w]);
	}

    // loop thru all the modal windows to determine which one generated the event
    if (win.mWin!=null && !win.mWin.closed) 
      trapEscapeAction(win.mWin);	
}


// Cannot pass a parameter to the event handler
function trapEscape()
{
   trapEscapeAction(top);
}


function setTrapEscape(win)
{
  if (!IS_IE)
    return;
  
  if (win.document)
    win.document.onkeydown=trapEscape;
  
  // set the event handler for all frame
  for(var w=0; w<win.frames.length; w++)
    setTrapEscape(win.frames[w]);

  // and all modal windows
  if (win.mWin!=null && !win.mWin.closed) 
    setTrapEscape(win.mWin);
}

function displayMessageJumpActions()
{
//bug 1044015

  if (top.justJumped && top.jumpActionOrigin == 'wfNotification' && top.jumpExitFlag == 'y')
  {
     alert(top.FND_MESSAGES['ICX_POR_CANNOT_UPDATE_REQ']);
     top.close();
     top.justJumped = false;
  }

  if (top.justJumped && (top.jumpActionOrigin == 'tpn' || top.jumpActionOrigin == 'wfNotification' || top.jumpActionOrigin == 'viewReqs'))
  {
    if (top.jumpErrorFlag == 'y' || (top.jumpErrorFlag == 'n' && top.jumpDisplayMessage != ''))
    { 
	  alert(top.jumpDisplayMessage);
      top.clearSubmitFlag();
	}
    top.justJumped = false;
    // display requisition number in page title
    var reqNum = top.getTop().allForms["POR_HEADER_R"][0].POR_REQ_NUM.value

    if (reqNum != '')
    {
      top.reqNumber = reqNum;
      if (top.emergencyReqFlag == 'Y')
      {
        top.emgAddItemsIndex.title = top.FND_MESSAGES['ICX_POR_TASK_EMG_REQS'].replace("&REQ_NUM",top.reqNumber).replace("&PO_NUM",top.getTop().allForms["POR_HEADER_R"][0].POR_EMERGENCY_PO_NUM.value);
        top.emgEditReqIndex.title = top.emgAddItemsIndex.title;
      }
      else
      {
//        top.addItemsIndex.title = top.FND_MESSAGES['ICX_POR_TASK_ADD_ITEMS'] + ' (' + top.reqNumber + ')';
//        top.editReqIndex.title = top.FND_MESSAGES['ICX_POR_TASK_EDIT'] + ' (' + top.reqNumber + ')';
        top.addItemsIndex.title = top.FND_MESSAGES['ICX_POR_TASK_ADD_ITEMS_2'].replace("&REQ_NUM",top.reqNumber);
        top.editReqIndex.title = top.FND_MESSAGES['ICX_POR_TASK_EDIT_2'].replace("&REQ_NUM",top.reqNumber);
      }
      top.findFrame(top, "por_top").location.reload();
    }

  }

}

function displayMessage(doc)
{
  top.getTop().initDocument(doc);

  var f = top.getTop().allForms["POR_MESSAGE_FORM"][0]

  if (f.POR_ERROR_FLAG.value == 'y') {
    // Close the warning window if this is submit requisition in case of error
    if (f.POR_ACTION.value == 'submitReq' || f.POR_ACTION.value == 'saveAndSubmitReq') 
       mWin.close();
       mWin = null;
    alert(f.POR_DISPLAY_MESSAGE.value);
    top.clearSubmitFlag();
  }


  if (f.POR_ERROR_FLAG.value == 'n' && f.POR_DISPLAY_MESSAGE.value != '')
  {
    if (f.POR_ACTION.value == 'submitReq' || f.POR_ACTION.value == 'saveAndSubmitReq')
      mWin.location='/OA_HTML/PORARSMF.htm';
    else
      alert(f.POR_DISPLAY_MESSAGE.value);
      top.clearSubmitFlag();
  }

  postDisplayMessage(f);
}

function postDisplayMessage(f)
{

  if(f) {
  	top.getTop().setReqTotal(f.POR_REQ_TOTAL.value);
	top.getTop().setLineCount(f.POR_TOTAL_LINES.value);
	}

  // show req number, if req is saved
  if ((f.POR_ACTION.value == 'saveReqToolbar' || f.POR_ACTION.value == 'saveReqFinalReview') && f.POR_ERROR_FLAG.value == 'n')
  {
    top.reqNumber = f.POR_REQ_NUM.value;
    if (top.emergencyReqFlag == 'Y')
	{
	  top.emgAddItemsIndex.title = top.FND_MESSAGES['ICX_POR_TASK_EMG_REQS'].replace("&REQ_NUM",top.reqNumber).replace("&PO_NUM",f.POR_EMG_PO_NUM.value);
      top.emgEditReqIndex.title = top.emgAddItemsIndex.title;
	}
	else
	{
      top.addItemsIndex.title = top.FND_MESSAGES['ICX_POR_TASK_ADD_ITEMS_2'].replace("&REQ_NUM",top.reqNumber);
      top.editReqIndex.title = top.FND_MESSAGES['ICX_POR_TASK_EDIT_2'].replace("&REQ_NUM",top.reqNumber);
    }
    top.findFrame(top, "por_top").location.reload();
  }

  // close the window if user preferences page
  if (f.POR_ACTION.value == 'savePreference' && f.POR_ERROR_FLAG.value == 'n')
  {
    clearSubmitFlag();
    top.close();
  }

  // set reqSessionId if Add to Requisition (special order)
  // Do not check for POR_ERROR_FLAG here, because error can happen only in one item if more
  // are being added.  We need to update the req total nevertheless.
  if (f.POR_ACTION.value == 'addSpecialItem' || f.POR_ACTION.value == 
    'addFavListItem' || f.POR_ACTION.value=='addCatalogItem')
  {
    top.getTop().reqSessionId = f.POR_REQ_SESSION_ID.value;

    var winName= (f.POR_ACTION.value =='addCatalogItem') ? "searchFooter" :
      "content_bottom";
    top.position(winName);
  }
  // close the window if info template page
  if (f.POR_ACTION.value == 'updateInfoTemplate' && f.POR_ERROR_FLAG.value == 'n')
    top.close();

  // display final review page, if no error message while saving requisition
  if (f.POR_ACTION.value == 'saveReqFinalReview' && f.POR_ERROR_FLAG.value == 'n')
  {
     top.setSubmitFlag();
     var url="/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN=" + 
       getReqToken("showPage", "finalReview", "requisition");
     findFrame(top, "por_main").location=url;
  }

  // refresh edit req page, if no error message after applying distributions, line details, mass update
  // copy lines or delete lines.  Any open modal window is closed automatically.
  if ((f.POR_ACTION.value == 'applyDistributions' || f.POR_ACTION.value == 'applyLineDetails' || f.POR_ACTION.value == 'applyMassUpdate' || f.POR_ACTION.value == 'copyLines' || f.POR_ACTION.value == 'deleteLines') && f.POR_ERROR_FLAG.value == 'n')
  {
//    top.setSubmitFlag();
    top.getTop().submitRequest(
      "requisition",
      "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      (top.getTop().emergencyReqFlag == 'N') ? "editReqContent" : "editEmgReqContent",
      "editReqContent",
// this is commented out because it causes line attachments to display in header
// instead of header attachments
//    "refreshPage",
      "",
      new Array(),
      "por_content");
  }
  
  // display account distributions page, if no error message while updating requisition
  if (f.POR_ACTION.value == 'updateReqBeforeAcctDist' && f.POR_ERROR_FLAG.value == 'n')
  {
//    top.setSubmitFlag();
    openModalWindow("", "accountDistributions");
    submitRequest(
    "requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "displayDistributions",
    "showPage",
    FIELD_DELIMITER+ "lineNum" + VALUE_DELIMITER + top.selectedLine,
    new Array(),
    "accountDistributions");
  }

  // display line details page, if no error message while updating requisition
  if (f.POR_ACTION.value == 'updateReqBeforeLineDetails' && f.POR_ERROR_FLAG.value == 'n')
  {   
//    top.setSubmitFlag();
    top.openModalWindow("", "editLineDetails");
    top.submitRequest("requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "lineDetails",
    "showPage",
//  top.FIELD_DELIMITER + "lineNum" + top.VALUE_DELIMITER + lineNum,
    top.FIELD_DELIMITER + "lineNum" + top.VALUE_DELIMITER + top.selectedLine,
    new Array(),
    "editLineDetails");
  }

  // show approver list, if no error message
  if (f.POR_ACTION.value == 'buildApprovalList' && f.POR_ERROR_FLAG.value == 'n')
  {   
//      top.setSubmitFlag();
	  top.openModalWindow("", "editApprovalList")
	  top.submitRequest(
	  "requisition",
   	  "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
	  "editApprovalList",
	  "editApprovalList",
	  "",
	  new Array(), 
	  "editApprovalList");
  }

  // refresh approver list, if no error message after performing approver list actions
  if ((f.POR_ACTION.value == 'addApprover' || f.POR_ACTION.value == 'changeFirstApprover' || f.POR_ACTION.value == 'resetApprovalList' || f.POR_ACTION.value == 'resequenceApprovers' || f.POR_ACTION.value == 'deleteApprover') && f.POR_ERROR_FLAG.value == 'n')
  {
//    top.setSubmitFlag();
    top.getTop().submitRequest(
      "requisition",
      "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "editApprovalListContent",
      "refreshPage",
      "",
      new Array(),
      "approvers_content");
  }

  // close the window if no error message while saving or cancelling approval list
  if ((f.POR_ACTION.value == 'saveApprovalList' || f.POR_ACTION.value == 'cancelApprovalList') && f.POR_ERROR_FLAG.value == 'n')
    top.close();


   if (f.POR_ACTION.value == 'displAttach' && f.POR_ERROR_FLAG.value == 'n')
     {
//        top.setSubmitFlag();
        top.getTop().openModalWindow("", "addAttachment");
        top.getTop().submitRequest("requisition",
        "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AttachOrder",
        "displAttachment",
        "displAttachment",
        top.getTop().FIELD_DELIMITER + "attachedDocId" +
        top.getTop().VALUE_DELIMITER +
        top.attachedDocId +
        top.getTop().FIELD_DELIMITER + "entityName" +
        top.getTop().VALUE_DELIMITER +
        "REQ_LINES" +
         top.getTop().FIELD_DELIMITER + "functionName" +
         top.getTop().VALUE_DELIMITER +
         "ICX_REQS" +
         top.getTop().FIELD_DELIMITER + "pk1value" +
         top.getTop().VALUE_DELIMITER +
         top.req_line_id +
         top.getTop().FIELD_DELIMITER + "lineNum" +
         top.getTop().VALUE_DELIMITER +
         top.line_num,
         new Array(),
        "addAttachment");
     } 

   if (f.POR_ACTION.value == 'displAttachHeader' && f.POR_ERROR_FLAG.value == 'n')
     {
//        top.setSubmitFlag();
        top.getTop().openModalWindow("", "addAttachment");
        top.getTop().submitRequest("requisition",
        "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AttachOrder",
        "displAttachment",
        "displAttachment",
        top.getTop().FIELD_DELIMITER + "attachedDocId" +
        top.getTop().VALUE_DELIMITER +
        top.getTop().attachedDocId +
        top.getTop().FIELD_DELIMITER + "entityName" +
        top.getTop().VALUE_DELIMITER +
        "REQ_HEADERS" +
         top.getTop().FIELD_DELIMITER + "functionName" +
         top.getTop().VALUE_DELIMITER +
         "ICX_REQS" +
         top.getTop().FIELD_DELIMITER + "pk1value" +
         top.getTop().VALUE_DELIMITER +
         top.getTop().req_header_id +
         top.getTop().FIELD_DELIMITER + "lineNum" +
         top.getTop().VALUE_DELIMITER +
         top.getTop().line_num,
         new Array(),
        "addAttachment");
     }

  var attchAction = f.POR_ACTION.value;
    
  if ((attchAction == 'headerAttachment'  || attchAction == 'insertAttachment' || attchAction == 'headerUpdate' || attchAction == 'updateAttachment') && f.POR_ERROR_FLAG.value == 'n')
     {
	var entity;
        var linenum;
        var target;
      if (attchAction == 'headerAttachment' || attchAction == 'headerUpdate')  {
                           entity = 'REQ_HEADERS';
			   linenum = -1;
			   target = 'por_content';
			   attchAction = (top.getTop().emergencyReqFlag == 'N') ? "nextAttachHeader" : "nextAttachEmgHeader";
			}
    else if (attchAction == 'insertAttachment' || attchAction == 'updateAttachment') {
                           entity = 'REQ_LINES';
			   linenum = top.opener.top.line_num;
			   target = 'req_line_details_content';
			   attchAction = (top.getTop().emergencyReqFlag == 'N') ? "nextAttachLines" : "nextAttachEmgLines";
			}

        top.close();


//    top.setSubmitFlag();    
	top.getTop().submitRequest("requisition",
                      "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AttachOrder",
                       attchAction,
                      "default",
                      top.getTop().FIELD_DELIMITER + "lineNum" +
                      top.getTop().VALUE_DELIMITER +
                      linenum +
                      top.getTop().FIELD_DELIMITER + "entityName" +
                      top.getTop().VALUE_DELIMITER +
                      entity,
                      new Array(),
                      target);
     }

if ((f.POR_ACTION.value == 'confirmReceiptSearch') && f.POR_ERROR_FLAG.value == 'n')
  {
        top.getTop().submitRequest("requisition",
          "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt",
          "confirmReceiptSearchSimple",
          "confirmReceiptSearchSimple",
       top.getTop().FIELD_DELIMITER + "queryStart" +top.getTop().VALUE_DELIMITER+top.crQueryStart+
        top.getTop().FIELD_DELIMITER + "queryEnd" +top.getTop().VALUE_DELIMITER+top.crQueryEnd+ 
        top.getTop().FIELD_DELIMITER + "queryMode" +top.getTop().VALUE_DELIMITER+top.queryMode,
          new Array("POR_CR_SEARCH"),
          "content_results",
	  "GET");
  }

   if (f.POR_ACTION.value == 'receipt_from_ro' && f.POR_ERROR_FLAG.value == 'n') {
    top.getTop().submitRequest(
     "requisition",
     "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt",
     "confirmReceiptSearchSimple",
     "confirmReceiptSearchSimple",
     top.getTop().FIELD_DELIMITER + "queryStart" +top.getTop().VALUE_DELIMITER +
     top.crQueryStart +
     top.getTop().FIELD_DELIMITER + "queryEnd" +top.getTop().VALUE_DELIMITER +
     top.crQueryEnd +
top.getTop().FIELD_DELIMITER + "queryMode" +top.getTop().VALUE_DELIMITER+top.queryMode,
     new Array("POR_CR_SEARCH"),
     "content_results",
	"GET");
   }
  else if (f.POR_ACTION.value == 'receipt_from_wf' && f.POR_ERROR_FLAG.value == 'n') 
 {
    top.getTop().submitRequest(
     "requisition",
     "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt",
     'rcvFromNotification',
     'rcvFromNotification',
     top.getTop().FIELD_DELIMITER + "queryStart" +top.getTop().VALUE_DELIMITER +
     top.crQueryStart +
     top.getTop().FIELD_DELIMITER + "queryEnd" +top.getTop().VALUE_DELIMITER +
     top.crQueryEnd +
     top.getTop().FIELD_DELIMITER + "requesterId" +
     top.getTop().VALUE_DELIMITER +
     '24' +
     top.getTop().FIELD_DELIMITER + "poHeaderId" +
     top.getTop().VALUE_DELIMITER +
     top.ro_po_header_id +
     top.getTop().FIELD_DELIMITER + "destOrgId" +
     top.getTop().VALUE_DELIMITER +
     top.ro_dest_org_id +
     top.getTop().FIELD_DELIMITER + "dueDate" +
     top.getTop().VALUE_DELIMITER +
     top.ro_exp_receipt_date, 
     new Array(),
     "rcv_content_results");
  }
 else if (f.POR_ACTION.value == 'receipt_from_vreqs' && f.POR_ERROR_FLAG.value == 'n')
 {
    top.getTop().submitRequest(
     "requisition",
     "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt",
     'rcvFromVReqs',
     'rcvFromViewReqs',
     top.getTop().FIELD_DELIMITER + "queryStart" +top.getTop().VALUE_DELIMITER +
     top.crQueryStart +
     top.getTop().FIELD_DELIMITER + "queryEnd" +top.getTop().VALUE_DELIMITER +
     top.crQueryEnd +
     top.getTop().FIELD_DELIMITER + "reqHeaderId" +
     top.getTop().VALUE_DELIMITER +
     top.ro_req_header_id,
     new Array(),
     "rcv_content_results");
  }
 else if (f.POR_ACTION.value == 'isReceivable' && f.POR_ERROR_FLAG.value == 'n')
 {

    if (top.currentPage == "VIEW_REQ_DETAILS")
     hiddenFrameName = "req_line_details_hidden";
   else
     hiddenFrameName = "por_hidden";

   myWin = top.openModalWindow("","receive_content_results");

/*
   top.getTop().submitRequest(
      "requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt", // url
    "rcvFromViewReqs",                              // action
    "rcvFromViewReqs",                              // function
    "",                                             // general Data
    new Array(),                                    // formNames
    'receive_content_results')                      // target
    "GET";
*/

/* using mywin.location so that the URL is preserved when we want to resize
   see comment in PORCRFVR.xml */
   
    myWin.location=
      ("/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt?REQ_TOKEN="
       + top.getTop().getReqToken("rcvFromViewReqs", "rcvFromViewReqs", "requisition"));
  }


  if (f.POR_ACTION.value == 'addFavListItem' && f.POR_ERROR_FLAG.value == 'n')
   {
    var favContentFrame = top.getTop().findFrame(top.getTop(),"content_results");
      favContentFrame.clearCheckboxes();
   }

  // display mass update page, if no error message while updating requisition
  if (f.POR_ACTION.value == 'updateReqBeforeMassUpdate' && f.POR_ERROR_FLAG.value == 'n')
  {
    openModalWindow("", "massUpdate");
    submitRequest(
      "requisition",
      "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "updateMultipleLines",
      "showPage",
      "",
      new Array(),
      "massUpdate");
  }

  if ((f.POR_ACTION.value == 'viewReqsSearch') && f.POR_ERROR_FLAG.value == 'n')
  {
        top.getTop().submitRequest("requisition",
   	  "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ReqsQueryOrder",
   	  "viewReqsSearchByStatus",
	  "viewReqsSearchByStatus",
    	  top.getTop().FIELD_DELIMITER + "queryStart" +top.getTop().VALUE_DELIMITER+top.queryStart+
          top.getTop().FIELD_DELIMITER + "queryEnd" +top.getTop().VALUE_DELIMITER+top.queryEnd+
          top.getTop().FIELD_DELIMITER + "queryMode" +top.getTop().VALUE_DELIMITER+top.queryMode,
   	  new Array("POR_REQ_SEARCH"), 
   	  "content_results",
          "GET");
  }

  // refresh view reqs page, if no error message after deleting requisition
  if ((f.POR_ACTION.value == 'deleteRequisition') && f.POR_ERROR_FLAG.value == 'n')
  {
    // top.setSubmitFlag();
    top.getTop().findFrame(top.getTop(), "content_top").reSubmit();
  }

  // refresh view reqs page, if no error message after cancelling requisition
  if ((f.POR_ACTION.value == 'cancelRequisition') && f.POR_ERROR_FLAG.value == 'n')
  {
    // top.setSubmitFlag();
    top.getTop().findFrame(top.getTop(), "content_top").reSubmit();
  }


  // Copy requisition
  if ((f.POR_ACTION.value == 'copyRequisition') && f.POR_ERROR_FLAG.value == 'n')
  {
    top.getTop().reqSessionId = f.POR_REQ_SESSION_ID.value;

    top.getTop().submitRequest("requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order",
    "editReqCopy",
    "showPage",
    "",
    new Array(),
    "_top");

    if (top.IS_TOP != true)
      top.close();   
  }  

  // Complete requisition
  if ((f.POR_ACTION.value == 'completeRequisition') && f.POR_ERROR_FLAG.value == 'n')
  {
    top.getTop().reqSessionId = f.POR_REQ_SESSION_ID.value;

    top.getTop().submitRequest("requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order",
    "editReqComplete",
    "showPage",
    "",
    new Array(),
    "_top");

    if (top.IS_TOP != true)
      top.close();

  }

  // Resubmit requisition
  if ((f.POR_ACTION.value == 'resubmitRequisition') && f.POR_ERROR_FLAG.value == 'n')
  {
    top.getTop().reqSessionId = f.POR_REQ_SESSION_ID.value;

    top.getTop().submitRequest("requisition",
    "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order",
    "editReqResubmit",
    "showPage",
    "",
    new Array(),
    "_top");

    if (top.IS_TOP != true)
      top.close();
  }


  if ((f.POR_ACTION.value == 'saveReqCancelDialog' || 
       f.POR_ACTION.value == 'discardReq' ||
       f.POR_ACTION.value == 'saveReqNotifications') 
      && f.POR_ERROR_FLAG.value == 'n')
  {
    top.close();
  }
  
  if ((f.POR_ACTION.value == 'favListContent'||
       f.POR_ACTION.value == 'addSpecialItem')
      && f.POR_ERROR_FLAG.value == 'n')
  {
    resetForms("POR_SPECIAL_ORDER_RATE_R");
    resetForms("POR_SPECIAL_ORDER_GOODS_R");
    resetForms("POR_SPECIAL_ORDER_AMOUNT_R");

    // Bug 991579
    // Need to re-initialize the dynamic elements line_type_code
    // value because after submission (adding to req or adding
    // to favorites) initNonCatalogItems() is not called.
    allForms["POR_SPECIAL_ORDER_RATE_R"][0].POR_LINE_TYPE_CODE.value="rate";
    allForms["POR_SPECIAL_ORDER_GOODS_R"][0].POR_LINE_TYPE_CODE.value="goods";
    allForms["POR_SPECIAL_ORDER_AMOUNT_R"][0].POR_LINE_TYPE_CODE.value="amount";
  }
  
  if (f.POR_ACTION.value == 'checkAddApprover' &&
      f.POR_ERROR_FLAG.value == 'n')
  {
    top.getTop().findFrame(top, "approvers_bottom").addApproverLOV();
  }

  if (f.POR_ACTION.value == 'checkChangeFirstApprover' &&
      f.POR_ERROR_FLAG.value == 'n')
  {
    top.getTop().findFrame(top, "approvers_bottom").changeFirstApproverLOV();
  }

}



// Event handler for when a window gains focus or has been clicked
// on.  This function checks to see if there's a modal window open
// and, if so, gives focus to the modal window.  Returns true iff
// there's no modal window open.
function clickOrFocus(e)
{
  if (mWin&& !mWin.closed)
  { 
    if(IS_IE && !window.top.objectsDisabled)
    {
      disableWindow(top);
      window.top.objectsDisabled=true;
    }
    // race condition problem exists when user closes modal window.
    // mWin may get closed after we check for mWin.closed (above).
    // The only remedy (not cure) we can come up with is to check for
    // mWin.closed again right before giving focus.

    if(mWin && !mWin.closed)
      mWin.focus();
    return false;
  }
  else
  {
    if (IS_NAV) 
    {
      return routeEvent(e);
    }
    else if (window.top.objectsDisabled)
    {
      enableWindow(top);
      window.top.objectsDisabled=false;
      return true;
    }
  }
}

// disables all objects in a window and its subwindow (recursively).
// This routine is necessary only for IE, due to its bubble up event
// handling mechanise.
function disableWindow(win) 
{
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)
    {
      var elem=cForm.elements[e];
      elem.oldOnfocus= elem.onfocus;
      elem.oldOnclick=elem.onclick;
      elem.onclick= clickOrFocus;
      elem.onfocus= clickOrFocus;
    }
  }
  for (l=0; l<win.document.links.length; l++) 
  {
    var cLink= win.document.links[l];
    cLink.oldOnclick= cLink.onclick;
    cLink.onclick=clickOrFocus;
    cLink.oldOnfocus= cLink.onfocus;
    cLink.onfocus=clickOrFocus;
  }
  win.onclick=clickOrFocus;
  win.onfocus=clickOrFocus;
  win.document.onfocus=clickOrFocus;
  win.document.onclick=clickOrFocus;
  for(var w=0; w<win.frames.length; w++)
    disableWindow(win.frames[w]);
}

// restore IE form elements and links to normal behavior
function enableWindow(win) 
{
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)
    {
      var elem=cForm.elements[e];
      if (elem.oldOnfocus) 
	    elem.onfocus= elem.oldOnfocus;
      if (elem.oldOnclick) 
        elem.onclick=elem.oldOnclick;
    }
  }
  for (l=0; l<win.document.links.length; l++) 
  {
    var cLink= win.document.links[l];
    if (cLink.oldOnclick)
      cLink.onclick=cLink.oldOnclick;

  }
  for(var w=0; w<win.frames.length; w++)
    enableWindow(win.frames[w]);
}


window.onClick= clickOrFocus;
window.onFocus= clickOrFocus;
window.onclick= clickOrFocus;
window.onfocus= clickOrFocus;

// opens a modal window  using attribute list as provided, or
// defaulted otherwise.  windowWidth and windowHeight are taken into
// consideration only if attributeList is not provided
function openModalWindow(url,   // url for the new window
                         name,  // name of the new window
                         attributeList, // attribute list for the new
                                        // window- by default, status=yes (all
                                        // else no) 
                         windowWidth,   // width for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 800 if null
                         windowHeight)  // height for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 600 if null
{
	var maxWindow = false;

	if(attributeList==null || attributeList=="") {
	    	var windowWidth= (windowWidth) ? windowWidth : 800;
    		var windowHeight= (windowHeight) ? windowHeight : 600;
		var attributeList = "";

		if (IS_IE) {
			if (screen.availWidth < 800 || screen.availHeight < 600) {
        	        	maxWindow = true;
				windowWidth = screen.availWidth;         
				windowHeight = screen.availHeight;
				attributeList=
                                        "menubar=no,location=no,toolbar=no,width="+windowWidth+
                                        ",height="+windowHeight+
                                        ",resizable=yes,scrollbars=yes,status=yes";
			} else {
                		attributeList=
                		"menubar=no,location=no,toolbar=no,width="+ windowWidth+
                		",height="+windowHeight+
                		",resizable=yes,scrollbars=yes,status=yes";
        		}
		} else {
			attributeList= 
				"menubar=no,location=no,toolbar=no,width="+ windowWidth+ 
				",height="+windowHeight+
				",resizable=yes,scrollbars=yes,status=yes";
		}
		if(IS_NAV) {
	      		var winLeft = window.screenX+((window.outerWidth -
                                       windowWidth) / 2);
		      	var winTop = window.screenY + ((window.outerHeight -
                                       windowHeight) / 2);
		      	attributeList +=",screenX=" +winLeft + ",screenY=" + winTop;
		}
		else {
		      	var winLeft = (screen.width - windowWidth) / 2;
		      	var winTop = (screen.height - windowHeight) / 2;
			if (maxWindow) {
				attributeList += ",left=0,top=0";
			} else {	
		      		attributeList += ",left=" + winLeft + ",top=" + winTop;
			}
    		}
  	}
  if(modalOpen())
  {
	mWin.openModalWindow(url, name, attributeList);
	if (IS_IE && maxWindow) {
		mWin.resizeTo(windowWidth, windowHeight);
	}
	return mWin;
  }
  else
  { 
    top.mWin= open(url, name, attributeList);

        if(IS_IE)
        {
		if (maxWindow)
			if (mWin)
				mWin.resizeTo(windowWidth, windowHeight);
			else
				top.mWin.resizeTo(windowWidth,windowHeight);
		disableWindow(top);
		window.top.objectsDisabled=true;
        }
    return top.mWin;
  }
}

function getTop()
{
  if(top.IS_TOP)
  {
    return top;
  }
  else
  {
    return opener.top.getTop();
  }
}

// returns true if this window has a modal window open
function modalOpen()
{
  return(mWin && !mWin.closed);
}

//  this function returns the upperMost modal window
function getActiveWindow()
{
  if(modalOpen())
  {
    return (mWin.getActiveWindow) ? mWin.getActiveWindow() : mWin;
  }
  else 
  {
    return window;
  }
}

// this function displays a message in the status bar
function showStatus(msg)
{
  window.status = msg;
  return true;
}

// closes all modal window in order, starting from top most window
function recursiveClose()
{
  window.onClick= null;
  window.onFocus= null;
  window.onclick= null;
  window.onfocus= null;
  var tempWin= mWin;
  mWin=null;
  
  if(tempWin && !tempWin.closed && tempWin.recursiveClose)
  {
    tempWin.recursiveClose();
  }
  else if(tempWin && !tempWin.closed)
  {
    tempWin.close();
  }
  //  alert("mWin="+mWin+"mWin.closed="+ (mWin ? mWin.closed : "n/a"));
  window.close();
}

function printWindow(frameName)
{
  if (checkSubmitFlag())
    return;

  if(top.getTop().IS_IE)
  {
   alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_CANNOT_PRINT"]);
   return;
  }

  // Nav
  var content=top.getTop().findFrame(top, frameName);
  if(content && content.frames.length==0)
  {
    content.print();
    return;
  }
   alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_CANNOT_PRINT"]);
}

