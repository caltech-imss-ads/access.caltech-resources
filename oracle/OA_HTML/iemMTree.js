/*
<!-- 
 +============================================================================+
 |      Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA        |
 |                         All rights reserved.                               |
 +============================================================================+
 |  FILENAME:  iemMTree.js                                                    |
 +============================================================================+	
 |  DESCRIPTION:                                                              |
 |    iemMTree.js is a javascript that draws a graphical tree, Mona Lisa style|
 |	Called by: iemAddFd.jsp,iemRemFd.jsp,iemTree.jsp                        | 
 +============================================================================+
 |  HISTORY                                                                   |
 |  [01/03/01, ukari] created, from iemTree.js for retaining Mona Lisa style  |
 |  [02/02/01. ukari] prevent user from deleting Drafts folder                |
 |  [02/05/01, Sunny Yang] use getURL for all links                           |
 +============================================================================+
-->
<!-- $Header: iemMTree.js 115.2 2001/02/15 19:44:49 pkm ship     $ -->
*/

/**
 * iemTree.js is a javascript that draws a graphical tree.
 * The tree can contain one or more nodes that can be expanded and 
 * de-expanded by clicking the "expand" plus/minus icon to the left of each node.
 * See iemTreBd.js for an example of Building a Tree recursively.
 *
 * @author      Bob Greenfield
 * @version     1.0
 */

// CONSTANTS----------------------------------

//IMAGE FILES USED TO DISPLAY TREE PARTS--
IMGSRC_HEADERLEFT = "/OA_MEDIA/iemHdrLt.gif"
IMGSRC_HEADERRIGHT = "/OA_MEDIA/iemHdrRt.gif"
IMGSRC_FOLDEROPEN="/OA_MEDIA/iemFldOp.gif"
IMGSRC_FOLDERCLOSED="/OA_MEDIA/iemFldCl.gif" 
IMGSRC_EXPANDMINUSLAST="/OA_MEDIA/iemExMnL.gif"  
IMGSRC_EXPANDMINUS="/OA_MEDIA/iemExMn.gif"
IMGSRC_EXPANDPLUSLAST="/OA_MEDIA/iemExPsL.gif"
IMGSRC_EXPANDPLUS="/OA_MEDIA/iemExPs.gif"
IMGSRC_BLANK="/OA_MEDIA/iemBlank.gif"
IMGSRC_LINEVERT ="/OA_MEDIA/iemLnVer.gif"
IMGSRC_NODEEND ="/OA_MEDIA/iemNdEnd.gif"
IMGSRC_NODE ="/OA_MEDIA/iemNd.gif"

//BROWSER TYPES--
BROWSER_IE=1
BROWSER_NS=2 

//GLOBAL VARS---------------------------------
gStartBG = "#FFFFFF"
curFoldId = -1
curFoldLabel = "none"
curFoldUrl = "none"
curFoldAcct = "none"
gheader_text = "" 
gmethod_text = ""
gadd_text = ""
grem_text = ""
totalfolders = 0 
browserType = 0 
folderList = new Array
foldStateAr = new Array
newFA = new Array
foldStates = ""
jspURL = "";

/** 
 * DatabaseData object constructor
 * Contains Messages retrieved from the database.
 *
 * @return          A new <CODE>DatabaseData<CODE> Object
 */
function DatabaseData()
{
  this.setMsg = ddSetMsg
  this.getMsg = ddGetMsg
  this.msgs = new Array
}

/** 
 * Sets a value of one of the messages stored in DatabaseData
 * 
 * @param msgNum    int The message number
 * @param msgText	String The message text to store
 * @return          Void
 */
function ddSetMsg(msgNum,msgText)
{
  this.msgs[msgNum] = msgText
}

/** 
 * Gets a value of one of the messages stored in DatabaseData
 * 
 * @param msgNum    int The message number
 * @return          The Message Text
 */
function ddGetMsg(msgNum)
{
  return(this.msgs[msgNum])
}

/** 
 * Folder object constructor
 * 
 * @param pLabel    String Label of the folder that will be created
 * @param pLink		String href that the Folder loads when clicked on.
 * @param pFrame    Target Frame for the Link
 * @param pUrlName	URL for the folder
 * @param pAcct     Account for this folder
 * @param pCount    Number of messages in this folder
 * @return          A new <CODE>Folder<CODE>Object
 */
function Folder(pLabel,pLink,pFrame,pUrlName,pAcct,pCount) 
{
  //VARIABLES
  this.totalchildren = 0
  this.children = new Array
  this.isOpened = true 
 
  //CONSTANTS
  this.urlname = pUrlName
  this.acct = pAcct
  this.count = pCount
  this.label = pLabel 
  this.link = pLink
  this.frame = pFrame
  this.imgDispSrc = IMGSRC_FOLDEROPEN
 
  this.id = -1   
  this.mylayer = 0  
  this.imgDisp = 0  
  this.imgExpand = 0  
  this.isEnd = 0 
 
  //GET METHODS
  this.getHeight = foldGetHeight
 
  //SET METHODS
  this.setStatus = foldSetStatus
  this.setVisible = foldSetVisible
  this.setHidden = foldSetHidden 
  this.addNode = foldAddNode
  
  //DRAW METHODS
  this.show = foldShow
  this.init = foldInit 
  this.makeIndex = foldMakeIndex 
  this.countChildFolds = foldCountChildFolds
  this.writeLink = foldWriteLink 
}  

/** 
 * Keeps track of folders when one is clicked on.  
 * Then calls <CODE>setTreeImgs</CODE> to redraw folders as necessary.
 * 
 * @param isOpened    Boolean that is <CODE>true</CODE> if folder is opened
 * @see               setTreeImgs
 * @return            VOID
 */ 
function foldSetStatus(isOpened) 
{
  var foldersBelow 
  var heightCount
  
  if (isOpened == this.isOpened) 
    return 
 
  if (browserType == BROWSER_NS)  
  {
    heightCount = 0 
    for (var c=0; c < this.totalchildren; c++) 
    {
      heightCount = heightCount + this.children[c].mylayer.clip.height
    }
    foldersBelow = this.countChildFolds() 
  
    if (this.isOpened) 
    {
      heightCount = 0 - heightCount
    } 
    for (var i = this.id + foldersBelow + 1; i < totalfolders; i++) 
    {
      folderList[i].mylayer.moveBy(0, heightCount)
    } 
  }  
  this.isOpened = isOpened 
  setTreeImgs(this) 
} 

/** 
 * Redraw folders and all children with open or closed folder images 
 * and the appropriate expansion icon (plus or minus sign).
 * 
 * @param topNode     <CODE>Folder</CODE> to draw
 * @return            VOID
 */  
function setTreeImgs(topNode) 
{   
  var i=0 
  if (topNode.totalchildren==0)  //IF THIS FOLDER HAS NO SUBFOLDERS:
  {
    if (curFoldId==topNode.id) //IF THIS FOLDER IS CURRENTLY SELECTED
      topNode.isOpened=1 //SHOW OPEN FOLDER ICON
    else
      topNode.isOpened=0 //SHOW CLOSED FOLDER ICON
  }
 
  if (topNode.isOpened) //IF NODE IS CURRENLY EXPANDED...
  { 
    if (topNode.imgExpand) 
    {
      if (topNode.isEnd) //IF LAST CHILD...
      {
        if (topNode.totalchildren==0) //IF THIS FOLDER HAS NO SUBFOLDERS
          topNode.imgExpand.src = IMGSRC_NODEEND //SHOW W/ NO EXPANSION ICON
        else
          topNode.imgExpand.src = IMGSRC_EXPANDMINUSLAST //SHOW W/ MINUS (LAST) ICON
      }
      else //NOT LAST CHILD...
      {
        if (topNode.totalchildren==0) //IF THIS FOLDER HAS NO SUBFOLDERS        
          topNode.imgExpand.src = IMGSRC_NODE //SHOW W/ NO EXPANSION ICON
        else
          topNode.imgExpand.src = IMGSRC_EXPANDMINUS //SHOW W/ MINUS ICON
      }
    }
    topNode.imgDisp.src = IMGSRC_FOLDEROPEN 
    for (i=0; i<topNode.totalchildren; i++) 
    {
      topNode.children[i].setVisible() 
    }
  } 
  else //NODE IS NOT CURRENLY EXPANDED...
  { 
    if (topNode.imgExpand) //MAKE SURE IMAGE OBJECT EXISTS
    {
      if (topNode.isEnd) //IF LAST CHILD...
      {
        if (topNode.totalchildren==0) //IF THIS FOLDER HAS NO SUBFOLDERS
          topNode.imgExpand.src = IMGSRC_NODEEND //SHOW W/ NO EXPANSION ICON
        else
          topNode.imgExpand.src = IMGSRC_EXPANDPLUSLAST //SHOW W/ PLUS (LAST) ICON
      }
      else //NOT LAST CHILD...
      {
        if (topNode.totalchildren==0) //IF THIS FOLDER HAS NO SUBFOLDERS
          topNode.imgExpand.src = IMGSRC_NODE //SHOW W/ NO EXPANSION ICON
        else
          topNode.imgExpand.src = IMGSRC_EXPANDPLUS //SHOW W/ PLUS ICON
      }
    }
    topNode.imgDisp.src = IMGSRC_FOLDERCLOSED 
    for (i=0; i<topNode.totalchildren; i++) 
      topNode.children[i].setHidden() 
  }  
} 

/** 
 * Initialize a folder and draw it onscreen.
 * 
 * @param branchNum     # Branch below parent
 * @param endChild      Boolean that is true if this node is the last child
 * @param textBeforeIcon      HTML String to be writen before node icon
 * @see               	foldShow
 * @return              VOID
 */  
function foldInit(branchNum, endChild, textBeforeIcon) 
{
  var nc       
  nc = this.totalchildren 
  this.makeIndex() 
  var auxEv = "" 
  if (browserType > 0) 
    auxEv = "<a href='javascript:expand("+this.id+")'>" 
  else 
    auxEv = "<a>" 
  if (branchNum > 0)
  { 
    if (endChild)
    { 
      this.show(textBeforeIcon + auxEv + "<img alt='' name='nodeIcon" + this.id + "' src='" + IMGSRC_EXPANDMINUSLAST +"' width=16 height=22 border=0></a>") 
      textBeforeIcon = textBeforeIcon + "<img alt='' src='" + IMGSRC_BLANK + "' width=16 height=22>"  
      this.isEnd = 1 
    } 
    else 
    { 
      this.show(textBeforeIcon + auxEv + "<img alt='' name='nodeIcon" + this.id + "' src='" + IMGSRC_EXPANDMINUS + "' width=16 height=22 border=0></a>") 
      textBeforeIcon = textBeforeIcon + "<img alt='' src='" + IMGSRC_LINEVERT + "' width=16 height=22>" 
      this.isEnd = 0 
    } 
  }
  else 
    this.show("") 

  if (nc > 0) 
  {
    branchNum++ 
    for (var i=0 ; i < this.totalchildren; i++)  
    { 
      if (i == this.totalchildren-1) 
        this.children[i].init(branchNum, 1, textBeforeIcon) 
      else 
        this.children[i].init(branchNum, 0, textBeforeIcon) 
    } 
  } 
} 

/** 
 * Called when "Add" button is clicked.
 * Allow user to type in name of new folder, and calls jsp to generate new folder on server.
 * 
 * @param parentUrl         <URLNAME> tag from Folder you are adding below
 * @param parentAcct        <ACCT> tag from Folder you are adding below
 * @param parentFolderLabel Name of Folder you are adding below
 */  
function popupNewFoldName(parentUrl, parentAcct, parentFolderLabel)
{
  var newFoldName = "";

  if (curFoldId == -1) 
  {
    alert(dbData.getMsg(0) + '  ' + dbData.getMsg(1))
    return;
  }
  var dupName = true
  while (dupName==true)
  {
    newFoldName = prompt(dbData.getMsg(2) + " '" + parentFolderLabel + "'.");

    //Check if newFoldName is same as any of the Folder's children...
    var Oldfolder = folderList[curFoldId]
    dupName = false
    for(var i=0;i<Oldfolder.children.length;i++)
    {
      if (Oldfolder.children[i].label==newFoldName)
        dupName = true
    }

    if ((newFoldName) && (newFoldName != "") && (newFoldName != "undefined"))
    {
      if (dupName)
        alert(dbData.getMsg(5))
      else
	{
        document.writeln("<form name='addform' method='post' action='" + jspURL + "'>");
        document.writeln("<%=servletSessionManager.getSessionInforAsHiddenParam()%>");
  	  document.writeln("<input type='hidden' name='urlname' value='" + parentUrl + "'>");
 	  document.writeln("<input type='hidden' name='acct' value='" + parentAcct + "'>");
        document.writeln("<input type='hidden' name='foldname' value='" + newFoldName + "'>");
        document.writeln("<input type='hidden' name='method' value='" + "add" + "'>");
        document.writeln("<input type='hidden' name='foldStates' value='" + foldStates + "'>");
        document.writeln("</form>");
        document.writeln("<SCRIPT>");
        document.writeln("document.addform.submit();");
        document.writeln("</SCRIPT>");
      }
    }
  } //End = While
}


/** 
 * Called when "Remove" button is clicked.
 * Allow user to confirm removal of folder, and calls jsp to remove the folder on the server.
 * 
 * @param pFolderUrl     		 <URLNAME> tag from Folder you are removing
 * @param pFolderAcct			 <ACCT> tag from Folder you are removing
 * @param pFolderLabel      		 Name of Folder you are removing
 * @return              			 VOID
 */  
function popupRemoveFolder(pFolderUrl, pFolderAcct, pFolderLabel)
{
  var bSure = false;

  if (curFoldId==-1) 
  {
    alert(dbData.getMsg(0) + "  " + dbData.getMsg(3));
    return;
  }

  bSure = confirm(dbData.getMsg(4) + " '" + pFolderLabel + "'?");
  if (bSure)
  {
    // REMOVE THIS FOLDER FROM STATE ARRAY
    var c
    for (c=0;c<curFoldId;c++)
      newFA[c] = foldStateAr[c]
    
    for (var j=c;j<totalfolders-1;j++)
      newFA[j] = foldStateAr[j+1]
    
    //UPDATE STATE ARRAY STRING
    foldStates = ""
    for (var i=0;i<=newFA.length-1;i++)
      foldStates = foldStates + newFA[i]

    document.writeln("<form name='addform' method='post' action='" + jspURL + "'>");
    document.writeln("<%=servletSessionManager.getSessionInforAsHiddenParam()%>");
    document.writeln("<input type='hidden' name='urlname' value='" + pFolderUrl + "'>");
    document.writeln("<input type='hidden' name='acct' value='" + pFolderAcct + "'>");
    document.writeln("<input type='hidden' name='foldname' value='" + pFolderLabel + "'>");
    document.writeln("<input type='hidden' name='method' value='" + "remove" + "'>");
    document.writeln("<input type='hidden' name='foldStates' value='" + foldStates + "'>");
    document.writeln("</form>");
    document.writeln("<SCRIPT>");
    document.writeln("document.addform.submit();");
    document.writeln("</SCRIPT>");

  }
}

/** 
 * Draw one folder onscreen.
 * Also draws Tree header-banner if this is the Top level Node.
 * Calls <CODE>writeLink</CODE> to write link information if the Folder has a link
 *
 * @param textBeforeIcon HTML String to be writen before node icon
 * @see                  writeLink
 */   
function foldShow(textBeforeIcon) 
{
  if (browserType == BROWSER_NS) 
  { 
    if (!document.yPos) 
      document.yPos=8 

    document.write("<layer id='folder" + this.id + "' top=" + document.yPos + " visibility=hidden>") 
  }

  // SHOW HEADER-BANNER ABOVE PARENT TREE IF NECESSARY
  if (this.id == 0  && gheader_text != "none")
  {
    document.write(
      '<table summary="" border=0 cellpadding=0 cellspacing=0 width=100% hspace=0 vspace=0>' +
        '<tr>' +
	'<td align=right class=tableBigHeaderCell><img alt="" border=0 src=' + IMGSRC_HEADERLEFT + ' width=7></td>' +
        '<td align=middle class=tableBigHeaderCell width="100%"><font color=#FFFFFF><b>' + gheader_text + '</b></font></td>' +
        '<td width=100% class=tableBigHeaderCell><img alt="" border=0 hspace=0 src=' + IMGSRC_HEADERRIGHT + '></td>' +
        '</tr>' +
      '</table>');

    //SHOW ADD AND DELETE METHODS BANNER IF NECESSARY
    if (gmethod_text != "none")
    {
      document.write(
        '<table summary="" border=0 cellpadding=0 cellspacing=0 height="15" width=100% hspace=0>' +
        '<tr>' +
        '<td align=center class=tableSubHeaderCell>' +
        '<a href="javascript:popupNewFoldName(curFoldUrl, curFoldAcct, curFoldLabel)" accesskey="a">' + gadd_text + '</a>,&nbsp' +
        '<a href="javascript:popupRemoveFolder(curFoldUrl, curFoldAcct, curFoldLabel)" accesskey="d">' + grem_text +'</a></td>' +
	'</tr>' +
      '</table>');
    }
  }

  document.write("<table summary='' ") 

  if (browserType == BROWSER_IE) 
    document.write(" id='folder" + this.id + "' style='position:block;' ") 

  document.write(" border=0 cellspacing=0 cellpadding=0>") 
  document.write("<tr><td>") 
  document.write(textBeforeIcon)

  //IF THIS FOLDER HAS A LINK
  if ((this.link != "none")) 
  {
    //WRITE FOLDER LINK TAG BEFORE FOLDER-ICON
    this.writeLink() 
  }
  
  document.write("<img alt='folder " + this.id + "' name='folderIcon" + this.id + "' ") 
  document.write("src='" + this.imgDispSrc + "' border=0>") 
  document.write(" " + this.label)

  //CLOSE THE FOLDER LINK TAG
  document.write("</A>")
  //WRITE THE MESSAGE COUNT
  if (this.count != "none")
    document.write(" (" + this.count + ")") 	 
  document.write("</td><td valign=middle nowrap>")
  document.write("</td>") 
  document.write("</table>") 
  if (browserType == BROWSER_NS) 
  { 
    document.write("</layer>") 
  } 
  if (browserType == BROWSER_IE) 
  {
    this.mylayer = document.all["folder"+this.id] 
    this.imgDisp = document.all["folderIcon"+this.id] 
    this.imgExpand = document.all["nodeIcon"+this.id] 
  } 
  else if (browserType == BROWSER_NS) 
  {
    this.mylayer = document.layers["folder"+this.id] 
    this.imgDisp = this.mylayer.document.images["folderIcon"+this.id] 
    this.imgExpand = this.mylayer.document.images["nodeIcon"+this.id] 
    document.yPos=document.yPos+this.mylayer.clip.height 
  } 
} 

/** 
 * Writes a folder's link to the html page.
 * 
 * @return              void
 */  
function foldWriteLink() 
{ 
  if (this.link) 
  {
    document.write("<A HREF='javascript:clickedFolder(" + this.id + ")'")
    document.write("CLASS='treeText'>") 
  } 
  else 
    document.write("<a>") 
} 

/** 
 * Function is used When a folder is clicked on:
 * 1) Opens the folder's link in the appropriate target frame.
 * 2) Highlights a folder when it is clicked on.
 * 3) Also updates global variables keeping track of the currently selected folder.
 * 
 * @param pId			ID of the folder that was clicked
 * @return              void
 */  
function clickedFolder(pId)
{
  if (curFoldId != -1)
  {
    if (browserType == BROWSER_IE) 
      document.all["folder"+curFoldId].style.background="transparent"
    else 
      document.layers["folder"+curFoldId].bgColor=gStartBG
    var Oldfolder = folderList[curFoldId]

    //CLOSE THE FOLDER ICON OF THE PREVIOUS SELECTED FOLDER (only needed if it has no subfolders)
    if (Oldfolder.totalchildren==0) 
    {
      Oldfolder.imgDisp.src = IMGSRC_FOLDERCLOSED
    }

    var state = Oldfolder.isOpened
    if (state) 
      foldStateAr[curFoldId] = "b"
    else
      foldStateAr[curFoldId] = "a"
   }

  curFoldUrl = folderList[pId].urlname
  curFoldAcct = folderList[pId].acct
    
  curFoldId = pId
  curFoldLabel = folderList[pId].label


  // Changed by mrabatin to support the DesktopClientApplet
  thetarget = "parent." + folderList[pId].frame + ".location.href" + "=folderList[pId].link"
  eval(thetarget)

  if (browserType == BROWSER_IE)
  {
    document.all["folder"+pId].style.background="#6699cc"
  }
  else
  {
    document.layers["folder"+curFoldId].bgColor="#6699cc"
  }
 
  //UPDATE FOLDER STATE ARRAY WITH NEWLY HIGHLIGHTED FOLDER
  if (pId != 0) // [BUG 1236508] Set to 'c' except if it's root folder 
    foldStateAr[curFoldId] = "c";
  foldStates = ""
  for (var i=0;i<=foldStateAr.length-1;i++)
    foldStates = foldStates + foldStateAr[i]

  //SET FOLDER's IMAGE TO AN OPENED FOLDER (needed for folders with no subfolders)
  if (folderList[pId].totalchildren==0)
  {
    folderList[pId].imgDisp.src = IMGSRC_FOLDEROPEN
  }
}

/** 
 * Adds a child to a folder.
 * 
 * @param childNode     the child <CODE>Folder</CODE>
 * @return              <CODE>Folder</CODE> childNode
 */   
function foldAddNode(childNode) 
{ 
  this.children[this.totalchildren] = childNode 
  this.totalchildren++ 
  return childNode 
} 

/** 
 * Counts the number of folders below this one, ignoring items.
 * 
 * @return              number of folders
 */  
function foldCountChildFolds() 
{ 
  var numfolds = this.totalchildren 
  for (var c=0; c < this.totalchildren; c++)
  { 
    if (this.children[c].children)  // IF THIS IS A FOLDER (IT CONTAINS CHILDREN)
      numfolds = numfolds + this.children[c].countChildFolds() 
  } 
  return numfolds
} 
 
/** 
 * Item object constructor
 * 
 * @param pLabel    String Label of the Item that will be created
 * @param pLink	 	String Href that the Item loads when clicked on.
 * @return          A new <CODE>Item<CODE>Object
 */
function Item(pLabel, pLink) // Constructor 
{ 
  // CONSTANTS
  this.id = -1
  this.label = pLabel 
  this.link = pLink
  this.imgDispSrc = IMGSRC_FOLDERCLOSED 
  this.imgDisp = 0
  this.mylayer = 0
  
  // GET METHODS
  this.getHeight = foldGetHeight
  //SET METHODS
  this.setHidden = itemSetHidden
  this.setVisible = foldSetVisible 
  //DRAW METHODS
  this.show = itemShow
  this.init = itemInit
  this.makeIndex = foldMakeIndex 
} 

/** 
 * Marks this folder as hidden (calls nodeSetHidden) and updates display. 
 * 
 * @see 			  nodeSetHidden
 * @return            VOID
 */  
function foldSetHidden() 
{ 
  nodeSetHidden(this) // SAME PARAMETERS NEED TO BE SET FOR FOLDERS AND ITEMS    
  this.setStatus(0)   //UPDATE DISPLAY NOW THAT FOLDER IS HIDDEN
} 

/** 
 * Marks this item as hidden (calls nodeSetHidden). 
 * 
 * @see nodeSetHidden
 * @return            VOID
 */ 
function itemSetHidden()
{
  nodeSetHidden(this)
}

/** 
 * Marks this node as hidden (sets appropriate display parameter). 
 * 
 * @return            VOID
 */   
function nodeSetHidden(pNode) 
{
  if (browserType == BROWSER_IE) 
  {
    if (pNode.mylayer.style.display == "none") 
      return 
    pNode.mylayer.style.display = "none" 
  } 
  else 
  {
    if (pNode.mylayer.visibility == "hidden") 
      return 
    pNode.mylayer.visibility = "hidden"
  }     
} 

/** 
 * Initialize a item and draw it onscreen.
 * 
 * @param branchNum     	# Branch below parent
 * @param endChild      	Boolean that is true if this node is the last child
 * @param textBeforeIcon    HTML String to be writen before node icon
 * @see               		itemShow
 * @return              	VOID
 */  
function itemInit(branchNum, endChild, textBeforeIcon) 
{
  this.makeIndex()  
  if (branchNum > 0)
  { 
    if (endChild)
    {
      this.show(textBeforeIcon + "<img alt='' src='" + IMGSRC_NODEEND  +"' width=16 height=22>") 
      textBeforeIcon = textBeforeIcon + "<img alt='' src='" + IMGSRC_BLANK + "' width=16 height=22>"  
    } 
    else 
    {
      this.show(textBeforeIcon + "<img alt='' src='" + IMGSRC_NODE +"' width=16 height=22>") 
      textBeforeIcon = textBeforeIcon + "<img alt='' src='" + IMGSRC_LINEVERT +"' width=16 height=22>" 
    } 
  }
  else 
    this.show("")   
} 

/** 
 * Draw one item onscreen.
 *
 * @param textBeforeIcon      HTML String to be writen before node icon
 * @return              VOID
 */   
function itemShow(textBeforeIcon) 
{
  if (browserType == BROWSER_NS) 
    document.write("<layer id='item" + this.id + "' top=" + document.yPos + " visibility=hidden>") 
  document.write("<table summary='' ") 
  if (browserType == BROWSER_IE) 
    document.write(" id='item" + this.id + "' style='position:block;' ") 
  document.write(" border=0 cellspacing=0 cellpadding=0>") 
  document.write("<tr><td>") 
  document.write(textBeforeIcon)
  //WRITE THE ITEM LINK INFO
//  document.write("<a href=" + this.link + " TARGET='this'>")
  document.write("<a href=" + this.link + ">")
  document.write("<img alt='item " + this.id + "' id='itemIcon"+this.id+"' ") 
  document.write("src='"+this.imgDispSrc+"' border=0>")  
  document.write(" "+this.label)
  document.write("</a>")
  document.write("</td><td valign=middle nowrap>")  
  document.write("</table>")   
  if (browserType == BROWSER_NS) 
    document.write("</layer>") 
  if (browserType == BROWSER_IE) 
  { 
    this.mylayer = document.all["item"+this.id] 
    this.imgDisp = document.all["itemIcon"+this.id] 
  }
  else if (browserType == BROWSER_NS) 
  { 
    this.mylayer = document.layers["item"+this.id] 
    this.imgDisp = this.mylayer.document.images["itemIcon"+this.id] 
    document.yPos=document.yPos+this.mylayer.clip.height 
  } 
} 
 
/** 
 * Marks this node as visible (sets appropriate display parameter)
 * (this same function is used for both Folders and Items)
 * 
 * @return              VOID
 */   
function foldSetVisible() 
{ 
  if (browserType == BROWSER_IE) 
    this.mylayer.style.display = "block" 
  else 
    this.mylayer.visibility = "show" 
} 

/** 
 * Makes master list of all nodes.
 * (this same function is used for both Folders and Items)
 * 
 * @return              VOID
 */ 
function foldMakeIndex() 
{ 
  this.id = totalfolders 
  folderList[totalfolders] = this 
  totalfolders++ 
} 

/** 
 * Finds the height of the tree with its children.
 * (Needed for Netscape only)
 * (this same function is used for both Folders and Items)
 * 
 * @return              VOID
 */  
function foldGetHeight()  
{
  if (this.isOpened) 
    for (var c=0 ;c<this.totalchildren;c++)  
      theheight = this.navObj.clip.height  + this.children[c].getHeight() 
  return theheight 
} 

/** 
 * Expands a Folder of the Tree (as if you clicked on the plus "expand" icon)
 *
 * @param pFolderId		int id of a Folder
 * @see FoldSetStatus
 * @return              VOID
 */  
function expand(pFolderId) 
{
  var folderToExpand = folderList[pFolderId] 
  var state = folderToExpand.isOpened 
  folderToExpand.setStatus(!state)

  //UPDATE FOLDER STATE ARRAY AND foldStates STRING
  if (foldStateAr[pFolderId] != "c")
  {
    var setto  
    if ((!state == true) && (pFolderId != 0))
      setto = "b"
    else
      setto = "a"
     foldStateAr[pFolderId] = setto
     //alert('Setting folder ' + pFolderId + ' to ' + setto);
  }
  foldStates = ""
  for (var i=0;i<=foldStateAr.length-1;i++)
    foldStates = foldStates + foldStateAr[i]  
} 

/** 
 * Restores open and selected Folder's states (opening necessary folders to resume prior state)
 *
 * @param pfstates      String representing folder states
 * @param isRemove      Boolean is true if a folder was just removed
 * @return              VOID
 */  
function restFolders(pfstates,isRemove)
{
  for (var i=0;i<=pfstates.length;i++)
  {
    if (pfstates.charAt(i)=='b')
    { 
      expand(i)
    }
    if (!isRemove)  // Do Not Highlight anything if a remove was just done
    {
      if (pfstates.charAt(i)=='c')
      { 
	expand(i)
	clickedFolder(i)
      }
    }
  }
}
	
/** 
 * Sets up the tree global vars, does browser detection, starts up the tree, expands some default Folders
 *
 * @param header_text	String text to display in Header above Tree for Title
 * @param method_text	String text to display in Header above Tree for Method
 * @param add_text	String text to display in Header above Tree for Add Folder functionality
 * @param rem_text	String text to display in Header above Tree for Delete Folder functionality
 * @param msg0	      String text to display for an Alert message
 * @param msg1	      String text to display for an Alert message
 * @param msg2	      String text to display for an Alert message
 * @param msg3	      String text to display for an Alert message
 * @param msg4	      String text to display for an Alert message
 * @param pft	      the folders Tree Object
 * @param url           the URL provide by ServletSessionManager to get around cookie 
 * @return              VOID
 */    
function initTree(header_text, method_text, add_text, rem_text, msg0, msg1, msg2, msg3, msg4, msg5, pft, url) 
{
  //REMOVE UNDERLINES FROM ITEM HYPERLINKS
  document.write('<style>a{text-decoration:none}</style>')

  //SET LINK AND VLINK COLOR...
  document.write('<BODY link="black" vlink="black">'); 

  //---
  foldersTree = pft
  gheader_text = header_text
  gmethod_text = method_text
  gadd_text = add_text
  grem_text = rem_text

  //STORE THE MESSAGES PASSED FROM iemTree.jsp...
  dbData = new DatabaseData
  dbData.setMsg(0,msg0)
  dbData.setMsg(1,msg1)
  dbData.setMsg(2,msg2)
  dbData.setMsg(3,msg3)
  dbData.setMsg(4,msg4)
  dbData.setMsg(5,msg5)
 
  jspURL = url;

  //---
  gStartBG = document.bgColor
  if (document.all) 
    browserType = BROWSER_IE  
  else 
  {
    if (document.layers)
    { 
      browserType = BROWSER_NS
    }
    else 
      browserType = 0 //UNKNOWN BROWSER
  }
 
  foldersTree.init(0, true, "") 
  foldersTree.setVisible()
  
  if (browserType > 0) 
  {
    //alert(folderList.length)

    document.write("<layer top="+folderList[totalfolders-1].mylayer.top + ">")
    document.write ("&nbsp;</layer>")

    // DRAW THE TREE BY EXPANDING THE 1st FOLDER
    expand(0) 
    expand(0)

    //EXPAND OTHER FOLDERS AFTER THIS LINE (OPTIONAL)

    // SET ALL FOLDER'S STATES TO CLOSED 
    // [BUG 1236508] changing totalfolders to folderList.length
    for (var s=0; s <= folderList.length; s++)
      foldStateAr[s] = 'a'
  } 
} 

/** 
 * Sets up the Knowledge Base Tree (or any other tree that may not contain Add,Delete Methods
 *                                  or a Header Banner)
 *
 * @param pTextAr       Array consisting of the following values (all optional): 
 *                        0)String text to display in Header above Tree for Title
 *                        1)String text to display in Header above Tree for Method
 *                        2)String text to display in Header above Tree for Add Folder functionality
 *                        3)String text to display in Header above Tree for Delete Folder functionality
 * @param pMsgAr	    Array of String text messages to display for Alert messages (optional)
 * @param pft	        the folders Tree Object
 * @return              VOID
 */   
function initKBTree(pTextAr,pMsgAr,pft) 
{
  var NUM_HDR_MSGS = 4
  var NUM_ALERT_MSGS = 1

  //REMOVE UNDERLINES FROM ITEM HYPERLINKS
  document.write('<style>a{text-decoration:none}</style>')
 
  //SET LINK AND VLINK COLOR...
  document.write('<BODY link="black" vlink="black">'); 

  //---
  foldersTree = pft
 
  //FILL IN MESSAGE ARRAYS IF THEY ARE NULL OR PARTIALLY EMPTY ---
  tTextAr = new Array
  tMsgAr = new Array
  if (pTextAr==null)  //IF A NULL ARRAY WAS PASSED IN, FILL IN null FOR ALL VALUES
  { 
    for (var i=0;i<NUM_HDR_MSGS;i++)
      tTextAr[i] = null
  }  
  else
  {
    //COPY WHATEVER WAS PASSED IN
    for (var i=0;i<pTextAr.length;i++)
      tTextAr[i] = pTextAr[i]

    //FILL IN null FOR EMPTY VALUES
    if (tTextAr.length < NUM_HDR_MSGS)
    { 
      for (var i=tTextAr.length;i<NUM_HDR_MSGS;i++)
        tTextAr[i] = null
    }
  }

  if (pMsgAr==null)  //IF A NULL ARRAY WAS PASSED IN, FILL IN null FOR ALL VALUES
  { 
    for (var i=0;i<NUM_ALERT_MSGS;i++)
      tMsgAr[i] = null
  }  
  else
  {
    //COPY WHATEVER WAS PASSED IN
    for (var i=0;i<pMsgAr.length;i++)
      tMsgAr[i] = pMsgAr[i]

    //FILL IN null FOR EMPTY VALUES
    if (tMsgAr.length < NUM_ALERT_MSGS)
    { 
      for (var i=tMsgAr.length;i<NUM_ALERT_MSGS;i++)
        tMsgAr[i] = null
    }
  } 
  //---END - FILLING IN NULL OR EMPTY MESSAGE VALUES ---

  //STORE MESSAGES FOR REST OF SCRIPT TO ACCESS...
  gheader_text = tTextAr[0]
  gmethod_text = tTextAr[1]
  gadd_text = tTextAr[2]
  grem_text = tTextAr[3]

  //STORE THE MESSAGES PASSED FROM iemTree.jsp...
  dbData = new DatabaseData

  //SET GLOBALLY ACCESSABLE DBDATA OBJECT WITH MESSAGES
  for (var i=0;i<tMsgAr.length;i++)
    dbData.setMsg(i,tMsgAr[i])

  //---
  gStartBG = document.bgColor
  if (document.all) 
    browserType = BROWSER_IE  
  else 
    if (document.layers)
    { 
      browserType = BROWSER_NS
    }
    else 
      browserType = 0 //UNKNOWN BROWSER
 
  foldersTree.init(0, true, "") 
  foldersTree.setVisible()
  
  if (browserType > 0) 
  {
    document.write("<layer top="+folderList[totalfolders-1].mylayer.top + ">")
    document.write ("&nbsp;</layer>")

    // DRAW THE TREE BY EXPANDING THE 1st FOLDER
    expand(0) 
    expand(0)

    //EXPAND OTHER FOLDERS AFTER THIS LINE (OPTIONAL)

    // SET ALL FOLDER'S STATES TO CLOSED
    for (var s=0;s<=folderList.length;s++)
      foldStateAr[s] = 'a'
  } 
} 

/** 
 * Adds the "http://" header to a link Only if necessary
 *
 * @param pLinkSpec	String equal to "add" if the header should be added
 * @param pLink		String the URL without the header
 * @param pFrame        String the link's target frame
 * @return              the complete link
 */    
function completeLink(pLinkSpec, pLink, pFrame)
{
  if (pLink != "none") //IF THIS FOLDER HAS A LINK
  {
    if (pLinkSpec=="add")
      theLink = "http://" + pLink
    else
      theLink = pLink
  }
  else theLink = pLink
  return (theLink)
}
