//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        drawing.js                        |
//  |                                                |
//  | Description: Drawing functions in web time     |
//  |                                                |
//  | History:                                       |
//  |          03/02/2000 Added fDrawContentFrameSet |
//  |          by kwidjaja                           |
//  |                                                |
//  +================================================+
/* $Header: pawdutil.js 115.52 2001/04/19 17:29:47 pkm ship      $ */

// map time constants to Cabo constants for buttons
var C_strROUND_ROUND 		= "RR";
var C_strSQUARE_ROUND 		= "SR";
var C_strROUND_SQUARE 		= "RS";
var C_bBUTTON_ENABLED 		= true;
var C_bBUTTON_DISABLED 		= false;
var C_strGRAYBG			= false;
var C_strBLUEBG			= true;
var C_strWIDE_GAP		= "W";
var C_strNARROW_GAP		= "N";
var C_strNO_GAP			= "";
var C_strLOCATOR		= "L";

function fIsPrimaryWindow(p_strMode) {
  if (  (p_strMode==C_strENTER_HOURS) 	||
	(p_strMode==C_strSAVE)		||
	(p_strMode==C_strSUBMIT)	||
	(p_strMode==C_strVIEW_ONLY)	||
	(p_strMode==C_strFINAL_REVIEW)	||
	(p_strMode==C_strUPLOAD)	||
	(p_strMode==C_strPREFERENCES)	||
	(p_strMode==C_strREVERSE)	||
	(p_strMode==C_strAUDIT)		||
	(p_strMode==C_strUNEXPECTED_ERROR)) { return true; }
  return false;
}

// For all Primary Windows only
function fGetPanelTitle(p_strMode){
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_TOOL_BAR");

  if (p_strMode==C_strUPLOAD) {

    return l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLOAD_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strPREFERENCES){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_PREFS_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strSAVE){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_CONFIRM_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strSUBMIT) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_CONFIRM_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strFINAL_REVIEW){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_REVIEW_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strVIEW_ONLY){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_DETAILS_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strAUDIT){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_AUDIT_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strREVERSE){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_REVERSAL_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strENTER_HOURS){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_ENTRY_PANEL_TITLE").mGetstrLabel();
  }
  else if (p_strMode==C_strUNEXPECTED_ERROR){
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_UNEXPERR_PANEL_TITLE").mGetstrLabel();
  }
  else {
    return "";
  }
}


function fGetWindowTitle(p_strMode) {
var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_TOOL_BAR");

// Window title for all Primary Windows is "Oracle Time - <Browser>"
// Window title for all secondary screens is based on the screen

  if (fIsPrimaryWindow(p_strMode)) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_PRIMARY_WINDOW_TITLE").mGetstrLabel();
  }
  else if (p_strMode==top.C_strPREFERENCES) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_PREFS_WINDOW_TITLE").mGetstrLabel();
  }
  else if (p_strMode==top.C_strCALENDAR) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_CALENDAR_WINDOW_TITLE").mGetstrLabel();
  }
  else if ((p_strMode==top.C_strDETAILS) || (p_strMode==top.C_strAUDIT)) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_DETAILS_WINDOW_TITLE").mGetstrLabel();
  }
  else if (p_strMode==top.C_strGENERAL_ERROR)  {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_GENERROR_WINDOW_TITLE").mGetstrLabel();
  }
  else if (p_strMode==top.C_strEXPECTED_ERRORS)  {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_EXPERROR_WINDOW_TITLE").mGetstrLabel();
  }
  else if (p_strMode==top.C_strHELP)  {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_HELP_WINDOW_TITLE").mGetstrLabel();
  }
}



// Functions needed to draw toolbar
function fDrawToolbarDivider(p_framTarget) {
  p_framTarget.document.writeln('<TD class="toolbar" nowrap height="30" align="middle"> <IMG src="' + g_strImagePath + 'FNDIWDVD.gif" align="absmiddle"> </TD>');
}

function fDrawToolbarTitle(p_framTarget) {

  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_TOOL_BAR");

  p_framTarget.document.writeln('<TD WIDTH=50 class="toolbar" nowrap height="30" align="middle">' +
  l_objCurRegion.mobjGetRegionItem("PA_WEB_TOOLBAR_TITLE").mGetstrLabel()    + '</TD>');
}

function fDrawToolbarButton(p_framTarget, p_strButtonType, p_bIsEnabled, p_strOnClick) {

  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_TOOL_BAR");

  var l_strZoomOutImage_src;
  var l_strZoomInImage_src;
  var l_strDisabledImage;
  var l_strEnabledHTML = "";
  var l_strToolTip= "";

// Based on the type of Button, initialize the zoomin, zoomout, disabled image and the tooltip varaibles
  if (p_strButtonType == C_strMENU) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWHOM.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWHO1.gif"
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strSAVE) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWSAV.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWSA1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWSAD.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strPRINT) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWPRT.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWPR1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWPRD1.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_PRINT_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strRELOAD) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWRLD.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWRL1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWRDD.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_RELOAD_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strSTOP) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWSTP.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWST1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWSTD.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_STOP_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strPREFERENCES) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWPPR.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWPP1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWPPD.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_PREFS_TOOL_TIP").mGetstrLabel();
  }
  else if (p_strButtonType == C_strHELP) {
	l_strZoomOutImage_src = g_strImagePath + "FNDIWHLP.gif";
	l_strZoomInImage_src = g_strImagePath + "FNDIWHL1.gif";
	l_strDisabledImage = g_strImagePath + "FNDIWHLD.gif";
	l_strToolTip = l_objCurRegion.mobjGetRegionItem("PA_WEB_HELP_TOOL_TIP").mGetstrLabel();
  }

  if (p_bIsEnabled) {

	l_strEnabledHTML = "<a href=\"" ;
	l_strEnabledHTML += p_strOnClick;
	l_strEnabledHTML += "\" onmouseover=\"document." + p_strButtonType + ".src='";
	l_strEnabledHTML += l_strZoomInImage_src + "'; return true\"";
	l_strEnabledHTML += " onmouseout=\"document." + p_strButtonType + ".src='";
	l_strEnabledHTML += l_strZoomOutImage_src + "'; return true\">";
	l_strEnabledHTML += "<img name=\"" + p_strButtonType + "\" src=\"" + l_strZoomOutImage_src; 
	l_strEnabledHTML += "\" align=absmiddle border=0 alt=\"";
	l_strEnabledHTML += l_strToolTip + "\"></a>";
	p_framTarget.document.writeln('<TD class="toolbar" nowrap height="30" align="middle">' + l_strEnabledHTML + '</TD>');

  }

  else {
	p_framTarget.document.writeln(
		'<TD class="toolbar" nowrap height="30" align="middle"><IMG src="' + l_strDisabledImage + '" align="absmiddle"> </TD>');
  }

}


function fPrintFrame(wind) {

  var l_bIsWin32;
  var l_bIsNav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))

  if (l_bIsNav4) {
    l_bIsWin32 = ((navigator.userAgent.indexOf("Win") != -1) && (navigator.userAgent.indexOf("Win16") == -1));
  } 
  else {
    l_bIsWin32 = ((navigator.userAgent.indexOf("Windows") != -1) && (navigator.userAgent.indexOf("Windows 3.1") == -1));
  }

  // no single frame printing available for Mac
  if (l_bIsWin32) {
    if (!l_bIsNav4) {
      wind.focus() // make sure desired frame has focus
    } 
    wind.print();
  } 
  else {
    alert(top.g_objFNDMsg.mstrGetMsg("PA_WEB_PRINT_ERROR"));
  }

}


//function fStopPage(wind) {
//	if (l_bIsNav4) {
//		wind.stop()
//	} else if (l_bIsWin32) {
//		wind.focus()
//		// requires same <OBJECT> as printing
//		IEControl.ExecWB(23, 0)
//	}
//}


//  --------------------------------------------------      
//  Function: fDrawToolbar
//  Description: Draws the toolbar for a page defined
//               by p_strMode                               
//  --------------------------------------------------      

function fDrawToolbar(p_framTarget, p_strMode) {

  var l_bSave = false;
  var l_bPrint = false;
  var l_bPrefs = false;
  var l_bReload = true;
  var l_bHelp = true;
  var l_strOnClickSave; 
  
  var l_strOnClickPrint    	= "javascript:top.fPrintFrame(top.framMainBody)";
  var l_strOnClickReload   	= "javascript:top.location.reload()";
  var l_strOnClickStop		= "";
  var l_strOnClickPrefs	        = "";
  var l_strOnClickHelp		= "javascript:top.fDrawHelp('"+p_strMode+"')"; 
  var l_strOnClickSave		= "";
  
  //Set values for each of the primary windows
  if (p_strMode==C_strENTER_HOURS){
    l_bSave = true; 
    l_strOnClickSave = "javascript:top.fOnClickSave()";
  }

  else if (p_strMode==C_strFINAL_REVIEW) {
    l_bSave = true;	
    l_strOnClickSave	= "javascript:top.fonClickSaveSubmitOrFinalReview(top.g_objTimecard, top.framMainBody.document.formSaveSubmitFReview, top.C_strFINAL_REVIEW, top.C_strSAVE, top.g_objAuditHistory)"; 
  }
  else if (p_strMode==C_strPREFERENCES) {
    l_bSave = true;	
    l_strOnClickSave	= "javascript:top.g_framCodeLoc.fPrefSave(parent.framMainBody, top.g_framCodeLoc.g_objPref, top.g_framCodeLoc.g_objNewPref)";
  }
  else if (p_strMode==C_strAUDIT && g_strCurrentAction!=C_strMODIFYDELETE) {
    l_bSave = true;
    l_strOnClickSave    = "javascript:top.fOnClickAuditSave()";
  }

  // Based on the screen, disable the help icon for the following:
  if ((p_strMode==C_strSAVE) ||
      (p_strMode==C_strVIEW_ONLY) ||
      (p_strMode==C_strUNEXPECTED_ERROR)) {
    l_bHelp = false;
  }

  // Based on the screen, disable the reload icon for the following:
  if ((p_strMode==C_strSAVE) ||
      (p_strMode==C_strSUBMIT) ||
      (p_strMode==C_strAUDIT) ||
      (p_strMode==C_strFINAL_REVIEW)) {
    l_bReload = false;
  }

  // Based on the screen, alert when user click return to main menu
  if ((p_strMode==C_strSAVE) ||
      (p_strMode==C_strSUBMIT) ||
      (p_strMode==C_strVIEW_ONLY) ||
      (p_strMode==C_strUNEXPECTED_ERROR) ||
      (g_strCurrentAction==C_strMODIFYDELETE)) {
    l_strOnClickMenu 	= "javascript:top.fExitTimecard(false)";
  }
  else if (p_strMode==C_strPREFERENCES) {
    l_strOnClickMenu = "javascript:top.g_framCodeLoc.fPrefCancel(self)";
  }
  else { 
    l_strOnClickMenu 	= "javascript:top.fExitTimecard(true)";
  }

  if ((p_strMode==C_strENTER_HOURS) ||
      (p_strMode==C_strAUDIT) ||
      (p_strMode==C_strPREFERENCES)) {
	l_strOnClickPrint    	= "javascript:top.fPrintFrame(top.framMainBody.framBodyContent)";
  }


  p_framTarget.document.writeln('<TABLE HEIGHT=50 width=100% Cellpadding=0 Cellspacing=0 border=0>');
  p_framTarget.document.writeln(' <TR><TD>');
  p_framTarget.document.writeln('  <TABLE cellpadding="0" cellspacing="0" border="0">');
  p_framTarget.document.writeln('   <TR>');
  p_framTarget.document.writeln('	   <TD rowspan="3"><IMG src="' + g_strImagePath + 'FNDGTBL.gif"></TD>');
  p_framTarget.document.writeln('	   <TD class="highlight" height="1" colspan="11"><IMG src="' + g_strImagePath + 'FNDPX6.gif"></TD>');
  p_framTarget.document.writeln('	   <TD rowspan="3"><IMG src="' + g_strImagePath + 'FNDGTBR.gif"></TD>');
  p_framTarget.document.writeln('   </TR><TR>');


	fDrawToolbarButton(p_framTarget, C_strMENU, true, l_strOnClickMenu);
	fDrawToolbarDivider(p_framTarget);
        fDrawToolbarTitle(p_framTarget);
	fDrawToolbarDivider(p_framTarget);
	fDrawToolbarButton(p_framTarget,  C_strSAVE, l_bSave, l_strOnClickSave);
	fDrawToolbarButton(p_framTarget,  C_strPRINT, true, l_strOnClickPrint);
	fDrawToolbarDivider(p_framTarget);
	fDrawToolbarButton(p_framTarget, C_strRELOAD, l_bReload, l_strOnClickReload);
	fDrawToolbarButton(p_framTarget, C_strSTOP, false, l_strOnClickStop);
//	fDrawToolbarDivider(p_framTarget);
//	fDrawToolbarButton(p_framTarget, C_strPREFERENCES, l_bPrefs, l_strOnClickPrefs);
	fDrawToolbarDivider(p_framTarget);
	fDrawToolbarButton(p_framTarget, C_strHELP, l_bHelp, l_strOnClickHelp);


  p_framTarget.document.writeln('   </TR><TR><TD class="shadow" height="1" colspan="11"><IMG src="' + g_strImagePath + 'FNDPXG2.gif"></TD></TR>');
  p_framTarget.document.writeln('  </TABLE></TD>');
  p_framTarget.document.writeln(' <TD rowspan=5 width=100% align=right><IMG src=' + g_strImagePath + 'FNDLWAPP.gif></TD></TR>');
  p_framTarget.document.writeln('</TABLE>');

}


function fDrawTopPanel(p_framTarget, p_strMode) {

var l_strClass;
var l_strTitle;

  p_framTarget.document.writeln('<TABLE HEIGHT=25 CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100%>');

  if (fIsPrimaryWindow(p_strMode)) {
    l_strClass = "paneltitle";
    l_strTitle = fGetPanelTitle(p_strMode);
  }
  else {
    p_framTarget.document.writeln('  <TR><TD>&nbsp</TD></TR>');     
    l_strClass = "panel";
    l_strTitle = "&nbsp"
  }

  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD class=' + l_strClass + ' ROWSPAN=2 valign=top width=1><IMG SRC=' + g_strImagePath + 'FNDCTTLS.gif></TD>');
  p_framTarget.document.writeln('    <TD class=highlight WIDTH=1000 HEIGHT=1><IMG SRC=' + g_strImagePath + 'FNDPX6.gif height=1></TD>');
  p_framTarget.document.writeln('    <TD class=' + l_strClass + ' ROWSPAN=2 valign=top width=1><IMG SRC=' + g_strImagePath + 'FNDCTTRS.gif></TD>');
  p_framTarget.document.writeln('  </TR>');

  p_framTarget.document.writeln('  <TR><TD class=' + l_strClass + ' valign=top>' + l_strTitle + '</TD></TR>'); 

  if (fIsPrimaryWindow(p_strMode)) {
    p_framTarget.document.writeln('<TR><TD COLSPAN=3 class=highlight WIDTH=1000 HEIGHT=1><IMG SRC=' + g_strImagePath + 'FNDPX6.gif></TD></TR>');}
  
  p_framTarget.document.writeln('</TABLE>');

}

function fDrawToolbarFrame(p_framTarget, p_strMode) {

// Draw Toolbar frame based on whether the page being rendered is a
// primary or Secondary window

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('</HEAD>');

  p_framTarget.document.writeln('<BODY class=appswindow>');

  if (fIsPrimaryWindow(p_strMode)) {
    fDrawToolbar(p_framTarget, p_strMode);
  }

  fDrawTopPanel(p_framTarget,p_strMode);

  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();

}

// Functions needed to draw bottom panel

function fDrawBottomPanel(p_framTarget){

  p_framTarget.document.writeln('<table width=100% cellpadding=0 cellspacing=0 border=0><tr>');
  p_framTarget.document.writeln('<td class=panel rowspan=3 valign=bottom width=10><img src=' + g_strImagePath + 'FNDCTBL.gif height=5 width=5></td>');
  p_framTarget.document.writeln('<td class=panel colspan=3 height=5 width=1000><img src=' + g_strImagePath + 'FNDPXG5.gif height=2></td>');
  p_framTarget.document.writeln('<td class=panel rowspan=3 align=right valign=bottom width=10><img src=' + g_strImagePath + 'FNDCTBR.gif height=5 width=5></td>');
  p_framTarget.document.writeln('</tr></table>');

}

function fDrawButton(p_framTarget, p_strButtonType, p_strButtonText, p_strOnClick, p_bEnabled, p_strBgcolor) {

  p_framTarget.document.writeln (makebuttons(p_strButtonType, p_strButtonText, p_strOnClick, p_bEnabled, p_strBgcolor, C_strNARROW_GAP));

}

//  --------------------------------------------------
//  Function: fDrawButtonFrame
//  Description: Draws the lower button frame for a page
//               defined by p_strMode and p_strAction                        
//  --------------------------------------------------
                                                      
function fDrawButtonFrame(p_framTarget, p_strMode, p_strAction) {

  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_BUTTON");
  var l_strLocator;
  var l_strOnClickNext;

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('</HEAD>');
  p_framTarget.document.writeln('<BODY class=appswindow>');

  fDrawBottomPanel(p_framTarget);

  p_framTarget.document.writeln('<TABLE WIDTH=100% BORDER=0><TR VALIGN=TOP>');

  if ((p_strMode==C_strENTER_HOURS) && (p_strAction==C_strNEW_TIMECARD)) {

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }
 
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),"top.fExitTimecard(true) ",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR,l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    
    p_framTarget.document.writeln('</TD>');
  }

  else if ((p_strMode==C_strENTER_HOURS) && 
           ((p_strAction==C_strMODIFYMODIFY) || (p_strAction==C_strMODIFYCOPY))) {

    // Navigate from the Modify Timecards inquiry
    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    
    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }
 
    p_framTarget.document.writeln(makebuttons(
        C_strROUND_ROUND, l_objCurRegion.mobjGetRegionItem("PA_WEB_RETURN_MODIFY").mGetstrLabel(), "top.fOnClickReturnToInquiry(\""+top.C_strENTER_HOURS+"\")", C_bBUTTON_ENABLED, C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR,l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));

    p_framTarget.document.writeln('</TD>');
  }

  else if ((p_strMode==C_strENTER_HOURS) && (p_strAction==C_strMODIFY)) {

    // Navigate from the Save Confirmation page
    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    
    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }
 
    p_framTarget.document.writeln(makebuttons(
        C_strROUND_ROUND, l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(), "top.fExitTimecard(true)", C_bBUTTON_ENABLED, C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR,l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext, C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if ((p_strMode==C_strENTER_HOURS) && (p_strAction==C_strHISTORYCOPY)) {

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }
 
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_RETURN_HISTORY").mGetstrLabel(),"top.fOnClickReturnToInquiry(\""+top.C_strENTER_HOURS+"\")",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext, C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));

    p_framTarget.document.writeln('</TD>');
  }

  else if ((p_strMode==C_strENTER_HOURS) && (top.g_strCurrentAction==C_strUPLOAD)) {

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel(), "top.fExitTimecard(true)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLD_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLOAD_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }
 
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_SQUARE,l_objCurRegion.mobjGetRegionItem("PA_WEB_BACK").mGetstrLabel(),"top.fInitUpload()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, 2,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext, C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strUPLOAD) {

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_DOWNLOAD").mGetstrLabel(),"top.fOnClickDownload()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CLEAR").mGetstrLabel(), "top.fOnClickClearUpload(top.framMainBody.document.formUploadTimecard)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));

    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>')

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLD_HOURS_AUDIT_REVIEW").mGetstrLabel();
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLOAD_HOURS_REVIEW").mGetstrLabel();
    }
 
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_SQUARE,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),"top.fOnClickExitUpload(top.framMainBody.document.formUploadTimecard)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), "top.fOnClickValidate(top.framMainBody.document.formUploadTimecard)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));

    p_framTarget.document.writeln('</TD>');

  }
  else if (p_strMode==top.C_strREVERSE) {

     if (p_strAction==top.C_strREVERSE) { // when reversing original timecard
      var l_strRegionItem = "PA_WEB_RETURN_HISTORY";
      var l_strOnClickHandler = "top.fOnClickReturnToInquiry(\""+top.C_strREVERSE+"\")";
    }
    else {   // when modifying reverse timecard
      var l_strRegionItem = "PA_WEB_CANCEL";
      var l_strOnClickHandler = "top.fExitTimecard(true)";
    }

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVS_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVERSAL_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickReverseNext(top.g_objSingleEntryTimecard.mGetobjSingleEntryLines(), top.g_objTimecard.mGetobjLines())";
    }

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem(l_strRegionItem).mGetstrLabel(),l_strOnClickHandler,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, 1,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if ((p_strMode==C_strENTER_HOURS) && 
           ((p_strAction==C_strREVERSE) || 
	    (p_strAction==C_strMODIFYREVERSE) ||
	    (p_strAction==C_strMODIFYMODIFYREVERSE) )  ) {

    if (p_strAction==top.C_strREVERSE) { // from Timecard History
      var l_strRegionItem = "PA_WEB_CANCEL";
      var l_strOnClickHandler = "top.fExitTimecard(true)";
    }
    else if (p_strAction==top.C_strMODIFYREVERSE) {   // From Save Conf back to Entry
      var l_strRegionItem = "PA_WEB_MENU";
      var l_strOnClickHandler = "top.fExitTimecard(true)";
    }
    else {  	// From Modify Timecards to Entry
      var l_strRegionItem = "PA_WEB_RETURN_MODIFY";  
      var l_strOnClickHandler = "top.fOnClickReturnToInquiry(\""+top.C_strENTER_HOURS+"\") ";
    }

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(), "top.fOnClickSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    // Check whether Audit is set to FULL
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      // Draw locator with Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVS_HOURS_AUDIT_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickHoursToAudit()";
    }
    else {
      // Draw locator without Audit
      l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVERSAL_HOURS_REVIEW").mGetstrLabel();
      l_strOnClickNext = "top.fOnClickFinalReview()";
    }

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem(l_strRegionItem).mGetstrLabel(),l_strOnClickHandler,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_SQUARE,l_objCurRegion.mobjGetRegionItem("PA_WEB_BACK").mGetstrLabel(),"top.fDrawReversal()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, 2,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(), l_strOnClickNext,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strSAVE) {
    var l_strOnClickEdit;
    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_PRINT").mGetstrLabel(),"top.fPrintFrame(top.framMainBody)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_EDIT").mGetstrLabel(),	"top.fOnClickEditTimecard()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),	"top.fExitTimecard(false)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strSUBMIT) {

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_PRINT").mGetstrLabel(), "top.fPrintFrame(top.framMainBody)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),	"top.fExitTimecard(false)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strFINAL_REVIEW) {
    var l_strLocator;
    var l_strFunBack;
    var l_nStep;

    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {

      l_nStep = 4;
      l_strFunBack="top.fDrawAuditInfo()";

      if (g_strCurrentAction==C_strREVERSE) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVS_HOURS_AUDIT_REVIEW").mGetstrLabel();
      }
      else if (p_strAction==C_strUPLOAD) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLD_HOURS_AUDIT_REVIEW").mGetstrLabel();
      }
      else {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
        l_nStep=3;
      }
    }
    else {
      // Audit is not Full
      l_nStep = 3;
      l_strFunBack = "top.fDrawTimeEntry2()";

      if (g_strCurrentAction==C_strREVERSE) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVERSAL_HOURS_REVIEW").mGetstrLabel();
      }
      else if (p_strAction==C_strUPLOAD) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLOAD_HOURS_REVIEW").mGetstrLabel();
      }
      else {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_REVIEW").mGetstrLabel();
        l_nStep=2;
      }
    }

    p_framTarget.document.writeln('<TD ALIGN=LEFT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(),"top.fonClickSaveSubmitOrFinalReview(top.g_objTimecard, top.framMainBody.document.formSaveSubmitFReview, top.C_strFINAL_REVIEW, top.C_strSAVE, top.g_objAuditHistory)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel(), "top.fExitTimecard(true)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_SQUARE,l_objCurRegion.mobjGetRegionItem("PA_WEB_BACK").mGetstrLabel(),l_strFunBack,C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, l_nStep,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SUBMIT").mGetstrLabel(),"top.fonClickSaveSubmitOrFinalReview(top.g_objTimecard, top.framMainBody.document.formSaveSubmitFReview, top.C_strFINAL_REVIEW, top.C_strSUBMIT, top.g_objAuditHistory)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
      p_framTarget.document.writeln('</TD>');

  }

  else if (p_strMode==C_strVIEW_ONLY) {

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_RETURN_HISTORY").mGetstrLabel(),"top.fOnClickReturnToInquiry(top.C_strVIEW_ONLY)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),	"top.fExitTimecard(false)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strUNEXPECTED_ERROR) {    // Unexpected Error

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_MENU").mGetstrLabel(),	"top.fExitTimecard(false)",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strDETAILS) {

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_OK").mGetstrLabel(),		"top.opener.fOnClickOkDetail()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel(),	"top.opener.fOnClickCancelDetail()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strCALENDAR) {

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CLOSE").mGetstrLabel(),	"top.opener.fModalWinClose()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strPREFERENCES) {

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
      C_strROUND_ROUND,
      l_objCurRegion.mobjGetRegionItem("PA_WEB_OK").mGetstrLabel(), 
      "top.g_framCodeLoc.fPrefSave(parent.framMainBody, top.g_framCodeLoc.g_objPref, top.g_framCodeLoc.g_objNewPref)",
                                                                                
      C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,                           
      C_strROUND_ROUND,
      l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel(), 

      "top.g_framCodeLoc.fPrefCancel(self)", C_bBUTTON_ENABLED, C_strBLUEBG, 
      C_strNO_GAP));

    p_framTarget.document.writeln('</TD>');
  }

  else if (p_strMode==C_strEXPECTED_ERRORS) {

    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');
    p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_OK").mGetstrLabel(),	"top.opener.g_winError.close()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
  }
 else if (p_strMode==C_strAUDIT) {

    var l_strLocator;
    var l_nStep = 3;

    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {

      if (p_strAction==C_strREVERSE) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_REVS_HOURS_AUDIT_REVIEW").mGetstrLabel();
      }
      else if (p_strAction==C_strUPLOAD) {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLD_HOURS_AUDIT_REVIEW").mGetstrLabel();
      } 
      else {
        l_strLocator = l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS_AUDIT_REVIEW").mGetstrLabel();
        l_nStep=2;
      }
    } 

    if (g_strCurrentAction!=C_strMODIFYDELETE) {
      p_framTarget.document.writeln('<TD ALIGN=LEFT>');
      p_framTarget.document.writeln(makebuttons(
      C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_SAVE").mGetstrLabel(),"top.fOnClickAuditSave()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP));
      p_framTarget.document.writeln('</TD>');
    }


    p_framTarget.document.writeln('<TD ALIGN=RIGHT>');

    if (g_strCurrentAction==C_strMODIFYDELETE) {
      p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL_DELETION").mGetstrLabel(), "top.fOnClickReturnToInquiry(top.C_strMODIFYDELETE)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_DELETE_TIMECARD").mGetstrLabel(), "top.fOnClickAuditDelete()", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP));
    }
    else 
      p_framTarget.document.writeln(makebuttons(
	C_strROUND_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel(), "top.fExitTimecard(true)", C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strROUND_SQUARE,l_objCurRegion.mobjGetRegionItem("PA_WEB_BACK").mGetstrLabel(),"top.fOnClickAuditToHours()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNARROW_GAP,
	C_strLOCATOR, l_strLocator, l_nStep,
	C_strSQUARE_ROUND,l_objCurRegion.mobjGetRegionItem("PA_WEB_NEXT").mGetstrLabel(),"top.fOnClickAuditToReview()",C_bBUTTON_ENABLED,C_strBLUEBG, C_strNO_GAP));
    p_framTarget.document.writeln('</TD>');
    
  }
  
  p_framTarget.document.writeln('</TR></TABLE>');
  p_framTarget.document.writeln('</BODY></HTML>');
  p_framTarget.document.close();

}

function fDrawTableTopBorder(p_framTarget, p_strAlign) {

  p_framTarget.document.writeln('<TABLE cellpadding=0 cellspacing=0 border=0 width=98% align=' + p_strAlign + '>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('    <TD height=1 bgcolor=black><IMG src=' + g_strImagePath + 'FNDPX1.gif></TD>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD align=left><IMG src=' + g_strImagePath + 'FNDRTCTL.gif></TD>');
  p_framTarget.document.writeln('    <TD class=TABLEROWHEADER WIDTH=675 height=1 nowrap><IMG src=' + g_strImagePath + 'FNDPX4.gif></TD>');
  p_framTarget.document.writeln('    <TD align=right><IMG src=' + g_strImagePath + 'FNDRTCTR.gif></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD bgcolor=black height=1><IMG src=' + g_strImagePath + 'FNDPX1.gif></TD>');
  p_framTarget.document.writeln('    <TD class=tablesurround height=1 colspan=3>');
}

function fDrawTableBottomBorder(p_framTarget) {

  p_framTarget.document.writeln('  </TD>');
  p_framTarget.document.writeln('  <TD height=1 bgcolor=white><IMG src=' + g_strImagePath + 'FNDPX1.gif></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('    <TD align=left><IMG src=' + g_strImagePath + 'FNDRTCBL.gif></TD>');
  p_framTarget.document.writeln('    <TD class=TABLEROWHEADER width=675 height=1><IMG src=' + g_strImagePath + 'FNDPX4.gif></TD>');
  p_framTarget.document.writeln('    <TD align=right><IMG src=' + g_strImagePath + 'FNDRTCBR.gif></TD>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('    <TD bgcolor=white width=675><IMG src=' + g_strImagePath + 'FNDPX1.gif></TD>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('</TABLE>');

}


function fDrawTableLabel(p_framTarget, p_strTableLabel) {
  p_framTarget.document.writeln('<TR><TD CLASS=FIELDDATA><B>' + p_strTableLabel + '</B></TD></TR>');
  p_framTarget.document.writeln('<TR><TD><IMAGE src=' + g_strImagePath + 'FNDPX3.gif width=98% height=2></TD></TR>');
}


//  ------------------------------------------------------------------------
//  Function: fDrawHelp
//  Description: Opens up the context-sensitive Help window with the
//               proper HTML file. Uses p_strContextPage to determine
//               the context.
//  Author: Kristian Widjaja
//  ------------------------------------------------------------------------

function fDrawHelp(p_strContextPage) {
  var l_strHelpUrl = g_strHelpUrl;
  var l_strDefault = g_strHelpDefault;
  var l_strHelpPage = "";

  // Create regular expression
  var l_regexp = new RegExp (l_strDefault);

  switch (p_strContextPage) {
    case C_strENTER_HOURS:              // Timecard Entry
    case C_strFINAL_REVIEW:             // Review Timecard
    case C_strSUBMIT:                   // Confirm Timecard
      l_strHelpPage = "sstcnt#sstcnt";
      break;
    case C_strUPLOAD:
      l_strHelpPage = "sstudns#sstudns";
      break;
    case C_strPREFERENCES:
      l_strHelpPage = "sstup#sstup";
      break;
    case C_strREVERSE:
      l_strHelpPage = "sstreverse#sstreverse";
      break;
    default:
      l_strHelpPage = l_strDefault;
      break;
  }
  // Construct the full URL by replacing the default target
  if (l_strHelpPage != l_strDefault) 
    l_strHelpUrl = l_strHelpUrl.replace (l_regexp, l_strHelpPage);

  // Open the window with that URL
  help_win = window.open(l_strHelpUrl, "help_win",
  "scrollbars=yes,resizable=yes,toolbar=yes,width=600,height=500");

  // Only refocus if the browser is Netscape
  if (fIsNetscape())
    help_win.focus();
}


// Global Error Window Object
//  ---------------------------------------------------------------------------
//  Function: fReturnLocation
//  Description: Given an Error object, Determine whether it is a
//   		general, line or cell error and return the appropriate string 
//          	to display the info in the Location column in the errors table
// 		of the Errors Window
//		Examples if strings returned by this function is as follows
//  			"General"
//			"Line 3"
//			"Line 3, Tue Feb 02"
//  Note: We don't have Header error check here because there is no validation
//        on the header from the client side.  Both the approver and the week
//        ending date are validation at the server side.  They are considered
//	  as General error.
//
//  ---------------------------------------------------------------------------
function fReturnLocation(p_objError) {

var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_ERRORS");
var l_strLocation;
var l_strFieldName;

  if (p_objError.mbIsGeneralError()) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_GENERAL").mGetstrLabel();
  }
  else if (p_objError.mbIsSetupError()) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_SETUP_ERROR").mGetstrLabel();
  }
  else if (p_objError.mbIsSetupWarning()) {
    return l_objCurRegion.mobjGetRegionItem("PA_WEB_SETUP_WARNING").mGetstrLabel();
  }
  else if (p_objError.mbIsHeaderError()) {
    if (p_objError.mGetstrFieldName() == C_strApprover) {
      return l_objCurRegion.mobjGetRegionItem("PA_WEB_OVERRIDING_APPROVER").mGetstrLabel();
    }
    else if (p_objError.mGetstrFieldName() == C_strWeekEnding) {
      return l_objCurRegion.mobjGetRegionItem("PA_WEB_WEEK_ENDING").mGetstrLabel();
    }    
  }
  else if (p_objError.mbIsLineError()) {    //  Should return "Line 3 Project"

    if (p_objError.mGetstrFieldName() == C_strProject) {
      l_strFieldName = l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT").mGetstrLabel();
    }
    else if (p_objError.mGetstrFieldName() == C_strTask) {
      l_strFieldName = l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK").mGetstrLabel();
    }
    else if (p_objError.mGetstrFieldName() == C_strType) {
      l_strFieldName = l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetstrLabel();
    }
    else if (p_objError.mGetstrFieldName() == C_strAlias) {
      l_strFieldName = l_objCurRegion.mobjGetRegionItem("PA_WEB_USER_PREF_ALIAS_NAME").mGetstrLabel();
    }
    
    l_strLocation = top.g_objFNDMsg.mstrGetMsg("PA_WEB_LINE_ERROR");
    l_strLocation = top.g_objFNDMsg.fstrReplToken(l_strLocation, "&LINE_NUM",p_objError.mGetiLine());
    l_strLocation = top.g_objFNDMsg.fstrReplToken(l_strLocation, "&FIELD_NAME",l_strFieldName);
    return l_strLocation;
  }

  else if (p_objError.mbIsCellError()) {    //  Should return "Line 3, Tue Feb 02"
    l_strLocation = top.g_objFNDMsg.mstrGetMsg("PA_WEB_CELL_ERROR");
    l_strLocation = top.g_objFNDMsg.fstrReplToken(
					l_strLocation, 
					"&LINE_NUM",
					p_objError.mGetiLine());
    l_strLocation = top.g_objFNDMsg.fstrReplToken(
					l_strLocation, 
					"&CELL_DAY",
					fDateToDayOfWeek(p_objError.mGetdCellDate())); 
    l_strLocation = top.g_objFNDMsg.fstrReplToken(
					l_strLocation, 
					"&CELL_DATE",
					fDateToShortString(
						top.g_strSessDateFormat, 
						p_objError.mGetdCellDate(), 
						true)); 
    return l_strLocation;
  }
}

//  --------------------------------------------------
//  Function: fOpenErrorWin
//  Description: Open the error window 
//
//  --------------------------------------------------
function fOpenErrorWin(p_objErrors, p_strHeaderForm, p_strLinesForm) {

  g_objTempErrors = p_objErrors;
  g_winError = fOpenNonModalWindow('winError', 500, 300, 200, 200);
  top.fDrawFrameSetPage(g_winError, "auto",'top.opener.fDrawExpectedErrors(\'' + p_strHeaderForm + '\',\'' + p_strLinesForm + '\')', null, null, C_strEXPECTED_ERRORS);

}

//  --------------------------------------------------
//  Function: fDrawExpectedErrors
//  Description: Draws the content of the expected error
//
//  --------------------------------------------------
function fDrawExpectedErrors(p_strHeaderForm, p_strLinesForm){

//  fDrawToolbarFrame(g_winError.framToolbar, top.C_strEXPECTED_ERRORS);
  return (fDrawExpectedErrorBody(g_objTempErrors, g_winError.framMainBody, p_strHeaderForm, p_strLinesForm));
//  fDrawButtonFrame(g_winError.framButtons, top.C_strEXPECTED_ERRORS);

}

//  --------------------------------------------------
//  Function: fDrawExpectedErrorBody
//  Description: Draw Error Body
//
//  --------------------------------------------------
function fDrawExpectedErrorBody(p_objErrors, p_framTarget, p_strHeaderForm, p_strLinesForm){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_ERRORS");
  var l_strHtmlReturn = "";

  l_strHtmlReturn += ('<HTML DIR="' + g_strDirection + '">\n');
  l_strHtmlReturn += ('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_strHtmlReturn += ('<HEAD>\n');
  l_strHtmlReturn += ('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></link>\n');
  l_strHtmlReturn += ('<link rel=stylesheet type="text/css" href="' + g_strHTMLLangPath + 'pawstime.css"></link>\n');
  l_strHtmlReturn += ('</HEAD>\n');
  l_strHtmlReturn += ('<BODY class=panel>\n');

  l_strHtmlReturn += ('  <TABLE CELLPADDING=5 CELLSPACING=0 BORDER=0 WIDTH=90%>\n');
  l_strHtmlReturn += ('  <TR>\n');
  l_strHtmlReturn += ('    <TD rowspan=' + (p_objErrors.arrErrors.length+3) + ' width=8% valign=top><IMG SRC="' + g_strImagePath + 'FNDERBLG.gif"></TD>\n');
  l_strHtmlReturn += ('    <TD class=fielddata colspan=2>' + l_objCurRegion.mobjGetRegionItem("PA_WEB_ERRORS_TITLE").mGetstrLabel() + '<IMG SRC="' + g_strImagePath + 'FNDPX3.gif" height=2 width=98%></TD>\n');
  l_strHtmlReturn += ('  </TR>\n');
  l_strHtmlReturn += ('  <TR>\n');
  l_strHtmlReturn += ('    <TD class=helptext colspan=2>' + top.g_objFNDMsg.mstrGetMsg("PA_WEB_ERR_INSTRUCT") + '</TD>\n');
  l_strHtmlReturn += ('  </TR>\n');
  
  l_strHtmlReturn += ('  <TR><TD colspan=2></TD></TR>\n');

  l_strHtmlReturn += fDrawExpectedErrorLines(p_objErrors, p_framTarget, p_strHeaderForm, p_strLinesForm);

  l_strHtmlReturn += ('  </TABLE>\n');
  l_strHtmlReturn += ('  </TD>\n');
  l_strHtmlReturn += ('</TR>\n');
  l_strHtmlReturn += ('</TABLE>\n');
  l_strHtmlReturn += ('</FORM>\n');
  l_strHtmlReturn += ('</BODY></HTML>\n');

  return (l_strHtmlReturn);
}


//  --------------------------------------------------
//  Function: fDrawExpectedErrorLines
//  Description: 
//
//  --------------------------------------------------
function fDrawExpectedErrorLines(p_objErrors, p_framTarget, p_strHeaderForm, p_strLinesForm){
  var l_strHtmlReturn = "";

  for(var i=0; i<p_objErrors.arrErrors.length; i++) {

    l_strHtmlReturn += ('  <TR>\n');
    if ((p_objErrors.arrErrors[i].mbIsGeneralError()) || 
      (p_objErrors.arrErrors[i].mbIsSetupError()) || 
      (p_objErrors.arrErrors[i].mbIsSetupWarning()) )   {
      l_strHtmlReturn += ('    <TD VALIGN=TOP class=infoblackright width=40%>' + 
                                       fReturnLocation(p_objErrors.arrErrors[i]) + 
                                       '</TD>\n');
    }
    else {
    // Construct the HREF 
    // NOTE : g_objTempErrors doesn't need to be here. Clean up ASAP
    l_strHtmlReturn += ('    <TD class=infoblackright VALIGN=TOP width=40%><A HREF="javascript:top.opener.fConstructHREF(top.opener.g_objTempErrors.arrErrors[' + i +  '], \'' + p_strHeaderForm + '\', \'' + p_strLinesForm + '\')" onMouseOver="window.status=\'Error\';return true">' + fReturnLocation(p_objErrors.arrErrors[i]) + '</A></TD>\n');
  }

  l_strHtmlReturn += ('    <TD class=infoblack>' + p_objErrors.arrErrors[i].mGetstrMessage() + '</TD>\n');
  l_strHtmlReturn += ('  </TR>\n');
  }
  return (l_strHtmlReturn);
}

//  --------------------------------------------------
//  Function: fConstructHREF
//  Description: Construct the HREF string for the error win
//
//  --------------------------------------------------
function fConstructHREF(p_objError, p_strHeaderForm, p_strLinesForm) {
  var l_strHREF = '';
  var l_strFieldName;

  if (p_objError.mGetstrFieldName() != ''){
    if (p_objError.mGetstrFieldName() == C_strApprover)
      l_strFieldName = 'txtDefApprName';
    if (p_objError.mGetstrFieldName() == C_strWeekEnding)
      l_strFieldName = 'popWeekEndingDate';
    if (p_objError.mGetstrFieldName() == C_strProject)
      l_strFieldName = 'txtProjNum';
    if (p_objError.mGetstrFieldName() == C_strTask)
      l_strFieldName = 'txtTaskNum';
    if (p_objError.mGetstrFieldName() == C_strType)
      l_strFieldName = 'popType';
    if (p_objError.mGetstrFieldName() == C_strAlias)
      l_strFieldName = 'txtAliasName';
  }

  if (p_objError.mbIsHeaderError()) {
    l_strHREF = p_strHeaderForm + '.' + l_strFieldName;
  }
  else if (p_objError.mbIsLineError()) {
    l_strHREF = p_strLinesForm + '.' + l_strFieldName + p_objError.mGetiLine();
  }
  else if (p_objError.mbIsCellError()) {
    l_strHREF = p_strLinesForm + '.' + 'txtHourEntryX' + p_objError.mGetiLine()
	        + 'Y' + p_objError.mGetdCellDate().getDay();
  }

  eval(l_strHREF).focus();
}


function fOpenNonModalWindow(p_strSecondary, p_nWidth, p_nHeight, p_nXcood, p_nYcood) {
var l_objWindow = new Object();

  if ( navigator.appName == "Netscape" ) {  // Open Window
    l_objWindow = open('', p_strSecondary, 'resizable=no,width=' + p_nWidth + ',height=' + p_nHeight + ',screenX=' + p_nXcood + ',screenY=' + p_nYcood + '\'');
  }
  else {
    l_objWindow = open('', p_strSecondary, 'resizable=no,width=' + p_nWidth + ',height=' + p_nHeight + ',left=' + p_nXcood + ',top=' + p_nYcood + '\'');
  }
  return l_objWindow;
}


//  --------------------------------------------------
//  Description: Draws out the FRAMESET page
//                                                    
//  --------------------------------------------------

function fDrawFrameSetPage(p_framTarget, p_strScrollingValue, p_funcInit, p_funcOnLoad, p_funcUnload, p_strMode) { 
  p_framTarget.document.open();
  if ((fIsIE5() && fIsWindows95())&& (p_strMode == C_strCALENDAR || p_strMode == C_strDETAILS || p_strMode == C_strEXPECTED_ERRORS)) 
    fDrawIE5FrameSet(p_framTarget, p_strScrollingValue, p_funcInit, p_funcOnLoad, p_funcUnload, p_strMode);
  else
    fDrawFrameSet(p_framTarget, p_strScrollingValue, p_funcInit, p_funcOnLoad, p_funcUnload, p_strMode);
  p_framTarget.document.close();
}


//  --------------------------------------------------
//  Function: fDrawIE5FrameSet
//  Description: Draws out the main FRAMESET
//                                                    
//  --------------------------------------------------

function fDrawIE5FrameSet(p_framTarget, p_strScrollingValue, p_funcInit, p_funcOnLoad, p_funcUnload, p_strMode) 
{ 
  var l_strFramesetHTML;
  var l_nToolbarRegionSize = 75;
  var l_strHtmlReturn = "";
  var l_strTop = "top.";

  if (fIsPrimaryWindow(p_strMode)) {
    l_nToolbarRegionSize = 75;
  }
  else {
    l_nToolbarRegionSize = 25;
    l_strTop = "top.opener.";
  }

  l_strFramesetHTML = '<FRAMESET rows="' + l_nToolbarRegionSize  
    + ',*,40" border="0"';
  
  // Append l_strFramesetHTML based on whether onLoad event handler is available or not
  if (p_funcOnLoad != null )
  {
    l_strFramesetHTML += ' onLoad="' + p_funcOnLoad + '" ';
  }

  // Append l_strFramesetHTML based on whether onUnload event handler is available or not
  if (p_funcUnload == null )
  {
    l_strFramesetHTML += '>';
  }
  else
  {
    l_strFramesetHTML += ' onUnload="' + p_funcUnload + '">';
  }

  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD><TITLE>' + fGetWindowTitle(p_strMode) + '</TITLE></HEAD>');
  p_framTarget.document.writeln('<FRAMESET cols="3,*,3" border="0">');
 
  // Left thin frame
  p_framTarget.document.writeln('    <FRAME name="framLMargin" FRAMEBORDER=NO MARGINHEIGHT= 0 MARGINWIDTH=0 noresize SCROLLING="no">');
  //p_framTarget.document.writeln('    <FRAME name="framLMargin" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawBlank()" MARGINHEIGHT= 0 MARGINWIDTH=0 noresize SCROLLING="no">');

  // Middle frameset
  p_framTarget.document.writeln(l_strFramesetHTML);

  // Tool bar frame
  p_framTarget.document.writeln('<FRAME name="framToolbar" FRAMEBORDER=NO MARGINHEIGHT=0 MARGINWIDTH=0 noresize SCROLLING="no">');

  // Body frame set
  switch (p_strMode) {
    case C_strCALENDAR:
    case C_strDETAILS:
    case C_strEXPECTED_ERRORS:
        p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;
  }

  // Button frame
  p_framTarget.document.writeln('<FRAME name="framButtons" FRAMEBORDER=NO MARGINWIDTH=0 noresize SCROLLING="no">');
  p_framTarget.document.writeln('</FRAMESET>');

  // Right thin frame
  p_framTarget.document.writeln('<FRAME name="framRMargin" FRAMEBORDER=NO MARGINHEIGHT=0 MARGINWIDTH=0 noresize SCROLLING="no">');
  //p_framTarget.document.writeln('<FRAME name="framRMargin" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawBlank()" MARGINHEIGHT=0 MARGINWIDTH=0 noresize SCROLLING="no">');
  p_framTarget.document.writeln('</FRAMESET>');
  p_framTarget.document.writeln('</HTML>');
  

    if (p_strMode == C_strCALENDAR){
      g_objModalWins.mGetobjLastModalWin().mGetwinModal().framLMargin.document.write(fDrawBlank());
      fDrawToolbarFrame(g_objModalWins.mGetobjLastModalWin().mGetwinModal().framToolbar, p_strMode);
      fDrawIE5CalendarBody(g_iCalDay, g_iCalMonth, g_iCalYear);
      //g_objModalWins.mGetobjLastModalWin().mGetwinModal().framMainBody.document.write(fDrawIE5CalendarBody());
      fDrawButtonFrame(g_objModalWins.mGetobjLastModalWin().mGetwinModal().framButtons, p_strMode, g_strCurrentAction);
      g_objModalWins.mGetobjLastModalWin().mGetwinModal().framRMargin.document.write(fDrawBlank());
    }
    else if (p_strMode == C_strDETAILS){
      g_objModalWins.mGetobjLastModalWin().mGetwinModal().framLMargin.document.write(fDrawBlank());
      fDrawToolbarFrame(g_objModalWins.mGetobjLastModalWin().mGetwinModal().framToolbar, p_strMode);
      fDrawIE5DetailFrameset();
      fDrawButtonFrame(g_objModalWins.mGetobjLastModalWin().mGetwinModal().framButtons, p_strMode, g_strCurrentAction);
      g_objModalWins.mGetobjLastModalWin().mGetwinModal().framRMargin.document.write(fDrawBlank());
    }
    else if (p_strMode == C_strEXPECTED_ERRORS){
      p_framTarget.framLMargin.document.write(fDrawBlank());
      fDrawToolbarFrame(p_framTarget.framToolbar, p_strMode);

      p_framTarget.framMainBody.document.open();
      p_framTarget.framMainBody.document.write(fDrawExpectedErrorBody(top.g_objErrors, p_framTarget.framMainBody, 'top.framMainBody.framBodyHeader.document.formEntryBodyHeader', 'top.framMainBody.framBodyContent.document.formEntryBodyContent'));
      p_framTarget.framMainBody.document.close();

      fDrawButtonFrame(p_framTarget.framButtons, p_strMode, g_strCurrentAction);
      p_framTarget.framRMargin.document.write(fDrawBlank());
    }

}

//  --------------------------------------------------
//  Function: fDrawFrameSet
//  Description: Draws out the main FRAMESET
//                                                    
//  --------------------------------------------------

function fDrawFrameSet(p_framTarget, p_strScrollingValue, p_funcInit, p_funcOnLoad, p_funcUnload, p_strMode) 
{ 
  var l_strFramesetHTML;
  var l_nToolbarRegionSize = 75;
  var l_strHtmlReturn = "";
  var l_strTop = "top.";

  if (fIsPrimaryWindow(p_strMode)) {
    l_nToolbarRegionSize = 75;
  }
  else {
    l_nToolbarRegionSize = 25;
    l_strTop = "top.opener.";
  }

  l_strFramesetHTML = '<FRAMESET rows="' + l_nToolbarRegionSize  
    + ',*,40" border="0"';
  
  // Append l_strFramesetHTML based on whether onLoad event handler is available or not
  if (p_funcOnLoad != null )
  {
    l_strFramesetHTML += ' onLoad="' + p_funcOnLoad + '" ';
  }

  // Append l_strFramesetHTML based on whether onUnload event handler is available or not
  if (p_funcUnload == null )
  {
    l_strFramesetHTML += '>';
  }
  else
  {
    l_strFramesetHTML += ' onUnload="' + p_funcUnload + '">';
  }

  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD><TITLE>' + fGetWindowTitle(p_strMode) + '</TITLE><HEAD>');
  p_framTarget.document.writeln('<FRAMESET cols="3,*,3" border="0">');
 
  // Left thin frame
  p_framTarget.document.writeln('    <FRAME name="framLMargin" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawBlank()" MARGINHEIGHT= 0 MARGINWIDTH=0 noresize SCROLLING="no">');

  // Middle frameset
  p_framTarget.document.writeln(l_strFramesetHTML);

  // Tool bar frame
  p_framTarget.document.writeln('<FRAME name="framToolbar" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawToolbarFrame(top.framToolbar,\'' + p_strMode + '\')" MARGINHEIGHT=0 MARGINWIDTH=0 noresize SCROLLING="no">');

  // Body frame set
  //fDrawContentFrameSet (p_framTarget, p_strMode);
  switch (p_strMode) {
    case C_strEXPECTED_ERRORS:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:'+ p_funcInit + '" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;

    case C_strUNEXPECTED_ERROR:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:'+ p_funcInit + '" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;

    case C_strCALENDAR:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawCalendarBody()" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');

      break;

    case C_strUPLOAD:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:top.fDrawWrap(\'top.fDrawUploadFrame(top.framMainBody, top.g_objEmployees);\')" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;

    case C_strREVERSE:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:top.fDrawWrap(\'top.fDrawRevBodyContent(top.framMainBody);\')" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;

    case C_strVIEW_ONLY:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:top.fDrawWrap(\'top.fDrawSummaryFrame(top.framMainBody, top.g_objTimecard, top.C_strVIEW_ONLY, top.g_objAuditHistory, top.g_objDeletedAuditHistory);\')" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
      break;

    default:
      p_framTarget.document.writeln('<FRAME name="framMainBody" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawContentFrameSet(\'' + p_strMode + '\')" MARGINHEIGHT=0 MARGINWIDTH=0 noresize scrolling="'+p_strScrollingValue+'">');
  }

  // Button frame
  p_framTarget.document.writeln('<FRAME name="framButtons" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawButtonFrame(top.framButtons,\'' + p_strMode + '\',\'' + g_strCurrentAction + '\')" MARGINWIDTH=0 noresize SCROLLING="no">');
  p_framTarget.document.writeln('</FRAMESET>');

  // Right thin frame
  p_framTarget.document.writeln('<FRAME name="framRMargin" FRAMEBORDER=NO SRC="javascript:' + l_strTop + 'fDrawBlank()" MARGINHEIGHT=0 MARGINWIDTH=0 noresize SCROLLING="no">');
  p_framTarget.document.writeln('</FRAMESET>');
  p_framTarget.document.writeln('</HTML>');

}


// General Error ( Alerts, Confirms, ....) functions
// Draw buttons, cannot use fDrawButtonFrame since the buttons can be different for different types of errors

var l_arrGeneralErrorButtonText = null;
var l_arrGeneralErrorOnClickHandler = null;

function fDrawGeneralErrorButtons(p_framTarget, p_arrButtonText, p_arrOnClickHandler) {
  var i=0;
  var l_strCodeLoc = g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc();

  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD><LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></LINK></HEAD>');
  p_framTarget.document.writeln('<BODY class=appswindow>');
  fDrawBottomPanel(p_framTarget);
  p_framTarget.document.writeln(' <TABLE BORDER=0 WIDTH=100%>');
  p_framTarget.document.writeln(' <TR><TD ALIGN=RIGHT><TABLE BORDER=0><TR VALIGN=TOP>');

// For each element in the array, call fDrawButton
  for (i=0; i<p_arrButtonText.length; i++) {
    p_framTarget.document.writeln(' <TD>');	    
    p_framTarget.document.writeln(fDrawButton(p_framTarget, C_strROUND_ROUND, p_arrButtonText[i], "javascript:" +l_strCodeLoc+ p_arrOnClickHandler[i], C_bBUTTON_ENABLED, C_strBLUEBG));
    p_framTarget.document.writeln(' </TD>');	    
  }
  p_framTarget.document.writeln(' </TR></TABLE>');	    
  p_framTarget.document.writeln(' </TD></TR></TABLE>');	    
  p_framTarget.document.writeln(' </BODY></HTML>');	    

}

function fDrawGeneralError(p_strTitle, p_strGif, p_strErrorMessage, p_arrButtonText, p_arrOnClickHandler) {

  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody;
  var l_funcOnFocus;

  l_funcOnFocus = "javascript:" +
		g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc()+ 
		"fModalWinOnFocusPreventClick()";

// Draw Top Panel
  fDrawToolbarFrame(l_winTarget.framToolbar, top.C_strGENERAL_ERROR);

// Draw Body
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<HEAD><LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></LINK></HEAD>');

  // Make onclick for IE and onfocus for Netscape
  if ( navigator.appName == "Microsoft Internet Explorer" ) { 
    l_framTarget.document.writeln('<BODY class=panel onClick="' + l_funcOnFocus + '">');
  }  
  else {
    l_framTarget.document.writeln('<BODY class=panel onFocus="' + l_funcOnFocus + '">');
  }

  l_framTarget.document.writeln(' <TABLE class=panel BORDER=0 CELLPADDING=5 CELLSPACING=0 WIDTH=100%>');
  l_framTarget.document.writeln('   <TR><TD ROWSPAN=3 VALIGN="TOP"><img src=' + g_strImagePath + p_strGif + ' align="absmiddle">  </TD>');
  l_framTarget.document.writeln('        <TD><TABLE CELLSPACING=0 CELLPADDING=1 WIDTH=100% BORDER=0>');
  l_framTarget.document.writeln('            <TR><TD class=INFOBLACK><B>' + p_strTitle + '</B></TD></TR>');
  l_framTarget.document.writeln('            <TR><TD><IMAGE src=' + g_strImagePath + 'FNDPX3.gif width=100% height=2></TD></TR>');
  l_framTarget.document.writeln('            <TR><TD class=INFOBLACK>' + p_strErrorMessage + '</TD></TR>');
  l_framTarget.document.writeln('            </TABLE></TD></TR>');
  l_framTarget.document.writeln('  </TABLE>');
  l_framTarget.document.writeln('</BODY>');
  l_framTarget.document.writeln('</HTML>');

  fDrawGeneralErrorButtons(l_winTarget.framButtons, p_arrButtonText, p_arrOnClickHandler);
}


function fGenErrButtonHandler(p_funcHandler) {
  var l_nTimerID;

  fModalWinClose();
  // Need to set timer so as avoid synchronization problems
  l_nTimerID = setTimeout(p_funcHandler,100);
}

function fDrawConfirm(p_strMessage, p_funcOKButtonHandler, p_funcCancelButtonHandler) { 
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_GENERAL_ERROR");
  fDrawOKCancel(l_objCurRegion.mobjGetRegionItem("PA_WEB_WARNING").mGetstrLabel(), "FNDIWARN.gif",p_strMessage,p_funcOKButtonHandler, p_funcCancelButtonHandler  );
}

function fDrawOKCancel(p_strTitle, p_strImage, p_strMessage, p_funcOKButtonHandler, p_funcCancelButtonHandler) { 

  var l_strOnClick = null;
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_GENERAL_ERROR");

  p_funcOKButtonHandler = 'fGenErrButtonHandler("' + p_funcOKButtonHandler + '")';
  p_funcCancelButtonHandler = 'fGenErrButtonHandler("' + p_funcCancelButtonHandler + '")';

  l_arrGeneralErrorButtonText = new Array();
  l_arrGeneralErrorOnClickHandler = new Array();

// Initialize buttons array
  l_arrGeneralErrorButtonText[0] = l_objCurRegion.mobjGetRegionItem("PA_WEB_OK").mGetstrLabel();
  l_arrGeneralErrorOnClickHandler[0]= p_funcOKButtonHandler;
  l_arrGeneralErrorButtonText[1]= l_objCurRegion.mobjGetRegionItem("PA_WEB_CANCEL").mGetstrLabel();;
  l_arrGeneralErrorOnClickHandler[1]= p_funcCancelButtonHandler;

  l_strOnClick = "top.opener.fDrawGeneralError(" 
			+ "'"+ p_strTitle + "',"
			+ "'"+ p_strImage   + "',"
			+ "'"+ p_strMessage + "',"
			+ "top.opener.l_arrGeneralErrorButtonText ,"
			+ "top.opener.l_arrGeneralErrorOnClickHandler)"

  top.fModalWinOpen(500, 300, 200, 200, l_strOnClick, top.C_strGENERAL_ERROR, null, top.g_objModalWins);
}

function fDrawAlert(p_strMessage, p_funcOKButtonHandler) { 

  var l_strOnClick = null;
  var l_strTitle;
  var l_strGif = "PALINFO.gif";
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_GENERAL_ERROR");

  p_funcOKButtonHandler = 'fGenErrButtonHandler("' + p_funcOKButtonHandler + '")';

  l_strTitle = l_objCurRegion.mobjGetRegionItem("PA_WEB_WARNING").mGetstrLabel();
  l_arrGeneralErrorButtonText = new Array();
  l_arrGeneralErrorOnClickHandler = new Array();
  l_arrGeneralErrorButtonText[0]=l_objCurRegion.mobjGetRegionItem("PA_WEB_OK").mGetstrLabel();
  l_arrGeneralErrorOnClickHandler[0]= p_funcOKButtonHandler;

  l_strOnClick = "top.opener.fDrawGeneralError(" 
			+ "'"+ l_strTitle + "',"
			+ "'"+ l_strGif   + "',"
			+ "'"+ p_strMessage + "',"
			+ "top.opener.l_arrGeneralErrorButtonText ,"
			+ "top.opener.l_arrGeneralErrorOnClickHandler)"

  top.fModalWinOpen(500, 300, 200, 200, l_strOnClick, top.C_strGENERAL_ERROR, null, top.g_objModalWins);
}


// General utility functions

function fInitUnexpectedError(p_strErrMesg) {

var l_framTarget = top.framMainBody;

  if (top.frames.length==0) {
    fDrawFrameSet(top, "auto", 'top.fDrawUnexpectedError(\'' + p_strErrMesg +  '\')', null, null, top.C_strUNEXPECTED_ERROR);
  }
  else {
    fDrawToolbarFrame(top.framToolbar, C_strUNEXPECTED_ERROR);
    l_framTarget.document.open();
    l_framTarget.document.writeln(fDrawUnexpectedError(p_strErrMesg));
    l_framTarget.document.close();
    fDrawButtonFrame(top.framButtons, C_strUNEXPECTED_ERROR, null);
  }

}


function fDrawUnexpectedError(p_strErrMesg) {

var l_strReturnHTML = '';

  l_strReturnHTML += '<HTML DIR="' + g_strDirection + '">';
  l_strReturnHTML += '<BASE HREF="' + top.g_strSessBaseHRef  + '">';
  l_strReturnHTML += '<HEAD><LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></LINK></HEAD>';
  l_strReturnHTML += ' <BODY class=panel>';
  l_strReturnHTML += '  <TABLE class=panel BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=100%>';
  l_strReturnHTML += '    <TR><TD CLASS=INFOBLACK COLSPAN=2>' + p_strErrMesg + '</TD></TR>';
  l_strReturnHTML += '  </TABLE></BODY></HTML>';

  return l_strReturnHTML;
}


//  --------------------------------------------------
//  Function: fCloseApp
//  Description: This is called by fCloseApp to close
//  	error window when closing the application
//	It is also called by fControlSSToCSContinue
//	to close error window when there are no errors
//	but the error window is open.
//  --------------------------------------------------
function fCloseErrorWindow() {
  if (g_winError!=null && !g_winError.closed) {
        g_winError.close();
  }
}


//  --------------------------------------------------
//  Function: fExitTimecard
//  Description: This is called when the cancel button
//  		is clicked or when the "Return to Main
//		Menu " is clicked on the toolbar.Based 
//		on the Input parameter,confirm with the 
//		user if that the click is intentional.
//  --------------------------------------------------
function fExitTimecard(p_bConfirm){
  if (p_bConfirm) {
    if (!confirm(g_objFNDMsg.mstrGetMsg("PA_WEB_EXIT_TIMECARD"))) {
      return;
    }
  }
  fCloseErrorWindow();
  window.close();
}

//  --------------------------------------------------
//  Function: fDrawBlank
//  Description: This functions creates a blank screen.
//  	It is the function replacement of pawblank.htm

function fDrawBlank() {
   return "<HTML><BODY BGCOLOR=#336699></BODY></HTML>";
}


//  --------------------------------------------------
//  Function: fDrawWrap
//  Description: This functions creates a wrapper around.
//  	a drawing function. It does this, so that
//	it will create a string that calls the function.

function fDrawWrap (p_strDrawFunction) {
  var l_strHtmlReturn = "";

  if (fIsNetscape())
     return (eval (p_strDrawFunction));

  l_strHtmlReturn += '<SCRIPT LANGUAGE="Javascript">\n';
  l_strHtmlReturn += p_strDrawFunction;
  l_strHtmlReturn += '\n</SCRIPT>\n';

  return (l_strHtmlReturn);
}

//  --------------------------------------------------
//  Function: fDrawContentFrameSet
//  Description: This function draws the content frameset.
//  		This frameset is usually drawn within the
//	        body frame. It will define the following
//              frames: framBodyHeader, framBodyContent,
//	        framBodyButtons.
//  Author:     jfeng and kwidjaja
//  --------------------------------------------------

function fDrawContentFrameSet(p_strMode)
{
  var l_nHeaderRegionSize = 220;
  var l_nButtonRegionSize = 40;
  var l_funcOnLoad = "";
  var l_strSrcBodyHeader = 'javascript:top.fDrawBlank()';
  var l_strSrcBodyContent = 'javascript:top.fDrawBlank()';
  var l_strSrcBodyButtons = 'javascript:top.fDrawBlank()';
  var l_strNameBodyHeader = 'framBodyHeader';
  var l_strNameBodyContent = 'framBodyContent';
  var l_strNameBodyButtons = 'framBodyButtons';
  var l_strHtmlReturn="";

  // Set parameters for different modes
  switch (p_strMode) {
    case C_strPREFERENCES:
      l_nHeaderRegionSize = 220;
      l_strSrcBodyHeader = "javascript:top.fDrawWrap('top.fDrawPrefBodyHeader (top.framMainBody.framBodyHeader);')";
      l_strSrcBodyContent = "javascript:top.fDrawWrap('top.fDrawPrefBodyContent (top.framMainBody.framBodyContent);')";
      l_strSrcBodyButtons = "javascript:top.fDrawWrap('top.fDrawPrefBodyButtons (top.framMainBody.framBodyButtons);')";
      l_funcOnLoad = 'javascript:top.fPrefUpdateDisp (top.framMainBody,true);';
      break;

    case C_strENTER_HOURS:
      l_nHeaderRegionSize = 225;
      l_strSrcBodyHeader ="javascript:top.fDrawWrap('top.fDrawEntryBodyHeader();')";
      l_strSrcBodyContent ="javascript:top.fDrawWrap('top.fDrawEntryBodyContent();')";
      l_strSrcBodyButtons ="javascript:top.fDrawWrap('top.fDrawEntryBodyButtons();')";
      l_funcOnLoad = 'javascript:top.fPopulateData();';
      break;

    case C_strDETAILS:
      l_nHeaderRegionSize = 150; 
      l_strSrcBodyHeader ="javascript:top.opener.fDrawDetailBodyHeader("+ g_iCurrLine +")";
      l_strSrcBodyContent ="javascript:top.opener.fDrawDetailBodyContent()";
      l_strSrcBodyButtons ="javascript:top.opener.fDrawDetailBodyButtons()";
      l_funcOnLoad = 'javascript:top.opener.fPopulateDetailWithTimeout();';
      break;

    case C_strAUDIT:
      l_nHeaderRegionSize = 85;
      l_nButtonRegionSize = 0;
      l_strSrcBodyHeader = "javascript:top.fDrawWrap('top.fDrawAuditBodyHeader (top.framMainBody.framBodyHeader,top.g_objTimecard);')";

      l_strSrcBodyContent = "javascript:top.fDrawWrap('top.fDrawAuditBodyContent(top.framMainBody.framBodyContent,top.g_objAuditHistory);')";

      l_strSrcBodyButtons = "javascript:top.fDrawWrap('top.fDrawAuditBodyButtons(top.framMainBody.framBodyButtons);')";
      break;
  }
  l_strHtmlReturn += '<HTML DIR="' + g_strDirection + '">\n';
  l_strHtmlReturn += '<BASE HREF="' + top.g_strSessBaseHRef  + '">';
  l_strHtmlReturn += ('<FRAMESET rows="' + l_nHeaderRegionSize
    + ',*,' + l_nButtonRegionSize +'" BORDER=0 FRAMESPACING=0 ');
  l_strHtmlReturn += ('onLoad="' + l_funcOnLoad +'">\n');

  l_strHtmlReturn += ('<FRAME NAME="' + l_strNameBodyHeader +'" SRC="' + 
    l_strSrcBodyHeader +'" MARGINHEIGHT=0 MARGINWIDTH=3 SCROLLING=NO FRAMEBORDER=NO>\n');
  l_strHtmlReturn += ('<FRAME NAME="' + l_strNameBodyContent +'" SRC="' + 
    l_strSrcBodyContent + '" MARGINHEIGHT=0 MARGINWIDTH=3 FRAMEBORDER=NO SCROLLING="AUTO">\n');
  l_strHtmlReturn += ('<FRAME NAME="' + l_strNameBodyButtons +'" SRC="' + 
    l_strSrcBodyButtons + '" MARGINHEIGHT=0 MARGINWIDTH=3 FRAMEBORDER=NO SCROLLING="NO">\n');

  l_strHtmlReturn += ('</FRAMESET>\n');
  l_strHtmlReturn += '</HTML>\n';
  return (l_strHtmlReturn);
}

function fDrawIE5DetailFrameset()
{
  var l_nHeaderRegionSize = 220;
  var l_nButtonRegionSize = 40;
  var l_funcOnLoad = "";
  var l_strHtmlReturn="";
  var l_strNameBodyHeader = 'framBodyHeader';
  var l_strNameBodyContent = 'framBodyContent';
  var l_strNameBodyButtons = 'framBodyButtons';

  var l_winTarget = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_framTarget = l_winTarget.framMainBody;

  // Set parameters for different modes
      l_nHeaderRegionSize = 150; 
      l_strSrcBodyHeader ="javascript:top.opener.fDrawDetailBodyHeader("+ g_iCurrLine +")";
      l_strSrcBodyContent ="javascript:top.opener.fDrawWrap('top.opener.fDrawDetailBodyContent();')";
      l_strSrcBodyButtons ="javascript:top.opener.fDrawWrap('top.opener.fDrawDetailBodyButtons();')";
      l_funcOnLoad = 'javascript:top.opener.fPopulateDetailWithTimeout();';

  l_framTarget.document.open();
  l_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">\n');
  l_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  l_framTarget.document.writeln('<FRAMESET rows="' + l_nHeaderRegionSize
    + ',*,' + l_nButtonRegionSize +'" BORDER=0 FRAMESPACING=0> ');
  //l_framTarget.document.writeln('onLoad="' + l_funcOnLoad +'">\n');

  l_framTarget.document.writeln('<FRAME NAME="' + l_strNameBodyHeader +'"  MARGINHEIGHT=0 MARGINWIDTH=3 SCROLLING=NO FRAMEBORDER=NO>\n');
  l_framTarget.document.writeln('<FRAME NAME="' + l_strNameBodyContent +'" MARGINHEIGHT=0 MARGINWIDTH=3 FRAMEBORDER=NO SCROLLING="AUTO">\n');
  l_framTarget.document.writeln('<FRAME NAME="' + l_strNameBodyButtons +'" MARGINHEIGHT=0 MARGINWIDTH=3 FRAMEBORDER=NO SCROLLING="NO">\n');

  l_framTarget.document.writeln('</FRAMESET>\n');
  l_framTarget.document.writeln('</HTML>\n');
   

  fDrawDetailBodyHeader(g_iCurrLine);
  fDrawDetailBodyContent();
  fDrawDetailBodyButtons();
  fPopulateDetailWithTimeout();
  
}
