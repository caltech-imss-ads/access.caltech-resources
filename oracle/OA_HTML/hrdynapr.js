<!-- hide the script contents from feeble browsers
// ==========================================================================
// Change History
// Version  Date         Author      Bug #    Remarks
// -------  -----------  ----------- -------- -------------------------------
// 110.00   20-JAN-2000  snachuri             Created 
// 110.01   25-FEB-2000  snachuri            Added new function
//                                           close_button_onClick and
//                                           modified cancel_button_onClick.
// 110.02   27-FEB-2000  snachuri            Modified function notifierLOV,
//                                           removed check for blank form data.
// 110.03   13-MAR-2000  snachuri            Changed the file name to 8.3 naming
//                                           convention.
// --------------------------------------------------------------------------
// $Header: hrdynapr.js 115.0 2000/03/21 11:55:46 pkm ship   $

// Function for invoking the approval page

var ApprovalWindow;
// Function Approval_button_onClick
function Approval_button_onClick()
{
       if (!ApprovalWindow || ApprovalWindow.closed)
        {

var URL = top.container_middle.document.DynamicApprovalForm.p_url.value;

      ApprovalWindow=window.open(URL,"ApprovalsNotifications","status=no,menubar=no,scrollbars=yes,toolbar= no,location=no,resizable=no,alwaysRaised=yes,HEIGHT=550,WIDTH=700");
 
       
     ApprovalWindow.opener = window;
                                                              
    }
        else {
          ApprovalWindow.focus();
             }


}



function verify_approval_form_data()
{
var response = 'OK';
var additional_approvers = document.approvalsForm.p_additional_approvers.value;
var i = document.approvalsForm.P_CURRENT_ROW.value - 1;
var count = 0;

// check for the balnk values in the approvers section.
if (additional_approvers>1)
{
  for (count =0;count<(additional_approvers -1);count++)
    {
      if (is_blank(document.approvalsForm.p_approver_name[count].value))
       {
        document.approvalsForm.p_approver_name[count].focus();
         return count.toString();
      }
    }
}
if (additional_approvers==1)
{
if (is_blank(document.approvalsForm.p_approver_name.value))
       {
        document.approvalsForm.p_approver_name.focus();
        return count.toString();
      }

}
  // check for the blank values in the notifiers section.
var notifier_count = document.approvalsForm.p_notifiers_count.value;
var counter = 0;
 if (notifier_count==1)
   {
     if (is_blank(document.approvalsForm.p_notifier_name.value))
       {
        document.approvalsForm.p_notifier_name.focus();
         counter =  101;
        return counter.toString();
       }
      
   }
 if (notifier_count>1)
  {
   for (counter =0;counter<(notifier_count -1);counter++)
    {
      if (is_blank(document.approvalsForm.p_notifier_name[counter].value))
       {
        document.approvalsForm.p_notifier_name[counter].focus();
         counter = counter + 100;
         return counter.toString();
      }
    }
 

  }
return ("OK");
}

// Function for adding approver row to the approver table

// Function add_approver_button_onClick

function add_approver_button_onClick()
{
var index = Number(document.approvalsForm.p_approver_index.value);
var response = verify_approval_form_data();
if (response =='OK')
{
   if (index ==9999) 
     {
        document.approvalsForm.p_approver_index.value = 0;
      alert(message_array[5]);
     }
   else
    {
     // set the approver index before submit
document.approvalsForm.p_approver_index.value = document.approvalsForm.P_SELECT_LIST.options[document.approvalsForm.P_SELECT_LIST.selectedIndex].index + 1;
}

document.approvalsForm.p_mode.value='ADD';
document.approvalsForm.submit();


}
else
{
 var counter = Number(response);
  if (counter>99)
   {
    alert(message_array[10]);
  }
  else
  {
 alert(message_array[4] + (Number(response) + 1));
  }
}

}


function delete_approver_button_onClick(p_row_num)
{
 if (confirm(message_array[6] +p_row_num))
{
document.approvalsForm.p_approver_index.value =p_row_num;
document.approvalsForm.p_mode.value='DELETE';
document.approvalsForm.submit();
}

}


// Function for adding notifier row to the notifier table

// Function add_notifier_button_onClick

function add_notifier_button_onClick()
{
var response = verify_approval_form_data();
   if (response =='OK')
     {
      document.approvalsForm.p_add_person.value ='';
      document.approvalsForm.p_mode.value='ADD_NOTIFIER';
      document.approvalsForm.submit();
      }
   else
     {
      var index = Number(response);
       if (index > 100)
         {
          alert(message_array[10]);
         }
       else
         {
          alert(message_array[4] + (Number(response) + 1));
          }
         

  
    }



}



<!-- Modified ICX LOV code
function myLOV(
   c_row_no
  ,c_attribute_app_id
  ,c_attribute_code
  ,c_region_app_id
  ,c_region_code
  ,c_form_name
  ,c_frame_name
  ,c_where_clause
  ,c_js_where_clause
  )
 {

document.approvalsForm.P_CURRENT_ROW.value = c_row_no ;
var i = document.approvalsForm.P_CURRENT_ROW.value - 1;
var person_name ;
person_name = document.approvalsForm.p_approver_name[i].value ;
document.add_approver.P_PERSON_NAME.value=person_name;

lov_win = window.open("icx_util.LOV?c_attribute_app_id=" +c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +"&c_region_app_id=" + c_region_app_id + "&c_region_code=" +c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +c_frame_name + "&c_where_clause=" + c_where_clause +"&c_js_where_clause=" + c_js_where_clause,"LOV","resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");

lov_win.opener = self;
    lov_win.focus()




}

function approver_changed() 
{
var person_name ='p_person_name_'+document.approvalsForm.P_CURRENT_ROW.value;
var additional_approvers = document.approvalsForm.p_additional_approvers.value;
var i = document.approvalsForm.P_CURRENT_ROW.value - 1;

if (additional_approvers>1)
{
document.approvalsForm.p_approver_name[i].value =document.add_approver.P_PERSON_NAME.value;

}
else
{
document.approvalsForm.p_approver_name.value =document.add_approver.P_PERSON_NAME.value;

}
document.approvalsForm.p_mode.value = 'UPDATE';
document.approvalsForm.submit();
} // End approver_changed





function notifierLOV(
   c_row_no
  ,c_attribute_app_id
  ,c_attribute_code
  ,c_region_app_id
  ,c_region_code
  ,c_form_name
  ,c_frame_name
  ,c_where_clause
  ,c_js_where_clause
  )
 {
   var response = verify_approval_form_data();
document.approvalsForm.P_CURRENT_ROW.value = c_row_no ;
var i = document.approvalsForm.P_CURRENT_ROW.value - 1;
var person_name ;
var count = document.approvalsForm.p_notifiers_count.value;
// set the index for the notifier name

// check if we have more than one notifier
    if (count >1)
      {
       person_name = document.approvalsForm.p_notifier_name[i].value ;
      }
   else
     {
      person_name = document.approvalsForm.p_notifier_name.value ;
     }
document.add_notifier.P_PERSON_NAME.value=person_name;


lov_win = window.open("icx_util.LOV?c_attribute_app_id=" +c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +"&c_region_app_id=" + c_region_app_id +"&c_region_code=" +c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +c_frame_name + "&c_where_clause=" + c_where_clause +"&c_js_where_clause=" +c_js_where_clause,"LOV","resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");

lov_win.opener = self;
    lov_win.focus()
}

function notifier_changed()
{
var name = document.add_notifier.P_PERSON_NAME.value;
var i = document.approvalsForm.P_CURRENT_ROW.value - 1;
var count = document.approvalsForm.p_notifiers_count.value;

   if (count>1)
    {
     document.approvalsForm.p_notifier_name[i].value = name;
    }
   else
   {
   document.approvalsForm.p_notifier_name.value = name;
   }

document.approvalsForm.p_mode.value ='UPDATE_NOTIFIER';
document.approvalsForm.submit();

}


// function notifier check

function notifier_onSubmit_check(row_no)
{
var response = verify_approval_form_data();
var i = Number(row_no) - 1;


if (i>0)
{
  if (document.approvalsForm.p_notify_onsubmit_flag[i].checked
      || document.approvalsForm.p_notify_onapproval_flag[i].checked) 
     {
      response = 'OK';

     }
  else
     {
      alert(message_array[8]);
      document.approvalsForm.p_notify_onsubmit_flag[i].checked =true;
     }
  
}

else
{
       if (document.approvalsForm.p_notify_onsubmit_flag.checked 
          ||document.approvalsForm.p_notify_onapproval_flag.checked) 
     {
         response = 'OK';

     }
       else
     {
      alert(message_array[8]);
      document.approvalsForm.p_notify_onsubmit_flag.checked=true;
     }



}

}

function notifier_onApproval_check(row_no)
{
var i = row_no -1;
var response = verify_approval_form_data();

if (i>0)
{
  if (document.approvalsForm.p_notify_onsubmit_flag[i].checked
      || document.approvalsForm.p_notify_onapproval_flag[i].checked)
     {
       response = 'OK';
     }
  else
     {
      alert(message_array[8]);
      document.approvalsForm.p_notify_onapproval_flag[i].checked=true;
     }
 
}

else
{
       if (document.approvalsForm.p_notify_onsubmit_flag.checked
          ||document.approvalsForm.p_notify_onapproval_flag.checked)
     {
              response = 'OK';

     }
       else
     {
      alert(message_array[8]);
      document.approvalsForm.p_notify_onapproval_flag.checked=true;
     }



}




}



function verify_form_data(input)
{
var response = verify_approval_form_data();

  if (response =='OK')
  {
  document.approvalsForm.p_mode.value ='VERIFY_CLOSE';
  document.approvalsForm.submit();
  
  }
 else
   {
   alert(message_array[4]); 
   }



}



function cancel_button_onClick(msg)
{

if (confirm(msg))
 {
parent.opener.top.container_middle.document.Redisplay.submit();
top.close();
 }

}


function close_window(msg)
{
top.close();
}



function delete_notifier_button_onClick(p_row_num)
   {
   if (confirm(message_array[9] +p_row_num))
   {
  document.approvalsForm.p_approver_index.value =p_row_num;
  document.approvalsForm.p_mode.value='DELETE_NOTIFIER';
  document.approvalsForm.submit();
  }
}


function close_button_onClick()
{

parent.opener.top.container_middle.document.Redisplay.submit();

top.close();

}

