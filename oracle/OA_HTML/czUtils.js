/* $Header: czUtils.js 115.14 2001/06/14 15:34:33 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99   CK Jeyaprakash, K MacClay  Created.                    |
 |                                                                           |
 +===========================================================================*/

function Utils ()
{

}

function Utils_clearCollection (coll)
{
  var len = 0;
  len = coll.length;
  if (len) {
    //coll is index based;
    for (var i=0; i<len; i++) {
      coll[i] = null;
      delete coll[i];
    }
    coll.length = 0;
  } else {
    //coll may be associative;
    for (var id in coll) {
      coll[id] = null;
      delete coll[id];
    }
  }
}

/**
 * Returns the actual image size as an array [width, height].
 * For wrong imgSrc or if no imgTestLayer is in czSrc window then it will
 * return null
 */

function Utils_getImageSize (imgSrc)
{
  var width, height, strHtml ='';
  var doc, block;
  if (ie4) {
    if (! document.all["imgTestLayer"]) 
      return null;
  } else if (ns4) {
    if (! document.layers["imgTestLayer"])
      return null;
  }
  strHtml += '<IMG src="' + imgSrc + '" BORDER=0></IMG>';
  if (ie4) {
    doc = document.all["imgTestLayer"];
    doc.innerHTML = strHtml;
    width = document.all["imgTestLayer"].scrollWidth;
    height = document.all["imgTestLayer"].scrollHeight;
  } else if (ns4) {
    doc = document.layers["imgTestLayer"].document;
    block = document.layers["imgTestLayer"];
    doc.open ();
    doc.writeln (strHtml);
    doc.close ();
    //NS adds a 4 pixel padding when reporting the values
    width = block.clip.width - 4;
    height = block.clip.height - 4;
  }    
  return (new Array (width, height));
}

// Class Methods
Utils.clearCollection = Utils_clearCollection;
Utils.getImageSize = Utils_getImageSize;
 
/**
* Class Font
*/

function Font (family, size, style, weight)
{
  //list of font names;
  if (family)
    this.family = family;
  else	
    this.family = null;
  //Font size;
  if (size)
    this.size = size;
  else
    this.size = null;
  //Font style;
  if (style)
    this.style = style;
  else
    this.style = null;
  //Font weight;
  if (weight)
    this.weight = weight;
  else
    this.weight = null;
  
  this.bPropsChanged = true;
}

function Font_setFamily (family)
{
  this.family = family
    this.bPropsChanged = true;
}

function Font_getFamily ()
{
  return (this.family)
}

function Font_setSize (size)
{
  this.size = size;
  this.bPropsChanged = true;
}

function Font_getSize ()
{
  return (this.size);
}

function Font_setStyle (style)
{
  this.style = style;
  this.bPropsChanged = true;
}

function Font_getStyle ()
{
  return (this.style);
}

function Font_setWeight (weight)
{
  this.weight = weight;
  this.bPropsChanged = true;
}

function Font_getWeight ()
{
  return (this.weight);
}

/**
*  @param - seed (optional) characters
*/
function Font_getCharWidth (seed, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (seed);
    return (this.charWidth);
  } else
    return (this.charWidth);
}

/**
*  @param - seed (optional) characters
*/
function Font_getCharHeight (seed, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (seed);
    return (this.charHeight);
  } else
    return (this.charHeight);
}

function Font_getTextWidth (text, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (text);
    return (this.textWidth);
  } else
    return (this.textWidth);
}

function Font_getTextHeight (text, bOverride)
{
  return (this.getCharHeight (text, bOverride));
}

/**
*  @param - seed (optional) characters
*/
function Font_determineFontProperties (seed)
{
  var width, height, strHtml ='';
  var doc, block;
  if (ie4) {
    if (! document.all.fontTestLayer) return;
  } else if (ns4) {
    if (! document.layers["fontTestLayer"])
      return;
  }
  if (ie4) {
    strHtml += '<NOBR ID=fontTest STYLE="position:absolute; left:0; top:0; ';
    strHtml += 'font-family:' + this.family + '; font-size:' + this.size + '; ';
    strHtml += 'font-style:' + this.style + '; font-weight:' + this.weight + ';">';
  } else if (ns4) {
    strHtml += '<NOBR CLASS="fontTest">';
  }
  if (seed)
    strHtml += seed + '</NOBR>';
  else
    strHtml += 'aArRsStTwWzZ</NOBR>';
  if (ie4) {
    doc = document.all["fontTestLayer"];
    doc.innerHTML = strHtml;
    width = document.all["fontTest"].scrollWidth;
    height = document.all["fontTest"].scrollHeight;
  } else if (ns4) {
    doc = document.layers["fontTestLayer"].document;
    block = document.layers["fontTestLayer"];
    with (document.classes.fontTest.NOBR) {
      fontFamily = this.family;
      fontSize = this.size;
      fontStyle = this.style;
      fontWeight = this.weight;
    }
    doc.open ();
    doc.writeln (strHtml);
    doc.close ();
    width = block.clip.width;
    height = block.clip.height;
  }
  if (seed) {
    this.charWidth = Math.round (width/seed.length);
    this.textWidth = Math.round (width);
  } else
    this.charWidth = Math.round (width/12);
  this.charHeight = height;
  this.bPropsChanged = false;
}

Font.prototype.setFamily = Font_setFamily;
Font.prototype.getFamily = Font_getFamily;
Font.prototype.setSize = Font_setSize;
Font.prototype.getSize = Font_getSize;
Font.prototype.setStyle = Font_setStyle;
Font.prototype.getStyle = Font_getStyle;
Font.prototype.setWeight = Font_setWeight;
Font.prototype.getWeight = Font_getWeight;
Font.prototype.getCharWidth = Font_getCharWidth;
Font.prototype.getCharHeight = Font_getCharHeight;
Font.prototype.getTextWidth = Font_getTextWidth;
Font.prototype.getTextHeight = Font_getTextHeight;
Font.prototype.determineFontProperties = Font_determineFontProperties;

/**
*	Column Descriptor Class
*/

function ColumnDescriptor (name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring)
{
  this.name = name;
  // types: label, inputtext, icon;
  this.type = type;
  this.index = index;

  // pixel dimensions;
  this.defHeight = ht;
  this.defWidth = wd; 
  this.defVisible = true;
  this.spring = false;
  
  if (pdgLt != null) {
    this.pdgLeft = pdgLt;
  } else {
    this.pdgLeft = 5;
  }  
  if (pdgTp != null) {
    this.pdgTop = pdgTp;
  } else {
    this.pdgTop = 0;
  }
  if (valign) {
    this.valign = valign;
  } else {
    this.valign = 'center';
  }
  if (halign) {
    this.halign = halign;
  } else {
    this.halign = 'left';
  }
  if (defvis != null) {
    this.defVisible = defvis;
  }
  if (spring != null) {
    this.spring = spring;
  }
}

function ColumnDescriptor_setPrefWidth (wd)
{
  this.prefWidth = wd;
  this.availWidth = wd;
}

function ColumnDescriptor_getPrefWidth ()
{
  if (isNaN (this.prefWidth)) {
    return (this.defWidth);
  } else {
    return (this.prefWidth);
  }
}

function ColumnDescriptor_setPrefHeight (ht)
{
  this.prefHeight = ht;
  this.availHeight = ht;
}

function ColumnDescriptor_getPrefHeight ()
{
  return (this.prefHeight);
}

function ColumnDescriptor_setAvailWidth (wd)
{
  this.availWidth = wd;
}

function ColumnDescriptor_getAvailWidth ()
{
  if (isNaN (this.availWidth)) {
    return (this.getPrefWidth());
  } else {
    return (this.availWidth);
  }
}

function ColumnDescriptor_setAvailHeight (ht)
{
  this.availHeight = ht;
}

function ColumnDescriptor_getAvailHeight ()
{
  return (this.availHeight);
}

function ColumnDescriptor_setLeft (lt)
{
  this.left = lt;
}

function ColumnDescriptor_getLeft ()
{
  return (this.left);
}

ColumnDescriptor.prototype.setPrefWidth = ColumnDescriptor_setPrefWidth;
ColumnDescriptor.prototype.getPrefWidth = ColumnDescriptor_getPrefWidth;
ColumnDescriptor.prototype.setPrefHeight = ColumnDescriptor_setPrefHeight;
ColumnDescriptor.prototype.getPrefHeight = ColumnDescriptor_getPrefHeight;
ColumnDescriptor.prototype.setAvailWidth = ColumnDescriptor_setAvailWidth;
ColumnDescriptor.prototype.getAvailWidth = ColumnDescriptor_getAvailWidth;
ColumnDescriptor.prototype.setAvailHeight = ColumnDescriptor_setAvailHeight;
ColumnDescriptor.prototype.getAvailHeight = ColumnDescriptor_getAvailHeight;
ColumnDescriptor.prototype.setLeft = ColumnDescriptor_setLeft;
ColumnDescriptor.prototype.getLeft = ColumnDescriptor_getLeft;


function ColumnDataDescriptor (name, value, wd)
{
  this.name = name;
  this.value = '';
  this.width = 0; // data width (generally,  in chars);
  
  if (value) {
    this.value = value;
  }
  if (wd) {
    this.width = wd; 
  }
}

