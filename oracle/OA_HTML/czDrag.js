/* $Header: czDrag.js 115.11 2001/06/14 15:33:53 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  CK Jeyaprakash K MacClay Created.                       |
 |                                                                           |
 +===========================================================================*/

function Drag(name, evtMgr) {
  
  if(name) {
    if(Drag.instances[name]) 
      return(Drag.instances[name]);
    else 
      Drag.instances[name] = this;
  }
  this.obj = null;
  this.dragList = null;
  this.active = false;
  this.zIndex = 0;
  this.reSort = true;
  this.lastX = null;
  this.lastY = null;
  if (evtMgr != null) {
    this.emg = evtMgr;
  } else if(self.EventManager)
    this.emg = new EventManager();
}

function Drag_add() 
{
  for (var i=0; i<arguments.length; i++) {
    if(arguments[i]) {
      if (this.dragList == null) 
        this.dragList = new Array();
      this.dragList[this.dragList.length]= arguments[i];
      arguments[i].resizable = true; //change back to false later;
      arguments[i].draggable = true;
      this.zIndex += 1;
      if(this.active) {
        arguments[i].addListener('mousedownCallback',this,'super');
      }
    }
  }
}


function Drag_remove() 
{
  if(this.dragList) {
    for (var i=0; i<arguments.length; i++) {
      for (var j=0; j<this.dragList.length; j++) {
        if (this.dragList[j]==arguments[i]) {
          if(this.active) {
            arguments[i].removeListener('mousedownCallback',this);
          }
          for (var k=j;k<=this.dragList.length-2;k++) 
            this.dragList[k] = this.dragList[k+1];
          this.dragList[this.dragList.length-1] = null;
          this.dragList[this.dragList.length-1].resizable = false;
          this.dragList[this.dragList.length-1].draggable = false;
          this.dragList.length -= 1;
          break
        }
      }
    }
  }
}

function Drag_off()
{
  if(!this.active) 
    return;

  if(this.dragList) {
    for(var i=0; i<this.dragList.length; i++) {
      if(this.dragList[i]) {
        this.dragList[i].removeListener('mousedownCallback',this);
      }
    }
    this.emg.removeListener('docMouseMove',this);
    this.emg.removeListener('docMouseUp',this);		
    this.active = false;
  }	
}

function Drag_on(emg)
{
  if(this.active) {
    if(emg) {
      if(this.emg != emg) {
        this.emg.removeListener('docMouseMove',this);
        this.emg.removeListener('docMouseUp',this);		
      }
    } else
      return true;
  }
  if(this.dragList) {
    if(emg)
      this.emg = emg;
    for(var i=0; i<this.dragList.length; i++) {
      if(this.dragList[i]) {
        this.dragList[i].addListener('mousedownCallback',this,'super');
      }
    }	
    this.emg.addListener('docMouseMove',this);
    this.emg.addListener('docMouseUp',this);		
    this.active = true;
  }
}

function Drag_removeAll() 
{
  if(this.dragList!=null) {
    for (var i=0; i<this.dragList.length; i++) {
      this.dragList[i] = null;
      this.dragList[i].resizable = false;
      this.dragList[i].draggable = false;
    }
    this.dragList = null;
  }
}

function Drag_setGrab(element,top,right,bottom,left) 
{ 
  element.dragGrab = new Array(top,right,bottom,left)
}


function Drag_lockXPos(block)
{
  block.lock ='x';
}

function Drag_lockYPos(block)
{
  block.lock ='y';
}

function Drag_lockRegion(block,top,right,bottom,left)
{
  block.lock ='region';
  block.lockTop = top;
  block.lockRight = right;
  block.lockBottom = bottom;
  block.lockLeft = left;
}

function Drag_reSort(sort)
{
  this.reSort = sort;
}

function Drag_mousedownCallback(e,obj) 
{
  //if other than left mouse button exit this function;
  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;

  if(obj.resizable) {
    if(this.resizeStart(e,obj)) {
      if (this.obj.onResizeStart) 
        this.obj.onResizeStart(e,this.obj);
      this.obj.notifyListeners('onResizeStart',e);
      return false;
    }
  }
  if(obj.draggable) {
    if (this.dragStart(e,obj)) {
      if (this.obj.onDragStart) 
        this.obj.onDragStart(e,this.obj);
      this.obj.notifyListeners('onDragStart',e);
      return false;
    }
  }		
  return true;		
}

function Drag_mousemoveCallback(e,obj) 
{
  if (this.obj) {
    if ( this.obj.eventManager.msdown == true )
       this.dragActive = true;
    if(this.resizeActive) {
      this.reSizeMove(e,this.obj);	 
      if (this.obj.onResizing) 
        this.obj.onResizing(e,this.obj);
      this.obj.notifyListeners('onResizing',e);
    }
    if(this.dragActive) {
      this.dragMove(e,this.obj);
      if (this.obj.onDragging) 
        this.obj.onDragging(e,this.obj);
      this.obj.notifyListeners('onDragging',e);
    }
    if(ie4)
      e.returnValue = false;
  }
  return true;
}

function Drag_mouseupCallback(e,obj) 
{
  if(this.obj) {
    if(this.resizeActive) {
      if (this.obj.onResizeEnd) 
        this.obj.onResizeEnd(e,this.obj);
      this.obj.notifyListeners('onResizeEnd',e);
      this.resizeActive = false;
    }
    if(this.dragActive) {
      if (this.obj.onDragDrop) 
        this.obj.onDragDrop(e,this.obj);
      this.obj.notifyListeners('onDragEnd',e);
      this.dragActive = false;
    }else
      this.obj.toggle(0);
    this.obj = null;
    return false;
  }
  return true;
}

function Drag_dragStart(e,obj) 
{
  if(obj.dragGrab) {
    var layerX = (ns4)? e.layerX : e.offsetX;
    var layerY = (ns4)? e.layerY : e.offsetY;

    if(!obj.isInside(layerX,layerY,obj.dragGrab[0],obj.dragGrab[1],obj.dragGrab[2],obj.dragGrab[3]))
      return false;
  }
  var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
  var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

  this.obj = obj;
  //this.dragActive = true;

  this.lastX = x;
  this.lastY = y;

  if (this.reSort) {
    this.zIndex++;
    if(obj.glue) {
      var glueList = obj.glue.glueArray;
      for(var i=0; i<glueList.length;i++)
        glueList[i].getHTMLObj().zIndex = this.zIndex;
    } else
      obj.getHTMLObj().zIndex = this.zIndex; 
  }
  return true;		
}

function Drag_dragMove(e, obj) 
{
  var x = (ns4)? e.pageX : e.clientX+document.body.scrollLeft;
  var y = (ns4)? e.pageY : e.clientY+document.body.scrollTop;
  var obj = this.obj;
  
  var dx = x- this.lastX;
  var dy = y- this.lastY;
  
  if(obj.lock) {
    switch(obj.lock) {
    case 'x':
      dx =0;
      break;
    case 'y':
      dy =0;
      break;
    case 'region':
      if((obj.left+ dx) < obj.lockLeft) 
        dx = obj.lockLeft- obj.left;
      else if ((obj.left+ obj.width + dx) > obj.lockRight) 
        dx = obj.lockRight - (obj.left+ obj.width);
      if((obj.top + dy) < obj.lockTop) 
        dy = obj.lockTop- obj.top;
      else if((obj.top + obj.height + dy) > obj.lockBottom) 
        dy = obj.lockBottom - (obj.top+ obj.height);
      break;
    }
  }
  obj.moveBy(dx,dy);
  this.lastX = x;
  this.lastY = y;

  return true;
}

function Drag_reSizeMove(e,obj)
{
  var X = (ns4)? e.pageX : e.clientX;
  var Y = (ns4)? e.pageY : e.clientY;
  
  if(this.resizeSide == 2)  {
    if((obj.width+ X-this.lastX)>5) 
      obj.setWidth(obj.width+ X-this.lastX);
  } else if(this.resizeSide == 3) {
    if((obj.height+ Y-this.lastY)>5) 
      obj.setHeight(obj.height+ Y-this.lastY);
  }
  this.lastX = X;
  this.lastY = Y;

  return true;
}

function Drag_resizeStart(e,obj)
{
  var layerX = (ns4)? e.layerX : e.offsetX;
  var layerY = (ns4)? e.layerY : e.offsetY;

  if (obj.isInside(layerX,layerY,0,obj.width,obj.height,0)) {
    if (!obj.isInside(layerX,layerY,0,obj.width-5,obj.height-5,0)) {
      if(layerX>(obj.width-5)) {
        if(obj.lock)
          if(obj.lock == 'x') return false;
        this.resizeSide = 2;
      } else {
        if(obj.lock) 
          if(obj.lock == 'y') return false;
        this.resizeSide = 3;
      }
      this.resizeActive = true;
      this.lastX = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
      this.lastY = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

      this.obj = obj;
      return true;
    }
  }
  return false;
}

function Drag_clearInstance (name)
{
  if (Drag.instances[name]) {
    Drag.instances[name].destroy ();
    Drag.instances[name] = null;
    delete Drag.instances[name];
  }
}

function Drag_destroy ()
{
  if (this.dragList) {
    Utils.clearCollection (this.dragList);
    this.dragList = null;
    delete this.dragList;
  }
}

Drag.clearInstance = Drag_clearInstance;

Drag.instances 			= new Array();
Drag.prototype.add 		= Drag_add;
Drag.prototype.on 		= Drag_on;
Drag.prototype.off 		= Drag_off;
Drag.prototype.remove 		= Drag_remove;
Drag.prototype.removeAll 	= Drag_removeAll
Drag.prototype.setGrab 		= Drag_setGrab;
Drag.prototype.reSort 		= Drag_reSort;
Drag.prototype.lockXPos		= Drag_lockXPos;
Drag.prototype.lockYPos 	= Drag_lockYPos;
Drag.prototype.lockRegion 	= Drag_lockRegion;
Drag.prototype.reSizeMove 	= Drag_reSizeMove;
Drag.prototype.resizeStart 	= Drag_resizeStart;
Drag.prototype.dragStart 	= Drag_dragStart;
Drag.prototype.dragMove 	= Drag_dragMove;
Drag.prototype.mousedownCallback= Drag_mousedownCallback;
Drag.prototype.docMouseUp 	= Drag_mouseupCallback;
Drag.prototype.docMouseMove 	= Drag_mousemoveCallback;
Drag.prototype.destroy = Drag_destroy;

