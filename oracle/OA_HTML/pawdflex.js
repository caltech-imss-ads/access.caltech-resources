//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawdflex.js                       |
//  |                                                |
//  | Description: Client-side class definition      |
//  |              for descriptive flexfields        |     
//  |                                                |
//  | History:     Created 09/02/99                  |
//  |              Shaun Kaneshiro                   |
//  |                                                |
//  +================================================+
/* $Header: pawdflex.js 115.8 2000/09/15 08:28:31 pkm ship        $ */

var C_strDFLEXNAME = "DFLEX";
var C_iHIDDEN = 1;
var C_iTEXTBOX = 2;
var C_iPOPLIST = 3;
var C_iCHECKBOX = 4;
var C_iNUMMAXDFLEXSEG = 10;
var C_strGLOBALCONTEXTNAME = "Global Data Elements";

// Value Class
function Value(p_strCode, p_strDesc) {
  this.strDesc = p_strDesc;
  this.strCode = p_strCode;
}

Value.prototype.mGetstrDesc = mGetstrDesc;
Value.prototype.mGetstrCode = mGetstrCode;

function mGetstrDesc() {
  return this.strDesc;
}

function mGetstrCode() {
  return this.strCode;
}

// Values Class
function Values(p_strName) {
  this.strName = p_strName;
  this.arrValue = new Array();
}

function mGetstrName() {
  return this.strName;
}

function mAddValue(p_objValue) {
  this.arrValue[this.arrValue.length] = p_objValue;
}

function miGetNumOfValues() {
  return this.arrValue.length;
}

function mobjFindValue(p_strCode) {

  for(var i=0; i<this.arrValue.length; i++) {
    if (this.arrValue[i].mGetstrCode() == p_strCode)
      return this.arrValue[i].mGetstrDesc();
  }
  return "";
}

Values.prototype.mAddValue = mAddValue;
Values.prototype.miGetNumOfValues = miGetNumOfValues;
Values.prototype.mobjFindValue = mobjFindValue;
Values.prototype.mGetstrName = mGetstrName;

// CollectionOfValues Class
function CollectionOfValues() {
  this.arrValues = new Array();
}

CollectionOfValues.prototype.mobjAddValues = mobjAddValues;
CollectionOfValues.prototype.mobjGetValues = mobjGetValues;

function mobjAddValues(p_objValues) {
  this.arrValues[p_objValues.mGetstrName()] = p_objValues;
  return p_objValues;
}

function mobjGetValues(p_strName) {
  var undefined;

  var objValues = this.arrValues[p_strName];
  if (objValues == undefined)
    return null;
  return objValues;
}

// Segment Class
function Segment(p_strPrompt, p_bIsRequired, p_strDefaultValue, p_iObjectType,
                 p_iTextBoxLength, p_objPoplistValues, p_iColumn, p_bIsNumericOnly, 
                 p_bIsUpperCaseOnly) {
  
  this.strPrompt = p_strPrompt;
  this.bIsRequired = p_bIsRequired;
  this.strDefaultValue = p_strDefaultValue;
  this.iObjectType = p_iObjectType;
  this.iTextBoxLength = p_iTextBoxLength;
  this.objPoplistValues = p_objPoplistValues;
  this.iColumn = p_iColumn;
  this.bIsNumericOnly = p_bIsNumericOnly;
  this.bIsUpperCaseOnly = p_bIsUpperCaseOnly;
}

Segment.prototype.mGetstrPrompt = mGetstrPrompt;
Segment.prototype.mGetbIsRequired = mGetbIsRequired;
Segment.prototype.mGetstrDefaultValue = mGetstrDefaultValue;
Segment.prototype.mGetiObjectType = mGetiObjectType;
Segment.prototype.mGetiTextBoxLength = mGetiTextBoxLength;
Segment.prototype.mGetobjPoplistValues = mGetobjPoplistValues;
Segment.prototype.mGetiColumn = mGetiColumn;
Segment.prototype.mGetbIsNumericOnly = mGetbIsNumericOnly;
Segment.prototype.mGetbIsUpperCaseOnly = mGetbIsUpperCaseOnly;

function mGetstrPrompt() {
  return this.strPrompt;
}

function mGetbIsRequired() {
  return this.bIsRequired;
}

function mGetstrDefaultValue() {
  return this.strDefaultValue;
}

function mGetiObjectType() {
  return this.iObjectType;
}

function mGetiTextBoxLength() {
  return this.iTextBoxLength;
}

function mGetobjPoplistValues() {
  return this.objPoplistValues;
}

function mGetiColumn() {
  return this.iColumn;
}

function mGetbIsNumericOnly() {
  return this.bIsNumericOnly;
}

function mGetbIsUpperCaseOnly() {
  return this.bIsUpperCaseOnly;
}

// Segments Class
function Segments() {
  this.arrSegment = new Array();
}

Segments.prototype.mCopy = mCopy;
Segments.prototype.mAddSegment = mAddSegment;
Segments.prototype.mobjGetSegment = mobjGetSegment;
Segments.prototype.miGetNumOfSegment = miGetNumOfSegment;

function mAddSegment(p_objSegment) {
  this.arrSegment[this.arrSegment.length] = p_objSegment;
}

function mobjGetSegment(p_strPrompt) {
  for(var i=0; i<this.arrSegment.length; i++)
    if (this.arrSegment[i].strPrompt == p_strPrompt)
      return this.arrSegment[i];
  return null;
}

function mCopy(p_objSegments) {
  for(var i=0; i<p_objSegments.arrSegment.length; i++)
    this.mAddSegment(p_objSegments.arrSegment[i]);
}

function miGetNumOfSegment() {
  return this.arrSegment.length;
}

// Context Class
function Context(p_strContextName, p_objSegments) {
  this.strContextName = p_strContextName;
  this.objSegments = p_objSegments;
}

Context.prototype.mGetstrContextName = mGetstrContextName;
Context.prototype.mGetobjSegments = mGetobjSegments;

function mGetstrContextName() {
  return this.strContextName;
}

function mGetobjSegments() {
  return this.objSegments;
}

// Contexts Class
function Contexts() {
  this.arrContext = new Array();
}

Contexts.prototype.mobjAddContext = mobjAddContext;
Contexts.prototype.mobjGetContext = mobjGetContext;
Contexts.prototype.mobjGetContextWithDefault = mobjGetContextWithDefault;

function mobjAddContext(p_objContext) {
//  alert('adding context: ' + p_objContext.mGetstrContextName());
  this.arrContext[p_objContext.mGetstrContextName()] = p_objContext;
  return p_objContext;
}

function mobjGetContext(p_strName) {
//  alert('getting context: ' + p_strName);
  var undefined;

  var objContext = this.arrContext[p_strName];
  if (objContext == undefined)
    return null;
  return objContext;
}

function mobjGetContextWithDefault(p_strName) {
  var objContext = this.mobjGetContext(p_strName);

  if (objContext == null)  
    objContext = this.mobjGetContext(C_strGLOBALCONTEXTNAME);
  return objContext;
}

var C_strDFLEXNAME = "DFLEX";
var C_iHIDDEN = 1;
var C_iTEXTBOX = 2;
var C_iPOPLIST = 3;
var C_iCHECKBOX = 4;

// ReturnS the Dflex p_iIndex-th widget on p_formTarget
function fGetDFlexWidget(p_formTarget, p_iIndex, p_iDayOfWeek) {
  return eval('p_formTarget.' + C_strDFLEXNAME + 'Y' + p_iDayOfWeek + 'I' + p_iIndex);
}

// Returns the Dflex p_iIndex-th widget on p_formTarget
function fGetDFlexWidgetName(p_iIndex, p_iDayOfWeek) {
  return C_strDFLEXNAME + 'Y' + p_iDayOfWeek + 'I' + p_iIndex;
}

function fPopulateSegment(p_objWidget, p_strWidgetType, p_strValue) {

  if (p_strWidgetType == C_iTEXTBOX) {
    p_objWidget.value = p_strValue;
  }
  else if (p_strWidgetType == C_iHIDDEN) {
    p_objWidget.value = p_strValue;
  }
  else if (p_strWidgetType == C_iPOPLIST) {
    for(var i=0;i<p_objWidget.length;i++)
      if (p_objWidget.options[i].value == p_strValue) {
        p_objWidget.options[i].selected = true;
        break; 
      }
  }
  else if (p_strWidgetType == C_iCHECKBOX) {
    p_objWidget.checked = (p_strValue == C_strYES);
  }

}

// Populates the DFlex HTML objects with the values in p_arrDFlex
function fPopulateDFlex(p_framTarget, p_strContext, p_arrDFlex, p_iDayOfWeek) {
  var objContext, objSegments;
  var formTarget = p_framTarget.document.formDetailBodyContent;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++){
    fPopulateSegment(fGetDFlexWidget(formTarget, i, p_iDayOfWeek),
                     objSegments.arrSegment[i].mGetiObjectType(), 
                     mobjGetDFlexValue(p_arrDFlex[i]));
  }
}

function mobjGetDFlexValue(p_SegmentValue){
var undefined;

  if (p_SegmentValue == undefined) 
    return '';
  else 
    return p_SegmentValue;
}

// Populates the DFlex HTML objects with the values in p_arrDFlex
function fDisplayDFlex(p_framTarget, p_strContext, p_arrDFlex) {
  var objContext, objSegments;
  var docT = p_framTarget.document;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();

  if (objSegments.arrSegment.length == 0) {
    docT.writeln(fFormatString(''));
  }
  else {
    for(var i=0; i<objSegments.arrSegment.length; i++) {

      if (objSegments.arrSegment[i].mGetiObjectType() == C_iPOPLIST) {
        docT.writeln(objSegments.arrSegment[i].mGetstrPrompt(), ' = ',
                   objSegments.arrSegment[i].mGetobjPoplistValues().mobjFindValue(p_arrDFlex[i]));
      }
      else if (objSegments.arrSegment[i].mGetiObjectType() == C_iCHECKBOX) {
        var l_objLookupType, l_objLookupCode, l_strLookupCodeDesc;

        l_strLookupCodeDesc = p_arrDFlex[i];
        if (((l_objLookupType = g_objLookupTypes.mobjGetLookupType("YES_NO")) != 
            "") &&
           ((l_objLookupCode = l_objLookupType.mobjGetLookupCode(p_arrDFlex[i])) != "")) {
	  l_strLookupCodeDesc = l_objLookupCode.mGetstrDesc();
        }

        docT.writeln(objSegments.arrSegment[i].mGetstrPrompt(), ' = ',
	  l_strLookupCodeDesc);                   
      }
      else if(objSegments.arrSegment[i].mGetiObjectType() == C_iHIDDEN) {
        continue;
      }
      else {
        docT.writeln(objSegments.arrSegment[i].mGetstrPrompt(), ' = ',
                   p_arrDFlex[i]);
      }

      docT.writeln('<BR>');
    }
  }
}

function fstrReadSegment(p_objWidget, p_strWidgetType) {

  if (p_strWidgetType == C_iPOPLIST) {
    return p_objWidget.options[p_objWidget.selectedIndex].value;
  }
  else if (p_strWidgetType == C_iCHECKBOX) {
//    alert('checkbox ' + p_objWidget.checked);
    if (p_objWidget.checked) 
      return C_strYES;
    else
      return C_strNO;
  }
  else
    return p_objWidget.value;
}

function fReadDFlex(p_formSrc, p_strContext, p_arrDFlex, p_iDayOfWeek) {
  var objContext, objSegments;
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();

  for(var i=0; i<objSegments.arrSegment.length; i++) {
    p_arrDFlex[i] =
      fstrReadSegment(fGetDFlexWidget(p_formSrc, i, p_iDayOfWeek),
          objSegments.arrSegment[i].mGetiObjectType());
  }
}

function fValidateSegment(p_objSegment, p_strValue, p_objErrors, p_iLine, p_dDate) {
  var undefined;
  var C_strEMPTY_STRING = "";
  var l_strValue = p_strValue;

  if (l_strValue==undefined) {
    l_strValue = C_strEMPTY_STRING;
  }

  if ((p_objSegment.mGetbIsRequired()) && (l_strValue == C_strEMPTY_STRING)) {
    var strErrorMsg = g_objFNDMsg.mstrGetMsg("PA_WEB_DFLEX_REQUIRED");

    strErrorMsg = fstrReplToken(strErrorMsg, "&SEGMENT", p_objSegment.mGetstrPrompt());
    p_objErrors.mobjAddError(p_iLine, p_dDate, strErrorMsg);
  }

  if ((p_objSegment.mGetbIsNumericOnly()) && (!fIsNumericOnly(l_strValue))) {
    var strErrorMsg = g_objFNDMsg.mstrGetMsg("PA_WEB_DFLEX_NUMERIC_ONLY");

    strErrorMsg = fstrReplToken(strErrorMsg, "&SEGMENT", p_objSegment.mGetstrPrompt());
    p_objErrors.mobjAddError(p_iLine, p_dDate, strErrorMsg);
  }

  if ((p_objSegment.mGetbIsUpperCaseOnly()) && (!fIsUpperCaseOnly(l_strValue))) {
    var strErrorMsg = g_objFNDMsg.mstrGetMsg("PA_WEB_DFLEX_UPPER_CASE_ONLY");

    strErrorMsg = fstrReplToken(strErrorMsg, "&SEGMENT", p_objSegment.mGetstrPrompt());
    p_objErrors.mobjAddError(p_iLine, p_dDate, strErrorMsg);
  }
}

function fValidateDFlex(p_strContext, p_arrDFlex, p_objErrors, p_iLine, p_dDate) {
  var objContext, objSegments;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) {
    fValidateSegment(objSegments.arrSegment[i], p_arrDFlex[i], p_objErrors, p_iLine, p_dDate);
  }
}

function fClearDFlex(p_formTarget, p_strContext, p_iDayOfWeek) {
  var objContext, objSegments;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) {
    fPopulateSegment(fGetDFlexWidget(p_formTarget, i, p_iDayOfWeek),
                     objSegments.arrSegment[i].mGetiObjectType(), 
                     "");
  }
}

function fClearCSDFlex(p_arrDFlex) {
  for(var i=g_iNumGlobalSeg; i<p_arrDFlex.length; i++) {
    p_arrDFlex[i] = "";
  }
}

function fClearDFlexArray(p_arrDFlex) {
  for (var i=0; i<g_iNumGlobalSeg+g_iNumMaxContSensSeg; i++) {
    p_arrDFlex[i] = "";
  }
}

function fbIsClearDFlexArray(p_arrDFlex) {
  for (var i=0; i<p_arrDFlex.length; i++) {
    if (p_arrDFlex[i] != "")
      return false;
  }
  return true;
}

function fInitDFlex(p_strContext, p_arrDFlex) {
  var objContext, objSegments;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) {
    p_arrDFlex[i] = objSegments.arrSegment[i].mGetstrDefaultValue();
  }

  // clear out context sensitive segments
  for(var i=objSegments.arrSegment.length; i<g_iNumGlobalSeg+g_iNumMaxContSensSeg; i++) {
    p_arrDFlex[i] = "";
  }
}

function fMapDFlexJSToPLSQL(p_strContext, p_arrSource) {
  
  var objContext, objSegments;
  var arrTemp = new Array();

  // Clear out array
  for(var i=0; i<=C_iNUMMAXDFLEXSEG; i++) 
    arrTemp[i] = "";

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) 
    arrTemp[objSegments.arrSegment[i].mGetiColumn()] = p_arrSource[i];

  // Copy to p_arrSource
  for(var i=0; i<=C_iNUMMAXDFLEXSEG; i++) 
    p_arrSource[i] = arrTemp[i];
}

function fMapDFlexPLSQLToJS(p_strContext, p_arrSource) {
  var objContext, objSegments;
  var arrTemp = new Array();
  var undefined;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) {
    arrTemp[i] = p_arrSource[objSegments.arrSegment[i].mGetiColumn()];
    if (arrTemp[i] == undefined)
      arrTemp[i] = "";
  }

  // Copy temp to p_arrSource
  for(var i=0; i<arrTemp.length; i++)
    p_arrSource[i] = arrTemp[i];

  // Clear out rest of p_arrSource
  for(var i=arrTemp.length; i<Math.max(g_iNumGlobalSeg+g_iNumMaxContSensSeg, p_arrSource.length); i++) {
    p_arrSource[i] = "";
  }

}


function fbIsDFlexUsed() {
  return (g_iNumGlobalSeg+g_iNumMaxContSensSeg > 0);
}

function fbIsDFlexVisible() {
  for (var i in g_objContexts.arrContext) {
    var objSegments = g_objContexts.mobjGetContext(i).mGetobjSegments();
    for (var j=0; j<objSegments.arrSegment.length; j++) {
      if (objSegments.arrSegment[j].mGetiObjectType() != C_iHIDDEN)
        return true;
    }
  }
  return false;
}

function fPaintSegment(p_objSegment, p_framTarget, p_iIndex, p_iDayOfWeek, p_bDisable) {
  var iType;
  var docT;
  var strPrompt, strWidget;
  var l_strOnFocus = "";
  
  iType = p_objSegment.mGetiObjectType();
  docT = p_framTarget.document;

  if (p_bDisable) {
    l_strOnFocus = " onFocus='this.blur();'";
  }

  if (iType == C_iTEXTBOX) {
    strPrompt = p_objSegment.mGetstrPrompt();
    strWidget = "<INPUT TYPE=TEXT LENGTH=" + p_objSegment.mGetiTextBoxLength() + 
                " NAME=" + fGetDFlexWidgetName(p_iIndex, p_iDayOfWeek) + l_strOnFocus +  
		" onChange='javascript:" + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 
		"fOnChangeDFF(top.framMainBody.framBodyContent.document.formDetailBodyContent," + 
		g_iCurrLine +  "," + p_iDayOfWeek + ', ' + iType + ', ' + p_iIndex + ");' >";
    docT.writeln("<TR><TD width=35%></TD><TD>" + strPrompt + "</TD><TD>" + strWidget + "</TD></TR>");
  }
  else if (iType == C_iHIDDEN) {
    strWidget = "<INPUT TYPE=HIDDEN NAME=" + fGetDFlexWidgetName(p_iIndex, p_iDayOfWeek) + " >";
    docT.writeln(strWidget);
  }
  else if (iType == C_iPOPLIST) {
    var objValues, objValue;

    strPrompt = p_objSegment.mGetstrPrompt();
    strWidget = "<SELECT NAME=" + fGetDFlexWidgetName(p_iIndex, p_iDayOfWeek) +  l_strOnFocus +
                " onChange='javascript:" + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() + 
                "fOnChangeDFF(top.framMainBody.framBodyContent.document.formDetailBodyContent," + 
                g_iCurrLine +  "," + p_iDayOfWeek + ', ' + iType + ', ' + p_iIndex + ");' >";
    docT.writeln("<TR><TD width=35%></TD><TD>" + strPrompt + "</TD><TD>" + strWidget);
    docT.writeln("<OPTION VALUE=\"\"></OPTION>");

    objValues = p_objSegment.mGetobjPoplistValues();
    for(var i=0; i<objValues.miGetNumOfValues(); i++) {
      objValue = objValues.arrValue[i];
      docT.writeln("<OPTION VALUE=\"" + objValue.mGetstrCode() + "\">" + objValue.mGetstrDesc() + "</OPTION>");
    }
    docT.writeln("</SELECT></TD></TR>");
  }
  else if (iType == C_iCHECKBOX) {
    strPrompt = p_objSegment.mGetstrPrompt();
    strWidget = "<INPUT TYPE=CHECKBOX NAME=" + fGetDFlexWidgetName(p_iIndex, p_iDayOfWeek) +  l_strOnFocus +
                " onClick='javascript:" + g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() +
                "fOnChangeDFF(top.framMainBody.framBodyContent.document.formDetailBodyContent," +
                g_iCurrLine +  "," + p_iDayOfWeek + ', ' + iType + ', ' + p_iIndex + ");' >";

    docT.writeln("<TR><TD width=35%></TD><TD></TD><TD>" + strWidget + strPrompt + "</TD></TR>");
  }
}

function fPaintDFlex(p_strContext, p_framTarget, p_iDayOfWeek, p_bDisable) {
  var objContext, objSegments;

  // populate context sensitive one
  objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
  objSegments = objContext.mGetobjSegments();
  for(var i=0; i<objSegments.arrSegment.length; i++) {  
    fPaintSegment(objSegments.arrSegment[i], p_framTarget, i, p_iDayOfWeek, p_bDisable);
  }  
}

function fSerializeDFlex(p_strContext, p_arrDFlex) {

  var l_arrTemp = new Array();
  var l_strSerializeString = new String();

  var C_strFIELD_DELIMITER = "@att@";

  for(var m=0; m<p_arrDFlex.length; m++) {
    l_arrTemp[m] = p_arrDFlex[m];
  }

  for(var m=p_arrDFlex.length; m<g_iNumGlobalSeg+g_iNumMaxContSensSeg; m++) {
    l_arrTemp[m] = "";
  }
  fMapDFlexJSToPLSQL(p_strContext, l_arrTemp);
  for(var k=1; k<=C_iNUMMAXDFLEXSEG; k++) {
    if (l_arrTemp[k] != "")
      l_strSerializeString += "@D" + k + "@" + l_arrTemp[k] + C_strFIELD_DELIMITER;
  }

  return l_strSerializeString;
}

function fCopyDFlex(p_arrDFlexSource, p_arrDFlexTarget) {

  // Copy DFlex fields
  for(var i=0; i<p_arrDFlexSource.length; i++)
    p_arrDFlexTarget[i] = p_arrDFlexSource[i];

  // Clear out remaining fields
  for(var i=p_arrDFlexSource.length; i<p_arrDFlexTarget.length; i++)
    p_arrDFlexTarget[i] = "";
}

function fIsDFlexArrayInitialized(p_arrDFlex) {
  return (p_arrDFlex.length != 0);
}

function fbIsSegmentChanged(p_formSrc, p_iIndex, p_objCurLine, p_iDayOfWeek) {
  var objContext, objSegments, l_widget;
 
  // populate context sensitive one
  objContext = 
    g_objContexts.mobjGetContextWithDefault(fGetContSensField(p_objCurLine));
  objSegments = objContext.mGetobjSegments();

  l_widget = fGetDFlexWidget(p_formSrc, p_iIndex, p_iDayOfWeek);

  return (fstrReadSegment(l_widget,
                      objSegments.arrSegment[p_iIndex].mGetiObjectType()) !=
          p_objCurLine.mGetobjDays().arrDay[p_iDayOfWeek].arrDFlex[p_iIndex]);

}

function fbIsDFlexChanged(p_formSrc, p_objCurLine) {
  var objContext, objSegments;

  // populate context sensitive one
  objContext = 
    g_objContexts.mobjGetContextWithDefault(fGetContSensField(p_objCurLine));
  objSegments = objContext.mGetobjSegments();

  for(var j=C_iSUNDAY; j<=C_iSATURDAY; j++){
    if(g_objPref.mGetobjWorkDays().bIsWorkDay(j) == true || g_bHourNotOnWorkDay){
      for(var i=0; i<objSegments.arrSegment.length; i++) {
        if (fbIsSegmentChanged(p_formSrc, i, p_objCurLine, j))
          return true;
      }
    }
  }
  return false;
}

function fDetermineDFlexChanged() {
  var l_objCurrLine;
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_formDetailBodyContent = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;

  l_objCurrLine = g_objTempLines.mobjFindLine(g_iPrevLineInDetail);

  //Save it to the data structure everytime 
  if ( (fbIsDFlexUsed()) && (fbIsDFlexChanged(l_formDetailBodyContent, l_objCurrLine)) ){
    g_bDetailDFlexModified = true;
    for (var j=C_iSUNDAY; j<=C_iSATURDAY; j++){
      if (g_objPref.mGetobjWorkDays().bIsWorkDay(j) == true || g_bHourNotOnWorkDay){
        fReadDFlex(l_formDetailBodyContent, fGetContSensField(l_objCurrLine), l_objCurrLine.mGetobjDays().arrDay[j].arrDFlex, j)
      }
    }
  }
}

//  --------------------------------------------------
//  Function: fOnChangeDFF
//  Description:
//
//  --------------------------------------------------
function fOnChangeDFF(p_formDisplay, p_iCurrLine, p_iDayOfWeek, p_iType, p_iIndex){
  var undefined;
  var l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();
  var l_objCurrLine   = g_objTempLines.mobjFindLine(g_iCurrLineInDetail);
  var l_formDetailBodyContent = l_winDetail.framMainBody.framBodyContent.document.formDetailBodyContent;

  if ((l_objCurrLine.mGetobjDays().arrDay[p_iDayOfWeek].mGetnHours() == undefined) ||
	( l_objCurrLine.mGetobjDays().arrDay[p_iDayOfWeek].mGetnHours() == '')) {
      l_winDetail.alert(g_objFNDMsg.mstrGetMsg("PA_WEB_HOURS_REQUIRED"));
    if (p_iType == top.C_iTEXTBOX)
      fGetDFlexWidget(p_formDisplay, p_iIndex, p_iDayOfWeek).value  = '';
    if (p_iType == top.C_iPOPLIST)
      fGetDFlexWidget(p_formDisplay, p_iIndex, p_iDayOfWeek).options[0].selected  = true;
    if (p_iType == top.C_iCHECKBOX) 
      fGetDFlexWidget(p_formDisplay, p_iIndex, p_iDayOfWeek).checked  = false;
  }
}
