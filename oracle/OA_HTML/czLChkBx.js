/* $Header: czLChkBx.js 115.10 2001/06/14 15:34:08 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |       xxxx-xx-99  CK Jeyaprakash  fixed event handler and reraising events|
 |                                                                           |
 +===========================================================================*/
 
function LogicCheckBox ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.enableKeyPressEvents();
  this.type = 'LogicCheckBox';
}

function LogicCheckBox_hideFeature(bHide)
{
  this.hFeature = bHide;
}

function LogicCheckBox_onLaunchCall()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.updateDone();
  }
}

function LogicCheckBox_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('state', 'icon', 0, 16, 20, 2, 0, 'top', 'left', true, false);
    this.columnDescriptors[1] = new ColumnDescriptor ('text', 'label', 1, 22, 50, 1, 2, 'top', 'left', true, true);
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function LogicCheckBox_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, wd, ht)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = wd;
  this.imageHeight = ht;
}

function LogicCheckBox_mouseoverCallback (e, eSrc)
{
  return true;
}

function LogicCheckBox_mouseoutCallback (e, eSrc)
{
  return true;
}

function LogicCheckBox_moreSetProperties (cell)
{
  if ((cell.colName == 'price') || (cell.colName == 'atp')) {
    cell.noclip = true;
    cell.addListener ('captionChangeCallback', this);
  }
}

function LogicCheckBox_captionChangeCallback (oldVal, cell)
{
  if (this.launched) { 			
    //resize the layer to the correct width and height;
    var wd = (ns4)? cell.block.clip.width : cell.block.scrollWidth;
    if (cell.width < wd) {
      var prefWd = wd + 5;
      cell.setWidth (prefWd);
      var colDesc = this.columnDescriptors[cell.index];
      colDesc.setPrefWidth (prefWd);
      this.updateColumnPosition ();
      this.fixDimensions ();
    }
  }
}

function LogicCheckBox_moreLaunch (em)
{
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].launch(em);
  }
  this.setModelItem (this.modelItem);
}

//Extend from ListItem
HTMLHelper.importPrototypes(LogicCheckBox, LogicItem);

// LogicItem overrides
LogicCheckBox.prototype.getColumnDescriptors = LogicCheckBox_getColumnDescriptors;
LogicCheckBox.prototype.setImages = LogicCheckBox_setImages;
LogicCheckBox.prototype.moreSetProperties = LogicCheckBox_moreSetProperties;
LogicCheckBox.prototype.moreLaunch = LogicCheckBox_moreLaunch;
LogicCheckBox.prototype.hideFeature = LogicCheckBox_hideFeature;
LogicCheckBox.prototype.onLaunchCall = LogicCheckBox_onLaunchCall;

//Event handlers
LogicCheckBox.prototype.mouseoverCallback = LogicCheckBox_mouseoverCallback;
LogicCheckBox.prototype.mouseoutCallback = LogicCheckBox_mouseoutCallback;
LogicCheckBox.prototype.captionChangeCallback = LogicCheckBox_captionChangeCallback;

