/* $Header: czBomItm.js 115.17 2001/07/06 09:09:09 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function BomItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'BomItem';
  this.bNoIcon = false;
  this.enableKeyBoardEvents();
  this.focCellInd = 1;
}

function BomItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  var ht = 0, wd = 0, pdgLt = 0, pdgTp = 0;
  if (this.columnDescriptors.length == 0) { 
    var index = 0; 
    // params - name, type, index, ht, wd, paddingLeft, paddingTop, valign, halign, defvis, spring;
    if (! this.bNoIcon) {
      wd = 20;
      pdgLt = 2;
      if (this._3DLook) {
        ht = 25;
        pdgTp = 5;
      } else if (this._flatLook) {
        ht = 16;
        pdgTp = 5;
      } else {
        ht = 16;
        pdgTp = 0;
      }
      this.columnDescriptors[index] = new ColumnDescriptor('state', 'icon', index, ht, wd, pdgLt, pdgTp, 'top', 'left', true, false);
      index++;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('count', 'inputtext', index, 22, 50, 1, 0, 'top', 'left', true, false);
    index++;

    ht = 25;
    wd = 50;
    pdgLt = 1;
    if (this._3DLook) {
      pdgTp = 2;
    } else if (this._flatLook) {
      pdgTp = 2;
    } else {
      pdgTp = 4;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, ht, wd, pdgLt, pdgTp, 'top', 'left', true, true);
    index++;

    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 10, pdgLt, pdgTp, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function BomItem_mouseoverCallback (e, eSrc)
{
  return true;
}

function BomItem_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function BomItem_updateDone()
{
  if(this.hFeature)
      this.hide();
  else
      this.show();
}

function BomItem_onLaunchCall()
{
  this.updateDone();
}

function BomItem_mouseoutCallback (e, eSrc)
{
  return true;
}

function BomItem_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, wd, ht)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = wd;
  this.imageHeight = ht;
}

function BomItem_moreCSS ()
{
  var CSSStr = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  this.setModelItem (this.modelItem);
  this.updatePreferredWidth ();
  this.updateColumnPosition();
  this.fixDimensions();

  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    CSSStr += this.cells[i].generateCSS() + '\n';
  }
  if (this.unSatIcon) {
    CSSStr += this.unSatIcon.generateCSS() + '\n';
  }
  if (this.bNoIcon) {
    this.createTopLayer();
    CSSStr += this.topLay.generateCSS();
  }
  return CSSStr;
}

//will be set only when it is 3D look
function BomItem_setBorderColor (col)
{
  if (! (this._3DLook || this._flatLook))
    return;

  if (col != null) {
    this.backgroundColor = col;
    
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
  } 
}

function BomItem_setBackgroundColor (col)
{
  if(col != null) {
    this.cellBgColor = col;
    if (! this._3DLook) {
      this.backgroundColor = col;
      if(this.launched) 
        HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
    }
    if (this.launched) {
      for (var i=0; i<this.cells.length; i++) {
        this.cells[i].setBackgroundColor (col);
      }
    }
  }
}

function BomItem_getBackgroundColor (col)
{
  return (this.itemBgColor);
}

function BomItem_updatePreferredWidth ()
{
  // find the preferred pixel dimensions;
  var colDesc = null;
  var prefWidth = 0;
  var prefHeight = 0;
  var charWidth, charHeight;
  
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  var len = this.columnDescriptors.length;
  for (var i = 0; i < len; i++) {
    colDesc = this.columnDescriptors[i];
    if ((colDesc.name == 'state') || (colDesc.name == 'text'))
      continue;

    if (colDesc.name == 'atp') {
      prefWidth = 11 * charWidth;
      prefHeight = charHeight; 
    } else if (colDesc.name == 'price') {
      prefWidth = 6 * charWidth;
      prefHeight = charHeight;
    } else if (colDesc.name == 'count') {
      prefWidth = 5 * charWidth;
      prefHeight = charHeight;
    }
    colDesc.setPrefWidth (prefWidth);
    colDesc.setPrefHeight (prefHeight);
  }
}

function BomItem_getMaxNumFocusCells()
{
  this.numFocus = 2;
  return this.numFocus;
}

function BomItem_onKeyDownCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 9:
    if (e.shiftKey) {
      //move focus backwards
      if (this.focCellInd == 1) {
        this.eventManager.focusBackward(e, this);
        return;
      } else {
        this.focCellInd--;
        this.setFocus(null);
      }
    } else {
      //move focus forwards
      if (this.focCellInd == 2) {
        this.eventManager.focusForward(e, this);
        this.focCellInd = 1;
        return;
      } else {
        this.focCellInd++;
        this.setFocus(this.focCellInd);
      }
    }
    e.returnValue = false;
    break;
  case 32:
    this.mouseupCallback (e, eSrc);
    e.returnValue = false;
    break;
  }
  return true;
}

//Extend from ListItem
HTMLHelper.importPrototypes(BomItem, LogicItem);

// class methods
BomItem.getPreferredColumnDescriptors = BomItem.getPreferredColumnDescriptors;

// overrides
BomItem.prototype.setBorderColor = BomItem_setBorderColor;
BomItem.prototype.setBackgroundColor = BomItem_setBackgroundColor;
BomItem.prototype.getBackgroundColor = BomItem_getBackgroundColor;
BomItem.prototype.getColumnDescriptors = BomItem_getColumnDescriptors;
BomItem.prototype.setImages = BomItem_setImages;
BomItem.prototype.moreCSS = BomItem_moreCSS;
BomItem.prototype.hideFeature = BomItem_hideFeature;
BomItem.prototype.updateDone = BomItem_updateDone;
BomItem.prototype.onLaunchCall = BomItem_onLaunchCall;
BomItem.prototype.updatePreferredWidth = BomItem_updatePreferredWidth;
BomItem.prototype.getMaxNumFocusCells = BomItem_getMaxNumFocusCells;

//Event handlers
BomItem.prototype.mouseoverCallback = BomItem_mouseoverCallback;
BomItem.prototype.mouseoutCallback = BomItem_mouseoutCallback;
BomItem.prototype.onKeyDownCallback = BomItem_onKeyDownCallback;
