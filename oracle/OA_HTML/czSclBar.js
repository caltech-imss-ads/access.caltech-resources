/* $Header: czSclBar.js 115.13 2001/06/14 15:34:22 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function ScrollBar()
{
  this.parentConstructor = Container;
  this.parentConstructor();

  this.backgroundColor = 'lightgrey';
  this.dir ='v';
  this.curValue = 0;
  this.minValue = 0;
  this.maxValue = 100;
  this.range = 100;
  this.cornerLength = 0;
  this.useSizedBar = false;
  this.arrow1 = null;
  this.arrow2 = null;
  this.bar = null;
  this.box = null;
  
  this.cycle = 1;
  this.currSec = 1;
  this.enable = true;
  this.step = 1;
  this.page = 10;	
  this.scrollPercentage = 0;
}

function ScrollBar_drawCorner(length)
{
  //where 'length' is generally the thickness of the opposing scrollbar;
  if (length > 0) {
    this.cornerLength = length;
  } else {
    this.cornerLength = 0;
  }
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);	
}

function ScrollBar_getAdjustedLength()
{
  if (this.dir == 'h') {
    return this.width - this.cornerLength;
  } else {
    return this.height - this.cornerLength;
  }
}
function ScrollBar_updateDimensions()
{
  if (this.built) {
    if (this.dir == 'h') {
      this.box.setLeft (this.height);
      this.box.setTop (0);
      this.box.setHeight (this.height);
      this.box.setWidth (this.getAdjustedLength()  - 2*this.height);
      this.bar.setLeft (this.arrow1.width);
      this.arrow1.setWidth (this.height);
      this.arrow2.setLeft (this.getAdjustedLength() - this.arrow2.width);
      this.bardrag.lockRegion(this.bar,0,this.box.width,this.box.height,0);
    } else {
      this.box.setTop (this.width);
      this.box.setLeft (0);
      this.box.setHeight (this.getAdjustedLength() - 2*this.width);
      this.box.setWidth (this.width);
      this.bar.setTop (this.arrow1.height);
      this.arrow1.setWidth (this.width);
      this.arrow2.setTop (this.getAdjustedLength() - this.arrow2.height);
      this.bardrag.lockRegion(this.bar,0,this.box.width,this.box.height,0);
    }
  }
}

function ScrollBar_attachTo(content)
{
  this.content = content;
}

function ScrollBar_reset()
{
  this.setValue(0);
}

function ScrollBar_setValues(v, min, max, step, page, bRiseEvent)
{
  this.curValue = v;
  this.minValue = min;
  this.maxValue = max;
  this.step = step;
  this.page = page;
  
  //the range is the absolute magnitude of min and max;
  var range = Math.abs(this.maxValue - this.minValue);
  if (range != this.range) {
    this.range = range;
  }
  
  //round to the nearest step;
  this.curValue = (v) - (v % this.step);
  this.scrollPercentage = (this.curValue / this.range);

  //position the scroll 'bar';
  if (this.bar) {
    if (this.dir == 'h') {
      this.bar.setLeft(this.scrollPercentage *(this.box.width-this.bar.width));
    } else {
      this.bar.setTop(this.scrollPercentage *(this.box.height-this.bar.height));
    }
  }
  if (bRiseEvent) {
    this.notifyListeners('scrollbarCallback', this);
  }
}

function ScrollBar_setMinValue(v)
{
  this.setValues(this.curValue, v, this.maxValue, this.step, this.page);
}

function ScrollBar_getMinValue()
{
  return this.minValue;
}

function ScrollBar_setMaxValue(v)
{
  if (v <= 0) {
    v = 1;
  }
  this.setValues(this.curValue, this.minValue, v, this.step, this.page);
}

function ScrollBar_getMaxValue()
{
  return this.maxValue;
}

function ScrollBar_setValue(v)
{
  this.setValues(v, this.minValue, this.maxValue, this.step, this.page, true);
}

function ScrollBar_getValue()
{
  return this.curValue;
}

function ScrollBar_setStepSize(v)
{
  this.setValues(this.curValue, this.minValue, this.maxValue, v, this.page);
}

function ScrollBar_getStepSize()
{
  return this.step;
}

function ScrollBar_setPageSize(v)
{
  this.setValues(this.curValue, this.minValue, this.maxValue, this.step, v);
}

function ScrollBar_getPageSize()
{
  return this.page;
}

function ScrollBar_buildControl ()
{
  if(this.built) return;
  else this.built = true;

  var arrow1,arrow2,bar,box;
  
  if(this.dir) {
    if(this.dir == 'h') {
      //left arrow;
      if(this.Arrow1ONImage == null)
	this.Arrow1ONImage = IMAGESPATH +'czLSArr.gif';
      if(this.Arrow1OFFImage == null)
        this.Arrow1OFFImage = IMAGESPATH +'czLSArr1.gif';
      arrow1 = new ImageButton();
      arrow1.setName (this.name+'xHArw1');
      arrow1.setDimensions (0, 0, this.height, this.height);
      arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);
      arrow1.stretch = true;
      arrow1.what = 'HArrow1';
      
      //right arrow;
      if(this.Arrow2ONImage == null)
        this.Arrow2ONImage = IMAGESPATH + 'czRSArr.gif';
      if(this.Arrow2OFFImage == null)
        this.Arrow2OFFImage = IMAGESPATH + 'czRSArr1.gif';
      arrow2 = new ImageButton();
      arrow2.setName (this.name+'xHArw2');
      arrow2.setDimensions (this.width-this.height, 0, this.height, this.height);
      arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);
      arrow2.stretch = true;
      arrow2.what = 'HArrow2';
      
      //horizontal box;
      box = new Container();
      box.setName (this.name+'xHBox');
      box.setDimensions (this.height, 0, this.width-this.height*2, this.height);
      box.setBackgroundColor ("#E0E0E0");
      
      //horizontal bar inside the box;
      if(this.BarONImage == null)
        this.BarONImage = IMAGESPATH + 'czHBar.gif';
      bar = new ImageButton();
      bar.setName (this.name+'xHBar');
      bar.setDimensions (0, 0, this.height*2, this.height);
      bar.setImages(this.BarONImage,this.BarOFFImage);
      bar.stretch = true;
      bar.what = 'HBar';
    } else {
      //add a up-arrow;
      if(this.Arrow1ONImage == null)
        this.Arrow1ONImage = IMAGESPATH +'czUSArr.gif';
      if(this.Arrow1OFFImage == null)
        this.Arrow1OFFImage = IMAGESPATH +'czUSArr1.gif';  
      arrow1 = new ImageButton();
      arrow1.setName (this.name+'xVArw1');
      arrow1.setDimensions (0, 0, this.width, this.width);
      arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);
      arrow1.stretch = true;
      arrow1.what = 'VArrow1';
      
      //add a down-arrow;
      if(this.Arrow2ONImage == null)
        this.Arrow2ONImage = IMAGESPATH + 'czDSArr.gif';
      if(this.Arrow2OFFImage == null)
        this.Arrow2OFFImage = IMAGESPATH + 'czDSArr1.gif';
      arrow2 = new ImageButton();
      arrow2.setName (this.name+'xVArw2');
      arrow2.setDimensions (0, this.height-this.width, this.width, this.width);
      arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);
      arrow2.stretch = true;
      arrow2.what = 'VArrow2';
      
      //add a vertical box;
      box = new Container();
      box.setName (this.name+'xVBox');
      box.setDimensions (0, this.width, this.width, this.height-2*this.width);
      box.setBackgroundColor ("#E0E0E0");
      
      //add a vertival bar inside the box;
      if(this.BarONImage == null)
        this.BarONImage = IMAGESPATH +'czVBar.gif';
      bar = new ImageButton();
      bar.setName (this.name+'xVBar');
      bar.setDimensions (0, 0, this.width, this.width*2);
      bar.setImages(this.BarONImage,this.BarOFFImage);
      bar.stretch = true;
      bar.what = 'VBar';
      if (this.dir == null) this.dir = 'v';
    }
  }
  arrow1.addListener('mousedownCallback',Arrow_MouseDownCallback);
  arrow2.addListener('mousedownCallback',Arrow_MouseDownCallback);
  arrow1.addListener('mouseupCallback',Arrow_MouseUpCallback);
  arrow2.addListener('mouseupCallback',Arrow_MouseUpCallback);

  arrow1.addListener('mouseoutCallback',Arrow_MouseOutCallback);
  arrow2.addListener('mouseoutCallback',Arrow_MouseOutCallback);
  arrow1.addListener('mousemoveCallback', Arrow_MouseMoveCallback);
  arrow2.addListener('mousemoveCallback', Arrow_MouseMoveCallback);

  box.addListener('mousedownCallback',ScrollBox_MouseDownCallback);
  box.addListener('mouseupCallback',ScrollBox_MouseUpCallback);
  box.addListener('mouseoutCallback',ScrollBox_MouseOutCallback);

  var bardrag = new Drag('BarDrag', this.eventManager);
  bardrag.add(bar);
  bardrag.lockRegion(bar,0,box.width,box.height,0);
  bardrag.on();
  this.bardrag = bardrag;
  
  bar.resizable =false;
  //KAM bar.slideInit();
  
  bar.addListener('onDragging', ScrollBar_draggingCallback);
  bar.addListener('onDragStart', ScrollBar_dragStartCallback);
  bar.addListener('onDragEnd', ScrollBar_dragEndCallback);

  this.add(box,arrow1,arrow2);
  box.add(bar);
  this.bar = bar;
  this.box = box;
  this.arrow1 = arrow1;
  this.arrow2 = arrow2;
  this.bar.addListener('onSlide',Bar_onSlide);

  this.updateDimensions ();
  this.setStepSize(this.step);	
}

function ScrollBar_scrollSteps(steps)
{
  this.setValue(this.curValue + this.step * steps);
}

function ScrollBar_setImages(a1On,a1Off,a2On,a2Off,barOn,barOff,boxBG)
{
  if(a1On) 
    this.Arrow1ONImage = a1On;
  if(a1Off) 
    this.Arrow1OFFImage = a1Off;
  this.arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);

  if(a2On) 
    this.Arrow2ONImage = a2On;
  if(a2Off) 
    this.Arrow2OFFImage = a2Off;
  this.arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);

  if(barOn) 
    this.BarONImage = barOn;
  if(barOff) 
    this.BarOFFImage = barOff;
  this.bar.setImages(this.BarONImage,this.BarOFFImage);

  if(boxBG) 
    this.BoxBGImage = boxBG;
}

function ScrollBar_setCycle(numCycle)
{
  if(numCycle) 
    this.cycle = numCycle;
}

function ScrollBar_getCycle()
{
  return this.cycle;
}

function ScrollBar_getScrollPercentage()
{
  return this.scrollPercentage;
}

function ScrollBar_setScrollPercentage(perc)
{	
  var scrVal=0;

  if(perc > 1)  
    perc =1;
  if(perc < 0) 
    perc = 0;

  if(perc == 1) {
    scrVal = this.maxValue;
  } else if(this.range) {
    scrVal = (perc*(this.range) + this.minValue);
  }
  
  this.setValue(scrVal);
}		

function ScrollBar_moreSetHeight(ht)
{
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);
}

function ScrollBar_moreSetWidth(wd)
{
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);
}

function ScrollBar_boundedValue(value) {
  if (value > this.maxValue) {
    return this.maxValue;  
  } else if (value < this.minValue) { 
    return this.minValue;
  } else {
    return value;
  }
}

function ScrollBar_setEventManager (em)
{
  this.eventManager = em;
}

function ScrollBar_scrollDown()
{
  var retVal = true;
  var boundVal = this.boundedValue (this.curValue+1);
  if (boundVal != (this.curValue+1))
    retVal = false;

  this.setValue(boundVal);

  return (retVal);
}

function ScrollBar_scrollUp()
{
  var retVal = true;
  var boundVal = this.boundedValue (this.curValue-1);
  if (boundVal != (this.curValue-1))
    retVal = false;
  
  this.setValue(boundVal);
  
  return (retVal);
}

//construction
HTMLHelper.importPrototypes(ScrollBar,Container);

ScrollBar.prototype.constructor 	= ScrollBar;

//public methods
ScrollBar.prototype.setMinValue = ScrollBar_setMinValue;
ScrollBar.prototype.setMaxValue = ScrollBar_setMaxValue;
ScrollBar.prototype.getMinValue = ScrollBar_getMinValue;
ScrollBar.prototype.getMaxValue = ScrollBar_getMaxValue;
ScrollBar.prototype.setValue = ScrollBar_setValue;
ScrollBar.prototype.getValue = ScrollBar_getValue;
ScrollBar.prototype.setImages = ScrollBar_setImages;
ScrollBar.prototype.setStepSize = ScrollBar_setStepSize;
ScrollBar.prototype.getStepSize = ScrollBar_getStepSize;
ScrollBar.prototype.setPageSize = ScrollBar_setPageSize;
ScrollBar.prototype.getPageSize = ScrollBar_getPageSize;
ScrollBar.prototype.reset = ScrollBar_reset;
ScrollBar.prototype.setScrollPercentage = ScrollBar_setScrollPercentage;
ScrollBar.prototype.getScrollPercentage = ScrollBar_getScrollPercentage;
ScrollBar.prototype.setCycle = ScrollBar_setCycle;
ScrollBar.prototype.getCycle = ScrollBar_getCycle;
ScrollBar.prototype.moreSetHeight = ScrollBar_moreSetHeight;
ScrollBar.prototype.moreSetWidth = ScrollBar_moreSetWidth;
ScrollBar.prototype.drawCorner = ScrollBar_drawCorner;
ScrollBar.prototype.setEventManager = ScrollBar_setEventManager;

//private methods
ScrollBar.prototype.buildControl = ScrollBar_buildControl;
ScrollBar.prototype.setValues = ScrollBar_setValues;
ScrollBar.prototype.scrollSteps = ScrollBar_scrollSteps;
ScrollBar.prototype.updateDimensions = ScrollBar_updateDimensions;
ScrollBar.prototype.getAdjustedLength = ScrollBar_getAdjustedLength;
ScrollBar.prototype.boundedValue = ScrollBar_boundedValue;

ScrollBar.prototype.scrollDown = ScrollBar_scrollDown;
ScrollBar.prototype.scrollUp = ScrollBar_scrollUp;

/*---------------------------------------------
Arrow Callback functions
---------------------------------------------*/
function Arrow_MouseUpCallback(e,obj)
{
  if(obj.parentObj.bar != null )
    obj.parentObj.bar.slideStop();
  obj.toggle(0);
  self.recObj = null;
  clearTimeout(self.timeId);
  self.timeId = null;
  return true;
}

function Arrow_MouseMoveCallback(e,obj)
{
  if ( self.timeId && obj.eventManager.msdown == true){
    obj.toggle(0);
    self.recObj = null;
    clearTimeout(self.timeId);
    self.timeId = null;
  }
  return true;
}

function Arrow_MouseOutCallback(e,obj)
{
  if ( obj.eventManager.msdown == true && self.timeId ){
    obj.toggle(0);
    self.recObj = null;
    clearTimeout(self.timeId);
    self.timeId = null;
  }
  return true;
}

function Arrow_MouseDownCallback(e,obj)
{
  self.recObj = null;
  Arrow_recursiveMouseDown(obj);
}

function Arrow_recursiveMouseDown(obj)
{
  if ( self.recObj )
    obj = self.recObj;

  if (obj == null) {
    clearTimeout (self.timeId);
    return true;
  }
  var par = obj.parentObj;
  var box = par.box;
  var bar = par.bar;
  
  obj.toggle(1);

  if(obj.what=='HArrow1') {
    if(bar.left <=0) {
      if(par.onScrollEnd) 
        par.onScrollEnd(0);
    } else { 
      par.setValue(par.curValue - par.step);
    }
  }
  
  if(obj.what=='VArrow1') {
    if(bar.top <=0) {
      if(par.onScrollEnd) 
        par.onScrollEnd(0);
      if (par.curValue > par.minValue) {
        par.setValue (par.curValue - par.step);
      }
    } else 
      par.setValue(par.curValue - par.step);
  }
  
  if(obj.what=='HArrow2') {
    if(bar.left >=(box.width-bar.width)) {
      if(par.onScrollEnd) 
        par.onScrollEnd(1);
    }
    else 
      par.setValue(par.curValue + par.step);
  }
  
  if(obj.what=='VArrow2') {
    if(bar.top >= (box.height-bar.height)) {
      if(par.onScrollEnd) 
        par.onScrollEnd(1);
      if (par.curValue < par.maxValue) {
        par.setValue (par.curValue + par.step);
      }
    } else 
      par.setValue(par.curValue + par.step);
  }
  
  self.recObj = obj;
  self.timeId = setTimeout("Arrow_recursiveMouseDown()", 100);

  return true;
}

function Arrow_onKeyDownCallback (e, sclBar)
{
  self.recObj = null;
  var key = (ns4)? e.which :e.keyCode;
  switch (key) {
  case 37:
  case 38:
    Arrow_recursiveMouseDown(sclBar.arrow1);
    break;
  case 39:
  case 40:
    Arrow_recursiveMouseDown(sclBar.arrow2);
    break;
  }
  switch (key) {
  case 37:
  case 39:
  case 38:
  case 40:
    if (ie4) {
      e.cancelBubble = true;
      e.returnValue = false;
    }
  }
  return true;
}

function Arrow_onKeyUpCallback (e, sclBar)
{
  var key = e.keyCode;
  switch (key) {
  case 37:
  case 38:
    Arrow_MouseUpCallback(e, sclBar.arrow1);
    break;
  case 39:
  case 40:
    Arrow_MouseUpCallback(e, sclBar.arrow2);
    break;
  }
  switch (key) {
  case 37:
  case 39:
  case 38:
  case 40:
    if (ie4)
      e.returnValue = false;
    break;
  }
  return true;
}

/*
Callback function for bar sliding
*/

function Bar_onSlide(speed,obj)
{
  var perc;
  var SBW = obj.parentObj.parentObj;
  
  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);
  
  if(SBW.contents)
    SBW.setScrollPercentage(perc);

  if(SBW.onScrolling) 
    SBW.onScrolling(perc,this);

  if(speed >50)
    speed = speed -20;
  return speed;
}


function ScrollBar_draggingCallback(e,obj)
{
  /*var perc;
  var SBW = obj.parentObj.parentObj;

  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);

  SBW.scrollContentTo(perc,false);
  */
}

function ScrollBar_dragEndCallback(e,obj)
{
  var perc;
  var SBW = obj.parentObj.parentObj;

  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);

  obj.toggle(0);

  SBW.setScrollPercentage(perc);
  
  if(SBW.onScrolling) 
    SBW.onScrolling(perc,this);

  return true;
}

function ScrollBar_dragStartCallback(e,obj)
{
  obj.toggle(1);

  return true;
}

/*---------------------------------------------
ScrollBox callback functions
---------------------------------------------*/

function ScrollBox_MouseUpCallback(e,box)
{
  var bar = box.parentObj.bar;
  bar.slideStop();
  self.barObj = null;
  self.barE = null;
  self.x = null;
  self.y = null;
  self.scrollDir = null;

  clearTimeout(self.barTimeId);
  self.barTimeId = null;
}

function ScrollBox_MouseOutCallback(e,box)
{
  if ( box.eventManager.msdown == true && self.barTimeId ){
    var bar = box.parentObj.bar;
    bar.slideStop();
    self.barObj = null;
    self.barE = null;
    self.x = null;
    self.y = null;

    clearTimeout(self.barTimeId);
    self.barTimeId = null;

  }
}

function ScrollBox_MouseDownCallback(e,box)
{
   self.barObj = null;

   var x = box.lastX = ns4?e.layerX:e.offsetX;
   var y = box.lastY = ns4?e.layerY:e.offsetY;
   var par = box.parentObj;	
   var bar = par.bar;
   if ( par.dir == 'h' ){
     if ( x > bar.left )
       self.scrollDir = 'right';
     else
       self.scrollDir = 'left';
   }else {
     if ( y > bar.top )
       self.scrollDir = 'down';
     else
       self.scrollDir = 'up';
   }     
   ScrollBox_recursiveMouseDown(e,box);
}

function ScrollBox_recursiveMouseDown(e,box)
{
  //window.status = 'mouseup on ' + box.objId;
  if ( self.barObj && self.barE ){
    e = self.barE;
    box = self.barObj;
  }
if( e && box ){
  if ( self.x && self.y && ie4 ){
    x = self.x;
    y = self.y;
  }else {
    var x = box.lastX = ns4?e.layerX:e.offsetX;
    var y = box.lastY = ns4?e.layerY:e.offsetY;
    self.x = x;
    self.y = y;
  }

  var par = box.parentObj;	
  var bar = par.bar;

  if(par.dir =='h') {
    //window.status = x + ' ' + bar.width/2 + bar.left;
    if (x < bar.width/2 + bar.left && self.scrollDir == 'left') {
        par.setValue(par.boundedValue(par.curValue - par.page));
    } else if (x > bar.width/2 + bar.left && self.scrollDir == 'right') {
        par.setValue(par.boundedValue(par.curValue + par.page));
    }
  } else {
    if (y <  bar.height/2 + bar.top && self.scrollDir == 'up') {
      par.setValue(par.boundedValue(par.curValue - par.page));
    } else if( y >  bar.height/2 + bar.top && self.scrollDir == 'down'){ 
      par.setValue(par.boundedValue(par.curValue + par.page));
    }
  }
  self.barObj = box;
  self.barE = e;
}
  self.barTimeId = setTimeout("ScrollBox_recursiveMouseDown()",70);
}
