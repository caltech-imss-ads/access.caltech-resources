/*=================================================================+
|               Copyright (c) 2000 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================+
| FILENAME                                                         |
|   icxcore.js                                                     |
|                                                                  |
| HISTORY                                                          |
|   09-AUG-00 nlbarlow Created                                     |
|   19-APR-02 Hgandhi  Fix for Bug 2239664                         |
|                                                                  |
+==================================================================*/
/* $Header: icxcore.js 115.11.1157.2 2002/07/25 22:30:54 aviswana noship $ */

var g_group = new Array();
var g_integer = new Boolean(true)
function uscon(x) {
    var y = "";
    var decimal = x.length;
    var group = 0;

    for (var i = x.length; i >= 0; i--)
        if (x.charAt(i) == '.') {
            y = "." + y;
            decimal = i;
            g_integer = false;
        }
        else
            if (x.charAt(i) != ',')
                y = x.charAt(i) + y;
            else
                if (group++ == g_group.length)
                    g_group[group-1] = decimal - i;

    return y;
};

function usdecon(x) {
    var y = "";
    var decimal = 0;
    var group = 0;
    var count = x.length;

    if (g_integer)
        decimal = x.length;

    for (var i = x.length; i >= 0; i--) {
        if (x.charAt(i) == '.') {
            y = "." + y;
            decimal = i;
        }
        else {
            if (count == decimal - g_group[group]) {
                y = "," + y;
                group++;
                count--;
            }
            y = x.charAt(i) + y;
        };
        count--;
    };
    g_integer = true;

    return y;
};

function othercon(x) {
    var y = "";
    var decimal = x.length;
    var group = 0;

    for (var i = x.length; i >= 0; i--)
        if (x.charAt(i) == ',') {
            y = "." + y;
            decimal = i;
            g_integer = false;
        }
        else
            if (x.charAt(i) != '.')
                y = x.charAt(i) + y;
            else
                if (group++ == g_group.length)
                    g_group[group-1] = decimal - i;

    return y;
};

function otherdecon(x) {
    var y = "";
    var decimal = 0;
    var group = 0;
    var count = x.length;

    if (g_integer)
        decimal = x.length;

    for (var i = x.length; i >= 0; i--) {
        if (x.charAt(i) == '.') {
            y = "," + y;
            decimal = i;
        }
        else {
            if (count == decimal - g_group[group]) {
                y = "." + y;
                group++;
                count--;
            }
        y = x.charAt(i) + y;
        };
    count--;
    };
    g_integer = true;

    return y;
};

function required(input,text) {
    if (input.value == "") {
        alert(text);
        input.focus();
        return false;
    } else
        return true;
};

function number(input,text) {
    var c = "";
    for (var i = 0; i < input.value.length; i++) {
        c = input.value.charAt(i);
        if ((c < "0" || "9" < c) && c != "." && c != ",") {
            alert(text);
            input.focus();
            return false;
        };
    };
    return true;
};

function date(input) {
var separator = "-";
var dayPosition = 0;
var monthPosition = 1;
var yearPosition = 2;

if (input.value != "") {
  var matchArray = input.value.split(separator);

  if ((matchArray.length != 3)) { // check format
    alert("Date is not in a valid format, 31-12-2000")
    input.focus();
    return false;
  }

  var day = matchArray[dayPosition];
  var month = matchArray[monthPosition];
  var year = matchArray[yearPosition];

  if (dateNumber(year) || (year < 1000 || year > 9999)) { // check year range
    alert("Year must be between 1000 and 9999.");
    input.focus();
    return false;
  }
  if (dateNumber(month) || (month < 1 || month > 12)) { // check month range
    alert("Month must be between 1 and 12.");
    input.focus();
    return false;
  }
  if (dateNumber(day) || (day < 1 || day > 31)) {
    alert("Day must be between 1 and 31.");
    input.focus();
    return false;
  }
  if ((month==4 || month==6 || month==9 || month==11) && day==31) {
    alert("Month "+month+" doesn't have 31 days!")
    input.focus();
    return false
  }
  if (month == 2) { // check for february 29th
    var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    if (day>29 || (day==29 && !isleap)) {
      alert("February " + year + " doesn't have " + day + " days!");
      input.focus();
      return false;
    }
  }
  return true;  // date is valid
}
return true;  // null
};


function oaOpenWin(regCode, regAppId, baseAM, amUsg, transId, paramList)
{
//  var url = "/OA_HTML/OA.jsp?akRegionCode=" + regCode;
  var url = "/OA_HTML/cabo/jsps/frameRedirect.jsp?";
  url += "redirect=/OA_HTML/OA.jsp&akRegionCode=" + regCode;
  url += "&akRegionApplicationId=" + regAppId + "&amUsageMode=" + amUsg;
  url += "&addBreadCrumb=S&baseAppMod=" + baseAM + "&transactionid=" + transId;
  url += paramList;
  openWindow(self, url, 'modal',{width:750, height:550, resizable:'yes'}, true); 
}

function lov(a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, c, p)
{
  var url = "";
  var proxy = null;

  // lovRowNum
  if (a12 != null)
  {
    proxy = new TableProxy(a13);
    url += "&lovTableName=" + a13;
    url += "&lovRowNum=" + a12;
  }


  url += "&regionCode=" + a2;
  url += "&regionAppId=" + a3;
  url += "&lovBaseItemName=" + a4;
  url += "&lovLabel=" + a6;
  url += "&lovMainCriteria=" + escape(a7 + "::");

// a8 = hasLovMainCriteria
  if (a8)
    url += getCriteria(a4, a12, proxy);
 

  var i = 0;
  for(paramName in c){
     var s = escape(paramName + "::") + getCriteria(c[paramName], a12, proxy);
     url += "&CRITERIA" + i + "=" + s;
     i++;
  }

  i = 0;
  for(paramName in p){
     var s = escape(paramName + "::") + getCriteria(p[paramName], a12, proxy);
     url += "&PASSIVE_CRITERIA" + i + "=" + s;
     i++;
  }

  if (a9 != null)
    url += "&lovJS=" + a9;
  if (a10)
    url += "&flexLov=t";
  oaOpenWin(a0, a1, a5, "1", a11, url);
};

// return LOV result
function putResult(itemName, res)
{
  var item = document.forms[0][itemName];
  if (item == 'undefined' || item == null){
    alert('LOV result ' + itemName + ' cannot be returned to base page.' + '\n' + 'It is either not rendered on the page or is not a form element.');
  }
  else if (item.type == 'select-one'){
    for (var i = 0; i < item.options.length; i++) {
      if (item.options[i].value == res){ 
         item.options[i].selected = true; break;
	}
     }
   }
   else 
     item.value = unescape(res); 
};

// retrieve LOV criteria
function getCriteria(c, row, proxy)
{
  var a0;
  if (row == null)
    a0 = document.forms[0][c];
  else
    a0 = proxy.getFormElement(c, row);

  //// Fix for Bug 2239664 - Hgandhi
 if (a0 == null)
    a0 = document.forms[0][c];

  var a1;
  if (a0.type == 'select-one')
     a1 = a0[a0.selectedIndex].value;
  else
     a1 = a0.value;
  return escape(escape(a1));
};


function lovClearValue(formElement)
{
  var retVal = true;
  if (formElement== undefined || formElement == null){
    retVal = false;}
  else if (formElement.type == 'select-one') {
    formElement.selectedIndex= 0; }
  else if (formElement.type == 'checkbox') {
    formElement.checked = false; }
  else if (formElement.value == '') {
    retVal = false; }
  else { formElement.value = ''; }
    return retVal; 
};
