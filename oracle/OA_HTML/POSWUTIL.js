/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSWUTIL.js 115.0 99/10/14 16:20:32 porting shi $ */

if(parseInt(navigator.appVersion) >= 4) 
{
  if (navigator.appName == "Netscape")
  {
    // Web browser is a netscape nevigator
    IS_NAV= true;
    IS_IE=false;
    coll = "";
    styleObj = "";
    window.captureEvents(Event.FOCUS|Event.CLICK |Event.RESIZE);
    suffix="_nav";
  }
  else // Internet Explorer
  {
    IS_NAV= false;
    IS_IE= true;
    coll = "all.";
    styleObj = ".style";
    layerTab= "div";
    suffix="_ie";
  }
}
else // invalid version
{
  alert("Invalid browser version");
}

mWin =null;



// Event handler for when a window gains focus or has been clicked
// on.  This function checks to see if there's a modal window open
// and, if so, gives focus to the modal window.  Returns true iff
// there's no modal window open.
function clickOrFocus(e)
{
  if (mWin&& !mWin.closed)
  { 
    if(IS_IE && !this.objectsDisabled)
    {
      disableWindow(top);
      this.objectsDisabled=true;
    }
    // race condition problem exists when user closes modal window.
    // mWin may get closed after we check for mWin.closed (above).
    // The only remedy (not cure) we can come up with is to check for
    // mWin.closed again right before giving focus.

    if(mWin && !mWin.closed)
      mWin.focus();
    return false;
  }
  else
  {
    if (IS_NAV) 
    {
      return routeEvent(e);
    }
    else if (this.objectsDisabled)
    {
      enableWindow(top);
      this.objectsDisabled=false;
      return true;
    }
  }
}

// disables all objects in a window and its subwindow (recursively).
// This routine is necessary only for IE, due to its bubble up event
// handling mechanise.
function disableWindow(win) 
{
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)
    {
      var elem=cForm.elements[e];
      elem.oldOnfocus= elem.onfocus;
      elem.oldOnclick=elem.onclick;
      elem.onclick= clickOrFocus;
      elem.onfocus= clickOrFocus;
    }
  }
  for (l=0; l<win.document.links.length; l++) 
  {
    var cLink= win.document.links[l];
    cLink.oldOnclick= cLink.onclick;
    cLink.onclick=clickOrFocus;
    cLink.oldOnfocus= cLink.onfocus;
    cLink.onfocus=clickOrFocus;
  }
  win.onclick=clickOrFocus;
  win.onfocus=clickOrFocus;
  win.document.onfocus=clickOrFocus;
  win.document.onclick=clickOrFocus;
  for(var w=0; w<win.frames.length; w++)
    disableWindow(win.frames[w]);
}

// restore IE form elements and links to normal behavior
function enableWindow(win) 
{
  for (var f=0; f<win.document.forms.length; f++)
  {
    var cForm=win.document.forms[f];
    for (var e=0; e < cForm.elements.length; e++)
    {
      var elem=cForm.elements[e];
      elem.onfocus= elem.oldOnfocus;
      elem.onclick=elem.oldOnclick;
    }
  }
  for (l=0; l<win.document.links.length; l++) 
  {
    var cLink= win.document.links[l];
    cLink.onclick=cLink.oldOnclick;
  }
  for(var w=0; w<win.frames.length; w++)
    enableWindow(win.frames[w]);
}


window.onClick= clickOrFocus;
window.onFocus= clickOrFocus;
window.onclick= clickOrFocus;
window.onfocus= clickOrFocus;

// opens a modal window  using attribute list as provided, or
// defaulted otherwise.  windowWidth and windowHeight are taken into
// consideration only if attributeList is not provided
function openModalWindow(url,   // url for the new window
                         name,  // name of the new window
                         attributeList, // attribute list for the new
                                        // window- by default, status=yes (all
                                        // else no) 
                         windowWidth,   // width for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 800 if null
                         windowHeight)  // height for new window, used
                                        // only if attribute list is
                                        // null.  defaults to 600 if null
{
  if(attributeList==null || attributeList=="")
  {
    var windowWidth= (windowWidth) ? windowWidth : 800;
    var windowHeight= (windowHeight) ? windowHeight : 600;
    attributeList= 
     "menubar=no,location=no,toolbar=no,width="+ windowWidth+ 
      ",height="+windowHeight+
      ",resizable=yes,scrollbars=yes,status=yes";
    if(IS_NAV)
    {
      var winLeft = window.screenX+((window.outerWidth -
                                       windowWidth) / 2);
      var winTop = window.screenY + ((window.outerHeight -
                                       windowHeight) / 2);
      attributeList +=",screenX=" +winLeft + ",screenY=" + winTop;
    }
    else // IS_IE
    {
      // best we can do is center in screen
      var winLeft = (screen.width - windowWidth) / 2;
      var winTop = (screen.height - windowHeight) / 2;
      attributeList += ",left=" + winLeft + ",top=" + winTop;
    }
  }
  if(modalOpen())
  {
    return mWin.openModalWindow(url, name, attributeList);
  }
  else
  { 
    top.mWin= open(url, name, attributeList);
        if(IS_IE)
          disableWindow(top);
    return top.mWin;
  }
}

function getTop()
{
  if(top.IS_TOP)
  {
    return top;
  }
  else
  {
    return opener.top.getTop();
  }
}

// returns true if this window has a modal window open
function modalOpen()
{
  return(mWin && !mWin.closed);
}

//  this function returns the upperMost modal window
function getActiveWindow()
{
  if(modalOpen())
  {
    return (mWin.getActiveWindow) ? mWin.getActiveWindow() : mWin;
  }
  else 
  {
    return window;
  }
}

// this function displays a message in the status bar
function showStatus(msg)
{
  window.status = msg;
  return true;
}

// closes all modal window in order, starting from top most window
function recursiveClose()
{
  window.onClick= null;
  window.onFocus= null;
  window.onclick= null;
  window.onfocus= null;
  var tempWin= mWin;
  mWin=null;
  
  if(tempWin && !tempWin.closed && tempWin.recursiveClose)
  {
    tempWin.recursiveClose();
  }
  else if(tempWin && !tempWin.closed)
  {
    tempWin.close();
  }
  //  alert("mWin="+mWin+"mWin.closed="+ (mWin ? mWin.closed : "n/a"));
  window.close();
}




