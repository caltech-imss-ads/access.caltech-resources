/* $Header: JTFBISRP.js 115.17 2001/02/20 16:20:43 pkm ship      $ */

/**********************************************
* JTFBISRP.js                                 *
*                                             *
* History                                     *
* Added fix for the case when only one of the *
* date parameter exist ie.start date or end   *
* date                                        *
* Bug no 1518622 - 01/19/01 by sspurani       *
* Added code for date validation when         *
* date_format is 'MON-YYYY'                   *
* Bug no 1385557 - 01/23/01 by sspurani       *
**********************************************/

ns4 = (document.layers)? true:false;
ie4 = (document.all)? true:false;
full_load = true;
window.onload=init;
CRMBISPrint = false;
ClippedSize = 0;
LayerShift = 0;
Chart=false;
//If the date parameters do not exist in a report
//Added by sspurani 02/08/01
var l_ak_nls_format = null;
//if(ns4)
//  window.captureEvents(Event.LOAD);

function init() {
	document.ReportForm = LookUpBlockFormObj();
	document.ReportForm.reportname = document.ReportForm.name;
//        document.SavedChartParameter = SaveChartParameter();
	document.SavedParameter = SaveParameter();
        if ( ie4 ){
	   document.domain = changeDomain(document.domain);
        }
}
function ChartEscape(InString)
{    
    OutString = InString.replace(/\+/g,"%252b");
    OutString = OutString.replace(/=/g,"%253d");
    return OutString;
}

function GetFormParameterString(FormObj,Base,Chart){
  var ParameterString = "";
  var PlusSign = (arguments.length>2)?"%252b":"*";
  var EqualSign = (arguments.length>2)?"%253d":"=";
  var TempParaString="";
  Base = (arguments.length==1)?0:Base;

   if (l_ak_nls_format != null)
    var ak_NLSformat=l_ak_nls_format;
   else
    var ak_NLSformat=NLSformat;

  if (ak_NLSformat != 'NIL')
  {
     NLSformat = ak_NLSformat;
  }


  for (var i =Base ; i < FormObj.elements.length; i++)

    if(FormObj.elements[i].value&&FormObj.elements[i].value!=""){
      if(FormObj.elements[i].name=="P_SESSION_INFO"){
	if(arguments.length<3&&false){
          ParameterString += PlusSign;
          ParameterString += FormObj.elements[i].value;
        }
      }
      else{	

          if(FormObj.elements[i].name == "P_VIEW_BY"){
            var OptObj = FormObj.elements[i].options;
            FormObj.report.value="";
            var OptValue = escape(ParseReportFileName(OptObj[OptObj.selectedIndex].value));
            ParameterString += OptValue;
          }
          else{

               ParameterString += PlusSign;
               ParameterString += FormObj.elements[i].name+ EqualSign ;

               if(arguments.length<3)
               {

               if(((FormObj.elements[i].name == "P_START_DATE")||
                  (FormObj.elements[i].name  == "P_END_DATE"))&&
                  (NLSformat == "MON-YYYY")) {
                   var urldatevalue = '01-' + FormObj.elements[i].value;
                   var tempString  = ((escape(urldatevalue)).replace("*","~at~")).replace("]","~end~"); 
                    ParameterString += tempString.replace("+","%2b");
                 }
                 else{

                 // Changed by apandian on 9/26 to resolve bug 1304607
                 // Changed the special characters * and ] as per ICX's instruction
                 // Changing + to hexcode as we are using + as the delimiter

	        //ParameterString +=escape(FormObj.elements[i].value)
	    TempParaString = ((escape(FormObj.elements[i].value)).replace("*","~at~")).replace("]","~end~");
	    ParameterString += TempParaString.replace("+","%2b");
           }
          }
          else
            ParameterString +=ChartEscape(FormObj.elements[i].value);
          }
      }
    }else
      if(ns4 && FormObj.elements[i].type=="select-one"){
        var OptObj = FormObj.elements[i].options;

        if(FormObj.elements[i].name == "P_VIEW_BY"){
          FormObj.report.value="";
          var OptValue = escape(ParseReportFileName(OptObj[OptObj.selectedIndex].value));
          ParameterString += OptValue;
        }
        else{

          ParameterString += PlusSign;
          ParameterString += FormObj.elements[i].name+ EqualSign;

          if(arguments.length<3)
            ParameterString += escape(OptObj[OptObj.selectedIndex].value);
          else
            ParameterString += ChartEscape(OptObj[OptObj.selectedIndex].value);

        }

      }
  return ParameterString;
}
function LookUpBlockFormObj(){
//  if(ns4) var FormObj=block.document.forms[0];
 var FormObj= "";
 if (l_FormInd > -1)
 {
  if(ns4)   FormObj=document.forms[l_FormInd];
  if(ie4) 
      FormObj=document.forms[l_FormInd];
  //{ var i;
  //  for(i=0;i<blockDiv.children.length;i++)
  //     if(blockDiv.children(i).tagName=="FORM") break;
  //   var FormObj=blockDiv.children(i);}
  }
  return FormObj;
}
function blocksubmit(){
  var FormObj=LookUpBlockFormObj();
  //var ActionString = FormObj.action;
  var ActionString=ICX_REPORT_LINK+"OracleOASIS.RunReport?report=";
  var Param=SaveParameter();
  var startdate;
  var enddate;

  if (l_ak_nls_format != null)
   var ak_NLSformat=l_ak_nls_format;
  else
   var ak_NLSformat=NLSformat;

  if (ak_NLSformat != 'NIL')
  {
     NLSformat = ak_NLSformat;
  }

  if (!FormObj.P_START_DATE  && FormObj.P_END_DATE )
  {
    startdate = 'NOTEXIST';
    if ( validate_date(startdate,
                     FormObj.P_END_DATE.value,
                     NLSformat) )
    {
      window.location=ActionString+document.ReportForm.reportname
                      +"&parameters="+Param+"*";
    }
  }

 else 
 {
  if (FormObj.P_START_DATE && !FormObj.P_END_DATE )
  {
    enddate  = 'NOTEXIST';
    if ( validate_date(FormObj.P_START_DATE.value,
                     enddate,
                     NLSformat) )
    {
      window.location=ActionString+document.ReportForm.reportname
                      +"&parameters="+Param+"*";
    }
  }
  else
  {
   if ( FormObj.P_START_DATE && FormObj.P_END_DATE )
   {

    if ( validate_date(FormObj.P_START_DATE.value,
                     FormObj.P_END_DATE.value,
                     NLSformat) )

     {
      window.location=ActionString+document.ReportForm.reportname
                      +"&parameters="+Param+"*";
     }
   }
   else
   {
    window.location=ActionString+document.ReportForm.reportname
                    +"&parameters="+Param+"*";
   }
  }
 }
}

function ParseReportFileName(InString){
   InArray=InString.split("+");
   NewString = "";
   for(var i=0;i<InArray.length;i++){
      InArray2=InArray[i].split("=");
      if(InArray2[0]=="report"){
        //alert("B4:"+InArray2[1]);
        //alert(document.ReportForm.reportname);
        //document.ReportForm.name=InArray2[1];
        document.ReportForm.reportname=InArray2[1];
        //alert(document.ReportForm.reportname);
       }else
         if(InArray[i])
           NewString += "*"+InArray[i];
   }
   return NewString;
}
function SaveParameter(){
  var FormObj=LookUpBlockFormObj();
  var ParameterString = "";
//  if(ns4) var FormObj2 = document.LAYER1.document.forms[0];
if (l_FormInd > -1)
{
  if(ns4) var FormObj2 = document.forms[l_FormInd];
  if(ie4) { 
   var FormObj2 = document.forms[l_FormInd];
  }
    FormObj2.report.value="";

//    var ParameterString = GetFormParameterString(FormObj);
    var ParameterString = GetFormParameterString(FormObj2);
 }

  return ParameterString;
}

function SaveChartParameter(){
  var FormObj=LookUpBlockFormObj();
//  if(ns4) var FormObj2 = document.LAYER1.document.forms[0];
  if(ns4) var FormObj2 = document.forms[0];
  if(ie4) { var i;
    for(i=0;i<LAYERDiv.children.length;i++)
      if(LAYERDiv.children(i).tagName=="FORM") break;
     var FormObj2=LAYERDiv.children(i);
  }
  if (FormObj2.elements.length > 0
        && FormObj2.elements[0].name == "P_VIEW_BY"){
    FormObj.report.value="";
    var ParameterString = GetFormParameterString(FormObj,0,true);
    //var OptObj = FormObj2.elements[0].options;
    //ParameterString += "%252b";
    //var Local_String = OptObj[OptObj.selectedIndex].value;
    //Local_String = Local_String.replace(/\+/g,"%252b");
    //Local_String = Local_String.replace(/=/g,"%253d");
    //alert(Local_String);
    //ParameterString += Local_String;
    ParameterString += GetFormParameterString(FormObj2,1,true);
  }else{
    FormObj.report.value=FormObj.name;
    var ParameterString = GetFormParameterString(FormObj,0,true);
    ParameterString += GetFormParameterString(FormObj2,0,true);
  }
  //alert(ParameterString);
  return ParameterString;

}

function BackgroundPrint(){
  var FormObj=LookUpBlockFormObj();
  //var ActionString = FormObj.action;
  var ActionString=ICX_REPORT_LINK+"OracleOASIS.RunReport?report=";
   bgpwin = window.open(ActionString+document.ReportForm.reportname+"&parameters="+document.SavedParameter+"*P_PRINT=YES*");
   bgpwin.blur();
   window.focus();
}

var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))

function SaveFavorite()
{
   var openleft=1;
   var opentop=1;
   var calwidth=1;
   var calheight=1;

   if (Nav4)
   {

      // center on the main window
      openleft = parseInt(window.screenX + ((window.outerWidth - calwidth) / 2));
      opentop = parseInt(window.screenY + ((window.outerHeight - calheight) / 2));
      var attr = "screenX=" + openleft + ",screenY=" + opentop;
      attr += ",width=" + calwidth + ",height=" + calheight;
   }
   else
   {

      // best we can do is center in screen
      openleft = parseInt(((screen.width - 75) - calwidth) / 2);
      opentop = parseInt(((screen.height - 110) - calheight) / 2);
      var attr = "left=" + openleft + ",top=" + opentop;
      attr += ",width=" + calwidth + ",height=" + calheight;
    }
   //alert(ICX_REPORT_LINK);
   //alert(document.ReportForm.name);
   //alert(document.SavedParameter);
   var l_function_name = document.ReportForm.P_FN_NAME.value;
   var l_report = document.ReportForm.reportname;
   sfpwin = window.open
    (ICX_REPORT_LINK+"jtf_bis_favorite_plug.SaveParameter?Z="+
     document.SavedParameter+
    "&Y="+escape(l_function_name)+"&W="+l_report,"sfp", attr, "dependent=yes");
    //"&Y="+escape(l_function_name)+"&W="+l_report,"sfp","width=200,height=75,dependent=yes");
}


function SaveDefault(){
   var l_function_name = document.ReportForm.P_FN_NAME.value;

   var openleft=1;
   var opentop=1;
   var calwidth=1;
   var calheight=1;

   if (Nav4)
   {

      // center on the main window
      openleft = parseInt(window.screenX + ((window.outerWidth - calwidth) / 2));
      opentop = parseInt(window.screenY + ((window.outerHeight - calheight) / 2));
      var attr = "screenX=" + openleft + ",screenY=" + opentop;
      attr += ",width=" + calwidth + ",height=" + calheight;
   }
   else
   {

      // best we can do is center in screen
      openleft = parseInt((screen.width - calwidth) / 2);
      opentop = parseInt(((screen.height - 110) - calheight) / 2);
      var attr = "left=" + openleft + ",top=" + opentop;
      attr += ",width=" + calwidth + ",height=" + calheight;

    }

   //alert(l_function_name);
   sfpwin = window.open
    (ICX_REPORT_LINK+"jtf_bis_favorite_plug.SaveParameter?Z="+
     document.SavedParameter+
    "&Y="+escape(l_function_name)+"&X="+escape(l_function_name),"sfp", attr, "dependent=yes");
    //"&Y="+escape(l_function_name)+"&X="+escape(l_function_name),"sfp", attr, "width=1,height=1,dependent=yes");
}
function LookUpHiddenValue(HiddenName)
{
  if (ns4){
    if(HiddenName)
      var HiddenElement = eval('block.document.forms[0].'+HiddenName);
    else
      return ;
  }
  if (ie4){
    for(var i=0;i<blockDiv.children.length;i++)
      if(blockDiv.children(i).tagName=="FORM") break;
    var HiddenElement = blockDiv.children(i).elements(HiddenName);
  }
//  alert(HiddenElement.name);
  return HiddenElement;
}

function BuildDynamicDropDownList(SelectObj)
{
  this.obj = SelectObj;
  //alert(SelectObj.name);
  //alert(this.obj.layer.id);
  if(SelectObj.name)
    this.hidden = LookUpHiddenValue(SelectObj.name);
  this.name = SelectObj.name;
  this.length = SelectObj.options.length;
  this.SelectedIndex = -1;
  this.Shrinked = false; 
  this.OptionBody = "<!--OptionBody-->";
  for(var i=0;i<SelectObj.length;i++){
   this.OptionBody = this.OptionBody +"<option VALUE=\"" + SelectObj.options[i].value + "\">" +
   SelectObj.options[i].text + "</option>";
  }
}

function layerWrite(id,nestref,text) {
                if (ns4) {
                        var lyr = (nestref)? eval('document.'+nestref+'.document.'+id+'.document') : document.layers[id].document
                        lyr.open();
                        lyr.write(text);
                        lyr.close();
                }
                else if (ie4) document.all[id].innerHTML = text
        }

function LayerMouseOut()
{
  if(ns4)
    SelectChange(this.document.forms[0].elements[0]);
  if(ie4){
//	this.onmouseout=null;
    if(window.event.toElement==null||window.event.fromElement==null) return false;
//    alert("out:src"+window.event.srcElement.tagName);
//    alert(window.event.fromElement.tagName);
//    alert(window.event.toElement);
    SelectChange(this.childNodes(0).elements[0]);
  }
//  alert(this.DyanmicDropDownList.length);
}

function SelectChange(obj)
{
  if (!obj.options.layer.DynamicDropDownList.Shrinked){
     OptionChange(obj.layer,obj.options);
  }
}

function OptionChange(LayerObj, OptionObj)
{   if(OptionObj.selectedIndex==-1) OptionObj.selectedIndex = LayerObj.DynamicDropDownList.SelectedIndex;
    LayerObj.DynamicDropDownList.SelectedIndex = OptionObj.selectedIndex;
    LayerObj.DynamicDropDownList.Shrinked = true;
    LayerObj.DynamicDropDownList.hidden.value = OptionObj[OptionObj.selectedIndex].value;
    var layercontent = "<form> <select size='1'>";
    layercontent = layercontent + "<option VALUE=\"" + OptionObj[OptionObj.selectedIndex].value + "\">"
      + OptionObj[OptionObj.selectedIndex].text.substr(0,9) + "" +"</option></select></form>";
    layerWrite(LayerObj.id,"blockDiv", layercontent);

    if(ns4){
	LayerObj.document.forms[0].elements[0].layer = LayerObj;
    }
    if(ie4){
      LayerObj.children(0).elements[0].layer = LayerObj;
//        LayerObj.children(0).onmouseout=new Function("window.event.cancelBubble = true;");
//	LayerObj.children(0).onmouseout=LayerMouseOut;
//	alert(LayerObj.children(0).name);
    }
    if(LayerObj.onmouseover!=LayerMouseOver)
	LayerObj.onmouseover=LayerMouseOver;
//    LayerObj.onmouseout=LayerMouseOut;
    if(ns4)
	LayerObj.captureEvents(Event.MOUSEOVER | Event.MOUSEOUT);
}



function LayerMouseOver()
{
  if (ns4) {
     var obj = this.document.forms[0].elements[0];
     this.releaseEvents(Event.MOUSEOVER);
  }
  if (ie4){
    if(window.event.toElement==null||window.event.fromElement==null) return false;
//    alert("over:"+window.event.srcElement.tagName);
//    alert(window.event.fromElement.tagName);
//    alert(window.event.toElement);
     this.onmouseover=null;	
//     this.onmouseout=null;
     var obj = this.childNodes(0).elements[0];
//     alert(this.firstChild.name);
  }
  if (obj.options.length==1){
     OptionMouseOver(obj.layer,obj.options);
     if (ns4) {
// 	this.document.forms[0].elements[0].selectedIndex = P1SourceSelected;
	this.onmouseout=LayerMouseOut;
//	this.captureEvents(Event.MOUSEOUT);
     }
     if (ie4){
//	this.childNodes(0).elements[0].selectedIndex = P1SourceSelected;
//        alert(this.firstChild.name);
	this.onmouseout=LayerMouseOut;
//	this.firstChild.elements[0].onmouseout=new Function("SelectChange(this)");
     }
  }
//  else
//   this.onmouseout=null;
}

function OptionMouseOver(LayerObj,obj)
{ 
  var layercontent = "<form> <select size='1' onChange='SelectChange(this)'>";
  layercontent = layercontent  + LayerObj.DynamicDropDownList.OptionBody;
  layercontent = layercontent  + "</select></form>";

  layerWrite(LayerObj.id, "blockDiv", layercontent);
  LayerObj.DynamicDropDownList.Shrinked = false;
  if(ns4){
    LayerObj.document.forms[0].elements[0].layer = LayerObj;
    LayerObj.document.forms[0].elements[0].selectedIndex = LayerObj.DynamicDropDownList.SelectedIndex;
  }
  if(ie4){
    LayerObj.children(0).elements[0].layer = LayerObj;
    LayerObj.children(0).elements[0].selectedIndex = LayerObj.DynamicDropDownList.SelectedIndex;
  }
}

function clipValues(obj,which) {
        if (ns4) {
                if (which=="t") return obj.clip.top
                if (which=="r") return obj.clip.right
                if (which=="b") return obj.clip.bottom
                if (which=="l") return obj.clip.left
        }
        else if (ie4) {
                var clipv = obj.clip.split("rect(")[1].split(")")[0].split("px")
                if (which=="t") return Number(clipv[0])
                if (which=="r") return Number(clipv[1])
                if (which=="b") return Number(clipv[2])
                if (which=="l") return Number(clipv[3])
        }
}

function clipTo(obj,t,r,b,l) {
        if (ns4) {
                obj.clip.top = t
                obj.clip.right = r
                obj.clip.bottom = b
                obj.clip.left = l
        }
        else if (ie4) obj.clip = "rect("+t+"px "+r+"px "+b+"px "+l+"px)"
}

function clipBy(obj,t,r,b,l) {
        if (ns4) {
                obj.clip.top = clipValues(obj,'t') + t
                obj.clip.right = clipValues(obj,'r') + r
                obj.clip.bottom = clipValues(obj,'b') + b
                obj.clip.left = clipValues(obj,'l') + l
        }
        else if (ie4) obj.clip = "rect("+(this.clipValues(obj,'t')+t)+"px "+(this.clipValues(obj,'r')+r)+"px "+Number(this.clipValues(obj,'b')+b)+"px "+Number(this.clipValues(obj,'l')+l)+"px)"
}


function moveTo(obj,x,y) {
	obj.xpos = x
	obj.left = obj.xpos
	obj.ypos = y
	obj.top = obj.ypos
}

function wipe2() {
	if (full_load){
	        clipBy(block,0,0,0-ClippedSize,0)
		full_load = false;
		if(ns4){
		  //alert(document.LAYER1.clip.bottom);
		  //alert(document.LAYER1.clip.height);
		  //document.LAYER1.resizeBy(0, 0-ClippedSize);
		  //window.innerHeight-=ClippedSize;
		  document.LAYER1.moveBy(0, 0-ClippedSize);
		  //alert(window.innerHeight);
                  MoreDiv=document.MoreDiv;
		}else{
		  LAYERDiv.style.pixelTop -=ClippedSize;
		}
                layerWrite(MoreDiv.id,'','<TABLE border=0 cellpadding=0 cellspacing=0>'+
			'<TR><TD height=22 rowspan=3>'+
			'<A href="javascript:wipe2()" onMouseOver="window.status=\''+
			document.MoreParametersLabel + '\';return true">'+
			'<IMG src="/OA_MEDIA/FNDJLFRL.gif" height=22 width=15 border=0>'+
			'</A></TD>'+
 			'<TD height=1 bgcolor=#FFFFFF colspan=2>'+
			'<IMG src="/OA_MEDIA/FNDINVDT.gif" height=1 width=1></TD>'+
			'<TD height=22 rowspan=3>'+
			'<A href="javascript:wipe2()" onMouseOver="window.status=\''+
 			document.MoreParametersLabel + '\';return true">'+
			'<IMG src="/OA_MEDIA/FNDJLFRR.gif" height=22 width=11 border=0>'+
			'</A></TD></TR>'+
			'<TR><TD height=20 align=center valign=center bgcolor=#cccccc '+
			'nowrap colspan=2>'+
			'<A href="javascript:wipe2()" style="text-decoration:none" '+
			'onMouseOver="window.status=\''+
 			document.MoreParametersLabel + '\';return true">'+
			'<FONT size=2 face="Arial,Helvetica,Geneva"  color=000000>'+
 			document.MoreParametersLabel +
			'</FONT></A></TD></TR>'+
			'<TR><TD height=1 bgcolor=000000 colspan=2>'+
			'<IMG src="/OA_MEDIA/FNDINVDT.gif" width=1 height=1></TD></TR>'+
			'</TABLE>');
	}else{
       	        clipBy(block,0,0,ClippedSize,0)
		full_load = true;
 		if(ns4){
		  //alert(document.LAYER1.clip.bottom);
		  //alert(document.LAYER1.clip.height);
		  //alert(window.innerHeight);
		  //document.LAYER1.resizeBy(0, ClippedSize);
		  //window.innerHeight+=ClippedSize;
		  document.LAYER1.moveBy(0, ClippedSize);
		  //alert(window.innerHeight);
		  MoreDiv=document.MoreDiv;
		}else{
		  LAYERDiv.style.pixelTop += ClippedSize;
		}
                layerWrite(MoreDiv.id,'', '<TABLE border=0 cellpadding=0 cellspacing=0>'+
                        '<TR><TD height=22 rowspan=3>'+
                        '<A href="javascript:wipe2()" onMouseOver="window.status=\''+
                        document.LessParametersLabel + '\';return true">'+
                        '<IMG src="/OA_MEDIA/FNDJLFRL.gif" height=22 width=15 border=0>'+
                        '</A></TD>'+
                        '<TD height=1 bgcolor=#FFFFFF colspan=2>'+
                        '<IMG src="/OA_MEDIA/FNDINVDT.gif" height=1 width=1></TD>'+
                        '<TD height=22 rowspan=3>'+
                        '<A href="javascript:wipe2()" onMouseOver="window.status=\''+
                        document.LessParametersLabel + '\';return true">'+
                        '<IMG src="/OA_MEDIA/FNDJLFRR.gif" height=22 width=11 border=0>'+
                        '</A></TD></TR>'+
                        '<TR><TD height=20 align=center valign=center bgcolor=#cccccc '+
                        'nowrap colspan=2>'+
                        '<A href="javascript:wipe2()" style="text-decoration:none" '+
                        'onMouseOver="window.status=\''+
                        document.LessParametersLabel + '\';return true">'+
                        '<FONT size=2 face="Arial,Helvetica,Geneva"  color=000000>'+
                        document.LessParametersLabel +
                        '</FONT></A></TD></TR>'+
                        '<TR><TD height=1 bgcolor=000000 colspan=2>'+
                        '<IMG src="/OA_MEDIA/FNDINVDT.gif" width=1 height=1></TD></TR>'+
                        '</TABLE> ');
	}

}

function preload(imgObj,imgSrc) {
  if (document.images) {
    eval(imgObj+' = new Image()');
    eval(imgObj+'.src = "'+imgSrc+'"');
  }
}

function changeImage(imgName,imgObj) {
  if (document.images) {
    document.images[imgName].src = eval(imgObj+".src")
  }
}

function changeLink(linkName,linkhref) {
  if (document.links) {
     document.links(linkName).href = linkhref;
    }
}

function changeDomain(domain){
  DomainArray = domain.split(".");
  return DomainArray[DomainArray.length - 2]
    + "." + DomainArray[DomainArray.length - 1];
}
