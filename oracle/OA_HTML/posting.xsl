<?xml version="1.0"?>
<!--$Header: posting.xsl 115.6 2000/06/06 17:31:00 pkm ship   $-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
     <xsl:apply-templates select="/ArrayList/Campaign/AttachmentLst/Attachment"/>
  </xsl:template>
  <xsl:template match="Attachment">
    <xsl:element name="a">
     <xsl:attribute name="href">
      <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute> 
     <xsl:element name="img">
      <xsl:attribute name="src">
	  <xsl:value-of select="child::FileLocation"/>
      </xsl:attribute>
     </xsl:element>
   </xsl:element>
 </xsl:template>
</xsl:stylesheet>
