/*********************************************************************
 * Javascript functions for To Do Create/Update
 *********************************************************************/
 <!-- $Header: asfCntctCreatFrm.js 115.2 2000/09/13 22:26:21 pkm ship   $ -->

/**
 * handle click on checkBox for Decision Maker Flag
 */

function setDecisionMakerFlag(secObjName)
{
  var theForm = document.forms['PerCreate'];

  var decisionMakerFlag = secObjName + 'DecisionMakerFlag';

  var checkBox = 'DecisionMakerFlag';

  if(theForm.elements[checkBox].checked)
  {
    /* alert("checkbox checked"); */
    theForm.elements[decisionMakerFlag].value = 'Y';
  }
  else
  {
    /* alert("checkbox not checked"); */
    theForm.elements[decisionMakerFlag].value = 'N';
  }
}

/**
 * handle click on checkBox for Reference Use Flag
 */

function setReferenceUseFlag(secObjName)
{
  var theForm = document.forms['PerCreate'];

  var referenceUseFlag = secObjName + 'ReferenceUseFlag';

  var checkBox = 'ReferenceUseFlag';

  if(theForm.elements[checkBox].checked)
  {
    /* alert("checkbox checked"); */
    theForm.elements[referenceUseFlag].value = 'Y';
  }
  else
  {
    /* alert("checkbox not checked"); */
    theForm.elements[referenceUseFlag].value = 'N';
  }
}

