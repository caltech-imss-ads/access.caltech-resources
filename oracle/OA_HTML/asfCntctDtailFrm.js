/*********************************************************************
 * Javascript functions for To Do Create/Update
 *********************************************************************/
 <!-- $Header: asfCntctDtailFrm.js 115.0 2000/08/16 16:30:16 pkm ship   $ -->

/**
 * handle click on checkBox for Inactive Flag
 */

function setInactiveFlag(secObjName)
{
  var theForm = document.forms['PerDtail'];

  var status = secObjName + 'Status';

  var checkBox = 'Status';

  if(theForm.elements[checkBox].checked)
  {
    alert("checkbox checked");
    theForm.elements[status].value = 'I';
  }
  else
  {
    alert("checkbox not checked");
    theForm.elements[status].value = 'A';
  }
}

