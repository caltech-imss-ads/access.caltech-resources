//  $Header: pvxldasg.js 115.3 2000/08/02 15:58:45 pkm ship      $
//===========================================================================+ |
// |      Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA       |
// |                         All rights reserved.                              |
// +===========================================================================+
// |  FILENAME                                                                 |
// |      pvxldasg.js                                                          |
// |                                                                           |
// |  DESCRIPTION                                                              |
// |      Javascript library for Lead Assignment                |
// |                                                                           |  
// | NOTES                                                                     |
// |                                                                           |
// |  DEPENDENCIES                                                             |
// |                                                                           |
// |  HISTORY                                                                  |
// |                                                                           |
// |                                                                           |
// |       14-JUN-2000  SPEDDU          created			       |  
// |                                                                           |
// +==========================================================================*/

function submitManual(thisForm){
    thisForm.action = 'pvxldadd.jsp';
    thisForm.submit();
}
function submitUpdate(thisForm){
    if(rankCheck(thisForm)) {
    thisForm.action = 'pvxldasg.jsp';
    thisForm.automatic.value='false';
    thisForm.submit(); }
}
function submitInsert(thisForm){
  if(rankCheck(thisForm)) 
 singleCheck(thisForm);
}
function singleCheck(thisForm){
  var asgntype = thisForm.asgntyp.options[thisForm.asgntyp.selectedIndex].value;
   if (asgntype == "SINGLE") {
      if (thisForm.partnerID.length > 1)
         alert("Only one partner can be submitted in Single assignment");
      else {
         thisForm.action = 'pvxcrasg.jsp';
         thisForm.submit();} }
   else{
      thisForm.action = 'pvxcrasg.jsp';
      thisForm.submit(); }
}
function rankCheck(thisForm){
 var rank = new Array();
 var returnVal = true;
 var previousRank = 0;
 for(var i=0; i<thisForm.partnerID.length;i++){
    var rankval = "thisForm.Rank"+i+".options[thisForm.Rank"+i+".selectedIndex].value;";
    var r1  = eval(rankval);
    rank[i] = r1;
 }
 for(var j= 0; j <rank.length; j++) {
  for (var k=(j+1); k < rank.length; k++){
    if(rank[k] < rank[j]) {
      var temp = rank[j];
      rank[j] = rank[k];
      rank[k] = temp;
    }
  }
 }
 for(var j= 0; j <rank.length; j++) {
  if (rank[j] == previousRank) {
   alert("Rank Values must be  unique");
   returnVal = false;
   break;
  }
  previousRank = rank[j];
 }
  return returnVal;
}
function changeRank(thisForm){
  thisForm.rankChange.value='true';
  thisForm.automatic.value='false';
}
function goNextPage()
{
   document.form.direction.value = 'Next'
   document.form.action='pvxldasg.jsp'
   document.form.submit()
}

function goPrevPage()
{
   document.form.direction.value = 'Previous'
   document.form.action='pvxldasg.jsp'
   document.form.submit()
}

function goFirstPage()
{
   document.form.direction.value = 'First'
   document.form.action='pvxldasg.jsp'
   document.form.submit()
}

function goLastPage()
{
  document.form.direction.value = 'Last'
  document.form.action='pvxldasg.jsp'
  document.form.submit()
}
