//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT1.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |              				     |     
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJCT.js is split into 4 files |
//  |              APWOBJT1.js, APWOBJT2.js ,        |
//  |              APWOBJT3.js, APWOBJT4.js          |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT1.js 115.4 2001/02/20 15:40:01 pkm ship     $ */


/*===========================================================================*
 *  ProfileOptions Class - Container Of ProfileOption Values                 *
 *===========================================================================*/

 /*--------------------------------------------* 
  * CONSTRUCTOR ProfileOptions:                *
  *   Params:  None                            *
  *  Returns:  None                            * 
  *     Desc:  Creates a ProfileOptions object *
  *--------------------------------------------*/ 
  function ProfileOptions() {
    this.arrProfiles = new Object();
  }


 /*---------------------------------------------------------------*
  * METHOD mvAddProfileOption:                                    *
  *   Params:  p_strProfileName -  Profile Option Name (string)   *
  *            p_vValue         -  Profile Option Value           *
  *  Returns:  Profile Option Value added to Profile Options Obj  *
  *     Desc:  Adds a profile option with name p_strProfileName   *
  *            with value p_vValue in the Profile Options Object  *
  *            and returns the new profile option value           *
  *---------------------------------------------------------------*/
  function mvAddProfileOption(p_strProfileName, p_vValue) {
    this.arrProfiles[p_strProfileName] = p_vValue;
    return this.arrProfiles[p_strProfileName];
  }


/*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mvGetProfileOption:                                    *
  *   Params:  p_strProfileName -  Profile Option Name (String)             *
  *  Returns:  Profile Option Value or null                                 *
  *     Desc:  Returns the Profile Option Value with the name               *
  *            p_strProfileName.  If profile name does not exist null is    *
  *            returned                                                     *
  *-------------------------------------------------------------------------*/
  function mvGetProfileOption(p_strProfileName) {
    var undefined;

    if (this.arrProfiles[p_strProfileName] == undefined)
      return null;
    return this.arrProfiles[p_strProfileName];
  }

  ProfileOptions.prototype.mvAddProfileOption    = mvAddProfileOption;
  ProfileOptions.prototype.mvGetProfileOption    = mvGetProfileOption;

  top.g_objProfileOptions = new top.ProfileOptions();

/*===========================================================================*
 *  Messages Class - Container Of Message Text                               *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Messages:                    *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Messages object     *
  *------------------------------------------*/ 
  function Messages() {
    this.arrMessages = new Object();
  }

 /*----------------------------------------------------------------*
  * METHOD mAddMessage:                                            *
  *   Params:  p_strMsgName -  Message Name (string)               *
  *            p_strMsgText -  Message Text (string)               *
  *  Returns:  None                                                *
  *     Desc:  Adds a message with name p_strMsgName with the      *
  *            message text of p_strMsgText to the Messages object *
  *----------------------------------------------------------------*/
  function mstrAddMesg(p_strMsgName, p_strMsgText)
  {
    this.arrMessages[p_strMsgName] = p_strMsgText;
  }

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetMessage:                                        *
  *   Params:  p_strMsgName -  Message Name (string)                        *
  *  Returns:  Message Text or null                                         *
  *     Desc:  Returns the message text with the name p_strMsgName.         *
  *            If the message name does not exist null is returned          *
  *-------------------------------------------------------------------------*/
  function mstrGetMesg(p_strMsgName)
  {
    var undefined;

    if (this.arrMessages[p_strMsgName] == undefined)
      return null;
    return this.arrMessages[p_strMsgName];

  }

  Messages.prototype.mstrAddMesg         = mstrAddMesg;
  Messages.prototype.mstrGetMesg         = mstrGetMesg;

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetMessageWTokensReplace:                          *
  *   Params:  p_strMsgName -  Message Name (string)                        *
  *            p_arrTokenNames  -  Array of Token Names (array of strings)  *
  *            p_arrTokenVals   -  Array of Token Values (array of strings) *
  *  Returns:  Message Text or null                                         *
  *     Desc:  Returns the message text with the name p_strMsgName.         *
  *            If the message name does not exist null is returned          *
  *-------------------------------------------------------------------------*/
  function mstrGetMesgWTokenReplace(p_strMsgName, 
                                    p_arrTokenNames, 
                                    p_arrTokenVals)
  {
    var strMsg = this.arrMessages[p_strMsgName]; 
    var count = 0;
    var undefined;

    while (p_arrTokenNames[count] != undefined){
      strMsg = strMsg.replace(p_arrTokenNames[count], p_arrTokenVals[count]);
      count++;  
    }

    strNew = " " + strMsg; 
    return strNew;
  }
  Messages.prototype.mstrGetMesgWTokenReplace       = mstrGetMesgWTokenReplace;

/*===========================================================================*
 *  Regions Class - Container of Regions                                     *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Regions:                     *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Regions object      *
  *------------------------------------------*/ 
  function Regions() {
    this.arrRegions = new Object();
  }

 /*---------------------------------------------------------------*
  * METHOD mobjAddRegion:                                         *
  *   Params:  p_strRegionName -  Region_Name of type string      *
  *  Returns:  Region Object added to Regions Array Object        *
  *     Desc:  Creates a Region object with name p_strRegionName  *
  *            and adds it to the Regions object and returns the  *
  *            new region                                         *
  *---------------------------------------------------------------*/
  function mobjAddRegion(p_strRegionName) {
    this.arrRegions[p_strRegionName] = new Region();
    return this.arrRegions[p_strRegionName];
  }

 /*---------------------------------------------------------------------*
  * ACCCESSOR METHOD mobjGetRegion:                                     *
  *   Params:  p_strRegionName -  Region Name of type string            *
  *  Returns:  Region Object or null                                    *
  *     Desc:  Returns the Region object with the name p_strRegionName. *  
  *            If the region does not exist null is returned            *
  *---------------------------------------------------------------------*/
  function mobjGetRegion(p_strRegionName) {
    var undefined;
    if (this.arrRegions[p_strRegionName] == undefined)
      return null;
    return this.arrRegions[p_strRegionName];
  }

  Regions.prototype.mobjAddRegion    = mobjAddRegion;
  Regions.prototype.mobjGetRegion    = mobjGetRegion;

/*===========================================================================*
 *  Region Class - Container of Region Items                                 *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Region:                      *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Region object       *
  *------------------------------------------*/ 
  function Region() {
    this.arrRegionItems = new Object();
  }

 /*-----------------------------------------------------------------------*
  * METHOD mobjAddRegionItem:                                             *
  *   Params:  p_strRegionItemName -  Region Item Name (string)           *
  *            p_strPrompt         -  Prompt of Region Item (string)      *
  *            p_iPromptLength     -  Prompt Display Length (int)         *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  Region Item Object added to Region Object                  *
  *     Desc:  Creates a Region Item object with name p_strRegionItemName *
  *            and adds it to the Region object and returns the new       *
  *            region item                                                *
  *-----------------------------------------------------------------------*/
  function mobjAddRegionItem(p_strRegionItemName, 
                             p_strPrompt, 
                             p_iPromptLength,
                             p_iValueLength,
                             p_strUpdateFlag,
                             p_strRequiredFlag) {
    this.arrRegionItems[p_strRegionItemName] = 
      new RegionItem(p_strPrompt, 
                     p_iPromptLength,
                     p_iValueLength,
                     p_strUpdateFlag,
                     p_strRequiredFlag);
    return this.arrRegionItems[p_strRegionItemName];
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mobjGetRegionItem:                                       *
  *   Params:  p_strRegionItemName -  Region Item Name (string)              *
  *  Returns:  Region Item Object or null                                    *
  *     Desc:  Returns the Region Item object with name p_strRegionItemName. *
  *            If the region item does not exist null is returned            *
  *--------------------------------------------------------------------------*/
  function mobjGetRegionItem(p_strRegionItemName) {
    var undefined;
    if (this.arrRegionItems[p_strRegionItemName] == undefined)
      return null;
    return this.arrRegionItems[p_strRegionItemName];
  }

  Region.prototype.mobjAddRegionItem    = mobjAddRegionItem;
  Region.prototype.mobjGetRegionItem    = mobjGetRegionItem;

/*===========================================================================*
 *  Region Item Class                                                        *
 *===========================================================================*/


// Constructor to create the RegionItem object
 /*-----------------------------------------------------------------------*
  * CONSTRUCTOR RegionItem:                                               *
  *   Params:  p_strPrompt          -  Prompt of Region Item (string)     *
  *            p_iPromptLength      -  Prompt Display Length (int)        *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  None                                                       *
  *     Desc:  Creates a Region Item object with the attributes assigned  *
  *            to the parameters passed                                   *
  *-----------------------------------------------------------------------*/
  function RegionItem(p_strPrompt, 
                      p_iPromptLength,
                      p_iValueLength,
                      p_strUpdateFlag,
                      p_strRequiredFlag) {
    this.strPrompt       = p_strPrompt;
    this.iPromptLength   = p_iPromptLength;
    this.iValueLength    = p_iValueLength;
    this.strUpdateFlag   = p_strUpdateFlag;
    this.strRequiredFlag = p_strRequiredFlag;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mstrGetPrompt:                                           *
  *   Params:  None                                                          *
  *  Returns:  Prompt of Region Item (string)                                *
  *     Desc:  Returns the Prompt of the Region Item object                  *
  *--------------------------------------------------------------------------*/
  function mstrGetPrompt() {
    return this.strPrompt;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD miGetPromptLength:                                       *
  *   Params:  None                                                          *
  *  Returns:  Prompt Display Length of Region Item (int)                    *
  *     Desc:  Returns the Prompt Length of the Region Item object           *
  *--------------------------------------------------------------------------*/
  function miGetPromptLength() {
    return this.iPromptLength;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD miGetValueLength:                                        *
  *   Params:  None                                                          *
  *  Returns:  Value Display Length of Region Item (int)                     *
  *     Desc:  Returns the Value Length of the Region Item object            *
  *--------------------------------------------------------------------------*/
  function miGetValueLength() {
    return this.iValueLength;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mtrGetUpdateFlag:                                        *
  *   Params:  None                                                          *
  *  Returns:  'Y' or 'N'                                                    *
  *     Desc:  Returns the Update Flag indicating if the field is updateable *
  *            or not 'Y' if updateable and 'N' if not                       *
  *--------------------------------------------------------------------------*/
  function mstrGetUpdateFlag() {
    return this.strUpdateFlag;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mtrGetRequiredFlag:                                      *
  *   Params:  None                                                          *
  *  Returns:  'Y' or 'N'                                                    *
  *     Desc:  Returns the Required Flag indicating if the field is required *
  *            or not, 'Y' if required and 'N' if not                        *
  *--------------------------------------------------------------------------*/
  function mstrGetRequiredFlag() {
    return this.strRequiredFlag;
  }

 /*-----------------------------------------------------------------------*
  * ACCESSOR METHOD mGetAttributes:                                       *
  *   Params:  p_strPrompt         -  Prompt of Region Item (string)      *
  *            p_iPromptLength     -  Prompt Display Length (int)         *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  None                                                       *
  *     Desc:  All the parameters are out parameters.  Function, returns  *
  *            all the attributes of the Region Item object by assigning  *
  *            them to the parameters.                                    *
  *-----------------------------------------------------------------------*/
  function mGetAttributes(p_strPrompt, 
                          p_iPromptLength,
                          p_iValueLength,
                          p_strUpdateFlag,
                          p_strRequiredFlag) {
    p_strPrompt       = this.iValueLength;
    p_iPromptLength   = this.strPrompt;
    p_iValueLength    = this.iValueLength;
    p_strUpdateFlag   = this.strUpdateFlag;
    p_strRequiredFlag = this.strRequiredFlag;
  }

  RegionItem.prototype.mstrGetPrompt       = mstrGetPrompt;
  RegionItem.prototype.miGetPromptLength   = miGetPromptLength;
  RegionItem.prototype.miGetValueLength    = miGetValueLength;
  RegionItem.prototype.mstrGetUpdateFlag   = mstrGetUpdateFlag;
  RegionItem.prototype.mstrGetRequiredFlag = mstrGetRequiredFlag;
  RegionItem.prototype.mGetAttributes      = mGetAttributes;


/*===========================================================================*
 *  LookupTypes Class - Container of Lookup Types                            *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR LookupTypes:                 *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a LookupTypes object  *
  *------------------------------------------*/ 
  function LookupTypes() {
    this.arrLookupTypes = new Object();
  }

 /*---------------------------------------------------------------*
  * METHOD mobjAddLookupType:                                     *
  *   Params:  p_strLookupType -  Lookup Type of type string      *
  *  Returns:  Lookup Type Object added to Lookup Types Object    *
  *     Desc:  Creates a Lookup Type object with name             *
  *            p_strLookup Type and adds it to the LookupTypes    *
  *            object and returns the new Lookup Type             *
  *---------------------------------------------------------------*/
  function mobjAddLookupType(p_strLookupTypeName) {
    this.arrLookupTypes[p_strLookupType] = new LookupType();
    return this.arrLookupTypes[p_strLookupType];
  }

 /*---------------------------------------------------------------------*
  * ACCCESSOR METHOD mobjLookupType:                                    *
  *   Params:  p_strLookupTypeName -  Lookup Type of type string        *
  *  Returns:  Lookup Type Object or null                               *
  *     Desc:  Returns the Lookup Type object with the name             *
  *            p_strLookupType.  If the region does not exist null is   *
  *            returned.                                                *
  *---------------------------------------------------------------------*/
  function mobjGetLookupType(p_strLookupType) {
    var undefined;
    if (this.arrLookupTypes[p_strLookupType] == undefined)
      return null;
    return this.arrLookupTypes[p_strLookupType];
  }

  LookupTypes.prototype.mobjAddLookupType  = mobjAddLookupType;
  LookupTypes.prototype.mobjGetLookupType  = mobjGetLookupType;
