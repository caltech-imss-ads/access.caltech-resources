<!-- hide the script's contents from feeble browsers                           
<!-- $Header: hrmecwfw.js 115.0 2000/01/20 12:49:33 pkm ship     $ -->             
<!-----------------------------------------------------------------------------
<!-- Change History                                                            
<!-- Version Date         Name          Bug #    Reason                        
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 
<!-- 110.11  13-OCT-1999  rajayara               Fixed  resubmit issue for
<!--                                             review page
<!-- 110.10  14-JUL-1999  rajayara               Added refresh_header to 
<!--                                             Enable print on confirm page 
<!-- 110.7   29-JUN-1999  rajayara      919289   Fixed the cancel event.
<!-- 110.0   17-Jun-1999  rajayara               Created.                      
<!-----------------------------------------------------------------------------
<!-----------------------------------------------------------------------------
<!-- Workflow module specific js file.                                      
<!--                                                                           
<!-----------------------------------------------------------------------------
                                                                               
function refresh_header (p_container_header_frame) {
  if (p_container_header_frame) {
    if (top.header.location.href != p_container_header_frame) {
      top.header.location.href = p_container_header_frame;
    }
  }
}
// The following function is invoked from the
// header frame when the user clicks on the 
// 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.exit_form.p_exit_from.value="main_menu_onClick";
  if (document.PendingForm.exists.value!="Y")
      { 
         if (confirm(msg))
            {
             document.exit_form.submit();
            }
       }
   else
      {
     top.container_middle.goto_main_menu ('hr_util_disp_web.hr_main_menu_page');
      }
}

//Set form has been changed
function set_form_changed () {
  document.exit_form.p_form_changed.value="Y";
}

// The following function is invoked from the
// header frame when the user clicks on the 
// 'Save' gif in the toolbar.
function save_gif_onClick () {
  alert ('save_gif_onClick');
}

// The following function is invoked from the
// header frame when the user clicks on the 
// 'Print' gif in the toolbar.
function print_gif_onClick () {
//  alert ('print_gif_onClick');
  print_confirm_page()
}

// The following function is invoked from the
// header frame when the user clicks on the 
// 'Help' gif (Question Mark) in the toolbar.
function help_gif_onClick () {
  alert ('help_gif_onClick');
}

// The following function is invoked from the 
// container_bottom frame when the user clicks
// on the 'Reset Page' button.
function reset_button_onClick () { 
//  alert ('reset_button_onClick');
  document.workspaceForm.reset();
}


// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Back' button.
function back_button_onClick () {
//  alert ('back_button_onClick');
  document.workspaceForm.p_result_code.value = 'PREVIOUS';
  top.clear_container_bottom_frame();
  document.workspaceForm.submit();
}

// container_bottom frame when the user clicks
// on the 'Cancel' button.
function cancel_button_onClick (msg) {
//   alert ('cancel_button_onClick');
  document.workspaceForm.p_result_code.value = 'CANCEL';
    document.exit_form.p_exit_from.value="cancel_button_onClick";
         if (confirm(msg))
            {
             top.clear_container_bottom_frame();
             document.exit_form.submit();
            }
  }	


// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Next' button.
function next_button_onClick () {
//  alert ('next_button_onClick');
//  document.workspaceForm.p_redraw.value ="N";
  top.clear_container_bottom_frame();
  document.workspaceForm.p_result_code.value = 'NEXT';
  document.workspaceForm.submit();
}
function submit_button_onClick () {
//  alert ('next_button_onClick');
//  document.workspaceForm.p_redraw.value ="N";
  top.clear_container_bottom_frame();
  document.workspaceForm1.p_result_code.value = 'NEXT';
  document.workspaceForm1.submit();
}
// This function is called to go back to the main menu
// pointed to by loc.
function goto_main_menu (loc) {
  parent.location.href = loc;
} 

function set_redraw_flag () {
  alert ('set redraw flag');
  document.workspaceForm.p_redraw.value ="Y";
  document.workspaceForm.submit();
}
function printFrame(wind) {
        // no single frame printing available for Mac
        if (Win32) {
                if (Nav4) {
                        wind.print()
                } else {
          // traps all script error messages hereafter until page reload
                        window.onerror = doNothing
                        // make sure desired frame has focus
                        wind.focus()
    // change second parameter to 2 if you don't want the print dialog to appear
                        IEControl.ExecWB(6, 1)
                }
        } else {
              alert("Sorry. Printing is available only from Windows 95/98/NT.")
        }
}


function print_confirm_page(){                                     
//  javascript:printFrame(top.container_middle)                    
//  javascript:top.container_middle.print();                       
  print();                                                         
}                                                                  

function submit_comment(){                                         
    if ( document.commentDialogForm.p_comm.value.length < 241) 
       {                                                           
         if (document.workspaceForm.change.value=="Y")                  
	 {   
          document.commentDialogForm.submit();                      
          alert('The changes have been saved');
          parent.opener.top.container_middle.document.Redisplay.submit();
          //setTimeout('window.close()',6000);                                
          window.close();
          } else {                                                        

          //setTimeout('window.close()',6000);                                
          window.close();
          } 
	}else {
          alert ('You have excceded the 240 char limit. Please reduce the comment length');   

       }
}
                                                                  
function set_change(){                                             
  document.workspaceForm.change.value ="Y";                        
}                                                                  

function cancel_comment(msg){
         if (document.workspaceForm.change.value=="Y")                  
           { if (confirm(msg))                   
              {                                
              window.close();
              }
           } else {
             window.close();                                
  
           }
}
