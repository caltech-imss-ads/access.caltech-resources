<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : DHTMLapi.js
// Description : Utility functions to handle difference between browsers
//               and simplify module specific libraries with functions for
//               cross-platform object positioning
//
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------  -----------  ------- ------- ---------------------------------
// pbrimble    10-Feb-2001  110.01  1637198 Support for IE5.x added.
// pbrimble    02-Dec-1999  110.00          Created.
// bdivvela    19-Mar-2002  115.3   2245138 Support for IE6.x in 
//                                          function shiftOnHorizScroll
// ==========================================================================
// $Header: DHTMLapi.js 115.3 2002/03/19 02:05:32 pkm ship        $
//

// DHTMLapi.js custom API for cross-platform object positioning

// Global variables

// Browser detection

//declare variable browser as a new Object
var browser = new Object();

//set isNetscape property of browser to false
browser.isNav = false;

//set isIE property of browser to false
browser.isIE = false;

var coll = ""
var styleObj = ""
if (parseInt(navigator.appVersion) >= 4) {
    if (navigator.appName == "Netscape"){
        browser.isNav = true
        browser.version = parseInt(navigator.appVersion)
    } else {
        browser.isIE  = true
        coll     = ".all"
        styleObj = ".style"
        if (navigator.appVersion.indexOf("MSIE 5") != -1) {
            browser.version = 5;
        } else {
            browser.version = parseInt(navigator.appVersion);
        }
    }
}

// Convert object name string or object reference into a
// valid object reference
function getObject(obj) {
    var theObj
    if (typeof obj == "string") {
        theObj = eval("document" + coll + "." + obj + styleObj)
    } else {
        theObj = obj
    }
    return theObj
}

// Positioning an object at a specific pixel coordinte
function shiftTo(obj, x, y) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        theObj.moveTo(x,y)
    } else {
        theObj.pixelLeft = x
        theObj.pixelTop  = y
    }
}

// Moving an object by x and/or y pixels
function shiftBy(obj, deltaX, deltaY) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        theObj.moveBy(deltaX, deltaY)
    } else {
        theObj.pixelLeft += deltaX
        theObj.pixelTop  += deltaY
    }
}

// Setting the z-order of an object
function setZIndex(obj, zOrder) {
    var theObj = getObject(obj)
    theObj.zIndex = zOrder
}

// Setting the background color of an object
function setBGColor(obj,color) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        theObj.bgColor = color
    } else {
        theObj.backgroundColor = color
    }
}

// Setting the visibility of an object to hidden
function hide(obj) {
    var theObj = getObject(obj)
    theObj.visibility = "hidden"
}

// Setting the visibility of an object to visable
function show(obj) {
    var theObj = getObject(obj)
    theObj.visibility = "visible"
}

// Retrieving the x coordinate of a positionable object
function getObjectLeft(obj) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        return theObj.left
    } else {
        return theObj.pixelLeft
    }
}

// Retrieving the y coordinate of a positionable object
function getObjectTop(obj) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        return theObj.top
    } else {
        return theObj.pixelTop
    }
}

// Utility function returns the available content width space in browser
function getInsideWindowWidth() {
    if (browser.isNav) {
        return window.innerWidth
    } else {
        return document.body.clientWidth
    }
}

// Utility function returns the available content height space in browser
function getInsideWindowHeight() {
    if (browser.isNav) {
        return window.innerHeight
    } else {
        return document.body.clientHeight
    }
}

// Utility function returns the current horizontal position of scrolling
function getLeftScrollPos() {
    if (browser.isNav) {
        return window.pageXOffset
    } else {
        return window.document.body.scrollLeft
    }
}

// Utility function returns the current vertical position of scrolling
function getTopScrollPos() {
    if (browser.isNav) {
        return window.pageYOffset
    } else {
        return window.document.body.scrollTop
    }
}

// Utility function returns the rendered width of an object content in pixels xxxx
function getObjWidth(obj) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        return theObj.clip.width
    } else {
        return theObj.clientWidth
    }
}

// Utility function returns the rendered height of an object content in pixels xxxx
function getObjHeight(obj) {
    var theObj = getObject(obj)
    if (browser.isNav) {
        return theObj.clip.height
    } else {
        return theObj.clientHeight
    }
}

//I don't think that you can overload a javascript function
//
//function shiftOnHorizScroll(obj) {
//    var theObj = getObject(obj)
//    if ((browser.isNav) || (browser.isIE && browser.version >= 5)) {
//        theObj.left = getLeftScrollPos()
//    } else {
//        theObj.left = 0
//    }
//}

//
function getLayerObject(obj) {
    var theObj
    if (typeof obj == "string") {
        theObj = eval("document" + coll + "[\"" + obj + "\"]" + styleObj)
    } else {
        theObj = obj
    }
    return theObj
}

// Moving an object to an Offset from the Left of the screen
//   when Scrolling Horizontally
function shiftOnHorizScroll(obj, leftOffset) {
    var theObj = getObject(obj)
// fix 2245138. IE6 gives appVersion as 4.0, but returns string containing MSIE 6. 
if ((browser.isNav) || (browser.isIE && browser.version >= 5) ||(navigator.appVersion.indexOf("MSIE 6.") >0)   ) {
        theObj.left = (getLeftScrollPos() + leftOffset)
    } else {
        //alert ("You are on IE 4: " + getLeftScrollPos() + " - Window Width: " + getInsideWindowWidth())
        theObj.left = (getLeftScrollPos()-getInsideWindowWidth()+320)
    }
}
