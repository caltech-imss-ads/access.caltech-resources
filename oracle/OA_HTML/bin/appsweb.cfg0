; Forms Web CGI Configuration File for Oracle Applications 11i
; $Header: appsweb.cfg 115.100 2001/08/16 12:49:06 pkm ship    $
; ------------------------------------------------------------
; This file defines parameter values used by the Forms Web CGI.
; These parameter values are defined on install.  Customizations
; are possible by modifying sections at the end of this file.
; Environment administrators should familiarize themselves with
; this file and its three sections:
; - environment specific parameters,
; - default parameter values,
; - specific configurations.

; ********************************
; ENVIRONMENT SPECIFIC PARAMETERS
; ********************************
; These parameters describe the main production environment.
; They have to be updated after every patching of this file.
;
; Forms Server Information: port, machine name and domain
; -------------------------------------------------------
serverPort=8013
serverName=fiji
domainName=.caltech.edu
; If using Oracle Forms load balancing, set your serverName to 
; serverName=%LeastLoadedHost%
; The following Metrics Server parameters define where the Forms Web CGI
; cartridge should obtain the name of the least loaded Forms Server.
; The default settings leave these parameters blank.
MetricsServerPort=3013
MetricsServerErrorURL= 
; Environment Name is shown in Forms session browser startup window
envName=
; Splash Screen: displayed as a separate window on startup.
; oracle/apps/media/splash.gif is the default Oracle Applications
; splash screen (11.5.2+). 
; Customers can customize this parameter by setting it to their 
; icon's name and providing the icon in the $JAVA_TOP mapped to
; by the web server's OA_JAVA directory.
splashScreen=oracle/apps/media/splash.gif
;
; Forms Client-Server Communication Mode: socket, http, or https
; --------------------------------------------------------------
; Oracle Applications is recommending use of socket mode for intranet
; use, and https for internet use. Check Metalink for current issues.
connectMode=socket
;
; Database Connection Parameters 
; ------------------------------
userid=APPLSYSPUB/PUB@CNTRL
fndnam=apps
;
; JInitiator Parameters
; ---------------------
; The following parameters relate to the version of JInitiator.
; !!! IMPORTANT !!!
; When patching this file, you must update these parameters to reflect
; the JInitiator version you are using in you environment. Follow
; Metalink Apps11i Alert "Upgrading the JInitiator version used with
; Oracle Applications 11i" (Note:124606.1)
jinit_ver_name=Version=1,1,8,19
jinit_mimetype=application/x-jinit-applet;version=1.1.8.19
jinit_classid=clsid:5e2a3510-4371-11d6-b64c-00c04faedb18
;
; Runform Parameters (NT platform ONLY)
; ------------------
; For Forms Servers running on NT platforms, the prodTop variable 
; needs to be set to a FND_TOP-like value: all backslashes (\) should 
; be modified to forward slashes (/). 
; For example if FND_TOP is D:\oracle\prodappl\fnd\11.5.0 , then 
; prodTop should be set to prodTop=D:/oracle/prodappl/fnd/11.5.0 . 
; If using platforms other than NT leave the default:
prodTop=/x00/oracle/CNTRLappl/fnd/11.5.0
; For more details see Runform Arguments section of Default Parameters.


; ************************
; DEFAULT PARAMETER VALUES
; ************************
; It is not recommended to modify these unless requested by Oracle.

; SYSTEM PARAMETERS
; -----------------
; These parameters have fixed names and give information required by the
; Forms Web CGI in order to function.  They cannot be specified in the 
; URL query string, but they can be overridden in a named configuration 
; (see sections below).
baseHTML=%OA_HTML%/US/appsbase.htm
baseHTMLJInitiator=%OA_HTML%/US/appsbase.htm
HTMLdelimiter=%
; The next parameter (IE50) specifies which JVM is used to execute the 
; Forms applet under Microsoft Internet Explorer 5.0. 
IE50=JInitiator

; ORACLE APPLICATIONS PARAMETERS
; ------------------------------
; These match variables (e.g. %FORM%) in the baseHTML file. Their values 
; may be overridden by specifying them in the URL query string
; (e.g. "http://myhost.mydomain.com/ifcgi60.exe?form=myform&width=700")
; or by overriding them in a specific, named configuration (see below)

; 1) Runform Arguments
; --------------------
; The module argument defines the first form to be started.
; It is composed from parameters %prodTop%/forms/%lang%/%formName%
; The default looks like $APPL_TOP/fnd/<version>/forms/US/FNDSCSGN
; Following parameters and prodTop are used for composing module.
; Note: Personal Home Page modifies the lang setting automatically.
formName=FNDSCSGN
lang=US
;
; Server Application default is 'OracleApplications'
serverApp=OracleApplications
;
; Registry Path defines the location for .dat file
; default is '/OA_JAVA/oracle/apps/fnd/formsClient'
registryPath=/OA_JAVA/oracle/apps/fnd/formsClient
;
; Other Forms Server Arguments
env=
form_params=

; 2) Java Client Code Parameters
; ------------------------------
; Codebase defines the location of Java code top;
; default value is /OA_JAVA
codebase=/OA_JAVA/
;
; Code defines the first Java class to be executed;
; default value is 'oracle.forms.engine.Main'
code=oracle.forms.engine.Main
;
; JAR Files
; ---------
; Client java code is distributed via JAR files.
; The order of jar files listed is important, as on-demand-loading is the
; default. For more JAR file loading options see below.
;
; Core JAR files used by all Forms-based products
archive=/OA_JAVA/oracle/apps/fnd/jar/fndforms.jar,/OA_JAVA/oracle/apps/fnd/jar/fndformsi18n.jar,/OA_JAVA/oracle/apps/fnd/jar/fndewt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndswing.jar,/OA_JAVA/oracle/apps/fnd/jar/fndbalishare.jar,/OA_JAVA/oracle/apps/fnd/jar/fndaol.jar,/OA_JAVA/oracle/apps/fnd/jar/fndctx.jar
; 
; The following JAR files are loaded on demand
; JAR files used for FND products
archive1=,/OA_JAVA/oracle/apps/fnd/jar/fndlist.jar
archive2=
archive3=
; Commenting the remainder of standard archive lists as they are
; no longer needed after applying AD patch for bug 1336029.
; archive1=,/OA_JAVA/oracle/apps/fnd/jar/fndlist.jar,/OA_JAVA/oracle/apps/fnd/jar/fndnetcharts.jar,/OA_JAVA/oracle/apps/fnd/jar/fndtdg.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjgl.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjle.jar,/OA_JAVA/oracle/apps/fnd/jar/fndlrucache.jar,/OA_JAVA/oracle/apps/fnd/jar/fndgantt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndpromise.jar,/OA_JAVA/oracle/apps/fnd/jar/fndewtpv.jar,/OA_JAVA/oracle/apps/fnd/jar/fndutil.jar,/OA_JAVA/oracle/apps/fnd/jar/fndtcf.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhier.jar,/OA_JAVA/oracle/apps/fnd/jar/fndfsec.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhelpc.jar,/OA_JAVA/oracle/apps/fnd/jar/wfmon.jar,/OA_JAVA/oracle/apps/fnd/jar/wfapi.jar,/OA_JAVA/oracle/apps/fnd/jar/wffrm.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhelp.jar,/OA_JAVA/oracle/apps/fnd/jar/fndsec.jar,/OA_JAVA/oracle/apps/fnd/jar/fndicebrwsr.jar,/OA_JAVA/oracle/apps/fnd/jar/fndprospect.jar,/OA_JAVA/oracle/apps/fnd/jar/fndtsgraph.jar,/OA_JAVA/oracle/apps/fnd/jar/fndaroraclnt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjewt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndvbj.jar,/OA_JAVA/oracle/apps/fnd/jar/fndswingall.jar,/OA_JAVA/oracle/apps/fnd/jar/fndrt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndmxt20.jar,/OA_JAVA/oracle/apps/fnd/jar/fnddpx20.jar
;
; JAR files for non-FND ERP products (the list starts with a comma)
; archive2=,/OA_JAVA/oracle/apps/ak/jar/akobjnav.jar,/OA_JAVA/oracle/apps/az/jar/azwizard.jar,/OA_JAVA/oracle/apps/wip/jar/wippcb.jar,/OA_JAVA/oracle/apps/wip/jar/wiputil.jar,/OA_JAVA/oracle/apps/wps/jar/wpsgantt.jar,/OA_JAVA/oracle/apps/xla/jar/xlatacct.jar,/OA_JAVA/oracle/apps/mrp/jar/mrpjar.jar,/OA_JAVA/oracle/apps/msc/jar/mscjar.jar,/OA_JAVA/oracle/apps/per/jar/performs.jar,/OA_JAVA/oracle/apps/bis/jar/bischart.jar,/OA_JAVA/oracle/apps/bom/jar/bomjar.jar,/OA_JAVA/oracle/apps/vea/jar/vea.jar,/OA_JAVA/oracle/apps/gl/jar/glgcs.jar,/OA_JAVA/oracle/apps/cz/jar/czclient.jar,/OA_JAVA/oracle/apps/per/jar/peradmin.jar,/OA_JAVA/oracle/apps/wip/jar/wipmedia.jar
;
; JAR files for CRM products  (the list starts with a comma)
; archive3=,/OA_JAVA/oracle/apps/asg/jar/asg.jar,/OA_JAVA/oracle/apps/asg/jar/asgmtran.jar,/OA_JAVA/oracle/apps/ast/jar/asthgrid.jar,/OA_JAVA/oracle/apps/ast/jar/astuijav.jar,/OA_JAVA/oracle/apps/cct/jar/cctotm.jar,/OA_JAVA/oracle/apps/cct/jar/cctnet.jar,/OA_JAVA/oracle/apps/cct/jar/cctsoft.jar,/OA_JAVA/oracle/apps/cct/jar/cctroute.jar,/OA_JAVA/oracle/apps/csf/jar/csfmap.jar,/OA_JAVA/oracle/apps/csf/jar/csfchart.jar,/OA_JAVA/oracle/apps/csf/jar/csflf.jar,/OA_JAVA/oracle/apps/csf/jar/csftds.jar,/OA_JAVA/oracle/apps/csr/jar/csrclient.jar,/OA_JAVA/oracle/apps/ibu/jar/ibu.jar,/OA_JAVA/oracle/apps/iem/jar/iemsrv.jar,/OA_JAVA/oracle/apps/iem/jar/iemadm.jar,/OA_JAVA/oracle/apps/iem/jar/iemegen.jar,/OA_JAVA/oracle/apps/iem/jar/iemedit.jar,/OA_JAVA/oracle/apps/iem/jar/iemclnt.jar,/OA_JAVA/oracle/apps/iem/jar/iemapplt.jar,/OA_JAVA/oracle/apps/ies/jar/iescommn.jar,/OA_JAVA/oracle/apps/ies/jar/iesclien.jar,/OA_JAVA/oracle/apps/ies/jar/iesservr.jar,/OA_JAVA/oracle/apps/ieu/jar/ieunet.jar,/OA_JAVA/oracle/apps/ieu/jar/ieustuba.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuui.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuclient.jar,/OA_JAVA/oracle/apps/ieu/jar/ieucommon.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuutil.jar,/OA_JAVA/oracle/apps/ieu/jar/ieutrans.jar,/OA_JAVA/oracle/apps/iex/jar/iexdbjav.jar,/OA_JAVA/oracle/apps/iex/jar/iexbeans.jar,/OA_JAVA/oracle/apps/jtf/jar/jtfui.jar,/OA_JAVA/oracle/apps/jtf/jar/jtfgrid.jar,/OA_JAVA/oracle/apps/jtf/jar/jtfgantt.jar,/OA_JAVA/oracle/apps/xnp/jar/xnpadptr.jar,/OA_JAVA/oracle/apps/csc/jar/csc.jar,/OA_JAVA/oracle/apps/cs/jar/cs.jar,/OA_JAVA/oracle/apps/ast/jar/astiesbn.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuagent.jar,/OA_JAVA/oracle/apps/flm/jar/flmkbn.jar,/OA_JAVA/oracle/apps/flm/jar/flmrtg.jar,/OA_JAVA/oracle/apps/fpt/jar/fpties.jar
;
; JAR files for Macintosh
; -----------------------
; JAR files for FND products for Macintosh
macarchive=/OA_JAVA/oracle/apps/fnd/jar/fndforms.jar,/OA_JAVA/oracle/apps/fnd/jar/fndformsi18n.jar,/OA_JAVA/oracle/apps/fnd/jar/fndewt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndswing.jar,/OA_JAVA/oracle/apps/fnd/jar/fndbalishare.jar,/OA_JAVA/oracle/apps/fnd/jar/fndaol.jar,/OA_JAVA/oracle/apps/fnd/jar/fndctx.jar,/OA_JAVA/oracle/apps/fnd/jar/fndnetcharts.jar,/OA_JAVA/oracle/apps/fnd/jar/fndtdg.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjle.jar,/OA_JAVA/oracle/apps/fnd/jar/fndlrucache.jar,/OA_JAVA/oracle/apps/fnd/jar/fndgantt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndpromise.jar,/OA_JAVA/oracle/apps/fnd/jar/fndutil.jar,/OA_JAVA/oracle/apps/fnd/jar/fndtcf.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhier.jar,/OA_JAVA/oracle/apps/fnd/jar/fndfsec.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhelpc.jar,/OA_JAVA/oracle/apps/fnd/jar/fndhelp.jar,/OA_JAVA/oracle/apps/fnd/jar/fndsec.jar,/OA_JAVA/oracle/apps/fnd/jar/fndicebrwsr.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjewt.jar,/OA_JAVA/oracle/apps/fnd/jar/fndmxt20.jar,/OA_JAVA/oracle/apps/fnd/jar/fnddpx20.jar,/OA_JAVA/oracle/apps/fnd/jar/fndjgl.jar
;
; JAR files for non-FND ERP products for Macintosh (list starts with a comma)
macarchive2=,/OA_JAVA/oracle/apps/ak/jar/akobjnav.jar,/OA_JAVA/oracle/apps/az/jar/azwizard.jar,/OA_JAVA/oracle/apps/wip/jar/wippcb.jar,/OA_JAVA/oracle/apps/wip/jar/wiputil.jar,/OA_JAVA/oracle/apps/wps/jar/wpsgantt.jar,/OA_JAVA/oracle/apps/xla/jar/xlatacct.jar,/OA_JAVA/oracle/apps/mrp/jar/mrpjar.jar,/OA_JAVA/oracle/apps/msc/jar/mscjar.jar,/OA_JAVA/oracle/apps/per/jar/performs.jar,/OA_JAVA/oracle/apps/bom/jar/bomjar.jar,/OA_JAVA/oracle/apps/cz/jar/czclient.jar
;
; JAR files for CRM products for Macintosh (the list starts with a comma)
macarchive3=,/OA_JAVA/oracle/apps/asg/jar/asg.jar,/OA_JAVA/oracle/apps/asg/jar/asgmtran.jar,/OA_JAVA/oracle/apps/ast/jar/asthgrid.jar,/OA_JAVA/oracle/apps/ast/jar/astuijav.jar,/OA_JAVA/oracle/apps/cct/jar/cctsoft.jar,/OA_JAVA/oracle/apps/csf/jar/csfmap.jar,/OA_JAVA/oracle/apps/csf/jar/csfchart.jar,/OA_JAVA/oracle/apps/csf/jar/csftds.jar,/OA_JAVA/oracle/apps/csr/jar/csrclient.jar,/OA_JAVA/oracle/apps/iem/jar/iemegen.jar,/OA_JAVA/oracle/apps/iem/jar/iemedit.jar,/OA_JAVA/oracle/apps/iem/jar/iemclnt.jar,/OA_JAVA/oracle/apps/iem/jar/iemapplt.jar,/OA_JAVA/oracle/apps/ies/jar/iescommn.jar,/OA_JAVA/oracle/apps/ies/jar/iesclien.jar,/OA_JAVA/oracle/apps/ies/jar/iesservr.jar,/OA_JAVA/oracle/apps/ieu/jar/ieunet.jar,/OA_JAVA/oracle/apps/ieu/jar/ieustuba.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuui.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuclient.jar,/OA_JAVA/oracle/apps/ieu/jar/ieucommon.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuutil.jar,/OA_JAVA/oracle/apps/iex/jar/iexbeans.jar,/OA_JAVA/oracle/apps/jtf/jar/jtfgrid.jar,/OA_JAVA/oracle/apps/jtf/jar/jtfgantt.jar,/OA_JAVA/oracle/apps/csc/jar/csc.jar,/OA_JAVA/oracle/apps/cs/jar/cs.jar,/OA_JAVA/oracle/apps/ast/jar/astiesbn.jar,/OA_JAVA/oracle/apps/ieu/jar/ieuagent.jar,/OA_JAVA/oracle/apps/flm/jar/flmkbn.jar,/OA_JAVA/oracle/apps/flm/jar/flmrtg.jar,/OA_JAVA/oracle/apps/fpt/jar/fpties.jar
;
; Other JAR File Parameters
; -------------------------
; Used by Oracle for debugging with JInitiator. Begining or ending commas
; are not required. DO NOT MODIFY unless so instructed by Oracle.
userjarfile=
; For Mac clients to use On-Demand Loading, switch to using the standard 
; archive parameters by setting the following parameter to true:
macodl=true
; If the following parameter is set to all, on-demand-loading
; is not used, and macarchive parameters (Forms-only) are used.
; This feature is desupported after 11.5.4, but should keep working
; for all JARs present as of 11.5.4 .
jarloading=

; 3) Values for the Forms applet parameters:
; ------------------------------------------
; Server-side proxy information. None by default; uncomment if used.
;proxyPort=myProxyPort
;proxyName=fiji
;
; UI parameters: DO NOT MODIFY unless so instructed by Oracle
colorScheme=blue
lookAndFeel=oracle
darkLook=true
readOnlyBackground=automatic
background=no
dontTruncateTabs=true
imageBase=CodeBase
more_form_params=
; The last parameter extends the Forms server arguments for ADA use.
;
; This is an Apple specific DotsPerInch setting.
macDPI=96

; Starts the Applet in a separate window if set to true, otherwise
; starts the applet within the window of the launch html file.
separateFrame=true
; If using separateFrame=false, the following parameters regulate
; the size of the section used by the applet.
; These parameters are currently ignored for the Forms applets,
; but are used for non-forms applets.
width=650
height=500


; 4) JInitiator Parameters
; ------------------------
; The following parameters defines the name and location of the 
; executable containing JInitiator. 
jinit_name=oajinit.exe
jinit_url=/jinitiator/
;
; Page displayed to Netscape users to allow them to download JInitiator.  
; If you create your own version, set this parameter to point to it.
jinit_download_page=/jinitiator/us/jinit_download.htm


; 5) HTML Parameters
; ------------------
; HTML page title, attributes for the BODY tag, and HTML to add before
; and after the form. 
HTMLpageTitle=Oracle Applications 11i
HTMLbodyAttrs=
HTMLpreApplet=<IMG SRC="/OA_JAVA/oracle/apps/media/logo.gif" border=0><br clear=all>
HTMLpreApplet1=
HTMLpreApplet2=
HTMLpreApplet3=
HTMLpostApplet=<P>Copyright &#169;1998, 1999, 2000, 2001 <a href="http://www.oracle.com">Oracle Corporation</a>
HTMLpostApplet1=
HTMLpostApplet2=
HTMLpostApplet3=


; 6) Non-Forms (Generic) Applet Parameters
; ----------------------------------------
; Placeholder parameters for supporting launching of non-Forms applets.
; The default mode is to support Forms client applet.
; If appletmode is set to nonforms, the below parameters are used and
; Forms parameters are not used.
appletMode=forms
; Applet name is defined through the following parameter.
appletName=NonForms
; gp (GenericParameter) and gv (GenericValue) are generic parameter 
; variables.  In the applet definition HTML gp[x] will be set to gv[x],
; and for x>=10 , to gv[x] + gv[x]1 + gv[x]2 + gv[x]3 .
; This way we allow for long variable values even though each variable 
; is allowed to be up to 255 characters.
gp1=
gv1=
gp2=
gv2=
gp3=
gv3=
gp4=
gv4=
gp5=
gv5=
gp6=
gv6=
gp7=
gv7=
gp8=
gv8=
gp9=
gv9=
gp10=
gv10=
gv101=
gv102=
gv103=
gp11=
gv11=
gv111=
gv112=
gv113=
gp12=
gv12=
gv121=
gv122=
gv123=
gp13=
gv13=
gv131=
gv132=
gv133=
gp14=
gv14=
gv141=
gv142=
gv143=
gp15=
gv15=
gv151=
gv152=
gv153=
gp16=
gv16=
gv161=
gv162=
gv163=


; 7) Special Functionality Parameters
; -----------------------------------
; Record parameter values include:
; - performance : records server events timings into log file
; - collect     : records Runtime Diagnostic data into log file
; - all         : records Diagnostic and Performance data
; - names       : adds UI names to messeges, no log generated
; - pecs        : old performance data saved into log file
; Otherwise no recording takes place.
record=  
;
; Log parameter specifies log file location and name. If no
; log value is specified the default is CollectionType_PID.log
log= 
;
; Browser parameter is the executable called for browser
; functionality if appletviewer is used (debugging only).
; Note that this variable does not override the automatic
; browser setting (clientBrowser parameter) when using JInitiator.
browser=netscape
;
; ORBdisableLocator is a CRM Scripting-specific parameter.
ORBdisableLocator=true
;
; Network Statistics can be displayed in the status bar. 
netStats=false
;
; Result of Forms startup JavaScript can be displayed in alert. 
htmlDebug=false
;
; Reports version of appsweb.cfg in produced HTML
appswebVersion=115.23
;
; Additional Parameter Names and Values
nparam1=
vparam1=
;
nparam2=
vparam2=
;
nparam3=
vparam3=
;
nparam4=
vparam4=


; 8) Special Configurations
; -------------------------
; These configurations separate the JAR files used by CRM and ERP products
; for those users who only use one or the other, but not both.
; If using only CRM applications, JAR files for non-FND  ERP products are 
; not needed. Simillarly, if using only ERP products, JAR files for
; CRM products are not needed. 
; When the above holds for all users the following two specific 
; configurations could be used as defaults.

[CRM]
archive2=
macarchive2=

[ERP]
archive3=
macarchive3=

;# ********************
;#
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
;#
;# ********************
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
;# Begin customizations
;#
;# ********************
;# Customizations below this line will be preserved if patching this
;# file via the instantiation utility.

; ***********************
; SPECIFIC CONFIGURATIONS
; ***********************
;  You may define your own specific, named configurations (sets of parameters)
;  by adding special sections as illustrated in the following examples.  
;  Note that you need only specify the parameters you want to change.  The 
;  default values (defined above) will be used for all other parameters.
;  Use of a specific configuration can be requested by including the text 
;  "config=<your_config_name>" in the query string of the URL used to run
;  a form.  For example, to use the sepwin configuration, your could issue
;  a URL like "http://myhost.mydomain.com/ifcgi60.exe?config=sepwin".

; Example 1: configuration to run forms in a separate browser window with 
;            "generic" look and feel (include "config=sepwin" in the URL)
[sepwin]
separateWindow=True
lookandfeel=Generic

; Example 2: configuration affecting users of MicroSoft Internet Explorer 5.0.
;            Forms applet will run under the browser's native JVM rather than 
;            using Oracle JInitiator.
[ie50native]
IE50=native

; Example 3: configuration forcing use of the base.htm base HTML file in all 
;            cases (means applet-style tags will always be generated and 
;            JInitiator will never be used).  
[applet]
baseHTMLJInitiator=

; Example 4: configuration to run the demos
;            PLEASE DO NOT REMOVE THIS EXAMPLE, !  
;            It is needed to run the Forms demos (if they are installed)
[demo]
pageTitle=Oracle Forms Server Demos
width=700
height=550
form=start60
userid=%Demos_ConnectString%
archive=f60all.jar, oracle_ice-4_03_1.jar
serverApp=/forms60demo/demo
lookAndFeel=oracle
colorScheme=teal

; Example 6: Oracle Applications Debug Configuration
; - logging turned on
; - network statistics
; - potentially debug jar files 
; - potentially other debug code
; - review JavaScript result for startup page (htmlDebug)
[debug]
envName=DEBUG
htmlDebug=true
record=all
netStats=true
;userjarfile=

; Example 7: Different JAR file configurations
; - All Needed JAR files, a.k.a. cup-of-tea mode
; - no On-Demand-Loading
[alljar]
jarloading=all

; ------------------------------------------
; Example 8: Customizing Oracle Applications
; ------------------------------------------
; - use the same appsweb.cfg for multiple environments
; - customize the JAR files' signature & JInitiator
; - customize the browser called from appletviewer
; - use load balancing
;[apps]


;# ********************
;#
;# End customizations
