<?xml version="1.0"?>
<!--$Header: ibapstng.xsl 115.9 2000/11/01 11:47:29 pkm ship   $-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

 <xsl:template match="/">
  <xsl:apply-templates select="//Campaign"/>
 </xsl:template>

 <xsl:template match="Campaign">
		  <xsl:apply-templates/>
 </xsl:template>

 <xsl:template match="AttachmentLst">
		  <xsl:apply-templates select="Attachment"/>
 </xsl:template>

 <xsl:template match="Attachment[starts-with(AttachmentType, 'IMG')]">
  <xsl:variable name="height">
    <xsl:value-of select="floor(child::DisplayHeight)"/>
  </xsl:variable>
  <xsl:variable name="width">
    <xsl:value-of select="floor(child::DisplayWidth)"/>
  </xsl:variable>
  <xsl:variable name="url">
    <xsl:value-of select="child::linkURL"/>
  </xsl:variable>
  <xsl:element name="a">
   <xsl:if test="boolean(normalize-space($url))">
     <xsl:attribute name="href">
       <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute>
   </xsl:if>
   <xsl:element name="img">
    <xsl:attribute name="src">
     <xsl:value-of select="child::FileLocation"/>
    </xsl:attribute>
    <xsl:attribute name="alt">
     <xsl:value-of select="child::AlternateText"/>
    </xsl:attribute>
    <!--add height attribute if non-zero and number-->
    <xsl:if test="not($height='NaN')">
      <xsl:if test="not($height='0')">
        <xsl:attribute name="height">
         <xsl:value-of select="$height"/>
        </xsl:attribute>
      </xsl:if>
    </xsl:if>
    <!--add width attribute if non-zero and number-->
    <xsl:if test="not($width='NaN')">
      <xsl:if test="not($width='0')">
        <xsl:attribute name="width">
         <xsl:value-of select="$width"/>
        </xsl:attribute>
      </xsl:if>
    </xsl:if>
    <xsl:attribute name="border">
      <xsl:text>0</xsl:text>
    </xsl:attribute>
   </xsl:element>
  </xsl:element>
 </xsl:template>

 <xsl:template match="Attachment[starts-with(AttachmentType,'TXT')]">
  <xsl:variable name="url">
    <xsl:value-of select="child::linkURL"/>
  </xsl:variable>
  <xsl:element name="a">
   <xsl:if test="boolean(normalize-space($url))">
     <xsl:attribute name="href">
       <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute>
   </xsl:if>
   <xsl:value-of select="child::DisplayText"/>
   </xsl:element>
 </xsl:template>

<xsl:template match="*"></xsl:template>

</xsl:stylesheet>





