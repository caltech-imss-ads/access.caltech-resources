<!-- hide the script's contents from feeble browsers
<!-- $Header: pejsasnw.js 115.0 99/07/18 15:29:35 porting sh $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date         Name          Bug #    Reason
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 110.0   28-AUG-1998  flemonni	         Created		
<!--
<!-- 110.1   24-Feb-1999  fychu         836155   In calculate_score function,
<!--                                             added code to check that if the
<!--                                             score is null, do not set 
<!--                                             "theForm.p_line_score[i].value"
<!--                                             field.  Otherwise, we'll get
<!--                                             "Nan is not defined" error in
<!--                                             NS3 browser.
<!-- 
<!-----------------------------------------------------------------------------
<!-----------------------------------------------------------------------------
<!-- count_field
<!-----------------------------------------------------------------------------
function count_field (form, field_name) {
  var  fieldcount = 0
  for (counter=0; counter<form.length; counter++) {
      if (form.elements[counter].name == field_name) {
        fieldcount = fieldcount + 1
      }
  }
  return (fieldcount)
}
<!-----------------------------------------------------------------------------
<!-- detect_field
<!-----------------------------------------------------------------------------
function detect_field (theForm, field_name) {
  return (count_field (theForm, field_name) > 0)
}
<!-----------------------------------------------------------------------------
<!-- detect_element
<!-----------------------------------------------------------------------------
function detect_element (theForm, theField) {
  var fieldcount = 0
  for (counter=0; counter<theForm.length; counter++) {
      if (theForm.elements[counter] == theField) {
        fieldcount = counter 
      }
  }
  return (fieldcount)
}
<!-----------------------------------------------------------------------------
<!-- char2value
<!-----------------------------------------------------------------------------
function char2value (selection) {
  var returnval = -1
  var theIndex = selection.text.indexOf(" - ")
  if (theIndex != -1) {
    returnval = 
        parseInt
          (selection.text.substring
            (0,theIndex)
          )
  } 
return returnval
}
<!-----------------------------------------------------------------------------
<!-- ASSESSMENT CALCULATION - CALCULATE_SCORE
<!-----------------------------------------------------------------------------
<!-- modified Rquance original
<!-----------------------------------------------------------------------------
function calculate_score(i, field) {
  var theForm = window.parent.frames[2].document.TransmitForm
<!--
<!-- find the line score field; indicates that scored assessment required
<!--
  if (detect_field (theForm, 'p_line_score')) {
    var ASSESSMENT_SCORE_REQUIRED = true
    var TOTAL_NUMBER_OF_ROWS = count_field (theForm, 'p_competence_id')
    if (TOTAL_NUMBER_OF_ROWS == 1) {
      var ONE_ROW = true
    }
    else var ONE_ROW = false
<!-----------------------------------------------------------------------------
<!-- what kind of score is required
<!--
    if (detect_field (theForm, 'p_average_score')) {
      var TOTAL_SCORE_FORMULA = 'AVERAGE_LINES'
      var theTotalField = theForm.p_average_score
    }
    else {
      var TOTAL_SCORE_FORMULA = 'SUM_LINES'
      var theTotalField = theForm.p_total_score
    }
<!-----------------------------------------------------------------------------
<!-- what kind of weighting is required
<!--
    if (detect_field (theForm, 'p_perf_weighting_level_id')) {
      var PERFORMANCE_USED_IN_SCORE = true
      var PROFICIENCY_USED_IN_SCORE = false
      var WEIGHTING_USED_IN_SCORE = true
      var LINE_SCORE_FORMULA_VALUE = "PERFORMANCE*WEIGHTING"
      if (ONE_ROW) {
        var thePerformanceField = 
		detect_element (theForm, theForm.p_performance_level_id)
        var theWeightingField = 
		detect_element (theForm, theForm.p_perf_weighting_level_id)
      }
      else {
        var thePerformanceField = 
		detect_element (theForm, theForm.p_performance_level_id[i])
        var theWeightingField = 
		detect_element (theForm, theForm.p_perf_weighting_level_id[i])
      }
    }
    else if (detect_field (theForm, 'p_prof_weighting_level_id')) {
      var PERFORMANCE_USED_IN_SCORE = false
      var PROFICIENCY_USED_IN_SCORE = true
      var WEIGHTING_USED_IN_SCORE = true
      var LINE_SCORE_FORMULA_VALUE = "PROFICIENCY*WEIGHTING"
      if (ONE_ROW) {
        var theProficiencyField = 
		detect_element (theForm, theForm.p_proficiency_level_id)
        var theWeightingField = 
		detect_element (theForm, theForm.p_prof_weighting_level_id)
      }
      else {
        var theProficiencyField = 
		detect_element (theForm, theForm.p_proficiency_level_id[i])
        var theWeightingField = 
		detect_element (theForm, theForm.p_prof_weighting_level_id[i])
      }
    }
    else {
       var PERFORMANCE_USED_IN_SCORE = true
       var PROFICIENCY_USED_IN_SCORE = true
       var WEIGHTING_USED_IN_SCORE = false
       var LINE_SCORE_FORMULA_VALUE = "PROFICIENCY*PERFORMANCE"
      if (ONE_ROW) {
        var theProficiencyField = 
		detect_element (theForm, theForm.p_proficiency_level_id)
        var thePerformanceField = 
		detect_element (theForm, theForm.p_performance_level_id)
      }
      else {
        var theProficiencyField = 
		detect_element (theForm, theForm.p_proficiency_level_id[i])
        var thePerformanceField = 
		detect_element (theForm, theForm.p_performance_level_id[i])
      }
    }
<!-----------------------------------------------------------------------------
<!-- determine the values to use in the calculation
<!--
    if (PROFICIENCY_USED_IN_SCORE) {
      selected_index = 
	theForm.elements[theProficiencyField].options.selectedIndex
      var PROFICIENCY  = 
	char2value 
          (
	    theForm.elements[theProficiencyField].options[selected_index]
          )
    }
    else {
      var PROFICIENCY = -1
    }
    if (PERFORMANCE_USED_IN_SCORE) {
      selected_index = 
	theForm.elements[thePerformanceField].options.selectedIndex
      var PERFORMANCE  = 
	char2value 
	  (
	   theForm.elements[thePerformanceField].options[selected_index]
	  )
    }
    else {
      var PERFORMANCE = -1
    }
    if (WEIGHTING_USED_IN_SCORE) {
      selected_index = 
	theForm.elements[theWeightingField].options.selectedIndex
      var WEIGHTING  = 
	char2value 
	  (
	   theForm.elements[theWeightingField].options[selected_index]
	  )
    }      
    else {
      var WEIGHTING = -1
    }
<!-----------------------------------------------------------------------------
    var not_null_rows = 0
    var null_rows = 0
    var total_score = 0
    var old_score
    var score
//
// Check if any of the scoring fields are null and if they are,
// set the line score for the row to null.
//
    if ((PROFICIENCY_USED_IN_SCORE &&  PROFICIENCY < 0) 
       || (PERFORMANCE_USED_IN_SCORE && PERFORMANCE < 0) 
       || (WEIGHTING_USED_IN_SCORE && WEIGHTING < 0)) {
      score = ""
    }
    else {
      score = eval(LINE_SCORE_FORMULA_VALUE)
    }
//
// update the line score field
//
    if (ONE_ROW) {
      if (score != "")
         theForm.p_line_score.value = score
    }
    else {
      if (score != "")
         theForm.p_line_score[i].value = score
    }
//
// Calculate the value for the overall assessment score.
// Calculate the value for the number of rows
//
    if (ONE_ROW) {
      not_null_rows++
      line_score = eval(theForm.p_line_score.value)
      if  (theForm.p_line_score.value.length== 0){
        total_score = ""
      }
      else {
        total_score = line_score
      }
    }
    else {
      for (j=0; j<TOTAL_NUMBER_OF_ROWS; j++) {
        if (theForm.p_line_score[j].value != "") {
          not_null_rows++
          line_score = eval(theForm.p_line_score[j].value)
          total_score  = eval("total_score  + line_score")
        }
        else {
          null_rows++
        }
      }
      if (TOTAL_SCORE_FORMULA == "AVERAGE_LINES") {
        if (not_null_rows != 0) {
          total_score = eval("total_score / not_null_rows")
          // The number needs to be rounded to 2 decimal places.
          total_score = ((Math.round(total_score*100))/100)
        }
      }
    }
//
    if (total_score == "0"){
      total_score =""
    }
//
    theTotalField.value = total_score
  }
}

<!-- done hiding from old browsers -->


