//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawmodal.js                       |
//  |                                                |
//  | Description: Utility functions in web time     |
//  |                                                |
//  | History:     Created by aandra 12/27/1999      |
//  |                                                |
//  +================================================+
/* $Header: pawmodal.js 115.1 2000/05/17 15:38:46 pkm ship      $ */

//  ---------------------------------------------------------------------------
//  ModalWins Class
//  Need to add to the array when a new modal window is opened.
//  We however cannot delete an array element, when the modal window
//  is closed. Hence we need attribute iModalWins to keep track of
//  the number of open modal windows irrespective of the length of the array
//  ---------------------------------------------------------------------------
function ModalWins() {
  this.iModalWins = 0;
  this.arrModalWin = new Array();
}

ModalWins.prototype.mAddModalWin 	= mAddModalWin;
ModalWins.prototype.mGetobjLastModalWin = mGetobjLastModalWin;
ModalWins.prototype.miGetNumModalWins 	= miGetNumModalWins;
ModalWins.prototype.mbIsModalWinOpen 	= mbIsModalWinOpen;
ModalWins.prototype.mRemoveModalWin 	= mRemoveModalWin;

// Add ModalWin object to the ModalWins array
function mAddModalWin(){
  this.iModalWins++;
  this.arrModalWin[this.iModalWins-1] = new ModalWin(this.iModalWins);
}

// Get info about the last Modal Window
function mGetobjLastModalWin() {
 return (this.arrModalWin[this.iModalWins-1])
}

// Return the number of modal windows open
function miGetNumModalWins() {
  return(this.iModalWins);
}

function mRemoveModalWin() {
  this.iModalWins--;
}

function mbIsModalWinOpen() { 
  if (this.miGetNumModalWins() > 1) {
    return true;
  }
  else {
    return false;
  }
}

//  ---------------------------------------------------------------------------
//  ModalWin Class
//  ---------------------------------------------------------------------------
function ModalWin(p_iLevel) {
  
  this.winModal = null;
  this.strName = mGenstrName(p_iLevel);
  this.strCodeLoc = mGenstrCodeLoc(p_iLevel);
  this.bIsClosed = true;
  this.bIsInitialized = false;
  this.arrIELinkForModalParent = new Array();
}

ModalWin.prototype.mGetwinModal			= mGetwinModal;
ModalWin.prototype.mGetstrName 			= mGetstrName;
ModalWin.prototype.mGetstrCodeLoc 		= mGetstrCodeLoc;
ModalWin.prototype.mGetbIsClosed 		= mGetbIsClosed;
ModalWin.prototype.mGetbIsInitialized 		= mGetbIsInitialized;
ModalWin.prototype.mGetarrIELinkForModalParent 	= mGetarrIELinkForModalParent;
ModalWin.prototype.mSetwinModal 		= mSetwinModal;
//ModalWin.prototype.mGenstrName 		= mGenstrName;
//ModalWin.prototype.mGenstrCodeLoc 		= mGenstrCodeLoc;
ModalWin.prototype.mSetbIsClosed 		= mSetbIsClosed;
ModalWin.prototype.mSetbIsInitialized 		= mSetbIsInitialized;


function mGetwinModal() {
  return this.winModal; // Window Handler
}

function mGetstrName() {
  return this.strName;
}

function mGetstrCodeLoc() {
  return this.strCodeLoc;
}

function mGetbIsClosed() {
  return this.bIsClosed;
}

function mGetbIsInitialized() {
  return this.bIsInitialized;
}

function mGetarrIELinkForModalParent() {
  return this.arrIELinkForModalParent;
}

function mSetwinModal(p_winHandler){
  this.winModal = p_winHandler;
}

function mGenstrName(p_iLevel) {
  return 'winModal' + p_iLevel; // Level1 modal window winModal1
}

function fIsOdd(p_iNumber) {
  if (eval(p_iNumber/2) < Math.round(p_iNumber/2)) 
	{ return true; }
  else 
	{return false; }
}

function mGenstrCodeLoc(p_iLevel) {

  var i;
  var l_strCodeLoc='';

  for (i=1; i<=p_iLevel; i++) {
    if (fIsOdd(i)) {
      l_strCodeLoc += 'top.'; }
    else {
      l_strCodeLoc += 'opener.'; }
  }
  return l_strCodeLoc;
}

function mSetbIsClosed(p_bIsClosed) {
  this.bIsClosed = p_bIsClosed;
}

function mSetbIsInitialized(p_bIsInitialized) {
  this.bIsInitialized = p_bIsInitialized;
}


// ------------------------------------------------------
//  Modal window functions are used to modalize the parent window
//  upon opening of a secondary window on top of it. 
//  Focus will be diverted to the secondary window if user tries
//  to click any widget or links on the parent window.
//  Uses classes ModalWin and ModalWins
// ------------------------------------------------------


// Event handler to prevent any Navigator widget action when modal window is active
function fModalWinPreventClick() {

  // If any modal wins exist
  if (g_objModalWins.mbIsModalWinOpen()) {

    // If the last modal window is not closed and the window was initialized earlier
    if (  ((g_objModalWins.mGetobjLastModalWin().mGetwinModal()==null) || 
	   (g_objModalWins.mGetobjLastModalWin().mGetwinModal().closed))
	&&(g_objModalWins.mGetobjLastModalWin().mGetbIsInitialized()==true)) { 

      g_objModalWins.mRemoveModalWin();  // Cleanup datastructure
      fModalWinUnBlockEvents(            // Unblock events for parent window
	g_objModalWins.mGetobjLastModalWin().mGetarrIELinkForModalParent() ,
	g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() ,
	g_objModalWins.mGetobjLastModalWin().mGetwinModal()); // Unblock events
    }
    else {
       // If modal window is not closed
       if (g_objModalWins.mGetobjLastModalWin().mGetbIsClosed()==false) {
         g_objModalWins.mGetobjLastModalWin().mGetwinModal().focus();  // Focus last open modal window
         return false;
      }
    }
  }
}


function fModalWinOnFocusPreventClick() {

  // If any modal wins exist
  if (g_objModalWins.mbIsModalWinOpen()) {

    // If the last modal window is not closed and the window was initialized earlier
    if (  ((g_objModalWins.mGetobjLastModalWin().mGetwinModal()==null) || 
	   (g_objModalWins.mGetobjLastModalWin().mGetwinModal().closed))
	&&(g_objModalWins.mGetobjLastModalWin().mGetbIsInitialized()==true)) { 

      g_objModalWins.mRemoveModalWin();  			// Cleanup datastructure
      fModalWinUnBlockEvents(            			// Unblock events for parent window
	g_objModalWins.mGetobjLastModalWin().mGetarrIELinkForModalParent() ,
	g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() ,
	g_objModalWins.mGetobjLastModalWin().mGetwinModal()); 
    }
    else {
       // If modal window is not closed
       if (g_objModalWins.mGetobjLastModalWin().mGetbIsClosed()==false) { 
         g_objModalWins.mGetobjLastModalWin().mGetwinModal().focus();  // Focus last open modal window
      }
    }
  }
}

// Wrapper to init function in fDrawFrameset, to draw contents.
// Need to set the variable bIsClosed before drawing contents
// Hence the wrapper
function fModalWinOnLoad(){
  g_objModalWins.mGetobjLastModalWin().mSetbIsClosed(false);
}

// Clean array of any modalwindows whose level is greater than current level
function fModalWinOnUnLoad(p_nCurrentModalWinLevel) {
  g_objModalWins.mGetobjLastModalWin().mSetbIsClosed(true);
  while (p_nCurrentModalWinLevel < top.g_objModalWins.miGetNumModalWins()) {
    g_objModalWins.mRemoveModalWin();
  }
}


function fModalWinOpen(p_nWidth, p_nHeight, p_nXcood, p_nYcood, p_funcOnLoad, p_strMode, p_strAction) {
  var l_strModalWin;
  var l_winModal = new Object();
  var l_funcOnUnLoad;
  var l_funcOnLoad; 

// Add info about top window if opening a first level ModalWin
  if (g_objModalWins.miGetNumModalWins()==0) {
    g_objModalWins.mAddModalWin();
    g_objModalWins.mGetobjLastModalWin().mSetwinModal(window);
  }

// Block events for the parent window
  fModalWinBlockEvents(
	g_objModalWins.mGetobjLastModalWin().mGetarrIELinkForModalParent() ,
	g_objModalWins.mGetobjLastModalWin().mGetstrCodeLoc() ,
	g_objModalWins.mGetobjLastModalWin().mGetwinModal()); //Block Events

  g_objModalWins.mAddModalWin();//Add Record to datastructure for the modal win to be opened
  g_objModalWins.mGetobjLastModalWin().mSetbIsInitialized(false);  // window not open yet, Set initialized to false 

// Open the window
  l_strModalWin = g_objModalWins.mGetobjLastModalWin().mGetstrName();
  if ( navigator.appName == "Netscape" ) {  // Open Window
  l_winModal = open('', l_strModalWin, 'resizable=no,width=' + p_nWidth + ',height=' + p_nHeight + ',screenX=' + p_nXcood + ',screenY=' + p_nYcood + '\'');
  }
  else {
  l_winModal = open('', l_strModalWin, 'resizable=no,width=' + p_nWidth + ',height=' + p_nHeight + ',left=' + p_nXcood + ',top=' + p_nYcood + '\'');
  }

  g_objModalWins.mGetobjLastModalWin().mSetbIsInitialized(true); // Set Initialized to true
  g_objModalWins.mGetobjLastModalWin().mSetwinModal(l_winModal); // Set window handler for the new window

// Set the onload and onunload event handlers
  l_funcOnLoad = "javascript:top.opener.fModalWinOnLoad()";
  l_funcOnUnload = "javscript:top.opener.fModalWinOnUnLoad(" + g_objModalWins.miGetNumModalWins() + ")";       

// Draw contents
  fDrawFrameSetPage(l_winModal, "auto", null, l_funcOnLoad, l_funcOnUnload, p_strMode);
  
}


function fModalWinClose() {
    g_objModalWins.mGetobjLastModalWin().mGetwinModal().close(); //Close the window
}


function fModalWinBlockEvents(p_arrIELinkForModalParent, p_strCodeLoc, p_winModalParent) {
  if ( navigator.appName == "Netscape" ) {
    p_winModalParent.captureEvents(Event.CLICK | Event.MOUSEDOWN | Event.MOUSEUP | Event.FOCUS ) 
    p_winModalParent.onclick = top.fModalWinPreventClick;
    p_winModalParent.onfocus = top.fModalWinOnFocusPreventClick;

  }
  else   {
    fDisableForms(p_winModalParent, p_arrIELinkForModalParent, true);      
  }
}

function fModalWinUnBlockEvents(p_arrIELinkForModalParent, p_strCodeLoc, p_winModalParent) {
  if ( navigator.appName == "Netscape" ) {
    p_winModalParent.releaseEvents(Event.CLICK | Event.MOUSEDOWN | Event.MOUSEUP | Event.FOCUS )
    p_winModalParent.onclick = null;
    p_winModalParent.onfocus = null;
  }
  else { // enable forms
    fEnableForms(p_winModalParent, p_arrIELinkForModalParent, true);
  }
}


function fDisableForms(p_winModalParent, p_varIELinkForModalParent, p_bParentCall)
{
  if (p_bParentCall) {
    g_iIELinkCounter = 0;
  }

// Disable each form element in the document
  for ( var i=0; i<p_winModalParent.document.forms.length; i++)  {
    for ( var j=0; j<p_winModalParent.document.forms[i].elements.length; j++)    {
      p_winModalParent.document.forms[i].elements[j].disabled = true;
    }
  }

  p_varIELinkForModalParent[g_iIELinkCounter] = new Array();

// Save old and assign on click event handler for all links in the document to fModalWinPreventClick
  for ( i=0; i< p_winModalParent.document.links.length; i++)  {
    p_varIELinkForModalParent[g_iIELinkCounter][i] = p_winModalParent.document.links[i].onclick;
    p_winModalParent.document.links[i].onclick = fModalWinPreventClick;
  }

  g_iIELinkCounter++;
// Repeat above 2 steps for all frames in the window
  if ( p_winModalParent.frames.length == 0 ) { return; } 
  else {
    for ( var k=0; k<p_winModalParent.frames.length; k++ ){
      fDisableForms(p_winModalParent.frames[k], p_varIELinkForModalParent ,false);
    }
  }
}

function fEnableForms(p_winModalParent, p_varIELinkForModalParent, p_bParentCall)
{
  if (p_bParentCall) {
    g_iIELinkCounter = 0;
  }

// Enable each form element in the document
  for ( var i=0; i<p_winModalParent.document.forms.length; i++){
    for ( var j=0; j<p_winModalParent.document.forms[i].elements.length; j++){
      p_winModalParent.document.forms[i].elements[j].disabled = false;
    }
  }
  
// Reset on click event handler for all links in the document to their initial values
  for ( i=0; i< p_winModalParent.document.links.length; i++){
    p_winModalParent.document.links[i].onclick =  p_varIELinkForModalParent[g_iIELinkCounter][i];
  }

  g_iIELinkCounter++;
// Repeat above 2 steps for all frames in the window
  if ( p_winModalParent.frames.length == 0 ) { return; } 
  else {
    for ( var k=0; k<p_winModalParent.frames.length; k++ )    {
      fEnableForms(p_winModalParent.frames[k], p_varIELinkForModalParent, false);
    }
  }
}

