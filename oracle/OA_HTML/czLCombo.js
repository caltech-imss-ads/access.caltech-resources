/* $Header: czLCombo.js 115.25 2001/06/14 15:34:09 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function LogicComboBox ()
{
  this.parentConstructor = ComboBox;
  this.parentConstructor ();

  this.type = 'LogicComboBox';
  this.dropdownClass = 'LogicList';
  this.editCellClass = 'ComboEditCell';
}

function LogicComboBox_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function LogicComboBox_propertyChangeCallback (change)
{
  if (change.property == 'state') {
    if ((change.value == 'utrue') || (change.value == 'ltrue')) {
      var itm = this.editCell.getModelItem();
      if ( itm !=null ){
      	if ( itm.getState() != 'unknown') {
          //clear the state of the old item temporarily;
          itm.clearState();
      	}
      }
      this.editCell.setModelItem (change.source);
    } else {
      var itm = this.editCell.getModelItem ();
      if (itm != null) {
      	if (itm == change.source && itm != null) {
          this.editCell.setModelItem (null);
      	}
      }
    }
    this.dropdown.hide ();
    //remove if this is still registered with event mgr; 
    this.eventManager.removeListener ('docMouseUp', this);
    this.dropdown.opened = false;
    //notify listeners;
    this.notifyListeners ('selectionChangeCallback', change);
  }
  return true;
}

function LogicComboBox_addOptionItem (rtid, item, index, state, price, atp, visible)
{
  if (typeof (item) == 'string') {
    var str = item;
    item = new LogicItemModel ();
    item.setText (str);
  }
  if (rtid) item.setName (rtid);
  if (state) {
    item.setState (state);
    if ((state == 'utrue') || (state == 'ltrue'))
      this.selItemModel = item;
  }
  if (price != null) item.setPrice (price);
  if (atp != null) item.setATP (atp);
  item.setVisibility(visible);
  this.listModel.addItem (rtid, item, index);
  item.addListener ('propertyChangeCallback', this);
  return item;
}

function LogicComboBox_moreInitializeDropdown ()
{
  this.dropdown.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  
  this.dropdown.setLogicStateProps (this.bNoIcon, this.bLogCol, this.bLogBld);
  this.dropdown.setPriceVisible (this.priceVisible);
  this.dropdown.setATPVisible (this.atpVisible);
}

function LogicComboBox_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
  if (this.dropdown) {
    this.dropdown.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
}

function LogicComboBox_setPriceVisible (bVis)
{
  this.priceVisible = bVis;
  if (this.built) {
    this.editCell.setPriceVisible (bVis);
    this.dropdown.setPriceVisible (bVis);
  }
}

function LogicComboBox_setATPVisible (bVis)
{
  this.atpVisible = bVis;
  if (this.built) {
    this.dropdown.setATPVisible (bVis);
  }
}

function LogicComboBox_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.left + this.width + 8;
    var tpos = this.top + (this.height - 16)/2;
    uicon.setDimensions (lpos, tpos, 16, 16);
    uicon.setReference (this.reference);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = this.reference.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = this.reference.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicComboBox_setWindowName (wndName)
{
  this.windowName = wndName;
  if (this.dropdown) {
    this.dropdown.setWindowName (wndName);
  }
}

function LogicComboBox_setMaxColWidth (colName, colWd)
{
  this.listModel.setMaxColWidth (colName, colWd);
}

function LogicComboBox_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;

  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld
}


HTMLHelper.importPrototypes(LogicComboBox, ComboBox);

LogicComboBox.prototype.constructor  = LogicComboBox;
LogicComboBox.prototype.setImages = LogicComboBox_setImages;
LogicComboBox.prototype.addOptionItem = LogicComboBox_addOptionItem;
LogicComboBox.prototype.setPriceVisible = LogicComboBox_setPriceVisible;
LogicComboBox.prototype.setATPVisible = LogicComboBox_setATPVisible;
LogicComboBox.prototype.setUnSatisfied = LogicComboBox_setUnSatisfied;
LogicComboBox.prototype.setWindowName = LogicComboBox_setWindowName;
LogicComboBox.prototype.setMaxColWidth = LogicComboBox_setMaxColWidth;
LogicComboBox.prototype.hideFeature = LogicComboBox_hideFeature;
LogicComboBox.prototype.setLogicStateProps = LogicComboBox_setLogicStateProps;

// private methods
LogicComboBox.prototype.moreInitializeDropdown = LogicComboBox_moreInitializeDropdown;
LogicComboBox.prototype.propertyChangeCallback = LogicComboBox_propertyChangeCallback;



/**
* ComboEditCell - Edit cell class for the logic combo box.
*                 Will create a fixed width price column and a variable width
*                 description column (the desc text in it can be truncated).
*/

function ComboEditCell ()
{
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'ComboEditCell';
}

function ComboEditCell_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) { 
    var index = 0; 
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, this.height, this.width, 1, 2, 'top', 'left', true, true);
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, this.height, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function ComboEditCell_moreSetProperties (cell)
{
  if (cell.colName == 'price') {
    cell.noclip = true;
    cell.addListener ('captionChangeCallback', this);
  }
}

function ComboEditCell_captionChangeCallback (oldVal, cell)
{
  if (this.launched) { 			
    //resize the layer to the correct width and height
      var wd = (ns4)? cell.block.clip.width : cell.block.scrollWidth;
    if (cell.width < wd) {
      cell.setWidth (wd + 5);
      cell.setLeft (this.width - cell.width);
      this.layoutColumns ();
    }
  }
}

function ComboEditCell_layoutColumns() 
{
  var curCell = null;
  var lastVisCell = null;
  var springCell = null;
  var springColDesc = null;
  var colDesc = null;  
  var totWidth = this.width;
  var totWidthUpToSpringCell = 0;
  var cellsWidth = 0;
  var len = this.columnDescriptors.length;
  
  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors [i];
    curCell = this.cells[i];
    if (curCell.isVisible ()) {
      curCell.setLeft (cellsWidth);
      // currently support one spring column;
      if (colDesc.spring) {
        springCell = curCell;    
        totWidthUpToSpringCell = cellsWidth;
      }
      cellsWidth += curCell.getWidth ();
    }
  }
  if (springCell) {
    cellsWidth -= totWidth;
    springCell.setWidth(springCell.getWidth() - cellsWidth);
    cellsWidth = totWidthUpToSpringCell + springCell.getWidth ();
    for (var i=springCell.index + 1; i<len; i++) {
      curCell = this.cells [i];
      curCell.setLeft (cellsWidth);
      cellsWidth += curCell.getWidth ();
    }
  } 
}

function ComboEditCell_mouseoverCallback (e, eSrc)
{
  return (true);
}

function ComboEditCell_mouseoutCallback (e, eSrc)
{
  return (true);
}

function ComboEditCell_mouseupCallback (e, eSrc)
{
  return (true);
}

// construction
HTMLHelper.importPrototypes(ComboEditCell, LogicItem);

//public methods
ComboEditCell.prototype.moreSetProperties = ComboEditCell_moreSetProperties;

// overrides
ComboEditCell.prototype.getColumnDescriptors = ComboEditCell_getColumnDescriptors;
ComboEditCell.prototype.layoutColumns = ComboEditCell_layoutColumns;

//Event handlers
ComboEditCell.prototype.mouseupCallback = ComboEditCell_mouseupCallback;
ComboEditCell.prototype.mouseoverCallback = ComboEditCell_mouseoverCallback;
ComboEditCell.prototype.mouseoutCallback = ComboEditCell_mouseoutCallback;
ComboEditCell.prototype.captionChangeCallback = ComboEditCell_captionChangeCallback;




