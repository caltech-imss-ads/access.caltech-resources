<!--$Header: jtfihjsl.js 115.1 2000/08/22 14:01:31 pkm ship        $-->

var ckList = "";
function checkSelected(alertMsg)
{
  // First make sure at least one message was selected
  one_chkd = false;
  for(i=0; i < document.form.elements.length; i++)
  {
    if (document.form.elements[i].type == "checkbox")
    {
      if (document.form.elements[i].name.indexOf("select") != -1)
      {
        if (document.form.elements[i].checked)
        {
          one_chkd = true;
          ckList = ckList + document.form.elements[i].value + ".";
        }
      }
    }
  }
  if (!one_chkd)
  {
    alert(alertMsg);
    return false;
  }
  return true;
}
function doDelete(whichTable,nextPage,startIndex,endIndex,alertMsg)
{
  if ((startIndex-1) <= endIndex) startIndex = 1;
  if (checkSelected(alertMsg))
  {
    // Do delete to table
    document.location.href = 'jtfihadl.jsp?table=' + whichTable + '&nextPage=' + nextPage + '&startIndex=' + startIndex + '&ids=' + ckList;
  }
}
function editDescription(id)
{
  window.open("jtfihadc.jsp?id=" + id,"ldesc","width=575,height=275");
}
function showDetails(whichTable,id,nextPage,param)
{
  if (whichTable == "interaction")
    self.location.href = "jtfihdet.jsp?id=" + id + "&table=" + whichTable + "&nextPage=" + nextPage + "&" + param;
  else if (whichTable == "activity")
    self.location.href = "jtfihdet.jsp?id=" + id + "&table=" + whichTable + "&nextPage=" + nextPage + "&" + param;
  else if (whichTable == "media")
    self.location.href = "jtfihdet.jsp?id=" + id + "&table=" + whichTable + "&nextPage=" + nextPage + "&" + param;
}

