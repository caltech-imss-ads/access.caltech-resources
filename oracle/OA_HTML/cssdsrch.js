
// $Header: cssdsrch.js 115.2 2000/05/10 13:54:40 pkm ship      $
// this file is modified-
// 10-May-2000 kreardon removed redundant datepicker code
//

 var valuechanged = false;
 var ns4 = (document.layers)? true:false;
 var ie4 = (document.all)? true:false;




function passback(lovform,defno,category,type,severity,priority,frequency,language,tier,phase,status,created_date_from,created_date_to,updated_date_from,updated_date_to,prid,prdesc,prver,prever,cpid,cpdesc,cpver,subid,subdesc, plid,pldesc,plverid,plver,ecid1,ecid2,ecid3,ecdesc1,ecdesc2,ecdesc3,resid,resname,createid,createname,updateid,updatename,phaseid,phaseowner,fixbyver,fixedinver,caller) {
  document.forms[lovform].elements["defectNumber"].value = defno;
  document.forms[lovform].elements["Category"].value = category;
  document.forms[lovform].elements["Type"].value = type;
  document.forms[lovform].elements["Severity"].value = severity;
  document.forms[lovform].elements["Priority"].value = priority;
  document.forms[lovform].elements["Frequency"].value = frequency;
  document.forms[lovform].elements["Language"].value = language;
  document.forms[lovform].elements["Tier"].value = tier;
  document.forms[lovform].elements["Phase"].value = phase;
  document.forms[lovform].elements["Status"].value = status;
  document.forms[lovform].elements["createdDateFrom"].value = created_date_from;
  document.forms[lovform].elements["createdDateTo"].value = created_date_to;
  document.forms[lovform].elements["updatedDateFrom"].value = updated_date_from;
  document.forms[lovform].elements["updatedDateTo"].value = updated_date_to;
  document.forms[lovform].elements["productID"].value = prid;
  document.forms[lovform].elements["productDesc"].value = prdesc;
  document.forms[lovform].elements["pVer"].value = prver;
  document.forms[lovform].elements["pEver"].value = prever;
  document.forms[lovform].elements["compID"].value = cpid;
  document.forms[lovform].elements["compDesc"].value = cpdesc;
  document.forms[lovform].elements["compVer"].value = cpver;
  document.forms[lovform].elements["subcompID"].value = subid;
  document.forms[lovform].elements["subcompDesc"].value = subdesc;
  document.forms[lovform].elements["platformID"].value = plid;
  document.forms[lovform].elements["platformDesc"].value = pldesc;
  document.forms[lovform].elements["platformVerID"].value = plverid; 
  document.forms[lovform].elements["platformVer"].value = plver;
  document.forms[lovform].elements["ecID1"].value = ecid1;
  document.forms[lovform].elements["ecID2"].value = ecid2
  document.forms[lovform].elements["ecID3"].value = ecid3;
  document.forms[lovform].elements["ecDesc1"].value = ecdesc1;
  document.forms[lovform].elements["ecDesc2"].value = ecdesc2;
  document.forms[lovform].elements["ecDesc3"].value = ecdesc3;
  document.forms[lovform].elements["ResourceId"].value = resid;
  document.forms[lovform].elements["ResourceName"].value = resname;
  document.forms[lovform].elements["CreatedId"].value = createid;
  document.forms[lovform].elements["CreatedName"].value = createname;
  document.forms[lovform].elements["UpdatedId"].value = updateid;
  document.forms[lovform].elements["UpdatedName"].value = updatename;
  document.forms[lovform].elements["phaseOwnerId"].value = phaseid;
  document.forms[lovform].elements["phaseOwner"].value = phaseowner; 
  document.forms[lovform].elements["FixbyVer"].value = fixbyver;
  document.forms[lovform].elements["FixedinVer"].value = fixedinver;
  document.forms[lovform].action=caller;
  document.forms[lovform].submit();
}


function my(lovform,prid,prdesc,prver,prever,resid,resname,l_res_id,l_res_name,caller) {
  document.forms[lovform].elements["productID"].value = prid;
  document.forms[lovform].elements["productDesc"].value = prdesc;
  document.forms[lovform].elements["pVer"].value = prver;
  document.forms[lovform].elements["product_earliest_ver"].value = prever;
  document.forms[lovform].elements["ResourceId"].value = resid;
  document.forms[lovform].elements["ResourceName"].value = resname;
  document.forms[lovform].action=caller;
  document.forms[lovform].submit();
}




//****************************************************************************
// Select a product 
//****************************************************************************
function getPrLOV() {
	document.forms["def_search"].elements["fromLOV"].value = "product";
	document.forms["def_search"].action = "cssprlov.jsp";
        document.forms["def_search"].submit();
}

//****************************************************************************
// Select a product version
//****************************************************************************
function getPvLOV() {
  document.forms["def_search"].elements["fromLOV"].value = "productVer";
  document.forms["def_search"].action = "csspvlov.jsp";
  document.forms["def_search"].submit();
}                     

function getPcLOV(from,to){
  document.forms["def_search"].elements["fromLOV"].value = "resid";
  document.forms["def_search"].action = csspclov.jsp;
  document.forms["def_search"].submit();
}

function getbackupLOV(from){
 document.forms["def_search"].elements["fromLOV"].value = "resname";
 document.forms["def_search"].action = "csspclov.jsp";
 document.forms["def_search"].submit();
}


function getLOV(from,to){
  document.forms["def_search"].elements["fromLOV"].value = from;
  document.forms["def_search"].action = to;
  document.forms["def_search"].submit();
}

function checkPID() {

  var str = document.forms["def_search"].productID.value;
  if (str==null || str=="" || str=="null") {
    alert ("Product not selected");
    document.forms["def_search"].elements["product"].focus();
    document.forms["def_search"].elements["product"].select();
    return false;
   }
 else { return true; }
} 



//*****************************************************************************
//check mandatory fileds. 
//*****************************************************************************
function isEmpty(name)  {
 if (name =="type" || name=="status" || name =="severity"){
  var pos = document.forms["def_search"].elements[name].selectedIndex;
  var str = document.forms["def_search"].elements[name].options[pos].value;
  }
 else  { 
 var str = document.forms["def_search"].elements[name].value; 
 }
 if (str == null || str =="") {
   alert("Please fill out "+"'"+ name+"'"+" field!"); 
   return true
  }
 return false 
}


   



function checkform(){
 if (!isEmpty("summary")) {
   if (!isEmpty("product")) {
     if (!isEmpty("prod_ver")) {
       if (!isEmpty("earliest_ver")) {
         if (!isEmpty("status")) {
           if (!isEmpty("type")) {
             if (!isEmpty("severity")) {
               document.forms["def_search"].submit();
               return true; 
             }
           }
         }
       }
     }
   }
 }
}
//*****************************************************************************
// Since nescape browser will lose the hidden field value, the these two function 
// used the restore the value
//*****************************************************************************
function restoreHiddenValues(){
	var srcForm;
	var tarForm; 
	if(ns4){
		var hiddenDiv = document.HIDDENFIELDS;
		var srcForm = hiddenDiv.document.forms["create_opp_h"];
		var tarForm = document.forms["create_opp"];
   	}
  	if(ie4){
        	var hiddenDiv = document.all["HIDDENFIELDS"];
        	var srcForm = hiddenDiv.document.forms["create_opp_h"];
		var tarForm = document.forms["create_opp"];
   	}	  
	for(var i=0; i<srcForm.elements.length;i++){
		tarForm.elements[srcForm.elements[i].name+"_m"].value= srcForm.elements[i].value;
        }

}

function changeHiddenValue(frm,elem,value){
	if(ns4){
		var hiddenDiv = document.HIDDENFIELDS;
		hiddenDiv.document.forms[frm].elements[elem].value = value;	
   	}
   	if(ie4){
		document.all["HIDDENFIELDS"].document.forms[frm].elements[elem].value = value;
   	}
}


//******************************************************************************************
// Check if the input data is number
//******************************************************************************************
function isNumber(obj, invNum){
	var myval = obj.value;
	var numStr = "012345556789.,";
	var charStr;
	var counter = 0;
	var i =0;
	var error = invNum;
	
	for(i; i<myval.length; i++){
  		charStr = myval.substring(i, i+1);
  		if(numStr.indexOf(charStr) !=-1)
   	  		counter ++;
 		}
	if(counter !=myval.length){
  		alert(error);
  		obj.value="";
  		obj.focus();
   	}
} 



function remove_commas(mynum)
{    
 var temp= mynum;
 var newnum,idx;
 while ((idx=temp.indexOf(",")) != -1)
 {
   newnum=temp.substring(0,idx);
   newnum+=temp.substring(idx+1,temp.length);
   temp=newnum;
 }
 return temp;
}   


 
//***********************************************************************************************
// Used to track which field's value is changed on the screen
//***********************************************************************************************
function markRow(frm, elm, hiddenfrm, hiddenelm){
	valuechanged = true;
	document.forms[frm].elements[elm].value="1";
	var str = "1";
	changeHiddenValue(hiddenfrm, hiddenelm, str); 

}



//***********************************************************************************************
// check if want to save data when user is leaving the page 
//***********************************************************************************************
/*function checkUnsavedData(UnsavedErr){
	if(valuechanged){
		var UnsaveMessage = UnsavedErr;
		if(confirm(UnsaveMessage)){
			valuechanged = false;
			document.forms["create_opp"].submit();
		}
	return false;
   	}
}
*/





