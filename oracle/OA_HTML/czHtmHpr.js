/* $Header: czHtmHpr.js 115.4 2001/06/14 15:34:03 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |       xxxx-xx-99  CK Jeyaprakash  modified createLayer.                   |
 |                                                                           |
 +===========================================================================*/

function _importPrototypes()
{
  if(arguments[0]) {
    for(var i=1; i<arguments.length;i++) {
      if(arguments[i])
        if(arguments[i].prototype) {
          for(var prop in arguments[i].prototype)
            arguments[0].prototype[prop] = arguments[i].prototype[prop];
        } else
          alert ("Import Prototypes: INVALID_CLASS");
    }
  }
}

/*
cross browser functions to manipulate HTML elements
*/

function HTMLHelper()
{
  return HTMLHelper;
}

function _setHTMLObjDimensions(htmlObj,lt,tp,wd,ht,clipRgn)
{
  if(lt!=null) htmlObj.left = lt;
  if(tp!=null) htmlObj.top = tp;
  if(wd!=null) htmlObj.width = wd;
  if(ht!=null) htmlObj.height = ht;
  
  if(clipRgn != 'undefined' && clipRgn != null) {
    if (ns4) {	 
      var clipv = clipRgn.split("rect(")[1].split(")")[0].split("px");
      htmlObj.clip.top = clipv[0];
      htmlObj.clip.right = clipv[1];
      htmlObj.clip.bottom = clipv[2];
      htmlObj.clip.left = clipv[3];
    } else if (ie4) {
      var clipv = clipRgn.split("rect(")[1].split(")")[0].split("px");
      htmlObj.clip = "rect("+clipv[0]+"px "+clipv[1]+"px "+clipv[2]+"px "+clipv[3]+"px)"; 
    }
  }
}

function _setHTMLBGColor(htmlObj,color)
{
  if(ns4) {
    if(!color || color=='transparent')
      htmlObj.bgColor = null;
    else
      htmlObj.bgColor = color;
  } else if(ie4) {
    if(!color)
      htmlObj.backgroundColor ='transparent';
    else
      htmlObj.backgroundColor = color;
  }
}

function _setHTMLVisibility(htmlObj,bShow)
{
  if(bShow)  
    var show = "inherit";
  else 
    var show = (ns4)? "hide" : "hidden";
  
  if(htmlObj) 
    htmlObj.visibility = show;
}

function _setHTMLFGColor (htmlObj, color)
{
  htmlObj.color = color;
}

function _writeToLayer(lyr, strContent)
{
  if(lyr) {	 
    if (ns4) {
      lyr.document.open ();
      lyr.document.write(strContent);
      lyr.document.close();
    } else if(ie4) {
      lyr.innerHTML = "";
      lyr.innerHTML = strContent;
    }
  }
}

//creating new layer
function _createLayer(id, parLayer,frm,content, styleClass)
{
  var lyr=null;
  var parDoc = null;
  
  if(frm == null) 
    frm = self;
  
  if(ns4) {
    if(parLayer)
      parDoc = parLayer.document;
    else 
      parDoc = frm.document;
    
    if(parLayer)
      lyr = parDoc.layers[id] = new Layer(100,parLayer);
    else 
      lyr = parDoc.layers[id] = new Layer(100,frm);
    
    if(content) {
      lyr.document.open();
      lyr.document.write(content);
      lyr.document.close();
    }
  } else if(ie4) {
    if(parLayer) 
      parDoc = parLayer;
    else 
      parDoc = frm.document.body;
    
    var txtCont = '<DIV ID='+id;
    if(styleClass)
      txtCont +=' CLASS ='+styleClass;
    txtCont +=' style="position:absolute;';
    txtCont += '"> ';
    if(content)
      txtCont += content;
    txtCont += '</DIV>';
    
    parDoc.insertAdjacentHTML('BeforeEnd',txtCont)
      lyr = parDoc.all[id];
  }
  return lyr;
}

function _destroyLayer(lyr)
{
  if(ns4) {
    //lyr.visibility = 'hide';
    //delete lyr;
    delete document.layers['base1'];
    //lyr = null;
  } else if(ie4) {
    lyr.style.visibility = 'hidden';
    lyr.innerHTML = "";
    lyr.outerHTML = "";
  }

}

//Class methods
HTMLHelper.setDimensions 	= _setHTMLObjDimensions;
HTMLHelper.setBackgroundColor 	= _setHTMLBGColor;
HTMLHelper.setForegroundColor 	= _setHTMLFGColor;
HTMLHelper.setVisible		= _setHTMLVisibility;
HTMLHelper.createLayer		= _createLayer;
HTMLHelper.destroyLayer		= _destroyLayer;
HTMLHelper.writeToLayer		= _writeToLayer;
HTMLHelper.importPrototypes	= _importPrototypes;

