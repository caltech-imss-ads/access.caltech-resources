  <!-- $Header: asfPhnSumLib.js 115.1 2000/08/30 22:12:31 pkm ship     $ -->
  var tdcData = "";
  var tdcHiddenText = "hidden";
  function tdcChangeHiddenText(newValue) {
    if(newValue == "text") {
      tdcHiddenText = "text";
    } else {
      tdcHiddenText = "hidden";
    }
  }
  function tdcGetValue(inFormNm, inFieldNm, inType) {
    var fieldValue = "";
    if (document.forms[inFormNm].elements[inFieldNm]) {
      var elementType = document.forms[inFormNm].elements[inFieldNm].type;
      if(elementType == "select-one") {
        var fieldIndex = document.forms[inFormNm].elements[inFieldNm].selectedIndex;
        fieldValue = document.forms[inFormNm].elements[inFieldNm].options[fieldIndex].value;
      } else if(elementType == "checkbox") {
        var fieldChecked = document.forms[inFormNm].elements[inFieldNm].checked;
        if (fieldChecked == true) {
          fieldValue = document.forms[inFormNm].elements[inFieldNm].value;
        } else {
          fieldValue = "";
        }
      } else {
        fieldValue = document.forms[inFormNm].elements[inFieldNm].value;
      }
    }
    return fieldValue;
  }
  function tdcParseNext(inDelim) {
    var delimLoc = tdcData.indexOf(inDelim);
    var parsedVal = "";
    if (delimLoc != -1) {
      parsedVal = tdcData.substring(0,delimLoc);
      tdcData = tdcData.substring(delimLoc+1);
    }
    return parsedVal;
  }
  function tdcParseDelim() {
    var tdcDelim = tdcData.substring(0,1);
    tdcData = tdcData.substring(1);
    return tdcDelim;
  }
  function tdcAppendData(inValue) {
    tdcData = tdcData + inValue + tdcDelim;
  }
  function tdcBeginData(delim,rows,inPopupSec,inNamesArray,inTypesArray) {
    tdcData = delim
    tdcDelim = delim;
    tdcAppendData(rows);
    tdcAppendData(inNamesArray.length);
    tdcAppendData(inPopupSec);
    for(var i=0; i<inNamesArray.length; i++)
      tdcAppendData(inNamesArray[i]);
    for(var i=0; i<inTypesArray.length; i++)
      tdcAppendData(inTypesArray[i]);
  }
  function tdcSaveValues(inFormName, inDisplayName, inSecName, inObjName, inPopupSec,
        inNamesArray, inTypesArray, inDelim, inRecType, inDisplayValue) {
  //Function puts current data back into parent
  //alert("in saveValues "+inSecName+inObjName);
  //prior to exit for later update or redisplay of
  //window for same context.
  //inOrigFlag should be true or false whether orig is saved
    var numRows = document.forms[0].elements[inPopupSec+".OBJ.$ROWS"].value;
    tdcBeginData(inDelim,numRows,inPopupSec,inNamesArray,inTypesArray);
    for (var i=0; i<numRows; i++) {
      for (var j=0; j<inNamesArray.length; j++) {
          tdcAppendData(tdcGetValue(0,inPopupSec+".ROW"+i+"."+inNamesArray[j]));
      }
    }
    var fullDisplayName = inSecName+inObjName+inDisplayName;
    var asfActionSection = inSecName;
    if (inObjName== ".OBJ.") {
	 asfActionSection = "PAGE";
    }
    opener.document.forms[inFormName].elements[fullDisplayName].value = inDisplayValue;
    opener.document.forms[inFormName].elements[fullDisplayName+"All"].value = tdcData;
    opener.document.forms[inFormName].elements[fullDisplayName+"Flag"].value = "true";
  //alert("before set  "+asfActionSection+inObjName+"asfAction");
    opener.document.forms[inFormName].elements[asfActionSection+inObjName+"asfAction"].value = inRecType;
  //alert("after set  "+asfActionSection+inObjName+"asfAction");
  }
  function tdcUpdateButton(inFormName, inDisplayName, inSecName, inObjName, inPopupSec,
        inNamesArray, inTypesArray, inDelim, inRecType, inDisplayValue) {
    tdcSaveValues(inFormName, inDisplayName, inSecName, inObjName, inPopupSec,
      inNamesArray, inTypesArray, inDelim, inRecType, inDisplayValue);
    self.close();
    return false;
  }
  function tdcWriteHidden(inRow, inFieldNm, inValue, inPopupSec) {
    if((inFieldNm!="isEmpty")||(inValue!=""))
      popupWindow.document.writeln('<input type="'+tdcHiddenText+'" '
          + 'name="'+inPopupSec+'.ROW'+inRow+'.'+inFieldNm+'" '
          + 'value="'+ inValue + '" >');
  }
  function tdcCallPopup(inFormName, inSecName,
      inPageId, inJSPName, inPopupName, inDisplayName, inObjName, inRecType) {
    var fullDisplayName = inSecName+inObjName+inDisplayName;
    var fullHiddenName = fullDisplayName+"All";
    var fullFromLOV = fullDisplayName+"Flag";
    var fieldValue = "";
    var fromLOV = document.forms[inFormName].elements[fullFromLOV].value;
    popupWindow = window.open("",
      inPopupName,"width=640,height=360,resizable,scrollbars");
    popupWindow.opener = self;
    popupWindow.document.open();
    popupWindow.document.writeln("<html>");
    popupWindow.document.writeln("<head>");
    popupWindow.document.writeln("</head>");
    popupWindow.document.writeln("<body>");
    popupWindow.document.writeln('<form name="popupInit" action="'+inJSPName+'" method="post">');
    if(tdcHiddenText == "text") {
      popupWindow.document.writeln('<input type="submit" name="continuebutton" value="continue" >');
    }
    popupWindow.document.writeln(inPageId);
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.DISPLAY_NAME" value="'+inDisplayName+'">');
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.OBJ_NAME" value="'+inObjName+'">');
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.SEC_NAME" value="'+inSecName+'">');
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.REC_TYPE" value="'+inRecType+'">');
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.FORM_NAME" value="'+inFormName+'">');
    popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.DISPLAY_VALUE" value="'+document.forms[inFormName].elements[fullDisplayName].value+'">');
    if (fromLOV == "true") {
      popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.LOV.FROMLOV" value="true">');
      tdcData = document.forms[inFormName].elements[fullHiddenName].value;
      inDelim = tdcParseDelim();
      //popupWindow.document.writeln('<input type="'+tdcHiddenText+'" name="PAGE.OBJ.TDC_DELIM" value="\\'+inDelim+'">');
      var numRows = tdcParseNext(inDelim);
      var numCols = tdcParseNext(inDelim);
      var popupSec = tdcParseNext(inDelim);
      var namesArray = new Array(numCols);
      for (var i=0; i<numCols; i++)
        namesArray[i] = tdcParseNext(inDelim);
      var typesArray = new Array(numCols);
      for (var i=0; i<numCols; i++)
        typesArray[i] = tdcParseNext(inDelim);
      for (var i=0; i<numRows; i++) {
        for (var j=0; j<numCols; j++) {
          fieldValue = tdcParseNext(inDelim);
          //empty strings not written if checkbox(10) or pseudocheckbox(19)
          if((fieldValue!="")|| !((typesArray[j]==10)||(typesArray[j]==19)))
            tdcWriteHidden(i,namesArray[j], fieldValue, popupSec);
        }
      }
    } else {
    }
    popupWindow.document.writeln("</form>");
    popupWindow.document.writeln("</body>");
    popupWindow.document.writeln("</html>");
    if(tdcHiddenText == "hidden") {
      popupWindow.document.forms[0].submit();
    }
    popupWindow.document.close();
  }












