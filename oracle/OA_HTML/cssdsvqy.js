//  $Header: cssdsvqy.js 115.9 2000/05/25 12:06:22 pkm ship  $ 

function getLOV(to){
  document.forms["saved_query"].action = to;
  document.forms["saved_query"].submit();
}


function passBackToMainFromCdlov(index, column_value, to) {

  var column_name = "colDis"+index;

  document.forms["cd_lov"].elements[column_name].value = column_value;
  document.forms["cd_lov"].action = to;
  document.forms["cd_lov"].submit();
} 

function passBackToMainFromOblov(index, column_value, to) {

  var column_name = "ordCol"+index;

  document.forms["ob_lov"].elements[column_name].value = column_value;
  document.forms["ob_lov"].action = to;
  document.forms["ob_lov"].submit();
}

function passBackToMainFromWclov(index, column_value, to) {

  var column_name = "wheCol"+index;

  document.forms["wc_lov"].elements[column_name].value = column_value;
  document.forms["wc_lov"].action = to;
  document.forms["wc_lov"].submit();

}

function passBackToMain(from, to) {
  history.go(-1);
}


function save_query(to, viewName) {

  if (checkView("Please enter the View Name") == false) {
     return false;
  } else if (checkEmptyValue("Column title in the Columns Displayed session cannot be null", "Value in the Where Column cannot be null") == false) {
     return false;
  } else if (checkDuplication("Columns chosen in the Columns Displayed session cannot be duplicated", "Columns chosen in the Order By session cannot be duplicated", "Columns chosen in the Where Clause session cannot be duplicated") == false) {
     return false;
  } else if (checkValidValue("The column value is invalid in the Columns Display session", "The column value is invalid in the Order By session", "The column value is invalid in the Where Clause session") == false) {
     return false; 
  } else {
     document.forms["saved_query"].elements["from_page"].value = "FROM_UPDATE";
     document.forms["saved_query"].action = to;
     document.forms["saved_query"].submit();
  }


}

function jump_to_preview(to) {

  document.forms["saved_query"].action = to;
  document.forms["saved_query"].submit();

}


function passQueryInfo(profileId, queryId, queryDesc, fromPage) {

  document.forms["profile_page"].elements["profile_id"].value = profileId;
  document.forms["profile_page"].elements["query_id"].value = queryId;
  document.forms["profile_page"].elements["from_page"].value = fromPage;
  document.forms["profile_page"].elements["view_name"].value = queryDesc;
  document.forms["profile_page"].submit();
}

// bunch of client side validation

function checkView(msg) {

  if (document.forms["saved_query"].elements["view_name"].value == "") {
     alert(msg);
     return false;
  }
  return true;
}


function checkEmptyValue(colMsg, wheMsg) {

   var colDis = document.forms["saved_query"].elements["colDis0"].value;
   var colTit = document.forms["saved_query"].elements["colTit0"].value;
   var endValue = document.forms["saved_query"].elements["from_which_cd_lov_row"].value;

   colDis = trimSpace(colDis);
   if ((colDis != null) && (colDis != "")) {
	 if ((colTit == null) || (colTit == "")) {
         alert(colMsg);
	    return false;
	 }
   }

   for (i=1; i<=parseInt(endValue); i++) {
       if (i == parseInt(endValue)) {
          colRem = "";
       } else {
          colRem = document.forms["saved_query"].elements["colRem" + i.toString(10)].value;
       }
       colDis = document.forms["saved_query"].elements["colDis" + i.toString(10)].value;
       colTit = document.forms["saved_query"].elements["colTit" + i.toString(10)].value;

       if ((colRem == null) || (colRem == "")) {
          colDis = trimSpace(colDis);
          if ((colDis != null) && (colDis != "")) {
             if ((colTit == null) || (colTit == "")) {
                alert(colMsg);
                return false;
             }
          }
       }
   }


   endValue = document.forms["saved_query"].elements["from_which_wc_lov_row"].value;
  
   for (i=0; i<parseInt(endValue); i++) {
       if (i == parseInt(endValue)-1) {
          wheRem = "";
       } else {            
	     wheRem = document.forms["saved_query"].elements["wheRem" + i.toString(10)].value;
	  }

	  wheCol = document.forms["saved_query"].elements["wheCol" + i.toString(10)].value;
	  wheVal = document.forms["saved_query"].elements["wheVal" + i.toString(10)].value;

       if ((wheRem == null) || (wheRem == "")) {
          wheCol = trimSpace(wheCol);
          if ((wheCol != null) && (wheCol != "")) {
             if ((wheVal == null) || (wheVal == "")) {
                alert(wheMsg);
			 return false;
             }
          }
       }

   }

   return true;
}

function checkDuplication(colDisMsg, ordByMsg, wheColMsg) {


   // for columns display region
   var endValue = document.forms["saved_query"].elements["from_which_cd_lov_row"].value;

   for (i=0; i<endValue; i++) {
       if (i==0) {
          firstColRem = "";
       } else {
          firstColRem = document.forms["saved_query"].elements["colRem" + i].value;
       }
       if ((firstColRem == null) || (firstColRem == "")) {
          for (j=i+1; j<=endValue; j++) {
              if (j == endValue) {
                 secondColRem = "";
              } else {
                 secondColRem = document.forms["saved_query"].elements["colRem" + j].value;
              }
              if ((secondColRem == null) || (secondColRem == "")) {
                 firstColDis = document.forms["saved_query"].elements["colDis" + i].value;
                 secondColDis = document.forms["saved_query"].elements["colDis" + j].value;
                 if (firstColDis == secondColDis) {
                    alert(colDisMsg);
                    return false;
                 }
              }
          }
          

       }
   }

   // for order by region
   endValue = document.forms["saved_query"].elements["from_which_ob_lov_row"].value;

   for (i=0; i<endValue; i++) {
       firstOrdRem = document.forms["saved_query"].elements["ordRem" + i].value;
       if ((firstOrdRem == null) || (firstOrdRem == "")) {
          for (j=i+1; j<=endValue; j++) {
              if (j==endValue) {
                 secondOrdRem = "";
              } else {
                 secondOrdRem = document.forms["saved_query"].elements["ordRem" + j].value;
              }
              if ((secondOrdRem == null) || (secondOrdRem == "")) {
                 firstOrdCol = document.forms["saved_query"].elements["ordCol" + i].value;
                 secondOrdCol = document.forms["saved_query"].elements["ordCol" + j].value;
                 if (firstOrdCol == secondOrdCol) {
                    alert(ordByMsg);
                    return false;
                 } 
              }
          }
       } 
   }

   // for where clause region
   endValue = document.forms["saved_query"].elements["from_which_wc_lov_row"].value;
   for (i=0; i<endValue; i++) {
       firstWheRem = document.forms["saved_query"].elements["wheRem" + i].value;
       if ((firstWheRem == null) || (firstWheRem == "")) {
          for (j=i+1; j<=endValue; j++) {
              if (j==endValue) {
                 secondWheRem = "";
              } else {
                 secondWheRem = document.forms["saved_query"].elements["wheRem" + j].value;
              }
              if ((secondWheRem == null) || (secondWheRem == "")) {
                 firstWheCol = document.forms["saved_query"].elements["wheCol" + i].value;
                 secondWheCol = document.forms["saved_query"].elements["wheCol" + j].value;
                 if (firstWheCol == secondWheCol) {
                    alert(wheColMsg);
                    return false;
                 }
              }
          }
       }
   }

   return true;

}

function checkValidValue(colMsg, ordMsg, wheMsg) {

   cdLOVLength = document.forms["saved_query"].elements["cdLOVLength"].value;
   cdLastRow = document.forms["saved_query"].elements["from_which_cd_lov_row"].value;
   colDis = document.forms["saved_query"].elements["colDis" + cdLastRow].value;
   colDis = trimSpace(colDis);
   if ((colDis == null) || (colDis == "")) {
   } else {
      for (i=0; i<cdLOVLength; i++) {
          currentVal = document.forms["saved_query"].elements["cdLOV" + i].value;
          if (colDis == currentVal) {
             break;
          }
          if (i == (cdLOVLength-1)) {
             alert(colMsg);
             return false;
          }
      }
   }

   obLOVLength = document.forms["saved_query"].elements["obLOVLength"].value;
   obLastRow = document.forms["saved_query"].elements["from_which_ob_lov_row"].value;
   ordCol = document.forms["saved_query"].elements["ordCol" + obLastRow].value;

   ordCol = trimSpace(ordCol);
   if ((ordCol == null) || (ordCol == "")) {
   } else {
      for (i=0; i<obLOVLength; i++) {
          currentVal = document.forms["saved_query"].elements["obLOV" + i].value;
          if (ordCol == currentVal) {
             break;
          }
          if (i == (obLOVLength-1)) {
             alert(ordMsg);
             return false;
          }
      }
   }


/*   
   wcLOVLength = document.forms["saved_query"].elements["cdLOVLength"].value;
   wcLastRow = document.forms["saved_query"].elements["from_which_wc_lov_row"].value;
   wheCol = document.forms["saved_query"].elements["wheCol" + wcLastRow].value;

   wheCol = trimSpace(wheCol);
   if ((wheCol == null) || (wheCol == "")) {
   } else {
      for (i=0; i<wcLOVLength; i++) {
          currentVal = document.forms["saved_query"].elements["cdLOV" + i].value;
          if (wheCol == currentVal) {
             break;
          }
          if (i == (wcLOVLength-1)) {
             alert(wheMsg);
             return false;
          }
      }
   }

*/
   return true;

}

function trimSpace(inVar) {
   for (k=0; k<inVar.length; k++) {
       if (inVar.charAt(k) != " ") {
          return inVar;
       }
   }
   return "";
}


function restoreValue(to) {

  queryId = document.forms["saved_query"].elements["query_id"].value;

  if (queryId == "") {

     document.forms["saved_query"].elements["from_page"].value = "FROM_ADD_VIEW";
  } else {

     document.forms["saved_query"].elements["from_page"].value = "FROM_SAVED_QUERY";
  }

  document.forms["saved_query"].action = to;
  document.forms["saved_query"].submit();

}

function checkColNOrdBy(i) {

  colDis = document.forms["saved_query"].elements["colDis" + i].value; 
  colRem = document.forms["saved_query"].elements["colRem" + i].checked; 
  
  // the checkbox has just been checked
  if (colRem == true) {
     nOrderBy = document.forms["saved_query"].elements["from_which_cd_lov_row"].value;
	for (k=0; k<nOrderBy; k++) {
         currentVal = document.forms["saved_query"].elements["ordCol" + k].value;
         if (currentVal == colDis) {
            document.forms["saved_query"].elements["ordRem"+k].checked = true;
	    }
	}
  }

}

function clearFields(reg, m) {
  if (reg == 'colDis') {
     document.forms["saved_query"].elements["colDis"+m].value = "";
     document.forms["saved_query"].elements["colTit"+m].value = "";
  } else if (reg == 'ordCol') {
     document.forms["saved_query"].elements["ordCol"+m].value = "";
  } else if (reg == 'wheCol') {
     document.forms["saved_query"].elements["wheCol"+m].value = "";
     document.forms["saved_query"].elements["wheCon"+m].value = "=";
     document.forms["saved_query"].elements["wheVal"+m].value = "";
  }

}


