<!-- hide the script's contents from feeble browsers
<!-- $Header: hrlcmnuw.js 115.0 2000/01/20 12:49:26 pkm ship     $ -->
<!-- ==========================================================================
<!-- Change History
<!-- Version  Date         Author      Bug #    Remarks
<!-- -------  -----------  ----------- -------- -------------------------------
<!-- 110.0    16-Jun-1999  akuan                Initial Creation.
<!-- 110.12   02-Jul-1999  akuan                Modify hardcode alert to message
<!--                                            for "Location code required" 
<!-- 110.13   02-Jul-1999  akuan                Set confirm message to Main Menu
<!-- 110.14   26-Aug-1999  svittal              Commented out target=_top to 
<!--                                            fix 934577
<!-- 110.20   27-Oct-1999  svittal              Enhancemnt to address various
<!--                                            combinations of WorkFlow attibs.
<!--                                            LOC_CODE_REQD = No is one of the
<!--                                            major issue addressed. Location
<!--                                            Matrix document is the refrence.

//var p_next_ok = "Y";
var p_form_changed = "N";
// Buttons
//
// The following function is invoked from the header frame when the user 
// clicks on the 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.display_page.p_result_code.value="MAIN_MENU";
  if (confirm(msg))
  {
    document.display_page.target= '_top';
    document.display_page.submit();
  }
}

// The following function is invoked from the container_bottom frame when 
// the user clicks on the 'Reset Page' button.
function reset_button_onClick () { 
  top.clear_container_bottom_frame();
  document.display_page.p_form_changed.value ="Y";
  document.display_page.reset();
}

// Invoke by the cancel button
// Cancel the transaction by submitting the form
// with the p_form_changed value switch to Y
function cancel_button_onClick (msg)
{
   document.display_page.p_result_code.value = 'CANCEL';
// document.display_page.target = '_top';    /* commented to fix 934577 */
   if (document.display_page.p_next_ok.value =="Y")
   {
      document.display_page.p_form_changed.value ="Y";
   }
   if (confirm(msg))
   {
      top.clear_container_bottom_frame();
      document.display_page.submit();
   }
}

// The following function is invoked from the container_bottom frame when 
// the user clicks on the 'Back' button.
function back_button_onClick (msg,newtarget) {
//  document.display_page.p_next_ok.value ="Y";
  if (document.display_page.p_next_ok.value =="Y")
  {
      document.display_page.p_form_changed.value ="Y";
  }
  if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.display_page.p_result_code.value = 'PREVIOUS';
        document.display_page.target = newtarget;
        document.display_page.submit();
  } 
}

// The following function is invoked from the container_bottom frame when 
// the user clicks on the 'Next' button.
function next_button_onClick () {
//  j_new_loc_code=document.display_page.P_LOC_CODE.value;
  if (document.display_page.p_select_list.value =="T")
  {
//    j_select_loc_code = document.display_page.P_LOC_CODE.options[document.display_page.P_LOC_CODE.selectedIndex].text;
      j_new_loc_code = document.display_page.P_LOC_CODE.options[document.display_page.P_LOC_CODE.selectedIndex].value;
      P_LOC_ID = document.display_page.P_LOC_CODE.options[document.display_page.P_LOC_CODE.selectedIndex].value;
  }
  else
  {
    j_new_loc_code=document.display_page.P_LOC_CODE.value;
  }
  j_hr_default=document.display_page.p_hr_use_defaults.value;
  if (j_hr_default =="N")
  {
    if (is_blank(j_new_loc_code) && (document.display_page.p_loc_code_reqd.value =="Y")) 
    {
     alert(message_array[8]);
     document.display_page.p_next_ok.value ="N";
     document.display_page.p_form_changed.value ="N";
     document.display_page.p_redraw.value ="N";
    }
    else 
    {
     document.display_page.p_next_ok.value ="Y";
     document.display_page.p_form_changed.value ="Y";
    }
  }
  else 
  {
    if (document.display_page.p_select_list.value =="F")
    {
      if (is_blank(j_new_loc_code) && (document.display_page.p_loc_code_reqd.value =="Y")) 
      {
        alert(message_array[8]);
        document.display_page.p_next_ok.value ="N";
        document.display_page.p_form_changed.value ="N";
        document.display_page.p_redraw.value ="N";
      }
      else 
      {
        document.display_page.p_next_ok.value ="Y";
        document.display_page.p_form_changed.value ="Y";
      }
    }
    else 
    {
     document.display_page.p_next_ok.value ="Y";
     document.display_page.p_form_changed.value ="Y";
    }
  }
  if (document.display_page.p_next_ok.value=="Y")
  {
      document.display_page.p_form_changed.value ="Y";

  }
  if (document.display_page.p_next_ok.value=="Y" ||
      document.display_page.p_next_ok.value=="") {
      top.clear_container_bottom_frame();
      document.display_page.p_redraw.value ="N";
      document.display_page.p_result_code.value = 'NEXT';
      document.display_page.target = 'container_middle';
      document.display_page.submit();
  } 
}

//Set form has been changed
function set_form_changed() {
  document.display_page.p_form_changed.value="Y";
}

// Redraw the form when user selects a location from LOV.
function redraw_location_page() {
    document.display_page.p_redraw.value ="Y";
    document.display_page.submit();
//  document.redraw_location_form.submit();
}

// Redraw the form when user selects a location from LOV.
function redraw_form() {
    document.redraw_new_location.submit();
}

function setChanged(j_town_or_city) {
    document.redraw_page5.p_town_or_city.value = j_town_or_city;
    document.redraw_page5.submit();
}

// Redraw the form when user selects a different country from the select list.
function new_country() {
  document.contents_form.p_country_code.value =
  document.display_page.P_COUNTRY_CODE.options[document.display_page.P_COUNTRY_CODE.selectedIndex].value;
  document.contents_form.submit();
}

<!-- ----------------------------------------------------------------------
<!-- ------------------------ LOV -----------------------------------------
<!-- ----------------------------------------------------------------------
<!-- The following is modified ICX LOV function so that we can use it w/ layers
<!-- depending on browser version.
<!-- Modified ICX LOV code
function LOV
  ( c_attribute_app_id
  , c_attribute_code
  , c_region_app_id
  , c_region_code
  , c_form_name
  , c_frame_name
  , c_where_clause
  ,c_js_where_clause
  ) {

 lov_win = window.open("icx_util.LOV?c_attribute_app_id=" +
     c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +
     "&c_region_app_id=" + c_region_app_id + "&c_region_code=" +
     c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +
     c_frame_name + "&c_where_clause=" + c_where_clause +
     "&c_js_where_clause=" + c_js_where_clause,"LOV",
     "resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");
    lov_win.opener = self;
    lov_win.focus()
}


// Redraw the form when user selects a context switch from the select list.
//function ddf_list() {
//    alert('welcome 1');
//j_ddf_context = document.display_page.p_attribute_category.options[document.display_page.p_attribute_category.selectedIndex].value;
//    alert('welcome 2');
//    document.display_page.p_redraw.value ="Y";
//    document.display_page.p_attribute_category.value = j_ddf_context;
//    document.display_page.submit();
//}

// redraw page when user selects a different context 
function set_redraw_flag () {
j_ddf_context = document.display_page.P_ASS_ATTRIBUTE_CATEGORY.options[document.display_page.P_ASS_ATTRIBUTE_CATEGORY.selectedIndex].value;
  document.display_page.p_redraw.value ="Y";
  document.display_page.P_ASS_ATTRIBUTE_CATEGORY.value = j_ddf_context;
  document.display_page.submit();
}

// Redraw the form when user selects a different location from the select list.
function location_list() {
j_loc_id = document.display_page.P_LOC_CODE.options[document.display_page.P_LOC_CODE.selectedIndex].value;
//    alert(j_loc_id);
    top.clear_container_bottom_frame();
    document.display_page.p_redraw.value ="Y";
    document.display_page.p_form_changed.value="Y";
    document.display_page.P_LOC_ID.value = j_loc_id;
    document.display_page.submit();
}

function location_code(msg) {
    location_new_code = document.display_page.P_LOC_CODE.value;
    if (location_new_code != location_old_code)
    {
        document.display_page.p_form_changed.value="Y";
        document.display_page.target= '_top';
        document.display_page.submit();
    }
    else
    {
    }
}
// Alert user if they select the same location code.
function new_location(j_id_new) {
  j_id_old=document.display_page.p_old_loc_id.value;
  top.clear_container_bottom_frame();
  document.display_page.p_redraw.value ="Y";
  document.display_page.p_next_ok.value ="Y";
  document.display_page.P_LOC_ID.value = j_id_new;
  document.display_page.p_form_changed.value="Y";
  document.display_page.target = 'container_middle';
  document.display_page.submit();
}

// This function checks whether there is a future
// assignment record or a hiring date in reference
// to the effective date that the user has inputed.
function chk_effective_date(error_code) {
  if (error_code == 'terminated') {
    // The date you have entered is invalid
    // because the person has been terminated.
    alert(message_array[7]);
    document.hoursForm.p_result_code.value = 'CANCEL';
    document.hoursForm.target= '_top';
    document.hoursForm.submit();
  }
  if (error_code == 'hire_date') {
    // The date you have chosen is before the hire date,
    // please choose a later date.
    alert(message_array[5]);
    document.hoursForm.p_result_code.value = 'CANCEL';
    document.hoursForm.target= '_top';
    document.hoursForm.submit();
    var error_flag = 1;
   }

   if (error_code == 'future_rec') {
    // A future change exists for this assignment,
    // please choose a later date.
    alert(message_array[3]);
    document.hoursForm.p_result_code.value = 'CANCEL';
    document.hoursForm.target= '_top';
    document.hoursForm.submit();
    var error_flag = 1;
   }

   //if (error_code == 'correction') {
   // You are entering data in correction mode.
   // if (confirm(message_array[6])) {
   //        void(0);
   //}
   //else {
   //      document.hoursForm.p_result_code.value = 'CANCEL';
   //     document.hoursForm.target= '_top';
   //         document.hoursForm.submit();
   //  }
   //}
}

