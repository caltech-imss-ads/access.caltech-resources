//-- $Header: hrprsalw.js 115.5 2001/09/29 04:24:58 pkm ship   $ -->
//=============================================================================
//-- Change History
//-- Version  Date         Author      Bug #    Remarks
//-- -------  -----------  ----------- -------- ----------------------------
//-- 110.0    06/13/99     VKOTHARI              Created  for pay rate change
//                                               module of HR Manager Employee
//                                               events. This file contains 
//                                               javascript routines to calcu
//                                               late salary and validate data.
//-- 110.1    06/16/99     VKOTHARI              Added check_required_fields
//                                               function, modified validate
//                                               data to check for mandatory
//                                               flex fields
//   110.2    06/16/99     VKOTHARI              Added functions for main menu
//                                               Added functions for back 
//                                               button. Removed all reference
//                                               to exit form. Removed all 
//                                               reference to exit form
//   110.3   06/17/99     VKOTHARI               Changed calc_pay_change 
//                                               function to check for blank 
//                                               and invalid fields
//                                               Changed reset_fields to clear
//                                               total line in the single mode
//  110.4   06/19/99     VKOTHARI                Displaying message_array[13]
//                                               if change exceeds max limit.
//                                               passing correct parameters to
//                                               calculate_fields procedure if
//                                               percent or total field is 
//                                               changed
//                                               clearing comparatio and 
//                                               quartile in reset_fields 
//				                 function
//110.5    06/20/99    VKOTHARI                  Removed grade parameters from
//                                               all the functions. Accepting 
//                                               these as a dummy parameter 
//                                               form hidden fields
//                                               Added code to check warning/
//                                               error if new pay rate exceeds
//                                               grade range or max limit
//110.6    06/21/99    VKOTHARI                  Change validate_data FN. to 
//                                              check for grade range only 
//                                              if it is not null.
//                                              Checking length of comments 
//                                              field , error if it is more 
//                                              than 2000 chars.
//                                              Changed next_button and main
//                                              menu function to set target
//                                              to _top. 
//110.7    06/22/99   VKOTHARI  914318          Changed reset_button function 
//                                              to reset change flag.
//                                              Corrected formatting for annual
//                                              equivalent in function
//                                               calc_pay_rate
//                                             Added call to refresh lower 
//                                             frame when user clicks cancel,
//                                             back or next button

//110.8  06/30/99    VKOTHARI                  Added p_target parameter to 
//                                             back_button_on_click function
//                                             Removed hardcoded target _top
//                                             to use p_target
//110.9  06/30/99    VKOTHARI                  Changed function cancel_button
//                                             on_click and main_menu_onClick 
//                                             not to check for whether any
//                                             changes were made before 
//                                             displaying confirm alert. 
//
//110.10 07/01/99   VKOTHARI                  Changed back_button_on_cancel
//                                            to display confirm alert all the 
//                                            time
//110.11 07/14/99   VKOTHARI  922263          Changed function calculate 
//                                            quartile to display null instead
//                                            of 5              
//110.12 07/15/99   VKOTHARI                  created function trunc to 
//                                            truncate comparatio to 2 decimal
//                                            places
//110.13 07/30/99   VKOTHARI  918824          Created function check_zero to raise
//                                            warning if user enteres 0 
//110.14 08/23/99   VKOTHARI                  changed function multiple_mode 
//                                            to correct javascript error on IE
//110.15 08/24/99   VKOTHARI 973379           changed target to 
//                                            container_middle for 
//                                            cancel_button_on_click proc 

//110.16 09/13/99   mvempaty 932697	      Added code that will check the 
//                                            value of
//                                            a hidden field p_processstate,
//                                            and if true
//                                            doesn't alert the user for null 
//                                            required fields.
//110.17 10/14/99   mvempaty		      Added ViewInEuro function
//110.18 10/21/99   vkothari                  Added code for component flex
//                                            mandatory validation and function
//                                            validate_reason_code
//110.19 10/25/99   vkothari                  changed function validate_all
//                                            amount to add current salary 
//110.20 10/25/99   vkothari 1019398          changed function 
//                                            calculate_comparatio to multiply
//                                            it by 100
//110.21 10/26/99   vkothari 1050220          1)changed calculate fields so 
//                                            that total salary fields are not 
//                                            populated here
//                                            2)Populating total fields in 
//                                            validate_all_amounts in 
//                                            components mode
//
//110.22 11/02/99   mvempaty 1055605          Modified View_In_Euro fn to alert
//					      user if no conversion rate is there
//110.23 11/09/99   mvempaty 1062953          Modified View_In_Euro to check
//					      if the EuroWindow is already open 
//
//110.24 11/11/99   mvempaty 1072355	      Changed code to recalculate values//					      when Euro button is re-pressed.
//110.25 12/17/99   svittal  1073684	      Added p_comment parameter so that
//                                            the focus goes to the p_comment   //                                            when user tries to edit p_object.
//110.26 09/26/00   lma      1395647          Fixed in validate_data. Should
//                                            continue checking error in case
//                                            of warnings.
//110.27 01/03/01   lma      1414461          Added check_numeric_component_flex
//                                           
//110.28 03/08/01   rbanda   1673211          Fixed format function
//110.29 03/09/01   vkothari 1670349          Replaced alert with confirm
//                                            in validate data in case of 
//                                            grade range and maz limit 
//                                            warning
//110.30 09/27/01   fychu    1949831          1)In calc_pay_change(), replaced
//(115.5)                                     the call to format() for 
//                                            p_change_value with roundOff().
//                                            2)Added a new roundOff function.
//                                            3)In validate_all_amounts(),
//                                            replaced call to format() with
//                                            roundOff() for p_total_amount and
//                                            p_total_annual.
//                                            4)In calculate_fields(), replaced
//                                            call to format() with roundOff()
//                                            for p_annual_salary,
//                                            p_total_amount, and p_total_annual
//                                            fields.
//
//-----------------------------------------------------------------------------

var EuroWindow;

// function to reset form
function reset_button()
{
  // reset the exit flag so that user does not get warning
  document.salary_form.p_form_changed.value="N"
  document.salary_form.reset();
  document.salary_selection_form.reset();
}

// function to set form in redisplay mode
function set_cflex_change ()
{

  document.salary_form.p_action_value.value = "CFLEXCHANGE";
  document.salary_form.submit();
}

function validate_reason_code ()
{
  var ln_no_of_rows =   document.salary_form.p_no_of_components.value ;


  if (ln_no_of_rows > 0 )
  {
     for ( var i = 0 ; i < ln_no_of_rows ; i++ ) 
     {
        var list1 = document.salary_form.P_REASON_CODE[i];
        var current_value = list1.options[list1.selectedIndex].value ; 
        for ( j=i+1 ;  j <= ln_no_of_rows ; j++ )
        {
           var list = document.salary_form.P_REASON_CODE[j];
           var existing_value = list.options[list.selectedIndex].value ; 
       
           if ( current_value == existing_value)
           {
             return false ; 
           }
        }
     }
  }
  return true ; 
}

// function to set form in redisplay mode
function set_redisplay_flag ()
{
  document.salary_form.p_action_value.value = "FLEXCHANGE";
  document.salary_form.submit();
}

// function to change mode to single mode

function singlemode()
{
  set_exit_form_changed();
  document.dummy_selection_form.p_selection_mode.value = "S" ; 
  document.dummy_selection_form.submit() ; 
}


// function to display mode to multiple

function multiplemode()
{
  set_exit_form_changed();
  document.salary_form.p_form_changed.value="Y" ; 
  document.dummy_selection_form.p_selection_mode.value = "M" ;
  document.dummy_selection_form.submit() ; 
}


function trunc ( num, decimalplaces ) 
{
  var str ; 
  var decimalpoint ; 
  var index ; 
  str = num.toString() ; 
  // get the NLS Numeric character
  decimal_point = document.dummy_parameter_form.p_decimal.value ; 
  index = str.indexOf(decimal_point ) ; 
  index = index + decimalplaces ; 

  return ( str.substring(0, index+1 )) ; 
  
}

// function to check for number

function isNumber(num ) 
{
  var decimal = false ;
  var str ; 
  var decimal_point ; 


  str = num.toString() ; 
  // get the NLS Numeric character
  decimal_point = document.dummy_parameter_form.p_decimal.value ; 

  for ( var i=0; i<str.length ; i++ )
  {
    var onechar = str.charAt(i)
    if ( i==0 && onechar =="-") 
    {
      continue ; 
    }
    if ( onechar == decimal_point  && !decimal)
    {
      decimal=true ; 
      continue ;
    }
    if ( onechar <"0" || onechar >"9" ) 
    {
      return false ; 
    }
   
  }
  return true ; 
}

//function to check for the mandatory components flex 
function check_component_flex ( p_no_of_rows )
{
  var fieldcheck = true ; 
  var fieldsNeeded = "" ;

    for(var fieldNum=0; fieldNum < pr_crequired_fields.length; fieldNum++) 
    {

       var e = document.salary_form.elements[pr_crequired_fields[fieldNum]]; 
       if (p_no_of_rows == 0 )
       { 
         if (is_blank(e.value))
           fieldsNeeded +="    "+ pr_crequired_prompts[fieldNum] + "    \n";
       }
       else 
       {
             
         var j = fieldNum ; 

         for ( var i=0 ; i < e.length; i++ )
         {
            if (is_blank(e[i].value) )
            {
              if (j == fieldNum )
              {
                fieldsNeeded +=  pr_crequired_prompts[fieldNum]+ "    \n";
                j = -1 ; 
              }
            }
         }
       }
     }
     return fieldsNeeded ; 
}
//function to check for the numeric components flex
function check_numeric_component_flex ( p_no_of_rows )
{
  var fieldcheck = true ;
  var fieldsNeeded = "" ;

    for(var fieldNum=0; fieldNum < pr_cnumeric_fields.length; fieldNum++)
    {

       var e = document.salary_form.elements[pr_cnumeric_fields[fieldNum]];
       if (p_no_of_rows == 0 )
       {
         if (isNumber(e.value)==false)
           fieldsNeeded +="    "+ pr_cnumeric_prompts[fieldNum] + "    \n";
       }
       else
       {
         
         var j = fieldNum ;

         for ( var i=0 ; i < e.length; i++ )
         {
            if (isNumber(e[i].value)==false )
            {
              if (j == fieldNum )
              {
                fieldsNeeded +=  pr_cnumeric_prompts[fieldNum]+ "    \n";
                j = -1 ;
              }
            }
         }
       }
     }
     return fieldsNeeded ;
}

//function to check for required fields 

function check_required_fields(input, requiredFields, fieldPrompts) {
  var fieldCheck   = true;
  var fieldsNeeded = "";

  for(var fieldNum=0; fieldNum < requiredFields.length; fieldNum++) 
  {

     var e = input.elements[requiredFields[fieldNum]]; 
     if (is_blank(e.value))
        fieldsNeeded += fieldPrompts[fieldNum] + "    \n";
   }

   return fieldsNeeded ;

}


//function to check for numeric fields

function check_numeric_fields(input, numericFields, fieldPrompts) {
  var fieldCheck   = true;
  var fieldsNeeded = "";

  for(var fieldNum=0; fieldNum < numericFields.length; fieldNum++)
  {

     var e = input.elements[numericFields[fieldNum]];
     if (isNumber(e.value)==false )
        fieldsNeeded += fieldPrompts[fieldNum] + "    \n";
   }

   return fieldsNeeded ;

}


// function to check for positive integer

function is_pos_integer ( val ) 
{
  var str = val.toString() ; 
  for ( var i=0 ; i<str.length ; i++ ) 
  {
    var onechar = str.charAt(i);
    if ( onechar <"0" || onechar>"9" )
    {
      return false ; 
    }
  }
  return true; 
}

// function to validate salary data 
// this function is invoked before submitting the form



function validate_data (p_max_limit )
{
   var error_occured ="N";
   var error_msg ="";
   var ln_no_of_rows ;
   var empty_fields ="";
   var invalid_numeric_fields ="";
   var invalid_fields ="";
   var lb_reason_code = true ; 
   var mesg ; 
   var ln_ranking ; 
   var ln_total_amount ; 
   var lv_comment ;
   var ln_grade_min ; 
   var ln_grade_max 

  // if single mode , then validate that amount is entered

   if (document.salary_form.p_selection_mode.value=='S')
   {

     if ( is_blank(document.salary_form.p_change_value.value))
     {
	if (document.salary_form.p_processstate.value == 'FALSE')
	{
          empty_fields = "\n"+ message_array[23];
          error_occured="Y" ; 
	}
     }
   }
   else 
   {
      // check if all the reason codes are unique
      if ( validate_reason_code() == false ) 
        lb_reason_code = false ; 

      ln_no_of_rows =   document.salary_form.p_no_of_components.value ;
      if ( validate_all_amounts(ln_no_of_rows )==false )
      {
         error_occured="Y" ; 
      }
      var l_empty_cflex = check_component_flex (ln_no_of_rows );
      var l_invalid_cnflex = check_numeric_component_flex( ln_no_of_rows );
   }  

   if (error_occured== "Y")
   {
     empty_fields = "\n    " +message_array[23];
     empty_fields = empty_fields + "\n    " +message_array[24];
     empty_fields = empty_fields + "\n    " +message_array[25];
   }


   var l_empty_flex = check_required_fields (
                      document.salary_form, 
                      pr_required_fields ,
                      pr_required_prompts ) ;
   var l_invalid_nflex = check_numeric_fields (
                      document.salary_form,
                      pr_numeric_fields ,
                      pr_numeric_prompts ) ;

    if ( l_empty_flex ) 
      empty_fields +="\n    "+  l_empty_flex ; 

    if ( l_empty_cflex ) 
      empty_fields +="\n    "+  l_empty_cflex ; 

    if ( l_invalid_nflex )
      invalid_numeric_fields +="\n    "+  l_invalid_nflex ;

    if ( l_invalid_cnflex )
      invalid_numeric_fields +="\n    "+  l_invalid_cnflex ;



   // validate ranking 

  ln_ranking = document.salary_form.p_ranking.value;

  if ( is_blank(ln_ranking)== false)
  {  

     if ( is_pos_integer(ln_ranking)== false  )
     {
        invalid_fields = "\n    "+message_array[26]+": "+message_array[5];
        error_occured="Y" ;
     }
  }

  // validate that comment is not more than 2000 characters
  lv_comment = document.salary_form.p_comment.value ; 
  
  if ( lv_comment.length > 2000 )
  {
   if (invalid_fields )
     invalid_fields += "\n    "+ message_array[27]+": "+message_array[21] ; 
   else 
     invalid_fields = "\n    " +message_array[27]+": "+message_array[21] ; 
  }
  if (!empty_fields && !invalid_fields && lb_reason_code == true && !invalid_numeric_fields)
  {

    //check for max limit 
    if ( parseFloat(document.salary_form.p_total_percent.value)>p_max_limit)
    {
       if (document.dummy_parameter_form.p_percent_warn_or_error.value ==
         "ERROR")
       {
         alert ( message_array[13]);
         return false ;
       }
       else 
       {
         if ( confirm(message_array[13])== false)
           return false ;
       }
     }

     // now check for grade range 
     ln_total_annual = parseFloat(document.salary_form.p_total_annual.value); 
     ln_grade_min =  document.dummy_parameter_form.p_grade_min.value ; 
     ln_grade_max = document.dummy_parameter_form.p_grade_max.value ; 

     if ( !is_blank(ln_grade_min ))
     {
       if ( ln_total_annual < ln_grade_min ) 
       {
         if (document.dummy_parameter_form.p_range_warn_or_error.value =="ERROR")
      
         { alert ( message_array[20]);
           return false ;
         }
         else 
         {
           if (confirm ( message_array[20])==false)
             return false ; 
         }
       }
     }
     if ( !is_blank(ln_grade_max ))
     {
       if ( ln_total_annual > ln_grade_max ) 
       {
         if (document.dummy_parameter_form.p_range_warn_or_error.value =="ERROR")
         {
           alert ( message_array[20]);
           return false ;
         }
      
         else 
         {
           if (confirm ( message_array[20])==false)
             return false ;
         }
       }
     }
     return true ;
  }
  else 
  {

     mesg = message_array[1]+"\n";
     mesg = mesg+message_array[2]+"\n";

     if ( lb_reason_code == false ) 
     mesg = mesg+"\n"+message_array[29]+"\n\n";
     if ( empty_fields)
       mesg+= message_array[3]+empty_fields+"\n\n" ;
     if (invalid_fields )
       mesg+= message_array[4]+invalid_fields+"\n" ;
     if (invalid_numeric_fields )
       mesg+= message_array[31]+invalid_numeric_fields+"\n" ;
     if ( mesg)
       alert(mesg);
     return false ;
   }
}


// function to disable fields 
// called from total fields and comparatio and quartile
function disable_field (p_object, p_comment ) 
{ 

  if ((document.salary_form.p_selection_mode.value == "S")&&
      ((p_object.name=="p_total_amount")||
       (p_object.name=="p_total_percent")||
       (p_object.name=="p_total_annual")))
  {
    return ; 
  }
  else 
  {
    p_comment.focus();
  }
}

// this function formats a number to passed precision

function formatOld(number, precision) 
{
  var decimal_point ; 

  decimal_point = document.dummy_parameter_form.p_decimal.value ; 

  if (precision==0) 
  {
    var str=Math.floor(number)
    return str;
  }
  else 
  {
    var str=""+Math.round(eval(number) * Math.pow(10,precision))
    while (str.length<= precision) 
    {
      str="0"+str
    }
    var decpt = str.length - precision
    return str.substring(0,decpt)+decimal_point+str.substr(decpt,str.length);
  }
}

// function formats the input number with the input decimal precision
// Example:
// Number   Precision ReturnValue
// 0.2345   3         0.234
// 0.2      3         0.200
// 0.0001   3         0.0001
// 1.0000   3         1.000
// 0.000015 3         0.0001

function format (number, precision)
{
    number = ""+number;

    // if precision is 0 just return the number
    if(precision != 0)
   {
      var decimalIndex = number.indexOf ('.');
      var left = "";
      var right = "";
      // check if the number has a decimal index
      if(decimalIndex != -1)
      {
         left = number.substring (0, decimalIndex);
         right = number.substring (decimalIndex+1);
      }
      else
      { 
         left = number;
      }
      
      // here we need to truncate the decimal portion
      if(right.length >= precision)
      {
         var tmp = right.substring (0, precision);

         // this if checks for numbers like 1.000001 with a precision 3.
         // if the number is like 1.2345, we return a value of 1.234
         if (parseInt (tmp, 10) != 0)
            right = tmp;
         else
         {
            // here we handle numbers like 1.000032 with a precision 3
            var i = 0;
            var tmp1 = "";
            while (i < right.length)
            {
               tmp1 = tmp1 + right.charAt(i);
               if (right.charAt(i) != 0)
               {
                  break;
               }
               i++;
            }
            if (i == right.length)
               right = tmp;
            else
               right = tmp1;
         }
      }
      // we need to pad the decimal portion with trailing Zeroes
      else
      {
         while (right.length < precision)
         {
            right = right + '0';   
         }
      }
      number = left + '.' + right;
   }
   
   return number;
}

//----------------------------------------------------------------------------
//Function roundOff: Round off long floating point numbers
// Steps: 1) Multiply the original number by 10 to the power of x (where x
//           is the decimal points or precision.
//        2) Apply Math.round() to the result.
//        3) Divide the result by 10 to the power of x (where x  is the 
//           decimal points or precision. 
//  
//----------------------------------------------------------------------------
function roundOff(value, precision)
{
     value = "" + value //convert value to string
     precision = parseInt(precision);

     var number = "" + Math.round(value * Math.pow(10, precision)) /
                         Math.pow(10, precision);

     //Need to pad trailing zeroes up to the specified decimal digits
    if(precision == 0)
      {
       return number;
       }
    else
       {
         var decimalIndex = number.indexOf ('.');
         var left = "";
         var right = "";
         // check if the number has a decimal index
         if(decimalIndex != -1)
           {
            left = number.substring (0, decimalIndex);
            right = number.substring (decimalIndex+1);
           }
         else
           {
            left = number;
            }
         
         //here we need to pad trailing zero if the decimal portion is
         //less than precision.
         if(right.length < precision)
           {
            while (right.length < precision)
                 {
                    right = right + '0';
                 }
             }

          number = left + '.' + right;

          return number;
       }
}

// function to go back to previous page
// Invoke by the back button
// Submits the form with p_result_code set to PREVIOUS
// Transitions to the previous activity
function back_button_onClick (msg, p_target) {
//  if (document.salary_form.p_form_changed.value=="Y") {

     if (confirm(msg)) {
         top.clear_container_bottom_frame(); 
        document.salary_form.p_result_code.value = 'PREVIOUS';
                document.salary_form.target= p_target;
        document.salary_form.submit();
     }
/*
  } else {
        top.clear_container_bottom_frame(); 
        document.salary_form.p_result_code.value = 'PREVIOUS';
                document.salary_form.target= "_top";
        document.salary_form.submit();
 }
*/
}


// this function is invoked when user clicks next button
// it validates and then submits form 

function next_button_onClick()
{
  var ln_max_limit ;

  ln_max_limit = parseFloat(document.dummy_parameter_form.p_max_limit.value);

  
  if ( validate_data(ln_max_limit)==true)
  { 


    top.clear_container_bottom_frame(); 
    document.salary_form.p_result_code.value = 'NEXT' ; 
    
    document.salary_form.p_action_value.value = 'SUBMIT' ;
    document.salary_form.submit(); 
  }
}

// function to delete a row 

function delete_row( p_row_no)
{

  if ( document.salary_form.p_no_of_components.value == 0 )
  {
    // message that row can not be deleted
    alert ( message_array[19]);
    return ; 
  }

  if ( confirm(message_array[16]) == false  )
    return ; 
  
  // give a confirmation message 
  document.salary_form.p_action_value.value = "DELETE" ; 
  document.salary_form.p_delete_row_no.value = p_row_no ; 
  document.salary_form.submit(); 
}

//function to add a row 

function add_component(){
  var  l_no_of_rows ;
  var  error_msg ="";
  document.salary_form.p_action_value.value = "ADD" ; 

  l_no_of_rows = document.salary_form.p_no_of_components.value;

  document.salary_form.p_selection_mode.value="M" ; 
  document.salary_form.submit(); 
}     

// function to reset salary change fields to NULL

function reset_fields ( 
  p_row_no, 
  p_no_of_rows ) 
{

   if ( p_no_of_rows == 0 ) 
   {
     document.salary_form.p_change_value.value="" ;
     document.salary_form.p_change_percent.value="" ; 
     document.salary_form.p_annual_salary.value = ""; 
   }
   else 
   {
     document.salary_form.p_change_value[p_row_no-1].value="" ;
     document.salary_form.p_change_percent[p_row_no-1].value="" ;   
     document.salary_form.p_annual_salary[p_row_no-1].value = "";

   }

  
   if (document.salary_form.p_selection_mode.value =="S")
   {

    document.salary_form.p_total_amount.value= "";
    document.salary_form.p_total_percent.value= "";
    document.salary_form.p_total_annual.value= "";
   }

   // clean out comparatio and quartile 
   document.salary_form.p_comparatio.value = "";
   document.salary_form.p_quartile.value = "";
   return ; 
   
}

// calculate comparatio
function calculate_comparatio()
{
  var comparatio ;
  var ln_grade_midpoint ; 
  var l_new_pay_rate  ;
  ln_grade_midpoint =  document.dummy_parameter_form.p_grade_mid.value ; 

  if (is_blank(ln_grade_midpoint))
    return ; 
  ln_grade_midpoint = parseFloat(ln_grade_midpoint);

  l_new_pay_rate = parseFloat(document.salary_form.p_total_amount.value);

  l_new_pay_rate = l_new_pay_rate * 
            parseFloat(document.salary_form.p_pay_annualization_factor.value);

 comparatio = (l_new_pay_rate/ln_grade_midpoint)*100 ;
 comparatio = trunc ( comparatio, 2 ) ; 
// document.salary_form.p_comparatio.value = l_new_pay_rate/ln_grade_midpoint; 
 document.salary_form.p_comparatio.value = comparatio ; 
 
}

// calculate quartile
function calculate_quartile ()
  

{
  var ln_grade_min ;
  var ln_grade_max ;
  var ln_grade_mid ;

  var l_new_pay_rate ; 
  var grade_dif ; 
 
  ln_grade_min = document.dummy_parameter_form.p_grade_min.value ;
  ln_grade_max = document.dummy_parameter_form.p_grade_max.value ; 
  ln_grade_mid =  document.dummy_parameter_form.p_grade_mid.value ; 

  if ( is_blank(ln_grade_min )||
       is_blank(ln_grade_max)||
       is_blank(ln_grade_mid))
  {
    return ; 
  }

  ln_grade_min = parseFloat(ln_grade_min ) ; 
  ln_grade_max = parseFloat(ln_grade_max ) ; 
  ln_grade_mid = parseFloat(ln_grade_mid ) ; 

  grade_dif = ln_grade_max - ln_grade_min ; 

  l_new_pay_rate = parseFloat(document.salary_form.p_total_amount.value);
  
  l_new_pay_rate = l_new_pay_rate *
             parseFloat(document.salary_form.p_pay_annualization_factor.value);

  if ( l_new_pay_rate < (ln_grade_min + grade_dif/4 ) )
  {
     document.salary_form.p_quartile.value = 1 ; 
  }
  else  if (l_new_pay_rate < (ln_grade_min + grade_dif/2 ) )
  {
     document.salary_form.p_quartile.value = 2 ; 
  }
  else if (l_new_pay_rate < (ln_grade_min + grade_dif * 3/4 ))
  {
    document.salary_form.p_quartile.value = 3 ; 
  }
  else if (l_new_pay_rate < ln_grade_max )
  {
    document.salary_form.p_quartile.value = 4 ; 
  }
  else 
  {
    document.salary_form.p_quartile.value = "" ; 
  }
}

//----------------------------------------------------------------------------
// function to validate all the amounts 
// returns true if no error is encountered 
// returns false in case of error
//----------------------------------------------------------------------------
function validate_all_amounts
  (p_no_of_rows )
{
  var l_total_amount ; 
  var l_total_percent ;
  var l_total_annual ; 
  var l_precision ; 

  var l_amount ; 
  var error = "N" ;
  var l_current_salary ; 

  l_precision = parseInt(document.salary_form.p_decimal_precision.value)
  l_current_salary = parseFloat ( document.salary_form.p_current_salary.value);
  l_total_amount = 0 ; 

  if ( p_no_of_rows == 0 ) 
  {
    l_total_amount = (document.salary_form.p_change_value.value);
    

    if ( is_blank(l_total_amount ))
    {
      return false ; 
    }
    else 
      l_total_amount = parseFloat(l_total_amount ) ; 
  }  
  else 
  {
    for ( var i=0; i<=p_no_of_rows ; i++ )
    {
      l_amount =document.salary_form.p_change_value[i].value ;

      if (is_blank(l_amount))
      {
        error = "Y" ; 
        continue ; 
      }
      else 
      {
        l_amount = parseFloat(l_amount ) ; 
        l_total_amount = l_total_amount + l_amount ; 
      }
    }
  }

  //Bug 1949831 Fix:
 // Call roundOff() instead of format() for p_total_amount.
  document.salary_form.p_total_amount.value = roundOff( (l_current_salary+l_total_amount), l_precision) ;  
  //document.salary_form.p_total_amount.value = format( (l_current_salary+l_total_amount), l_precision) ;  

  l_total_percent =
     (parseFloat(l_total_amount )*100)/
                   parseFloat(document.salary_form.p_current_salary.value) ;
  l_total_annual = parseFloat(document.salary_form.p_total_amount.value *
         document.salary_form.p_pay_annualization_factor.value) ; 
 
            
  document.salary_form.p_total_percent.value = 
       format(l_total_percent,3) ;

 //Bug 1949831 Fix:
 // Call roundOff() instead of format() for p_total_annual.
  document.salary_form.p_total_annual.value = roundOff(l_total_annual,
                                                        l_precision) ;    
  /* document.salary_form.p_total_annual.value = format(l_total_annual,
                                                        l_precision) ;    
  */

  if ( error == "Y" ) 
  {
    return false ;
  }
  else 
  {
    return true ; 
  }
}


//--------------------------------------------------------------------
// function to calculate fields 
// This function calls roundOff() to round the number value up to the
// decimals specified.
//--------------------------------------------------------------------

function calculate_fields ( 
  p_amount , 
  p_row_no , 
  p_current_salary,
  p_no_of_rows)

{
   var l_percent_change ; 
   var l_annual_amount ; 

   var l_total_amount ; 
   var l_total_percent ; 
   var l_total_annual ; 
   var l_precision ; 

   l_precision = 
    parseInt (document.salary_form.p_decimal_precision.value ) ; 

   l_percent_change = p_amount/(p_current_salary/100) ; 
   l_percent_change = format ( l_percent_change , 3 ) ; 

   l_annual_amount = p_amount *
                     document.salary_form.p_pay_annualization_factor.value ;

   if ( p_no_of_rows == 0 ) 
   {

     document.salary_form.p_change_percent.value=l_percent_change ; 
    
     //Bug 1949831 Fix:
    // Call roundOff() instead of format() for p_annual_salary.
     document.salary_form.p_annual_salary.value= roundOff(
                                             l_annual_amount,l_precision); 
   /*
     document.salary_form.p_annual_salary.value= format(
                                             l_annual_amount,l_precision); 
   */
     document.salary_form.p_change_value.value=p_amount ; 
     l_total_amount = p_amount ; 
     l_total_percent = document.salary_form.p_change_percent.value ; 
     l_total_annual = document.salary_form.p_annual_salary.value ; 
     l_total_amount = parseFloat(p_current_salary)+parseFloat(l_total_amount) ;


     l_total_annual = parseFloat(p_current_salary* 
                      document.salary_form.p_pay_annualization_factor.value)+
                      parseFloat(l_total_annual) ;

     //Bug 1949831 Fix:
    // Call roundOff() instead of format() for p_total_amount.
     document.salary_form.p_total_amount.value = 
       roundOff(l_total_amount, l_precision) ; 
   /*
     document.salary_form.p_total_amount.value = 
       format(l_total_amount, l_precision) ; 
   */

     document.salary_form.p_total_percent.value = 
       format(l_total_percent,3) ;

    //Bug 1949831 Fix:
    // Call roundOff() instead of format() for p_total_annual.
     document.salary_form.p_total_annual.value = roundOff(l_total_annual,
                                                        l_precision) ;
   /*
     document.salary_form.p_total_annual.value = format(l_total_annual,
                                                        l_precision) ;
   */

   }
   else 
   {

      //populate the percentage and annual fields for this row
      document.salary_form.p_change_percent[p_row_no-1].value=
        l_percent_change ;   

     //Bug 1949831 Fix:
    // Call roundOff() instead of format() for p_annual_salary.
     document.salary_form.p_annual_salary[p_row_no-1].value = 
       roundOff( l_annual_amount, l_precision ) ; 
   /*
     document.salary_form.p_annual_salary[p_row_no-1].value = 
       format( l_annual_amount, l_precision ) ; 
   */

     document.salary_form.p_change_value[p_row_no-1].value=p_amount ; 

     //  now validate and calculate total amount 
     validate_all_amounts(p_no_of_rows) ; 

/*
     l_total_percent =
     (parseFloat(document.salary_form.p_total_amount.value )*100)/
                   parseFloat(document.salary_form.p_current_salary.value) ;


     l_total_amount = parseFloat(document.salary_form.p_total_amount.value )+
                      parseFloat(p_current_salary);
*/


     l_total_annual = parseFloat(l_total_amount *
         document.salary_form.p_pay_annualization_factor.value) ;            
     
     // now display new pay rate 
/*
     document.salary_form.p_total_amount.value = 
       format(l_total_amount, l_precision) ; 


     document.salary_form.p_total_percent.value = 
       format(l_total_percent,3) ;
     document.salary_form.p_total_annual.value = format(l_total_annual,
                                                        l_precision) ;
*/
  }

  calculate_quartile() ; 
  calculate_comparatio(); 
  return true ; 
}

//--------------------------------------------------------------------
// function to calculate fields 
// This function calls fomrat() to truncate number value up to the
// decimals specified.
//--------------------------------------------------------------------

function calculate_fields_old ( 
  p_amount , 
  p_row_no , 
  p_current_salary,
  p_no_of_rows)

{
   var l_percent_change ; 
   var l_annual_amount ; 

   var l_total_amount ; 
   var l_total_percent ; 
   var l_total_annual ; 
   var l_precision ; 

   l_precision = 
    parseInt (document.salary_form.p_decimal_precision.value ) ; 

   l_percent_change = p_amount/(p_current_salary/100) ; 
   l_percent_change = format ( l_percent_change , 3 ) ; 
   //   l_percent_change = roundOff( l_percent_change , 3 ) ; 

   l_annual_amount = p_amount *
                     document.salary_form.p_pay_annualization_factor.value ;

   if ( p_no_of_rows == 0 ) 
   {

     document.salary_form.p_change_percent.value=l_percent_change ; 
     document.salary_form.p_annual_salary.value= format(
                                             l_annual_amount,l_precision); 
     document.salary_form.p_change_value.value=p_amount ; 
     l_total_amount = p_amount ; 
     l_total_percent = document.salary_form.p_change_percent.value ; 
     l_total_annual = document.salary_form.p_annual_salary.value ; 
     l_total_amount = parseFloat(p_current_salary)+parseFloat(l_total_amount) ;


     l_total_annual = parseFloat(p_current_salary* 
                      document.salary_form.p_pay_annualization_factor.value)+
                      parseFloat(l_total_annual) ;
     document.salary_form.p_total_amount.value = 
       format(l_total_amount, l_precision) ; 
     document.salary_form.p_total_percent.value = 
       format(l_total_percent,3) ;
     document.salary_form.p_total_annual.value = format(l_total_annual,
                                                        l_precision) ;
   }
   else 
   {

      //populate the percentage and annual fields for this row
      document.salary_form.p_change_percent[p_row_no-1].value=
        l_percent_change ;   
     document.salary_form.p_annual_salary[p_row_no-1].value = 
       format( l_annual_amount, l_precision ) ; 
     document.salary_form.p_change_value[p_row_no-1].value=p_amount ; 

     //  now validate and calculate total amount 
     validate_all_amounts(p_no_of_rows) ; 

/*
     l_total_percent =
     (parseFloat(document.salary_form.p_total_amount.value )*100)/
                   parseFloat(document.salary_form.p_current_salary.value) ;


     l_total_amount = parseFloat(document.salary_form.p_total_amount.value )+
                      parseFloat(p_current_salary);
*/


     l_total_annual = parseFloat(l_total_amount *
         document.salary_form.p_pay_annualization_factor.value) ;            
     
     // now display new pay rate 
/*
     document.salary_form.p_total_amount.value = 
       format(l_total_amount, l_precision) ; 


     document.salary_form.p_total_percent.value = 
       format(l_total_percent,3) ;
     document.salary_form.p_total_annual.value = format(l_total_annual,
                                                        l_precision) ;
*/
  }

  calculate_quartile() ; 
  calculate_comparatio(); 
  return true ; 
}


//--------------------------------------------------------------------
// function to calculate amount, % and annual equivalent 
// if user enters total amount or total annual or total % 
// function to calculate amount , % and annual amount 
// if total is entered
//--------------------------------------------------------------------

function calc_pay_change(p_object ) 
{
  var ln_total_amount ;
  var ln_pecent_change ;
  var ln_annual_amount ; 
  var ln_current_salary;
  var ln_precision ;
  var ln_annual_factor ; 

  set_exit_form_changed();
  ln_precision =  
  parseInt (document.salary_form.p_decimal_precision.value ) ; 

  ln_annual_factor = 
  parseFloat(document.salary_form.p_pay_annualization_factor.value);
                  
  ln_current_salary = 
  parseFloat(document.salary_form.p_current_salary.value);

  if ( is_blank(p_object.value ))
  {

    document.salary_form.p_change_value.value="" ;
    document.salary_form.p_change_percent.value="" ; 
    document.salary_form.p_annual_salary.value = ""; 
    document.salary_form.p_total_amount.value= "";
    document.salary_form.p_total_percent.value= "";
    document.salary_form.p_total_annual.value= "";
    return ; 

  }

  if ( isNumber(p_object.value)== false)
  {
    alert(message_array[14]);

    document.salary_form.p_change_value.value="" ;
    document.salary_form.p_change_percent.value="" ; 
    document.salary_form.p_annual_salary.value = ""; 
    document.salary_form.p_total_amount.value= "";
    document.salary_form.p_total_percent.value= "";
    document.salary_form.p_total_annual.value= "";
    return ; 
  }
  if ( p_object.name == "p_total_amount")
  {

    ln_total_amount = parseFloat(p_object.value ) ; 

      // Bug 1949831 Fix:
    ln_total_amount = format(ln_total_amount, ln_precision);
  // Bug 1949831 Fix Ends

  }
  else if ( p_object.name=="p_total_percent")
  {
    if (isNumber(p_object.value)==false)
    {
      alert ( message_array[14]);
      return ; 
    }
    ln_percent_change= parseFloat(p_object.value);
    ln_total_amount =  parseFloat(ln_current_salary)+
                       (parseFloat(ln_current_salary/100)*ln_percent_change) ;
  }
  else 
  { 
    if (isNumber(p_object.value)==false )
    {
      alert ( message_array[14]);
      return ; 
    }

    ln_annual_amount = parseFloat(p_object.value ) ; 
    ln_total_amount = ln_annual_amount /ln_annual_factor ;
  }

  //now calculate and fill in change value ,  % change and 
  // annual change

  document.salary_form.p_change_value.value = roundOff(
                                               (ln_total_amount-
                                                ln_current_salary),ln_precision) ; 

 ln_percent_change = (document.salary_form.p_change_value.value/
                       ln_current_salary)*100

 document.salary_form.p_change_percent.value = format(ln_percent_change,3);
 document.salary_form.p_total_percent.value = format(ln_percent_change, 3);

 document.salary_form.p_annual_salary.value = 
     roundOff((document.salary_form.p_change_value.value*ln_annual_factor ), 
           ln_precision) ; 

 //add current salary to get the total new pay rate 

 ln_total_amount = ln_current_salary + 
                    parseFloat(document.salary_form.p_change_value.value );

  document.salary_form.p_total_amount.value =
               roundOff(ln_total_amount, ln_precision ) ; 

 // get total annual new pay rate
 ln_annual_amount = (ln_current_salary*ln_annual_factor ) + 
                     parseFloat(document.salary_form.p_annual_salary.value);


  document.salary_form.p_total_annual.value = 
               roundOff(ln_annual_amount , ln_precision ) ;

  calculate_quartile() ;
  calculate_comparatio() ; 
}


//function to check if the amount entered is 0 
// if so raise a warning 

function check_zero(num ) 
{
  if ( num == 0 ) 
  {
    alert ( message_array[15]); 
    return ; 
  }
}     

// this function is invoked when user enters any of the amount , % or annual 
// equivalent, this function calculates and populates all other fields 

function calcPercent(p_row_no, p_object)
{
    var l_percent_change;
    var l_current_salary;
    var l_new_amount; 
    var l_annual_amount ; 
    var l_new_salary ; 
    var no_of_rows ; 
    var l_precision ;
    var total_amount ; 

    no_of_rows = 
    parseInt (document.salary_form.p_no_of_components.value );

    //  if user entered a blank value , reset all the fields 
    if ( is_blank(p_object.value ))
    {
      reset_fields(p_row_no, no_of_rows ) ; 
      return ;
    }

    check_zero(p_object.value ) ; 

    set_exit_form_changed();
    l_precision = 
    parseInt (document.salary_form.p_decimal_precision.value ) ; 
/*
    no_of_rows = 
    parseInt (document.salary_form.p_no_of_components.value );
*/
    l_current_salary = parseFloat(p_object.form.p_current_salary.value);

    if ( p_object.name == "p_change_value")
    {
      if ( no_of_rows == 0  ) 
        l_new_amount = p_object.form.p_change_value.value ; 
      else 
        l_new_amount = p_object.form.p_change_value[p_row_no-1].value ;
      if ( isNumber (l_new_amount)==true )
      {
        l_new_amount = parseFloat(l_new_amount);
        l_new_amount=format(l_new_amount , l_precision ) ; 

        calculate_fields ( 
          l_new_amount , 
          p_row_no, 
          l_current_salary, 
          no_of_rows );

      }
      else 
      { // alert ,please enter valid pay rate changes
        alert ( message_array[14]);
        reset_fields(p_row_no, no_of_rows ) ; 
        return ;
      }
    }
     
    else if ( p_object.name == "p_change_percent")
    {
       if (no_of_rows==0 )
       { 
         l_percent_change =  p_object.form.p_change_percent.value ; 
       }
       else 
       {
         l_percent_change = 
         p_object.form.p_change_percent[p_row_no-1].value ;
       }
       if ( isNumber (l_percent_change ) == true ) 
       {
         l_percent_change = parseFloat(l_percent_change);
         l_new_amount = (l_current_salary/100)*l_percent_change ; 
         l_new_amount = format(l_new_amount, l_precision ) ; 
         calculate_fields ( 
           l_new_amount , 
           p_row_no, 
           l_current_salary, 
           no_of_rows );

       }
       else 
       {
         alert (message_array[14]);
         reset_fields(p_row_no, no_of_rows ) ;
         return ; 
       }
    }
    else
    {
      if ( no_of_rows==0 ) 
      {
        l_annual_amount = p_object.form.p_annual_salary.value;
      }
      else 
      {
        l_annual_amount = 
        p_object.form.p_annual_salary[p_row_no-1].value; 
      }  

      if ( isNumber(l_annual_amount)==true )
      { 
        l_new_amount = l_annual_amount/
                  document.salary_form.p_pay_annualization_factor.value ;
        l_new_amount = format(l_new_amount, l_precision ) ; 
        calculate_fields ( 
          l_new_amount , 
          p_row_no, 
          l_current_salary, 
          no_of_rows);


      }
      else 
      { // please enter valid pay rate changes
        alert ( message_array[14]);
        reset_fields(p_row_no, no_of_rows);
        return ;
      }
    }  
    l_new_salary = l_new_amount ; 
    document.salary_form.p_new_salary.value = l_new_salary ; 
           
}


function main_menu_gif_onClick(msg)
{
//  document.exit_form.p_exit_from.value="main_menu_onClick";

 document.salary_form.p_result_code.value="MAIN_MENU";
// if (document.salary_form.p_form_changed.value=="Y")
//  {
    if (confirm(msg))
    {
      document.salary_form.submit();
    }
/*
  }
  else
  {
    document.salary_form.submit();
  }
*/
}
  

function set_exit_form_changed()
{
  document.salary_form.p_form_changed.value="Y" ; 
}


<!-- ----------------------------------------------------------------------
<!-- --------------------- cancel_button_onClick --------------------------
<!-- ----------------------------------------------------------------------
//Display a confirmation
//message.  If confirmed to exit, exit to Person Search.

function cancel_button_onClick (msg)
{
  document.salary_form.p_result_code.value="CANCEL";
//  if (document.salary_form.p_form_changed.value=="Y")
//  {
    if (confirm(msg))
    {
      top.clear_container_bottom_frame(); 
      document.salary_form.target= "container_middle" ; 
      document.salary_form.submit();
    }
//  }
/*
  else
  {
    top.clear_container_bottom_frame(); 
    document.salary_form.target ="container_middle" ; 
    document.salary_form.submit();
  }
*/
}

// Function ViewInEuro_button_onClick
function ViewInEuro_button_onClick()
{
      if (document.dummy_parameter_form.p_rateexists.value == 'N')
        alert(message_array[30]);  
      else 
      {
      var viewineuro = "hr_salary_web.View_In_Euro?p_current_salary=";
      viewineuro =  viewineuro+document.salary_form.p_current_salary.value ;

      viewineuro =  viewineuro+"&p_new_salary=";
      viewineuro =  viewineuro+document.salary_form.p_new_salary.value;

      viewineuro =  viewineuro+"&p_no_of_components=";
      viewineuro =  viewineuro+document.salary_form.p_no_of_components.value;

      viewineuro =  viewineuro+"&p_grade_min=";
      viewineuro =  viewineuro+document.dummy_parameter_form.p_grade_min.value;

      viewineuro =  viewineuro+"&p_grade_mid=";
      viewineuro =  viewineuro+document.dummy_parameter_form.p_grade_mid.value;

      viewineuro =  viewineuro+"&p_grade_max=";
      viewineuro =  viewineuro+document.dummy_parameter_form.p_grade_max.value;

      viewineuro =  viewineuro+"&p_last_payrate_change=";
      viewineuro =  viewineuro+document.dummy_parameter_form.p_last_payrate_change.value;

      viewineuro =  viewineuro+"&p_currency=";
      viewineuro =  viewineuro+document.salary_form.p_currency.value;


      viewineuro =  viewineuro+"&p_pay_annualization_factor=";
      viewineuro =  viewineuro+document.salary_form.p_pay_annualization_factor.value;

      viewineuro =  viewineuro+"&p_payroll_id=";
      viewineuro =  viewineuro+document.salary_form.p_payroll_id.value;

      viewineuro =  viewineuro+"&p_effective_date=";
      viewineuro =  viewineuro+document.salary_form.p_effective_date.value;

      viewineuro =  viewineuro+"&j=";
      viewineuro =  viewineuro+document.salary_form.j.value;


      if (document.salary_form.p_no_of_components.value < 1 ) {

      viewineuro =  viewineuro+"&p_change_value=";
      viewineuro =  viewineuro+document.salary_form.p_change_value.value;
     
      var list = document.salary_form.P_REASON_CODE;

      viewineuro =  viewineuro+"&P_REASON_CODE=";

      viewineuro =  viewineuro+list.options[list.selectedIndex].value;


      if ( is_blank(document.salary_form.p_change_value.value))
      alert(message_array[28]);
      else
      {
       if (!EuroWindow || EuroWindow.closed){
      EuroWindow=window.open(viewineuro,"","status=no,menubar=no,scrollbars=yes,toolbar= no,location=no,resizable=no,alwaysRaised=yes,HEIGHT=350,WIDTH=500");

        EuroWindow.opener = window;
                                                              }
        else {
	  EuroWindow.close()
        EuroWindow=window.open(viewineuro,"","status=no,menubar=no,scrollbars=yes,toolbar= no,location=no,resizable=no,alwaysRaised=yes,HEIGHT=350,WIDTH=500");

        EuroWindow.opener = window;
	     }
      }
      
      }
      else 
      {
      for (var i = 0; i <= document.salary_form.p_no_of_components.value; i++) 
      {
      var list = document.salary_form.P_REASON_CODE[i];

      viewineuro =  viewineuro+"&p_change_value=";
      viewineuro =  viewineuro+document.salary_form.p_change_value[i].value;
      viewineuro =  viewineuro+"&P_REASON_CODE=";
      viewineuro =  viewineuro+list.options[list.selectedIndex].value; 
      }	   
       if (!EuroWindow || EuroWindow.closed){ 
      EuroWindow=window.open(viewineuro,"","status=no,menubar=no,scrollbars=yes,toolbar=no,location=no,resizable=no,alwaysRaised=yes,HEIGHT=400,WIDTH=500");
        EuroWindow.opener = window;
                                                              }
        else {
	  EuroWindow.close()
        EuroWindow=window.open(viewineuro,"","status=no,menubar=no,scrollbars=yes,toolbar= no,location=no,resizable=no,alwaysRaised=yes,HEIGHT=350,WIDTH=500");

        EuroWindow.opener = window;
             }

      }
    }
}


