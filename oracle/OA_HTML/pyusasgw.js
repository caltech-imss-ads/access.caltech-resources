<!-- Hide from browsers which cannot handle JavaScript. 
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : hrmecmnw.js
// Description : This file contains all the common JavaScript routines
//               used in Assignment Employee Screen(online Payslip)
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------- ----------   ------- ------- ---------------------------------
// ahanda      07-FEB-2000  110.1           Changed verify_form_data function
//                                          to close the window if assignment
//                                          has not changed.
// ahanda      17-JUL-1999  110.0           Created.
// ==========================================================================
// $Header: pyusasgw.js 115.0 2000/02/28 15:45:15 pkm ship      $

// ------------------- Global Variables -------------------

// Detect  browser type and version
var Nav4 = ((navigator.appName == "Netscape") && 
            (parseInt(navigator.appVersion) == 4));
var browserVer = parseInt(navigator.appVersion);

// Flag to check whether we have submitted anything to server.
var submit_in_progress = "N";

// Place holder for sub window.
var subWindow;

var Win32;

if (Nav4) {
  Win32 = ((navigator.userAgent.indexOf("Win") != -1) && 
           (navigator.userAgent.indexOf("Win16") == -1));
} else {
  Win32 = ((navigator.userAgent.indexOf("Windows") != -1) && 
           (navigator.userAgent.indexOf("Windows 3.1") == -1));
}

// ------------------- Function Definitions -------------------

// Function which does nothing. Used in print function.
function do_nothing () {
  return true;
}


// This function will reload the the container middle frame.
function reload_gif_onClick () {
  top.container_middle.history.go(0);
}


// This function will stop reloading the the container middle frame.
function stop_gif_onClick () {
  if (Nav4) {
    void top.container_middle.stop();
  }
  else if (Win32) {
    top.container_middle.focus();
    // requires same <OBJECT> as printing
    IEControl.ExecWB(23, 0);
  }
}


// Function to generate a blank page with dark blue color.
function get_dark_blue_page () {
  return "<HTML><BODY BGCOLOR=#336699></BODY></HTML>"
}

// Function to generate a blank page with gray color.
function get_gray_page () {
  return "<HTML><BODY BGCOLOR=#CCCCCC></BODY></HTML>"
}


// This function will clear the container bottom frame so that buttons are
// not displayed when processing is going on in the container middle frame.
function clear_container_bottom_frame () {
  parent.container_bottom.document.open();
  parent.container_bottom.document.write (
    "<HTML>" +
    "<HEAD>" +
    "</HEAD>" +
    "<BODY BGCOLOR=#336699>" +
    "  <TABLE  WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>" +
    "  <TR BGCOLOR=#CCCCCC>" +
    "    <TD ALIGN=LEFT><IMG SRC=" + FND_IMAGES['FNDCTBL'] + " BORDER=0></TD>" +
    "    <TD WIDTH=100%><IMG SRC=" + FND_IMAGES['FNDPXG5'] + " BORDER=0></TD>" +
    "    <TD ALIGN=RIGHT><IMG SRC=" + FND_IMAGES['FNDCTBR'] + " BORDER=0></TD>" +
    "  </TR>" +
    "</TABLE>" +
    "</BODY>" +
    "</HTML>"
  );
  parent.container_bottom.document.close();
}


// Function to refresh container_top and container_bottom frames.
function refresh_frames (p_container_top_frame, p_container_bottom_frame) {
  if (p_container_top_frame) {
    if (top.container_top.location.href != p_container_top_frame) {
      top.container_top.location.href = p_container_top_frame;
    }
  }
  if (p_container_bottom_frame) {
    if (top.container_bottom.location.href != p_container_bottom_frame) {
      top.container_bottom.location.href = p_container_bottom_frame;
    }
  }
}


// Roll over image functions for onMouseOver and onMouseOut JavaScript events.
function set_onMouse_gif (p_document_name, p_image_name, p_gif_file) {
  if (browserVer >= 3 ) {
    eval(p_document_name + '.' + p_image_name + '.src=' + '"' + p_gif_file+'"');
  }
  else {
    return true;
  }
}


// Function to check whether the user has selected any value or not
function isBlank(str)
{
   //alert("Total Records : " + str.length);
   for (var i = 0; i < str.length; i++)
   {
      //alert("checked Value : " + str[i].checked);
      if (str[i].checked) {
         return true
      }
   }

   return false
} //End of function


function RadioValue(str)
{
  for (var i = 0; i < str.length; i++)
  {
    if (str[i].checked)
    {
      return str[i].value
    }
  }
} //End of function


function verify_form_data (p_caller)
{
  var CheckedRadio = document.MultiAssignment.SelectAssignment

  if (isBlank(CheckedRadio))
  {
    var HiddenMiddleForm = self.opener.frames.container_middle.document.forms.PayslipMiddle;

    if (HiddenMiddleForm.p_assignment_id.value != RadioValue(CheckedRadio))
    {

       HiddenMiddleForm.p_assignment_id.value = RadioValue(CheckedRadio);
       for (var i = self.opener.top.p_array_length; i >= 0; i--)
       {
         delete self.opener.top.paydata[i]
       }

       HiddenMiddleForm.p_recreate_array.value = "Y"

       HiddenMiddleForm.submit();
    }

    window.close();
  }
  else
  {
    alert (FND_MESSAGES['HR_SELECT_MULTI_ASSIGNMENT_WEB']);
  }
} //End of function

<!-- Done hiding from browsers which cannot handle JavaScript. -->
