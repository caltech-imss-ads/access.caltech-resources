<%--=========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  FILENAME                                                                 |
 |        asfFrcstJslib.js                                                   | 
 |                                                                           | 
 |  DESCRIPTION                                                              |
 |     Forecast - Java Script Library                                        |
 |  NOTES                                                                    |
 |                                                                           |
 |  DEPENDENCIES                                                             |
 |  HISTORY                                                                  |
 |                                                                           |
 |       11-July-2000  sachanna created                                      |
 |+========================================================================= |
 |                                                                           |            
 +========================================================================= --%>
 <%-- $Header: asfFrcstJslib.js 115.13 2001/03/24 15:16:16 pkm ship      $ --%>
 
 
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
 



var valuechanged = false;    
var purchaseLines = new Array();    
var ns4 = document.layers?true:false;    



function setFlag(s) { 	
   var JapplyFlag = s;
    document.forms["fctDetail"].action='<%=ServletSessionManager.getURL("asfFrcstOppwsMain.jsp")%>';
   document.forms["fctDetail"].elements["applyFlag"].value = JapplyFlag;   
   document.forms["fctDetail"].elements["forecastId"].value = null;
   document.forms["fctDetail"].elements["forecastWksId"].value = null;
   document.forms["fctDetail"].elements["startFrom"].value = 1;
	document.forms["fctDetail"].submit(); 
}   

function _replaceChar(str,c,r,recur)
{
	var temp='';
	if(recur == null) recur = true;

	if(recur == false)
	{
		var pos = str.indexOf(c);
		if(pos!=-1)
			temp = str.substring(0,pos)+r+str.substring(pos+1,str.length);
		else
			temp = str;
	}
	else
	for(var i=0; i<str.length;i++)
		if(str.charAt(i)==c)
		  	temp +=r;
		else
		 	temp +=str.charAt(i);

return temp;
}
  
function _parseNumber(number,D,G,L)
{

if(G)
number = _replaceChar(number,G,'');
if(D)
number = _replaceChar(number,D,'.');
if(L)
number = _replaceChar(number,L,'');

number=number.replace(/</,'-');
number=number.replace(/>/,'');
number = _replaceChar(number,' ','');

 
if(arguments[3])
	for(var i=3;i<arguments.length;i++)
		number = _replaceChar(number,arguments[i],'');

if(isNaN(number))
{
   alert("<%=errorInvalidNumber%>");
   return null;
	}
	else
	{
		var s = parseFloat(number);
		return s;
	}       
		return eval(number);
}      
 
 
function isNumber(inNumber) {
                
                if (inNumber.value.length == 0)
                 return 0;
                 
                var myval =inNumber.value;
                myval=myval.replace(/</,'-');
                myval=myval.replace(/>/,'');
                
                var numStr="0123456789.,-";
                var charStr;
                var counter = 0;

                var i = 0;

                for (i; i < myval.length ;  i++)
                {
                       charStr = myval.substring(i, i+1);
                       if (numStr.indexOf(charStr) != -1)
                           counter ++;
                }
                  if (counter != myval.length)
                  {
		    return 0;
                  }
	return 1;
  }  
 
 
function validateAmount(inAmt,j) {
   var myfield = inAmt.name;
  
    if (isNumber(inAmt) == 0) {
      alert("<%=errorInvalidNumber%>");
		if (myfield == 'wcAmount'+j) {	
		inAmt.value = inAmt.defaultValue;
         	inAmt.focus();
		}
		if (myfield == 'fcAmount'+j) {	
		inAmt.value = inAmt.defaultValue;
         	inAmt.focus();
		}
		if (myfield == 'bcAmount'+j) {	
		inAmt.value = inAmt.defaultValue; 
         	inAmt.focus();
		}
		valuechanged = false;
		return false;
	}	//end if to check if value entered is a number

	// validation complete - input amount is a valid number amount 
	// check if the input amount is undefined. 
//			alert("values of inAmt.value before parse: " + inAmt.value);
  	     	inAmt.value =  _parseNumber(inAmt.value,'.',','); 
//			alert("values of inAmt.value before parse: " + inAmt.value);
			if (inAmt.value == 'undefined' || inAmt.value == 'NaN') { 
			   inAmt.value = inAmt.defaultValue;
			   alert("<%=errorInvalidNumber%>");
			   inAmt.focus();
			   return false;
			}
          
		if (inAmt.value == null || inAmt.value == 'null') { 
	     inAmt.value = inAmt.defaultValue;
	     inAmt.focus();
		valuechanged = false;  
		return false;
		}
	
			valuechanged = true;
          	document.forms["fctDetail"].elements["changed"+j].value = "1";
			return true;  
}   
   
    
function markRow(rowNum) { 
   var Num = rowNum;
	valuechanged = true;    
	document.forms["fctDetail"].elements["changed" + rowNum].value = "1";
}
 
    

function changePage(sFrom) {  
    var sf=sFrom;
    document.forms["fctDetail"].elements["downLoad"].value = 0;
    //var pt=pType;
     //alert(valueHolder);
     document.fctDetail.startFrom.value = sFrom;
     //document.fctDetail.periodType.value = pType;
    //document.fctDetail.submit();
	if (valuechanged) {  
	   var unsaveMsg = 'You have unsaved data on this page. Do you want to save before you leave?. Click "Ok" to Save and Click "Cancel" to discard the change';  
		if (confirm("<%=checkUnsavedData%>")) {    
			document.visibility = false;    
			valuechanged = false;
			
			document.forms["fctDetail"].submit(); 
			
		} else { 
		//document.forms["fctDetail"].elements["periodType"].value = valueHolder;   
		return false; 
		}
	}  
	 document.forms["fctDetail"].submit(); 
	}  
	  
 
function periodChange(s) { 
   var submitFlag = s
   document.forms["fctDetail"].elements["startFrom"].value = "1";  
   document.forms["fctDetail"].elements["periodChanged"].value = "T"; 
   document.forms["fctDetail"].elements["dropDownChanged"].value = "T";
   document.forms["fctDetail"].elements["downLoad"].value = 0;
   document.forms["fctDetail"].elements["applyFlag"].value = 0; 
   document.forms["fctDetail"].action='<%=ServletSessionManager.getURL("asfFrcstOppwsMain.jsp")%>';
  	if (valuechanged) {  
	   var unsaveMsg = 'You have unsaved data on this page. Do you want to save before you leave?. Click "Ok" to Save and Click "Cancel" to discard the change';  
		if (confirm("<%=checkUnsavedData%>")) {    
			document.visibility = false;    
			valuechanged = false;
			document.forms["fctDetail"].elements["save"].value = submitFlag;   
			document.forms["fctDetail"].submit(); 
			
		} else { 
		//document.forms["fctDetail"].elements["periodType"].value = valueHolder; 
		document.forms["fctDetail"].submit();   
		//return false; 
		}
		 
	}  
   	 document.forms["fctDetail"].submit(); 
	}
	
	
function checkUnsavedData(s) { 
   var submitFlag = s
   document.forms["fctDetail"].elements["startFrom"].value = "1";  
   document.forms["fctDetail"].elements["downLoad"].value = 0;
   document.forms["fctDetail"].action='<%=ServletSessionManager.getURL("asfFrcstOppwsMain.jsp")%>';
  	if (valuechanged) {  
	   var unsaveMsg = 'You have unsaved data on this page. Do you want to save before you leave?. Click "Ok" to Save and Click "Cancel" to discard the change';  
		if (confirm("<%=checkUnsavedData%>")) {    
			document.visibility = false;    
			valuechanged = false;
			document.forms["fctDetail"].elements["save"].value = submitFlag;   
			document.forms["fctDetail"].submit(); 
			
		} else { 
		//document.forms["fctDetail"].elements["periodType"].value = valueHolder; 
		document.forms["fctDetail"].submit();   
		//return false; 
		}
		 
	}  
   	 document.forms["fctDetail"].submit(); 
	}
 

function manualSubmit(s) { 
	
   var submitFlag = s;
 if (document.forms["fctDetail"].elements["fromPage"].value != "Forecast") {  
   var sindexCH = false;
   var sindexGR =document.forms["fctDetail"].elements["groupRole"].selectedIndex; 
   var sindexSPN =document.forms["fctDetail"].elements["selectPeriodName"].selectedIndex; 

   
   if (!document.forms["fctDetail"].elements["groupRole"].options[sindexGR].defaultSelected) {
  	sindexCH = true;
       }
       
   if (!document.forms["fctDetail"].elements["selectPeriodName"].options[sindexSPN].defaultSelected) {
  	sindexCH = true;
       }
           
   // alert (document.forms["fctDetail"].elements["groupRole"].options[sindex].defaultSelected);
  
  
  
   if (grouproleflag || periodnameflag || sindexCH) { 
  
   alert ("<%=oracle.apps.asf.util.EncodeUtil.jsEncode(notSaveMsg)%>"); 
  
   } 
   else { 
   	
           document.forms["fctDetail"].elements["save"].value = submitFlag;
           document.forms["fctDetail"].elements["downLoad"].value = 0; 
           document.forms["fctDetail"].action='<%=ServletSessionManager.getURL("asfFrcstOppwsMain.jsp")%>';   
           document.forms["fctDetail"].submit();
      }
} else {
 	   document.forms["fctDetail"].elements["save"].value = submitFlag;
           document.forms["fctDetail"].elements["downLoad"].value = 0; 
           document.forms["fctDetail"].action='<%=ServletSessionManager.getURL("asfFrcstOppwsMain.jsp")%>';   
           document.forms["fctDetail"].submit();
      }	     
      
  }                                                 
  
function download(s) { 
   var downloadFlag = s
   
   //if (confirm("Do you want to Down Load all pages")){
    //document.forms["fctDetail"].elements["downloadAll"].value = downloadFlag;
   //}
   
   document.forms["fctDetail"].elements["downLoad"].value = downloadFlag;   
   document.forms["fctDetail"].submit();
  
}        


function verify_number(num_holder)
{
  var newnum=num_holder.value;
  var numStr="0123456789.,";
  var thisChar,counter=0;
  if (num_holder.value == "") return;
  for (var i=0;i<newnum.length;i++)
  {
     thisChar=newnum.substring(i,i+1);
     if (numStr.indexOf(thisChar) != -1) counter++;
  }
  
  if (counter != newnum.length)
  {
   num_holder.focus();
   alert("<%=errorInvalidNumber%>"); 
   //alert("Only numeric values allowed!");
   num_holder.value = "";
    }
 
  return;
}
 




function remove_commas(mynum)
{
 var temp= mynum;
 var newnum,idx;
 while ((idx=temp.indexOf(",")) != -1)
 {
   newnum=temp.substring(0,idx);
   newnum+=temp.substring(idx+1,temp.length);
   temp=newnum;
 }
 return temp;
}
  
function limit2dec(val)
{
  if (val == 0) return ("0");
  if (val == "") return ("");
  var inputVal = ("" + val);
  if (inputVal.indexOf(".") == -1) inputVal+=".000";
  var decPart = inputVal.substring(0,inputVal.indexOf("."));
  var fracPart = inputVal.substring(inputVal.indexOf(".")+1,inputVal.indexOf(".")+3);
  var dec3Part=parseInt(inputVal.substring(inputVal.indexOf(".")+3,inputVal.indexOf(".")+4));
  if ((dec3Part >= 5))
  {
   if (fracPart.indexOf("0") == 0) fracPart = "0" + (parseFloat(fracPart)+1);
   else
   fracPart= ""+ (parseFloat(fracPart) + 1);
  }
  return ("" + decPart + "." + fracPart);
}


function manualFrcstSubmit(flag) {
	valuechanged = false;
	   flag.value = 'Y';
		document.forms["forecast"].submit();
	} 

function applyFilterValues() {
		valuechanged = false;
	 document.forms["forecast"].elements["oppWktFlag"].value = "";
	 document.forms["forecast"].elements["prodWktFlag"].value = "";   
		//flag.value = 'Y';
		document.forms["forecast"].submit();

} 

function validateFrcstAmount(inAmt) {
   var myfield = inAmt.name;

  if (isNumber(inAmt) == 0) {
   alert("<%=errorInvalidNumber%>"); 
	   inAmt.value = inAmt.defaultValue;
	   inAmt.focus();
	   valuechanged = false;
	   return false;
  }    //end if to check if value entered is a number

   // validation complete - input amount is a valid number amount
   // check if the input amount is undefined or NaN
	   inAmt.value =  _parseNumber(inAmt.value,'.',',');
   // if user has entered a null value -- then reassign
   // it to a 0 value.
	   if (inAmt.value == 'undefined' || inAmt.value == 'NaN') {
		 inAmt.value = "0";
	 }
	
	if (inAmt.value == null || inAmt.value == 'null') {      
              inAmt.value = inAmt.defaultValue;
		    inAmt.focus();
		    valuechanged = false;
	         return false;
	    }
		    valuechanged = true;
	    //   document.forms["forecast"].elements["changed"].value = "1";
		    return true;
}        


function changeGroup() {
	   var unsaveMsg = 'You have unsaved data on this page. Do you want to save before you leave?. Click "Ok" to Save and Click "Cancel" to discard the change';  

	if (valuechanged) {
		 if (confirm("<%=checkUnsavedData%>")) {
		 document.visibility = false;
		 valuechanged = false;
	//	 document.forms["forecast"].elements["fctFlag"].value = "Y";
	 document.forms["forecast"].elements["oppWktFlag"].value = "";
	 document.forms["forecast"].elements["prodWktFlag"].value = "";
		 document.forms["forecast"].submit();
	 }
   }
	   document.forms["forecast"].submit();
}

