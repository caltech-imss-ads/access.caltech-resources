/*===========================================================================+
 |               Copyright (c) 1999 Oracle Corporation                       |
 |                  Redwood Shores, California, USA                          |
 |                       All rights reserved.                                |
 +===========================================================================*/
/* $Header: POSASNED.js 115.2 1999/11/30 12:19:51 pkm ship   $ */

var check_state = false;

var r_row;

var lov_win;

var uom;
var wip_job_info;
var wip_entity_id;
var wip_oper_seq;
var wip_line_id;
var dist_id;
var item_id;
var item_desc;

var r_attribute_code;

var intervalID;

function explode()
{
 
  document.POS_ASN_SHIPMENTS.POS_SUBMIT.value = "EXPLODE";
  document.POS_ASN_SHIPMENTS.submit();

}

function delt()
{
  document.POS_ASN_SHIPMENTS.POS_SUBMIT.value = "DELETE";
  document.POS_ASN_SHIPMENTS.submit();
}

function check_all()
{
 check_state = !check_state;
 if (document.POS_ASN_SHIPMENTS.POS_SELECT[0] == null)
    document.POS_ASN_SHIPMENTS.POS_SELECT.checked = check_state;
 else 
   { for (var i=0; i < document.POS_ASN_SHIPMENTS.POS_SELECT.length; i++)
      document.POS_ASN_SHIPMENTS.POS_SELECT[i].checked = check_state;
   }
}

function details(p_detail_row)
{
 var asn_id;
 var split_id;
 var quantity;
 var unit_of_measure;

 if (document.POS_ASN_SHIPMENTS.POS_ASN_LINE_ID[0] == null)
   {
     asn_id   = document.POS_ASN_SHIPMENTS.POS_ASN_LINE_ID.value;
     split_id = document.POS_ASN_SHIPMENTS.POS_ASN_LINE_SPLIT_ID.value;
     quantity = document.POS_ASN_SHIPMENTS.POS_QUANTITY_SHIPPED.value;
     unit_of_measure = document.POS_ASN_SHIPMENTS.POS_UNIT_OF_MEASURE.value;
   }
 else
   {
     asn_id   = document.POS_ASN_SHIPMENTS.POS_ASN_LINE_ID[p_detail_row].value;
     split_id = document.POS_ASN_SHIPMENTS.POS_ASN_LINE_SPLIT_ID[p_detail_row].value;
     quantity = document.POS_ASN_SHIPMENTS.POS_QUANTITY_SHIPPED[p_detail_row].value;
     unit_of_measure = document.POS_ASN_SHIPMENTS.POS_UNIT_OF_MEASURE[p_detail_row].value;
   } 

 top.getTop().openShipmentDetails(asn_id, split_id, quantity, unit_of_measure);

}

function watchme()
{
  if (lov_win.closed)
  {
   if (r_attribute_code == "POS_UNIT_OF_MEASURE")
    { uom.value = document.POS_ASN.POS_UNIT_OF_MEASURE.value;
    }
   if (r_attribute_code == "POS_ASN_WIP_JOB")
    {
      wip_job_info.value  = document.POS_ASN.POS_ASN_WIP_JOB.value;
      wip_entity_id.value = document.POS_ASN.POS_WIP_ENTITY_ID.value;
      wip_oper_seq.value  = document.POS_ASN.POS_WIP_OPERATION_SEQ_NUM.value;
      wip_line_id.value   = document.POS_ASN.POS_WIP_LINE_ID.value; 
      dist_id.value      = document.POS_ASN.POS_PO_DISTRIBUTION_ID.value;
    }
   if (r_attribute_code == "POS_ITEM_DESCRIPTION")
    {
      item_id.value   = document.POS_ASN.POS_ITEM_ID.value;
      item_desc.value = document.POS_ASN.POS_ITEM_DESCRIPTION.value;
    }
   clearInterval(intervalID);
   return;
  }
}

function call_LOV(c_attribute_code, c_row, l_script, l_row)
{
  var c_js_where_clause = "";
  var interval = 100;

  if (c_attribute_code == "POS_UNIT_OF_MEASURE")
   {
    if (document.POS_ASN_SHIPMENTS.POS_UNIT_OF_MEASURE[0] == null)
     {
       uom = document.POS_ASN_SHIPMENTS.POS_UNIT_OF_MEASURE;
       c_js_where_clause = "uom_class='" + document.POS_ASN_SHIPMENTS.POS_UOM_CLASS.value + "'";
     }
    else
     {
       uom = document.POS_ASN_SHIPMENTS.POS_UNIT_OF_MEASURE[c_row];     
       c_js_where_clause = "uom_class='" + document.POS_ASN_SHIPMENTS.POS_UOM_CLASS[c_row].value + "'";
     }

    document.POS_ASN.POS_UNIT_OF_MEASURE.value = uom.value;
   }
  
  if  (c_attribute_code == "POS_ASN_WIP_JOB")
   {
    if (document.POS_ASN_SHIPMENTS.POS_ASN_WIP_JOB[0] == null)
     {
       wip_job_info  = document.POS_ASN_SHIPMENTS.POS_ASN_WIP_JOB;
     }
    else
     {
       wip_job_info  = document.POS_ASN_SHIPMENTS.POS_ASN_WIP_JOB[l_row];
     }
    document.POS_ASN.POS_ASN_WIP_JOB.value = wip_job_info.value;
    wip_entity_id = document.POS_ASN_SHIPMENTS.POS_WIP_ENTITY_ID[c_row];
    wip_oper_seq  = document.POS_ASN_SHIPMENTS.POS_WIP_OPERATION_SEQ_NUM[c_row];
    wip_line_id   = document.POS_ASN_SHIPMENTS.POS_WIP_LINE_ID[c_row];
    dist_id       = document.POS_ASN_SHIPMENTS.POS_PO_DISTRIBUTION_ID[c_row];
   
    if (document.POS_ASN_SHIPMENTS.POS_PO_LINE_LOCATION_ID[0] == null)
    {
      c_js_where_clause = "line_location_id=" + 
                          document.POS_ASN_SHIPMENTS.POS_PO_LINE_LOCATION_ID.value;
    }
    else 
    {
      c_js_where_clause = "line_location_id=" + 
                          document.POS_ASN_SHIPMENTS.POS_PO_LINE_LOCATION_ID[c_row].value;
    }
   }

  if  (c_attribute_code == "POS_ITEM_DESCRIPTION")
   {
    if (document.POS_ASN_SHIPMENTS.POS_ITEM_DESCRIPTION[0] == null)
     {
       item_id   = document.POS_ASN_SHIPMENTS.POS_ITEM_ID;
       item_desc = document.POS_ASN_SHIPMENTS.POS_ITEM_DESCRIPTION;
       c_js_where_clause = "item_id=" + document.POS_ASN_SHIPMENTS.POS_ITEM_ID.value;
     }
    else
     {
       item_id   = document.POS_ASN_SHIPMENTS.POS_ITEM_ID[c_row];
       item_desc = document.POS_ASN_SHIPMENTS.POS_ITEM_DESCRIPTION[c_row];
       c_js_where_clause = "item_id=" + document.POS_ASN_SHIPMENTS.POS_ITEM_ID[c_row].value;
     }
   }

  r_row = c_row;
  r_attribute_code = c_attribute_code;

  c_js_where_clause = escape(c_js_where_clause, 1);

  lov_win = window.open(l_script + "/icx_util.LOV?c_attribute_app_id=178" + 
                         "&c_attribute_code=" + c_attribute_code +
                         "&c_region_app_id=178" +
                         "&c_region_code=POS_ASN_SHIPMENTS" + 
                         "&c_form_name=POS_ASN"  +
                         "&c_frame_name=shipments" + 
                         "&c_where_clause=" +
                         "&c_js_where_clause=" + c_js_where_clause,"LOV",
                         "resizable=yes,menubar=yes,scrollbars=yes,width=780,height=300");


  intervalID = window.setInterval("watchme()", interval);

}

function call_lov(c_attribute_code)
{
  var c_js_where_clause = "";

  if (c_attribute_code == "POS_FREIGHT_CARRIER")
  {
    c_js_where_clause = "ORGANIZATION_ID=" + document.POS_ASN_HEADER.POS_SHIP_TO_ORGANIZATION_ID.value;
  }

  c_js_where_clause = escape(c_js_where_clause, 1);

  LOV("178", c_attribute_code, "178", "POS_ASN_HEADERS", "POS_ASN_HEADER",
      "header", "", c_js_where_clause);
}


function LoadPage(sub_state, msg, but1, but2, but3)
{
  if (sub_state == "SUBMIT"){
     parent.shipments.document.POS_ASN_SHIPMENTS.submit();
  }

  if (sub_state == 'OK')
  {  
    var w = window.open("", "error", "resizable=yes,scrollbars=yes,toolbar=no,status=yes,menubar=no,width=600,height=200");
    var d = w.document;

    d.write(msg);  
    d.write('<table align=center cellpadding=0 cellspacing=0 border=0>');
    d.write('<tr>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBRNDL.gif ></td>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif ></td>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBSQRR.gif ></td>');
    d.write('<td width=15 rowspan=5></td>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBSQRL.gif ></td>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif ></td>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBRNDR.gif ></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td>');
    d.write('<td bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td>');
    d.write('</tr>');
    d.write(' <tr>');
    d.write('<td bgcolor=#cccccc height=20 nowrap>');
    d.write('<a href="javascript:top.opener.top.backClicked();javascript:self.close()"><font class=button>');
    d.write(but1);
    d.write('</font></a></td>');
    d.write('<td bgcolor=#cccccc height=20 nowrap>'); 
    d.write('<a href="javascript:top.opener.top.recursiveClose();javascript:self.close()"><font class=button>');
    d.write(but2);
    d.write('</font></a></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#666666><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('<td bgcolor=#666666><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('</tr>');
    d.write('</table>');
    d.write('</body></html>');
    d.close();
  }
 
  if (sub_state == 'ERROR')
  {
    if (but1 == ""){
      var f = top.controlregion;
      var b = top.upperbanner;

      var controlRegionURL = top.scriptName + 
                           "/pos_control_region_sv.PaintControlRegion";
      var bannerURL = top.scriptName + "/pos_upper_banner_sv.PaintUpperBanner";

      f.location.href = controlRegionURL + "?p_position=EDIT";
      b.location.href = bannerURL + "?p_product=ICX" +
                       "&p_title=ICX_POS_ASN_EDIT"; 
      top.whereAmI = "EDIT";
    }

    var w = window.open("", "error", "resizable=yes,scrollbars=yes,toolbar=no,status=yes,menubar=no,width=600,height=400");
    var d = w.document;

    d.write(msg);
    d.write('<table align=center cellpadding=0 cellspacing=0 border=0>');
    d.write('<tr>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBRNDL.gif ></td>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif ></td>');
    d.write('<td rowspan=5><img src=/OA_MEDIA/FNDBRNDR.gif></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#ffffff><img src=/OA_MEDIA/FNDPX6.gif></td>');
    d.write('</tr>');
    d.write(' <tr>');
    d.write('<td bgcolor=#cccccc height=20 nowrap>');
    d.write('<a href="javascript:self.close()"><font class=button>');
    d.write(but3);
    d.write('</font></a></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#666666><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('</tr>');
    d.write('<tr>');
    d.write('<td bgcolor=#333333><img src=/OA_MEDIA/FNDPX3.gif></td>');
    d.write('</tr>');
    d.write('</table>');
    d.write('</body></html>');
    d.close();
  }
}
