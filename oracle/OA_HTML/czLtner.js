/* $Header: czLtner.js 115.9 2000/12/10 16:10:36 pkm ship     $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function Listener()
{
}

function Listener_addListener(sub,target,type)
{
  if(!this.Listeners) 
    this.Listeners = new Array();
  if (!this.Listeners[sub])
    this.Listeners[sub] = new Array();
  
  var currListener = this.Listeners[sub];	
  if(type =='super'){
    for(var i=currListener.length; i>0; i--)
      currListener[i] = currListener[i-1];
    currListener[0] = target;
  }
  else 
    currListener[currListener.length] = target;
}

function Listener_removeListener(sub,target)
{
  var currListener;
  if(this.Listeners) {
    if(this.Listeners[sub]) {
      currListener = this.Listeners[sub];
      var i=0;
      var len = currListener.length;
      while (i < len) {
        if (currListener[i] == target) {
          for (var j=i; j<len-1; j++) {
            currListener[j] = currListener[j+1];
          }
          currListener[len-1] = null;
          currListener.length-- ;
          break;
        } else
          i++
      }
    }
  }
}

function Listener_notifyListeners(sub,e,src)
{

  var result = true;

  if(!src)
    src = this;
  
  if(this.Listeners) {
    if (this.Listeners[sub]) {
      var listeners = this.Listeners[sub];
      //before starting to notify make a copy of listener references;
      var curListeners = new Array ();
      var len = listeners.length;
      for (var j=0; j < len; j++) {
        curListeners[j] = listeners[j];
      }
      for(var i = 0; i < len; i++) {
        if(result) {
          if('object' == typeof curListeners[i]) {
            if(eval('curListeners[i].'+sub))
              result = eval('curListeners[i].'+sub+'(e, src)');
          }
          else if ('function' == typeof curListeners[i]) {		
            if(curListeners[i])
              result = curListeners[i](e,src);
          }
          else 
            result = eval(curListeners[i]);
        }
        else break;
      }
    }
  }
  return result;
} 

function Listener_clearQueue () 
{
  var id;
  for (id in this.Listeners) {
    if (this.Listeners[id]) {
      var len = this.Listeners[id].length;
      for (var i=0; i<len; i++) {
        this.Listeners[id][i] = null;
        delete this.Listeners[id][i];
      }
      this.Listeners[id].length = 0;
    }
    this.Listeners[id] = null;
    delete this.Listeners[id];
  }
  this.Listeners = null;
  delete this.Listeners;
}

Listener.prototype.addListener 		= Listener_addListener;
Listener.prototype.removeListener 	= Listener_removeListener;
Listener.prototype.notifyListeners 	= Listener_notifyListeners;
Listener.prototype.clearQueue = Listener_clearQueue;
