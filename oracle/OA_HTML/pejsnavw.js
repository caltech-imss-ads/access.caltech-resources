 
<!-- hide the script's contents from feeble browsers
<!-- $Header: pejsnavw.js 115.2 2000/04/10 16:51:44 pkm ship   $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- 110.0	28-AUG-1998	flemonni	Created	
<!-- 110.2      14-SEP-1998     flemonni          Working File	
<!-- 110.3      22-SEP-1998     flemonni          Working File	
<!-- 110.4      30-SEP-1998     flemonni          Working File	
<!-- 110.5      19-SEP-1998     flemonni          Working File	
<!-- 110.6      21-SEP-1998     flemonni          Working File	
<!-- 110.7      23-NOV-1998     flemonni          Working File	
<!-- 110.8      30-NOV-1998     flemonni          Working File	
<!-- 110.9      02-DEC-1998     flemonni          Working File	
<!-- 110.10     17-DEC-1998     flemonni          Working File	
<!-- 110.11     23-DEC-1998     flemonni          Working File	
<!-- 110.12     08-Feb-1999     fychu    815459   In SubmitProcess function,
<!--                                              added code to check textarea
<!--                                              length if there is no        
<!--                                              mandatory fields on the page.
<!-- 110.13     08-Feb-1999     fychu    790884   Increase stdiw variable to 
<!--                                              650.
<!-- 110.14     11-Feb-1999     lbandaru 788004   Commented eval() call when 
<!--                                              display = JSACTION in 
<!--                                              ClickEvent function becos
<!--                                              JavaScript throws an error
<!--                                              in IE when it tries to
<!--                                              execute '#' returned by eval.
<!-- 110.15     12-Feb-1999     mstewart 821775   Uncommented eval() call!!
<!--                                              Added if condition so that
<!--                                              the eval call is not made if
<!--                                              the action is '#'.
<!-- 110.16     12-Feb-1999     mstewart 821775   Fixed the previous fix.
<!-- 110.17     17-Feb-1999     fychu    825092   Added a new function called
<!--                                              Check_Cmptnce_Eval_Fields
<!--                                              for use by Update Competence
<!--                                              Evaluation form.
<!-- 110.18     08-Mar-1999     fychu             1)In ClickEvent function,
<!--                                                replaced the hard-coded
<!--                                                message "unknown option" 
<!--                                                with FND message.
<!--                                              2)Removed the windowOpener 
<!--                                                function since it is not
<!--                                                referenced in any js file or
<!--                                                packages.  This function 
<!--                                                contains hard-coded text.
<!--                                                
<!-- 110.19     12-Apr-1999     fychu   861622    1)In ModifyParticipant      
<!--                                                function, added code to
<!--                                                handle 'SUBMITAPPROVAL'type.
<!--                                                
<!-- 110.20     06-May-1999     fychu             1)Removed ShowTipError func
<!--                                                which is obsolete and 
<!--                                                contained hard-coded error
<!--                                                messages.
<!--                                              2)Removed "modal display" 
<!--                                                hard-coded error message 
<!--                                                from ClickEvent function.
<!--                                                
<!-- 115.2      10-Apr-2000     fychu   1261956   1)Added a new function called
<!--                                                clear_button_frame.
<!--                                              2)In SubmitProcess function,
<!--                                                added a call to
<!--                                                clear_button_frame after
<!--                                                submit.
<!--                                              3)In ClickEvent function,
<!--                                                added a call to
<!--                                                clear_button_frame if
<!--                                                AllowReplace is true.
<!--
<!-----------------------------------------------------------------------------
<!-- --------------------------------------------------------------------------
<!-- globals
<!-- --------------------------------------------------------------------------
<!-- min screen size
<!-- --------------------------------------------------------------------------
  var w = 800
  var h = 600
<!-- --------------------------------------------------------------------------
<!-- automatically get user sreen size if possible
<!-- --------------------------------------------------------------------------
  if (navigator.userAgent.indexOf("MSIE")==-1){
    if(navigator.javaEnabled()) {
     var tools=java.awt.Toolkit.getDefaultToolkit();
     var size=tools.getScreenSize();
     w =size.width;
     h=size.height;
    }
  }
<!-- --------------------------------------------------------------------------
<!-- standard message window
<!-- --------------------------------------------------------------------------
  var stdw = 400
  var stdh = 200
<!-- --------------------------------------------------------------------------
<!-- standard error tip window
<!-- --------------------------------------------------------------------------
  var stdetw = 600
  var stdeth = 155
<!-- --------------------------------------------------------------------------
<!-- standard info window
<!-- --------------------------------------------------------------------------
  var stdiw = 650
  var stdih = 400
<!-- --------------------------------------------------------------------------
<!-- std colours
<!-- --------------------------------------------------------------------------
  var stdconfirmcolor = "#FFFFE5"
  var stdwarningcolor = "#AFAFE5"
<!-- --------------------------------------------------------------------------
<!-- std frames
<!-- --------------------------------------------------------------------------
  var theParent = window.parent
  var theWorkspace = window.parent.frames[2]
<!-- --------------------------------------------------------------------------
<!-- monitor permanent leave event
<!-- --------------------------------------------------------------------------
  var LeaveFunctionality = -1
<!-- --------------------------------------------------------------------------
<!-- FND_NEW_MESSAGES and AK_PROMPTS cross_reference
<!-- --------------------------------------------------------------------------
<!-- the arrays are populated by the toolkit code and should be referenced here
<!-- for information 
<!-- --------------------------------------------------------------------------
<!-- Messages
<!-- --------------------------------------------------------------------------
<!-- cross-referencing for fnd_new_message store in JSMsg javascript array
<!-- always reference the array in frame[0]
<!-- 0 - HR_99999_WAIT_FOR_FRAMES_WEB
<!-- 1 - HR_99999_CONFIRM_LEAVE_WEB
<!-- 2 - HR_99999_CONFIRM_DELETE_WEB
<!-- 3 - HR_99999_MANDATORY_FIELDS_WEB
<!-- 4 - HR_99999_CHANGE_APPRAISER
<!-- --------------------------------------------------------------------------
<!-- Prompts
<!-- --------------------------------------------------------------------------
<!-- cross-referencing for display text JSPrompts to create pop up windows
<!-- 0 - title Mandatory Field Error
<!-- 0 - title Fields too long
<!-- --------------------------------------------------------------------------
<!-- Std window formats
<!-- --------------------------------------------------------------------------
<!-- --------------------------------------------------------------------------
<!-- ------------------------ ModalWindowHeader -------------------------------
<!-- --------------------------------------------------------------------------
function ModalWindowHeader (ToWindow, WindowTitle, BGcolor) {
ToWindow.document.write ("<HEAD>" +
"<TITLE>" + WindowTitle + "</TITLE>" +
"</HEAD>" +
"<BODY BGCOLOR=\"#FFFFE5\" " +
"onBlur=\"self.focus(); opener.setwin()\" onUnload=\"opener.resetwin()\">")
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------ StdWindowFooter ---------------------------------
<!-- --------------------------------------------------------------------------
function StdWindowFooter (ToWindow) {
    ToWindow.document.write("</BODY></HTML>")
}
<!-- --------------------------------------------------------------------------
<!-- speial windows
<!-- --------------------------------------------------------------------------
<!-- --------------------------------------------------------------------------
<!-- ------------------------ StdWindowFooter ---------------------------------
<!-- --------------------------------------------------------------------------
function ConfirmWindowBody (ToWindow, MsgTxt) {
  ToWindow.document.write ("<CENTER>" +
"<TABLE WIDTH=\"50%\">" +
"<TR>" +
"<TD ALIGN=\"left\">" + MsgTxt + 
"</TD>" +
"</TR>" +
"</TABLE>" +
"</CENTER>")
}
<!-- --------------------------------------------------------------------------
<!-- ------------------------ WarningWindowBody -------------------------------
<!-- --------------------------------------------------------------------------
function WarningWindowBody (Heading, PlaceText, ToWindow) {
ToWindow.document.write ("<TABLE WIDTH=\"100%\">")
ToWindow.document.write ("<TR>")
ToWindow.document.write ("<TD ALIGN=\"center\">")
ToWindow.document.write (Heading)
ToWindow.document.write ("</TD>")
ToWindow.document.write ("</TR>")
ToWindow.document.write ("<TR>")
ToWindow.document.write ("<TD ALIGN=\"left\">")
ToWindow.document.write (PlaceText)
ToWindow.document.write ("</TD>")
ToWindow.document.write ("</TR>")
ToWindow.document.write ("</TABLE>")
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ ChkMandatoryFields --------------------------
<!-- ----------------------------------------------------------------------
function ChkMandatoryFields (ChkArray, frameindex) {
<!--
<!-- reset the instances counting element
<!--
  for (i=0; i < ChkArray.length; i++) {
    ChkArray [i][2] = 0
  }
<!--
<!-- loop through the mandatory field elements,  checking if values are set
<!-- check multiple (A) instances for an array or exit for simple (V) variable
<!--
  var theform  = window.parent.frames[frameindex].document.TransmitForm
  for (aindex=0; aindex < ChkArray.length; aindex++) {
    for (findex=0; findex < theform.length; findex++) {
      if (theform.elements[findex].name == ChkArray [aindex][0]) {
        var l_string = theform.elements[findex].type
<!--
<!-- check values for a select list
<!--
        if (l_string.indexOf("select") > -1) {
          var selindex = theform.elements[findex].options.selectedIndex
          if (theform.elements[findex].options[selindex].value == "") { 
            ChkArray [aindex][2] = ChkArray [aindex][2] + 1
          }
        }
<!--
<!-- check values for input field
<!--
        else {
          var ValidString = false
          var sindex = 0
          var theLength = 0
// ?? tried to set up a break on validString - this failed
// tried to set up the whil .. AND .. !validString - also failed
// 15 used as an arbitrary length  if too many chars the eats up CPU
          if (theform.elements[findex].value.length >=15) {
            theLength = 15
          }
          else {
            theLength = theform.elements[findex].value.length
          }
          while (sindex < theLength) {
            if ((escape(theform.elements[findex].value.charAt(sindex)) == '%0A')
            || (escape(theform.elements[findex].value.charAt(sindex)) == '%0D' )
            || (theform.elements[findex].value.charAt(sindex) == ' ')) {
              null
            }
            else {
              ValidString = true
            }
            sindex++
          }
          if (!ValidString) {
            theform.elements[findex].value = ""
          }
          if (theform.elements[findex].value == "") {
            ChkArray [aindex][2] = ChkArray [aindex][2] + 1
            if (ChkArray [aindex][1] == "V") {
             break
            }
          }
        }
      }
    }
  }
  return ChkArray
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ ChkTextAreas --------------------------------
<!-- ----------------------------------------------------------------------
function ChkTextAreas (theArray, theFrameIndex) {
  var theForm = window.parent.frames[theFrameIndex].document.TransmitForm
  for (tindex=0; tindex < theArray.length; tindex++) {
    for (findex=0; findex < theForm.length; findex++) {
      if (theForm.elements[findex].name == theArray [tindex][0]) {
        if (theForm.elements[findex].value.length > theArray [tindex][1]) {
          theArray [tindex][2] = 1  
        }
      }  
    }
  }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ textareaLength ------------------------------
<!-- ----------------------------------------------------------------------
/*
function textareaLength (frameindex) {
  var theForm = window.parent.frames[frameindex].document.TransmitForm
  var TextAreas = window.parent.frames[frameindex].TALengths
  ChkTextAreas (TextAreas, frameindex)
  for (i=0;i<theForm.length;i++) {
    if (theForm.elements[i].type == "textarea") {
      alert (theForm.elements[i].toString())
    }
  }
}
*/
<!-- ----------------------------------------------------------------------
<!-- --------------------- Check_Cmptnce_Eval_Fields------------------------
<!-- ----------------------------------------------------------------------
<!-- Check the length of the form to prevent users from just creating 
<!-- Competence Type without assigning competencies to each type.  Thus, there
<!-- will be no enterable fields on the form.
function Check_Cmptnce_Eval_Fields(frameindex) 
   {
     if (window.parent.frames[frameindex].document.TransmitForm.length <= 1)
        { alert (window.parent.frames[2].JSMsgs[12]) }
     else {SubmitProcess(frameindex)}
   }

<!-- ----------------------------------------------------------------------
<!-- ------------------------ SubmitProcess --------------------------
<!-- ----------------------------------------------------------------------
function SubmitProcess(frameindex) {
  var theTitle = window.parent.frames[2].JSPrompts[0]
  var StartText =  window.parent.frames[2].JSMsgs[3]
  var theTATitle = window.parent.frames[2].JSPrompts[1]
  var TAStartText = window.parent.frames[2].JSMsgs[7]
//  var ContainerTitle = window.parent.frames[2].JSPrompts[1]
  var l_report_string = ""
  var l_tareport_string = ""
  var doprocess = false
  var textarea_length_err = false
  if (window.parent.frames[frameindex].MandatoryFields.length > 0) {
    doprocess = true
  }
//                 
    var TextAreas = window.parent.frames[frameindex].TALengths
    ChkTextAreas (TextAreas, frameindex)
    for (i=0; i < TextAreas.length; i++) {
      if (TextAreas[i][2] != 0) {
        if (i==0) {
          l_tareport_string = ""
        }
        l_tareport_string = l_tareport_string 
		+ " - " + TextAreas[i][3] + " (" + TextAreas[i][1] + ")"
        l_tareport_string = l_tareport_string + "\n"
      }
    }
  if (l_tareport_string != 0) {
    l_tareport_string = theTATitle + "\n\n" + TAStartText + "\n\n" + l_tareport_string
    textarea_length_err = true
    alert (l_tareport_string)
  }

  if (doprocess && !textarea_length_err) {
    var MandatoryFields = window.parent.frames[frameindex].MandatoryFields
    ChkMandatoryFields (MandatoryFields, frameindex)
    for (i=0; i < MandatoryFields.length; i++) {
      if (MandatoryFields[i][2] != 0) {
        if (i==0) {
          l_report_string = ""
        }
        l_report_string = l_report_string 
		+ " - " + MandatoryFields[i][3] 
        if (MandatoryFields[i][1] == "A") {
          l_report_string = l_report_string 
		+ "[" + MandatoryFields[i][2] + " instances]"
        }
        l_report_string = l_report_string + "\n"
      }
    }
<!--l_report_string = ""
    if (l_report_string.length != 0) 
      {
  var theTitle = window.parent.frames[2].JSPrompts[0]
      l_report_string = l_report_string + "\n"
      l_report_string = theTitle + "\n\n" + StartText + "\n\n" +l_report_string
//    WarnWindowOpener(theTitle, l_report_string, ContainerTitle) 
      alert (l_report_string)
       }
    else window.parent.frames[frameindex].document.TransmitForm.submit()
    }
  else 
    {if (!textarea_length_err)
        { window.parent.frames[frameindex].document.TransmitForm.submit() }
    }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ AllowReplace --------------------------------
<!-- ----------------------------------------------------------------------
<!-- when the pop up window is displayed the calling code continues; the 
<!-- close event on the window has to have something to call
<!-- nb href needs to be processed as travel through the window appears
<!-- to lead to loss of scope (i.e. just trying replace(event) does not work
<!-- javascript does not know the path
<!--
function AllowReplace(event) {
  chkstr=location.href
  pos = chkstr.lastIndexOf("/")
  window.parent.location.replace(chkstr.substring(0, pos + 1) + event)
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ ClickEvent ----------------------------------
<!-- ----------------------------------------------------------------------
function ClickEvent 
  ( display
  , event
  , frameno
  , field1
  , value1
  , field2
  , value2
  , field3
  , value3
  ) {
//  if (theParent.FrameLength 
//     > theParent.FrameCount) {
//     var theWarnTitle = window.parent.frames[2].JSPrompts[4]
//     var theWarnContainerTitle = window.parent.frames[2].JSPrompts[1]
//     msgstr = theWorksapce.JSMsgs[0] + theParent.FrameLength + ' ' + theParent.FrameCount
//     theParent.FrameCount = theParent.FrameLength 
//     WarnWindowOpener(theWarnTitle, msgstr, theWarnContainerTitle) 
//  }
//  else 
//   {
  var DoReplace = false
  if (parent.frames.length == 0) {
    var Msg = parent.JSMsgs[1]
    var theTitle = ''
// parent.JSPrompts[0]
  }
  else {
    var theTitle = ''
//window.parent.frames[2].JSPrompts[0]
  }
<!-- added 11/7 to monitor change event
  var basestr = 'window.frames[' + frameno + '].document.TransmitForm.'
  var evalstr = ''
  if (!field1 == null) {
    evalstr = basestr + field1 + '.value=' + value1
    eval (evalstr)
  }
  if (!field2 == null) {
    evalstr = basestr + field2 + '.value=' + value2
    eval (evalstr)
  }
  if (!field3 == null) {
    evalstr = basestr + field3 + '.value=' + value3
    eval (evalstr)
  }
  if (display == "MODAL") {
    alert (display + event)
  }
  else if (display == "NONMODALINFO") {
    URLwindowOpener(event, display)
  }
  else if (display == "NONMODALET") {
    URLwindowOpener(event, display)
  }
  else if (display == "REPLACE") {
    if (PermanentLeaveOK ()) {
      DoReplace = true
    }
    else {
      PermLeaveMsg = window.parent.frames[2].JSMsgs[1]
      if (confirm (PermLeaveMsg)) {
        DoReplace = true
        window.parent.LeaveFunctionality = 1
      }
      else window.parent.LeaveFunctionality = -1
    }
    if (DoReplace) {
      if ((HasChangeOccurred ()) && (window.parent.LeaveFunctionality = -1)) {
      var Msg = window.parent.frames[2].JSMsgs[6]
        if (confirm (Msg)) {
          AllowReplace(event)
//        ConfwindowOpener (Msg, event, theTitle)
        }
      }
      else {
        AllowReplace (event)
      }
    }
  }
  else if (display == "FRAME") {
    window.frames[frameno].location.replace(event)
  }
  else if (display == "JSACTION") {
    if (event != '#')
       eval(event)
    return
  }
  else {
    var Msg = window.parent.frames[2].JSMsgs[13]
    alert (Msg + event)
  }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ setField ------------------------------------
<!-- ----------------------------------------------------------------------
function setField (fieldname, value) {
  theform = "window.parent.frames[2].document.TransmitForm."
  eval (theform + fieldname + ".value='" + value + "'")
}
<!-- ----------------------------------------------------------------------
<!-- monitoring onchange event
<!-- ----------------------------------------------------------------------
<!-- note : changes occur in the workspace_frame
<!-- the library is attached to all frames so the button_bar_frame
<!-- must reference the workspace_frame
<!-- use frame index
<!-- ----------------------------------------------------------------------
<!-- variables
<!-- ----------------------------------------------------------------------
  ChangeOccurred = -1
  DetectChangeRequired = -1
<!-- ----------------------------------------------------------------------
<!-- ------------------------ SetChangeRequired ---------------------------
<!-- ----------------------------------------------------------------------
function SetChangeRequired () {
 DetectChangeRequired = 1
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ FlagChangeOccurred --------------------------
<!-- ----------------------------------------------------------------------
function FlagChangeOccurred () {
  ChangeOccurred = 1
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ PermanentLeaveOK  ---------------------------
<!-- ----------------------------------------------------------------------
function PermanentLeaveOK () {
  var returnval = false
  if (theParent.LeaveFunctionality == -1) {
    returnval = true
  }
return returnval
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ HasChangeOccurred ---------------------------
<!-- ----------------------------------------------------------------------
function HasChangeOccurred () {
  var returnval = false
  if ((theParent.DetectChangeRequired == 1)
  && (theParent.ChangeOccurred == 1)) {
    returnval = true
  }
return returnval
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ NoChangeOccurred ----------------------------
<!-- ----------------------------------------------------------------------
function NoChangeOccurred () {
  var returnval = false
  if ((theParent.DetectChangeRequired == 1)
  && (theParent.ChangeOccurred == -1)) {
    returnval = true
  }
return returnval
}
<!-- ----------------------------------------------------------------------
<!-- monitoring on click events if not all frames loaded - ClickEvent
<!-- ----------------------------------------------------------------------
<!-- variables
<!-- ----------------------------------------------------------------------
  FrameLength = theParent.frames.length
  FrameCount = 0
<!-- ----------------------------------------------------------------------
<!-- modal windows
<!-- ----------------------------------------------------------------------
<!-- variables
<!-- ----------------------------------------------------------------------
  var winopen = 0
  window.onerror=null
<!-- ----------------------------------------------------------------------
<!-- ------------------------ setwin --------------------------------------
<!-- ----------------------------------------------------------------------
function setwin() {
  winopen = 1
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ boolwin -------------------------------------
<!-- ----------------------------------------------------------------------
function boolwin (){
  if (winopen== 1) {
  return false
  }
  else {
    return true
  }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ resetwin ------------------------------------
<!-- ----------------------------------------------------------------------
function resetwin() {
winopen = -1
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ MsgWinOpen ----------------------------------
<!-- ----------------------------------------------------------------------
function MsgWinOpen() {
  if (!boolwin ()) {
    return true
  }
  else return false
}
<!-- ----------------------------------------------------------------------
<!-- ---------------------  clickevent  -----------------------------------
<!-- ----------------------------------------------------------------------
function clickevent (event) {
  var retval = true
  if (boolwin ()) {
    if (event.length != 0) {
      eval(event)
    }
  }
  else {
    retval = false
  }
  return retval
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------  --------------------------------
<!-- ----------------------------------------------------------------------
<!-- modify this to the information window
function URLwindowOpener(URL, theType) {
  if(theType == "NONMODALET") {
    var winwidth = stdetw
    var winheight = stdeth
  }
  else {
    var winwidth = stdiw
    var winheight = stdih
  }
  var windefw = w
  var wintop = (h / 2) - (0.5 * winheight)
  var winleft = (w / 2) - (0.5 * winwidth )

    urlWindow=window.open
       ( URL
       ,"URLWindow"
       ,"menubar=no,dependent=yes,scrollbars=no,status=no,width="
        + winwidth + ",height=" + winheight + ",top=" + wintop 
        + ",left=" + winleft
      )
    urlWindow.focus()
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ ConfwindowOpener ----------------------------
<!-- ----------------------------------------------------------------------
<!-- generates a confirmation modal window
<!-- ----------------------------------------------------------------------
function ConfwindowOpener(TextMessage, event, theTitle) {
  var winwidth = stdw
  var winheight = stdh
  var windefw = w
  var wintop = (h / 2) - (0.5 * winheight)
  var winleft = (w / 2) - (0.5 * winwidth )
  var theColor = stdconfirmcolor
<!--
  if (!MsgWinOpen()) {
    setwin()
    msgWindow=window.open
      ( ""
      ,"ConfWindow","menubar=no,dependent=yes,scrollbars=no,status=no,width="
        + winwidth + ",height=" + winheight + ",top=" + wintop 
        + ",left=" + winleft
      )
    ModalWindowHeader (msgWindow, theTitle, theColor)
    ConfirmWindowBody (msgWindow, TextMessage)
<!-- cannot put form tag into a procedure for some reason must call form from here
    msgWindow.document.write("<FORM>")
    msgWindow.document.write("<TABLE WIDTH=\"100%\">")
    msgWindow.document.write("<TR>")
    msgWindow.document.write("<TD WIDTH=\"50%\" ALIGN=\"center\">")
    if (event == '') {
    msgWindow.document.write("<INPUT type=\"button\" value=\"true\" " +
      "onClick=\"self.close();opener.parent.frames[2].SubmitProcess(2)\">")
    }
    else {
    msgWindow.document.write("<INPUT type=\"button\" value=\"true\" " +
      "onClick=\"self.close();opener.parent.frames[2].AllowReplace(\'" +
      event + "\')\">")
    }

    msgWindow.document.write("</TD>")
    msgWindow.document.write("<TD WIDTH=\"50%\" ALIGN=\"center\">")
    msgWindow.document.write("<INPUT type=\"button\" value=\"false\" " +
      "onClick=\"self.close()\">")
    msgWindow.document.write("</TD>")
    msgWindow.document.write("</TR>")
    msgWindow.document.write("</TABLE>")
    msgWindow.document.write("</FORM>")
    StdWindowFooter (msgWindow)
  }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ WarnWindowOpener ----------------------------
<!-- ----------------------------------------------------------------------
function WarnWindowOpener(theHeader, theText, theTitle) {
  var winwidth = stdw
  var winheight = stdh
  var windefw = w
  var wintop = (h / 2) - (0.5 * winheight)
  var winleft = (w / 2) - (0.5 * winwidth )
  var theColor = stdwarningcolor
<!--
  if (!MsgWinOpen()) {
    setwin()
    msgWindow=window.open
      (""
      ,"WarnWindow"
      ,"menubar=no,dependent=yes,scrollbars=no,status=no,width=" +
        winwidth + ",height=" + winheight + ",top=" + 
        wintop + ",left=" + winleft)
    ModalWindowHeader (msgWindow, theTitle, theColor)
    WarningWindowBody (theHeader, theText, msgWindow)
<!-- cannot put form tag into a procedure for some reason must call form from here
    msgWindow.document.write("<FORM>")
    msgWindow.document.write("<TABLE WIDTH=\"100%\">")
    msgWindow.document.write("<TR>")
    msgWindow.document.write("<TD WIDTH=\"100%\" ALIGN=\"center\">")
    msgWindow.document.write("<INPUT type=\"button\" value=\"false\" " +
      "onClick=\"self.close()\">")
    msgWindow.document.write("</TD>")
    msgWindow.document.write("</TR>")
    msgWindow.document.write("</TABLE>")
    msgWindow.document.write("</FORM>")
    StdWindowFooter (msgWindow)
  }
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ SetTipOffImage ------------------------------
<!-- ----------------------------------------------------------------------
function SetTipOffImage (ImageName, ImageIndex) {
theDoc = window.parent.frames[2].document
var CountOccurrence = 0
var theIndex = -1
for (i=0;i<theDoc.images.length;i++){
  if(theDoc.images[i].name == ImageName) {
    theDoc.images[i].src = window.parent.frames[2].TipOn.src
    CountOccurrence = CountOccurrence + 1
  }
  if (CountOccurrence == ImageIndex) {
    theDoc.images[i].src = window.parent.frames[2].TipOff.src
    theIndex = i
  }
}
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ SetTipOnImage -------------------------------
<!-- ----------------------------------------------------------------------
function SetTipOnImage (ImageName) {
theDoc = window.parent.frames[2].document
for (i=0;i<theDoc.images.length;i++){
  if(theDoc.images[i].name == ImageName) {
    theDoc.images[i].src = window.parent.frames[2].TipOn.src
  }
}
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ LOV -----------------------------------------
<!-- ----------------------------------------------------------------------
<!-- from icx
function LOV
  ( c_attribute_app_id
  , c_attribute_code
  , c_region_app_id
  , c_region_code
  , c_form_name
  , c_frame_name
  , c_where_clause
  ,c_js_where_clause
  ) {
SetFULLNAME()
  lov_win = window.open("icx_util.LOV?c_attribute_app_id=" + 
     c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +
     "&c_region_app_id=" + c_region_app_id + "&c_region_code=" +
     c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +
     c_frame_name  + "&c_where_clause=" + c_where_clause +
     "&c_js_where_clause=" + c_js_where_clause,"LOV",
     "resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");
    lov_win.opener = self;
    lov_win.focus()
}
function SetPersonName() {
var theDisplayForm = window.parent.frames[2].document.DisplayForm
var theTransmitForm = window.parent.frames[2].document.TransmitForm
theDisplayForm.p_dummy.value = theDisplayForm.P_FULL_NAME.value
theTransmitForm.p_new_person_id.value = theDisplayForm.P_PICK_PERSON_ID.value
}
function SetFULLNAME() {
var theDisplayForm = window.parent.frames[2].document.DisplayForm
theDisplayForm.P_FULL_NAME.value = theDisplayForm.p_dummy.value 
}
function ChangeAppraiserConfirm(frameindex) {

  var theDisplayForm = window.parent.frames[frameindex].document.DisplayForm
  var theTransmitForm = window.parent.frames[frameindex].document.TransmitForm
  var ContainerTitle = ''
//window.parent.frames[2].JSPrompts[1]
  var CMsg = window.parent.frames[2].JSMsgs[4]
  var theCTitle = ''
//window.parent.frames[2].JSPrompts[0]
  var WMsg = window.parent.frames[2].JSMsgs[5]
  var theWTitle = ''
//window.parent.frames[2].JSPrompts[1]

  if (theTransmitForm.p_new_person_id.value == '') {
    WMsg = WMsg
    alert (WMsg)
//   WarnWindowOpener(theWTitle, WMsg, ContainerTitle) 
  }
  else {
    CMsg = theCTitle + "\n\n" + CMsg
      if (confirm (CMsg)) {
        SubmitProcess(2)
      }
//    ConfwindowOpener (CMsg, '', theCTitle)
  }
//alert("reached cac")
}
function ResetChangeAppraiser() {
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  var WMsg = window.parent.frames[2].JSMsgs[5]
  var theWTitle = ''
//window.parent.frames[2].JSPrompts[1]
  theTransmitForm.p_new_person_id.value == ''
  alert (theWTitle + "\n\n" + WMsg)
}
<!-- --------------------------------------------------------------------------
<!-- working with participants
<!-- --------------------------------------------------------------------------
var CountNotify = 0
<!-- --------------------------------------------------------------------------
<!-- SetNotify
<!-- --------------------------------------------------------------------------
function SetNotify (){
  var theCounter = 0
  var existspnotify = false
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  if ( DetectChangeRequired == 1) {
    FlagChangeOccurred()
  }
  for (i=0;i<theTransmitForm.length;i++) {
    if (theTransmitForm.elements[i].name == "p_notify") {
      existspnotify = true
      if (theTransmitForm.elements[i].checked) {
        theCounter = theCounter + 1
      }
    }
  CountNotify = theCounter
  }
  if ((CountNotify ==0) && (existspnotify)) {
    theTransmitForm.p_notify_msg.value = ''
  }
}
<!-- --------------------------------------------------------------------------
<!-- NewParticipant
<!-- --------------------------------------------------------------------------
function NewParticipant (frameIndex) {
  var theDisplayForm = window.parent.frames[2].document.DisplayForm
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  theTransmitForm.p_new_participant_id.value = theDisplayForm.P_PICK_PERSON_ID.value
  theTransmitForm.p_whatnext.value = "ADD"
//alert (theTransmitForm.p_whatnext.value)
  SubmitProcess(2)
}
<!-- --------------------------------------------------------------------------
<!-- ModifyParticipant
<!-- --------------------------------------------------------------------------
function ModifyParticipant (ParticipantId, ovn, theType) {
  var theUnchkMsg = window.parent.frames[2].JSMsgs[9]
  var theChkMsg = window.parent.frames[2].JSMsgs[8]
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  var DoProcess = false
  if (CountNotify != 0) {
    if (theType != 'NOTIFY') {
      alert (theUnchkMsg)
    }
    else {
      theTransmitForm.p_whatnext.value = theType
      DoProcess = true
    }
  }
  else {
    if((theType == 'LOCK') || (theType == 'DELETE')) {
      theTransmitForm.p_modify_participant_id.value = ParticipantId
      theTransmitForm.p_object_version_number.value = ovn
      theTransmitForm.p_whatnext.value = theType
      if (theType == 'DELETE') {
        DoProcess = false
        DeleteConfirm('PARTICIPANT')
      }
      else {
        DoProcess = true
      }
    }
    else  if (theType == 'LOCKALL') {
      theTransmitForm.p_whatnext.value = theType
      DoProcess = true
    }   
    else  if (theType == 'NOTIFY') {
      alert(theChkMsg)
    }   
    else  if (theType == 'APPRAISEE') {
// note this adds the value to the new participant field not
// the modify participant field
      theTransmitForm.p_new_participant_id.value = ParticipantId
      theTransmitForm.p_whatnext.value = theType
      DoProcess = true
    }    
    else  if (theType == 'SUBMITAPPROVAL') {
       // This is an Employee 360 Self Appraisal which requires approval of 
      // appraiser selection.
      theTransmitForm.p_whatnext.value = theType
      DoProcess = true
    }
  }
  if (DoProcess) {
    SubmitProcess(2)
  }
}
<!-- --------------------------------------------------------------------------
<!-- DeleteConfirm
<!-- --------------------------------------------------------------------------
function DeleteConfirm (thetype) {
  var Msg = ""
  if (thetype == 'OBJECTIVE' ) {
    Msg = window.parent.frames[2].JSMsgs[2]
  }
  else {
<!-- participant
    Msg = window.parent.frames[2].JSMsgs[11]
  }

  if (confirm (Msg)) {
    SubmitProcess(2)
  }
}
<!-- --------------------------------------------------------------------------
<!-- setvalue
<!-- --------------------------------------------------------------------------
function setvalue () {
s= "window.parent.frames[2].document.TransmitForm." 
   + window.parent.frames[2].FieldToSet 
   + ".value='" + window.parent.frames[2].ValueToCopy + "'"
eval(s)
}
<!-- --------------------------------------------------------------------------
<!-- setFocus
<!-- --------------------------------------------------------------------------
function setFocus(fieldname) {
  window.parent.frames[2].FieldToSet = fieldname
}
<!-- --------------------------------------------------------------------------
<!-- Questionnaire code
<!-- --------------------------------------------------------------------------
var CountPublish = 0
<!-- --------------------------------------------------------------------------
<!-- SetPublish
<!-- --------------------------------------------------------------------------
function SetPublish (){
  var theCounter = 0
  var existsppublish = false
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  for (i=0;i<theTransmitForm.length;i++) {
    if (theTransmitForm.elements[i].name == "p_publish") {
      existsppublish = true
      if (theTransmitForm.elements[i].checked) {
        theCounter = theCounter + 1
      }
    }
  CountPublish = theCounter
  }
}
<!-- --------------------------------------------------------------------------
<!-- PublishQuestionnaire
<!-- --------------------------------------------------------------------------
function PublishQuestionnaire () {
  var theChkMsg = window.parent.frames[2].JSMsgs[10]
  var theTransmitForm = window.parent.frames[2].document.TransmitForm
  if (CountPublish == 0) {
    alert(theChkMsg)
  }
  else {
    SubmitProcess(2)  
  }
}

<!-- done hiding from old browsers -->
