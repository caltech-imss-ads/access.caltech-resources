<html xsl:version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- $Header: okcolass.xsl 115.1 2000/07/27 16:29:08 pkm ship       $-->
  <head>
    <title>ARTICLES INFO</title>
  </head>
     <body text="#000000" bgcolor="#FFFFFF" link="#0000EE" vlink="#551A8B" alink="#FF0000">
	<center>
	  <b><font face="Times New Roman,Times"><font color="#000099">
	     <font size="+3">ARTICLES INFO</font></font></font>
	  </b>
	</center>
    <xsl:for-each select="Articles/Text">
       <p><b><font face="Arial,Helvetica"><font size="-1"><xsl:value-of select="LABEL"/></font></font></b>
       <b><font face="Arial,Helvetica"><font size="-1"><xsl:value-of select="HEADING"/></font></font></b></p>
       <p><b><font face="Arial,Helvetica"><font size="-1"><xsl:value-of select="CONTENT_LABEL"/></font></font></b></p>
       <font face="Arial,Helvetica"><font size="-1"><xsl:value-of select="TEXT"/></font></font>
    </xsl:for-each>
  </body>
</html>

