<!-- hide the script's contents from feeble browsers
<!-- $Header: pyppmwm.js 115.3 2001/11/01 08:47:07 pkm ship       $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date         Name          Bug #    Reason
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 110.0   03-Mar-2000  arundell               Created.
<!-- 115.0   20-Apr-2000  dscully		 put into 115 $PAY_TOP
<!-- 115.1   15-Jun-2000  arashid       1314290  Pass a message parameter to
<!--                                             PPMSelectChange().
<!-- 115.2   25-Oct-2001  arashid       2002963  Added LOV function for flex
<!--                                             LOV handling.
<!-- 115.3   31-Oct-2001  arashid       2002963  Modified LOV code for new
<!--                                             view definitions. Also 
<!--                                             escape where-clause string so
<!--                                             that it's URL-safe.
<!-----------------------------------------------------------------------------
<!-----------------------------------------------------------------------------
<!-- Javascript file for Personal Payment Types Module
<!--  
<!-----------------------------------------------------------------------------
top.PRINT_FLAG = 'Y';
// The following function is invoked from the
// header frame when the user clicks on the 
// 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.workflowForm.p_result_code.value = 'MAIN_MENU';  
  if (document.workflowForm.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
       top.clear_container_bottom_frame();
       document.workflowForm.target = '_top'; 
       document.workflowForm.submit();
     }
  }
  else {
       top.clear_container_bottom_frame();
       document.workflowForm.target = '_top';
       document.workflowForm.submit();
  }
}
//when user click cancel button
function cancel_button_onClick (msg) {
  if (document.workflowForm.p_form_changed.value=="Y") {
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'CANCEL';
        document.workflowForm.submit();
     }
  } else {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'CANCEL';
        document.workflowForm.submit();
  }
}
//when user click cancel button
function back_button_onClick (msg,newtarget) {
  if (document.workflowForm.p_form_changed.value=="Y") {
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'PREVIOUS';
        document.workflowForm.target = newtarget;
        document.workflowForm.submit();
     }
  } else {
        top.clear_container_bottom_frame();
        document.workflowForm.p_result_code.value = 'PREVIOUS';
        document.workflowForm.target = newtarget;
        document.workflowForm.submit();
  }
}


// Reset page button
function ResetPPMSummary(j) {
 document.reset_ppm_summary.j.value = j;
 top.clear_container_bottom_frame();
 document.reset_ppm_summary.submit();
}

// Change assignment button
function ChangeAssignment(j) {
 document.change_assignment.j.value = j;
 top.clear_container_bottom_frame();
 document.change_assignment.submit();
}

// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Next' button.
function next_button_onClick () {
  top.clear_container_bottom_frame();
  document.workflowForm.p_result_code.value = 'NEXT';
  document.workflowForm.submit();
  }
//
// This is the cancel ppm form function
//
function CancelPPMForm() {
  top.clear_container_bottom_frame();
  document.Cancel_PPM_Form.submit();
  }
//
// This is the reset page PPM form function
//
function ResetPPMForm() {
  top.clear_container_bottom_frame();
  document.Reset_PPM_Form.submit();
  }
//
// This is the submit PPM form
// function, called when the 
// user clicks 'OK' on the page
//
//function SubmitPPMForm(){
//   alert('Called Submit PPM form:')
//}
//
// This function is called when the user
// changes the PPM select list.
// It redefines the refresh form, and
// submits it, to keep the currency
// field in sync with with the OPM.
//
function PPMSelectChange(msg){
 var i = document.ppm_form.P_ORG_PAYMENT_METHOD_ID.selectedIndex;
 var val = document.ppm_form.P_ORG_PAYMENT_METHOD_ID.options[i].value;
 document.Refresh_PPM_Form.p_org_payment_method_id.value = val;
 alert(msg);
 top.clear_container_bottom_frame();
 document.Refresh_PPM_Form.submit();
}
//
// This is the change assignment cancel button
//
function ChangeAsgCancel(){
 top.clear_container_bottom_frame();
 document.AsgListChild.p_swap_assignment.value = 'NO';
 document.AsgListChild.submit();
}
//
// This is the change assignment ok button
//
function ChangeAsgOK(){
  top.clear_container_bottom_frame();
  document.AsgListChild.p_swap_assignment.value = 'YES';
  document.AsgListChild.submit();
}
<!-- ----------------------------------------------------------------------
<!-- ------------------------ LOV -----------------------------------------
<!-- ----------------------------------------------------------------------
<!-- The following is modified ICX LOV function so that we can use it w/ layers
<!-- depending on browser version.
<!-- Modified ICX LOV code
function LOV
  ( c_attribute_app_id
  , c_attribute_code
  , c_region_app_id
  , c_region_code
  , c_form_name
  , c_frame_name
  , c_where_clause
  ,c_js_where_clause
  ) {

 //
 // Need a WHERE-clause for JP bank branches to restrict by bank code.
 // It would be better to some link between the Bank Branch and Bank LOVs
 // because the WHERE clause makes the underlying SQL different for each Bank.
 //
 var l_js_where_clause = c_js_where_clause;
 if (c_region_code == "PAY_JP_BANK_BRANCH_LOV_PAGE" ||
     c_region_code == "PAY_JP_BANK_LOV_PAGE")
 {
   if (l_js_where_clause != null && l_js_where_clause != "")
     l_js_where_clause = l_js_where_clause + " AND ";
   else
     l_js_where_clause = "";
   //
   // Restrict JP Bank Branch by Bank Code.
   //
   if (c_region_code == "PAY_JP_BANK_BRANCH_LOV_PAGE")
   {
     l_js_where_clause = l_js_where_clause + "BANK_CODE = ";
     if (document.ppm_form.P_SEGMENT1.value != null)
       l_js_where_clause = 
       l_js_where_clause + "'" + document.ppm_form.P_SEGMENT1.value + "'";
     else
       // This will ensure 0 rows are returned.
       l_js_where_clause = l_js_where_clause + "NULL";
     l_js_where_clause = l_js_where_clause + " AND ";
   }
   l_js_where_clause = l_js_where_clause + 
   "TRUNC(SYSDATE) BETWEEN START_DATE_ACTIVE AND END_DATE_ACTIVE";
   //
   // Make it URL-safe.
   //
   l_js_where_clause = escape(l_js_where_clause);
 }
 //
 // Standard LOV code.
 //
 lov_win = window.open("icx_util.LOV?c_attribute_app_id=" +
     c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +
     "&c_region_app_id=" + c_region_app_id + "&c_region_code=" +
     c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +
     c_frame_name + "&c_where_clause=" + c_where_clause +
     "&c_js_where_clause=" + l_js_where_clause,"LOV",
     "resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");
  lov_win.opener = self;
  lov_win.focus()
}
<!-- done hiding from old browsers -->
