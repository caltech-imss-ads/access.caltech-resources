/*=================================================================+
|               Copyright (c) 1999 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSICONS.js 115.0 99/10/14 16:20:11 porting shi $ */


/* These are initializations for icon gifs that are used
 * on the toolbar.  Each image has only 4 states:
 * - onMouseOver
 * - onMouseDown
 * - onMouseOut
 * - disabled
 */

cancel_over = new Image();
cancel_over.src = "/OA_MEDIA/FNDIWHO1.gif";
cancel_out = new Image();
cancel_out.src = "/OA_MEDIA/FNDIWHOM.gif";
cancel_down = new Image();
cancel_down.src = "/OA_MEDIA/FNDIWHO1.gif";
cancel_disable = new Image();
cancel_disable.src = "/OA_MEDIA/FNDIWHOD.gif";

save_over = new Image();
save_over.src = "/OA_MEDIA/FNDIWSA1.gif";
save_out = new Image();
save_out.src = "/OA_MEDIA/FNDIWSAV.gif";
save_down = new Image();
save_down.src = "/OA_MEDIA/FNDIWSA1.gif";
save_disable = new Image();
save_disable.src = "/OA_MEDIA/FNDIWSAD.gif";

print_over = new Image();
print_over.src = "/OA_MEDIA/FNDIWPR1.gif";
print_out = new Image();
print_out.src = "/OA_MEDIA/FNDIWPRT.gif";
print_down = new Image();
print_down.src = "/OA_MEDIA/FNDIWPR1.gif";
print_disable = new Image();
print_disable.src = "/OA_MEDIA/FNDIWPRD.gif";

reload_over = new Image();
reload_over.src = "/OA_MEDIA/FNDIWRL1.gif";
reload_out = new Image();
reload_out.src = "/OA_MEDIA/FNDIWRLD.gif";
reload_down = new Image();
reload_down.src = "/OA_MEDIA/FNDIWRL1.gif";
reload_disable = new Image();
reload_disable.src = "/OA_MEDIA/FNDIWRDD.gif";

stop_over = new Image();
stop_over.src = "/OA_MEDIA/FNDIWST1.gif";
stop_out = new Image();
stop_out.src = "/OA_MEDIA/FNDIWSTP.gif";
stop_down = new Image();
stop_down.src = "/OA_MEDIA/FNDIWSP1.gif";
stop_disable = new Image();
stop_disable.src = "/OA_MEDIA/FNDIWSTD.gif";

prefs_over = new Image();
prefs_over.src = "/OA_MEDIA/FNDIWPP1.gif";
prefs_out = new Image();
prefs_out.src = "/OA_MEDIA/FNDIWPPR.gif";
prefs_down = new Image();
prefs_down.src = "/OA_MEDIA/FNDIWPP1.gif";
prefs_disable = new Image();
prefs_disable.src = "/OA_MEDIA/FNDIWPPD.gif";

help_over = new Image();
help_over.src = "/OA_MEDIA/FNDIWHL1.gif";
help_out = new Image();
help_out.src = "/OA_MEDIA/FNDIWHLP.gif";
help_down = new Image();
help_down.src = "/OA_MEDIA/FNDIWHP1.gif";
help_disable = new Image();
help_disable.src = "/OA_MEDIA/FNDIWHLD.gif";
