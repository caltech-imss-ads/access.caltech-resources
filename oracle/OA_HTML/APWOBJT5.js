//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT5.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |              				     |     
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJT1.js is split into 2 files |
//  |              APWOBJT1.js, APWOBJT5.js          |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT5.js 115.0 2000/11/09 13:53:04 pkm ship        $ */

/*===========================================================================*
 *  LookupType Class - Container of Lookup Code displayed text               *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR LookupType:                  *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Lookup Type object  *
  *------------------------------------------*/ 
  function LookupType() {
    this.arrLookupCodes = new Object();
  }

 /*-----------------------------------------------------------------------*
  * METHOD mobjAddLookupCode:                                             *
  *   Params:  p_strLookupCode     -  Lookup Code (string)                *
  *            p_strDisplayedVal   -  Displayed Value of Lookup (string)  *
  *  Returns:  n/a                                                        *
  *     Desc:  Adds the lookup code to the Lookup type object and         *
  *-----------------------------------------------------------------------*/
  function mobjAddLookupCode(p_strLookupCode, 
                             p_strDisplayedVal) 
  {
    this.arrLookupCodes[p_strLookupCode] = p_strDisplayedVal; 
  }

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetLookupCode:                                     *
  *   Params:  p_strLookupCode -  Lookup Code(string)                       *
  *  Returns:  Displayed Value or null                                      *
  *     Desc:  Returns the displayed value of lookup code p_strLookupCode.  *
  *            If the lookup code does not exist null is returned           *
  *-------------------------------------------------------------------------*/
  function mobjGetLookupCode(p_strLookupCode)
  {
    var undefined;

    if (this.arrLookupCodes[p_strLookupCode] == undefined)
      return null;
    return this.arrLookupCodes[p_strLookupCode];

  }

  LookupType.prototype.mobjAddLookupCode    = mobjAddLookupCode;
  LookupType.prototype.mobjGetLookupCode    = mobjGetLookupCode;


/*===========================================================================*
 *  countryInfo Class                                                        *
 *===========================================================================*/

  function countryInfo(code, name)
  {   
    this.code = code;
    this.name = name;
  }

  function CY_getCode()
  {
    return this.code;
  }

  function CY_getName()
  { 
    return this.name;
  }

  countryInfo.prototype.getCode = CY_getCode;
  countryInfo.prototype.getName = CY_getName;


/*===========================================================================*
 *  creditCardTransaction Class                                              *
 *===========================================================================*/

  function creditCardTransaction(id, 
                                 date, 
                                 folioType, 
                                 merchant, 
                                 city, 
                                 amt, 
                                 trxCurrency, 
                                 trxAmt, 
                                 category, 
                                 refNum)
  {
    this.id               = id;
    this.date             = date;
    this.merchant         = merchant;
    this.city             = city;
    this.amt              = amt;
    this.folioType        = folioType;  
    this.trxCurrency      = trxCurrency;
    this.trxAmt           = trxAmt;
    this.category         = category;
    this.refNum           = refNum;
    this.oldCategory      = category;
  }

  // new creditCardTransaction(null, null, null, null, null, null, null);  

  function TRX_getId()
  {  
    return this.id; 
  }

  function TRX_getDate()
  {  
    return this.date; 
  }

  function TRX_getFolioType()
  {  
    return this.folioType; 
  }

  function TRX_getMerchant()
  {  
    return this.merchant; 
  }

  function TRX_getCity()
  {  
    return this.city; 
  }

  function TRX_getAmount()
  {  
    return this.amt; 
  }

  function TRX_getTrxCurr()
  {  
    return this.trxCurrency; 
  }


  function TRX_getTrxAmt()
  {    
    return this.trxAmt; 
  }

  function TRX_getRefNum() 
  { 
    return this.refNum;
  }

  function TRX_getCategory()
  {    
    return this.category; 
  }

  function TRX_setCategory(category)
  {    
    this.category = category; 
  }

  function TRX_isCategoryChanged()
  { 
    return (this.category != this.oldCategory);
  }

  function TRX_copy(trx)
  {
    this.id           = trx.getId();
    this.date         = trx.getDate();
    this.merchant     = trx.getMerchant();
    this.city         = trx.getCity();
    this.amt          = trx.getAmt();
    this.folioType    = trx.getFolioType();
    this.trxCurrency  = trx.getTrxCurr();
    this.trxAmt       = trx.getTrxAmt();
    this.category     = trx.getCategory();
    this.refNum       = trx.getRefNum();
    this.oldCategory  = trx.oldCategory;
  }

  function disputedCharges()
  {
    this.paymentTrxn  = null;
    this.creditTrxn   = null;
  }

  creditCardTransaction.prototype.getId             = TRX_getId;
  creditCardTransaction.prototype.getDate           = TRX_getDate;
  creditCardTransaction.prototype.getFolioType      = TRX_getFolioType;
  creditCardTransaction.prototype.getMerchant       = TRX_getMerchant;
  creditCardTransaction.prototype.getCity           = TRX_getCity;
  creditCardTransaction.prototype.getAmt            = TRX_getAmount;
  creditCardTransaction.prototype.getTrxCurr        = TRX_getTrxCurr;
  creditCardTransaction.prototype.getTrxAmt         = TRX_getTrxAmt;
  creditCardTransaction.prototype.getRefNum         = TRX_getRefNum;
  creditCardTransaction.prototype.getCategory       = TRX_getCategory;
  creditCardTransaction.prototype.setCategory       = TRX_setCategory;
  creditCardTransaction.prototype.isCategoryChanged = TRX_isCategoryChanged;
  creditCardTransaction.prototype.copy              = TRX_copy;


/*===========================================================================*
 *  creditCardTransactions Class - Container Class of Credit Card Trxns      *
 *===========================================================================*/

  // constructor for creditCardTransactions object
  
  function creditCardTransactions()
  {
    this.next       = null;
    this.arr        = new Array();
    this.disputeArr = new Array();
  }

  // Create a prototype object
  //new creditCardTransactions();

  function CCT_add(id, 
                   date, 
                   expType, 
                   merchant, 
                   city, 
                   amt, 
                   trxCurrency, 
                   trxAmt, 
                   category)
  {
    if (arguments.length == 1) 
      this.arr[this.arr.length] = arguments[0];
    else
      this.arr[this.arr.length] = new creditCardTransaction(id, 
                                                            date, 
                                                            expType, 
                                                            merchant, 
                                                            city, 
                                                            amt, 
                                                            trxCurrency, 
                                                            trxAmt, 
                                                            category); 
    
    return;
  }

  function CCT_insert(id, 
                      date, 
                      folioType, 
                      merchant, 
                      city, 
                      amt, 
                      trxCurrency, 
                      trxAmt, 
                      category)
  {
    var date1 = fStringToDate(date, false);
    var date2;
    var found = false;
    var i = 0;

    while (i<this.arr.length && found == false)
    {
       date2 = fStringToDate(this.arr[i].getDate(), false);
       if (date1.getTime() < date2.getTime())
           found = true;
       else
           i++;
    }

    this.arr[this.arr.length] = new creditCardTransaction(null,
                                                          null,
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null); 
    var j = 0;

    for (j=this.arr.length-2; j >= i; j--)
      this.arr[j+1].copy(this.arr[j]);

    this.arr[i] = new creditCardTransaction(id, 
                                            date, 
                                            folioType, 
                                            merchant, 
                                            city, 
                                            amt, 
                                            trxCurrency, 
                                            trxAmt, 
                                            category);
 
    return ;
  }

  function CCT_getTrx(id)
  {
    for (var i=0; i<this.arr.length; i++)
    {
      if (this.arr[i].id == id) 
        return this.arr[i];
    }
    return null;
  }


  function CCT_getNumberOfTransactions(id)
  {
    return this.arr.length;
  }

  function CCT_reset()
  {
    if (this.arr.length > 0)
      this.next = 0;
    else
      this.next = null;
    return ;
  }

  function CCT_nextTrxn()
  {
    return ((this.next != null) && (this.next < this.arr.length) 
                                       ? this.arr[this.next++] : null);
  }

  // this method MUST be called when:
  // 1. after the transactions object is inititalized
  // 2. after some charges are disputed
  // 3. after some disputed charges are cleared
  function CCT_flagDisputedCreditCharges()
  {
     var creditArr = new Array();

     this.disputeArr.length = 0;  // reset it

     for (var i=0; i< this.arr.length; i++)
     {
       if (this.arr[i].getCategory() == g_cDispute) {
         this.disputeArr[this.disputeArr.length] = new disputedCharges();
         this.disputeArr[this.disputeArr.length-1].paymentTrxn = this.arr[i];
       }else{
         if (this.arr[i].getCategory() == g_cCredit)
           this.arr[i].setCategory("Business");

         if (this.arr[i].getTrxAmt() < 0)
           creditArr[creditArr.length] = this.arr[i];
       }
     }

    // Flag the credit charges for disputed ones
    if (this.disputeArr.length > 0) 
    {
      for (var i=0; i< creditArr.length; i++)
      {
        for (var j=0; j< this.disputeArr.length; j++)
        {    
          if (creditArr[i].getMerchant() == 
                              this.disputeArr[j].paymentTrxn.getMerchant() &&
              creditArr[i].getTrxAmt() == -
                              (this.disputeArr[j].paymentTrxn.getTrxAmt()) &&
              creditArr[i].getRefNum() == 
                               this.disputeArr[j].paymentTrxn.getRefNum())
          {
            creditArr[i].setCategory(g_cCredit);
            this.disputeArr[j].creditTrxn = creditArr[i];
          }
        }    //  close for (var j...
      }      //  close for (var i..
    }        //  close if (this....
  }          //  close function


  function CCT_getDisputedCharges() 
  { 
    return this.disputeArr; 
  }

  creditCardTransactions.prototype.add                        = CCT_add;
  creditCardTransactions.prototype.insert                     = CCT_insert;
  creditCardTransactions.prototype.getTrx                     = CCT_getTrx;
  creditCardTransactions.prototype.getNumberOfTransactions    = 
                                                 CCT_getNumberOfTransactions;
  creditCardTransactions.prototype.reset                      = CCT_reset;
  creditCardTransactions.prototype.nextTrxn                   = CCT_nextTrxn;
  creditCardTransactions.prototype.fFlagDisputedCreditCharges = 
                                                 CCT_flagDisputedCreditCharges
  creditCardTransactions.prototype.fGetDisputedCharges        = 
                                                 CCT_getDisputedCharges;

