/*===========================================================================+
 |      Copyright (c) 1998 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  FILENAME                                                                 |
 |      PORLOV.js                                                           | 
 |                                                                           |
 |  DESCRIPTION                                                              |
 |      Javascript rountines for LOV's                                       |
 |                                                                           |
 |  NOTES                                                                    |
 |     Must be included in every file that contains an LOV button            |
 |  DEPENDENCIES                                                             |
 |                                                                           |
 |  HISTORY                                                                  |
 |       23-NOV-98  marychu  created                                         |
 |                                                                           |
 +===========================================================================*/

/* $Header: PORLOV.js 115.1 99/08/05 17:57:12 porting ship $ */

function getOrgId(rForm)
{
  return ((rForm.POR_DELIVER_TO_ORG_ID && 
          rForm.POR_DELIVER_TO_ORG_ID.value != "") ? 
          "("+rForm.POR_DELIVER_TO_ORG_ID.value +")" :
     "(select nvl(hrl.inventory_organization_id,"+
                   "fsp.inventory_organization_id)"+
      " from hr_locations hrl," +
           "financials_system_parameters fsp " +
      "where hrl.location_code='" + rForm.POR_DELIVER_TO_LOCATION.value+
           "' and sysdate < nvl(hrl.inactive_date,sysdate+1))");
}

function getProjectId(rForm)
{
  return (rForm.POR_PROJECT_ID.value != "") ? rForm.POR_PROJECT_ID.value :
     "(select project_id from pa_projects_expend_v "+
       "where project_number='" + rForm.POR_PROJECT.value + "')";
}

function isInventory(rForm)
{
 return (rForm.POR_INV_REPL_REQUEST ? rForm.POR_INV_REPL_REQUEST.checked : 
   (rForm.POR_INV_REPLENISHMENT_FLAG ? rForm.POR_INV_REPLENISHMENT_FLAG.checked : false));
}
  
function callLOV(attr_id, attr_code, region_id, region_code,
                 form_name, frame_name, where_clause,
                 ind) 
{
  if(ind<0)
  {
    ind=top.getTop().findIndex(form_name, attr_code);
  }
  
  window.document.LOVReceiver= top.getTop().allForms[form_name][ind];
  var rForm= window.document.LOVReceiver;
  var js_where_clause= "";

  // set where clause
 
  if(attr_code=="POR_DELIVER_TO_SUBINVENTORY")
  {
//     if(top.getTop().isEmpty(rForm.POR_REQUESTER.value))
//     {
//       top.getActiveWindow().alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_ENTER_REQUESTER"]);
//       return;
//     }
    if(top.getTop().isEmpty(rForm.POR_DELIVER_TO_LOCATION.value))
    {
      top.getActiveWindow().alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_ENTER_LOCATION"]);
      return;
    }
    js_where_clause = "organization_id in " + getOrgId(rForm);
  }
  else if(attr_code=="POR_PROJECT" &&
          region_code!="POR_MASS_UPDATE_R")
    //&& region_code=="POR_LINE_DETAILS"
  {
    js_where_clause= 
      (isInventory(rForm) ?
       "INVENTORY_ORGANIZATION_ID="+ getOrgId(rForm) :
       "INVENTORY_ORGANIZATION_ID=-9999");
    //js_where_clause="";
    //       "");
  }
  else if (attr_code=="POR_TASK")
  {
    if(top.getTop().isEmpty(rForm.POR_PROJECT.value))
    {
      top.getActiveWindow().alert(top.getTop().FND_MESSAGES["ICX_POR_ALRT_ENTER_PROJECT"]);
      return;
    }
    js_where_clause = "project_id="+ getProjectId(rForm);

    // if this is an inventory replenishment request, we also need to
    // check the task level of the deliver to org.
    if(isInventory(rForm) && region_code!='POR_MASS_UPDATE_R')
    {
      js_where_clause += 
        "and enabled_task_level in "+
         "(select MP.PROJECT_CONTROL_LEVEL FROM MTL_PARAMETERS MP"+
         " WHERE MP.ORGANIZATION_ID=" + getOrgId(rForm) + ")";    
    }
  }
  else if (attr_code=="POR_DELIVER_TO_LOCATION" && 
           region_code!='POR_MASS_UPDATE_R' &&
           rForm.POR_ITEM_ID && 0<rForm.POR_ITEM_ID.value)
  {
    if(isInventory(rForm))
    {
      js_where_clause= 
        " deliver_to_org_id in (select msi1.organization_id "+
        " from mtl_system_items msi1 " +
        " where msi1.inventory_item_id = "+ rForm.POR_ITEM_ID.value+
        " and msi1.stock_enabled_flag = 'Y') ";

    }
    else // line is for expense
    {
      js_where_clause =" deliver_to_org_id in (select msi2.organization_id "+
                                            " from mtl_system_items msi2 " +
                                            " where msi2.inventory_item_id ="+
                                                    rForm.POR_ITEM_ID.value +")";
    }

    if(rForm.POR_ITEM_REVISION && !top.getTop().isEmpty(rForm.POR_ITEM_REVISION.value))
    {
      js_where_clause += 
        " organization_id in (select mir.organization_id "+
        " from mtl_item_revisions mir "+
        " where mir.revision = "+ rForm.POR_ITEM_REVISION.value+
        " and mir.inventory_item_id = " + rForm.POR_ITEM_ID.value;
    }
  }
  else 
  {
    js_where_clause = "";
  }

  // ideally would null out after user selects from LOV, but doing it
  // here for time 
  if(attr_code=="POR_DELIVER_TO_LOCATION")
  {
    if (rForm.POR_DELIVER_TO_SUBINVENTORY)
      rForm.POR_DELIVER_TO_SUBINVENTORY.value="";
  }
  else if(attr_code=="POR_PROJECT_ID")
  {
    if (rForm.POR_TASK)
    {
      rForm.POR_TASK= rForm.POR_TASK_ID="";
    }
  }

  //  if(document.debugLOV)
  //    top.getActiveWindow().alert(js_where_clause);
  js_where_clause= escape(js_where_clause,1);
  //  top.getActiveWindow().alert(js_where_clause);
  //alert("js_where_clause="+js_where_clause);
  
  rForm.updated=true;

  LOV(attr_id, attr_code, region_id, region_code,"LOVReceiver",
      frame_name , where_clause,js_where_clause);
}
function postRequesterLOV()
{
  window.document.LOVReceiver.POR_DELIVER_TO_LOCATION.value=
    window.document.LOVReceiver.POR_DELIVER_TO_LOCATION_2.value;
  window.document.LOVReceiver.POR_DELIVER_TO_LOCATION_ID.value=
    window.document.LOVReceiver.POR_DELIVER_TO_LOCATION_ID_2.value;
  window.document.LOVReceiver.POR_DELIVER_TO_ORG_ID.value=
    window.document.LOVReceiver.POR_DELIVER_TO_ORG_ID_2.value;
  if(window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY) 
    window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY.value="";
  if(window.document.LOVReceiver.POR_SUBINVENTORY) 
    window.document.LOVReceiver.POR_SUBINVENTORY.value="";
}

function postLocationLOV()
{
  if(window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY) 
    window.document.LOVReceiver.POR_DELIVER_TO_SUBINVENTORY.value="";
}

function addApprover(approverId, approverName)
{
  top.getTop().submitRequest("requisition",  // should be approver_list
                             "/" + top.getTop().JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
                             window.functionName, // actionName
                             window.functionName, // functionName
                             top.getTop().FIELD_DELIMITER+ "approverName"+
                             top.getTop().VALUE_DELIMITER+ approverName +
                             top.getTop().FIELD_DELIMITER+ "approverId"+
                             top.getTop().VALUE_DELIMITER+ approverId,
                             (window.functionName=="addApprover" ?
                              new Array("POR_APPROVERS_R") : null),
                             "approvers_hidden");
}


