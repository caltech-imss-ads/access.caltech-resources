/* $Header: czProxy.js 115.10 2001/07/25 08:28:05 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

var isLoaded = false;
var numFrames = 0;
var numFramesLoaded = 0;

var canPostConfigEvent = false;

function setTotalFrames (nFrames) {
  numFrames = nFrames;
}
	
function notifyFrameLoaded (frName) {
  numFramesLoaded ++;
  if ((numFrames > 0) && (numFrames == numFramesLoaded)) {
    if (parent.frames.czSrc.isLoaded) {
      parent.frames.czSrc.startApp ();
    }
    isLoaded = true;
  }
}

function doLayout () {
  //post show config event on to UiServlet thru the content frame
  if (parent.frames.czContent.isLoaded) {
    postShowConfigEvent ();
  } else
    canPostConfigEvent = true;
}
	
function postShowConfigEvent () {
  var sessionId = '';
  var xmlMsg = '';
  var servletURL = null;
  
  sessionId = document.form1.sessionId.value;
  servletURL = document.form1.action;
  //xmlMsg = 'show_config';
  xmlMsg += "<client-event session-id='" + sessionId + "'>";
  xmlMsg += "<show-config/><dhtml-cookie>"+ document.cookie +"</dhtml-cookie>";
  xmlMsg += "</client-event>";
  var doc = parent.frames.czContent.document;
  //set servlet URL
  if (servletURL)
    doc.form1.action = servletURL;
  doc.form1.sessionId.value = sessionId;
  doc.form1.XMLmsg.value = xmlMsg;
  doc.form1.submit ();
}
