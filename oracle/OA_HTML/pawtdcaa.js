//  +================================================+
//  |    Copyright(c) 2000 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtdcaa.js                       |
//  |                                                |
//  | Description: This is the standalone JavaScript |
//  |              file for SST Change Audit         |
//  |              functionality.                    |
//  |                                                |
//  | History: 5/25/00 Created by Kristian Widjaja   |
//  |                                                |
//  +================================================+
/* $Header: pawtdcaa.js 115.2 2001/04/19 18:17:50 pkm ship      $ */

var C_strFORMLATEAUDIT          = "formLateAudit"; 
var C_strFORMCHANGEAUDIT        = "formChangeAudit";

//  ----------------------------------------------------
//  Function: fDrawAuditBodyHeader
//  Description: Draws the Audit body header in the
//               frame specified by p_framTarget, using
//               the timecard specified by p_objTimecard
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fDrawAuditBodyHeader(p_framTarget, p_objTimecard)
{
  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_BODY");
  var l_objHeader = p_objTimecard.mGetobjHeader();
  var l_nPairs = 2;

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawstime.css">');
  p_framTarget.document.writeln('<BODY class=PANEL>');
  p_framTarget.document.writeln('<TABLE CELLPADDING=2 CELLSPACING=0 BORDER=0 WIDTH=100%>');

  // Employee, Week Ending 
  fDrawHeaderLine(p_framTarget,
        l_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_EMPLOYEE'),
        fFormatEmployee(l_objHeader.mGetobjEmployee()),
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_ENDING'),
        fDateToLongString(top.g_strSessDateFormat, l_objHeader.mGetdWeekEndingDate(),true));

  // Comment , Week Total
  fDrawHeaderLine(p_framTarget,
        l_nPairs,
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_COMMENT'),
        fFormatString(l_objHeader.mGetstrComment()),
        fGetRegionPrompt(l_objCurRegion, 'PA_WEB_WEEK_TOTAL'),
        fFormatHours(p_objTimecard.mGetnTotalHours()));

  // Approver and / or Business Message
  fDrawApprAndBMesg(p_framTarget, l_objHeader, l_nPairs, C_strAUDIT);
  
  p_framTarget.document.writeln('</TABLE><HR>');
  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}


//  ----------------------------------------------------
//  Function: fDrawAuditBodyContent
//  Description: Draws the Audit body content in the
//               frame specified by p_framTarget, using
//               the timecard specified by p_objTimecard
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fDrawAuditBodyContent(p_framTarget, p_objAuditHistory)
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_BODY");
  var l_bNeedsLateAudit = fbNeedsLateAudit (p_objAuditHistory);
  var l_bNeedsChangeAudit = fbNeedsChangeAudit (p_objAuditHistory);

  g_bNeedsLateAudit = l_bNeedsLateAudit;
  g_bNeedsChangeAudit = l_bNeedsChangeAudit;

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawstime.css">');
  p_framTarget.document.writeln('<BODY class=PANEL>');

  if (l_bNeedsLateAudit && g_strCurrentAction != C_strMODIFYDELETE) {
    // draw Late Audit entries
    p_framTarget.document.writeln('<DIV class=fielddata>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_LATE_AUDIT") + '<IMG SRC="' + g_strImagePath + 'FNDPX3.gif" HEIGHT=2 WIDTH="100%"></DIV>');
  
    // Write description
    p_framTarget.document.writeln('<DIV class=helptext>');
    p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_LATE_AUDIT_TEXT"));
    p_framTarget.document.writeln('<IMG SRC="' + g_strImagePath + 'FNDREQUI.gif" align=absmiddle>');
    p_framTarget.document.writeln('</DIV><BR>');

    fDrawAuditForm (p_framTarget, C_strFORMLATEAUDIT, g_objLookupTypes.mobjGetLookupType("PA_SST_LATE_ENTRY_REASON"), p_objAuditHistory);
  }
  if (l_bNeedsChangeAudit || g_strCurrentAction==C_strMODIFYDELETE) {
    // draw Change Audit entries
    p_framTarget.document.writeln('<DIV class=fielddata>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_CHANGE_AUDIT") + '<IMG SRC="' + g_strImagePath + 'FNDPX3.gif" HEIGHT=2 WIDTH="100%"></DIV>');

    // Write description
    p_framTarget.document.writeln('<DIV class=helptext>');

    // Check if this is a deletion
    if (g_strCurrentAction==C_strMODIFYDELETE)
      p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_AUDIT_DELETION"));
    else 
      p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_CHANGE_AUDIT_TEXT"));
    p_framTarget.document.writeln('<IMG SRC="' + g_strImagePath + 'FNDREQUI.gif" align=absmiddle>');
    p_framTarget.document.writeln('</DIV><BR>');

    fDrawAuditForm (p_framTarget, C_strFORMCHANGEAUDIT, g_objLookupTypes.mobjGetLookupType("PA_SST_CHANGE_REASON"), p_objAuditHistory);
  }
  if (!l_bNeedsChangeAudit && !l_bNeedsLateAudit && g_strCurrentAction != C_strMODIFYDELETE) {
    // This card does not need any audit information
    p_framTarget.document.writeln('<DIV class=helptext>');
    p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_NO_AUDIT_NEEDED"));
    p_framTarget.document.writeln('</DIV>');
   
  }
  // Workaround for dates in deleted history
  fTouchDate (top.g_objDeletedAuditHistory);

  // Draw Delete Submit form
  if (g_strCurrentAction==C_strMODIFYDELETE)
    fDrawAuditDeleteSubmitForm (p_framTarget);

  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}


//  ----------------------------------------------------
//  Function: fDrawAuditBodyButtons
//  Description: Draws the Audit body buttons in the
//               frame specified by p_framTarget, using
//               the timecard specified by p_objTimecard
//               For the Audit window, this frame is 
//               empty except for some hidden values.
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fDrawAuditBodyButtons(p_framTarget)
{
  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="' 
    + g_strHTMLLangPath + 'pawstime.css">');
  p_framTarget.document.writeln('<BODY class=PANEL>');

  p_framTarget.document.writeln('<FORM Name=formSaveSubmitFReview method="POST" ACTION="' + top.g_strSessDAD  + '/PA_SELF_SERVICE_HANDLER_PVT.From_SaveSubmitFReview" TARGET="framMainBody">');
                                                                                
  p_framTarget.document.writeln('<input type="hidden" name="hidTCHeader">');
  p_framTarget.document.writeln('<input type="hidden" name="hidTCLines">');
  p_framTarget.document.writeln('<input type="hidden" name="hidAuditHistory">');
  p_framTarget.document.writeln('<input type="hidden" name="hidFromPage">');
  p_framTarget.document.writeln('<input type="hidden" name="hidAction">');
  p_framTarget.document.writeln('</FORM>');

  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}

//  ----------------------------------------------------
//  Function: fbNeedsChangeAudit
//  Description: Determines if the card needs Audit
//               for any change.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fbNeedsChangeAudit (p_objAuditHistory)
{
  var l_arrAuditLine = p_objAuditHistory.mGetarrAuditLine();
  var l_iNumLines = l_arrAuditLine.length;
  var l_bFoundChange = false;
  var l_strAuditType;

  // Go through all audit lines and check for any change

  for (i=0 ; i < l_iNumLines && !l_bFoundChange; i++) {
    l_strAuditType = l_arrAuditLine[i].mGetstrAuditType();
    switch (l_strAuditType) {
      case C_strCHANGE_HISTORY:
      case C_strCHANGE_REVERSAL:
      case C_strCHANGE_DELETION:
        l_bFoundChange = true;
      break;
    }
  }
  return (l_bFoundChange);
}


//  ----------------------------------------------------
//  Function: fbNeedsLateAudit
//  Description: Determines if the card needs Audit
//               for being late.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fbNeedsLateAudit (p_objAuditHistory)
{
  var l_arrAuditLine = p_objAuditHistory.mGetarrAuditLine();
  var l_iNumLines = l_arrAuditLine.length;
  var l_bFoundLate = false;

  for (i=0 ; i < l_iNumLines && !l_bFoundLate; i++) {
    l_strAuditType = l_arrAuditLine[i].mGetstrAuditType();
    if (l_strAuditType==C_strLATE_ENTRY) {
      l_bFoundLate = true;
      break;
    }
  }
  return (l_bFoundLate);
}


//  ----------------------------------------------------
//  Function: fTouchDate
//  Description: Accesses a date object once
//    For some reason this has to be done to prevent
//    IE5 from suffering Javascript errors.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fTouchDate (p_objAuditHistory)
{
  var l_arrAuditLine = p_objAuditHistory.mGetarrAuditLine();
  var l_iNumLines = l_arrAuditLine.length;

  var l_strtest;
  
  if (l_iNumLines > 0)
    l_strtest = fDateToLongString(
          top.g_strSessDateFormat, 
          l_arrAuditLine[0].mGetdEntryDate(),
          true);                   
}


//  ----------------------------------------------------
//  Function: fDrawAuditForm
//  Description: Draws the late or change audit form.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditForm (p_framTarget, p_strFormName, p_objLookupType, p_objAuditHistory)
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_BODY");

  p_framTarget.document.writeln('<FORM name="' + p_strFormName + '">');
  p_framTarget.document.writeln('<TABLE cellpadding=0 cellspacing=0 border=0 width="100%">');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD class=promptblackright>');

  // check for deletion
  if (g_strCurrentAction==C_strMODIFYDELETE)
    p_framTarget.document.writeln('<IMG SRC="' + g_strImagePath + 'FNDREQUI.gif" align=absmiddle>');
  p_framTarget.document.writeln(fGetRegionPrompt(l_objCurRegion,"PA_WEB_REASON")+'&nbsp;</TD>');
  p_framTarget.document.writeln('    <TD>');
  fDrawAuditReasonPopList (p_framTarget, p_objLookupType, 'popAuditReason', p_strFormName, "", true);
  p_framTarget.document.writeln('      <INPUT type=text size=' +
    l_objCurRegion.mobjGetRegionItem("PA_WEB_REASON").mGetiLength() + 
    ' name="txtAuditDescription" onfocus="blur()" READONLY>');
  p_framTarget.document.writeln('    </TD>');
  p_framTarget.document.writeln('    <TD> &nbsp; </TD>');
  p_framTarget.document.writeln('  </TR><TR>');
  p_framTarget.document.writeln('    <TD class=promptblackright>');
  p_framTarget.document.writeln(fGetRegionPrompt(l_objCurRegion,"PA_WEB_COMMENT")+'&nbsp;</TD>');
  p_framTarget.document.writeln('    <TD>');
  p_framTarget.document.writeln('      <INPUT type=text size=' +

    // Let this comment field's width be 3 times the width of the comment
    // field in the table.
    l_objCurRegion.mobjGetRegionItem("PA_WEB_COMMENT").mGetiLength()*3 + 
    ' maxlength=80 name="txtAuditComment">');
  p_framTarget.document.writeln('    </TD>');

  p_framTarget.document.writeln('    <TD>');

  // Check if action is not delete
  if (g_strCurrentAction != C_strMODIFYDELETE) {
    fDrawButton(p_framTarget, C_strROUND_ROUND,                            
      fGetRegionPrompt(l_objCurRegion,'PA_WEB_APPLY_TO_SELECTED'),
      "top.fAuditOnClickApplyToSelected(self.document."+
      p_strFormName +")",   
      C_bBUTTON_ENABLED, C_strGRAYBG);                                     
  }
  else {
    p_framTarget.document.writeln('   &nbsp; ');
  }
  p_framTarget.document.writeln('    </TD>');

  p_framTarget.document.writeln('</TABLE><p>');
  
  // Check if action is not delete
  if (g_strCurrentAction != C_strMODIFYDELETE) {
    fDrawTableTopBorder(p_framTarget);
    fDrawAuditTableTitle(p_framTarget, p_strFormName);

    // Draw table lines
    fDrawAuditTableLines(p_framTarget, p_strFormName, p_objLookupType, p_objAuditHistory);

    p_framTarget.document.writeln('</TABLE>');
    fDrawTableBottomBorder(p_framTarget);

    p_framTarget.document.writeln('<P>');

  p_framTarget.document.writeln(makebuttons(
        C_strROUND_ROUND, fGetRegionPrompt(l_objCurRegion,"PA_WEB_SELECT_ALL_LINES"),     
    "top.fAuditOnClickSetAllLines(self.document."+
    p_strFormName +",true)", C_bBUTTON_ENABLED, C_strGRAYBG, C_strNARROW_GAP,                                
        C_strROUND_ROUND,fGetRegionPrompt(l_objCurRegion,"PA_WEB_UNSELECT_ALL_LINES"),
    "top.fAuditOnClickSetAllLines(self.document."+
    p_strFormName +",false)", C_bBUTTON_ENABLED,C_strGRAYBG, C_strNO_GAP));

  }
  p_framTarget.document.writeln('</FORM>');
}


//  ----------------------------------------------------
//  Function: fDrawAuditDeleteSubmitForm
//  Description: Draws the Submit form for audit deletion
//             
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditDeleteSubmitForm (p_framTarget) {
  
  p_framTarget.document.writeln('<FORM name="formDeleteAudit" method="POST" ACTION="' + top.g_strSessDAD +
      '/PA_SELF_SERVICE_INQUIRIES_PVT.ProcessAuditDeletion">');
  
  p_framTarget.document.writeln('<input type="hidden" name="hidExpenditureId">');
  p_framTarget.document.writeln('<input type="hidden" name="hidAuditReason">');
  p_framTarget.document.writeln('<input type="hidden" name="hidAuditComment">');
  p_framTarget.document.writeln('</FORM>');
}


//  ----------------------------------------------------
//  Function: fDrawAuditReasonPopList
//  Description: Draws the Audit Reason poplist given
//               the lookup type.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditReasonPopList (p_framTarget, p_objLookupType, p_strName, p_strFormName, p_strSelectedReason, p_bOnChangeActive)
{
  var l_strReason;
  var l_bFound = false;

  // Only put an onchange handler for the main popList, 
  // not in the multi-row table
  if (p_bOnChangeActive) {
    p_framTarget.document.writeln('      <SELECT name="'+p_strName+'" onchange="javascript:top.fAuditOnChangeAuditReason(self.document.' + p_strFormName +
  ',\''+p_strFormName+ '\')">');
  }
  else {
    p_framTarget.document.writeln('      <SELECT name="'+p_strName+'">');
  }

  // Determine which reason should be selected
  if (p_strSelectedReason =="") {
    p_framTarget.document.writeln('      <OPTION value="" selected>');
    l_bFound = true;
  } 
  else {
    p_framTarget.document.writeln('      <OPTION value="">');
  }

  for(var i = 0; i<p_objLookupType.arrLookupCode.length; i++) {               
    l_strReason=p_objLookupType.arrLookupCode[i].mGetstrCode()

    p_framTarget.document.write('      <OPTION value="'+l_strReason);
    
    // Check if this reason should be selected
    if (l_strReason == p_strSelectedReason) {
      p_framTarget.document.write('" selected>');
      l_bFound = true;
    }
    else {
      p_framTarget.document.write('">');
    }
    p_framTarget.document.writeln(l_strReason);
  }                                                                

  p_framTarget.document.writeln('      </SELECT>');

}


//  --------------------------------------------------
//  Function: fDrawAuditTableTitle
//  Description: Draws the title of the Audit multi-row table. 
//
//  --------------------------------------------------
function fDrawAuditTableTitle(p_framTarget, p_strFormName){
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_BODY");

  p_framTarget.document.writeln('      <TABLE cellpadding=2 cellspacing=1 border=0 width=100%>');
  p_framTarget.document.writeln('      <TR>');
  if (p_strFormName == C_strFORMCHANGEAUDIT)
    p_framTarget.document.writeln('        <TD colspan=8></TD>');
  else
    p_framTarget.document.writeln('        <TD colspan=7></TD>');

  p_framTarget.document.writeln('      </TR>');
  p_framTarget.document.writeln('      <TR>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_RADIO_SELECT") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_PROJECT_TASK_TYPE") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_DAY") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_DATE") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_HOURS") + '</TD>');

  // Only display due to column for Change Audit
  if (p_strFormName == C_strFORMCHANGEAUDIT)
    p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_DUE_TO") + '</TD>');

  p_framTarget.document.write('          <TD class=TABLEROWHEADER>');
  p_framTarget.document.write('<IMG SRC="' + g_strImagePath + 'FNDREQUI.gif">');
  p_framTarget.document.writeln(fGetRegionPrompt(l_objCurRegion,"PA_WEB_REASON") + '</TD>');
  p_framTarget.document.writeln('          <TD class=TABLEROWHEADER>' + fGetRegionPrompt(l_objCurRegion,"PA_WEB_COMMENT") + '</TD>');

  p_framTarget.document.writeln('      </TR>');
}


//  ----------------------------------------------------
//  Function: fDrawAuditTableLines
//  Description: Draws the lines within an audit form.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditTableLines (p_framTarget, p_strFormName, p_objLookupType, p_objAuditHistory)
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_AUDIT_BODY");
  var l_arrAuditLine = p_objAuditHistory.mGetarrAuditLine();
  var l_iNumLines = l_arrAuditLine.length;
  var l_iCurrLine = 0;
  var l_strAuditType;

  for (i=0; i < l_iNumLines; i++) {
    l_strAuditType = l_arrAuditLine[i].mGetstrAuditType();
    if ((p_strFormName == C_strFORMLATEAUDIT && 
         l_strAuditType == C_strLATE_ENTRY) || 
        (p_strFormName == C_strFORMCHANGEAUDIT &&
         (l_strAuditType == C_strCHANGE_HISTORY ||
          l_strAuditType == C_strCHANGE_DELETION ||
          l_strAuditType == C_strCHANGE_REVERSAL)))
      {

      // Draw audit row
      p_framTarget.document.writeln('<TR>');

      // Draw Select box
      p_framTarget.document.writeln('<TD class=tablecell align=center>');
      p_framTarget.document.writeln('<INPUT TYPE="CHECKBOX" NAME="cbxAuditSelect"></TD>');

      // Print Project, Task, Type
      p_framTarget.document.writeln('<TD class=tablecell>' +
        fCheckNullString(l_arrAuditLine[i].mGetstrProjectNum()) + ', ' +
        fCheckNullString(l_arrAuditLine[i].mGetstrTaskNum()) + ', ' +
        fCheckNullString(l_arrAuditLine[i].mGetobjType().mGetstrName()) +
        '</TD>');

      // Print Day
      p_framTarget.document.writeln('<TD class=tablecell>' +
        fIntToDayOfWeek(l_arrAuditLine[i].mGetdEntryDate().getDay()) + 
        '</TD>');

      // Print Date
      p_framTarget.document.writeln('<TD class=tablecell>' +
        fDateToLongString(
          top.g_strSessDateFormat, 
          l_arrAuditLine[i].mGetdEntryDate(),
          true) +                  
        '</TD>');

      // Print Hours
      p_framTarget.document.writeln('<TD class=tablecell>' +
        fFormatHours(l_arrAuditLine[i].mGetnQuantity()) +
        '</TD>');

      // Print Due To
      if (p_strFormName == C_strFORMCHANGEAUDIT) {
        p_framTarget.document.writeln('<TD class=tablecell>' +
        fAuditPrintDueTo(l_strAuditType) +
        '</TD>');
      }

      // Draw Reason Box
      p_framTarget.document.writeln('<TD class=tablecell>'); 
      fDrawAuditReasonPopList (p_framTarget, p_objLookupType, 'popAuditReason'+ l_iCurrLine, p_strFormName, l_arrAuditLine[i].mGetstrAuditReason(), false); 
      p_framTarget.document.writeln('</TD>');

      // Draw Comment Box
      p_framTarget.document.write('<TD class=tablecell>' + 
        '<INPUT type="text" size="' +
        l_objCurRegion.mobjGetRegionItem("PA_WEB_COMMENT").mGetiLength() +
        '" maxlength=80 name="txtAuditComment'+l_iCurrLine +'" value="');

      // Print comment if it exists.
      if (l_arrAuditLine[i].mGetstrAuditComment() != "")
        p_framTarget.document.write (fFormatString(l_arrAuditLine[i].mGetstrAuditComment())); 

      p_framTarget.document.writeln('"></TD>');

      p_framTarget.document.writeln('</TR>');

      l_iCurrLine++;
    }
  }
}


//  ----------------------------------------------------
//  Function: fAuditPrintDueTo
//  Description: Prints the Audit Type in the Due to column
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fAuditPrintDueTo (p_strAuditType)
{
  var l_objLookupType = g_objLookupTypes.mobjGetLookupType("PA_SST_AUDIT_TYPE");
  var l_strPrompt="";


  switch (p_strAuditType) {
    case C_strCHANGE_HISTORY : 
      l_strPrompt=l_objLookupType.mobjGetLookupCode('CHANGE').mGetstrDesc();
      break;
    case C_strLATE_ENTRY : 
    case C_strCHANGE_REVERSAL : 
    case C_strCHANGE_DELETION : 
      l_strPrompt=l_objLookupType.mobjGetLookupCode(p_strAuditType).mGetstrDesc();
      break;
  }

  return (l_strPrompt);
}


//  ----------------------------------------------------
//  Function: fAuditPrintAuditType
//  Description: Prints the Audit Type
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fAuditPrintAuditType (p_strAuditType)
{
  var l_objLookupType = g_objLookupTypes.mobjGetLookupType("PA_SST_AUDIT_TYPE");
  var l_strPrompt="";
  var l_objLookupCode = l_objLookupType.mobjGetLookupCode(p_strAuditType);

  if (l_objLookupCode!="")
    l_strPrompt = l_objLookupCode.mGetstrDesc();

  return (l_strPrompt);
}


//  ----------------------------------------------------
//  Function: fAuditOnClickApplyToSelected
//  Description: Applies the reason and comment to
//  each selected row
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fAuditOnClickApplyToSelected (p_formAudit)
{
  var l_bFoundSelected=false;
  var undefined;

  // Make sure a line is selected

  if (p_formAudit.cbxAuditSelect.length == undefined) {
    // Only one line
    if (p_formAudit.cbxAuditSelect.checked)
      l_bFoundSelected=true;
  }
  else {
    // multiple lines
    for (var i=0; i < p_formAudit.cbxAuditSelect.length && !l_bFoundSelected; i++) {
   //   alert (" box["+i+"] = " + p_formAudit.cbxAuditSelect[i].checked);
      if (p_formAudit.cbxAuditSelect[i].checked == true) {
        l_bFoundSelected = true;
      }
    }
  }

  // Alert error if no row is selected
  if (!l_bFoundSelected) {
    alert (g_objFNDMsg.mstrGetMsg("PA_WEB_AUDIT_SELECT_ROW"));
    return;
  }

  var l_iSelectedIndex = p_formAudit.popAuditReason.selectedIndex;

  // Copy reason and comment to each selected row
  if (p_formAudit.cbxAuditSelect.length == undefined) {
    // Only one line
    if (p_formAudit.cbxAuditSelect.checked)
      fGetInputWidget (p_formAudit, "popAuditReason", 0).selectedIndex = 
        l_iSelectedIndex;;
      fGetInputWidget (p_formAudit, "txtAuditComment", 0).value = 
        p_formAudit.txtAuditComment.value;
  }
  else {
    // multiple lines
    for (var i=0; i < p_formAudit.cbxAuditSelect.length; i++) {
   //   alert (" box["+i+"] = " + p_formAudit.cbxAuditSelect[i].checked);
      if (p_formAudit.cbxAuditSelect[i].checked == true) {
        fGetInputWidget (p_formAudit, "popAuditReason", i).selectedIndex = 
          l_iSelectedIndex;;
        fGetInputWidget (p_formAudit, "txtAuditComment", i).value = 
          p_formAudit.txtAuditComment.value;
      }
    }
  }
}


//  ----------------------------------------------------
//  Function: fAuditOnClickSetAllLines
//  Description: Sets all rows to the given value 
//    (true or false)
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fAuditOnClickSetAllLines (p_formAudit, p_bValue)
{
  var undefined;
  if (p_formAudit.cbxAuditSelect.length == undefined) {
    // Only one line
    p_formAudit.cbxAuditSelect.checked = p_bValue;
  }
  else {
    // multiple lines
    for (var i=0; i < p_formAudit.cbxAuditSelect.length; i++) {
      p_formAudit.cbxAuditSelect[i].checked = p_bValue; 
    }
  }
}


//  ----------------------------------------------------
//  Function: fAuditOnChangeAuditReason
//  Description: Updates the Audit description whenever
//  the reason is changed.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fAuditOnChangeAuditReason (p_formAudit, p_strFormName)
{
  var l_objLookupType;
  var l_objLookupCode;
  var l_strCode;
 
  if (p_strFormName==C_strFORMLATEAUDIT) {
      l_objLookupType=g_objLookupTypes.mobjGetLookupType("PA_SST_LATE_ENTRY_REASON");
  }
  else if (p_strFormName==C_strFORMCHANGEAUDIT) {
      l_objLookupType=g_objLookupTypes.mobjGetLookupType("PA_SST_CHANGE_REASON");
  }

  l_strCode = p_formAudit.popAuditReason.options[p_formAudit.popAuditReason.selectedIndex].value;

  if (l_strCode == "") {
    p_formAudit.txtAuditDescription.value = "";
  }
  else {
    l_objLookupCode = l_objLookupType.mobjGetLookupCode(l_strCode);
    p_formAudit.txtAuditDescription.value = l_objLookupCode.mGetstrDesc();
  }
}


//  ----------------------------------------------------
//  Function: fOnClickAuditToReview
//  Description: called when the Next button is
//  clicked on the Audit Information screen
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fOnClickAuditToReview()
{
  var l_framBodyContent = top.framMainBody.framBodyContent;
  var l_formChangeAudit = l_framBodyContent.document.formChangeAudit;
  var l_formLateAudit = l_framBodyContent.document.formLateAudit;
  
  // Check to see whether all entries have audit information entered.
  if ((g_bNeedsChangeAudit && (!fbAuditCheckFormEntered(l_formChangeAudit)))
      ||
      (g_bNeedsLateAudit && (!fbAuditCheckFormEntered(l_formLateAudit))))
  {
    alert (g_objFNDMsg.mstrGetMsg("PA_WEB_AUDIT_ALL_ENTRIES"));
    return;
  }
  
  // Save form info into Audit History data structure
  fUpdateAuditHistory (top.g_objAuditHistory, l_formLateAudit, l_formChangeAudit);

  // Continue to Final Review page
  fControlSSToCSContinue(top.g_objTimecard, top.g_objErrors, top.C_strFINAL_REVIEW, top.g_strCurrentAction, top.g_objAuditHistory, top.g_objDeletedAuditHistory);

}


//  ----------------------------------------------------
//  Function: fOnClickAuditSave
//  Description: called when the Save button is
//  clicked on the Audit Information screen
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fOnClickAuditSave()
{
  var l_framBodyContent = top.framMainBody.framBodyContent;
  var l_formChangeAudit = l_framBodyContent.document.formChangeAudit;
  var l_formLateAudit = l_framBodyContent.document.formLateAudit;
  
  // Save form info into Audit History data structure
  fUpdateAuditHistory (top.g_objAuditHistory, l_formLateAudit, l_formChangeAudit);

  // Submit and Continue to Save Confirmation Page
  fonClickSaveSubmitOrFinalReview(top.g_objTimecard, top.framMainBody.framBodyButtons.document.formSaveSubmitFReview, top.C_strAUDIT, top.C_strSAVE, top.g_objAuditHistory)

}


//  ----------------------------------------------------
//  Function: fOnClickAuditDelete
//  Description: called when the Next button is
//  clicked on the Delete / Audit Information screen
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fOnClickAuditDelete()
{
  var l_framBodyContent = top.framMainBody.framBodyContent;
  var l_formChangeAudit = l_framBodyContent.document.formChangeAudit;
  var l_formDeleteAudit = l_framBodyContent.document.formDeleteAudit;
  var l_iSelectedIndex = l_formChangeAudit.popAuditReason.selectedIndex;
  
  // Check to see whether an audit reason has been entered.
  if (l_iSelectedIndex==0) 
  {
    alert (g_objFNDMsg.mstrGetMsg("PA_WEB_AUDIT_REASON_REQUIRED"));
    return;
  }
  
  // Populate Delete submit form
  l_formDeleteAudit.hidExpenditureId.value = 
    top.g_objTimecard.mGetobjHeader().mGetiExpenditureID();

  l_formDeleteAudit.hidAuditReason.value = 
    l_formChangeAudit.popAuditReason[l_iSelectedIndex].value;

  l_formDeleteAudit.hidAuditComment.value = 
    l_formChangeAudit.txtAuditComment.value;

  // Perform delete
  l_formDeleteAudit.submit();

  // Display Modify Timecard screen
}


//  ----------------------------------------------------
//  Function: fOnClickAuditToHours
//  Description: called when the Back button is
//  clicked on the Audit Information screen
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fOnClickAuditToHours()
{
  var l_framBodyContent = top.framMainBody.framBodyContent;
  var l_formChangeAudit = l_framBodyContent.document.formChangeAudit;
  var l_formLateAudit = l_framBodyContent.document.formLateAudit;
  
  // Save form info into Audit History data structure
  fUpdateAuditHistory (top.g_objAuditHistory, l_formLateAudit, l_formChangeAudit);

  // Continue to Hours page
  fDrawTimeEntry2();
}


//  ----------------------------------------------------
//  Function: fbAuditCheckFormEntered
//  Description: Checks whether all the reasons in this
//  form have been entered.
//    Returns true if all entries have a reason.
//    Returns false if there is an entry without a reason
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fbAuditCheckFormEntered (p_formAudit)
{
  var undefined;
  var l_bFoundNotEntered = false;

  if (p_formAudit.cbxAuditSelect.length == undefined) {
    // Only one line
    if (fGetInputWidget (p_formAudit, "popAuditReason", 0).selectedIndex==0)
      l_bFoundNotEntered=true;
  }
  else {
    // multiple lines
    for (var i=0; i < p_formAudit.cbxAuditSelect.length && !l_bFoundNotEntered; i++) {
    if (fGetInputWidget (p_formAudit, "popAuditReason", i).selectedIndex==0)
      l_bFoundNotEntered=true;
    }
  }
  return (!l_bFoundNotEntered);
}


//  ----------------------------------------------------
//  Function: fUpdateAuditHistory
//  Description: Update each corresponding entry in the 
//    Audit History object with the applicable audit comment, 
//    and reason.
//  Author: Kristian Widjaja
//  ----------------------------------------------------
function fUpdateAuditHistory (p_objAuditHistory, p_formLateAudit, p_formChangeAudit)
{
  var l_arrAuditLine = p_objAuditHistory.mGetarrAuditLine();
  var l_iNumLines = l_arrAuditLine.length;
  var l_iLateAuditLine = 0;
  var l_iChangeAuditLine = 0;
  var l_strAuditType;
  var l_objSelect;

  for (i=0; i < l_iNumLines; i++) {
    l_strAuditType = l_arrAuditLine[i].mGetstrAuditType();
    switch (l_strAuditType) {
      case C_strLATE_ENTRY :
        // Get reason and comment from the Late Audit form
        l_objSelect =fGetInputWidget(p_formLateAudit,"popAuditReason", l_iLateAuditLine);
        l_arrAuditLine[i].mSetstrAuditReason(l_objSelect.options[l_objSelect.selectedIndex].value);
        l_arrAuditLine[i].mSetstrAuditComment(fGetInputWidget(p_formLateAudit,"txtAuditComment", l_iLateAuditLine).value);
        l_iLateAuditLine++;
        break;

      case C_strCHANGE_HISTORY :
      case C_strCHANGE_DELETION :
      case C_strCHANGE_REVERSAL :
        // Get reason and comment from the Change Audit form
        l_objSelect =fGetInputWidget(p_formChangeAudit,"popAuditReason", l_iChangeAuditLine);
        l_arrAuditLine[i].mSetstrAuditReason(l_objSelect.options[l_objSelect.selectedIndex].value);
        l_arrAuditLine[i].mSetstrAuditComment(fGetInputWidget(p_formChangeAudit,"txtAuditComment", l_iChangeAuditLine).value);
        l_iChangeAuditLine++;
        break;
    }
  }
}


//  ----------------------------------------------------
//  Function: fDrawAuditInfo
//  Description: Draws the Audit Information page.
//               This is called when called standalone.
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fDrawAuditInfo(){
  var l_framTarget = top.framMainBody;

  fDrawToolbarFrame(top.framToolbar, C_strAUDIT);

  l_framTarget.document.open();
  l_framTarget.document.writeln(fDrawContentFrameSet(C_strAUDIT));  
  l_framTarget.document.close();

  fDrawButtonFrame(top.framButtons, C_strAUDIT, g_strCurrentAction);

  // Bug 1736500. Need to explicitly draw here, otherwise
  // we get "Action Cancelled" message on Win95/IE5.
  if (fIsIE5() && fIsWindows95()) {
    fDrawAuditBodyHeader (top.framMainBody.framBodyHeader,top.g_objTimecard);
    fDrawAuditBodyContent(top.framMainBody.framBodyContent,top.g_objAuditHistory);
    fDrawAuditBodyButtons(top.framMainBody.framBodyButtons);
  }
}


//  ----------------------------------------------------
//  Function: fOnClickHoursToAudit
//  Description: On Click handler which is executed when
//               the next button is clicked in Time 
//               Entry when Full Audit is enabled
//  Author: Kristian Widjaja
//  ----------------------------------------------------

function fOnClickHoursToAudit()
{
//  fDrawAuditInfo();
var l_objForm;                                                                  
                                                                                
  fCodeForExplorer();                                                           
  fIsFormValid();
  l_objForm = top.framMainBody.framBodyButtons.document.formSaveSubmitFReview;  
                                                                                
  // Save implicitly, and go to the audit information screen
  if (top.g_objErrors.miGetNumOfErrors() < 1){
    top.fonClickSaveSubmitOrFinalReview(top.g_objTimecard, l_objForm, C_strENTER_HOURS, C_strAUDIT, top.g_objAuditHistory);
  }
}
