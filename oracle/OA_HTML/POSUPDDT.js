/*=================================================================+
|               Copyright (c) 1999 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSUPDDT.js 115.0 99/10/14 16:20:24 porting shi $ */


function SearchPOs()
{
  self.document.POS_UPD_DATE.submit();
}

function clearFields()
{
  self.document.POS_UPD_DATE.POS_PO_NUMBER.value="";
  self.document.POS_UPD_DATE.POS_ITEM_NUMBER.value="";
  self.document.POS_UPD_DATE.POS_ITEM_DESCRIPTION.value="";
  self.document.POS_UPD_DATE.POS_SUPPLIER_ITEM_NUMBER value="";
}
