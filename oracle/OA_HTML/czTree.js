/* $Header: czTree.js 115.23 2001/07/03 10:56:19 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99   CK Jeyaprakash, K MacClay  Created.                    |
 |                                                                           |
 +===========================================================================*/

function Tree ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.initializeControl ();
  this.nodeColl = new Array ();
  this.totalNodeCount = 0;  
  this.type = 'Tree';
  this.lastNodeBottom = 0;
  this.treeStyle = Tree.STYLE_TREE_LINES_AND_ICONS;
}

function Tree_initializeControl ()
{
  this.zIndex = 0;
  this.treeIcons = new Array ();
  this.treeIcons['pnode'] = IMAGESPATH + 'czPNode.gif';
  this.treeIcons['plast'] = IMAGESPATH + 'czPLNode.gif';
  this.treeIcons['mnode'] = IMAGESPATH + 'czMNode.gif';
  this.treeIcons['mlast'] = IMAGESPATH + 'czMLNode.gif';
  this.treeIcons['tnode'] = IMAGESPATH + 'czTNode.gif';
  this.treeIcons['last']  = IMAGESPATH + 'czLNode.gif';
  this.treeIconWidth = 16;
  this.treeIconHeight = 22;
  this.treeNodeHeight = 22;
}

function Tree_createBorder ()
{
  return (0);
}

function Tree_setReference (ref)
{
  this.reference = ref;
  if (this.root) {
    var n = this.root;
    if (ns4)
      n.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    else if (ie4)
      n.setReference (this.reference);
  }
}

function Tree_setTreeStyle (iStyle)
{
  if (this.launched) 
    return;
  this.treeStyle = iStyle;
}

function Tree_getTreeStyle ()
{
  return (this.treeStlyle);
}

function Tree_setSelectedBackColor (c)
{
  this.selectedBackColor = c;
  if (this.selectedNode) {
    this.selectedNode.hilite (c);
  }
}

function Tree_getSelectedBackColor (c)
{
  return (this.selectedBackColor);
}

function Tree_createBorder (borderWidth, borderColor)
{
	// override for future use
}

function Tree_setTransparent ()
{
  if (this.launched) return;
  this.bTransparent = true;
}

function Tree_isTransparent ()
{
  return (this.bTrasparent);
}

function Tree_addNode (relative, relationship, key, text, bUnSatisfied, bAvailable, iconNormal, iconSelected)
{
  if (ns4 && this.launched) {
    var ht = this.totalNodeCount * this.treeNodeHeight + 50;
    if (ht > this.height && (window.getUiBroker)) {
      //adding a new node will get clipped in NS now. Do not add a new node
      //get the ui broker and post refresh frames
      var ub = window.getUiBroker();
      if (ub != null) {
        ub.refreshTreeFrame();
	return;
      }
    }
  }
  var n;
  if (relative) {
    if (typeof (relative) == 'string') {
      var relKey = relative;
      relative = this.nodeColl[relKey];
    }
  }
  if (!relative && !relationship) {
    n = this.createRootNode (key, text, iconNormal, iconSelected);
  } else if (!relative) {
    if (this.root) {
      n = this.root.addNode (this.root, relationship, key, text, iconNormal, iconSelected);
    } else  {
      n = this.createRootNode (key, text, iconNormal, iconSelected);
    }
  } else if (relative == this.root) {
    n = this.root.addNode (this.root, relationship, key, text, iconNormal, iconSelected);
  } else if (relationship != Tree.AT_CHILD) {
    n = relative.parent.addNode (relative, relationship, key, text, iconNormal, iconSelected);
  } else {
    n = relative.addNode (relative, relationship, key, text, iconNormal, iconSelected);
  }
  this.nodeColl[n.key] = n;
  n.setRoot (this.root);
  n.setUnSatisfied (bUnSatisfied);
  n.setAvailability (bAvailable);
  n.setWindowName(this.windowName);
  //if (this.font) 
  //  n.setFont (this.font);
  n.addListener ('treeCollapseCallback', this);
  n.addListener ('treeExpandCallback', this);
  n.addListener ('mousedownCallback', this);
  n.addListener ('mouseupCallback', this);
  
  this.notifyListeners ('objectAddedCallback', n,this);
  this.totalNodeCount++;
  return n;
}

function Tree_addNodes (aryNodes)
{
  var len = aryNodes.length;
  for (var i=0; i<len; i++) {
    var n = aryNodes [i];
    this.addNode (n.relative, n.relationship, n.key, n.text, n.iconNormal, n.iconSelected);
  }
}

function Tree_createRootNode (key, text, iconNormal, iconSelected)
{
  this.root = new TreeNode ();
  this.root.setDimensions(0,0,this.width,22);
  this.root.setParentObject(this);
  this.root.setName(key);
  this.root.label.setValue(text);
  this.root.setToolTipText (text);
  this.root.setIconNormal(iconNormal);
  this.root.setIconSelected(iconSelected);
  this.root.setSpanNormal(this.spanNormal);
  this.root.setSpanSelected(this.spanSelected);
  this.root.setBackgroundColor(this.backgroundColor);
  this.root.setExpanded(true);
  this.root.setStyle(this.treeStyle);

  this.setReference (this.reference);
  if (this.launched) {
    this.root.bRoot = true;
    this.root.createLayer (this);
  }
  this.root.setAsRootNode = TreeNode_setAsRootNode;
  this.root.setAsRootNode ();

  return (this.root);
}

function Tree_removeNode (key)
{
  var n = null;
  n = this.nodeColl [key];
  if (n) {
    var nParent = n.getParent ();
    if (nParent) {
      nParent.removeNode (n);
      if (this.selectedNode.key == n.key) {
        this.selectedNode = null;
      }
      this.nodeColl[key] = null;
      delete this.nodeColl[key];
    } else {
      this.root.recursiveDelete ();
      this.root = null;
      delete this.root;
      this.nodeColl = new Array ();
      this.selectedNode = null;
    }
  }
}

function Tree_removeAll ()
{
  if (this.root)
    this.removeNode (this.root.key);
}

function Tree_getItem (key)
{
  if (this.nodeColl[key])
    return (this.nodeColl[key]);
  else
    return null;
}

function Tree_getItems ()
{
  if (this.nodeColl)
    return (this.nodeColl);
  else
    return null;
}

function Tree_setNodeIcons ()
{
  var len = arguments.length;
  for (var i=0; i < len; i = i+2) {
    if (!this.iconsHash) this.iconsHash = new Array();
    this.iconsHash [arguments[i]] = IMAGESPATH + arguments[i+1];
  }
}

function Tree_getNodeIconSrc (key)
{
  return (this.iconsHash[key]);
}

function Tree_getTreeIconSrc (key)
{
  return (this.treeIcons[key]);
}

function Tree_setSelectedNode (node)
{
  if (typeof (node) == 'string') {
    var key = node;
    node = this.getItem (key);
  }
  if (this.selectedNode)
    this.selectedNode.setSelected (false);
  node.setSelected (true);
  this.selectedNode = node;
}

function Tree_getSelectedNode ()
{
  if (this.selectedNode)
    return this.selectedNode;
  else
    return null;
}

function Tree_isSelectedNode (node)
{
  if (typeof (node) == 'string') {
    var key = node;
    node = this.getItem (key);
  }
  return (node.isSelected ());
}

function Tree_generateCSS()
{
  var CSSStr = "";
  
  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";
  
  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";
  
  if(this.height != null) {
    var ht = this.totalNodeCount * this.treeNodeHeight + 30;
    if (this.height < ht) {
      this.height = ht;
    }
    CSSStr += "height:" + this.height + "; "
  }
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height) 
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";

  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";

  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  CSSStr += "}";
  
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();

  return(CSSStr);
}

function Tree_moreCSS ()
{
  var css = "";
  
  //this.fixTop ();

  if (this.root)
    css += this.root.generateCSS ();

  return (css);
}

function Tree_innerRender ()
{
  var sBuffer = "";
  if (this.root)
    sBuffer += '\n' + this.root.render ();
  
  return (sBuffer);
}

function Tree_moreLaunch (em)
{
  if (this.root) {
    this.root.launch (em);
    this.selectedNode = this.root;
    this.root.setSelected (true);
  }
  this.fixTop();
  if (em) {
    em.addListener ('onKeyDownCallback', this);
    em.addListener ('onKeyUpCallback', this);
  }
}

function Tree_fixTop ()
{
  var key;
  if (this.root) {
    this.lastNodeBottom  = this.root.getHeight ();
    this.root.fixTop ();
  }
}

function Tree_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;
  
  this.font = fnt;
  
  this.spanNormal = this.name + 'SpnNml';
  this.spanSelected = this.name + 'SpnSel';
  
  style = this.getStyleObject (this.spanNormal, true);
  this.setFontAttributes (style);
  
  style = this.getStyleObject (this.spanSelected, true);
  this.setFontAttributes (style);
}

function Tree_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
}

function Tree_setColor (col)
{
  if (this.launched)
    return;
  this.color = col;
  this.spanNormal = this.name + 'SpnNml';
  var styl = this.getStyleObject (this.spanNormal, true);
  styl.setAttribute ('color', col);
}

function Tree_setSelectedColor (col)
{
  if (this.launched)
    return;
  this.selectedColor = col;
  this.spanSelected = this.name + 'SpnSel';
  var styl = this.getStyleObject (this.spanSelected, true);
  styl.setAttribute ('color', col);
}

function Tree_mousedownCallback (e, src)
{
  var rtnVal = true;
  rtnVal = this.notifyListeners ('mousedownCallback', e, src);

  if (self.editObj){
    if (self.editObj.oldVal != self.editObj.textElement.value){
      self.ub.pendingEvt = true;
      self.ub.pendingObj = src;
      self.ub.evtType = "<click rtid='" + self.ub.formatRuntimeId(src.getName()) +  "'/>";
      self.editObj.doBlur();
      rtnVal = false;
    }
    if (self.cntObj){
      self.cntObj.resetHeight();
      self.cntObj = null;
    }
  }
  if (rtnVal) {
    rtnVal = this.notifyListeners ('beforeNodeClickCallback', e, src);
    if (rtnVal) {
      if (this.selectedNode)
        this.selectedNode.setSelected (false);
      src.setSelected (true);
      this.selectedNode = src;
      rtnVal = this.notifyListeners ('nodeClickCallback', e, src);
    }
  }
  return rtnVal;
}

function Tree_mouseupCallback (e, src)
{
  return (this.notifyListeners ('mouseupCallback', e, src));
}

function Tree_treeCollapseCallback (e, src)
{
  //TODO: Fetch data and update tree
  return true;
}

function Tree_treeExpandCallback (e, src)
{
  //TODO: Fetch data and update tree
  return true;
}

function Tree_onKeyDownCallback (e, eSrc)
{
  var n = null;
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 38:
    n = this.selectedNode.getPreviousVisibleNode();
    break;
  case 40:
    n = this.selectedNode.getNextVisibleNode ();
    break;
  case 13:
    if (this.selectedNode.children.length) {
      if (this.selectedNode.expanded)
        this.selectedNode.collapse();
      else
        this.selectedNode.expand();
      this.selectedNode.propogateStateChange ();
    }
    break;
  }
  if (n) {
    this.setSelectedNode (n);
    self._tree = this;
    self._treeSelNode = n;
    if (this.timeoutHdl)
      clearTimeout (this.timeoutHdl);
    this.timeoutHdl = setTimeout("Tree_synchNodeSelection()", 500);
  }
  switch (key) {
  case 13:
  case 38:
  case 40:
    if (ie4) {
      e.cancelBubble = true;
      e.returnValue = false;
    }
  }
}

function Tree_onKeyUpCallback (e, eSrc)
{

}

function Tree_synchNodeSelection(nodeId)
{
  var tree = self._tree;
  var selNode = self._treeSelNode;
  if (selNode == tree.getSelectedNode()) {
    selNode.notifyListeners('mouseupCallback', null, selNode);
    self._tree = null;
    self._treeSelNode = null;
    clearTimeout(tree.timeoutHdl);
  }
}

function Tree_moreDestroy ()
{
  if (this.nodeColl) {
    for (var id in this.nodeColl) {
      this.nodeColl[id].destroy ();
      this.nodeColl [id] = null;
      delete this.nodeColl[id];
    }
    this.nodeColl = null;
    delete this.nodeColl;
    if (this.root) {
      this.root.destroy ();
      this.root = null;
      delete this.root;
    }
  }
}

//Static members
Tree.AT_FIRST = 0;
Tree.AT_LAST = 1;
Tree.AT_NEXT = 2;
Tree.AT_PREVIOUS = 3;
Tree.AT_CHILD = 4;

//Static members for tree lines and icon settings
Tree.STYLE_NONE = 0;
Tree.STYLE_TREE_LINES_ONLY = 1;
Tree.STYLE_ICONS_ONLY = 2;
Tree.STYLE_TREE_LINES_AND_ICONS = 3;

//Extend from Base
HTMLHelper.importPrototypes(Tree, Base);

Tree.prototype.constructor = Tree;
Tree.prototype.setReference = Tree_setReference;
Tree.prototype.setFont = Tree_setFont;
Tree.prototype.setTreeStyle = Tree_setTreeStyle;
Tree.prototype.getTreeStyle = Tree_getTreeStyle;
Tree.prototype.setColor = Tree_setColor;
Tree.prototype.setSelectedColor = Tree_setSelectedColor;
Tree.prototype.setSelectedBackColor = Tree_setSelectedBackColor;
Tree.prototype.getSelectedBackColor = Tree_getSelectedBackColor;

//has more overheads use only when requried
Tree.prototype.setTransparent = Tree_setTransparent;
Tree.prototype.isTransparent = Tree_isTransparent;
Tree.prototype.createBorder = Tree_createBorder;

Tree.prototype.addNode = Tree_addNode;
Tree.prototype.addNodes = Tree_addNodes;
Tree.prototype.removeNode = Tree_removeNode;
Tree.prototype.removeAll = Tree_removeAll;
Tree.prototype.getItem = Tree_getItem;
Tree.prototype.getItems = Tree_getItems;
Tree.prototype.setSelectedNode = Tree_setSelectedNode;
Tree.prototype.isSelectedNode = Tree_isSelectedNode;
Tree.prototype.getSelectedNode = Tree_getSelectedNode;

Tree.prototype.setNodeIcons = Tree_setNodeIcons;
Tree.prototype.getNodeIconSrc = Tree_getNodeIconSrc;
Tree.prototype.getTreeIconSrc = Tree_getTreeIconSrc;

Tree.prototype.moreCSS  = Tree_moreCSS;
Tree.prototype.generateCSS  = Tree_generateCSS;
Tree.prototype.innerRender  = Tree_innerRender;
Tree.prototype.moreLaunch = Tree_moreLaunch;
Tree.prototype.moreDestroy = Tree_moreDestroy;

//private methods
Tree.prototype.initializeControl = Tree_initializeControl;
Tree.prototype.createRootNode = Tree_createRootNode;
Tree.prototype.fixTop = Tree_fixTop;
Tree.prototype.setFontAttributes = Tree_setFontAttributes;

Tree.prototype.mousedownCallback = Tree_mousedownCallback;
Tree.prototype.mouseupCallback = Tree_mouseupCallback;
Tree.prototype.onKeyDownCallback = Tree_onKeyDownCallback;
Tree.prototype.onKeyUpCallback = Tree_onKeyUpCallback;
Tree.prototype.treeCollapseCallback = Tree_treeCollapseCallback;
Tree.prototype.treeExpandCallback = Tree_treeExpandCallback;
