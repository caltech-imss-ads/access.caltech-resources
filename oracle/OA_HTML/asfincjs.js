<!-- $Header: asfincjs.js 115.28 2001/05/14 23:12:29 pkm ship  $ -->

function doAsfAction(formName, actType) {
  if (validateRequired(formName)) {
    document.forms[formName].elements['PAGE.OBJ.asfAction'].value = actType;
    document.forms[formName].submit();
  }
}

function doAsfDownload(formName, pageName) {
  document.forms[formName].elements["PAGE.OBJ.CSVDOWNLOAD"].value = "YES";
  self.onfocus = doAfterAsfDownload;
  self.asfFormName = formName;
  self.submitAction = document.forms[formName].action;
  document.forms[formName].action = pageName;
  document.forms[formName].submit();
}

function doAfterAsfDownload() {
  document.forms[self.asfFormName].elements["PAGE.OBJ.CSVDOWNLOAD"].value = "";
  document.forms[self.asfFormName].action = self.submitAction;
}

function getElementValue(element) {
  if (!element) return null;
  if (element.type == "select-one"){
    var index = element.selectedIndex;
    if (index < 0) index = 0;
    if (element.options[index])
      return element.options[index].value;
    else return "";
  }
  else return element.value;
}

function getValue(formName, fieldName) {
  return getElementValue(document.forms[formName].elements[fieldName]);
}

function showMap(address, city, state, province, zipCode, country) {
  w = window.open("http://maps.yahoo.com/py/maps.py?BFCat=&Pyt=Tmap&newFL=Use+Address+Below&" + encodeUrlData('addr', address, 'csz', city + ' ' + state + ' ' + province + ' ' + zipCode, 'country', country) + "&Get%A0Map=Get+Map", "", "scrollbars,HEIGHT=500,WIDTH=500,resizable");
}

function showTextAttachment(atchId, typeId) {
  w = window.open("asfAttchDtailMain.jsp?atchId=" + atchId + "&typeID=" + typeId, "", "scrollbars,HEIGHT=500,WIDTH=500,resizable");
}

function showURLAttachment(atchURL) {
  w = window.open(atchURL, "", "scrollbars,HEIGHT=500,WIDTH=500,resizable");
}

//encode string
function encodeUrlData(name,val) {
  str="";
  if(name!=null)
    str += _replaceChar(escape(name),'+','%2B',true);
    if(val!=null)
      str += '='+_replaceChar(escape(val),'+','%2B',true);

    if(arguments.length >2)
      for(var i=2; i<arguments.length;i=i+2) {
        if(arguments[i]) 
          str += '&'+_replaceChar(escape(arguments[i]),'+','%2B',true);
          if(arguments[i+1])
            str += '='+_replaceChar(escape(arguments[i+1]),'+','%2B',true);
      }
  return str;
}

//decode encoded string and retrieve individual name and value
function decodeUrlData(encodedData) {
  var dataArr = new Array();
  if(encodedData) {
    if(encodedData.charAt(0)=='?')
      encodedData = encodedData.substring(1,encodedData.length);
      encodedData = _replaceChar(encodedData,'+',' ');
  }
  else
    return null;

  var strArr = encodedData.split('&');
  if(strArr) {
    if(strArr.length)
      for(var i=0; i<strArr.length;i++) {
        var str =strArr[i].split('=');
        if(str.length==1)
          dataArr[unescape(str[0])]= null;
        else if(str.length>1)
          dataArr[unescape(str[0])]= unescape(str[1]);
      }
      return dataArr;
  }
  return null;
}

function buildURL(url,postfix)
{
    var retURL = "";
    if(url && postfix)
    {
      var pos = url.indexOf("?");
      if(pos == -1)
          retURL=url+"?"+postfix; 
      else
         retURL=url+"&"+postfix;
    }
        
    return retURL;
} 

//replace function 
function _replaceChar(str,c,r,recur) {
  var temp='';
  if(recur == null) recur = true;

  if(recur == false) {
    var pos = str.indexOf(c);
    if(pos!=-1)
      temp = str.substring(0,pos)+r+str.substring(pos+1,str.length);
    else
      temp = str;
  }
  else
    for(var i=0; i<str.length;i++)
      if(str.charAt(i)==c)
        temp +=r;
      else
        temp +=str.charAt(i);

  return temp;
}

function trim(str) {
  if (!str) return "";
  var start = 0;
  var len = str.length;
  var end = len-1;
  for(var i = 0; i < len; i++)
    if (str.charCodeAt(i) == 32 || str.charCodeAt(i) == 9)
      start++;
    else
      break;
  for(var i=end;i>=0;i--)
    if(str.charCodeAt(i) == 32 || str.charCodeAt(i) == 9)
      end--;
    else
      break;
  if (start <= end)
    return(str.substring(start,end+1));
  else return "";
}

function validateRequired(formName) {
  thisForm = document.forms[formName];
  found = false;
  if (self.reqSections) {
    for (var i = 0; i < self.reqSections.length; i++) {
      secName = self.reqSections[i].name;
      cols = self.reqSections[i].cols;
      for (var j = 0; j < cols.length; j ++) {
        rowId = 0;
        while (thisForm.elements[secName + ".ROW" + rowId + "." + cols[j].name]) {
          asfAction = thisForm.elements[secName + ".ROW" + rowId + ".asfAction"];
          asfRemove = thisForm.elements[secName + ".ROW" + rowId + ".asfRemove"];
          if (asfAction && (asfAction.value == "UPD" || asfAction.value == "CRE")
              && !(asfRemove && asfRemove.checked)) {
            var e = thisForm.elements[secName + ".ROW" + rowId + "." + cols[j].name];
            var focusE = thisForm.elements[secName + ".ROW" + rowId + "." + cols[j].focusName];
              if (trim(getElementValue(e)).length == 0) {
                alert(cols[j].errMsg);
                  focusE.focus();
                return false;
              }
          }
          rowId ++;
        }
      }
    }
  }
  if (self.reqFields) {
    for (var i = 0; i < self.reqFields.length; i++) {
      element = thisForm.elements[self.reqFields[i].name];
      focusElement = thisForm.elements[self.reqFields[i].focusName];
      if (element && trim(getElementValue(element)).length == 0) {
        alert(self.reqFields[i].errMsg);
          focusElement.focus();
        return false;
      }
    }
  }
  return true;
}

function addRequiredField(name, errMsg, focusName) {
  if (!self.reqFields) self.reqFields = new Array();
  fieldNum = self.reqFields.length;
  self.reqFields[fieldNum] = new Object();
  self.reqFields[fieldNum].name = name;
  self.reqFields[fieldNum].errMsg = errMsg;
  if(focusName) {
    self.reqFields[fieldNum].focusName = focusName;
  } else {
    self.reqFields[fieldNum].focusName = name;
  }
}

function isAsfDigits(str, min, max) {
  var len = str.length;
  for(var i = 0; i < len; i++)
    if (str.charAt(i) > '9' || str.charAt(i) <'0') 
      return false;
  var value = parseInt(str);
  if (min)
    if (value < min) return false;
  if (max)
    if (value > max) return false;
  return true;
}

function isAsfNumber(str) {
  str = trim(str);
  if (str == "")
    return true;
  str = str.replace(/</,'-');
  str = str.replace(/>/,'');
  str = str.replace(/--/,'');
  str = str.replace(/ /g,'');
  str = str.replace(/,/g,'');
                
  if (isNaN(str))
    return false;
  return true;
}

function parseAsfNumber(str) {
  str = trim(str);
  if (str == "")
    return 0;
  str = str.replace(/</,'-');
  str = str.replace(/>/,'');
  str = str.replace(/--/,'');
  str = str.replace(/ /g,'');
  str = str.replace(/,/g,'');
                
  if (isNaN(str))
    return 0;
  return parseFloat(str);
}

function checkAsfNumber(element, errMsg, min, max) {
  if (element && !isAsfNumber(element.value, min, max)) {
    alert(errMsg);
    element.focus();
    //element.value = '';
    return false;
  }
  else return true;
}

function checkAsfAllDigits(element, errMsg) {
  if (element && !isAsfDigits(element.value)) {
    alert(errMsg);
    element.focus();
    //element.value = '';
    return false;
  }
  else return true;
}

function checkMinLength(element, len, errMsg1, errMsg2, leadWildcard) {
  if(!leadWildcard)leadWildcard = "N";
  if (element) {
    if (trim(element.value).length < len) {
      alert(errMsg1);
      element.focus();
      return false;
    }
    else if (trim(element.value).charAt(0) == '%' && leadWildcard == "N"){
      alert(errMsg2);
      element.focus();
      return false;
    }
    else return true;
  }
  else return false;
}

function enforceMaxDateRange(sDateString, eDateString, maxDays, message)
{
   var startDate = (sDateString.split("**"))[1];
   var endDate = (eDateString.split("**"))[2];
   if(checkMaxDateRange(startDate, endDate, maxDays))
     return true
   else
   {
     alert(message);
     return false;
   }
   
}

function checkMaxDateRange(startDate,endDate,maxDays)
{
   if(startDate && endDate && maxDays)
   {
      var sd = startDate.split(" ");
      var sDateElems = sd[0].split("-");
      var sTimeElems = sd[1].split(":");
      var ed = endDate.split(" ");
      var eDateElems = ed[0].split("-");
      var eTimeElems = ed[1].split(":");
 
      var sDate = new Date();
      sDate.setFullYear(sDateElems[0]);
      sDate.setMonth(sDateElems[1]);
      sDate.setDate(sDateElems[2]);
      sDate.setHours(sTimeElems[0]);
      sDate.setMinutes(sTimeElems[1]);
      sDate.setSeconds(sTimeElems[2]);

      var eDate = new Date();
      eDate.setFullYear(eDateElems[0]);
      eDate.setMonth(eDateElems[1]);
      eDate.setDate(eDateElems[2]);
      eDate.setHours(eTimeElems[0]);
      eDate.setMinutes(eTimeElems[1]);
      eDate.setSeconds(eTimeElems[2]);

      var maxRangeMillis = maxDays*30*24*60*60*1000; 
   	
      if((eDate.getTime()-sDate.getTime())>maxRangeMillis)
        return false; 
      else
       return true; 
  }
  else
     return true;
}

function reloadDependentLOV(formName, currentPageName)
{
     setFromReqFlag(formName);
     document.forms[formName].action=currentPageName;
     document.forms[formName].submit();    
}

function setFromReqFlag(formName)
{
    if(document.forms[formName].elements["PAGE.OBJ.asfFromRequest"])
      document.forms[formName].elements["PAGE.OBJ.asfFromRequest"].value="Y";
}
 
function resetFromReqFlag(formName)
{
    if(document.forms[formName].elements["PAGE.OBJ.asfFromRequest"])
      document.forms[formName].elements["PAGE.OBJ.fromRequest"].value="";
}

function safeSubmit(formName)
{
  if (!document.forms 
     || !document.forms[formName]
     || !document.forms[formName].elements 
     || !document.forms[formName].elements["PAGE.OBJ.asfAction"]) {
    alert(asfSafeSubmitErrMsg);
  } else {
    document.forms[formName].submit();
  }
}

