/* $Header: czTNode.js 115.24 2001/07/03 10:56:18 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99   CK Jeyaprakash, K MacClay  Created.                    |
 |                                                                           |
 +===========================================================================*/

function TreeNode ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.initializeControl ();
  this.children = new Array ();
  
  this.type = 'TreeNode';
  this.expanded = false;
  this.bRoot = false;
  this.bTLineVisible = true;
  this.bIconVisible = true;
  this.bAvailable = true;
}

function TreeNode_initializeControl ()
{
  this.height = 22;
  this.zIndex = 2;
  
  this.collapser = new ImageButton ();
  this.icon = new ImageButton ();
  this.label = new Prompt ();
  
  this.collapser.setLeft (0);
  this.collapser.setHeight (22);
  this.collapser.setWidth (16);
  this.collapser.setTop (Math.round ((this.height-this.collapser.height)/2));
  this.collapser.setParentObject (this);
  this.collapser.stretch = true;
  
  this.icon.setLeft (this.collapser.left + this.collapser.width);
  this.icon.setWidth (16);
  this.icon.setHeight (22);
  this.icon.setTop (Math.round ((this.height-this.icon.height)/2));
  this.icon.setParentObject (this);
  this.icon.setSrc (IMAGESPATH + 'czfldcls.gif');
  
  this.label.setParentObject (this);
  this.label.setLeft (this.icon.left + this.icon.width + 5);
  this.label.setTop (0);
  this.label.setHeight (this.height);
  this.label.setWidth (20);
  this.label.noclip = true;
  this.label.onresizecall = TreeNode_onResizeCallback;
  
  this.label.addListener ('mousedownCallback', this);
  this.label.addListener ('mouseupCallback', this);
  this.label.addListener ('doubleclickCallback', this);
  this.icon.addListener ('mousedownCallback', this);
  this.icon.addListener ('mouseupCallback', this);
  this.icon.addListener ('doubleclickCallback', this);
  this.collapser.addListener ('mousedownCallback', this);
}

function TreeNode_moreSetName (nm)
{
  this.key = nm; 
  this.label.setName (this.name + 'Lbl');
  if (this.bIconVisible)
    this.icon.setName (this.name + 'Icon');
  if (this.bTLineVisible)
    this.collapser.setName (this.name + 'Clpsr');
}

function TreeNode_setCaption (val)
{
  this.label.setValue (val);
}

function TreeNode_getCaption ()
{
  return (this.label.getValue ());
}

function TreeNode_moreSetBackgroundColor (col)
{
  this.label.setBackgroundColor (col);
  if (this.bTLineVisible)
    this.collapser.setBackgroundColor (col);
  if (this.bIconVisible)
    this.icon.setBackgroundColor (col);
}

function TreeNode_moreSetHeight (ht)
{
  this.label.setHeight (this.height);
  if (this.bTLineVisible)
    this.collapser.setTop (Math.round ((this.height-this.collapser.height)/2));
  if (this.bIconVisible)		
    this.icon.setTop (Math.round ((this.height-this.icon.height)/2));
}

function TreeNode_setStyle (iStyle)
{
  if (iStyle == Tree.STYLE_NONE) {
    this.bIconVisible = false;
    this.bTLineVisible = false;
    this.icon.destroy ();
    this.icon = null;
    this.collapser = null;
    this.label.setLeft (3);
  } else if (iStyle == Tree.STYLE_ICONS_ONLY) {
    this.bTLineVisible = false;
    this.collapser.destroy ();
    this.collapser = null;
    this.icon.setLeft (0);
    this.label.setLeft (this.icon.width + 3);
  } else if (iStyle == Tree.STYLE_TREE_LINES_ONLY) {
    this.bIconVisible = false;
    this.icon.destroy ();
    this.icon = null;
    this.label.setLeft (this.collapser.width + 3);
  }
}

function TreeNode_setSpanNormal (classId)
{
  this.spanNormal = classId;
  this.label.spanClass = classId;
}

function TreeNode_setSpanSelected (classId)
{
  this.spanSelected = classId;
}

function TreeNode_setIconNormal (iconName)
{
  this.iconNormal = iconName;
  if (this.bIconVisible && (! this.bSelected)) {
    var src = this.parentObj.getNodeIconSrc (iconName);
    this.icon.setSrc (src);
  }
}

function TreeNode_setIconSelected (iconName)
{
  this.iconSelected = iconName;
  if (this.bIconVisible && this.bSelected) {
    var src = this.parentObj.getNodeIconSrc (iconName);
    this.icon.setSrc (src);
  }
}

function TreeNode_setIcon (iconName)
{
  if (!this.icon) return;
  var src = this.parentObj.getNodeIconSrc (iconName);
  this.icon.setSrc (src);
}

function TreeNode_setTreeIcon (iconName)
{
  if (!this.collapser) return;
  this.treeIcon = iconName;
  var src = this.parentObj.getTreeIconSrc (iconName);
  this.collapser.setSrc (src);
}

function TreeNode_reverseTreeIcon ()
{
  if (!this.bTLineVisible) return;
  if (this.bRoot) return;
  if (this.treeIcon.charAt (0) == 'm')
    this.treeIcon = this.treeIcon.replace (/m/i, 'p');
  else
    this.treeIcon = this.treeIcon.replace (/p/i, 'm');
  var src = this.parentObj.getTreeIconSrc (this.treeIcon);
  this.collapser.setSrc (src);
}

function TreeNode_addNode (relative, relationship, key, text, iconNormal, iconSelected) 
{
  var left;
  if (this.parentObj.treeStyle == Tree.STYLE_NONE) {
    left = this.left + 5;
  } else if (this.parentObj.treeStyle == Tree.STYLE_ICONS_ONLY) {
    left = (this.left + this.icon.left + this.icon.width);
  } else if (this.parentObj.treeStyle == Tree.STYLE_TREE_LINES_ONLY) {
    if (this.bRoot)
      left = this.left + 5;
    else
      left = (this.left + this.collapser.left + this.collapser.width);
  } else if (this.parentObj.treeStyle == Tree.STYLE_TREE_LINES_AND_ICONS) {
    left = (this.left + this.icon.left + this.icon.width/2) -
    this.parentObj.treeIconWidth/2;
  }


  // get the tree node label width
  var textWidth = 0;

  if ( this.font )
  {
    textWidth = this.font.getTextWidth(text,true)  
  }

  if ( this.width < textWidth )
    this.width = textWidth;

  var tmpWidth = left + textWidth;

  if (tmpWidth > this.parentObj.width)
    this.parentObj.setWidth (tmpWidth + 15);
  
  var n = new TreeNode ();
  n.setDimensions(left,this.top + this.height,this.width,this.height);
  n.setParentObject(this.parentObj);
  n.setName(key);
  n.setIconNormal(iconNormal);
  n.setIconSelected(iconSelected);
  n.setSpanNormal(this.spanNormal);
  n.setSpanSelected(this.spanSelected);
  n.setBackgroundColor(this.backgroundColor);
  n.setReference(this.reference);
  n.setCaption(text);
  n.setToolTipText (text);
  if (!this.expanded) n.hide();
  n.setStyle(this.parentObj.treeStyle);
  
  if (relationship == Tree.AT_FIRST) {
    this.insertNode (0, n);
  } else if (relationship == Tree.AT_LAST) {
    this.insertNode (this.children.length, n);
  } else if (relationship == Tree.AT_NEXT) {
    this.insertNode (relative.index + 1, n);
  } else if (relationship == Tree.AT_PREVIOUS) {
    this.insertNode (relative.index, n);
  } else if (relationship == Tree.AT_CHILD) {
    this.insertNode (this.children.length, n);
  }

  if (this.launched) {
    n.createLayer (this.parentObj);
    this.parentObj.lastNodeBottom = n.parent.top + n.parent.height;
    this.expand();
    this.refresh();
    this.propogateStateChange();
    /*
    if (this.expanded) {
      this.expand();
      this.refresh ();
      //n.parent.fixTop();
      this.propogateStateChange ();
    } else {
      this.refresh();
      n.hide();
    }
    */
  }
  return n;
}

function TreeNode_removeNode (node)
{
  var prev = node.getPrevious ();
  var next = node.getNext ();
  if (this.launched) {
    if (this.bTLineVisible && node.isLastNode() && prev) {
      prev.treeLine = node.treeLine;
      prev.treeLine.setHeight (prev.top -(prev.parent.top + prev.parent.height/2));
      node.treeLine = null;
      delete node.treeLine;
    }
  }
  var index = node.index;
  var len = this.children.length;
  if (index == len-1) {
    this.children[index] = null;
    delete this.children[index];
  } else {
    for (var i=index; i<(len-1); i++) {
      this.children[i] = this.children[i+1];
      this.children[i].index = i;
    }
    this.children[len-1] = null;
    delete this.children[len-1];
  }
  this.children.length--;
  
  if (this.launched) {
    node.recursiveDelete ();
    this.parentObj.lastNodeBottom = node.top;
    if (next) {
      next.moveTo (next.left, this.parentObj.lastNodeBottom);
      if (next.treeLine) {
        next.treeLine.setTop (next.parent.top + next.parent.height);
        next.treeLine.setHeight (next.top - (next.parent.top + next.parent.height));
      }
      this.parentObj.lastNodeBottom += next.height;
      next.fixTop ();
      next.propogateStateChange ();
    } else {
      this.parentObj.lastNodeBottom = this.top + this.height;
      this.fixTop ();
      this.refresh ();
      this.propogateStateChange ();
    }
  }
}

function TreeNode_recursiveDelete ()
{
  if (this.launched) {
    this.hide ();
    if (ie4) {
      this.getHTMLObj().innerHTML = "";
      this.getHTMLObj().outerHTML = "";
    }
    if (this.treeLine)
      this.treeLine.hide ();
  }
  if (this.getChildren()) {
    for (var i=this.children.length-1; i>=0; ) {
      this.children[i].recursiveDelete ();
      this.children[i] = null;
      delete this.children[i];
      this.children.length--;
      i = this.children.length-1;
    }
  }
}

function TreeNode_insertNode (index, node)
{
  var len = this.children.length;
  
  node.setParent (this);
  
  if (index == len) {
    this.children[len] = node;
    this.children[len].index = len;
    return;
  }
  if (index <= 0) {
    index = 0;
  }
  for (var i=(len-1); i >= index; i--) {
    this.children[i+1] = this.children[i];
    this.children[i+1].index = (i+1);
  }
  this.children [index] = node;
  this.children[index].index = index;
}

function TreeNode_createLayer (par)
{	
  var tree = this.parentObj;
  //ToDo: Rework this later whent we inherit this from container;
  //this is set to bypass more lauch method call;
  this.bRuntimeCreation = true;

  this.runtimeRender (null, tree, tree.eventManager);
  this.setZIndex (this.zIndex);
  
  this.label.runtimeRender (null, this, tree.eventManager);
  this.label.setValue (this.label.caption);
  
  if (this.bIconVisible) {
    this.icon.runtimeRender (null, this, tree.eventManager);
  }
  if (this.bTLineVisible) {
    this.collapser.runtimeRender (null, this, tree.eventManager);
  }
  if (this.bTLineVisible && this.isLastNode ()) {
    if (this.getPrevious()) {
      var p = this.getPrevious ();
      this.treeLine = p.treeLine;
      this.treeLine.setHeight (this.top-(this.parent.top + this.parent.height));
      p.treeLine = null;
      delete p.treeLine;
    } else {
      this.createTreeLine ();
      this.treeLine.runtimeRender (null, this.parentObj, tree.eventManager);
      this.treeLine.setZIndex (this.treeLine.zIndex);
    }
  }
}

function TreeNode_createTreeLine ()
{
  this.treeLine = new Base ();
  this.treeLine.setName(this.name + 'tvLine');
  var tp = this.parent.top + this.parent.height/2
  this.treeLine.setDimensions(this.left + this.collapser.width/2 - 1, tp, 1, this.top - tp);
  this.treeLine.setBackgroundColor('#003D84');
  this.treeLine.setParentObject(this.parentObj);
  this.treeLine.setZIndex(1);
  this.treeLine.hide ();
}

function TreeNode_setAsRootNode ()
{
  this.bRoot = true;
  this.collapser = null;
  if (this.bIconVisible) {
    this.icon.setLeft (0);
    this.label.setLeft (this.icon.width + 3);
  } else
    this.label.setLeft (0);
}

function TreeNode_getChildren () 
{
  if (this.children.length == 0)
    return null;
  else
    return (this.children);
}

function TreeNode_getParent () 
{
  if (this.parent)
    return (this.parent);
  else
    return null;
}

function TreeNode_setParent (parent) 
{
  this.parent = parent;
}

function TreeNode_getRoot () 
{
  if (this.root) 
    return this.root;
  else
    return null;
}

function TreeNode_setRoot (root) 
{
  this.root = root;
}

function TreeNode_getPrevious ()
{
  var n = null;
  if (this.parent) {
    if (this.index)
      n = this.parent.children[this.index-1];
  }
  return n;
}

function TreeNode_getNext ()
{
  var n = null;
  if (this.isLastNode())
    return null;

  if (this.parent)
    n = this.parent.children[this.index+1];
  
  return n;
}

function TreeNode_isFirstNode ()
{
  if (this.index == 0) 
    return true;
  return false;
}

function TreeNode_isLastNode ()
{
  if (this.bRoot) return false;
  return (this.index == this.parent.children.length-1);
}

function TreeNode_setExpanded (bExpand) {
  this.expanded = bExpand;
  if (this.launched) {
    this.reverseTreeIcon ();
    if (this.expanded) {
      this.parentObj.lastNodeBottom = this.top + this.height;
      this.fixTop ();
    } else {
      for (var i=0; i<this.children.length; i++) {
        var n = this.children[i];
        if (n.treeLine)
          n.treeLine.hide ();
        n.recursiveHide ();
      }
    }
  }
}

function TreeNode_recursiveHide ()
{
  this.hide ();
  var len  = this.children.length;
  if (this.expanded) {
    for (var i=0; i<len; i++) {
      n = this.children[i];
      if (n.treeLine)
        n.treeLine.hide ();
      n.recursiveHide ();
    }
  }
}

function TreeNode_recursiveShow ()
{
  this.show ();
  if (this.expanded) {
    for (var i=0; i<this.children.length; i++) {
      n = this.children[i];
      n.recursiveShow ();
    }
  }
}

function TreeNode_expand () 
{
  this.setExpanded (true);
}

function TreeNode_collapse () 
{
  this.setExpanded (false);
}

function TreeNode_setSelected (bSel)
{
  if (bSel) {
    var col = this.parentObj.selectedBackColor;
    if (col)
      this.label.hilite (col);
    else
      this.label.hilite ('lightgrey');
    this.label.setSpanClass (this.spanSelected);
    this.label.setValue (this.label.getValue());
    if (this.iconSelected)
      this.setIcon (this.iconSelected);

    //Expand the tree if one of the child node is selected
    var ptNodeAry = new Array();
    var parentNode = null;
    for (var i=0,parentNode = this.getParent(); parentNode != null; i++,parentNode = parentNode.getParent()) {
      ptNodeAry[i] = parentNode;
    }
    for (var i=ptNodeAry.length-1; i>=0; i--) {
      parentNode = ptNodeAry[i];
      if (! parentNode.expanded) {
        parentNode.expand();
        parentNode.propogateStateChange();
        parentNode.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this);
      }
    }
  } else {
    if (this.bSelected) {
      this.label.unhilite ();
      this.label.setSpanClass (this.spanNormal);
      this.label.setValue (this.label.getValue());
      if (this.iconNormal)
        this.setIcon (this.iconNormal);
    }
  }
  this.bSelected = bSel;
}

function TreeNode_isSelected ()
{
  return (this.bSelected);
}

function TreeNode_ensureVisible ()
{
  
}

function TreeNode_setUnSatisfied (bVal)
{
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.setIconNormal ('closed-unsatisfied');
    this.setIconSelected ('open-unsatisfied');
    if (this.bIconVisible && ie4)
      this.icon.setToolTipText (this.getCaption() + " " + self._unsatToolTip);
  } else {
    this.setIconNormal ('closed');
    this.setIconSelected ('open');
    if (this.bIconVisible && ie4)
      this.icon.setToolTipText (this.getCaption());
  }
}

function TreeNode_moreCSS ()
{
  var CSSStr = "";
  
  CSSStr += this.label.generateCSS () + '\n';
  if (this.bIconVisible)
    CSSStr += this.icon.generateCSS () + '\n';
  
  if (this.bTLineVisible && (this.isLastNode()) && (!this.bRoot)) {
    this.createTreeLine ();
    CSSStr += this.treeLine.generateCSS () + '\n';
  }  
  if (this.bTLineVisible && (! this.bRoot))
    CSSStr += this.collapser.generateCSS () + '\n';
  
  for (var i=0; i<this.children.length; i++)
    CSSStr += this.children[i].generateCSS ();
  
  return CSSStr;
}

function TreeNode_render () 
{
  var sBuffer = '';
  
  sBuffer += '<DIV ';
  if (this.objId != '')
    sBuffer += 'ID=' + this.objId;
  
  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  sBuffer += '>' + '\n';

  if (this.bTLineVisible && (! this.bRoot))
    sBuffer += this.collapser.render () + '\n';
  if (this.bIconVisible)
    sBuffer += this.icon.render () + '\n';
  sBuffer += this.label.render () + '\n';
  sBuffer += '</DIV>' + '\n';

  if (this.treeLine)
    sBuffer += this.treeLine.render () + '\n';
  
  for (var i=0; i<this.children.length; i++)
    sBuffer += this.children[i].render ();
  
  return sBuffer;
}

function TreeNode_moreLaunch (em)
{
  //ToDo: rework this later;
  if (this.bRuntimeCreation) return;

  if (this.bTLineVisible && (! this.bRoot))
    this.collapser.launch (em);
  
  if (this.bIconVisible)
    this.icon.launch (em);
  
  this.label.launch (em);
  this.label.setCaption (this.label.caption);
  
  if (this.treeLine)
    this.treeLine.launch ();
  
  for (var i=0; i<this.children.length; i++)
    this.children[i].launch (em);
}

function TreeNode_fixTop ()
{
  if (this.children.length) {
    if (this.expanded) {
      var childs = this.children;
      var len = childs.length;
      for (var i=0; i<len; i++) {
        var n = childs [i];
        if (n.isAvailable()) {
          n.moveTo (n.getLeft(), this.parentObj.lastNodeBottom);
          n.show ();
          if (n.treeLine) {
            n.treeLine.setTop (n.parent.top + n.parent.height);
            n.treeLine.setHeight (n.top - (n.parent.top + n.parent.height));
            n.treeLine.show();
          }
          this.parentObj.lastNodeBottom += n.getHeight ();
          n.refresh ();
          if (n.expanded)
            n.fixTop ();
        } else {
          //check if this node has treeLine then it should move;
          //to previous available node;
          if (n.treeLine) {
            var prv = n.getPreviousAvailableNode();
            if (prv) {
              prv.treeLine = n.treeLine;
              n.treeLine = null;
              prv.treeLine.setTop (prv.parent.top + prv.parent.height);
              prv.treeLine.setHeight (prv.top - (prv.parent.top + prv.parent.height));
              prv.treeLine.show();
            } else
              n.treeLine.hide();
          }
          n.hide();
        }
      }
    }
  }
  if (this.parentObj.lastNodeBottom > this.parentObj.height)
    this.parentObj.setHeight (this.parentObj.lastNodeBottom + 30);
}

function TreeNode_refresh ()
{
  if (this.hasAvailableChildren()) {
    if (this.getNextAvailableNode())
      key = 'node';
    else
      key = 'last';
    if (this.expanded) 
      key = 'm' + key;
    else
      key = 'p' + key;
  } else if (this.getNextAvailableNode ())
    key = 'tnode';
  else
    key = 'last';
  this.setTreeIcon (key);
}

function TreeNode_selected(curNode,selNode)
{
  if(selNode == null)
	return false;

  var parNode = selNode.parent;
  var found = true;
  
  if(parNode == null)
    return false;

  if( curNode.getName() == parNode.getName())
      found = true;
  else {
    found = false;
    if(this.selected(curNode,parNode))
      return true;
  }
  return found;
}

function TreeNode_mousedownCallback (e, src) 
{
  if ((src == this.label) || (src == this.icon)) {
    return (this.notifyListeners ('mousedownCallback', e, this));
  } else if (src == this.collapser) {
    if (this.expanded) {
      this.collapse ();
      this.propogateStateChange ();
      if (this.selected(this, this.parentObj.selectedNode)) {
        if (self.ub)
          self.ub.postClientMessage("<click rtid='" + self.ub.formatRuntimeId(this.getName()) +  "'/>");
        this.parentObj.setSelectedNode(this);
      }
      return (this.notifyListeners ('treeCollapseCallback', 'treeCollapseEvent', this));
    } else {
      this.expand ();
      this.propogateStateChange ();
      return (this.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this));
    }
  }
}

function TreeNode_mouseupCallback (e, src)
{
  return (this.notifyListeners ('mouseupCallback', e, this ));
}

function TreeNode_doubleclickCallback (e, eSrc)
{
  if (this.expanded) {
    this.collapse ();
    this.propogateStateChange ();
    return (this.notifyListeners ('treeCollapseCallback', 'treeCollapseEvent', this));
  } else {
    this.expand ();
    this.propogateStateChange ();
    return (this.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this ));
  }
}

function TreeNode_propogateStateChange ()
{
  if (!this.expanded)
    this.parentObj.lastNodeBottom = this.getTop() + this.getHeight ();
  if (this.bRoot) return;
  var n = this.getNextAvailableNode ();
  if (n) {
    n.moveTo (n.getLeft(), this.parentObj.lastNodeBottom);
    if (n.treeLine) {
      n.treeLine.setTop (n.parent.top + n.parent.height);
      n.treeLine.setHeight (n.top - (n.parent.top + n.parent.height));
      n.treeLine.show();
    }
    this.parentObj.lastNodeBottom += n.getHeight ();
    n.fixTop ();
    n.propogateStateChange ();
  } else if (this.getParent())
    this.getParent().propogateStateChange ();
  else return;
}

function TreeNode_onResizeCallback (obj)
{
  var wd = 0;
  var node = obj.getParentObject ();
  if (node.collapser)
    wd += node.collapser.width;
  if (node.icon)
    wd += node.icon.width + 5;
  wd += obj.width;
  if (wd > node.width)
    node.setWidth (wd);
  
  if (node.left + node.width > node.parentObj.width)
    node.parentObj.setWidth (node.left + node.width + 15);
}

function TreeNode_moreDestroy ()
{
  if (this.icon) {
    this.icon.destroy ();
    this.icon = null;
    delete this.icon;
  }
  if (this.collapser) {
    this.collapser.destroy ();
    this.collapser = null;
    delete this.collapser;
  }
  if (this.label) {
    this.label.destroy ();
    this.label = null;
    delete this.label;
  }
  if (this.treeLine) {
    this.treeLine.destroy ();
    this.treeLine = null;
    delete this.treeLine;
  }
}

function TreeNode_setAvailability (bAvail)
{
  if (bAvail != null) {
    this.bAvailable = bAvail;
    if (this.launched) {
      if (! bAvail)
        this.recursiveHide();
      if (this.bRoot) {
        this.parentObj.lastNodeBottom = this.top + this.height; 
        this.fixTop ();
      } else {
        var next = this.parent.getNext();
        //if this is the last available node and if it does not have treeline;
        //get back the tree line object;
        if (! this.getNextAvailableNode()) {
          if (! this.treeLine) {
            var prev = this.getPreviousTreeLineNode();
            if ( prev != null){
              this.treeLine = prev.treeLine;
              prev.treeLine = null;
            } else {
              var nxtTLineNode = this.getNextTreeLineNode();
              //no need to check for null
              this.treeLine = nxtTLineNode.treeLine;
              nxtTLineNode.treeLine = null;
            }
          }
        }
        this.parentObj.lastNodeBottom = this.parent.top + this.parent.height;
        if ( this.parent.isVisible() ){
          this.parent.fixTop();
          this.parent.refresh();
          this.parent.propogateStateChange();
        }
        if (this.parent.bRoot && next)
          next.propogateStateChange();
      }
    }
  }
}

function TreeNode_isAvailable ()
{
  return this.bAvailable;
}

function TreeNode_getPreviousAvailableNode ()
{
  var prv = this.getPrevious();
  while (prv) {
    if (prv.isAvailable())
      return prv;
    else
      prv = prv.getPrevious();
  }
  return null;
}

function TreeNode_getNextAvailableNode ()
{
  var next = this.getNext();
  while (next) {
    if (next.isAvailable())
      return next;
    else
      next = next.getNext();
  }
  return null;
}

function TreeNode_hasAvailableChildren ()
{
  if (this.children.length) {
    var len = this.children.length;
    for (var i=0; i<len; i++) {
      if (this.children[i].isAvailable ())
        return true;
    }
  }
  return false;
}

//returns the node that has the tree line associated with it currently
function TreeNode_getPreviousTreeLineNode ()
{
  var prv = this.getPrevious();
  while (prv) {
    if (prv.treeLine)
      return prv;
    else
      prv = prv.getPrevious();
  }
  return null;
}

function TreeNode_getNextTreeLineNode ()
{
  var nxt = this.getNext();
  while (nxt) {
    if (nxt.treeLine)
      return nxt;
    else
      nxt = nxt.getNext();
  }
}

function TreeNode_setToolTipText(toolTipText)
{
  if (this.icon)
    this.icon.setToolTipText (toolTipText);
}

function TreeNode_getNextVisibleNode(stIndex)
{
  if (this.expanded && this.children.length) {
    var len = this.children.length;
    if (stIndex == null)
      var stIndex = 0;
    for (var i=stIndex; i<len; i++) {
      if (this.children[i].isAvailable ())
        return this.children[i];
    }
  }
  if (this.parent)
    return this.parent.getNextVisibleNode(this.index+1);
}

function TreeNode_getPreviousVisibleNode()
{
  var n = this.getPreviousAvailableNode();
  if (n) {
    while (true) {
      if (n.expanded && n.children.length) {
        var len = n.children.length;
        for (var i=len-1; i>=0; i--) {
          if (n.children[i].isAvailable()) {
            n = n.children[i];
            break;
          }
        }
      } else
        return n;
    }
  }
  if (this.parent)
    return this.parent;

  return null;
}


//Extend from Base
HTMLHelper.importPrototypes(TreeNode, Base);

TreeNode.prototype.constructor = TreeNode;
TreeNode.prototype.setCaption = TreeNode_setCaption;
TreeNode.prototype.getCaption = TreeNode_getCaption;
TreeNode.prototype.setSpanNormal = TreeNode_setSpanNormal;
TreeNode.prototype.setSpanSelected = TreeNode_setSpanSelected;
TreeNode.prototype.setIconNormal = TreeNode_setIconNormal;
TreeNode.prototype.setIconSelected = TreeNode_setIconSelected;
TreeNode.prototype.setStyle = TreeNode_setStyle;

TreeNode.prototype.addNode = TreeNode_addNode;
TreeNode.prototype.removeNode = TreeNode_removeNode;
TreeNode.prototype.getChildren = TreeNode_getChildren;
TreeNode.prototype.getParent = TreeNode_getParent;
TreeNode.prototype.setParent = TreeNode_setParent;
TreeNode.prototype.getRoot = TreeNode_getRoot;
TreeNode.prototype.setRoot = TreeNode_setRoot;
TreeNode.prototype.getPrevious = TreeNode_getPrevious;
TreeNode.prototype.getNext = TreeNode_getNext;
TreeNode.prototype.isFirstNode = TreeNode_isFirstNode;
TreeNode.prototype.isLastNode = TreeNode_isLastNode;
TreeNode.prototype.setExpanded = TreeNode_setExpanded;
TreeNode.prototype.expand = TreeNode_expand;
TreeNode.prototype.collapse = TreeNode_collapse;
TreeNode.prototype.ensureVisible = TreeNode_ensureVisible;
TreeNode.prototype.setSelected = TreeNode_setSelected;
TreeNode.prototype.isSelected = TreeNode_isSelected;
TreeNode.prototype.selected = TreeNode_selected;
TreeNode.prototype.setToolTipText = TreeNode_setToolTipText;
TreeNode.prototype.setUnSatisfied = TreeNode_setUnSatisfied;
TreeNode.prototype.setAvailability = TreeNode_setAvailability;
TreeNode.prototype.isAvailable = TreeNode_isAvailable;

TreeNode.prototype.moreCSS  = TreeNode_moreCSS;
TreeNode.prototype.render  = TreeNode_render;
TreeNode.prototype.moreLaunch = TreeNode_moreLaunch;
TreeNode.prototype.moreDestroy = TreeNode_moreDestroy;

//private methods
TreeNode.prototype.moreSetName = TreeNode_moreSetName;
TreeNode.prototype.moreSetHeight = TreeNode_moreSetHeight;
TreeNode.prototype.moreSetBackgroundColor = TreeNode_moreSetBackgroundColor;
TreeNode.prototype.initializeControl = TreeNode_initializeControl;
TreeNode.prototype.setIcon = TreeNode_setIcon;
TreeNode.prototype.setTreeIcon = TreeNode_setTreeIcon;
TreeNode.prototype.insertNode = TreeNode_insertNode;
TreeNode.prototype.fixTop = TreeNode_fixTop;
TreeNode.prototype.refresh = TreeNode_refresh;
TreeNode.prototype.reverseTreeIcon = TreeNode_reverseTreeIcon;
TreeNode.prototype.recursiveHide = TreeNode_recursiveHide;
TreeNode.prototype.recursiveShow = TreeNode_recursiveShow;
TreeNode.prototype.propogateStateChange = TreeNode_propogateStateChange;
TreeNode.prototype.createLayer = TreeNode_createLayer;
TreeNode.prototype.createTreeLine = TreeNode_createTreeLine;
TreeNode.prototype.recursiveDelete = TreeNode_recursiveDelete;
TreeNode.prototype.getPreviousAvailableNode = TreeNode_getPreviousAvailableNode;
TreeNode.prototype.getNextAvailableNode = TreeNode_getNextAvailableNode;
TreeNode.prototype.hasAvailableChildren = TreeNode_hasAvailableChildren;
TreeNode.prototype.getPreviousTreeLineNode = TreeNode_getPreviousTreeLineNode;
TreeNode.prototype.getNextTreeLineNode = TreeNode_getNextTreeLineNode;
TreeNode.prototype.getNextVisibleNode = TreeNode_getNextVisibleNode;
TreeNode.prototype.getPreviousVisibleNode = TreeNode_getPreviousVisibleNode;

//Event handlers
TreeNode.prototype.mousedownCallback = TreeNode_mousedownCallback;
TreeNode.prototype.mouseupCallback = TreeNode_mouseupCallback;
TreeNode.prototype.doubleclickCallback = TreeNode_doubleclickCallback;
