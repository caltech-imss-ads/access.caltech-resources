//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawconst.js                       |
//  |                                                |
//  | Description: Constants used in web time        |
//  |                                                |
//  | History:     07-Jul-99 created by Akita Ling   |
//  |                                                |
//  +================================================+
/* $Header: pawconst.js 115.7 2001/03/23 17:40:38 pkm ship      $ */

// Constants for Toolbar buttons
var C_strMENU 		= "MENU";
var C_strPRINT 		= "PRINT";
var C_strRELOAD 	= "RELOAD";
var C_strSTOP 		= "STOP";
var C_strPREFERENCES 	= "PREFERENCES";
var C_strHELP 		= "HELP"; 	

// Constants for MODE for Primary Windows
var C_strSAVE 			= "SAVE";
var C_strSUBMIT 		= "SUBMIT";
var C_strVIEW_ONLY 		= "VIEW ONLY";
var C_strFINAL_REVIEW 		= "FINAL REVIEW";
var C_strENTER_HOURS 		= "LINES";
var C_strUPLOAD 		= "UPLOAD";
var C_strREVERSE      		= 'REVERSE';
var C_strREVERSAL     		= 'REVERSAL'; // To be merged with C_strREVERSE. 
var C_strAUDIT			= "AUDIT";
var C_strUNEXPECTED_ERROR 	= "UNEXPECTED_ERROR";

// Constants for MODE for Secondary Windows
var C_strEXPECTED_ERRORS	= "ERRORS";
var C_strGENERAL_ERROR  	= "GENERAL_ERROR";
var C_strDETAILS		= "DETAILS";
var C_strCALENDAR		= "CALENDAR";

// Constants for Audit Type
var C_strLATE_ENTRY             = "LATE ENTRY";
var C_strCHANGE_REVERSAL        = "REVERSAL";
var C_strCHANGE_DELETION        = "DELETION";
var C_strCHANGE_CURRENT         = "CHANGE-CURRENT";
var C_strCHANGE_HISTORY         = "CHANGE-HISTORY";

// Constants for ACTION
var C_strMODIFY 		= "MODIFY";        // Modify action from Save Confirmation
var C_strHISTORY 		= "HISTORY";
var C_strNEW_TIMECARD 		= "NEW";
var C_strEDIT	      		= 'EDIT';
var C_strCOPY	      		= 'COPY';          
var C_strMODIFYMODIFY 		= "MODIFY_MODIFY"; // Modify action from Modify Timecards
var C_strMODIFYCOPY		= 'MODIFY_COPY';   // Copy action from Modify Timecards
var C_strMODIFYDELETE           = 'DELETE';   // Delete action from Modify Timecards
var C_strMODIFYREVERSE		= 'MODIFY_REVERSE';   // Modify action from Save Confirmation
var C_strMODIFYMODIFYREVERSE	= 'MODIFY_MODIFY_REVERSE';   // Modify action from Modify Timecards
var C_strHISTORYCOPY   		= 'HISTORY_COPY';  // Copy action from Timecard History
var C_strNEW  	      		= 'NEW';

// Constants for Expected Errors -- newly added for 11i errors
var C_strGENERAL      = 'GENERAL'; 
var C_strSETUP        = 'SETUP';   
var C_strApprover     = 'Overriding Approver';
var C_strWeekEnding   = 'Week Ending Date';
var C_strProject      = 'Project Number';
var C_strTask	      = 'Task Number';
var C_strType	      = 'Expenditure Type';
var C_strAlias	      = 'Alias';

var C_strHEADER       = 'HEADER';
var C_strLINES        = 'LINES';
var C_strCELL         = 'CELL';

var C_iSUNDAY	  = 0;
var C_iMONDAY	  = 1;
var C_iTUESDAY	  = 2;
var C_iWEDNESDAY  = 3;
var C_iTHURSDAY	  = 4;
var C_iFRIDAY	  = 5;
var C_iSATURDAY	  = 6;

var C_iJAN        = 0;
var C_iFEB        = 1;
var C_iMAR        = 2;
var C_iAPR        = 3;
var C_iMAY        = 4;
var C_iJUN        = 5;
var C_iJUL        = 6;
var C_iAUG        = 7;
var C_iSEP        = 8;
var C_iOCT        = 9;
var C_iNOV        = 10;
var C_iDEC        = 11;

var C_iWEEKDAYS   = 7;
var C_iMONTHS     = 12;

var C_iMinAliasLines = 4;
var C_iTimecardLinesIncrement = 4;

var C_sYES = 'Y'; // temporary
var C_sNO  = 'N'; // temporary
var C_strYES = 'Y';
var C_strNO  = 'N';
var C_strFULL  = 'F';

// Constants for the different states of a singleentryline during reversal
var C_strNEW_CHECK_YES = 'NEW_CHECK_YES';
var C_strNEW_CHECK_NO = 'NEW_CHECK_NO';
var C_strNEW_UNCHECK_YES = 'NEW_UNCHECK_YES';
var C_strNEW_UNCHECK_NO = 'NEW_UNCHECK_NO';
