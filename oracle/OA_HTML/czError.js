/* $Header: czError.js 115.8 2001/06/14 15:33:56 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function Error()
{
  return null;
}

function Error_processError(str)
{
  if(!self._DEBUG) 
    return;
  
  if(!self._TRACE) {
    str = arguments.caller.callee.name +'>>'+str;
  } else {		
    var strSource="";
    var args = arguments;
    while(args.caller)
    {
      strSource =args.caller.callee.name +' >> '+strSource;
      args = args.caller;
    }
    if(strSource)
      str = strSource + '>>'+str;
  }
  alert(str);
}

Error.processError = Error_processError;

