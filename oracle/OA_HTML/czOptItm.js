/* $Header: czOptItm.js 115.12 2001/06/14 15:34:18 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function OptionItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'OptionItem';
}

function OptionItem_getColumnDescriptors (bUseImg, bNoIcon)
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {
    var index = 0;  
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    if (! bNoIcon) {
      this.columnDescriptors[index] = new ColumnDescriptor ('state', 'icon', index, 16, 20, 2, 0, 'top', 'left', true, false);
      index++;
    }
    if (bUseImg) {
      //use image for description;
      this.columnDescriptors[index] = new ColumnDescriptor ('descImg', 'descImg', index, 22, 50, 0, 0, 'top', 'left', true, true);
      
    } else {
      //display description;
      this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, 22, 50, 1, 2, 'top', 'left', true, true);
    }
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

//Extend from ListItem
HTMLHelper.importPrototypes(OptionItem, LogicItem);

// LogicItem overrides
OptionItem.prototype.getColumnDescriptors = OptionItem_getColumnDescriptors;

