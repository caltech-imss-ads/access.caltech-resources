/* $Header: czList.js 115.36 2001/07/13 09:53:20 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function List ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.cells = new Array();
  this.activeItemsList = new Array();
  this.activeItemsListScroll = new Array();
  this.unWantedItemsList = new Array();
  this.cellsBuilt = false;
  this.className = 'List';
  this.itemBackgroundColor = 'white';
  this.backgroundColor = this.itemBackgroundColor;
  this.activeBackColor = 'lightgrey';
  this.selectedBackColor = 'gray';
  this.cellRenderClass = 'ListItem';
  this.itemHt = 22;
  this.visibleRowCount = 0;
  this.mode = List.MODE_STANDARD;
  this.bLastCellClipped = false;
  this.borderOffset = 1;
  this.sclBarWd = 15;

  this.setModel (new ListModel (ListModel.SELECT_MULTI));

  // currently viewed model item indices;
  this.firstVisibleIndex = 0;
  this.lastVisibleIndex = 0;
}

function List_setModel (listModel)
{
  this.listModel = listModel;
  // register for events;
  this.listModel.addListener ('propertyChangeCallback', this);
  this.listModel.addListener ('objectAddedCallback', this);
  this.listModel.addListener ('objectRemovedCallback', this);
}

function List_buildControl ()
{
  if (this.built) return;

  this.built = true;
  var border = 'black';
  if ( this.noBorder == true)
    border = 'white';
  this.createBorder(this.borderOffset, border);
  this.enableKeyBoardEvents();
}

function List_onLaunchCall()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.updateDone();
  }
}

function List_refreshActiveList()
{
  for (var i=0; i<this.activeItemsList.length; i++)
  {
    this.activeItemsList[i]=this.activeItemsList[i+1];
  }
  this.activeItemsList[i-1]=null;
  this.activeItemsList.length--;
}

function List_updateDone()
{
    Utils.clearCollection(this.activeItemsList);
    Utils.clearCollection(this.activeItemsListScroll);
    if(this.unWantedItemsList.length>0)
    {
      Utils.clearCollection(this.unWantedItemsList);
      this.unWantedWas = true;
    }
    var curActiveItem=null;
  
    for(var i=0; i<this.listModel.items.length; i++)
    {
      curActiveItem=this.listModel.items[i]; 
      if(curActiveItem.getVisibility()==true)
      {
        this.activeItemsList[this.activeItemsList.length]=curActiveItem;
        this.activeItemsListScroll[this.activeItemsListScroll.length]=curActiveItem;
      }else
        this.unWantedItemsList[this.unWantedItemsList.length]=curActiveItem;
    }

    if(this.unWantedItemsList.length>0) 
    {
      this.noChange = false;
      this.needTotalUpdate = false;
    }
    else if(this.activeItemsListScroll.length == this.listModel.items.length && this.unWantedWas)
    {
      this.noChange = true;
      this.needTotalUpdate = true;
    }
    else
    {
      this.noChange = false;
      this.needTotalUpdate = false;
    }
    if(this.activeItemsListScroll.length == this.listModel.items.length && this.needTotalUpdate)
    {
      this.reArrangeItems();
      this.needTotalUpdate = false;
      this.unWantedWas = false;
    }
    else if (this.unWantedItemsList.length > 0 && !this.noChange)
    {
      this.arrangeItems();
      this.noChange=true;
    }
} 

function List_reArrangeItems()
{
  var vScroll = this.getVerticalScrollBarVisibility();
  var visRow = this.visibleRowCount;
  var activeRowCount = this.activeItemsList.length;
  var firstItem = false;

  if(this.activeItemsList.length == this.listModel.items.length)
  {
    for (var i=0; i<this.cells.length; i++)
    {
      if(this.activeItemsList.length>0)
      {
        for(var j=0; j<this.activeItemsList.length; j++)
        {
          var curActiveItem = this.activeItemsList[j];
          this.cells[i].setModelItem(curActiveItem);
          this.refreshActiveList();
          break;
        }
      } else {
         break;
      }
    }
  }
  if(this.getVerticalScrollBarVisibility())
  {
    this.setVerticalScrollBarVisibility(true);  
    if(this.activeItemsListScroll.length == this.listModel.items.length && this.vScroll){
      this.firstVisibleIndex=0;
      var maxVal = (Math.ceil(this.listModel.items.length-this.visibleRowCount)>0)?Math.ceil(this.listModel.items.length-this.visibleRowCount):1;
      this.vScroll.setValues(0,0,maxVal,1,this.visibleRowCount,false);
    }
  }
}

function List_arrangeItems()
{
  var vScroll = this.getVerticalScrollBarVisibility();
  var visRow = this.visibleRowCount;
  var activeRowCount = this.activeItemsList.length;
  var firstItem = false;

  for (var i=0; i<this.cells.length;i++)
  {
    if(this.activeItemsList.length>0)
    {
      for(var j=0; j<this.activeItemsList.length; j++)
      {
        var curActiveItem = this.activeItemsList[j];
        this.cells[i].setModelItem(curActiveItem);
        this.refreshActiveList();
        break;
      }
    }
    else
    {
       this.cells[i].setModelItem(null);
    }
  }

  if(this.getVerticalScrollBarVisibility())
  {

    if(this.activeItemsListScroll.length > this.unWantedItemsList.length)
    {
      if(this.activeItemsListScroll.length < this.visibleRowCount)
        this.setVerticalScrollBarVisibility(false);
      else
        this.setVerticalScrollBarVisibility(true);
    } else
        this.setVerticalScrollBarVisibility(false);

    if(this.activeItemsListScroll.length > this.unWantedItemsList.length && this.vScroll)
    {
      this.firstVisibleIndex=0;
      var maxVal = (Math.ceil(this.activeItemsListScroll.length-this.visibleRowCount)>0)?Math.ceil(this.activeItemsListScroll.length-this.visibleRowCount):1;
      this.vScroll.setValues(0,0,maxVal,1,this.visibleRowCount,false);
    }
  }
}

function List_createScrollBar ()
{
  var sclBarWd = this.sclBarWd;
  // add vertical scrollbar;
  var scroll = this.vScroll = new ScrollBar();
  scroll.setName (this.name + 'VScr');
  scroll.setParentObject (this);
  scroll.setDimensions (this.width-sclBarWd, 0, sclBarWd, this.height);
  scroll.dir = 'v';
  scroll.setZIndex (9999);
  // vertical;
  if (this.listModel.getItemCount() > 0) {
    scroll.setMaxValue (this.listModel.getItemCount() - this.visibleRowCount);
  }
  scroll.setPageSize (this.visibleRowCount);
  scroll.setEventManager (this.eventManager);
  
  //add horizontal scrollbar;
  scroll = this.hScroll = new ScrollBar();
  scroll.setName (this.name + 'HScr');
  scroll.setDimensions (0, this.height-sclBarWd, this.width, sclBarWd);
  scroll.setParentObject (this);
  scroll.dir = 'h';
  scroll.setZIndex (9999);
  scroll.setEventManager (this.eventManager);
  //add scroll bar listeners;
  this.vScroll.addListener ('scrollbarCallback', this);
  this.hScroll.addListener ('scrollbarCallback', this);

  if (this.launched) {
    this.vScroll.runtimeRender (null, this, this.eventManager);
    this.vScroll.setZIndex (9999);
    this.hScroll.runtimeRender (null, this, this.eventManager);
    this.hScroll.setZIndex (9999);
  }
}

function List_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
      //if vertical scroll bar is visible do a hit test on it;
      if (this.vScroll && this.vScroll.isVisible()) {
        if (this.isInside (xpos, ypos, absTop+this.vScroll.top, rt, bt, absLeft+this.vScroll.left))
          return (this.vScroll.box.objId);
      }
      //if horizontal scroll bar is visible do a hit test on it;
      if (this.hScroll && this.hScroll.isVisible()) {
        if (this.isInside (xpos, ypos, absTop+this.hScroll.top, rt, bt, absLeft+this.hScroll.left))
          return (this.hScroll.box.objId);
      }
      //go thru cells and do hit test;
      for (var i=0; i < this.cells.length; i++) {
        var objId = this.cells[i].hitTest(e);
        if (objId)
          return objId;
      }
    }
  }
  return (null);
}

function List_getCellWithModelItem (modelItem)
{
  for (var i = 0; i<this.cells.length; i++) {
    var cell = this.cells[i];
    if (cell.modelItem == modelItem) return cell;
  }
  return null;
}

function List_setCellRenderClass (cls)
{
  if (!this.launched) {
    this.cellRenderClass = cls;
  }
}

function List_getCellRenderClass ()
{
  return this.cellRenderClass;
}

function List_scrollbarCallback (scr)
{
  if (this.cellsBuilt) {
    if (scr.dir == 'v') { 
      //vertical;
      //we could prevent same index rerendering,  but that fools the;
      //fix for bumping up the last modelItem for visibility;
      if (this.launched) {
        var scrVal = scr.getValue();
        this.setFirstVisibleItem (this.listModel.getItemAtIndex(scrVal));
      }
    } else { 
      //horizontal;
      this.adjustIndent (-scr.getValue());
    }
  }
}

function List_adjustIndent (pos) 
{
  var maxIndex = this.cells.length;
  for (var i = 0; i < maxIndex; i++) {
    this.cells[i].setLeft (pos);
  }
}

function List_setFirstVisibleItem (item)
{
  var i = 0;
  var prevCell = null;
  var curCell = null;
  var lastCell = null;
  var reorderedCells = new Array();
  var index = 0;
  var modelLength = 0;
  var lastCellClipped = false;
  
  //grab the last view cell and set its property last cell boolean to false;
  this.cells[this.visibleRowCount-1].bLastCell = false;

  //grab the length of the model,  so we can use it to bounds check in the algorithm;
  if(this.activeItemsListScroll.length>0)
    modelLength=this.activeItemsListScroll.length;
  else
    if (this.listModel) {
      modelLength = this.listModel.getItemCount();
  }
  //if item is an integer,  we will scroll by that amount positive or negative;
  //if item is an object,  we will scroll to that objects index;
  if (isNaN(item)) {
    index = item.index;
  } else {
    index = this.firstVisibleIndex + item;
  }
  var offset = index - this.firstVisibleIndex;
  if (offset > 0 && offset < this.visibleRowCount) {
    //scroll down;
    //shift up all pre-rendered items;
    var j = 0;
    for (i = offset; i <= this.visibleRowCount; i++) {
	curCell = this.cells[j];
      // recover the item height for a clipped cell;
      if (curCell.listBorderClipped) {
        curCell.setHeight (this.itemHt);
        curCell.listBorderClipped = false;
      }
      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[this.firstVisibleIndex+i]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(this.firstVisibleIndex+i));
        lastCell = curCell;
      } else {
        curCell.setModelItem (null);
      }
	j++;
    }
    
    //render whats left;
    prevCell = curCell;	
    var adjust = index + (this.visibleRowCount - offset);
    for (i = 0; i < offset; i++) {
     	curCell = this.cells[this.cells.length-1];

      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[adjust + i]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(adjust + i));
        lastCell = curCell;
      } else {
        curCell.setModelItem (null);
      }
    }  
  } else if (offset < 0 && Math.abs(offset) < this.visibleRowCount) {
    offset = Math.abs(offset);
    //scroll up;
    var j = 0;
    var adjust = (this.visibleRowCount - offset);
    for (i = 0; i < this.visibleRowCount; i++) {
      curCell = this.cells[i];
      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[index+j]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(index+j));
      } else {
        curCell.setModelItem (null);
      }
      // recover the item height for a clipped cell;
      if (curCell.listBorderClipped) {
        curCell.setHeight (this.itemHt);
        curCell.listBorderClipped = false;
      }			
      j++;
    }
  } else {
    //just render everything...;
    for (i = 0; (i < this.visibleRowCount) && (i < modelLength); i++) {
      curCell = this.cells[i];	
      if(this.activeItemsListScroll.length>0)
        curCell.setModelItem (this.activeItemsListScroll[i + index]);
      else
        curCell.setModelItem (this.listModel.getItemAtIndex(i + index));
    }
  }
  this.firstVisibleIndex = index;
  if (curCell.modelItem != null) {
    this.lastVisibleIndex = curCell.modelItem.index;
    this.adjustLastCellHeight ();
  } else {
    this.lastVisibleIndex = 0;
  }
  
  //if the horizontal bar is in the way,  move up another cell;
  if (isNaN(item)) { 
    //no adjustment in this case;
    if ((this.lastVisibleIndex == (modelLength-1))  &&  (this.firstVisibleIndex != 0) &&
        ((this.hScroll.isVisible() && this.bLastCellClipped ) || lastCellClipped || this.bLastCellClipped)) {
      this.setFirstVisibleItem (1); //scroll down 1
    }
  }
}

function List_updateScrollBarVisibility()
{
  if ((! this.vScroll) && (! this.hScroll)) {
    if (this.getVerticalScrollBarVisibility() || this.getHorizontalScrollBarVisibility()) {
      //create scroll bar;
      this.createScrollBar ();
    }
  }
  if (this.vScroll && this.hScroll) {
    var bDrawnCorner = (this.vScroll.cornerLength > 0);
    //vertical;
    if (this.getVerticalScrollBarVisibility()) {
      this.vScroll.show();
    } else {
      this.vScroll.hide();
    }
    //horizontal;
    if (this.getHorizontalScrollBarVisibility()) {
      this.hScroll.show();
    } else {
      this.hScroll.hide();
    }
    if (!bDrawnCorner && (this.hScroll.isVisible() && this.vScroll.isVisible())) {
      this.vScroll.drawCorner (this.hScroll.height);
      this.hScroll.setWidth (this.width - this.vScroll.width);
    } else if (bDrawnCorner && (! this.hScroll.isVisible())) {
      this.vScroll.drawCorner (0);
    }
  }
}

function List_updateVerticalScrollValues () {
  if (this.listModel.getItemCount() < this.visibleRowCount) 
    return;
  if (this.vScroll) {
    this.vScroll.setMaxValue (this.listModel.getItemCount() - this.visibleRowCount);
    if (this.cellsBuilt) {	
      this.vScroll.setPageSize (this.visibleRowCount);
    }
  }
}

function List_updateHorizontalScrollValues () {
  if (this.vScroll && this.hScroll) {
    var wd = (this.vScroll.isVisible()) ? this.vScroll.width : 0;
    this.hScroll.setMaxValue ((Math.abs(this.cells[0].width - this.width) + wd));
  }
}

function List_setVerticalScrollBarVisibility(show)
{
  if(show)
  {
    if ( this.vScroll )
    { 
      this.vScroll.show();
      this.vScroll.setZIndex(9999);
      this.vScroll.arrow1.show();
      this.vScroll.arrow2.show();
      this.vScroll.box.show();
      this.vScroll.bar.show();
      if(this.getHorizontalScrollBarVisibility())
      {
        this.hScroll.setWidth(this.hScroll.oldWidth);
      }
    }
  }
  else
  {
    if ( this.vScroll )
    { 
      this.vScroll.arrow1.hide();
      this.vScroll.arrow2.hide();
      this.vScroll.box.hide();
      this.vScroll.bar.hide();
      this.vScroll.hide();
      this.vScroll.setZIndex(0000);
      if(this.getHorizontalScrollBarVisibility())
      {
        this.hScroll.oldWidth = this.hScroll.getWidth();
        var diffWidth = this.getWidth()-this.hScroll.oldWidth;
        this.hScroll.setWidth(this.getWidth());
      }
    }
  }
}

function List_getVerticalScrollBarVisibility()
{
  if (! this.built) return false;
  
  if (this.listModel && this.cellsBuilt) {
    var count = this.listModel.getItemCount();
    if ((count > this.visibleRowCount) || ((count == this.visibleRowCount) &&
                                           this.bLastCellClipped)) {
      return true;
    }
  }
  return false;
}

function List_getHorizontalScrollBarVisibility()
{
  if (! this.built) return false;

  return ((this.cells.length > 0) && (this.cells[0].width > this.width));
}

function List_propertyChangeCallback(change)
{
  this.notifyListeners('objectPropertyChangeCallback', change, this);
  return true;
}

function List_objectAddedCallback (object)
{
  if (this.cellsBuilt) {
    this.updateScrollBarVisibility();
    this.updateVerticalScrollValues ();
    this.updateHorizontalScrollValues ();
  }
  this.notifyListeners ('objectAddedCallback', object, this);
  return true;
}

function List_objectRemovedCallback (object)
{
  this.notifyListeners ('objectRemovedCallback', object, this);
  return true;
}

function List_objectPropertyChangeCallback (change, eSrc)
{
  return true;
}

function List_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  this.listModel.addItem (rtid, item, index, isVisible, isSelected);
  
  item.addListener ('propertyChangeCallback', this);
  
  return (item);
}

function List_removeItem (index)
{
  this.listModel.removeItemAtIndex (index);
}

function List_removeAll ()
{
  this.listModel.removeAll ();
}

function List_moreSetWidth (wd)
{
  for (var i=0; i<this.cells.length; i++)
    this.cells[i].setWidth (wd);
  
  if (this.vScroll) {
    this.vScroll.setLeft (this.width - this.vScroll.width);
  }
  if (this.hScroll) {
    this.hScroll.setWidth (this.width);
  }
  this.updateScrollBarVisibility();
}

function List_moreSetHeight(ht)
{
  if (this.vScroll) {
    this.vScroll.setHeight (this.height);
  }
  if (this.hScroll) {
    this.hScroll.setTop (this.height - this.hScroll.height);
  }
  this.updateScrollBarVisibility();
}

function List_setBackgroundColor (col)
{
  if(col!=null) {
    this.backgroundColor = col;
    this.itemBackgroundColor = col;
    
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
    
    for (var i=0; i<this.cells.length; i++) {
      this.cells[i].setBackgroundColor (col);
    }
  }
}

function List_getBackgroundColor (col)
{
  return (this.itemBackgroundColor);
}

function List_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function List_getActiveBackColor ()
{
  return (this.activeBackColor);
}

function List_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function List_getSelectedBackColor ()
{
  return (this.selectedBackColor);
}

function List_getVisibleRowCount ()
{
  return (this.visibleRowCount);
}

function List_isMultiSelect ()
{
  return (this.listModel.getSelectionMode() == ListModel.SELECT_MULTI);
}

function List_setMultiSelect (bVal)
{
  if (bVal) {
    this.listModel.setSelectionMode (ListModel.SELECT_MULTI);
  } else {
    this.listModel.setSelectionMode (ListModel.SELECT_SINGLE);
  }
}

function List_getSelectedIndex ()
{
  return this.listModel.getSelectedItems();
}

function List_setSelectedIndex (index)
{
  this.listModel.selectItem (index);
}

function List_getSelectedItem ()
{
  return this.listModel.getSelectedItems();
}

function List_getSelectedCount () {
  return this.listModel.getSelectedCount();
}

function List_getItemCount ()
{
  return this.listModel.getItemCount ();
}

function List_getItemAtIndex (index)
{
  return this.listModel.getItemAtIndex (index);
}

function List_generateCSS ()
{
  if (! this.built) {
    this.buildControl ();
    this.buildCellRenderers ();
  }
  var CSSStr = "";

  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";

  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";

  if(this.height != null)
    CSSStr += "height:" + this.height + "; ";
      
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height) 
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";
  
  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";
  
  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  
  CSSStr += "}";

  if (this.border) {
    CSSStr += '\n' + this.border.generateCSS ();
  }
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();
  
  return(CSSStr);
}

function List_moreCSS()
{
  var CSSStr = '';
  if ((navigator.platform.indexOf('MacPPC') != -1 ) || (this.mode == List.MODE_STANDARD)) {
    this.fixDimensions ();
  }
  for (var i=0; i < this.cells.length; i++) {
    CSSStr += this.cells[i].generateCSS() + "\n";
  }
  if (this.vScroll)
    CSSStr += this.vScroll.generateCSS();
  if (this.hScroll)
    CSSStr += this.hScroll.generateCSS();
  if (this.unSatIcon)
    CSSStr += this.unSatIcon.generateCSS () + '\n';
  return CSSStr;
}

function List_render()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.name != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    this.addListener ('onKeyDownCallback', this);
    this.addListener ('onKeyUpCallback', this);
    this.addListener ('onFocusCallback', this);
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
    sBuffer += ' onfocus="javascript:doOnFocus(null,\''+ this.self + '\')"';
  }
  sBuffer +='>';

  for (var i=0; i < this.cells.length; i++) {
    sBuffer += this.cells[i].render () + '\n';
  }
  if (this.vScroll)
    sBuffer += this.vScroll.render ();
  if (this.hScroll)
    sBuffer += this.hScroll.render ();
  sBuffer += '</DIV>' + '\n';
  
  if (this.border)
    sBuffer += '</DIV>' + '\n';

  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';

  return (sBuffer);
}

function List_buildCellRenderers()
{
  if (this.visibleRowCount == 0) {
    this.visibleRowCount = Math.ceil(this.height / this.itemHt);
  } else {
    var count = this.listModel.getItemCount ();
    if ((count > 0) && (count < this.visibleRowCount)) {
      this.visibleRowCount = count;
    } else if ((count == 0) && (this.mode == List.MODE_DROPDOWN)) {
      this.visibleRowCount = 1;
    }
    this.setHeight (this.visibleRowCount * this.itemHt + 2);
  }
  
  var cell;
  var modelItem;
  //Cache the column Descriptor and set this one to all the List Items;
  cell = eval('new ' + this.cellRenderClass + '();');
  this.columnDescriptors = cell.getColumnDescriptors ();
  this.colDescHash = cell.getColumnDescriptorsHash ();

  for (var i = 0; i < this.visibleRowCount; i++) {
    cell = eval('new ' + this.cellRenderClass + '();');
    cell.setName(this.name + 'cell' + i);
    cell.setColumnDescriptors (this.columnDescriptors, this.colDescHash);
    cell.setParentObject(this);
    if (ns4) {
      cell.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    } else if (ie4) {
      cell.setReference (this.reference);
    }
    cell.setDimensions (0, this.itemHt*i, this.width, this.itemHt);
    cell.setBackgroundColor (this.itemBackgroundColor);
    cell.setActiveBackColor (this.activeBackColor);
    cell.setSelectedBackColor (this.selectedBackColor);
    cell.setSpanClass (this.spanClass);
    cell.setColor (this.color);
    cell.listBorderClipped = false;
    cell.index = i;
    this.cells[i] = cell;
    modelItem = this.listModel.getItemAtIndex(i);
    cell.setModelItem(modelItem);
    cell.addListener ('actionMouseUp', this);
  }
  // we only do this ONCE;
  this.cellsBuilt = true;
}

function List_moreLaunch (em)
{	
  var ln = this.cells.length;
  for (var i = 0; i < ln; i++) {
    this.cells[i].launch (em);
  }
  if (this.vScroll)
    this.vScroll.launch (em);
  if (this.hScroll)
    this.hScroll.launch (em);
  if (this.unSatIcon)
    this.unSatIcon.launch ();
}

function List_fixDimensions ()
{
  var itemLen = this.cells.length;
  var cell = null;
  if (itemLen) {
    var colDescs = this.columnDescriptors;
    this.maxDataDescriptors = this.listModel.getMaxColumnDataDescriptors (colDescs);
    
    //determine perferred dimensions for each column;
    this.updatePreferredWidth();

    //determind available dimensions for each column;
    this.updateColumnPosition ();
    
    //update list item dimension and positions;
    this.fixItemDimensions ();
  }
  this.updateScrollBarVisibility();
  this.updateVerticalScrollValues();
  this.updateHorizontalScrollValues();
  
  if (this.mode == List.MODE_DROPDOWN) {
    //adjust the height of the list to fit the visible count;
    if (this.getHorizontalScrollBarVisibility ()) {
      this.setHeight ((this.visibleRowCount * this.itemHt + 2) + this.hScroll.height);
    } else {
      this.setHeight (this.visibleRowCount * this.itemHt + 2);
    }
  } else if (this.mode == List.MODE_STANDARD) {
    //calculate the max visible cell count,  leave any overlapping cells as empty regions;
    var maxCount, listHt;
    if (this.getHorizontalScrollBarVisibility ()) {
      listHt = this.height - this.hScroll.height;
      maxCount = Math.ceil (listHt / this.itemHt);
    } else {
      listHt = this.height;
      maxCount = Math.ceil (listHt / this.itemHt);
    }
    if (maxCount < itemLen) {
      this.visibleRowCount = maxCount;
      for (var i=maxCount; i < itemLen; i++) {
        this.cells[i].hide();
        this.cells[i] = null;
        delete this.cells[i];
      }
      this.cells.length = maxCount;
    }
    this.adjustLastCellHeight ();
    //doubly link each view items in the list;
    for (var i=0; i<maxCount; i++) {
      if (i==0){
        this.cells[i].prevItem = this.cells[maxCount-1];
        this.cells[i].nextItem = this.cells[i+1];
      } else if (i==(maxCount-1)){
        this.cells[i].prevItem = this.cells[i-1];
        this.cells[i].nextItem = this.cells[i-(maxCount-1)];
      } else{
        this.cells[i].prevItem = this.cells[i-1];
        this.cells[i].nextItem = this.cells[i+1];
      }
    }
  }
  this.updateScrollBarVisibility();
  this.updateVerticalScrollValues();
}

function List_fixItemDimensions ()
{
  var itemLen = this.cells.length;
  //set the itemHeight and reposition to the right value;
  if (this.itemHt < this.cellPrefHeight) {
    this.itemHt = this.cellPrefHeight;
    for (var j=0; j<itemLen; j++) {
      this.cells[j].setTop (this.itemHt * j);
      this.cells[j].setHeight(this.itemHt);
    }
  }
  //set the width of each list item;
  if (this.cells[0].width < this.cellPrefWidth) {
    for (var j=0; j<itemLen; j++) {
      this.cells[j].setWidth (this.cellPrefWidth);
    }
  }
  for (var i = 0; i < itemLen; i++) {
    this.cells [i].fixDimensions ();
  }
}

function List_adjustLastCellHeight ()
{
  var lastIndex = this.visibleRowCount - 1;
  var cell = this.cells[lastIndex];
  var listHt;
  if (this.getHorizontalScrollBarVisibility ()) {
  //if (this.hScroll.isVisible ()) {
    listHt = this.height - this.sclBarWd;
  } else {
    listHt = this.height;
  }
  cell.setHeight (listHt - (this.itemHt * lastIndex));

  if (cell.height != this.itemHt) {
    //last cell is clipped;
    this.bLastCellClipped = true;
    cell.listBorderClipped = true;
  }
  cell.bLastCell = true;
}

function List_selectItem (index)
{
  if (this.listModel != null) {
    var litem = this.listModel.getItemAtIndex (index);
    if (litem != null) {
      litem.setSelected (true);
    }
  }
}

function List_deSelectItem (index)
{
  if (this.listModel != null) {
    var litem = this.listModel.getItemAtIndex (index);
    if (litem != null) {
      litem.setSelected (false);
    }
  }
}

function List_setZIndex (index)
{
  this.zIndex = index;
  if (this.border)
    this.border.setZIndex (index);
  
  if (this.launched)
    this.getHTMLObj ().zIndex = index;
  
  //walk thru the list of item and make it visible;
  for (var i=0; i < this.cells.length; i++) {
    this.cells[i].setZIndex (index);
  }
}

function List_show (zIndex)
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }		
  //walk thru the list of item and make it visible;
  for (var i=0; i<this.cells.length; i++) {
    this.cells[i].show();
  }
}

function List_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;
  
  this.font = fnt;
  
  this.spanClass = this.name + 'SpnNml';
  
  style = this.getStyleObject (this.spanClass, true);
  this.setFontAttributes (style);
}

function List_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
  stylObj.setAttribute ('padding-left', 5);
  //stylObj.setAttribute ('padding-right', 5);
}

function List_setPaddingAttributes (stylObj, pad)
{
  stylObj.setAttribute ('padding-left', pad);
  stylObj.setAttribute ('padding-right', pad);
}

function List_setSpanClass (cls)
{
  this.spanClass = cls;
}

function List_setVisibleRowCount (num) {
  if (this.launched) return;
  this.visibleRowCount = num;
}

function List_getVisibleRowCount () {
  return this.visibleRowCount;
}

function List_actionMouseUp (modelItem, modelView)
{
  this.setFocussedItem (modelView, 1);
  this.notifyListeners ('actionMouseUp', modelItem, modelView);
  return true;
}

function List_actionOnChange (modelItem, modelView)
{
  this.notifyListeners ('actionOnChange', modelItem, modelView);
  return true;
}

function List_isMouseInside (xpos, ypos)
{
  var rt = this.getLeft() + this.getWidth();
  var bt = this.getTop() + this.getHeight();
  if ((xpos >= this.getLeft()) && (xpos <= rt) && (ypos >= this.getTop()) 
      && (ypos <= bt)) {
    return (true);
  } else 
    return (false);
}

function List_createBorder (borderWidth, borderColor)
{
  if (this.launched) return;
  
  if (borderWidth != null)
    this.borderWidth = borderWidth;
  else
    this.borderWidth = 1;
  
  if (borderColor != null)
    this.borderColor = borderColor;
  else if (this.color)
    this.borderColor = this.color
  else
    this.borderColor = 'black';
  
  if (ns4) {
    this.bBorder = true;
    this.borderVisible = true;
  }
  this.border = new Base ();
  this.border.setName ('BDR-' + this.name);
  this.border.setBackgroundColor (this.borderColor);
  if (! this.visibility) {
    this.border.visibility = false;
  }
  this.visibility = true;
  
  //resize this controls diemensions and properties;
  this.setDimensions (this.left, this.top, this.width, this.height);
  this.setParentObject (this.parentObj);
  this.setReference (this.reference);
}

function List_updatePreferredWidth ()
{
  // find the preferred pixel dimensions;
  var dataDesc = null;
  var colDesc = null;
  var descsLen = this.maxDataDescriptors.length;
  var prefWidth = 0;
  var prefHeight = 0;
  var charWidth, charHeight;
  var totWidth = 0, maxHt = 0;
  
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  for (var i = 0; i < descsLen; i++) {
    dataDesc = this.maxDataDescriptors[i];
    colDesc = this.columnDescriptors[i];
    if (colDesc.name == 'text') {
      prefWidth = dataDesc.width * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else if (colDesc.name == 'descImg') {
      var prefSize = Utils.getImageSize (dataDesc.value);
      if (prefSize == null) {
        prefSize = new Array (colDesc.defWidth, colDesc.defHeight);
        //prefSize = new Array (103, 29);
      }
      prefWidth = prefSize [0];
      prefHeight = prefSize [1];
      //change the value to some realistic one if the image height is large;
      if (prefHeight > this.height/2)
        prefHeight = 22;
    } else if (colDesc.name == 'state') {
      prefWidth = 20;
      prefHeight = 16;
    } else if (colDesc.name == 'atp') {
      prefWidth = 10 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight; 
    } else if (colDesc.name == 'price') {
      prefWidth = 8 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else if (colDesc.name == 'count') {
      prefWidth = 5 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else {
      prefWidth = 100;
      prefHeight = this.height;
    }
    colDesc.setPrefWidth (prefWidth);
    colDesc.setPrefHeight (prefHeight);
    totWidth += prefWidth;
    if (maxHt < prefHeight) {
      maxHt = prefHeight;
    }
  }
  this.cellPrefWidth = totWidth;
  this.cellPrefHeight = maxHt;
}

function List_updateColumnPosition ()
{
  var colDesc = null;
  var len = this.columnDescriptors.length;
  if (! this.springColDesc) {
    for (var i=0; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.spring) {
        this.springColDesc = colDesc;
      }
    }
  }

  var totWidth = 0;
  var totWidthUpToSpringCell =0;
  var springColDesc = this.springColDesc;

  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors[i];
    if (colDesc.defVisible) {
      if (colDesc == springColDesc) {
        totWidthUpToSpringCell = totWidth;
      }
      colDesc.setLeft (totWidth);
      totWidth += colDesc.getPrefWidth ();
    }
  }
  if (this.width > this.cellPrefWidth) {
    springColDesc.setAvailWidth (springColDesc.getPrefWidth() + (this.width - this.cellPrefWidth));

    //now shift everything else from spring cell;
    totWidth = totWidthUpToSpringCell + springColDesc.getAvailWidth ();
    for (var i=springColDesc.index + 1; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.defVisible) {
        colDesc.setLeft (totWidth);
      }
      totWidth += colDesc.getAvailWidth ();
    }
  } else {
    //use the preferred with for the spring cell as available widht;
    springColDesc.setAvailWidth (springColDesc.getPrefWidth ());
  }
}

function List_showColumn (colName)
{
  var colDesc = this.colDescHash [colName];
  if (colDesc && (! colDesc.defVisible)) {
    colDesc.defVisible = true;
    this.updateColumnPosition ();
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].showColumn (colName);
    }
  }
}

function List_hideColumn (colName)
{
  var colDesc = this.colDescHash [colName];
  if (colDesc && colDesc.defVisible) {
    colDesc.defVisible = false;
    this.updateColumnPosition ();
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].hideColumn (colName);
    }
  }
}

function List_moreDestroy ()
{
  if (this.listModel) {
    this.listModel.destroy ();
    this.listModel = null;
    delete this.listModel;
  }
  if (this.cells) {
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].destroy ();
      
      this.cells[i] = null;
      delete this.cells[i];
    }
    this.cells.length = 0;
    this.cells = null;
    delete this.cells;
  }
  if (this.vScroll) {
    this.vScroll.destroy ();
    this.vScroll = null;
    delete this.vScroll;
  }
  if (this.hScroll) {
    this.hScroll.destroy ();
    this.hScroll = null;
    delete this.hScroll;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
  Utils.clearCollection (this.columnDescriptors);
  Utils.clearCollection (this.colDescHash);

  this.compMgr = null;
  delete this.compMgr;

  this.evtMgr = null;
  delete this.evtMgr;
}

function List_setEventManager (em)
{
  this.eventManager = em;
}

function List_onKeyDownCallback(e, eSrc)
{
  var key = e.keyCode;
  switch (key) {
  case 9:
    var itm = this.focViewItem;    
    if (e.shiftKey) {
      if (itm == null) {
        return;
      }
      //move focus backwards
      if (this.focCellInd == 1) {
        this.focCellInd = this.numFocusPerItem;
        if (itm.index == 0) {
          var success = false;
          //we reached the top visible cell we may have to scroll up
          if (this.vScroll && this.vScroll.isVisible()) {
            success = this.vScroll.scrollUp();
          }
          if (! success) {
            this.focCellInd = 1;
            this.eventManager.focusBackward(e, this);
            return;
          }
        } else
          this.focViewItem = this.cells[itm.index-1];
      } else
        this.focCellInd--;
    } else {
      //move focus forward
      if (itm == null) {
        this.focViewItem = this.cells[0];
        this.focCellInd = 1;
      } else if (this.focCellInd == this.numFocusPerItem) {
        this.focCellInd = 1;
        if (itm.index == this.cells.length-1) {
          var success = false;
          //we reached the last visible cell we may have to scroll down
          if (this.vScroll && this.vScroll.isVisible()) {
            success = this.vScroll.scrollDown();
          }
          if (! success) {
            this.focCellInd = this.numFocusPerItem;
            this.eventManager.focusForward(e, this);
            return;
          }
        } else
          this.focViewItem = this.cells[itm.index+1];
      } else
        this.focCellInd++;
    }
    if (this.focViewItem.getModelItem())
      this.focViewItem.setFocus(this.focCellInd);
    else {
      this.focViewItem = this.cells[this.focViewItem.index - 1];
      this.focCellInd = this.numFocusPerItem;
      this.eventManager.focusForward(e, this);
      return;
    }
    //e.cancelBubble = true;
    e.returnValue = false;
    break;
  case 37:
  case 39:
    if (this.hScroll && this.hScroll.isVisible()) {
      Arrow_onKeyDownCallback (e, this.hScroll);
    }
    break;
  case 38:
  case 40:
    if (this.vScroll && this.vScroll.isVisible()) {
      Arrow_onKeyDownCallback (e, this.vScroll);
    }
    break;
  }
}

function List_onKeyUpCallback(e, eSrc)
{
  var key = (ns4)?e.which : e.keyCode;
  switch (key) {
  case 37:
  case 39:
    if (this.hScroll && this.hScroll.isVisible()) {
      Arrow_onKeyUpCallback (e, this.hScroll);
    }
    break;    
  case 38:
  case 40:
    if (this.vScroll && this.vScroll.isVisible()) {
      Arrow_onKeyUpCallback (e, this.vScroll);
    }
  }
}

function List_setFocussedItem (vItm, cellInd)
{
  this.focViewItem = vItm;
  this.numFocusPerItem = this.focViewItem.getMaxNumFocusCells();
  if (! cellInd) {
    //assign the max cell index
    this.focCellInd = this.numFocusPerItem;
  } else
    this.focCellInd = cellInd;

  this.focViewItem.setFocus(this.focCellInd);
}

function List_onFocusCallback(e, eSrc)
{
  if (! this.focViewItem) {
    //move the focus to the first list item
    this.focViewItem = this.cells[0];
  }
  if (! this.focCellInd)
    this.focCellInd = 1;
  this.focViewItem.setFocus(this.focCellInd);

  this.numFocusPerItem = this.cells[0].getMaxNumFocusCells();
  e.returnValue = true;
  return true;
}

function List_resetFocus ()
{
  this.focViewItem = this.cells[0];
  this.focCellInd = 1;
}

HTMLHelper.importPrototypes(List, Base);

//public methods
List.prototype.refreshActiveList = List_refreshActiveList;
List.prototype.setVerticalScrollBarVisibility = List_setVerticalScrollBarVisibility;
List.prototype.arrangeItems = List_arrangeItems;
List.prototype.reArrangeItems = List_reArrangeItems;
List.prototype.updateDone = List_updateDone;
List.prototype.onLaunchCall = List_onLaunchCall;
List.prototype.moreSetWidth = List_moreSetWidth;
List.prototype.moreSetHeight = List_moreSetHeight;
List.prototype.setZIndex = List_setZIndex;
List.prototype.hitTest = List_hitTest;
List.prototype.createBorder = List_createBorder;
List.prototype.addItem = List_addItem;
List.prototype.removeItem = List_removeItem;
List.prototype.removeAll = List_removeAll;
List.prototype.setBackgroundColor = List_setBackgroundColor;
List.prototype.getBackgroundColor = List_getBackgroundColor;
List.prototype.setActiveBackColor = List_setActiveBackColor;
List.prototype.getActiveBackColor = List_getActiveBackColor;
List.prototype.setSelectedBackColor = List_setSelectedBackColor;
List.prototype.getSelectedBackColor = List_getSelectedBackColor;
List.prototype.show = List_show;
List.prototype.moreLaunch = List_moreLaunch;
List.prototype.selectItem = List_selectItem;
List.prototype.deSelectItem = List_deSelectItem;
List.prototype.isMultiSelect = List_isMultiSelect;
List.prototype.setMultiSelect = List_setMultiSelect;	
List.prototype.getSelectedIndex = List_getSelectedIndex;
List.prototype.setSelectedIndex = List_setSelectedIndex;
List.prototype.getSelectedItem = List_getSelectedItem;
List.prototype.getSelectedCount = List_getSelectedCount;
List.prototype.getItemCount = List_getItemCount;
List.prototype.setCellRenderClass = List_setCellRenderClass;
List.prototype.getCellRenderClass = List_getCellRenderClass;
List.prototype.setVisibleRowCount = List_setVisibleRowCount;
List.prototype.getVisibleRowCount = List_getVisibleRowCount;
List.prototype.setFirstVisibleItem = List_setFirstVisibleItem;
List.prototype.setFont = List_setFont;
List.prototype.setSpanClass = List_setSpanClass;
List.prototype.isMouseInside = List_isMouseInside;
List.prototype.generateCSS = List_generateCSS;
List.prototype.render = List_render;
List.prototype.setEventManager = List_setEventManager;
List.prototype.setModel = List_setModel;

//Protected Methods
List.prototype.updatePreferredWidth = List_updatePreferredWidth;
List.prototype.updateColumnPosition = List_updateColumnPosition;
List.prototype.showColumn = List_showColumn;
List.prototype.hideColumn = List_hideColumn;
List.prototype.moreDestroy = List_moreDestroy;

//private methods
List.prototype.scrollbarCallback = List_scrollbarCallback;
List.prototype.propertyChangeCallback = List_propertyChangeCallback;
List.prototype.objectPropertyChangeCallback = List_objectPropertyChangeCallback;
List.prototype.objectAddedCallback = List_objectAddedCallback;
List.prototype.objectRemovedCallback = List_objectRemovedCallback;
List.prototype.getCellWithModelItem = List_getCellWithModelItem;
List.prototype.buildCellRenderers = List_buildCellRenderers;
List.prototype.updateScrollBarVisibility = List_updateScrollBarVisibility;
List.prototype.updateVerticalScrollValues = List_updateVerticalScrollValues
List.prototype.updateHorizontalScrollValues = List_updateHorizontalScrollValues
List.prototype.moreCSS = List_moreCSS;
List.prototype.setFontAttributes = List_setFontAttributes;
List.prototype.getVerticalScrollBarVisibility = List_getVerticalScrollBarVisibility;
List.prototype.getHorizontalScrollBarVisibility = List_getHorizontalScrollBarVisibility;
List.prototype.adjustIndent = List_adjustIndent;
List.prototype.fixDimensions = List_fixDimensions;
List.prototype.fixItemDimensions = List_fixItemDimensions;
List.prototype.adjustLastCellHeight = List_adjustLastCellHeight;
List.prototype.buildControl = List_buildControl;
List.prototype.createScrollBar = List_createScrollBar;
List.prototype.setFocussedItem = List_setFocussedItem;
List.prototype.resetFocus = List_resetFocus;

//Event Handlers
List.prototype.actionMouseUp = List_actionMouseUp;
List.prototype.actionOnChange = List_actionOnChange;
List.prototype.onKeyDownCallback = List_onKeyDownCallback;
List.prototype.onKeyUpCallback = List_onKeyUpCallback;
List.prototype.onFocusCallback = List_onFocusCallback;

//list mode constants
List.MODE_STANDARD = 0;
List.MODE_DROPDOWN = 1;


