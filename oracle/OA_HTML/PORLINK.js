/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: PORLINK.js 115.15 2001/06/07 12:39:37 pkm ship    $ */

function shoppingCart() {

  if ((allForms['POR_RESULT_FORM'] == null)||(allForms['POR_PAGE_INFO']== null)|| (allForms['POR_REQ_INFO_FORM'] == null))
     initResultForm(document);


        if(top.reqLines>0)
          var action="displayCartFull";
		else
          var action="displayCartEmpty";
        var func="";
        var url="/"+ top.JAVA_VIRTUAL_PATH +"/oracle.apps.icx.por.apps.AppsManager";
        
                
  top.submitRequest("createReq",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top",
                    "");
}

function expressCheckoutCrumb() {

   setStateInfo('checkoutFlow', 'express');
   var action="displayExpressCheckout";
   var func="updateReqWithoutValidate";
   var url="/"+top.JAVA_VIRTUAL_PATH+"/oracle.apps.icx.por.apps.AppsManager";

   top.submitRequest("createReq",
			url,
			action,
			func,
			"",
			"",
			"_top")






}



function corporateShoppingList(){

 setStateInfo('sortByPubList','POR_LIST_NAME');
 setStateInfo('sortByDirectionPubList','ASC');

        var action="displayPublicLists";
	var func="publicLists";
	var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

  top.submitRequest("publiclists",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");

}

function personalShoppingList(){

 setStateInfo('sortByFavList','SUPPLIER_NAME');
 setStateInfo('sortByDirectionFavList','ASC');

        var action="displayShoppingList";
	var func="shoppingList";
	var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

  top.submitRequest("shoplists",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");

}


function startOver()
{

	var url="/"+top.JAVA_VIRTUAL_PATH+"/oracle.apps.icx.por.apps.AppsManager";
	var func = 'cancelCheckOut';

	if (checkoutFlow=='approver') {
		var action = 'displayCartApprover';
                func = '';
        } 
	else if (reqLines>0)
		var action = 'displayCartFull';
	else if (reqLines==0)
		var action = 'displayCartEmpty';

     top.submitRequest("createReq",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");
}

function home()
{
	var url="/"+top.JAVA_VIRTUAL_PATH+"/oracle.apps.icx.por.apps.AppsManager";
	var action = 'displayHomePage';
	var func = ''; 

	top.submitRequest("menu",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");                       
}



function viewReceipts() {
  var action = "displayReceiveMenu";
  var func = "displayReceiveMenu";
  var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

  var generalData ="";
  top.submitRequest("receiveOrders",
                    url,
                    action,
                    func,
                    generalData,
                    new Array(""),
                    "_top");

}


function viewMyReceipt() {
        
  setStateInfo('queryMode','VIEW_MODE_MY');
  setStateInfo('sortBy','SORT_NEED_BY');
  setStateInfo('sortByDirection','DESC');

	var action = "displayReceiveOrders";
	var func = "queryReceipts";
	var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

 var generalData =  FIELD_DELIMITER + "showRequester" + VALUE_DELIMITER + "N";

  top.submitRequest("receiveOrders",
                    url,
                    action,
                    func,
                    generalData,
                    new Array(""),
                    "_top");

}

function viewAllReceipt() {
        
  setStateInfo('queryMode','VIEW_MODE_ALL');
  setStateInfo('sortBy','SORT_NEED_BY');
  setStateInfo('sortByDirection','DESC');

	var action = "displayReceiveOrders";
	var func = "queryReceipts";
	var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";

 var generalData =  FIELD_DELIMITER + "showRequester" + VALUE_DELIMITER + "Y";

  top.submitRequest("receiveOrders",
                    url,
                    action,
                    func,
                    generalData,
                    new Array(""),
                    "_top");

}

function viewCorrectReceipt(){
 var action = "displayCorrectReceipts";
        var func = "queryReceipts";
        var url="/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager";
  top.submitRequest("correctReceipts",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");
 
}


function viewReceiptDetail(index)
   {
  //        var form= top.allForms["RECEIVE_ORDERS"][index];
            var receipt_num = allForms["RECEIVE_ORDERS"][index].elements["POR_RECEIPT_NUM"].value;
	    reqHeaderId  =  allForms["RECEIVE_ORDERS"][index].elements["TRANSACTION_ID"].value;
            ordSessionId  = 0;
            reqHeaderId   =    form.POR_REQ_HEADER_ID.value;            

           submitRequest("correctReceipts",
		"/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.AppsManager",
                       "displayTransactionHistory",
                       "displayReceiptDetail",
		FIELD_DELIMITER + "receiptNum" + VALUE_DELIMITER + receipt_num,
			"",
                       "_top");

     }

function viewApprovals()
{

  setStateInfo('app_queryName',null);
  setStateInfo('app_sortBy','CREATION_DATE');
  setStateInfo('app_sortByDirection','DESC');

  queryName = getStateInfo('app_queryName');
  if (queryName == null)
    queryName = "VIEW_MODE_TO_APPROVE";
  sortBy = getStateInfo('app_sortBy');
  if (sortBy == null)
    sortBy = "CREATION_DATE";
  sortByDirection = getStateInfo('app_sortByDirection');
  if (sortByDirection == null)
    sortByDirection = "DESC";

  var generalData = FIELD_DELIMITER + "queryName" + 
                    VALUE_DELIMITER + queryName + 
  		    FIELD_DELIMITER + "sortBy" + 
		    VALUE_DELIMITER + sortBy +
		    FIELD_DELIMITER + "sortByDirection" +
		    VALUE_DELIMITER + sortByDirection +
                    FIELD_DELIMITER + "checkAccess" +
		    VALUE_DELIMITER + "Y";

      submitRequest("orderHistory",
                    "/"+ top.JAVA_VIRTUAL_PATH +"/oracle.apps.icx.por.apps.AppsManager",   
                    "displayApprovals",
                    "displayApprovals",
                    generalData,
                    new Array(""),
                    "_top");
}

function shop()     
{
 if ((allForms['POR_RESULT_FORM'] == null)||(allForms['POR_PAGE_INFO']== null)|| (allForms['POR_REQ_INFO_FORM'] == null))
     initResultForm(document);

window.location.href = '/OA_HTML' + top.JSP_DIRECTORY_STRUCTURE + 'catalog/CatalogShoppingHome.jsp';
}

function specialOrder()     
{
        var url="/"+top.JAVA_VIRTUAL_PATH+"/oracle.apps.icx.por.apps.AppsManager";
        var action = 'displaySpecialOrder';
        var func = '';              

	for(var i=0; i<CHECK_OUT_PAGES.length; i++) {
		if (CHECK_OUT_PAGES[i] == allForms['POR_PAGE_INFO'][0].elements['currentUITemplate'].value)
			func = 'cancelCheckOut';
	}

        top.submitRequest("createReq",     
                    url,
                    action,
                    func,  
                    "",    
                    new Array(""),
                    "_top");
}

function orderHistory() {

  if ((allForms['POR_RESULT_FORM'] == null)||(allForms['POR_PAGE_INFO']== null)|| (allForms['POR_REQ_INFO_FORM'] == null))
     initResultForm(document);


  setStateInfo('queryName',null);
  setStateInfo('sortBy','CREATION_DATE');
  setStateInfo('sortByDirection','DESC');

  queryName = getStateInfo('queryName');
  if (queryName == null)
    queryName = "VIEW_MODE_MY_RECENT";
  sortBy = getStateInfo('sortBy');
  if (sortBy == null)
    sortBy = "CREATION_DATE";
  sortByDirection = getStateInfo('sortByDirection');
  if (sortByDirection == null)
    sortByDirection = "DESC";

  var generalData = FIELD_DELIMITER + "queryName" + 
                    VALUE_DELIMITER + queryName + 
  		    FIELD_DELIMITER + "sortBy" + 
		    VALUE_DELIMITER + sortBy +
		    FIELD_DELIMITER + "sortByDirection" +
		    VALUE_DELIMITER + sortByDirection +
                    FIELD_DELIMITER + "checkAccess" +
		    VALUE_DELIMITER + "Y";

  top.submitRequest("orderHistory",
                    "/"+ top.JAVA_VIRTUAL_PATH +"/oracle.apps.icx.por.apps.AppsManager",
                    "displayOrderHistory",
                    "displayOrderHistory",
                    generalData,
                    new Array(""),
                    "_top",
                    "");

}

function myProfile()
{
   if ((allForms['POR_RESULT_FORM'] == null)||(allForms['POR_PAGE_INFO']== null)|| (allForms['POR_REQ_INFO_FORM'] == null))
     initResultForm(document);

       var url="/"+top.JAVA_VIRTUAL_PATH+"/oracle.apps.icx.por.apps.AppsManager";
        var action = 'displayMyProfile';
        var func = '';

        top.submitRequest("userPref",
                    url,
                    action,
                    func,
                    "",
                    new Array(""),
                    "_top");
}


function callHelp(helpId)
{
  var help_win = window.open('/OA_HTML' + top.JSP_DIRECTORY_STRUCTURE + 'help.jsp?id=' + helpId, "help_win", "resizable=yes,scrollbars=yes,toolbar=yes,width=600,height=500");
  help_win.focus();
}

function help()
{

  if ((allForms['POR_RESULT_FORM'] == null)||(allForms['POR_PAGE_INFO']== null)|| (allForms['POR_REQ_INFO_FORM'] == null))
  initResultForm(document);

  callHelp();
}

function catalog()
{
window.location.href = '/OA_HTML' + top.JSP_DIRECTORY_STRUCTURE + 'catalog/CatalogShoppingHome.jsp';
}


function viewnotifications()
{

window.location.href = '/OA_HTML' + top.JSP_DIRECTORY_STRUCTURE + 'services/SSPWFDefaultContainer.jsp?WFContextArg=WORKLIST&DefaultView=PO_QUERY_OPEN_TODO&reqSessionId=' + reqSessionId + '&OA_SERVER_NAME=' + getStateInfo('OAS_SERVER_NAME') + '&OA_SERVER_PORT=' + getStateInfo('OAS_SERVER_PORT');

}








