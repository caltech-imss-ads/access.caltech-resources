/* $Header: czLLItM.js 115.9 2001/01/08 15:13:58 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function LogicItemModel (rtid, text, state, count, price, atp, isVisible)
{
  this.parentConstructor = ListItemModel;
  this.parentConstructor (rtid, text, false, isVisible);
  this.type = 'LogicItemModel';
  
  // may be required... depends on the logical type;
  if (state != null) {
    this.setValue ('state', state);
  } else {
    this.setValue ('state', 'unknown');
  }
  // may be required... depends on the logical type;
  if (count != null) {
    this.setValue ('count', count);
  } else {
    this.setValue ('count', 0);
  }
  // optional;
  if (price != null) {
    this.setValue ('price', price);
  } else {
    this.setValue ('price', '');
  }
  if (atp != null) {
    this.setValue ('atp', atp);
  } else {
    this.setValue ('atp', '');
  }
}

function LogicItemModel_setState (state) 
{
  this.setValue ('state', state);
}

function LogicItemModel_setVisibility (visible) 
{
  this.setValue ('visible', visible);
}

function LogicItemModel_getVisibility ()
{
  return this.getValue ('visible');
}

function LogicItemModel_getState ()
{
  return this.getValue ('state');
}

function LogicItemModel_clearState ()
{
  //set state to unknown temporarily and without throwing events;
  //and let the server to set the actual state;
  this.columnHash['state'] = 'empty';
}

function LogicItemModel_setCount (count)
{
  this.setValue ('count', count);
}

function LogicItemModel_getCount ()
{
  return this.getValue ('count');
}

function LogicItemModel_setPrice (price)
{
  this.setValue ('price', price);
}

function LogicItemModel_getPrice ()
{
  return this.getValue ('price');
}

function LogicItemModel_setATP (atp)
{
  this.setValue ('atp', atp);
}

function LogicItemModel_getATP ()
{
  return this.getValue ('atp');
}

function LogicItemModel_setDescImage (imgSrc)
{
  this.setValue ('descImg', imgSrc);
}

function LogicItemModel_getDescImage ()
{
  return this.getValue ('descImg');
}

HTMLHelper.importPrototypes(LogicItemModel, ListItemModel);

LogicItemModel.prototype.getState = LogicItemModel_getState;
LogicItemModel.prototype.setState = LogicItemModel_setState;
LogicItemModel.prototype.clearState = LogicItemModel_clearState;
LogicItemModel.prototype.getCount = LogicItemModel_getCount;
LogicItemModel.prototype.setCount = LogicItemModel_setCount;
LogicItemModel.prototype.getPrice = LogicItemModel_getPrice;
LogicItemModel.prototype.setPrice = LogicItemModel_setPrice;
LogicItemModel.prototype.getATP = LogicItemModel_getATP;
LogicItemModel.prototype.setATP = LogicItemModel_setATP;	
LogicItemModel.prototype.setVisibility = LogicItemModel_setVisibility;	
LogicItemModel.prototype.getVisibility = LogicItemModel_getVisibility;	
LogicItemModel.prototype.setDescImage = LogicItemModel_setDescImage;
LogicItemModel.prototype.getDescImage = LogicItemModel_getDescImage;
