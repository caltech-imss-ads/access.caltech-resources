/*=================================================================+
|               Copyright (c) 1999 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POSSICPJ.js 115.1 99/10/15 17:17:01 porting shi $ */

function clear()
{
  document.POS_SIC_UPDATE.POS_SIC_ACTION.value = "CLEAR";
  document.POS_SIC_UPDATE.submit();
}

function toPosInt(str) 
{
  var res = 1;
  var num = parseInt(str);

  if ((num ==0) && (str.length == 1))
    return num;
  
  for (var i=1; i<str.length; i++) 
    res = 10 * res;

  if (isNaN(num) || (num < res))
   return -1;

  return num;
}

function addrows()
{
  var more_rows = toPosInt(document.POS_SIC_UPDATE.POS_MORE_ROWS.value);
  var msg = window.top.FND_MESSAGES["ICX_POS_VALID_NUMBER"];

  if (more_rows == -1) {
    alert(msg);
    return;
  }

  document.POS_SIC_UPDATE.POS_SIC_ACTION.value = "ADDROWS";
  document.POS_SIC_UPDATE.submit();
}

function check_before_finish()
{
  var msg = window.top.FND_MESSAGES["ICX_POS_VALID_NUMBER"];
  var more_rows = toPosInt(document.POS_SIC_UPDATE.POS_MORE_ROWS.value);

  if (more_rows == -1) {
    alert(msg);
    return false;
  }

  if (parseInt(document.POS_SIC_UPDATE.POS_SIC_ROWS.value) <= 0)
    return true;

  var e = document.POS_SIC_UPDATE.POS_SIC_CAPACITY_PER_DAY;
  var e1 = document.POS_SIC_UPDATE.POS_SIC_FROM_DATE;
  var e2 = document.POS_SIC_UPDATE.POS_SIC_TO_DATE;
  for(var i = 0;i < e.length; i++) {
    var v = toPosInt(e[i].value);
    if ((!(e[i].value == "" && e1[i].value == "" && e2[i].value == "")) && 
        (v == -1)) {
        document.POS_SIC_UPDATE.POS_ERROR_ROW.value = (i+1) + "" ;
        document.POS_SIC_UPDATE.POS_SIC_ACTION.value = "ERROR";
	document.POS_SIC_UPDATE.submit();
        alert (msg);
        return false;
    }
  }
 
  return true;
}

function check()
{
  document.POS_SIC_UPDATE.POS_SIC_ACTION.value = "CHECK";
  document.POS_SIC_UPDATE.submit();
}

function finish()
{
  var winWidth = 400;
  var winHeight = 150;
  var winAttributes = "menubar=no,location=no,toolbar=no," + 
                      "width=" + winWidth + ",height=" + winHeight + 
	              ",screenX=" + (screen.width - winWidth)/2 + 
                      ",screenY=" + (screen.height - winHeight)/2 + 
                      ",resizable=yes,scrollbars=yes";
  var url = top.getTop().scriptName + "/pos_window.dialogbox";
 
  parent.result.document.POS_ASL_RESULT.submit(); 
  document.POS_SIC_UPDATE.POS_SIC_ACTION.value = "SUBMIT";
  document.POS_SIC_UPDATE.submit();
  parent.tolerance.finish();
  top.openModalWindow(url, "Finish", winAttributes, winWidth, winHeight);
}

function show_error(p_msg,l_date_format)
{
   var msg = window.top.FND_MESSAGES["ICX_POS_DATE_FORMAT"];
  if (p_msg != ""){
    if(p_msg == msg)
    {
     alert(p_msg + " " + l_date_format);
    }
    else
    {
     alert(p_msg); 
    }
  }
}











