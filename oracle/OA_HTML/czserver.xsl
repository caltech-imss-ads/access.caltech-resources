<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<!-- $Header: czserver.xsl 115.8 2000/12/10 16:10:53 pkm ship     $-->

<!-- ********************************************************************** -->
<xsl:template match="/">
	<xsl:apply-templates select="config-ui"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-ui">
<!--xsl:pi name="xml">version="1.0" standalone="yes"</xsl:pi-->
	<xsl:element name="update_ui">    
		<xsl:apply-templates select="config-layout|config-state|config-messages"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-layout">
	<xsl:apply-templates select="cmd-add|cmd-navigate|cmd-remove|cmd-clear|cmd-replace|application|cmd-remove-row|cmd-insert"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-insert">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
  	        <xsl:apply-templates select="row-data" mode="insert"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-add">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
		<xsl:apply-templates select="panel|treeview|treenode|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|grid-column|row-data" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-navigate">
	<xsl:element name="control">
		<xsl:attribute name="operation">navigate</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-remove">
	<xsl:element name="control">
		<xsl:attribute name="operation">delete</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@remove-id"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-clear">
	<xsl:element name="control">
		<xsl:attribute name="operation">initialize</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-replace">
	<xsl:element name="control">
		<xsl:attribute name="operation">initialize</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
		<xsl:apply-templates select="panel|treeview|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|grid-column|row-data|find-control" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="config-state">
	<xsl:apply-templates select="object-update|row-update|model-update|cmd-navigate"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="row-update">
	<xsl:apply-templates select="col-data" mode="update"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="object-update">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@row-id"/></xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="cmd-remove-row">
	<xsl:element name="control">
		<xsl:attribute name="operation">delete</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/>.<xsl:value-of select="@remove-id"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="application">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:if test="not(@rtid)">
				<xsl:attribute name="ui_obj_id">mainWindow</xsl:attribute>
			</xsl:if>
			<xsl:if test="@rtid">
				<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
			</xsl:if>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">window</xsl:element>
			<xsl:element name="layout_info">   
				<xsl:attribute name="name">width</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@width"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">height</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@height"/></xsl:attribute>
			</xsl:element>
			<xsl:if test="@bgcolor">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">bgcolor</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@bgcolor"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
		<xsl:apply-templates select="frameset"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="frameset">
	<xsl:choose>
		<xsl:when test="../system-frame">
			<xsl:element name="control">
				<xsl:attribute name="operation">create</xsl:attribute>
				<xsl:element name="content">
					<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout">
					<xsl:element name="ctl_type">panel</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">fillParent</xsl:attribute>
						<xsl:attribute name="value">true</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<!-- code added to pass the parameter -->
				<xsl:apply-templates select="frameset|frame"/>
			</xsl:element>
		</xsl:when>
		<xsl:when test="../frame/@sizable='false'">
			<xsl:element name="control">
				<xsl:attribute name="operation">create</xsl:attribute>
				<xsl:element name="content">
					<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout">
					<xsl:element name="ctl_type">panel</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">top</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
								<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">left</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
								<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:if test="position()=1">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=2">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width2"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height2"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				<xsl:apply-templates select="frameset|frame"/>
			</xsl:element>
		</xsl:when>
		<xsl:when test="../frame/@sizable='true' or count(../frameset)=2">
			<xsl:element name="control">
				<xsl:attribute name="operation">create</xsl:attribute>
				<xsl:element name="content">
					<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout">
					<xsl:element name="ctl_type">pane</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">top</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
								<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">left</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
								<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:if test="position()=1">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=2">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width2"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height2"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				<xsl:apply-templates select="frameset|frame"/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="frameset|frame"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="frame">
	<xsl:choose>
		<xsl:when test="@sizable='false'">
			<xsl:element name="control">
				<xsl:attribute name="operation">create</xsl:attribute>
				<xsl:element name="content">
					<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout">
					<xsl:element name="ctl_type">panel</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">top</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
								<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">left</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
								<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:if test="position()=1">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=2">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width2"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height2"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				<xsl:apply-templates select="panel" mode="add"/>
			</xsl:element>
		</xsl:when>
		<xsl:when test="@sizable='true'">
			<xsl:element name="control">
				<xsl:attribute name="operation">create</xsl:attribute>
				<xsl:element name="content">
					<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout">
					<xsl:element name="ctl_type">pane</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">top</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='rows'">
								<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="layout_info">
						<xsl:attribute name="name">left</xsl:attribute>
						<xsl:choose>
							<xsl:when test="not(position()=1) and ../@rows-or-cols='cols'">
								<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="value">0</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
					<xsl:if test="position()=1">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width1"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height1"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
					<xsl:if test="position()=2">
						<xsl:element name="layout_info">
							<xsl:attribute name="name">width</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@width2"/></xsl:attribute>
						</xsl:element>
						<xsl:element name="layout_info">
							<xsl:attribute name="name">height</xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="../@height2"/></xsl:attribute>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				<xsl:apply-templates select="panel" mode="add"/>
			</xsl:element>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates select="panel" mode="add"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="panel" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">panel</xsl:element>
			<xsl:if test="alignment/@fill-parent='true'">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">fillParent</xsl:attribute>
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="position">
				<xsl:apply-templates select="position"/>
			</xsl:if>
		</xsl:element>
		<xsl:apply-templates select="panel|treeview|grid|optiongroup|checkbox|radiobutton|list-control|text-control|number-control|text-object|button|image-object|value-display|find-control" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="treeview" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">tree</xsl:element>
			<xsl:if test="alignment/@fill-parent='true'">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">fillParent</xsl:attribute>
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="position">
				<xsl:apply-templates select="position"/>
			</xsl:if>
			<xsl:apply-templates select="model-tree-icons"/>
		</xsl:element>
		<xsl:apply-templates select="treenode" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="model-tree-icons">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">open</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="@open"/></xsl:attribute>
	</xsl:element>
	<xsl:element name="layout_info">
		<xsl:attribute name="name">closed</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="@closed"/></xsl:attribute>
	</xsl:element>
	<xsl:element name="layout_info">
		<xsl:attribute name="name">open_unsatisfied</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="@open-unsatisfied"/></xsl:attribute>
	</xsl:element>
	<xsl:element name="layout_info">
		<xsl:attribute name="name">closed_unsatisfied</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="@closed-unsatisfied"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="treenode" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">treeNode</xsl:element>
			<xsl:if test="text">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="image">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">iconLocation</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="image/@src"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
		<xsl:apply-templates select="treenode" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="button" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">button</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="caption">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="caption/text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="image">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">iconLocation</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="image/@src"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="checkBox" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">checkBox</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="caption">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="caption/text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="radioButton" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">radioButton</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="caption">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="caption/text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="text-control" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">textBox</xsl:element>
			<xsl:apply-templates select="position"/>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="number-control" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">textBox</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="@spin-button">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">spinButton</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@spin-button"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@min-val">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">minVal</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@min-val"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@max-val">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">maxVal</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@max-val"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="text-object" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">label</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="text">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@active">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">active</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@active"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@multiline">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">multiLine</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@multiline"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="value-display" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">label</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="@text">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="image-object" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">image</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="image">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">location</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="image/@src"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@active">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">active</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@active"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="option-group" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">optionGroup</xsl:element>
			<xsl:apply-templates select="position"/>
			<xsl:if test="@control-type">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">optionStyle</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@control-type"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="areastyle/@bgcolor">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">bgColor</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="areastyle/@bgcolor"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="option" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">option</xsl:element>
			<xsl:if test="caption">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">caption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="caption/text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>					
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="find-control" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">panel</xsl:element>
			<xsl:apply-templates select="position"/>
		</xsl:element>
		<xsl:apply-templates select="text-control|button|image|text-object" mode="add"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="grid" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">grid</xsl:element>
			<xsl:if test="alignment/@fill-parent='true'">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">fillParent</xsl:attribute>
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="position">
				<xsl:apply-templates select="position"/>
			</xsl:if>
			<xsl:if test="@column-count">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">nCols</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@column-count"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@col-header">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">colHeader</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@col-headers"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@row-header">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">rowHeader</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@row-header"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates select="logic-state-display"/>
		</xsl:element>
		<xsl:apply-templates select="grid-column"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="logic-state-display">
	<xsl:apply-templates select="logic-icons"/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="logic-icons">
	<xsl:if test="@usage='check-list'">
		<xsl:element name="layout_info">
			<xsl:attribute name="name">ufalse</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@ufalse"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout_info">
			<xsl:attribute name="name">utrue</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@utrue"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout_info">
			<xsl:attribute name="name">unknown</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@unknown"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout_info">
			<xsl:attribute name="name">ltrue</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@ltrue"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout_info">
			<xsl:attribute name="name">lfalse</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@lfalse"/></xsl:attribute>
		</xsl:element>
	</xsl:if>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="grid-column">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridColumn</xsl:element>
			<xsl:if test="@index">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">iCol</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@index"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="caption">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">headerCaption</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="caption/text/text()"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@control-type">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">controlType</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@control-type"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:if test="@width">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">colWidth</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@width"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="row-data" mode="add">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="../@target"/>.<xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridRow</xsl:element>
		</xsl:element>
		<xsl:apply-templates select="col-data"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->

<xsl:template match="row-data" mode="insert">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="../@target"/>.<xsl:value-of select="@rtid"/></xsl:attribute>
			<xsl:attribute name="value"></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridRow</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name"><xsl:value-of select="../@relativePosition"/></xsl:attribute>
				<xsl:attribute
	name="value"><xsl:value-of select="
../@target"/>.<xsl:value-of select="../@relativeTarget"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:apply-templates select="col-data" mode="insert"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->

<xsl:template match="col-data">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="../@rtid"/>.<xsl:value-of select="@col-id"/></xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridCell</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">colObjId</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@col-id"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->

<xsl:template match="col-data" mode="update">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="../@row-id"/>.<xsl:value-of select="@col-id"/></xsl:attribute>
			<!--xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute-->
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridCell</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">value</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="col-data" mode="insert">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="../@rtid"/>.<xsl:value-of select="@col-id"/></xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">gridCell</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">colObjId</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@col-id"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<!--
<xsl:template match="model-update">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">treeNode</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">unsatisfied</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="unsatisfied/text()"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">branch_unsatisfied</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="branch-unsatisfied/text()"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:element>
</xsl:template>

-->
<!-- Rahul modified the above code as follows -->

<!-- ********************************************************************** -->
<xsl:template match="model-update">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@target"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:apply-templates select="unsatisfied|branch-unsatisfied|value"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="unsatisfied">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">unsatisfied</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="branch-unsatisfied">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">branch_unsatisfied</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="value">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">caption</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>


<!-- ********************************************************************** -->
<xsl:template match="config-messages">
	<xsl:element name="control">
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id">mainWindow</xsl:attribute>
		</xsl:element>
		<xsl:apply-templates select="contradiction-message|validation-message|warning-message|prompt-confirm-message|prompt-input-message|information-message"/>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="contradiction-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">contradiction</xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idYes</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-yes"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textYes</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-yes-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idNo</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-no"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textNo</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-no-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="prompt-text|title-text|message-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="validation-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">validation</xsl:attribute>
			</xsl:element>
			<xsl:if test="@id-ok">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">idOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout_info">
					<xsl:attribute name="name">textOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok-caption"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="title-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="warning-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">warning</xsl:attribute>
			</xsl:element>
			<xsl:if test="@id-ok">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">idOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout_info">
					<xsl:attribute name="name">textOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok-caption"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="warning-text|title-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-confirm-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">confirm</xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idConfirm</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-yes-ok"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textConfirm</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-yes-ok-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idNegate</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-no-cancel"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textNegate</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-no-cancel-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="prompt-text|title-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-input-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">input</xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idDone</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-ok"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textDone</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@id-ok-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">idCancel</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@idcancel"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">textCancel</xsl:attribute>
				<xsl:attribute name="value"><xsl:value-of select="@idcancel-caption"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="prompt-text|title-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="information-message">
	<xsl:element name="control">
		<xsl:attribute name="operation">create</xsl:attribute>
		<xsl:element name="content">
			<xsl:attribute name="ui_obj_id"><xsl:value-of select="@rtid"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="layout">
			<xsl:element name="ctl_type">messageBox</xsl:element>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">msgType</xsl:attribute>
				<xsl:attribute name="value">information</xsl:attribute>
			</xsl:element>
			<xsl:if test="@id-ok">
				<xsl:element name="layout_info">
					<xsl:attribute name="name">idOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok"/></xsl:attribute>
				</xsl:element>
				<xsl:element name="layout_info">
					<xsl:attribute name="name">textOk</xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="@id-ok-caption"/></xsl:attribute>
				</xsl:element>
			</xsl:if>
			<xsl:element name="layout_info">
				<xsl:attribute name="name">errText</xsl:attribute>
				<xsl:attribute name="value"><xsl:apply-templates select="error-text"/></xsl:attribute>
			</xsl:element>
			<xsl:apply-templates select="message-text|title-text"/>
		</xsl:element>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="message-text">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">msgText</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="error-text">
	<xsl:if test="position() > 2">
		<xsl:text>|</xsl:text>
	</xsl:if>
	<xsl:apply-templates/>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="prompt-text">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">promptText</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="title-text">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">title</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>

<!-- ********************************************************************** -->
<xsl:template match="warning-text">
	<xsl:element name="layout_info">
		<xsl:attribute name="name">warnText</xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="text()"/></xsl:attribute>
	</xsl:element>
</xsl:template>


<!-- ********************************************************************** -->
<xsl:template match="position">
	<xsl:if test="@width">
		<xsl:element name="layout_info">
			<xsl:attribute name="name">width</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@width"/></xsl:attribute>
		</xsl:element>
	</xsl:if>
	<xsl:if test="@height">
		<xsl:element name="layout_info">
			<xsl:attribute name="name">height</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@height"/></xsl:attribute>
		</xsl:element>
	</xsl:if>
	<xsl:if test="@top">
		<xsl:element name="layout_info">
			<xsl:attribute name="name">top</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@top"/></xsl:attribute>
		</xsl:element>
	</xsl:if>
	<xsl:if test="@left">
		<xsl:element name="layout_info">
			<xsl:attribute name="name">left</xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="@left"/></xsl:attribute>
		</xsl:element>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
