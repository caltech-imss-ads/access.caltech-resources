//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        apwdflex.js                       |
//  |                                                |
//  | Description: Client-side class definition      |
//  |              for descriptive flexfields        |     
//  |                                                |
//  | History:     Created 02/29/2000                |
//  |              David Tong                        |
//  |                                                |
//  +================================================+
/* $Header: APWDFLEX.js 115.23 2001/02/13 13:01:24 pkm ship  $ */

var C_strDFLEXNAME = "DFLEX";
var C_HIDDEN = "HIDDEN";
var C_TEXTBOX = "TEXT";
var C_POPLIST = "SELECT";
var C_CHECKBOX = "CHECKBOX";
var C_strGLOBALCONTEXTNAME = "Global Data Elements";



function fPaintDFFButton(docT){
  var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_DETAIL');

    docT.writeln("<TD>");
    docT.writeln("  <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
    docT.writeln("  document.write(top.opener.top.fDynamicButton('"+tempRegion.mobjGetRegionItem('AP_WEB_CALC_AMOUNT').mstrGetPrompt()+"',");
    docT.writeln("                                  \"APWBRNDL.gif\",");
    docT.writeln("                                  \"APWBRNDR.gif\",");
    docT.writeln("                                  '"+tempRegion.mobjGetRegionItem('AP_WEB_CALC_AMOUNT').mstrGetPrompt()+"',");
    docT.writeln("                                  \"javascript:top.opener.top.CalculateAmountHandler()\",");
    docT.writeln("                                  \""+ top.g_strLangCode+"\",");
    docT.writeln("                                  false,");
    docT.writeln("                                  false,");
    docT.writeln("                                  'top.g_winDetail.flexFields'));");
    docT.writeln("  <\/SCRIPT>");
    docT.writeln("</TD>");
}

function fPaintFlexField(p_objFlexField, p_framTarget, p_strValue, p_bCalAmtFlag) {
  var iType;
  var docT;
  var strPrompt, strWidget, strWidgetName;
  var strReqImage = "";

  if (p_objFlexField.mGetbIsRequired()) strReqImage = "<img src=\""+top.g_strImgDir+"APWREQUI.gif\">";

  iType = p_objFlexField.mGetiObjectType();
  docT = p_framTarget.document;

  if (iType == C_TEXTBOX) {
    strPrompt = p_objFlexField.mGetstrPrompt();
    strWidgetName = top.ReplaceInvalidChars(strPrompt);

    strWidget = "<INPUT TYPE=TEXT LENGTH=" + p_objFlexField.mGetiTextBoxLength() + " MAXLENGTH=240 NAME=\"" + strWidgetName + 
		"\" onChange = \"javascript:top.fStoreGlobalDFFValues(); top.fStoreSensitiveDFFValues();\"";
    strWidget += " VALUE=\""+p_strValue + "\" >";
    docT.writeln("<TR><TD>"+strReqImage+"<FONT CLASS=PROMPTBLACK>" + strPrompt + "</FONT></TD><TD><FONT CLASS=PROMPTBLACK>" + strWidget + "</FONT></TD>");
    
    if (p_bCalAmtFlag) top.fPaintDFFButton(docT);
 
    docT.writeln("</TR>");
  }
  else if (iType == C_HIDDEN) {
    strPrompt = p_objFlexField.mGetstrPrompt(); 
    strWidgetName = top.ReplaceInvalidChars(strPrompt);
   
    strWidget = "<INPUT TYPE=HIDDEN NAME=\"" + strWidgetName + 
  		"\" onChange = \"javascript:top.fStoreGlobalDFFValues(); top.fStoreSensitiveDFFValues();\"";
    strWidget += " VALUE=\""+p_strValue + "\" >";
    docT.writeln(strWidget);
  }
  else if (iType == C_POPLIST) {
    var objValues, objValue;

    strPrompt = p_objFlexField.mGetstrPrompt();
    strWidgetName = top.ReplaceInvalidChars(strPrompt);
    strWidget = "<SELECT NAME=\"" + strWidgetName + 
		"\" onChange = \"javascript:top.fStoreGlobalDFFValues(); top.fStoreSensitiveDFFValues();\"" +
		" >";
    docT.writeln("<TR><TD>"+strReqImage+"<FONT CLASS=PROMPTBLACK>" + strPrompt + "</FONT></TD><TD>" + strWidget);
    docT.writeln("<OPTION VALUE=\"\"></OPTION>");

    objValues = p_objFlexField.mGetobjPoplistValues();

    for(var i=0; i<objValues.miGetNumOfValues(); i++) {
      objValue = objValues.arrValue[i];
      docT.write("<OPTION VALUE=\"" + objValue.mGetstrCode() + "\"");
      if (objValue.mGetstrCode() == p_strValue) docT.write(" SELECTED");
      docT.writeln(">" + objValue.mGetstrDesc() + "</OPTION>");
    }
    docT.writeln("</SELECT></FONT></TD>");
    if (p_bCalAmtFlag) top.fPaintDFFButton(docT);
    docT.writeln("</TR>");
  }
  else if (iType == C_CHECKBOX) {
    strPrompt = p_objFlexField.mGetstrPrompt();
    strWidgetName = top.ReplaceInvalidChars(strPrompt);
    strWidget = "<INPUT TYPE=CHECKBOX NAME=\"" + strWidgetName + 
		"\" onClick = \"javascript:top.fStoreGlobalDFFValues(); top.fStoreSensitiveDFFValues();\"";
    if (p_strValue){
	if (p_strValue == 'Y') strWidget += " CHECKED= true>"; 
	else strWidget += " >";
    }
    else strWidget += ">";

    docT.writeln("<TR><TD></TD><TD>"+strReqImage+"<FONT CLASS=PROMPTBLACK>" + strWidget + strPrompt + "</FONT></TD>");
    if (p_bCalAmtFlag) top.fPaintDFFButton(docT);
    docT.writeln("</TR>");
  }
}



function fPaintFlex(p_strContextID, iRecIndex) {
  if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_DESC_FLEX_NAME')=='N'){
    return;
  }

  var p_framTarget = eval('top.g_winDetail.flexFields');
  if (p_strContextID){}
  else {
    p_framTarget.document.open();
    p_framTarget.document.writeln(top.fBlankFlexFrame());
    p_framTarget.document.close();
    return;
  }
 
  var objGlobalContext, objContext;
  if (iRecIndex < 0){
    top.g_bFlexFrameLoaded = false;
    p_framTarget.document.open();
    p_framTarget.document.writeln(top.fBlankFlexFrame());
    p_framTarget.document.close();
    return;
  }
  
  top.g_bFlexFrameLoaded = false;

  var objRec = top.fGetReceiptObj(iRecIndex);
  var p_strContext;
  if (objRec.getObjExpenseType())
      p_strContext = objRec.getObjExpenseType().mGetstrCode();
  var objRecDFF = objRec.flexFields;
  var bCalAmtFlag, bGlobalCalAmtFlag = false;
  p_framTarget.document.open();
  p_framTarget.document.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
  p_framTarget.document.writeln("<HTML dir=\""+top.g_strDirection+"\">");
  top.fGenerateStyleCodes(top.g_winDetail.flexFields);
  p_framTarget.document.writeln("<BODY class=colorg5 onLoad=\"top.opener.top.g_bFlexFrameLoaded = true;self.defaultStatus=''; return true;\"><FORM NAME=\"flexForm\" onSubmit=\"return(false)\"><TABLE BORDER=0 CELLSPACING=0>");
  if (objRecDFF){
    objContext = g_objContexts.mobjGetContextWithDefault(p_strContext);
    if (objRec.getObjExpenseType().mGetbCalAmtFlag()) bCalAmtFlag = true;

    // populate global segments 
    // ------------------------
    objGlobalContext = g_objContexts.mobjGetContextWithDefault(top.g_strGlobalContext);
    //if (objGlobalContext.mGetbCalAmtFlag()) bGlobalCalAmtFlag = true;
    for(var i=0; i<objGlobalContext.miGetNumOfFlexField(); i++) {  
      if (objRecDFF[i])
	if ((i== objGlobalContext.miGetNumOfFlexField()-1) && (objGlobalContext.miGetNumOfFlexField() <= i))
         fPaintFlexField(objGlobalContext.arrFlexFields[i], p_framTarget, objRecDFF[i].mGetValue(),bCalAmtFlag);
	else
	 fPaintFlexField(objGlobalContext.arrFlexFields[i], p_framTarget, objRecDFF[i].mGetValue(),false);
      
    }
    var iNumofGlobalContext = i;

    // populate context sensitive segments
    // -----------------------------------
    for(var j=0; j<objContext.miGetNumOfFlexField(); j++) {  
      if (objRecDFF[j+iNumofGlobalContext]){
	if (j == objContext.miGetNumOfFlexField()-1){
      	  fPaintFlexField(objContext.arrFlexFields[j], p_framTarget, objRecDFF[j+iNumofGlobalContext].mGetValue(), bCalAmtFlag);
	}
	else 
	  fPaintFlexField(objContext.arrFlexFields[j], p_framTarget, objRecDFF[j+iNumofGlobalContext].mGetValue(), false);
	}

    }
  }// objRecDFF is not null
  p_framTarget.document.writeln("</TABLE></FORM>");
  p_framTarget.document.writeln("<SCR" + "IPT LANGUAGE=\"JavaScript\">");
  p_framTarget.document.writeln("top.opener.top.g_bFlexFrameLoaded=true;");
  p_framTarget.document.writeln("function evalstr(str) { eval(str); }");
  p_framTarget.document.writeln("</SCR" + "IPT></BODY></HTML>");
  p_framTarget.document.close();

}


function fSerializeDFlex(p_objReceipt) {
var l_strSerializeString = ""; 
  if (p_objReceipt.getObjExpenseType()){
    if (!p_objReceipt.flexFields){
	 p_objReceipt.flexFields = new Array();
  	 fAddFlexFields(p_objReceipt, p_objReceipt.getObjExpenseType().mGetstrCode());
    }
   }
// chiho:populate the values in the l_strSerialize even when expense type
// was undefined.

   var objRecFF = p_objReceipt.flexFields;
   for (var i=0;i<top.g_iNumMaxFlexField;i++){
   if (objRecFF[i]){
	l_strSerializeString += objRecFF[i].mGetValue() + "@att@";
   }
   else
	l_strSerializeString += "@att@";
   }
 
   return l_strSerializeString;  
}


function fBlankFlexFrame() {
        return "<HTML dir=\""+top.g_strDirection+"\"><BODY BGCOLOR=#cccccc onLoad = \"top.opener.top.g_bFlexFrameLoaded = true;\"></BODY></HTML>"
	
}


function fFocusOnFlexField(strName,nLine, iTab){
// iTab == 1 : OOP
// iTab == 2 : CC
   if ((iTab == 1) && (!fIsOOP())) fSwitchTabs('OOP');  
   else if ((iTab == 2) && (fIsOOP())) fSwitchTabs('CC');

   var tempField = null;
   if ((top.g_winDetail==null) || (top.g_winDetail.closed)){
     if (iTab == 1) top.fOpenDetail('OOP',nLine);
     else top.fOpenDetail('CC',nLine);
   }
   else{
 	top.fChangeReceiptDetail(nLine);
    	tempField = eval('top.g_winDetail.flexFields.document.flexForm.'+strName);
    	tempField.focus();
   }
}

function fGetWidgetValue(p_objWidget, p_strWidgetType){
  if (p_strWidgetType == 'SELECT') {
    return p_objWidget.options[p_objWidget.selectedIndex].value;
  }
  else if (p_strWidgetType == 'CHECKBOX') {
    if (p_objWidget.checked) 
      return 'Y';
    else
      return 'N';
  }
  else {
    if (p_objWidget) return p_objWidget.value;
    else return '';
  }
}

// Given a receipt object and expense type name, add flex fields that belong to the exepense type to the receipt object
function fAddFlexFields(p_objReceipt, p_strExpenseTypeName){
	var objFF, tempFFObj;
	p_objReceipt.flexFields.length = 0;
	if (top.g_strGlobalContext){
	    var objContext = top.g_objContexts.mobjGetContext(top.g_strGlobalContext);
	    if (objContext) 
 		for (var i=0;i<objContext.miGetNumOfFlexField();i++){
   		objFF = objContext.arrFlexFields[i];
   		tempFFObj = new flexFieldValues(objFF.mGetstrPrompt(),objFF.mGetstrDefaultValue());
	        p_objReceipt.addFlexFields(tempFFObj);
	        }
	}
	objContext = top.g_objContexts.mobjGetContext(p_strExpenseTypeName);
	if (objContext) {
 		for (var i=0;i<objContext.miGetNumOfFlexField();i++){
   		    objFF = objContext.arrFlexFields[i];
   		    tempFFObj = new flexFieldValues(objFF.mGetstrPrompt(),objFF.mGetstrDefaultValue()); 
	            p_objReceipt.addFlexFields(tempFFObj);
		}//end for
	}//end objContext
}




