/* $Header: czEvtMgr.js 115.17 2001/07/03 10:56:13 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function EventManager(name)
{
  if (name) {
    if (EventManager.instances[name]) 
      return(EventManager.instances[name]);
    else 
      EventManager.instances[name] = this;
  } else {
    if(self.singleEMInstance == null)
      self.singleEMInstance = this;
    else 
      return self.singleEMInstance;
  }
  this.locked = false;
  this.components = new Array();
  this.tabComps = new Array();
  
  this.onmouseup = _doc_onmouseup;
  this.onmousedown = _doc_onmousedown;
  this.onmousemove = _doc_onmousemove;
  this.onmouseover = _doc_onmouseover;
  this.onmouseout = _doc_onmouseout;
  this.ondblclick = _doc_ondblclick;
  this.onkeydown = _doc_onkeydown;
  this.onkeyup = _doc_onkeyup;
  this.onfocus = _window_onfocus;
  
  this.active = false;
  this.mouseover_id = null;
  this.msdown = false;
  
  EventManager.Count++;
  this.self = 'EventManager'+EventManager.Count;
}

function EventManager_init(doc)
{	
  if (doc)
    this.doc = doc;
  else
    this.doc = document;
  
  if (ns4) {
    //check for NS4.0; 
    this.doc.captureEvents(Event.MOUSEUP | Event.MOUSEDOWN | 
                           Event.MOUSEMOVE| Event.MOUSEOVER | Event.MOUSEOUT | 
                           Event.DBLCLICK | Event.KEYDOWN | Event.KEYUP);
  }
  eval(this.self + '=this');
  
  this.doc.onmouseup = new Function('event','return '+this.self+ '.onmouseup(event);');
  this.doc.onmousedown = new Function('event','return '+this.self+ '.onmousedown(event);');
  this.doc.onmousemove = new Function('event','return '+this.self+ '.onmousemove(event);');
  this.doc.onmouseover = new Function('event','return '+this.self+ '.onmouseover(event);');
  this.doc.onmouseout  = new Function('event','return '+this.self+ '.onmouseout(event);');
  this.doc.onkeydown = new Function('event','return '+this.self+ '.onkeydown(event);'); 
  this.doc.onkeyup = new Function('event','return '+this.self+ '.onkeyup(event);');
  this.doc.ondblclick  = new Function('event','return '+this.self+ '.ondblclick(event);');
}

function EventManager_lock()
{
  this.locked = true;
}

function EventManager_unLock()
{
  this.locked = false;
}

function EventManager_add()
{
  for (var i=0; i<arguments.length; i++) {
    if(arguments[i])
      this.components[arguments[i].getId()] = arguments[i];
  }
}

function EventManager_getElementID(e)
{
  var id = null;
  if (ns4) {
    //check for ns4;
    id = e.target.id;
    if(!id) if(e.target.name)
      id = e.target.name;
  }
  else if (ie4) //check for ie4
    id = e.srcElement.id;

  if(id) {
    var dashPos = id.indexOf('-');
    while(dashPos != -1) {
      id = id.slice(dashPos + 1);
      dashPos = id.indexOf('-');
    }
  }
  return id;
}

function EventManager_clearInstance (name)
{
  if (name) {
    if (EventManager.instances[name]) {
      EventManager.instances[name].destroy ();

      EventManager.instances[name] = null;
      delete EventManager.instances[name];
    }
  }
}


function EventManager_setCompMgr ( compMgr )
{
  this.compMgr = compMgr;
}

function EventManager_hitTest (e)
{
  var objId;
  if ( this.compMgr ) {
    for (objId in this.compMgr.compHash) {
      var objId = this.compMgr.compHash[objId].hitTest (e);
      if (objId) {
        return objId;
      }
    }
  }
  return null;
}

function EventManager_addTabComp(comp)
{
  if (comp.tabindex > 0)
    this.tabComps[comp.tabindex] = comp;
}

//move focus from comp to next 
function EventManager_focusForward(e, comp)
{
  var tabInd = comp.tabindex;
  if (this.tabComps[tabInd+1]) {
    this.tabComps[tabInd+1].setFocus();
    e.returnValue = false;
  }
}

//move focus backwards from comp to previous
function EventManager_focusBackward(e, comp)
{
  var tabInd = comp.tabindex;
  if (tabInd <= 1) {
    this.doc.parentWindow.focus();
    return;
  }
  if (this.tabComps[tabInd-1]) {
    this.tabComps[tabInd-1].setFocus();
    e.returnValue = false;
  } else
    this.doc.parentWindow.focus();
}

function EventManager_destroy ()
{
  if (! this.bDestroyed) {
    this.tabComps = null;
    delete this.tabComps;

    var id;
    for (id in this.components) {
      if (this.components[id].destroy) {
        this.components[id].destroy ();
      }
      this.components[id] = null;
      delete this.components[id];
    }
    this.components = null;
    delete this.components;
    
    this.compMgr = null;
    delete this.compMgr;
    
    this.bDestroyed = true;
  }
}

/***********************
//global event handlers
************************/
function _doc_onmouseup(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }

  var res =true;
  if(ie4) e = this.doc.parentWindow.event;

  res = this.notifyListeners('docMouseUp',e);
  if (res) {
    if (! this.mouseover_id) {
      if (ns4) {
        this.mouseover_id = this.hitTest (e); 
      } else if (ie4) {
        this.mouseover_id = EventManager.getElementID(e);
      }
    }
  }
  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])
      res = this.components[this.mouseover_id].doMouseUp(e);

  this.msdown = false;
  return res;
}		

function _doc_onmousedown(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }

  var res = true;
  if(ie4) e = this.doc.parentWindow.event;
  this.msdown = true;

  res= this.notifyListeners('docMouseDown',e);
  if (res) {
    if (! this.mouseover_id) {
      if (ns4) {
        this.mouseover_id = this.hitTest (e);
      } else if (ie4) {
        this.mouseover_id = EventManager.getElementID(e);
      }
    }
  }
  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])			
      res = this.components[this.mouseover_id].doMouseDown(e);

  return res;
}

function _doc_onmouseover(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    //	return false;
  }
  var res = true;
  if(ie4) e = this.doc.parentWindow.event;
  
  res = this.notifyListeners('docMouseOver',e);
  
  this.mouseover_id = EventManager.getElementID(e);
  if (res && this.mouseover_id)
    if(this.components[this.mouseover_id])		
      res=this.components[this.mouseover_id].doMouseOver(e);
  return res;
}

function _doc_onmouseout(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    //	return false;
  }

  var res =true;
  if(ie4) e = this.doc.parentWindow.event;
  
  if(self.Tooltip)
    Tooltip.hide();
  
  res = this.notifyListeners('docMouseOut',e);	
  if(res && this.mouseover_id)
    if(this.components[this.mouseover_id]) {
      res = this.components[this.mouseover_id].doMouseOut(e);
    }
  this.mouseover_id = null;
  return res;
}

function _doc_onmousemove(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    return false;
  }

  var res=true;
  if(ie4) e = this.doc.parentWindow.event;
  
  this.lastX = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
  this.lastY = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

  res = this.notifyListeners('docMouseMove',e);	
  if(res && this.mouseover_id) {
    if(this.components[this.mouseover_id]) 
      res = this.components[this.mouseover_id].doMouseMove(e);
  }
  return res;	 
}

function _doc_ondblclick (e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  var res = true;

  if (ie4) e = this.doc.parentWindow.event;

  res = this.notifyListeners('docDoubleClick',e);	

  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])			
      res = this.components[this.mouseover_id].doDoubleClick(e);

  return res;
}

function _doc_onkeydown (e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  if(ie4)e = this.doc.parentWindow.event;

  if(ns4) 
    var key = e.which;
  else 
    var key = e.keyCode;

  this.notifyListeners ('onKeyDownCallback', e);
  return true;
}

function _doc_onkeyup(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  if(ie4)e = this.doc.parentWindow.event;

  if(ns4) 
    var key = e.which;
  else 
    var key = e.keyCode;

  this.notifyListeners ('onKeyUpCallback', e);

  switch(key){
  case 13:
    this.notifyListeners('docEnterKeyUp',e);
    break;
  case 9:
  case 20:
  case 84:
    this.notifyListeners('docTabKeyUp',e);
    break;
  }
  return true;
}

function _window_onfocus(e)
{
  if (ns4) return;
  e = this.doc.parentWindow.event;
  if (! e.shiftKey) {
    var len = this.tabComps.length;
    for (var i=0; i<len; i++) {
      if (this.tabComps[i] && this.tabComps[i].resetFocus)
        this.tabComps[i].resetFocus();
    }
  }
}

/*************************************/

//Extending from Listener implementation
HTMLHelper.importPrototypes(EventManager,Listener);

//static methods
EventManager.Count =0;
EventManager.instances = new Array ();
EventManager.getElementID = EventManager_getElementID;
EventManager.clearInstance = EventManager_clearInstance;

EventManager.prototype.lock = EventManager_lock;
EventManager.prototype.unLock = EventManager_unLock;
EventManager.prototype.add = EventManager_add;
EventManager.prototype.init = EventManager_init;
EventManager.prototype.hitTest = EventManager_hitTest;
EventManager.prototype.setCompMgr = EventManager_setCompMgr;
EventManager.prototype.destroy = EventManager_destroy;

EventManager.prototype.addTabComp = EventManager_addTabComp;
EventManager.prototype.focusForward = EventManager_focusForward;
EventManager.prototype.focusBackward = EventManager_focusBackward;
