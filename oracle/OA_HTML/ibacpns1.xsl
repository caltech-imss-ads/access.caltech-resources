<?xml version="1.0"?>
<!--$Header: ibacpns1.xsl 115.5 2000/11/01 11:47:26 pkm ship   $-->

<!--This stylesheet is for rendering Camapign by its Full Banner image. If a Full Banner-->
<!--image is not available, Half Banner image is tried for. Otherwise, a default image is -->
<!--shown alongwith short description. The attachment types expected by this stylesheet are-->
<!--IMG_FULL_BANNER, IMG_HALF_BANNER and TXT_SHORT_DESC-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/">
  <table border="0" cellpadding="1" cellspacing="2">
    <xsl:apply-templates select="//Campaign"/>
  </table>
</xsl:template>

<xsl:template match="Campaign">
  <tr>
    <td>
      <table border="0" cellpadding="2" cellspacing="0" align="center">
	   <tr></tr>
        <xsl:choose>
          <xsl:when test="AttachmentLst/Attachment/AttachmentSubType='IMG_FULL_BANNER'">
            <!--got a full banner image for this Campaign-->
            <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'IMG_FULL_BANNER')]">
              <tr>
                <td> 
                  <xsl:call-template name="IMAGE"/>
                </td>
              </tr>
            </xsl:for-each>
          </xsl:when>
          <xsl:when test="AttachmentLst/Attachment/AttachmentSubType='IMG_HLF_BANNER'">
            <!--got a half banner image for this Campaign-->
            <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'IMG_HLF_BANNER')]">
              <tr>
                <td> 
                  <xsl:call-template name="IMAGE"/>
                </td>
              </tr> 
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <!--no full or half banner image available for this Campaign-->
            <xsl:for-each select="AttachmentLst/Attachment[starts-with(AttachmentSubType, 'TXT_DESC')]">
              <tr>
                <td> 
                  <xsl:call-template name="TEXT"/>
                </td>
              </tr> 
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
      </table>
    </td>
  </tr>
</xsl:template>


<xsl:template name="IMAGE">
  <xsl:variable name="height">
    <xsl:value-of select="floor(child::DisplayHeight)"/>
  </xsl:variable>
  <xsl:variable name="width">
    <xsl:value-of select="floor(child::DisplayWidth)"/>
  </xsl:variable>
  <xsl:variable name="url">
    <xsl:value-of select="child::linkURL"/>
  </xsl:variable>
  <xsl:element name="a">
   <xsl:if test="boolean(normalize-space($url))">
     <xsl:attribute name="href">
       <xsl:value-of select="child::linkURL"/> 
     </xsl:attribute>
   </xsl:if>
    <xsl:element name="img">
      <xsl:attribute name="src">
        <xsl:value-of select="child::FileLocation"/>
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="child::AlternateText"/>
      </xsl:attribute>
      <!--add height attribute if non-zero and number-->
      <xsl:if test="not($height='NaN')">
        <xsl:if test="not($height='0')">
          <xsl:attribute name="height">
           <xsl:value-of select="$height"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:if>
      <!--add width attribute if non-zero and number-->
      <xsl:if test="not($width='NaN')">
        <xsl:if test="not($width='0')">
          <xsl:attribute name="width">
           <xsl:value-of select="$width"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:if>
      <xsl:attribute name="border">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="TEXT">
      <xsl:variable name="url">
        <xsl:value-of select="child::linkURL"/>
      </xsl:variable>
      <xsl:element name="a">
        <xsl:element name="img">
          <xsl:attribute name="src">
            <!--default image source file name-->
            <xsl:text></xsl:text>
          </xsl:attribute>
          <xsl:attribute name="alt">
            <!--default image alternate text-->
            <xsl:text></xsl:text>
          </xsl:attribute>
        </xsl:element>
      </xsl:element>
      <br>
      </br>
      <!--place the Campaign's short description here-->	 
      <xsl:element name="a">
        <xsl:if test="boolean(normalize-space($url))">
          <xsl:attribute name="href">
            <xsl:value-of select="child::linkURL"/> 
          </xsl:attribute>
        </xsl:if>
        <xsl:value-of select="child::DisplayText"/>
      </xsl:element>
</xsl:template>

<xsl:template match="*"></xsl:template>

</xsl:stylesheet>





