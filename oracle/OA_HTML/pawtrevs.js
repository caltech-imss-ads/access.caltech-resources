//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtrevs.js                       |
//  |                                                |
//  | Description: Client-side definition            |
//  |              for reversal selection            |     
//  +================================================+
/* $Header: pawtrevs.js 115.4 2000/12/01 14:51:06 pkm ship    $ */

var g_bFromTimeEntryToReversal = false;

//  --------------------------------------------------
//  Function: fDrawReversal
//  Description: It draws the Reversal page
//
//  --------------------------------------------------
function fDrawReversal(){
  g_bFromTimeEntryToReversal = true;

  fDrawToolbarFrame(top.framToolbar, top.C_strREVERSE);
  fDrawRevBodyContent(top.framMainBody);
  fDrawButtonFrame(top.framButtons, top.C_strREVERSE, g_strCurrentAction);
}

function fDrawRevBodyContent(p_framTarget) {

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="'+ g_strHTMLLangPath + 'pawscabo.css">');
  p_framTarget.document.writeln('<LINK REL=stylesheet TYPE="text/css" HREF="'+ g_strHTMLLangPath + 'pawstime.css">');
  p_framTarget.document.writeln('</HEAD>');
  p_framTarget.document.writeln('<BODY class=panel>');

  fDrawRevBodyHeader(p_framTarget);
  fDrawRevBodyLines(p_framTarget);
  fDrawRevBodyButton(p_framTarget);

  p_framTarget.document.writeln('</BODY>');
  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();

}


function fDrawRevBodyHeader(p_framTarget)
{
  p_framTarget.document.writeln('<TABLE class=panel CELLPADDING=5 BORDER=0 CELLSPACING=0 WIDTH="100%">');
  p_framTarget.document.writeln('<TR><TD ALIGN="MIDDLE">');
  p_framTarget.document.writeln('<TABLE CLASS=panel CELLPADDING=5 BORDER=0 CELLSPACING=0 WIDTH="100%">');

  fDrawReviewHeaderSummary(
		p_framTarget,
		g_objSingleEntryTimecard.mGetnTotalHours(),
		g_objSingleEntryTimecard.mGetobjHeader(),
		top.C_strREVERSE);

  p_framTarget.document.writeln('</TABLE>');
  p_framTarget.document.writeln('</TD></TR></TABLE>');
}

function fDrawRevBodyLines(p_framTarget)
{

  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_REVERSAL_SELECTION");
  var l_objSingleEntryLines = g_objSingleEntryTimecard.mGetobjSingleEntryLines();
  var l_bIsDFlexDisplayed = ((fbIsDFlexUsed()) && (fbIsDFlexVisible()));
  var l_strCheck;

  p_framTarget.document.writeln('<DIV align=center>');
  p_framTarget.document.writeln('<FORM name=formHourEntry>');
  p_framTarget.document.writeln('<CENTER>');
  p_framTarget.document.writeln('<TABLE cellpadding=0 cellspacing=0 border=0 width=98% align=CENTER>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('    <TD bgcolor=black width=100%><IMG src="' + g_strImagePath + 'FNDPX1.gif" height=1 width=1></TD>');
  p_framTarget.document.writeln('    <TD></TD>');
  p_framTarget.document.writeln('    <TD rowspan=2></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD align=left><IMG src="' + g_strImagePath + 'FNDRTCTL.gif"></TD>');
  p_framTarget.document.writeln('    <TD class=TABLEROWHEADER width=750 nowrap><IMG src="' + g_strImagePath + 'FNDPX4.gif" width=700 height=1></TD>');
  p_framTarget.document.writeln('    <TD align=right><IMG src="' + g_strImagePath + 'FNDRTCTR.gif"></TD>');
  p_framTarget.document.writeln('  </TR>');
  p_framTarget.document.writeln('  <TR>');
  p_framTarget.document.writeln('    <TD bgcolor=black><IMG src="' + g_strImagePath + 'FNDPX1.gif"></TD>');
  p_framTarget.document.writeln('    <TD class=tablesurround colspan=3>');
  p_framTarget.document.writeln('      <TABLE CELLPADDING=2 CELLSPACING=1 BORDER=0 width=100%>');
  p_framTarget.document.writeln('      <TR>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_REVERSE").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_PROJECT_NUMBER").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_TASK_NUMBER").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_EXP_TYPE").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_DAY").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_DATE").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_HOURS").mGetstrLabel() + '</TD>');
  p_framTarget.document.writeln('        <TD class=TABLEROWHEADER>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_HEADER_COMMENT").mGetstrLabel() + '</TD>');

  // DFlex column header
  if (l_bIsDFlexDisplayed) {
    p_framTarget.document.writeln('<TD CLASS=TABLEROWHEADER>'+ 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_ADDITIONAL_INFO").mGetstrLabel() + '</TD>');
  }

  p_framTarget.document.writeln('      </TR>');
  
  // Draw timecard entries
  for ( var i=0; i<l_objSingleEntryLines.miGetNumLines(); i++ )
  {

    if (l_objSingleEntryLines.arrSingleEntryLine[i].mGetbIsReversed())
      l_strCheck = "CHECKED";
    else 
      l_strCheck = "";

    p_framTarget.document.writeln('<TR>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL ALIGN=CENTER><input type="CHECKBOX"  ' + l_strCheck + '  name=cbEntry'+i+ '></TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetstrProjectNum() + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetstrTaskNum() + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetobjType().mGetstrExpenditureType() + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		fIntToDayOfWeek(l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetnDayOfWeek()) + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		fDateToLongString(g_strSessDateFormat, fDayOfWeekToDate(l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetnDayOfWeek())) + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELLRIGHT>' + 
		fFormatDecimals(l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetobjDay().mGetnHours()) + '</TD>');
    p_framTarget.document.writeln('<TD CLASS=TABLECELL>' + 
		fFormatString(l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetobjDay().mGetstrComment())+'</TD>');

    // DFlex values
    if (l_bIsDFlexDisplayed) {
      p_framTarget.document.writeln('<TD CLASS=TABLECELL VALIGN="TOP">');
      fDisplayDFlex(p_framTarget, 
		fGetContSensField(l_objSingleEntryLines.mGetarrSingleEntryLine()[i]), 
		l_objSingleEntryLines.mGetarrSingleEntryLine()[i].mGetobjDay().arrDFlex);
      p_framTarget.document.writeln('</TD>');
    }
    p_framTarget.document.writeln('</TR>');
  }

  p_framTarget.document.writeln('</TABLE>');
  p_framTarget.document.writeln('</TD>');
  p_framTarget.document.writeln('  <TD bgcolor=white><IMG src="' + g_strImagePath + 'FNDPX6.gif"></TD>');
  p_framTarget.document.writeln('</TR>');
  p_framTarget.document.writeln('<TR>');
  p_framTarget.document.writeln('  <TD rowspan=2></TD>');
  p_framTarget.document.writeln('  <TD><IMG src="' + g_strImagePath + 'FNDRTCBL.gif"></TD>');
  p_framTarget.document.writeln('  <TD class=TABLEROWHEADER width=1000><IMG src="' + g_strImagePath + 'FNDPX3.gif"></TD>');
  p_framTarget.document.writeln('  <TD><IMG src="' + g_strImagePath + 'FNDRTCBR.gif"></TD>');
  p_framTarget.document.writeln('  <TD rowspan=2></TD>');
  p_framTarget.document.writeln('</TR>');
  p_framTarget.document.writeln('<TR>');
  p_framTarget.document.writeln('  <TD></TD>');
  p_framTarget.document.writeln('  <TD bgcolor=white width=1000><IMG src="' + g_strImagePath+ 'FNDPX6.gif"></TD>');
  p_framTarget.document.writeln('  <TD></TD>');
  p_framTarget.document.writeln('</TR>');
  p_framTarget.document.writeln('</TABLE>');
  p_framTarget.document.writeln('</CENTER>');
  p_framTarget.document.writeln('</FORM>');
  p_framTarget.document.writeln('</DIV>');
}


function fDrawRevBodyButton(p_framTarget)
{
  var l_objCurRegion = g_objAKRegions.mobjGetRegion("PA_WEB_REVERSAL_SELECTION");
                                                   
  p_framTarget.document.writeln('<TABLE cellpadding=3 cellspacing=5 border=0><TR VALIGN=TOP><TD>');
  fDrawButton(
	p_framTarget, 
	C_strROUND_ROUND, 
    	l_objCurRegion.mobjGetRegionItem("PA_WEB_SELECT_ALL_LINES").mGetstrLabel(), 
	"top.fOnClickSelectAllLines(top.g_objSingleEntryTimecard.mGetobjSingleEntryLines())", 
	C_bBUTTON_ENABLED, 
    	C_strGRAYBG);
  p_framTarget.document.writeln('</TD></TR></TABLE>');
}


function fOnClickSelectAllLines(p_objSingleEntryLines)
{
  var cbCurEntry;

  for ( var i=0; i<p_objSingleEntryLines.miGetNumLines(); i++ )
  {
     cbCurEntry = fGetInputWidget(top.framMainBody.document.formHourEntry,'cbEntry',i);
     cbCurEntry.checked = true;
  }
}

function fIsLinesSelected(p_objSingleEntryLines) {

  for ( var i=0; i<p_objSingleEntryLines.miGetNumLines(); i++ )
  {
     var cbCurEntry = fGetInputWidget(top.framMainBody.document.formHourEntry,'cbEntry',i);
     if ( cbCurEntry.checked ) { return true;}
  }
  return false;

}

function fOnClickReverseNext(p_objSingleEntryLines, p_objLines)
{
  if (!fIsLinesSelected(p_objSingleEntryLines)) 
  {
    alert(top.g_objFNDMsg.mstrGetMsg('PA_WEB_SELECT_ONE_ITEM'));
    return;
  }

  fUpdateReverseSelectionToStruct(p_objSingleEntryLines);
  fCreateReverseTimecard(p_objSingleEntryLines, p_objLines);

  fPadTimecardLines(p_objLines);
  top.g_iTotalDisplayedLines = top.g_objTimecard.objLines.miGetNumLines();

  if (top.g_objTimecard.objLines.miGetNumNotEmptyLines() > 0) 
    top.g_bLinesEntered = true;
  else
    top.g_bLinesEntered = false;

g_bFromTimeEntryToReversal = false;

  fDrawTimeEntry2();
}

function fUpdateReverseSelectionToStruct(p_objSingleEntryLines)
{
  for ( var i=0; i<p_objSingleEntryLines.miGetNumLines(); i++ ) {

    var cbCurEntry = fGetInputWidget(top.framMainBody.document.formHourEntry,'cbEntry',i);
    var objCurLine = p_objSingleEntryLines.mGetarrSingleEntryLine()[i];

    if ( cbCurEntry.checked ) {
      if ( objCurLine.mGetbIsReversed() ) {  // Line Checked and previously reversed
	objCurLine.mSetobjState(C_strNEW_CHECK_NO);
      }
      else { // Line Checked and previously not reversed
	objCurLine.mSetobjState(C_strNEW_CHECK_YES);
      }
    }
    else   {
      if ( objCurLine.mGetbIsReversed()) {   //Line not checked but previously reversed
	objCurLine.mSetobjState(C_strNEW_UNCHECK_YES);
      }  
      else  {   // Line not checked and previously not reversed
	objCurLine.mSetobjState(C_strNEW_UNCHECK_NO);
      } 

    } // End cbCurEntry.checked
  }  // End for loop
} // End function


function fCreateReverseTimecard(p_objSingleEntryLines, p_objLines)
{

  for ( var i=0; i<p_objSingleEntryLines.miGetNumLines(); i++ )
  {
    var l_objCurLine = p_objSingleEntryLines.mGetarrSingleEntryLine()[i];
 
    if ( l_objCurLine.mGetbIsNewlyReversed() ){

      // If this singleentryline is newly reversed and line with same denorm id exists in
      // timecard lines, then update the corresponding day information for that line 
      // else add the new line to lines.
      
      if ( (objTimecardLine = p_objLines.mobjFindLineByOrigDenormID(l_objCurLine.mGetnOrigDenormID())) == null ) {
        // Line does not exist..insert line

        p_objLines.mobjInsertLine(l_objCurLine.mobjConvertToLine(),fFindIndexByDenormId(p_objLines,l_objCurLine.mGetnOrigDenormID()));
  	objTimecardLine = p_objLines.mobjFindLineByOrigDenormID(l_objCurLine.mGetnOrigDenormID());

      }
      else {  
	// Line exists ..update day information

        objTimecardLine.mGetobjDays().mobjGetDay(l_objCurLine.mGetnDayOfWeek()).mSetDay(fFormatDecimals(-(l_objCurLine.mGetobjDay().mGetnHours())), l_objCurLine.mGetobjDay().mGetstrComment())
	fCopyDFlex(l_objCurLine.mGetobjDay().arrDFlex, 
	           objTimecardLine.mGetobjDays().mobjGetDay(l_objCurLine.mGetnDayOfWeek()).arrDFlex);
      }
        objTimecardLine.mSetbIsDeleted(false);
        objTimecardLine.mSetbIsEmpty(false);
    }
    else if ( l_objCurLine.mGetbIsObsolete() ){

      // Since the line is marked obsolete, it must have been there previously, so get the line
      objTimecardLine = p_objLines.mobjFindLineByOrigDenormID(l_objCurLine.mGetnOrigDenormID());

      // Clear out day value for the corresponding day
      objTimecardLine.mGetobjDays().mobjGetDay(l_objCurLine.mGetnDayOfWeek()).mSetDay('','');

      // Delete Line if entire week is empty.
      if (objTimecardLine.mGetobjDays().mGetbIsEmptyWeek()) {
        objTimecardLine.mSetbIsDeleted(true);
        objTimecardLine.mSetbIsEmpty(true);
      }
    }

  }  // End for loop
  p_objLines.mResetDisplayNum();
}

// Called from the server side when modifying a reverse timecard.
// Need to set the reverse flag to true for the corresponding 
// reverse entries in the reverse timecard. 
function fSetReverseSelection(p_objSingleEntryLines, p_objLines)  {
  var l_objSingleEntryLine = new SingleEntryLine();

  for (var i=0; i<p_objLines.miGetNumLines(); i++) {
    if (p_objLines.arrLine[i].mbIsReverseLine()) { // If reverse line
      for (var j=0; j<C_iWEEKDAYS; j++) { 
	if (p_objLines.arrLine[i].mGetobjDays().mobjGetDay(j).mGetnHours() != "") {  // hours exist
          // Find this line in g_objSingleEntryLines and set the reverse flag to true.
	  p_objSingleEntryLines.mobjFindSingleEntryLine(p_objLines.arrLine[i].mGetnOrigDenormID(), j).mSetbIsReversed(true);
	}
      }
    }
  }
}


function fFindIndexByDenormId(p_objLines, p_iDenormId) {
  for (var i=0; i<p_objLines.miGetNumReversedLines() ; i++) {
    if (p_objLines.arrLine[i].mGetOrigDenormID() >  p_iDenormId) { break; }
  }
  return i;
}

// This function will sort single entry timecard in the order
// of project num, task number, type and date.
function fSortSingleEntryTimecard() 
{
  var objSortedSingleEntryLines = new SingleEntryLines();

  if (g_objSingleEntryTimecard.mGetobjSingleEntryLines().miGetNumLines()<= 1) {
    return;
  }

  for ( var i=0; i<g_objSingleEntryTimecard.mGetobjSingleEntryLines().miGetNumLines(); i++) {
    var objCurLine = g_objSingleEntryTimecard.mGetobjSingleEntryLines().mGetarrSingleEntryLine()[i];
    var bInserted = false;

    for ( var j=0; j<objSortedSingleEntryLines.miGetNumLines(); j++)   {
      var objComparingLine = objSortedSingleEntryLines.mGetarrSingleEntryLine()[j];
      if (objComparingLine.mGetstrProjectName() > objCurLine.mGetstrProjectName() )     {
        // OK, we found the place.
        objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine,j);
        bInserted = true;
        break;
      }
      else if ( objComparingLine.mGetstrProjectName() == objCurLine.mGetstrProjectName() ) {
        if ( objComparingLine.mGetstrTaskName() > objCurLine.mGetstrTaskName() )  {
          // OK, we found the place.
          objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine, j);
          bInserted = true;
          break;
        }
        else if ( objComparingLine.mGetstrTaskName() == objCurLine.mGetstrTaskName() ) {
          if ( objComparingLine.mGetobjType().mGetstrName() > objCurLine.mGetobjType().mGetstrName())  {
            // OK, we found the place.
            objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine, j);
            bInserted = true;
            break;
          }
          else if ( objComparingLine.mGetobjType().mGetstrName() == objCurLine.mGetobjType().mGetstrName() ) {
            var dWeekEndingDate, dComparingDate, dCurLineDate;
            dWeekEndingDate = g_objSingleEntryTimecard.mGetobjHeader().mGetdWeekEndingDate();
               
            dComparingDate = fDayOfWeekToDate(objComparingLine.mGetnDayOfWeek());
            dCurLineDate = fDayOfWeekToDate(objCurLine.mGetnDayOfWeek());
            if ( dComparingDate > dCurLineDate) {
              // OK we found the place
              objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine, j);
              bInserted = true;
              break;
            }
            else if ( dComparingDate = dCurLineDate )   {
              // OK, we found the place.
              objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine, j+1);
              bInserted = true;
              break;
            }
          }
        }
      }
    }
    if ( ! bInserted )
    {
      // Insert in the end
      objSortedSingleEntryLines.mobjInsertSingleEntryLineSorted(objCurLine, objSortedSingleEntryLines.miGetNumLines());
    }
 }
 // Now g_objSingleEntryTimecard becomes a sorted timecard.
 g_objSingleEntryTimecard.mSetobjSingleEntryLines(objSortedSingleEntryLines);
}
