<!-- hide the script's contents from feeble browsers
<!-- $Header: hrasdspw.js 115.3 2001/03/14 20:37:56 pkm ship   $ -->
<!-- ==========================================================================
<!-- Change History
<!-- Version  Date         Author      Bug #    Remarks
<!-- -------  -----------  ----------- -------- -------------------------------
<!-- 110.14   06-Mar-2001  lma         1669317  Fixed in refreshFlex function. 
<!-- 110.13   03-Jan-2001  lma         1414461  Validate numeric fields on
<!--                                            next_button_onClick.
<!-- 110.12   04-Jun-2000  fychu       1321326  In set_form_changed function,
<!--                                            set p_mgr_dummy_flag checkbox
<!--                                            value.
<!--
<!-- 110.11   19-Aug-1999  fychu       934577   In cancel_button_onClick,
<!--                                            removed the target statement.
<!--                                            The target of the link is set in
<!--                                            display_container_bottom_frame
<!--                                            of hr_assignment_display_web.
<!--
<!-- 110.10   15-Jul-1999  fychu       924954   1)Display an alert when  
<!--                                              top.submit_in_progress = Y
<!--                                              in next_button_onClick,
<!--                                              main_menu_gif_onClick,
<!--                                              cancel_button_onClick and
<!--                                              reset_button_onClick functions
<!--                                              for Netscape only.
<!--                                             2)In reset_button_onClick,
<!--                                              added a url parameter.
<!--
<!-- 110.9    14-Jul-1999  fychu                In next_button_onClick, added  
<!--                                            "else" clause to submit the 
<!--                                            form when the global variable
<!--                                            top.submit_in_progress = Y.
<!--
<!-- 110.8    14-Jul-1999  fychu       924954   1)In refreshPage, remmoved
<!--                                              top.clear_container_bottom_
<!--                                              frame
<!--                                              to avoid Netscape browser
<!--                                              crash.
<!--
<!-- 110.7    09-Jul-1999  fychu       919301   1)In refreshPage, set the
<!--                                              top.submit_in_progress global
<!--                                              variable before submit.
<!--                                            2)In next_button_onClick and
<!--                                              launchSubWindow func, check
<!--                                              the top.submit_in_progress
<!--                                              global variable.
<!-- 
<!-- 110.6    09-Jul-1999  fychu                In refreshPage and 
<!--                                            next_button_onClick js func,
<!--                                            changed references to 
<!--                                            checkMandatoryFields to     
<!--                                            top.checkMandatoryFields.
<!-- 
<!-- 110.5    06-Jul-1999  fychu       919301   In refreshPage function, moved
<!--                                            top.clear_container_bottom_frame
<!--                                            after form submit statement.
<!-- 
<!-- 110.4    01-Jul-1999  fychu                1)Changed the js variable 
<!--                                            subWindow in launchSubWindow
<!--                                            to MoreInfoSubWindow in order
<!--                                            not to confict with the same
<!--                                            name in the Eff Date window.
<!--                                            2)Removed check for 
<!--                                              p_form_changed in back_button_
<!--                                              onClick, cancel_button_onClick
<!--                                              and main_menu_gif_onClick     
<!--                                              functions.
<!-- 
<!-- 110.3    30-Jun-1999  fychu                Added a launchSubWindow func.
<!-- 
<!-- 110.2    29-Jun-1999  fychu                Added jTarget parm to 
<!--                                            back_button_onClick function.
<!-- 
<!-- 110.1    23-Jun-1999  fychu                In set_form_changed function,
<!--                                            set p_manager_flag value to 'Y'
<!--                                            if checked, 'N' if unchecked.
<!-- 
<!-- 110.0    21-Jun-1999  fychu                Initial Creation.
<!-- 
<!-----------------------------------------------------------------------------
<!-- --------------------------------------------------------------------------
<!-- 
<!-- Messages - Cross referencing for fnd_new_messages stored in javascript 
<!--            message array.  Always reference the array in 
<!--            container_middle_frame
<!--  [0] - HR_WORK_IN_PROGRESS_WEB
<!--
<!-- Global Variables
     var actionURL = "hr_assignment_display_web.validate_before_redisplay_page";

<!-- ----------------------------------------------------------------------
<!-- ------------------------ refresh_frames  -----------------------------
<!-- ----------------------------------------------------------------------
//Draw the top and bottom frame 
function refresh_frames(TopUrl, BottomUrl) {
    if (TopUrl != "") {
        if (window.parent.container_top.location.href != TopUrl)
           {window.parent.container_top.location.href = TopUrl}
        }
//
    if (BottomUrl != "") {
        if (window.parent.container_bottom.location.href != BottomUrl)
           {window.parent.container_bottom.location.href = BottomUrl}
        }
}

<!-- ----------------------------------------------------------------------
<!-- ------------------------- new_organization ---------------------------
<!-- ----------------------------------------------------------------------
//Refresh page after organization is selected from ICX LOV
function new_organization(newOrgID, newOrgName) {
    document.TransmitForm.P_ORGANIZATION_ID.value = newOrgID;
    document.TransmitForm.P_ORGANIZATION_NAME.value = newOrgName;
    document.TransmitForm.p_page_mode.value = "refreshORG";
    document.TransmitForm.p_form_changed.value="Y";
    document.TransmitForm.action = "hr_assignment_display_web.validate_before_redisplay_page";
    top.clear_container_bottom_frame();
    document.TransmitForm.submit();
 }

<!-- ----------------------------------------------------------------------
<!-- ---------------------------- new_job ---------------------------------
<!-- ----------------------------------------------------------------------
//Refresh page after job is selected from ICX LOV
function new_job(newJobID, newJobName) {
    document.TransmitForm.P_JOB_ID.value = newJobID;
    document.TransmitForm.P_JOB_NAME.value = newJobName;
    document.TransmitForm.p_page_mode.value = "refreshJOB";
    document.TransmitForm.p_form_changed.value="Y";
    document.TransmitForm.action = "hr_assignment_display_web.validate_before_redisplay_page";
    top.clear_container_bottom_frame();
    document.TransmitForm.submit();
 }

<!-- ----------------------------------------------------------------------
<!-- ---------------------------- new_position ----------------------------
<!-- ----------------------------------------------------------------------
//Refresh page after position is selected from ICX LOV
function new_position(newPosID, newPosName) {
    document.TransmitForm.P_POSITION_ID.value = newPosID;
    document.TransmitForm.P_POSITION_NAME.value = newPosName;
    document.TransmitForm.p_page_mode.value = "refreshPOS";
    document.TransmitForm.p_form_changed.value="Y";
    document.TransmitForm.action = "hr_assignment_display_web.validate_before_redisplay_page";
    top.clear_container_bottom_frame();
    document.TransmitForm.submit();
 }

<!-- ----------------------------------------------------------------------
<!-- ---------------------------- new_grade -------------------------------
<!-- ----------------------------------------------------------------------
//Refresh page after grade is selected from ICX LOV
function new_grade(newGradeID, newGradeName) {
    document.TransmitForm.P_GRADE_ID.value = newGradeID;
    document.TransmitForm.P_GRADE_NAME.value = newGradeName;
    document.TransmitForm.p_page_mode.value = "refreshGRADE";
    document.TransmitForm.p_form_changed.value="Y";
    document.TransmitForm.p_grade_lov.value="Y";
    document.TransmitForm.action = "hr_assignment_display_web.validate_before_redisplay_page";
    top.clear_container_bottom_frame();
    document.TransmitForm.submit();
 }

<!-- ----------------------------------------------------------------------
<!-- ------------------------- set_form_changed ---------------------------
<!-- ----------------------------------------------------------------------
//Set form has been changed               
function set_form_changed(elementName) 
   { 
    document.TransmitForm.p_form_changed.value = "Y"
    if (document.TransmitForm.p_mgr_dummy_flag.checked)
       {
        document.TransmitForm.p_mgr_dummy_flag.value = "Y"
        document.TransmitForm.p_manager_flag.value = "Y"
       }
    else
       {
        document.TransmitForm.p_mgr_dummy_flag.value = "N"
        document.TransmitForm.p_manager_flag.value = "N"
       }
   }

<!-- ----------------------------------------------------------------------
<!-- ------------------------- refreshFlex --------------------------------
<!-- ----------------------------------------------------------------------
//Set form has been changed and refresh page 
function refreshFlex(type,objType,refresh) 
{

  document.TransmitForm.p_form_changed.value="Y";
  if (refresh == "Y") {
  document.TransmitForm.p_page_mode.value = "refreshContextChanged" + type + objType;
  document.TransmitForm.action = 
              "hr_assignment_display_web.display_container_middle_frame";
  document.TransmitForm.submit();
  top.submit_in_progress = "Y";
  }
}  


<!-- ----------------------------------------------------------------------
<!-- ------------------------- refreshPage --------------------------------
<!-- ----------------------------------------------------------------------
//Set form has been changed and refresh page 
function refreshPage(type,objType, msg) 
  {
    if (type == "ORG") 
      {
       document.TransmitForm.P_ORGANIZATION_ID.value = "";
       }
//  
    if (type == "JOB") 
      {
       document.TransmitForm.P_JOB_ID.value = "";
       }
//
    if (type == "POS") 
      {
       document.TransmitForm.P_POSITION_ID.value = "";
       }          
//
    if (type == "GRADE") 
       {
          if (objType == "SELECTED")
            {
            document.TransmitForm.P_GRADE_NAME.value = "";
            }
          else  //from a text box
            {
            document.TransmitForm.P_GRADE_ID.value = "";
            }
       }
//
    if (type == "STEP") 
      {
       actionURL = "hr_assignment_display_web.validate_before_redisplay_page";
       }
//
//NOTE: Do not clear out the bottom frame either before or after submit the
//      page.  Otherwise, it will cause Netscape to crash whenever a user types
//      over the text field and presses NEXT.
//
//
  if (type == "ORG") 
     {
       if (top.checkMandatoryFields(document.TransmitForm, requiredFieldList,
                                fieldPromptList, msg)) 
          {
           top.errorExists = "";
           document.TransmitForm.p_form_changed.value="Y";
           document.TransmitForm.p_page_mode.value = "refresh" + type + objType;
           document.TransmitForm.action = actionURL;
           document.TransmitForm.submit();
           top.submit_in_progress = "Y";
          }
       else
          {
           top.errorExists = "Y";
          }
      }
//
//    
//NOTE: Do not put a nested else for checking TYPE=POS.  That will cause
//      Netscape to crash whenever a user types over the text field and 
//      presses NEXT.
//
     if (type == "POS")
       {
       if (top.checkMandatoryFields(document.TransmitForm, requiredFieldList,
                                fieldPromptList, msg)) 
          {
           top.errorExists = "";
           document.TransmitForm.p_form_changed.value="Y";
           document.TransmitForm.p_page_mode.value = "refresh" + type + objType;
           document.TransmitForm.action = actionURL;
           top.submit_in_progress = "Y";
           document.TransmitForm.submit();
          }
       else
          {
           top.errorExists = "Y";
          }
       }

//
   if (type != "ORG" && type != "POS")
      {
           document.TransmitForm.p_form_changed.value="Y";
           document.TransmitForm.p_page_mode.value = "refresh" + type + objType;
           document.TransmitForm.action = actionURL;
           top.submit_in_progress = "Y";
           document.TransmitForm.submit();
//         top.clear_container_bottom_frame();
      }
 }
//


<!-- ----------------------------------------------------------------------
<!-- ------------------------- launchSubWindow ----------------------------
<!-- ----------------------------------------------------------------------
//Launch More Info Sub Window
function launchSubWindow (p_url,p_width,p_height,p_scrolling,p_resize)
   {
    var scrollbar_attr;
    var resize_attr;
    if (p_scrolling == "Y")
      {
       scrollbar_attr = ",scrollbars=yes";
      }
    else
      {
       scrollbar_attr = ",scrollbars=no";
      }
//
    if (p_resize == "Y")
      {
       resize_attr = ",resizable=yes";
      }
    else
       {
        resize_attr = ",resizable=no";
       }
//
//Check that if submit is in progress, such as org/job/pos/grade text field
//has been entered and the onChange event refreshPage function has been fired.
  if (top.submit_in_progress != "Y")
    {
     attr = "menubar=no," + resize_attr + ",width=" + p_width + 
           ",height=" + p_height + scrollbar_attr;

     MoreInfoSubWindow = window.open(p_url, "more_info", attr);
     MoreInfoSubWindow.window.focus();
    }
  }

<!-- ----------------------------------------------------------------------
<!-- ------------------------- launchICXLov -------------------------------
<!-- ----------------------------------------------------------------------
//Launch ICX LOV window
function launchICXLov
         (gradeID
         ,attrbAppID
         ,attrbCode
         ,regionAppID
         ,regionCode
         ,formName
         ,frameName
         ,whereClause
         ,jsWhereClause
        )
  {
   if (gradeID == -1)  
    {
     LOV(attrbAppID
        ,attrbCode
        ,regionAppID
        ,regionCode
        ,formName
        ,frameName
        ,whereClause
        ,jsWhereClause)
     }
  }

<!-- ----------------------------------------------------------------------
<!-- --------------------- main_menu_gif_onClick --------------------------
<!-- ----------------------------------------------------------------------
//Check that if the form has changed.  If changed, display a confirmation
//message.  If confirmed to exit, exit to main menu.
function main_menu_gif_onClick(msg)
  {
   //Only check top.submit_in_progress for Netscape browser because it will
   //continue the onChange refreshPage function to repaint the page.  We
  //want to alert the user and inform him.
  //IE behaves correctly and the Main Menu button will go to the Menu
  //Page.  If you change the text field in org/job/pos/grade and then
  //press Main Menu, the onChange event is superceded by the 
  //Main Menu action in IE.
  if (top.Nav4)
       {
        if (top.submit_in_progress != "Y")
           {
            if (confirm(msg))
              {
               top.clear_container_bottom_frame();
               document.TransmitForm.p_result_code.value = 'MAIN_MENU';
               document.TransmitForm.submit();
              }
            }
        else
           {
            //Submit In Progress, cannot perform Main Menu action
             alert(jsMsg[0]);
             document.TransmitForm.submit();
            }
       }
  else
      // IE browsers
      {
       top.clear_container_bottom_frame();
       document.TransmitForm.p_result_code.value = 'MAIN_MENU';
       document.TransmitForm.submit();
       }
   }
      
<!-- ----------------------------------------------------------------------
<!-- --------------------- cancel_button_onClick --------------------------
<!-- ----------------------------------------------------------------------
//Check that if the form has changed.  If changed, display a confirmation
//message.  If confirmed to exit, exit to Person Search.
function cancel_button_onClick (msg)
  {
   //Only check top.submit_in_progress for Netscape browser because it will
   //continue the onChange refreshPage function to repaint the page.  We
  //want to alert the user and inform him.
  //IE behaves correctly and the Cancel button will go back to the Person
  //Search Page.  If you change the text field in org/job/pos/grade and then
  //press Cancel, the onChange event is superceded by the Cancel action in IE.
  if (top.Nav4)
       {
        if (top.submit_in_progress != "Y")
           {
            document.TransmitForm.p_result_code.value = 'CANCEL';
            if (confirm(msg))
              {
               top.clear_container_bottom_frame();
               document.TransmitForm.submit();
              }
            }
        else
           {
            //Submit In Progress, cannot perform Cancel action
             alert(jsMsg[0]);
             document.TransmitForm.submit();
            }
       }
  else
      // IE browsers
      {
       document.TransmitForm.p_result_code.value = 'CANCEL';
       if (confirm(msg))
          {
           top.clear_container_bottom_frame();
           document.TransmitForm.submit();
           }
       }
 }
      
<!-- ----------------------------------------------------------------------
<!-- ----------------------- back_button_onClick --------------------------
<!-- ----------------------------------------------------------------------
//Check that if the form has changed.  If changed, display a confirmation
//message.  If confirmed to exit, go back to Previous page.
function back_button_onClick (msg,jTarget)
  {
   if (jTarget == "")
      {
       document.TransmitForm.target = "_self";
      }
   else
      {
       document.TransmitForm.target = jTarget;
      }
//
   //Only check top.submit_in_progress for Netscape browser because it will
   //continue the onChange refreshPage function to repaint the page.  We
  //want to alert the user and inform him.
  //IE behaves correctly and the Back button will go back to the previous
  //process.  If you change the text field in org/job/pos/grade and then
  //press Back, the onChange event is superceded by the Back action in IE.
  if (top.Nav4)
       {
        if (top.submit_in_progress != "Y")
           {
            if (confirm(msg))
              {
               top.clear_container_bottom_frame();
               document.TransmitForm.p_result_code.value = "PREVIOUS";
               document.TransmitForm.submit();
              }
            }
        else
           {
            //Submit In Progress, cannot perform Back action
             alert(jsMsg[0]);
             document.TransmitForm.target = "_self";
             document.TransmitForm.submit();
            }
       }
  else
      // IE browsers
      {
       if (confirm(msg))
          {
           top.clear_container_bottom_frame();
           document.TransmitForm.p_result_code.value = "PREVIOUS";
           document.TransmitForm.submit();
          }
      }
   }
      
<!-- ----------------------------------------------------------------------
<!-- -------------------------- next_button_onClick ----------------------
<!-- ----------------------------------------------------------------------
//The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'NEXT' button.

function next_button_onClick (msg,msg2,decimalPoint) {
  if (top.errorExists == "" || top.errorExists == null)
    {
    
//Only check top.submit_in_progress for Netscape browser because it will 
//cause a browser crash.
     if (top.Nav4)
       {
        if (top.submit_in_progress != "Y")
           {
            if(top.checkMandatoryFields(document.TransmitForm,requiredFieldList,
                          fieldPromptList, msg)) 
              {
              if(top.checkNumericFields(document.TransmitForm,numericFieldList,
                          numericPromptList, msg2,decimalPoint))
              {
               document.TransmitForm.p_result_code.value = 'NEXT';
               top.clear_container_bottom_frame();
               document.TransmitForm.submit();
               }
              }
            }
        else
           {
            //Submit In Progress, cannot perform Next action
             alert(jsMsg[0]);
             document.TransmitForm.submit();
            }
        }
    else
       //MS IE browsers
           {
            if (top.submit_in_progress != "Y")
               {
                if(top.checkMandatoryFields(document.TransmitForm,requiredFieldList,
                          fieldPromptList, msg)) 
                 {
                if(top.checkNumericFields(document.TransmitForm,numericFieldList,
                          numericPromptList, msg2,decimalPoint))
                 {
                  document.TransmitForm.p_result_code.value = 'NEXT';
                  top.clear_container_bottom_frame();
                  document.TransmitForm.submit();
                 }
                }
               }
            else
               {
                 //IE ignores the following code and continues to call
                 //process_save to save the page because the refreshPage for
                 //the onChange event does not get fired.
                 //We leave the code here for future debugging.
                 alert(jsMsg[0]);
                 document.TransmitForm.action = actionURL;
                 document.TransmitForm.submit();
                }
            }
   }
}

<!-- ----------------------------------------------------------------------
<!-- -------------------------- reset_button_onClick ----------------------
<!-- ----------------------------------------------------------------------
// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Reset Page' button.
function reset_button_onClick (MiddleUrl) {
  if (top.errorExists == "")
     {
      top.clear_container_bottom_frame();
      window.parent.container_middle.location.href = MiddleUrl;
     }
}

//

