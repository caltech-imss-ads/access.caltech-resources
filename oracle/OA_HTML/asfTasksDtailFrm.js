/*********************************************************************
 * Javascript functions for To Do Create/Update
 *********************************************************************/
 <!-- $Header: asfTasksDtailFrm.js 115.4 2000/12/29 12:07:10 pkm ship   $ -->

/**
 * handle click on checkBox for Private Flag
 */

function setPrivateFlag(secObjName)
{
  var theForm = document.forms['TasksDetail'];

  var privateFlag = secObjName + 'privateFlag';

  var checkBox = 'privateFlag';

  if(theForm.elements[checkBox].checked)
  {
    theForm.elements[privateFlag].value = 'Y';
  }
  else
  {
    theForm.elements[privateFlag].value = 'N';
  }
}

