<!-- hide the script's contents from feeble browsers
<!-- $Header: hrcscrtw.js 115.0 2000/01/20 12:49:19 pkm ship       $ -->
<!-----------------------------------------------------------------------------
<!-- Change History
<!-- Version Date         Name          Bug #    Reason
<!-- ------- -----------  ------------- -------- ------------------------------
<!-- 110.1   23-Sep-1999  jxtan         934577   changed cancel button target
<!--                                             to container_middle.
<!-- 110.0   26-Jun-1999  jxtan                  Created.
<!-- 110.0   02-Jul-1999  jxtan                  set back button target.
<!-----------------------------------------------------------------------------
<!-- cost center module specific js file. 
<!--  
<!-----------------------------------------------------------------------------

// The following function is invoked from the
// header frame when the user clicks on the 
// 'Main Menu' gif in the toolbar.
function main_menu_gif_onClick (msg) {
  document.csk_form.p_result_code.value = 'MAIN_MENU';  
//  if (document.csk_form.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
       document.csk_form.target = '_top'; 
       document.csk_form.submit();
     }
/*
  }
  else {
       document.csk_form.target = '_top';
       document.csk_form.submit();
  }
*/
}

//Set form has been changed
function set_form_changed () {
  document.csk_form.p_form_changed.value="Y";
}

// The following function is invoked from the 
// container_bottom frame when the user clicks
// on the 'Reset Page' button.
function reset_button_onClick () { 
//  alert ('reset_button_onClick');
  document.csk_form.reset();
}

//when user click cancel button
function back_button_onClick (msg, newtarget) {
//  if (document.csk_form.p_form_changed.value=="Y") {
     if (confirm(msg)) {
         top.clear_container_bottom_frame();
        document.csk_form.p_result_code.value = 'PREVIOUS';
        document.csk_form.target = newtarget;
        document.csk_form.submit();
     }
/*
  } else {
        top.clear_container_bottom_frame();
        document.csk_form.p_result_code.value = 'PREVIOUS';
        document.csk_form.target = newtarget;
        document.csk_form.submit();
  }
*/
}


// container_bottom frame when the user clicks
// on the 'Cancel' button.
function cancel_button_onClick (msg) {
//  if (document.csk_form.p_form_changed.value=="Y") { 
     if (confirm(msg)) {
        top.clear_container_bottom_frame();
        document.csk_form.p_result_code.value = 'CANCEL';
        document.csk_form.target = 'container_middle';
        document.csk_form.submit();
     }
/*
  } else {
        top.clear_container_bottom_frame();
        document.csk_form.p_result_code.value = 'CANCEL';
        document.csk_form.target = '_top';
        document.csk_form.submit();
  }
*/
}


// The following function is invoked from the
// container_bottom frame when the user clicks
// on the 'Next' button.
function next_button_onClick (msg1, msg2) 
{
//  document.csk_form.p_redraw.value ="N";
  if(checkMandatoryFields(document.csk_form, requiredFieldList,
     fieldPromptList, msg1)) 
  {
    top.clear_container_bottom_frame();
 //   document.csk_form.p_result_code.value = 'NEXT';
    document.csk_form.submit();
  }
}

// This function is called to go back to the main menu
// pointed to by loc.
function goto_main_menu (msg,loc) {
  alert (msg);
  parent.container_middle.location.href = loc;
} 

function set_redraw_flag () {
  document.csk_form.p_redraw.value ="Y";
  document.csk_form.submit();
}
