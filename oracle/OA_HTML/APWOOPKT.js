//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOOPKT.js                       |
//  |                                                |
//  | Description: Client-side Javascript functions  |
//  |              for multi-row page		     |     
//  |                                                |
//  | History:     Created 03/08/2000	
//  |              Modified 03/28/2000               |
//  |              Modified the fsetparseStringfunction
//  |              Modified fRequiredfieldsEntered.
//  |              Added a new function fDrawerrorwindow.
//  |		   Moved fSetParseString to save frame
//  |              		                     |
//  |                                                |
//  +================================================+
/* $Header: APWOOPKT.js 115.68 2001/03/01 17:20:47 pkm ship  $ */

    function fSeedEq(iLine) {
    var iRecIndex = fGetObjExpReport().getNthReceipt(iLine);
    var r = null;
    if (iRecIndex>=0){
      r = fGetReceiptObj(iRecIndex);
    var recCurr=r.receiptCurr;
    var eq="";
    var action = fDetermineConversion(recCurr,fGetObjHeader().getReimbursCurr(),r);
      if (action == top.g_reimbEqualsRec) {
	eq = fGetObjHeader().getReimbursCurr()+" = 1 " + recCurr;
      } else if (action == top.g_reimbFixedRecFixed) {
	eq = recCurr + " -> " + top.g_strEuroCode + " -> "+fGetObjHeader().getReimbursCurr();
      } else if ((action == top.g_reimbEurRecFixed) || (action == top.g_reimbFixedRecEur)) {
	eq = recCurr + " -> "+fGetObjHeader().getReimbursCurr();
      } else {
        if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='N')
        eq = fGetObjHeader().getReimbursCurr()+" = 1 " + recCurr;
        else
	eq = recCurr + " = 1 " +fGetObjHeader().getReimbursCurr();
      }
     } // iRecIndex>=0
     }
   
function fPopulateLineTotal(nLine){
  var nActualLine = fGetObjExpReport().getNthReceipt(nLine); 
  var totalField = eval('top.framMain.lines.document.data.txtTotal'+nLine);
  var reimbCurr = fGetObjHeader().getReimbursCurr();
  var tempObj = fGetReceiptObj(nActualLine);
  var reimb_amount = fMoneyFormat(tempObj.getReimbursAmount(),reimbCurr);
    
  totalField.value = reimb_amount;
  
    if (fIsDetailOpen())
       fGetObjDetail().fDetailSetReimbursAmount(reimb_amount);
  
}




function fGenMultiRowFuncs(){
        var targetFrame = top.framMain.lines;
	var d = top.framMain.lines.document;

  
	d.writeln("function fUpdateButtonsFrame(){");
	d.writeln("var nTotalReceipts = top.objExpenseReport.getTotalReceipts('OOP');");
	d.writeln("	var nTotalAmount = top.fMoneyFormat(top.objExpenseReport.calculateTotal('OOP'), top.objExpenseReport.header.getReimbursCurr());");
 	d.writeln(" 	top.framMain.framButtons.setTotalInfo(nTotalReceipts, nTotalAmount);");
	d.writeln("}");


/*--------------------------
-- Functions for project and task
-------------------------- */
	d.writeln("	function populateProjectNumberField(projectNumber) {");
	d.writeln("	var txtProjectID = document.data.AP_WEB_PA_PROJECT_ID;");
	d.writeln("	if (txtProjectID.value != \"\") if (top.fIsNum(txtProjectID.value,false))");
	d.writeln("	 fChangeProjectNumber(projectNumber,txtProjectID.value, top.framMain.lines.g_iCurrentLine);");
	d.writeln("	 else fChangeProjectNumber(projectNumber,\"\",g_iCurrentLine);");
	d.writeln("	 document.data.AP_WEB_PA_PROJECT_ID.value = \"\";");

	d.writeln("	}");
                   
	d.writeln("	function populateTaskNumberField(taskNumber) { ");
	d.writeln("	var txtTaskID =document.data.AP_WEB_PA_TASK_ID;");
	d.writeln("	if (txtTaskID.value != \"\") if (top.fIsNum(txtTaskID.value,false))");
	d.writeln("	 fChangeTaskNumber(taskNumber,txtTaskID.value,top.framMain.lines.g_iCurrentLine);");
	d.writeln("	 else fChangeTaskNumber(taskNumber,\"\", top.framMain.lines.g_iCurrentLine);");
        d.writeln("	 document.data.AP_WEB_PA_TASK_ID.value = \"\";");           
	d.writeln("	}");

	d.writeln("	function LOV(a,b,c,d,e,f,g,h) { ");
	d.writeln("	document.data.AP_WEB_PA_PROJECT_NUMBER.value = \"\";");
	d.writeln("	document.data.AP_WEB_PA_TASK_NUMBER.value = \"\";");
	d.writeln("     iRecIndex =  top.fGetObjExpReport().getNthReceipt(top.framMain.lines.g_iCurrentLine);");
    	d.writeln("	var r = top.fGetReceiptObj(iRecIndex);");
	d.writeln("	if (r){ if (!r.isProjectEnabled()) return;}");
        d.writeln("	if (r){ if (r.getProjectNumber() != \"\") top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER.value = r.getProjectNumber();");
	d.writeln("		if (r.getTaskNumber() != \"\") top.framMain.lines.document.data.AP_WEB_PA_TASK_NUMBER.value = r.getTaskNumber();");
	d.writeln("	}");  
	d.writeln("	else{ document.data.AP_WEB_PA_PROJECT_NUMBER.value = \"\";");
	d.writeln("		document.data.AP_WEB_PA_TASK_NUMBER.value = \"\";}");
        d.writeln("       if((b==\"AP_WEB_PA_TASK_NUMBER\")&&(eval('top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER'+g_iCurrentLine).value == \"\")){");
	d.writeln("     alert(top.g_objMessages.mstrGetMesg('AP_WEB_TASK_LOV_REQ_PROJ')); return;");
        d.writeln("              }");
        d.writeln("           top.LOV(a,b,c,d,e,f,g,h);");
        d.writeln("         }");

/* ----------------------------------
-- Change Expense Type
---------------------------------- */

	d.writeln("function fChangeExpType(value,nLine){");
	d.writeln("  if (top.framMain.lines.document.tax_reqimg) ");
	d.writeln("	top.framMain.lines.document.tax_reqimg.src = top.reqimg1.src;");
	d.writeln("  top.fUpdateLineStatus(nLine,true);");
	d.writeln("  var iRecIndex =  top.fGetObjExpReport().getNthReceipt(nLine);");
	d.writeln("  var r = top.fGetReceiptObj(iRecIndex);");
	d.writeln("  var popExpType = eval('document.data.popExpType'+nLine);");
	d.writeln("  var objE = top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(value);");
	d.writeln("	r.setAmountIncludesTax(objE.bAITFlag == 'Y');");
	d.writeln("	if (objE.strDefaultTaxCode != \"\")");
	d.writeln("	    r.setTaxCode(objE.strDefaultTaxCode);");

	d.writeln("	  var txtProject = eval(\"top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER\"+nLine);");
      	d.writeln("	  var txtTask = eval(\"top.framMain.lines.document.data.AP_WEB_PA_TASK_NUMBER\"+nLine);");
	d.writeln("if (top.fGetObjExpReport().header.getObjEmployee().mGetbIsProjectEnabled())");
	d.writeln("	if (objE.strExpenditureType != \"\"){ // project-related expense type");
	d.writeln("	  if (txtProject) txtProject.value = r.getProjectNumber();");
	d.writeln("	  if (txtTask) txtTask.value = r.getTaskNumber();");
	d.writeln("	}");
	d.writeln("	else{ // not project-related expense type");
	d.writeln("	  if ((r.getProjectNumber() == '') && (r.getTaskNumber() == '')){");
	d.writeln("	    if (txtProject) txtProject.value = top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("	    if (txtTask) txtTask.value = top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("	  }");

	d.writeln("	  else{ // project or task is not null");
	d.writeln("	    if (confirm(\""+top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_WRN')+"\")){");
	d.writeln("	         txtProject.value = top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("		 txtTask.value = top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("		 r.setProjectNumber('');");
	d.writeln("		 r.setTaskNumber('');");
	d.writeln("	    }");
	d.writeln("	    else{");
	d.writeln("		if (r.objExpenseType)");
	d.writeln("		  top.fRecoverExpenseType(r.objExpenseType.ID,eval('top.framMain.lines.document.data.popExpType'+nLine));");
	d.writeln("		else top.fRecoverExpenseType('',eval('top.framMain.lines.document.data.popExpType'+nLine));");
	d.writeln("		return;");
	d.writeln("	    }");
	d.writeln("	  }");
	d.writeln("	}");

	d.writeln("  r.setAmountIncludesTax(objE.bAITFlag == 'Y');");
	d.writeln("  fSetAmtIncTax(r.getAmountIncludesTax(),nLine);");
	d.writeln("  if (top.fIsDetailOpen()) top.fGetObjDetail().fDetailSetAmtIncTax(r.getAmountIncludesTax());");
	d.writeln("	if (objE.strDefaultTaxCode != \"\"){");
	d.writeln("  	  var e = eval(\"document.data.popTaxCode\" + nLine);");
	d.writeln("	  if (e){");
	d.writeln("		e.selectedIndex = 0;");
	d.writeln("		var jj = 0;");
	d.writeln("		for (var j=0;j<e.options.length;j++){");
	d.writeln("		  if (e.options[j].text == objE.strDefaultTaxCode) {");
	d.writeln("		     fSetTaxCode(j,nLine);");
	d.writeln("	 	     jj = j;");
	d.writeln("		     j = e.options.length;");
	d.writeln("		  } //end if");
	d.writeln("		}//end for");
	d.writeln("		r.setTaxCode(objE.strDefaultTaxCode);");
	d.writeln("		if (top.fIsDetailOpen()) top.fGetObjDetail().fDetailSetTaxCode(jj);");
	d.writeln("	  }");
	d.writeln("	}");
	d.writeln("	else{");
	d.writeln("  	  var e = eval(\"document.data.popTaxCode\" + nLine);");
	d.writeln("	  if (e) fSetTaxCode(0,nLine);");
	d.writeln("	  r.setTaxCode(\"\");");
	d.writeln("	  if (top.fIsDetailOpen()) { "); //top.fGetObjDetail().fDetailSetTaxCode(eval(\"0\"));");
	d.writeln("	     var popTaxCode = eval('top.g_winDetail.framReceiptDetail.document.formDetail.popTaxCode');");
	d.writeln("	     if (popTaxCode) popTaxCode.selectedIndex = 0;");
	d.writeln("	  }");
	d.writeln("	}");
    	d.writeln("	r.setExpenseType(objE);");
	d.writeln("     if (!r.flexFields) {");
	d.writeln("        r.flexFields = new Array();");
	d.writeln("	r.flexFields.length = 0;");
	d.writeln("	top.fAddFlexFields(r,r.getObjExpenseType().mGetstrCode()); }");
	d.writeln("	var bIsReq = 'N';");
	d.writeln("	if (r.getObjExpenseType()) bIsReq = r.getObjExpenseType().mGetbJustifRequired();");
	d.writeln("		if (bIsReq == 'Y')");
	d.writeln("		    top.fSetReqImg();");
	d.writeln("		else document.reqimg.src = top.g_strImgDir+\"APWPX4.gif\";");
	d.writeln("	if (top.framMain.lines.document.tax_reqimg){");
	d.writeln("        if (r.getAmountIncludesTax()) top.framMain.lines.document.tax_reqimg.src = top.reqimg1.src;");
	d.writeln("	   else top.framMain.lines.document.tax_reqimg.src = top.g_strImgDir+\"APWPX4.gif\";");
	d.writeln("	}");
 	d.writeln("     if (top.fIsDetailOpen()){");
	d.writeln("		top.fGetObjDetail().iRecIndex = iRecIndex;");
	d.writeln("		top.fGetObjDetail().iLineNum = nLine;");
	d.writeln("		top.fGetObjDetail().fDetailSetExpenseType(popExpType.selectedIndex);");
	d.writeln("	 	top.fGetObjDetail().fDetailSetProjectNumber(r.getNAProjectNumber());");
	d.writeln("	 	top.fGetObjDetail().fDetailSetTaskNumber(r.getNATaskNumber());");
     	d.writeln("	}");
	d.writeln("}");

  	d.writeln("function fSetExpenseType(selectedIndex,nLine){");
	d.writeln("	var popExpType = eval('top.framMain.lines.document.data.popExpType'+nLine);");
	d.writeln("	if (popExpType) popExpType.selectedIndex = selectedIndex;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");


/* ---------------------------
-- Change Receipt Amount
--------------------------- */

	d.writeln("function fChangeReceiptAmount(value,nLine) {");
	d.writeln("if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_CREDIT_LINES')=='N'){");
	d.writeln("if (!top.fCheckPosNum(value,false)){");
	d.writeln("var nTempAmount = top.fMoneyFormat(\"0\",top.fGetObjHeader().getReimbursCurr());");
	d.writeln("  fSetReceiptAmount(nTempAmount,nLine);");
	d.writeln("  return;");
	d.writeln("  }");
	d.writeln("}");
  	d.writeln("	var recAmountField = eval('top.framMain.lines.document.data.txtAmount'+nLine);");
 	d.writeln("	var curr;");
	d.writeln("	var iRecIndex;");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("     iRecIndex =  top.fGetObjExpReport().getNthReceipt(nLine);");    
	d.writeln("	var r = top.fGetReceiptObj(iRecIndex);");
  	d.writeln("	if (value != \"\") {");
	d.writeln("	     curr = r.getReceiptCurr();");
    	d.writeln("	     var tempAmount = 0;");
     	d.writeln("	     if (top.fCheckNum(value, false, true)){ // show alert at first checking time");
     	d.writeln("		r.setExpensedAmount(value);");
     	d.writeln("		if (r.itemizeId == null) r.setReceiptAmount(value);");
	d.writeln("  		r.setReimbursAmount(top.fCalcReimbursAmount(iRecIndex));");
	d.writeln("		fSetReimbursAmount(r.getReimbursAmount(),nLine);");
     	d.writeln("		recAmountField.value = top.fMoneyFormat(eval(recAmountField.value),curr);");
	d.writeln("     	if (top.fIsDetailOpen()) {");
	d.writeln("	 	   top.fGetObjDetail().fDetailSetReceiptAmount(recAmountField.value);");
	d.writeln("		   var framReceiptDoc = eval('top.fGetObjDetail().framReceiptDetail.document')");
	d.writeln("	 	   top.fGetObjDetail().fDetailSetRate(top.fMoneyFormat(eval(recAmountField.value)/ eval(framReceiptDoc.formDetail.txtOccurs.value), curr));");
	d.writeln("		} //end if DetailOpen() ");
	d.writeln("		top.fPopulateLineTotal(nLine);");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("	     } ");
    	d.writeln("	     else{");
	d.writeln("		recAmountField.value = \"\";");
        d.writeln("		recAmountField.focus();");
        d.writeln("		recAmountField.select();");
   	d.writeln("	     }");
  	d.writeln("	}");
  	d.writeln("    	else{//value = empty");
	d.writeln("		r.setExpensedAmount(0);");
     	d.writeln("		if (r.itemizeId == null) r.setReceiptAmount(r.getReimbursAmount());");
	d.writeln("		top.fPopulateLineTotal(nLine);");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("	}");
	d.writeln("} //end function fChangeReceiptAmount");


	d.writeln("function fSetReceiptAmount(value,nLine){");
	d.writeln("	var recAmountField = eval('top.framMain.lines.document.data.txtAmount'+nLine);");
	d.writeln("	if (recAmountField) recAmountField.value=value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");


	d.writeln("function fSetReimbursAmount(value, nLine) {");
	d.writeln("  var reimbursAmountField = eval('top.framMain.lines.document.data.txtTotal'+nLine);");
        d.writeln("  reimbursAmountField.value = top.fMoneyFormat(value,top.objExpenseReport.header.getReimbursCurr());"); 
	d.writeln("	if (top.fIsDetailOpen())");
	d.writeln("	  top.fGetObjDetail().fDetailSetReimbursAmount(reimbursAmountField.value);");
	d.writeln("}");

/* -------------------------------
-- Change Receipt Currency
------------------------------- */

	d.writeln("function fChangeCurrency(value,nLine){");
 	d.writeln("	var popCurrency = eval('top.framMain.lines.document.data.popCurrency'+nLine);");
	d.writeln("	var recAmountField = eval('top.framMain.lines.document.data.txtAmount'+nLine);");

      	d.writeln("	if (value != \"\"){");
	d.writeln("		var iRecIndex;");
	d.writeln("  		top.fUpdateLineStatus(nLine,true);");
	d.writeln("     	iRecIndex =  top.fGetObjExpReport().getNthReceipt(nLine);");
	d.writeln("		var r = top.fGetReceiptObj(iRecIndex);");

     	d.writeln("		r.setCurrency(popCurrency.options[popCurrency.selectedIndex].value);");
	d.writeln("		if (recAmountField.value != \"\")");
	d.writeln("		recAmountField.value = top.fMoneyFormat(eval(recAmountField.value),value);");
     	d.writeln("		top.fPopulateLineTotal(nLine);");
	d.writeln("		r.setExchRate(1);");
	d.writeln("		var tempRate = top.fGetAndSetExchRate(r);");
	d.writeln("		r.setReimbursAmount(top.fCalcReimbursAmount(iRecIndex));");
	d.writeln("		fSetReimbursAmount(r.getReimbursAmount(),nLine);");
	d.writeln("		fUpdateButtonsFrame();");
	
	d.writeln("		if (top.fIsDetailOpen()){");
	d.writeln("	 	  top.fGetObjDetail().fDetailSetReceiptCurr(popCurrency.selectedIndex);");
	d.writeln("		  top.fGetObjDetail().fDetailSetExchRate(tempRate);");
	d.writeln("	        }");
	d.writeln("		popCurrency.focus();");
        d.writeln("	}// value is not empty");
	d.writeln("}");

	d.writeln("function fSetCurrency(selectedIndex,nLine){");
	d.writeln("	var popCurrency = eval('top.framMain.lines.document.data.popCurrency'+nLine);");
	d.writeln("	if (popCurrency) popCurrency.selectedIndex = selectedIndex;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");

/* ---------------------------
-- Change Receipt Date
--------------------------- */


	d.writeln("function fChangeDate(value,nLine){");
 	d.writeln("	var dateField = eval('top.framMain.lines.document.data.txtDate'+nLine);");
	d.writeln("	var objDate = top.fStringToDate(dateField.value, true);");
	d.writeln("	var iRecIndex;");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("     iRecIndex =  top.fGetObjExpReport().getNthReceipt(nLine);");
	d.writeln("	var r = top.fGetReceiptObj(iRecIndex);");
  	d.writeln("	if (value != \"\"){ ");
	d.writeln("	  	if (objDate != null){");
       	d.writeln("		  top.g_strDay = objDate.getDate();");
       	d.writeln("		  top.g_strMonth = objDate.getMonth();");
       	d.writeln("		  top.g_strYear = objDate.getFullYear();");
     	d.writeln("		}");
	d.writeln("		else{");
	d.writeln("		  dateField.value = \"\";");
	d.writeln("		  dateField.focus();");
	d.writeln("		  r.setDate(\"\");");
        d.writeln("		  dateField.select(); return;");
	d.writeln("		}");
	d.writeln("	}");
    	d.writeln("	r.setDate(value);");
	d.writeln("	var tempRate = top.fGetAndSetExchRate(r);");
	d.writeln("	r.setReimbursAmount(top.fCalcReimbursAmount(iRecIndex));");
	d.writeln("	fSetReimbursAmount(r.getReimbursAmount(),nLine);");
	d.writeln("	fUpdateButtonsFrame();");
     	d.writeln("	if (top.fIsDetailOpen()){");
	d.writeln("	  top.fGetObjDetail().fDetailSetDate(value);");
	d.writeln("	  top.fGetObjDetail().fDetailSetExchRate(tempRate);");
	d.writeln("	}");
        d.writeln("}");

	d.writeln("function fSetDate(value,nLine){");
	d.writeln("	var dateField = eval('top.framMain.lines.document.data.txtDate'+nLine);");
	d.writeln("	if (dateField) dateField.value = value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");



/* --------------------------
-- Change Justification
-------------------------- */

  	d.writeln("function fChangeJustification(value,nLine){");
	d.writeln("	var multiJustificationField = eval('top.framMain.lines.document.data.txtJustification'+nLine);");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
  	d.writeln(" 	r.setJustification(multiJustificationField.value);");

	d.writeln("	if (top.fIsDetailOpen())");
	d.writeln("	   top.fGetObjDetail().fDetailSetJustification(value);");
	d.writeln("}");

	d.writeln("function fSetJustification(value,nLine){");
	d.writeln("	var tempField = eval('top.framMain.lines.document.data.txtJustification'+nLine);");
	d.writeln("	if (tempField) tempField.value = value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");



/* --------------------------
-- Change Project Number
-------------------------- */

  	d.writeln("function fChangeProjectNumber(value,projectID,nLine){");
	d.writeln("	var txtProject = eval('top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER'+nLine);");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
  	d.writeln(" 	if (r) r.setProjectNumber(value);");
	d.writeln("	fSetProjectNumber(value,nLine);");
  	
	d.writeln("	if (top.fIsDetailOpen())");
	d.writeln("	   top.fGetObjDetail().fDetailSetProjectNumber(value);");
	d.writeln("}");

	d.writeln("function fSetProjectNumber(value,nLine){");
	d.writeln("	var tempField = eval('top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER'+nLine);");
	d.writeln("	if (tempField) tempField.value = value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");


/* --------------------------
-- Change Task Number
-------------------------- */

  	d.writeln("function fChangeTaskNumber(value,taskID,nLine){");
	d.writeln("	var txtTask = eval('top.framMain.lines.document.data.AP_WEB_PA_TASK_NUMBER'+nLine);");
	d.writeln("	var txtProject = eval('top.framMain.lines.document.data.AP_WEB_PA_PROJECT_NUMBER'+nLine);");
	d.writeln("	if ((txtProject) && (txtTask)) {if (txtTask!=\"\") if (txtProject.value == \"\"){");
	d.writeln("     txtTask.value = \"\";");
	d.writeln("	txtProject.focus();");
	d.writeln("     alert(top.g_objMessages.mstrGetMesg('AP_WEB_TASK_LOV_REQ_PROJ')); return;}}");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
  	d.writeln(" 	if (r) r.setTaskNumber(value);");
	d.writeln("	if (txtTask) txtTask.value = value;");
  	
	d.writeln("	if (top.fIsDetailOpen())");
	d.writeln("	   top.fGetObjDetail().fDetailSetTaskNumber(value);");
	d.writeln("}");

	d.writeln("function fSetTaskNumber(value,nLine){");
	d.writeln("	var tempField = eval('top.framMain.lines.document.data.AP_WEB_PA_TASK_NUMBER'+nLine);");
	d.writeln("	if (tempField) tempField.value = value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");



/* -----------------------------
-- Change Receipt Missing
----------------------------- */

	d.writeln("function fClickReceiptMissing(value,nLine){");
	d.writeln("	var cbxReceiptMissing = eval('top.framMain.lines.document.data.cbxReceiptMissing'+nLine);");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
	d.writeln("	r.setIsMissing(cbxReceiptMissing.checked);");
	d.writeln("	if (top.fGetObjDetail() != null) if (!top.fGetObjDetail().closed)");
	d.writeln("	   top.fGetObjDetail().fDetailSetReceiptMissing(value);");
	d.writeln("}");
 	

	d.writeln("	function fSetReceiptMissing(value,nLine){");
	d.writeln("	var tempField = eval('top.framMain.lines.document.data.cbxReceiptMissing'+nLine);");
	d.writeln("	tempField.checked = value; ");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("	}");


/* -----------------------------
-- Change Expense Group
----------------------------- */

  	d.writeln("function fChangeExpGroup(value,nLine){");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
	d.writeln("	r.setGroup(value);");
	d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetGroup(value);");
	d.writeln("}");

	d.writeln("function fSetExpGroup(value,nLine){");
	d.writeln("	var tempField = eval('top.framMain.lines.document.data.txtGroup'+nLine);");
	d.writeln("	if (tempField) tempField.value = value;");
	d.writeln("		fUpdateButtonsFrame();");
	d.writeln("}");


/* -----------------------------
-- Change Country Of Supply
----------------------------- */
	d.writeln("function fChangeCountryOfSup(value, selectedIndex, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setSupCountry(value);");	        
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetCountryOfSup(selectedIndex);");
	d.writeln("}");

        d.writeln("function fSetCountryOfSup(selectedIndex, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.popSupplyCountry\" + nLine); ");
	d.writeln("	if (e) e.selectedIndex = selectedIndex;}");

/* -----------------------------
-- Change Tax Code
----------------------------- */

	d.writeln("function fChangeTaxCode(value, selectedIndex, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
	d.writeln("     r.setTaxCode(value);");
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetTaxCode(selectedIndex);");
        d.writeln("}");

        d.writeln("function fSetTaxCode(selectedIndex, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.popTaxCode\" + nLine);");
        d.writeln("  if (e) e.selectedIndex = selectedIndex;}");


/* -----------------------------
-- Change Merch Name
----------------------------- */

	d.writeln("function fChangeMerchName(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setMerchant(value);");
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetMerchName(value);");
        d.writeln("}");

        d.writeln("function fSetMerchName(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.txtMerchName\" + nLine);");
        d.writeln("  if (e) e.value = value;}");


/* -----------------------------
-- Change Doc Num
----------------------------- */

	d.writeln("function fChangeDocNum(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setDocNum(value);");   
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetDocNum(value);");
        d.writeln("}");

        d.writeln("function fSetDocNum(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.txtDocNum\" + nLine);");
        d.writeln("  if (e) e.value = value;}");

/* -----------------------------
-- Change Registration Num
----------------------------- */

	d.writeln("function fChangeTaxRegNum(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setTaxRegNum(value);");   
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetTaxRegNum(value);");
        d.writeln("}");

        d.writeln("function fSetTaxRegNum(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.txtTaxRegNum\" + nLine); ");
	d.writeln("	if (e) e.value = value;}");

/* -----------------------------
-- Change AIT
----------------------------- */

	d.writeln("function fChangeAmtIncTax(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));"); 
        d.writeln("     r.setAmountIncludesTax(value);");
	d.writeln("	if (top.framMain.lines.document.tax_reqimg){");
	d.writeln("        if (r.getAmountIncludesTax()) top.framMain.lines.document.tax_reqimg.src = top.reqimg1.src;");
	d.writeln("	   else top.framMain.lines.document.tax_reqimg.src = top.g_strImgDir+\"APWPX4.gif\";");
	d.writeln("	}");
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetAmtIncTax(value);");
        d.writeln("}");

        d.writeln("function fSetAmtIncTax(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.cbxAIT\" + nLine); if (e) e.checked = value;}");

/* -----------------------------
-- Change Reference
----------------------------- */

	d.writeln("function fChangeReference(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setMerchRef(value);");        
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetReference(value);");
        d.writeln("}");

        d.writeln("function fSetReference(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.txtMerchRef\" + nLine); if (e) e.value = value;}");


/* -----------------------------
-- Change Tax Payer ID
----------------------------- */

	d.writeln("function fChangeMerchantTaxId(value, nLine) {");
	d.writeln("  	top.fUpdateLineStatus(nLine,true);");
	d.writeln("	var r = top.fGetReceiptObj(top.fGetObjExpReport().getNthReceipt(nLine));");
        d.writeln("     r.setTaxPayerID(value);");        
        d.writeln("     if (top.fGetObjDetail() != null && !top.fGetObjDetail().closed)");
        d.writeln("         top.fGetObjDetail().fDetailSetMerchantTaxId(value);");
        d.writeln("}");
        d.writeln("function fSetMerchantTaxId(value, nLine) {");
	d.writeln("		fUpdateButtonsFrame();");
        d.writeln("  var e = eval(\"document.data.txtTaxPayerID\" + nLine); if (e) e.value = value;}");


	d.writeln("function fPopulateLines(sourceLine,targetLine){");
	d.writeln("var curr = \"\"; ");    
	d.writeln("var actualIndex = top.fGetObjExpReport().getNthReceipt(targetLine);");
        d.writeln("if ((targetLine>0) && (actualIndex == -1)){");
        d.writeln(" // Trying to populate in a new line");
        d.writeln(" actualIndex = top.fGetObjExpReport().getTotalReceipts(\"OOP\")-1;");
	d.writeln("}");
	d.writeln("var cbxSelect = eval(\"document.data.cbxSelect\"+targetLine);");
        d.writeln("var dateField = eval(\"document.data.txtDate\"+targetLine);");
        d.writeln("var amountField = eval(\"document.data.txtAmount\"+targetLine);");
        d.writeln("var justificationField = eval(\"document.data.txtJustification\"+targetLine);");
        d.writeln("var totalField = eval(\"document.data.txtTotal\"+targetLine);");
	d.writeln("var popCurrency = eval(\"document.data.popCurrency\"+targetLine);");
	d.writeln("var popExpType = eval(\"document.data.popExpType\"+targetLine);");
        d.writeln("var txtGroup = eval(\"document.data.txtGroup\"+targetLine);");
	d.writeln("var cbxMissing = eval(\"document.data.cbxReceiptMissing\"+targetLine);");
        d.writeln("var objTemp = top.fGetReceiptObj(sourceLine);");
	d.writeln("var txtProject = eval(\"document.data.AP_WEB_PA_PROJECT_NUMBER\"+targetLine);");
	d.writeln("var txtTask = eval(\"document.data.AP_WEB_PA_TASK_NUMBER\"+targetLine);");
	// for bug 1363873
	d.writeln("if (txtProject) txtProject.value = objTemp.getNAProjectNumber();");
	d.writeln("if (txtTask) txtTask.value = objTemp.getNATaskNumber();");

	d.writeln("dateField.value = objTemp.date;");
	d.writeln("curr = objTemp.receiptCurr;");
	d.writeln("cbxSelect.value = sourceLine;");
	d.writeln("popCurrency.selectedIndex = top.fGetCurrencyIndex(objTemp.receiptCurr);");
	d.writeln("var i=0;");
	d.writeln("if (objTemp.getObjExpenseType())");
        d.writeln("  i = top.fGetSelectedIndex(popExpType.options, objTemp.getObjExpenseType().mGetstrName());");
        d.writeln("if (i > 0) ");
        d.writeln("    popExpType.selectedIndex = i;");
        d.writeln("else");
        d.writeln("    popExpType.selectedIndex = 0; // blank");
	d.writeln("amountField.value = top.fMoneyFormat(objTemp.expensedAmount,curr);");
        d.writeln("justificationField.value = objTemp.getJustification();");
	d.writeln("totalField.value = top.fMoneyFormat(objTemp.getReimbursAmount(),curr);");
        d.writeln("if (txtGroup)");
        d.writeln("   txtGroup.value = objTemp.getGroup();");
	d.writeln("cbxMissing.checked = objTemp.getIsMissing();");

	d.writeln("if (top.g_bTaxEnable){");
 	d.writeln("  popTaxCode = eval(\"document.data.popTaxCode\"+targetLine);");
	d.writeln("  if (popTaxCode){");
        d.writeln("    popTaxCode.selectedIndex = 0;");
        d.writeln("    for (i=0;i<popTaxCode.options.length;i++){");
	d.writeln("     if (popTaxCode.options[i].value == objTemp.getTaxCode())");
	d.writeln("       popTaxCode.selectedIndex = i;");
	d.writeln("    }");
	d.writeln("  }");
	
	d.writeln("  cbxAIT = eval(\"document.data.cbxAIT\"+targetLine);");
	d.writeln("  if (cbxAIT) cbxAIT.checked = objTemp.getAmountIncludesTax();");

	d.writeln("  if (top.g_bEnableNewTaxFields == 'Y'){");
	d.writeln("    popSupplyCountry =  eval(\"document.data.popSupplyCountry\"+targetLine);");
	d.writeln("    if (popSupplyCountry){");
	d.writeln("	popSupplyCountry.selectedIndex = 0;");
        d.writeln("	for (i=0;i<popSupplyCountry.options.length;i++){");
	d.writeln("   	  if (popSupplyCountry.options[i].value == objTemp.getSupCountry())");
	d.writeln("   	  popSupplyCountry.selectedIndex = i;");
	d.writeln("	}");
	d.writeln("    }");
	d.writeln("    txtDocNum = eval(\"document.data.txtDocNum\"+targetLine);");
	d.writeln("    txtDocNum.value = objTemp.getDocNum();");

	d.writeln("    txtTaxRegNum = eval(\"document.data.txtTaxRegNum\"+targetLine);");
	d.writeln("    txtTaxRegNum.value = objTemp.getTaxRegNum();");
	
	d.writeln("    txtMerchRef = eval(\"document.data.txtMerchRef\"+targetLine);");
	d.writeln("    txtMerchRef.value = objTemp.getMerchRef();");

	d.writeln("    txtTaxPayerID = eval(\"document.data.txtTaxPayerID\"+targetLine);");
	d.writeln("    txtTaxPayerID.value = objTemp.getTaxPayerID();");
	d.writeln("  }");
	
	d.writeln(" }");
  	d.writeln("}");
}


function fDuplicateLines(bDetail){
	nLine = framMain.lines.g_iCurrentLine;
	var iRecIndex = fGetObjExpReport().getNthReceipt(nLine);
	var select_field = null;
	var tempIndex = 0;
	var nNewLine = nLine; 
	if (!top.Nav4 && top.fGetObjDetail() != null && !top.fGetObjDetail().closed)
 	  top.fGetObjDetail().fDetailIEOnChangeTrigger();

         if (top.fGetReceiptObj(iRecIndex) == null) return;

        // Store flex field values in receipt object
	fGetReceiptObj(iRecIndex).setFlexValues();
	if (!bDetail) { //not called from detail window
	   for (var i=1;i<=top.g_iTotalDisplayedLines;i++){
            select_field = eval("top.framMain.lines.document.data.cbxSelect"+i);
            if (select_field.checked){
        	   iRecIndex = fGetObjExpReport().getNthReceipt(i);
		   if (fGetObjExpReport().getTotalReceipts('OOP')==top.g_iTotalDisplayedLines)
		   {
		    
        		   fCreateMoreLines(top.g_iInitialDisplayedLines,false);
        	    }
		   else select_field.checked = false; // for bug 1367140
		   nNewLine = top.fGetObjExpReport().getTotalReceipts('OOP')+1;
        	   framMain.lines.fPopulateLines(iRecIndex,nNewLine);
		   fGetObjExpReport().addReceipt("",1,0,0,"N",fGetObjHeader().getReimbursCurr(),1,null,"",null,"",null,"",null,"","","","");
		   tempIndex = top.fGetObjExpReport().getNthReceipt(top.fGetObjExpReport().getTotalReceipts('OOP'));
		   fGetReceiptObj(tempIndex).addTax('','','','','','','','N','N');
		   fGetReceiptObj(tempIndex).copy(top.fGetReceiptObj(iRecIndex));
		   fGetReceiptObj(tempIndex).setItemizeId(null);  //reset itemize id
		   fGetReceiptObj(tempIndex).setReceiptAmount(fGetReceiptObj(tempIndex).getExpensedAmount());  //reset itemize id
             }
           } //end for
	} //end if
	else { //detail
	           iRecIndex = fGetObjExpReport().getNthReceipt(nLine);
		   if (fGetObjExpReport().getTotalReceipts('OOP')==top.g_iTotalDisplayedLines)
		   {
		    
        		   fCreateMoreLines(top.g_iInitialDisplayedLines,false);
        	    }//end if
		   nNewLine = top.fGetObjExpReport().getTotalReceipts('OOP')+1;
        	   framMain.lines.fPopulateLines(iRecIndex,nNewLine);
		   fGetObjExpReport().addReceipt("",1,0,0,"N",fGetObjHeader().getReimbursCurr(),1,null,"",null,"",null,"",null,"","","","");
		   tempIndex = top.fGetObjExpReport().getNthReceipt(top.fGetObjExpReport().getTotalReceipts('OOP'));
		   fGetReceiptObj(tempIndex).addTax('','','','','','','','N','N');
		   fGetReceiptObj(tempIndex).copy(top.fGetReceiptObj(iRecIndex));  
		   fGetReceiptObj(tempIndex).setItemizeId(null);  //reset itemize id  
		   fGetReceiptObj(tempIndex).setReceiptAmount(fGetReceiptObj(tempIndex).getExpensedAmount());  //reset itemize id
        }//end else
	var f = framMain.header.document.reportHeaderForm;
    	f.txtCostCenter.focus();
    	f.txtPurpose.focus();
    	if (f) if (f.AP_WEB_FULLNAME) fChangeApprover(f.AP_WEB_FULLNAME.value,f.AP_WEB_EMPID.value);
    	if (fIsOOP()){
	 txtD = eval('framMain.lines.document.data.txtDate'+nNewLine);
	 txtJ = eval('framMain.lines.document.data.txtJustification'+nNewLine);
	 if (txtD.value) framMain.lines.fChangeDate(txtD.value,nNewLine);
	 if (txtJ.value) framMain.lines.fChangeJustification(txtJ.value,nNewLine)
	 txtD.focus();
	 txtJ.focus();
    	}
	framMain.lines.g_iCurrentLine = nNewLine;
	select_field = eval("top.framMain.lines.document.data.cbxSelect"+nNewLine);
        select_field.checked = true;
	framMain.lines.fUpdateButtonsFrame();

	top.iLineNum = nNewLine;
	top.iRecIndex = fGetObjExpReport().getNthReceipt(nNewLine);

	if (top.fGetObjDetail() != null){
	 if (!top.fGetObjDetail().closed){
	    fGetObjDetail().iLineNum = nNewLine;
	    fGetObjDetail().iRecIndex = fGetObjExpReport().getNthReceipt(nNewLine);
	    fGenerateDetailButtons();
	    fUpdatePopShowReceipt();  	 
	    g_winDetail.bRunPopDetail = true;
	    fPopulateReceiptDetail(fGetObjExpReport().getNthReceipt(framMain.lines.g_iCurrentLine));
   	    fGetObjDetail().focus();
	 }
	}
} //end functin fDuplicateLines

function fClearLines(){
	var selectField = null;
	var actualIndex = 0;
	var counter = 0;
	// dtong for bug #1296911
        for (var i=1;i<=top.g_iTotalDisplayedLines;i++){
          selectField = eval("top.framMain.lines.document.data.cbxSelect"+i);
       
          if (selectField.checked){
                actualIndex = fGetObjExpReport().getNthReceipt(i-counter);
		if (actualIndex>=0){
		 fGetReceiptObj(actualIndex).taxInfo = null
		 fGetReceiptObj(actualIndex).flexFields = null;
		 fGetObjExpReport().oopReceipts[actualIndex] = null;
		 counter ++;
		}
          }
        }  //end for    
	var newLine = 1; 
	if (counter > 1){
	   newLine = 1;
	   top.framMain.lines.g_iCurrentLine = newLine;
	}
 	else {
	   newLine = top.framMain.lines.g_iCurrentLine - 1;
           if (newLine < 1) newLine = 1;
	   top.framMain.lines.g_iCurrentLine = newLine;
	} //end else
	
        fCreateMoreLines(0,false); 
	framMain.lines.fUpdateButtonsFrame();
	selectField = eval("top.framMain.lines.document.data.cbxSelect"+newLine);
        selectField.checked = true;
    	
	if (top.fGetObjDetail() != null){
	 if (!top.fGetObjDetail().closed){
	  fGetObjDetail().iLineNum = newLine;
	  fGetObjDetail().iRecIndex = fGetObjExpReport().getNthReceipt(newLine);
	  fGenerateDetailButtons();
	  fUpdatePopShowReceipt();
  	  top.g_winDetail.bRunPopDetail = true;
	  fPopulateReceiptDetail(fGetObjExpReport().getNthReceipt(framMain.lines.g_iCurrentLine));
	 }
        }
} //end function fClearLines


/* ------------------------------
PROCEDURE GenCurrencyWin IS
------------------------------ */
        function fGenCurrencyWindow(iRecIndex,nLine){
        var targetFrame = eval(top.g_winCurrency);
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CURRENCY_WIN');
	if (fGetReceiptObj(iRecIndex) == null){
	var receiptCurr = "";
	var reimbursCurr = fGetObjHeader().getReimbursCurr();
	var expensedAmount = fMoneyFormat("0",reimbursCurr);
	var exchRate = "1";
        var total = "";
	
        }
	else{
        var receiptCurr = fGetReceiptObj(iRecIndex).receiptCurr;
	var reimbursCurr = fGetObjHeader().getReimbursCurr();
	var expensedAmount = fMoneyFormat(fGetReceiptObj(iRecIndex).expensedAmount,reimbursCurr);
	var exchRate = fGetReceiptObj(iRecIndex).exchRate;
        var total = fGetReceiptObj(iRecIndex).getReimbursAmount();
	}
	var d = top.g_winCurrency.document;
	
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        fGenerateStyleCodes(targetFrame);
	d.writeln("<title>"+tempRegion.mobjGetRegionItem('AP_WEB_CURR_CONV').mstrGetPrompt()+"</title>");
        d.writeln("<body class=color3>");
        d.writeln("<form name=formCurrency>");
        d.writeln("<!--beginning of no-tab container code-->");
        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>");
        d.writeln("<TR class=colorg5>");
        d.writeln("          <TD ROWSPAN=2 valign=top align=left width=1><IMG SRC="+top.g_strImgDir+"APWCTTL.gif></TD>");
        d.writeln("          <TD class=color6 Width=1000 HEIGHT=1><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD>");
        d.writeln("          <TD ROWSPAN=2 align=right width=1><IMG SRC="+top.g_strImgDir+"APWCTTR.gif></TD>");
        d.writeln("  </TR>");
        d.writeln("  <TR>");
        d.writeln("          <TD height=5 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif></TD>");
        d.writeln("  </TR>");
        d.writeln("  <TR>");
        d.writeln("          <TD class=colorg5 COLSPAN=3 valign=top align=center> ");
	d.writeln("	     <table width=95% border=0><tr><td>");
        d.writeln("          <font class=helptext>"+top.g_objMessages.mstrGetMesg("AP_WEB_CURR_WIN_HEADER")+"</font><br><br>");
	d.writeln("</td></tr></table>");
        d.writeln("          <hr width=95%>");
        d.writeln("          </TD>");
        d.writeln("  </TR>");
        d.writeln("  <TR align=center>");
        d.writeln("  <TD class=colorg5 COLSPAN=3>");
        d.writeln("<center>");
        d.writeln("<table border=0 cellpadding=0 cellspacing=0 width=100%>");
        d.writeln("<tr>");
        d.writeln("<td align=right><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt()+"&nbsp;</font></td>");
        d.writeln("<td align=left><font class=datablack>"+expensedAmount+"&nbsp;</font></td>");

        d.writeln("</tr>");
        d.writeln("<tr><td></td></tr>");

        d.writeln("<tr>");

        d.writeln("<td align=right nowrap><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_RECCURR').mstrGetPrompt()+"&nbsp;</font></td>");
	d.writeln("<td><select name=popReceiptCurr onChange = \"top.opener.top.fCurrWinTempChangeCurrency("+nLine+");\">"+top.fGenRecCurrPopOptions(false)+"</select></td>");

        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td align=right><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_CONVRATE2').mstrGetPrompt()+"&nbsp;</font></td>");
        d.writeln("<td><font class=promptblack><input type = \"text\" name=txtRate onChange = \"top.opener.top.fCurrWinTempChangeRate("+nLine+");\"> ");
       
        d.writeln(" </font></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("<td align=right><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_CONV_AMT').mstrGetPrompt()+"("+reimbursCurr+")&nbsp;</font></td>");
        d.writeln("<td><input type = \"text\" name=txtTotal onfocus=\"this.blur()\" Disabled></td>");
        d.writeln("</tr>");

        d.writeln("</table>");
        d.writeln("</center>");

        d.writeln("</td></tr>");

        d.writeln("<TR class=colorg5>");
        d.writeln("  <TD><IMG SRC="+top.g_strImgDir+"APWCTBL.gif height=5 width=5></TD>");
        d.writeln("  <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=1></TD>");
        d.writeln("  <TD><IMG SRC="+top.g_strImgDir+"APWCTBR.gif height=5 width=5></TD>");
        d.writeln("</TR></table>");

        d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");

        d.writeln("    <TR><td height=5></td></TR>");
        d.writeln("    <TR>");
        d.writeln("    <TD align=right>");

        d.writeln("  <table border=0 cellspacing=0 cellpadding=0>");
        d.writeln("  <tr align=right valign=center>");
        d.writeln("  <td valign=center>");
        d.writeln("  <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
        d.writeln("  document.write(opener.top.fDynamicButton(\""+tempRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"\",");
        d.writeln("                                  \"APWBRNDL.gif\",");
        d.writeln("                                  \"APWBRNDR.gif\",");
        d.writeln("\""+tempRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"\",");
        d.writeln("                                  \"javascript:opener.top.fCloseCurrWin()\",");
        d.writeln("                                  'US',");
        d.writeln("                                  false,");
        d.writeln("                                  false,");
        d.writeln("                                  'top.framMain.lines'));");
        d.writeln("  <\/SCRIPT>");
        d.writeln("  </td>");
	d.writeln("  <td width=5>");
	d.writeln("  </td>");
        d.writeln("  <td valign=center>");
        d.writeln("  <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
        d.writeln("  document.write(opener.top.fDynamicButton(\""+tempRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
        d.writeln("                                  \"APWBRNDL.gif\",");
        d.writeln("                                  \"APWBRNDR.gif\",");
        d.writeln("                                  \""+tempRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
        d.writeln("                                  \"javascript:opener.top.fSaveCurrencyInfo("+nLine+")\",");
        d.writeln("                                  'US',");
        d.writeln("                                  false,");
        d.writeln("                                  false,");
        d.writeln("                                  'top.framMain.lines'));");
        d.writeln("  <\/SCRIPT>");
        d.writeln("  </td>");
        d.writeln("  </tr>");
        d.writeln("  </table>");

        d.writeln("  </TD>");
        d.writeln("  </TR>");
        d.writeln("</TABLE>");
        d.writeln("</form>");
        d.writeln("</body>");
        //d.writeln("</html>");

} // -- end of fGenCurrencyWindow


   function fCurrWinChangeRate(iRecIndex,nLine){
     var r = null;
     r = fGetReceiptObj(iRecIndex);
     var reimbursCurr = fGetObjHeader().getReimbursCurr();
     var receiptCurr = r.getReceiptCurr();
     var rateField = top.g_winCurrency.document.formCurrency.txtRate;
     var totalField = top.g_winCurrency.document.formCurrency.txtTotal;
     var strTotal = "";
     var action = fDetermineConversion(receiptCurr,reimbursCurr,r);
     var tempMesg = top.g_objMessages.mstrGetMesg("AP_WEB_FIXED");

     // for euro
     if ((action == top.g_reimbFixedRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbEurRecFixed)) {
	  if (rateField.value != tempMesg) {
	    alert(top.g_objMessages.mstrGetMesg("AP_WEB_DONOT_CHANGE_RATE"));
            rateField.value = tempMesg;
	    return false;
	  }
     }



     if (rateField.value+""!="")
     if (fIsNum(rateField.value,true)){
       if ((reimbursCurr == receiptCurr) && (rateField.value != "1")){
          alert(top.g_objMessages.mstrGetMesg("AP_WEB_RATE_ONE_WRN"));
	  rateField.value = "1";
	  return false;
       }
       else {
	if (r){
	var originRate = r.getExchRate();
	top.fUpdateLineStatus(nLine,true);
	r.setExchRate(rateField.value);
	strTotal = fCalcReimbursAmount(iRecIndex) - 0;
	totalField.value = fMoneyFormat(strTotal,reimbursCurr);
	r.setExchRate(originRate);
	return true;
	}
       }
     }
	else return true;
}


function fCurrWinChangeCurrency(nLine){
     var popLineCurrency = eval('top.framMain.lines.document.data.popCurrency'+nLine);
     var popCurrency = top.g_winCurrency.document.formCurrency.popReceiptCurr;
     var txtRate = top.g_winCurrency.document.formCurrency.txtRate;
     var strCurrency = popCurrency.options[popCurrency.selectedIndex].value;

     if (popLineCurrency.selectedIndex != popCurrency.selectedIndex){
       popLineCurrency.selectedIndex = popCurrency.selectedIndex;
       framMain.lines.fChangeCurrency(strCurrency,nLine);
     }
}


function fCurrWinTempChangeRate(nLine){
     var nActualLine = fGetObjExpReport().getNthReceipt(nLine);
     tempObj = fGetReceiptObj(nActualLine);
     var reimbursCurr = fGetObjHeader().getReimbursCurr();
     var popCurrency = top.g_winCurrency.document.formCurrency.popReceiptCurr;
     var strCurrency = popCurrency.options[popCurrency.selectedIndex].value;
     var rateField = top.g_winCurrency.document.formCurrency.txtRate;
     var totalField = top.g_winCurrency.document.formCurrency.txtTotal;
     var strTotal = "";
     var action = fDetermineConversion(strCurrency,reimbursCurr,tempObj);
     var tempMesg = top.g_objMessages.mstrGetMesg("AP_WEB_FIXED");

     // for euro
     if ((action == top.g_reimbFixedRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbEurRecFixed)) {
	  if (rateField.value != tempMesg) {
	    top.g_winCurrency.alert(top.g_objMessages.mstrGetMesg("AP_WEB_DONOT_CHANGE_RATE"));
            rateField.value = tempMesg;
	    return false;
	  }
     }

     if (rateField.value+""!="")
      if (fIsNum(rateField.value,true)){
       if ((reimbursCurr == strCurrency) && (rateField.value != "1")){
          top.g_winCurrency.alert(top.g_objMessages.mstrGetMesg("AP_WEB_RATE_ONE_WRN"));
	  rateField.value = "1";
	  return false;
       }
       else{
	 if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='N')
	    totalField.value = fMoneyFormat(tempObj.getExpensedAmount() * rateField.value,reimbursCurr);
	 else 
	    totalField.value = fMoneyFormat(tempObj.getExpensedAmount()/rateField.value,reimbursCurr);
	 return true;
       }
     }
	else return true;
}


function fCurrWinTempChangeCurrency(nLine){
     var reimbursCurr = fGetObjHeader().getReimbursCurr();
     var popLineCurrency = eval('top.framMain.lines.document.data.popCurrency'+nLine);
     var popCurrency = top.g_winCurrency.document.formCurrency.popReceiptCurr;
     var txtRate = top.g_winCurrency.document.formCurrency.txtRate;
     var strCurrency = popCurrency.options[popCurrency.selectedIndex].value;
     var nActualLine = fGetObjExpReport().getNthReceipt(nLine);
     var txtTotal = top.g_winCurrency.document.formCurrency.txtTotal;
     tempObj = fGetReceiptObj(nActualLine);
     var action = fDetermineConversion(strCurrency,reimbursCurr,tempObj);
	if (action == g_reimbEqualsRec) {
	          txtRate.value = 1;
		  txtTotal.value = fMoneyFormat(tempObj.getExpensedAmount(),reimbursCurr);
         } 
	else if (action == g_reimbEurRecFixed) {
	     txtRate.value = g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	     fixed_rate = fGetFixedRate(tempObj.getReceiptCurr());
		txtTotal.value = fMoneyFormat(tempObj.getExpensedAmount()*(1/eval(fixed_rate)),reimbursCurr);
	}
	else if (action == g_reimbFixedRecEur) {
	     fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
		txtTotal.value = fMoneyFormat(tempObj.getExpensedAmount()*fixed_rate,reimbursCurr);
	     txtRate.value = g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	}
	else if (action == g_reimbFixedRecFixed){
	     var rate1 = fGetFixedRate(strCurrency);
             var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
	     fixed_rate = fMultiply(rate2, rate1);
	     var tempReimbAmount = fMultiply((eval(tempObj.getExpensedAmount())/rate1), rate2);
	     txtTotal.value =  fMoneyFormat(tempReimbAmount,reimbursCurr);
	     txtRate.value = g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	}
	else{
		 txtRate.value = 1;
	         txtTotal.value = fMoneyFormat(tempObj.getExpensedAmount(),reimbursCurr);
	}
}


function fGetAndSetExchRate(objR){
     var reimbursCurr = fGetObjHeader().getReimbursCurr();
     if (objR){
       var receiptCurr = objR.receiptCurr;
       var receiptRate = objR.getExchRate();
       var action = top.fDetermineConversion(objR.getReceiptCurr(), fGetObjHeader().getReimbursCurr(),objR);
    
	if (action == g_reimbEqualsRec) {
	    return receiptRate;
         } 
	else if (action == g_reimbEurRecFixed) {
	    fixed_rate = fGetFixedRate(objR.getReceiptCurr());
	    objR.setExchRate(1/eval(fixed_rate));
	    return g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	}
	else if (action == g_reimbFixedRecEur) {
	    fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
	    objR.setExchRate(fixed_rate);
	    return g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	}
	else if (action == g_reimbFixedRecFixed){
	    var rate1 = fGetFixedRate(objR.getReceiptCurr());
            var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
	    objR.setExchRate(fMultiply(rate2, rate1));
	    return g_objMessages.mstrGetMesg('AP_WEB_FIXED');
	}
    }
    return(receiptRate);
}


 function fCurrWinStoreRate(nLine){
     var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
     var reimbursCurr = fGetObjHeader().getReimbursCurr();
     var iRecIndex = top.fGetObjExpReport().getNthReceipt(nLine);
     var popCurrency = top.g_winCurrency.document.formCurrency.popReceiptCurr;
     var strCurrency = popCurrency.options[popCurrency.selectedIndex].value;
     var receiptCurr = top.fGetReceiptObj(iRecIndex).receiptCurr;
     var rateField = top.g_winCurrency.document.formCurrency.txtRate;
     var strTotal = "";
     var objR = fGetReceiptObj(iRecIndex);
     if (objR){
   	var action = top.fDetermineConversion(strCurrency, fGetObjHeader().getReimbursCurr(),objR);
    
        fUpdateLineStatus(nLine,true);
	if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;

	if (action == g_reimbEqualsRec) {
	    objR.setExchRate(rateField.value);
         } 
	else if (action == g_reimbEurRecFixed) {
	    fixed_rate = fGetFixedRate(objR.getReceiptCurr());
	    objR.setExchRate(1/eval(fixed_rate));
	}
	else if (action == g_reimbFixedRecEur) {
	    fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
	    objR.setExchRate(fixed_rate);
	}
	else if (action == g_reimbFixedRecFixed){
	    var rate1 = fGetFixedRate(objR.getReceiptCurr());
            var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
	    objR.setExchRate(fMultiply(rate2, rate1));
	}

       framMain.lines.fChangeReceiptAmount(objR.expensedAmount,nLine);
       fPopulateLineTotal(nLine,false);
       framMain.lines.fUpdateButtonsFrame();
       return true;
     }
     else return false;
       
}

/* -----------------------------
PROCEDURE ReqFieldsEntered 
----------------------------- */

function fShowTitle(iTab){
      if (iTab == 1)
        return "<tr><td colspan=2><font class=datablack>Out of Pocket Receipts</font></td></tr>";
      else
	return "<tr><td colspan=2><font class=datablack>Credit Card Transactions</font></td></tr>";
}


function fGenError(strName,strTemp,bShowLinePrompt,iLine, iTab){
// iTab == 1 : OOP
// iTab == 2 : CC
var strErrorContent = "";
var errorRegion = top.g_objRegions.mobjGetRegion('AP_WEB_ERROR_WIN');

	  if (!bShowLinePrompt){
	    strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.fFocusOnFlexField('"+strName+"'," +iLine + ","+ iTab + ");\">";
	    strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+iLine+"</a></font></td>";
	    strErrorContent += "<td><font class=promptblack>"+strTemp+"</font></td></tr>";
	    bShowLinePrompt = true;
	  }
	  else
	    strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+strTemp+"</font></td></tr>";
  return strErrorContent;
 
}

function fDrawErrorWindow(strTitle,strErrorContent){
top.g_strErrorContent = "";
 
  if (bVersion.ns4){

   if ( (strErrorContent != top.g_strErrorContent))
   {
     top.fOpenErrorWindow(strTitle,strErrorContent);
     top.g_strErrorContent = strErrorContent;
     return false;
   }
   else if ((strErrorContent != top.g_strErrorContent) && ((top.g_winError == null) || (top.g_winError.closed)) )
   {
     top.fOpenErrorWindow(strTitle,strErrorContent);
     top.g_strErrorContent = strErrorContent;
     return false;
   }  
   else if (strErrorContent == "")
   {
   	 strErrorContent += "";
     
    	if ((top.g_winError == null) || (top.g_winError.closed))
	{}
    	else
	{
      	      top.g_winError.focus();
    	}
   	 return  true;
   }
  }//bversion
  else
  {
     if ((strErrorContent != top.g_strErrorContent))
     {
      
	      if ((top.g_winError == null) || (top.g_winError.closed))
	       	top.fOpenErrorWindow(strTitle,strErrorContent);
		
              else
       		top.fGenErrorBody(strErrorContent);
                
      	top.g_strErrorContent = strErrorContent;
     	 return false;
      
     }
     else if (strErrorContent == "")
     {
     	 strErrorContent += top.g_objMessages.mstrGetMesg('AP_WEB_NO_ERROR');
      
	 if ((top.g_winError == null) || (top.g_winError.closed))
	 {}
         return true;
     }
  }//end of else bversion

}//end of function fDrawErrorWindow




function fRequiredFieldsEntered(strTitle,bFullCheck,iReceiptIndex) {
  // If iReceiptIndex >= 0 means we are check a receipt for calculating amount.
  var strErrorContent = "";
  top.g_strErrorContent = "";
  var bErrorFound = false;
  var targetField = null;
  var errorRegion = top.g_objRegions.mobjGetRegion('AP_WEB_ERROR_WIN');
  if ((top.fGetObjDetail()==null) || (top.fGetObjDetail().closed)) top.g_strPreviousExpType = "";


/*--------------------
-- Validate Employee
-------------------- */

 if (bFullCheck){ 
  if (top.objEnvironment.bEmployeeNameReq){
     targetField = top.framMain.header.document.reportHeaderForm.popEmployeeId;
     if (fGetObjHeader().getObjEmployee().mGetstrEmployeeID() == ""){
        strErrorContent += "<tr><td colspan=2><font class=promptblack><a href=\"javascript:top.opener.top.framMain.header.document.reportHeaderForm.popEmployeeId.focus();\">Employee Name is Required</a></font></td></tr>"

     }
  }
 }


/* ----------------------
-- Validate CostCenter
---------------------- */

 if (bFullCheck){ 
  if (top.objEnvironment.bCostCenterReq){

      targetField = top.framMain.header.document.reportHeaderForm.txtCostCenter;
      if (targetField.value+"" == ""){
        strErrorContent += "<tr><td colspan=2><font class=promptblack><a href=\"javascript:top.opener.top.framMain.header.document.reportHeaderForm.txtCostCenter.focus();\">"+top.g_objMessages.mstrGetMesg('AP_WEB_COST_CENTER_MISSING')+"</a></font></td></tr>"

      }
  }
 }


/* ------------------------
-- Validate Approver
------------------------ */

 if (bFullCheck){
  if ((top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_OVERRIDE_APPROVER')=='Y') && (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_OVERRIDE_APPR_REQ')=='Y')){
       targetField = top.framMain.header.document.reportHeaderForm.AP_WEB_FULLNAME;
      if (targetField.value+"" == ""){
        strErrorContent += "<tr><td colspan=2><font class=promptblack><a href=\"javascript:top.opener.top.framMain.header.document.reportHeaderForm.AP_WEB_FULLNAME.focus();\">"+top.g_objMessages.mstrGetMesg('AP_WEB_OVERRIDER_REQUIRED')+"</a></font></td></tr>"

      }
    }
   if ((top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_OVERRIDE_APPROVER')=='Y') && (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_APPROVER_REQ_CC')=='Y')){
       if (fGetObjHeader().getCostCenter()!=fGetObjHeader().getObjEmployee().strCostCeter){
       		targetField = top.framMain.header.document.reportHeaderForm.AP_WEB_FULLNAME;
      		if (targetField.value+"" == ""){
        	strErrorContent += "<tr><td colspan=2><font class=promptblack><a href=\"javascript:top.opener.top.framMain.header.document.reportHeaderForm.AP_WEB_FULLNAME.focus();\">"+top.g_objMessages.mstrGetMesg('AP_WEB_OVERRIDER_REQ_DIFF_CC')+"</a></font></td></tr>"
      		}
	}
    }
  
 }


/* -----------------------
-- Validate purpose
----------------------- */

 if (bFullCheck){
  if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_PURPOSE_REQUIRED')=='Y'){
       targetField = top.framMain.header.document.reportHeaderForm.txtPurpose;
      if (targetField.value+"" == ""){
        strErrorContent += "<tr><td colspan=2><font class=promptblack><a href=\"javascript:top.opener.top.framMain.header.document.reportHeaderForm.txtPurpose.focus();\">"+top.g_objMessages.mstrGetMesg('AP_WEB_PURPOSE_REQUIRED')+"</a></font></td></tr>"

      }
   }
 }

// chiho: add the checking for "bFullCheck" to allow expense report with 0 receipt to be "saved".
if ( bFullCheck ) 
{
  if ((top.fGetObjExpReport().getTotalReceipts('OOP')==0) && (top.fGetObjExpReport().getTotalReceipts('CC')==0)){
   strErrorContent += "<tr><td><br><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_ONE_RECPT_REQUIRE')+"</font></td></tr>";  }
}

/* -----------------------
--  Validate Lines
----------------------- */

 for (var tab=1;tab<=2;tab++){ // loop through both tabs
  var orig_tab = top.g_strCurrentTab;
  var nActualLine = 0;
  var bShowLinePrompt = false;
  var bAnyError = false;
  var r = null;
  var strTab = "OOP";
  if (tab == 1){
    if(top.fGetObjExpReport().getTotalReceipts('OOP')>0){
	top.g_strCurrentTab = 'OOP';
	r = top.fGetObjExpReport().oopReceipts;
	strTab = "OOP";
    }
  }
  else if (top.fGetObjExpReport().getTotalReceipts('CC')>0){
	top.g_strCurrentTab = 'CC';
	r = top.fGetObjExpReport().cCardReceipts;
	strTab = "CC";
  }
  //strErrorContent += "<tr><td><br></td></tr>";
  if (r)
  for (var i=1;i<=top.fGetObjExpReport().getTotalReceipts(tab == 1?'OOP':'CC');i++){
    var nActualLine = top.fGetObjExpReport().getNthReceipt(i);
    bShowLinePrompt = false;

/* -------------------------------
-- Check Receipt Date
------------------------------- */

    if (r[nActualLine].date+"" == ""){
       if (bFullCheck){
          if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  
       }
          if (!bShowLinePrompt){
       		strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.fSwitchTabs('"+strTab+"');top.opener.top.framMain.lines.document.data.txtDate"+i+".focus();\">"+errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i;
		strErrorContent += "</a></font></td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_VALID_ST_DATE_REQUIRED')+"</font></td></tr>";
       		bShowLinePrompt = true;
     	   }
            else
     	    	strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_VALID_ST_DATE_REQUIRED')+"</font></td></tr>";

	}
    }
    else // date is not null
    if (top.fStringToDate(r[nActualLine].date,false) == null){
     if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  }
     if (!bShowLinePrompt){
       strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.fSwitchTabs('"+strTab+"');top.opener.top.framMain.lines.document.data.txtDate"+i+".focus();\">";
	strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i+"</a></font></td><td><font class=promptblack>";
	strErrorContent += top.g_objMessages.mstrGetMesg('AP_WEB_VALID_ST_DATE_REQUIRED')+"</font></td></tr>";
       bShowLinePrompt = true;
     }
     else
     strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_VALID_ST_DATE_REQUIRED')+"</font></td></tr>";

    }
 
if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_TAX_ENABLE')=='Y'){
/* --------------------------
-- Check Tax Info
-------------------------- */

if (r[nActualLine].taxInfo)
  if (r[nActualLine].taxInfo.taxCode!=""){
	var exp_start_date = r[nActualLine].getDate();
        
	var tax_code_start_date = top.MyTaxes.getStartDate(r[nActualLine].taxInfo.mGetTaxCode());
	var tax_code_inactive_date = top.MyTaxes.getInactiveDate(r[nActualLine].taxInfo.mGetTaxCode());
        if (top.MyTaxes.getID(r[nActualLine].taxInfo.mGetTaxCode(),exp_start_date)!="")
	{
 	      r[nActualLine].taxInfo.mSetTaxID(top.MyTaxes.getID(r[nActualLine].taxInfo.mGetTaxCode(),exp_start_date));
	}
	else 
	{
	   if (bFullCheck){
          	if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  
       		}
          	if (!bShowLinePrompt){
       		  strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:if ((top.opener.top.fGetObjDetail()==null) || (top.opener.top.fGetObjDetail().closed)) top.opener.top.fOpenDetail('OOP',"+i+");";
		  strErrorContent += "else{tempField = top.opener.top.fGetObjDetail().framReceiptDetail.document.formDetail.popTaxCode; if (tempField) tempField.focus();}\">";
		strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i;
		  strErrorContent += "</a></font></td><td><font class=promptblack>" + top.g_objMessages.mstrGetMesg('AP_WEB_TAX_INVALID_DATE') + "</font></td></tr>";
       		bShowLinePrompt = true;
     	   	}
            	else
     	    	  strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>" + top.g_objMessages.mstrGetMesg('AP_WEB_TAX_INVALID_DATE') + "</font></td></tr>";

	       
	    }// bFullCheck
	}// else
	

	if ((tax_code_start_date!="") && (tax_code_inactive_date!="")){
	   if ((fStringToDate(tax_code_start_date)>fStringToDate(exp_start_date)) || (fStringToDate(exp_start_date)>fStringToDate(tax_code_inactive_date))){
	   var arrTokenNames = new Array();
	   var arrTokenVals = new Array();
	   arrTokenNames[0] = "&s_date";
	   arrTokenVals[0] = tax_code_start_date;
	   arrTokenNames[1] = "&e_date";
	   arrTokenVals[1] = tax_code_inactive_date;
	   var tempStr = top.g_objMessages.mstrGetMesgWTokenReplace('AP_WEB_TAX_DATE_RANGE', arrTokenNames, arrTokenVals);
	   if (bFullCheck){
		if (!bAnyError)
		{ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  		      }
          	if (!bShowLinePrompt){
       		  strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.framMain.lines.document.data.popTaxCode"+i+".focus();\">";
		strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i;
		  strErrorContent += "</a></font></td><td><font class=promptblack>"+ tempStr + "</font></td></tr>";
       		bShowLinePrompt = true;
     	   	}
            	else
     	    	  strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>" + tempStr +" </font></td></tr>";

	    } // bFullCheck
	  }
	 } // start_date != "" and inactive_date!=""
	

 } // taxCode!=""
 else{
     if (r[nActualLine].getAmountIncludesTax()){
	  if (bFullCheck){
          	if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  
       		}
          	if (!bShowLinePrompt){
       		  strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:if ((top.opener.top.fGetObjDetail()==null) || (top.opener.top.fGetObjDetail().closed)) top.opener.top.fOpenDetail('OOP',"+i+");";
		  strErrorContent += "else{tempField = top.opener.top.fGetObjDetail().framReceiptDetail.document.formDetail.popTaxCode; if (tempField) tempField.focus();}\">";
		strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i;
		  strErrorContent += "</a></font></td><td><font class=promptblack>" + top.g_objMessages.mstrGetMesg('AP_WEB_TAX_REQUIRED') + "</font></td></tr>";
       		bShowLinePrompt = true;
     	   	}
            	else
     	    	  strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>" + top.g_objMessages.mstrGetMesg('AP_WEB_TAX_REQUIRED') + "</font></td></tr>";

	       
	    }// bFullCheck


    }
 } // tax_code is null
}
/* ---------------------------
-- Check Receipt Amount
--------------------------- */

    if ((r[nActualLine].expensedAmount -0 == 0) || (r[nActualLine].expensedAmount == null)){
       if (bFullCheck){
     	if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  }
     	if (!bShowLinePrompt){
        	strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.fSwitchTabs('"+strTab+"');top.opener.top.framMain.lines.document.data.txtAmount"+i+".focus();\">"+errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i;
		strErrorContent += "</a></font></td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_RECEIPT_AMOUNT_MISSING')+"</font></td></tr>";
        	bShowLinePrompt = true;
     	 }
     	 else strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_RECEIPT_AMOUNT_MISSING')+"</font></td></tr>";

        }
    }
    else if (!top.fIsNum((r[nActualLine].expensedAmount+""),false)){
	if (!bAnyError){ 
		strErrorContent += top.fShowTitle(tab);   bAnyError = true;         	    }
    
     var arrTokenNames = new Array();
     var arrTokenVals = new Array();
     arrTokenNames[0] = "&RECEIPT_AMOUNT";
     arrTokenVals[0] = r[nActualLine].expensedAmount+"";
     var strTemp = top.g_objMessages.mstrGetMesgWTokenReplace("AP_WEB_RECEIPT_AMOUNT_INVALID",arrTokenNames,arrTokenVals);
     if (!bShowLinePrompt){
        strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.framMain.lines.document.data.txtAmount"+i+".focus();\">";
	strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i+"</a></font></td><td><font class=promptblack>"+strTemp+"</font></td></tr>";
        bShowLinePrompt = true;
     }
     else strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+strTemp+"</font></td></tr>";

	}
	
   
/* --------------------------
-- Check Expense Type
-------------------------- */

  var expTypeError = false;
// chiho:check the 'expense type' only when the bFullCheck flag was set.
if ( bFullCheck )
{
  if (!r[nActualLine].getObjExpenseType()) expTypeError = true;
  else
   if (r[nActualLine].getObjExpenseType()){
    if (r[nActualLine].getObjExpenseType().mGetstrName() == "")
	expTypeError = true;
   }
  if (expTypeError)
    {
     if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  }
     if (!bShowLinePrompt){
       strErrorContent += "<tr><td><font class=promptblack>&nbsp&nbsp<a href=\"javascript:top.opener.top.fSwitchTabs('"+strTab+"');top.opener.top.framMain.lines.document.data.popExpType"+i+".focus();\">";
	strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i+"</a></font></td><td><font class=promptblack>";
	strErrorContent += top.g_objMessages.mstrGetMesg('AP_WEB_EXPENSE_TYPE_REQUIRED')+"</font></td></tr>";
       bShowLinePrompt = true;
     }
     else
     strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_EXPENSE_TYPE_REQUIRED')+"</font></td></tr>";

    }
} // if bFullCheck



/* ----------------------------
-- Check Justification
---------------------------- */

 if (bFullCheck){  
    var bIsReq = 'N';
    if (r[nActualLine].getObjExpenseType())
      bIsReq = r[nActualLine].getObjExpenseType().mGetbJustifRequired();
    if (bIsReq == 'Y'){
	if ((r[nActualLine].getJustification() == "") || (!r[nActualLine].getJustification())){
          if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  }
     	  if (!bShowLinePrompt){
       		strErrorContent += "<tr><td align=left valign=top><font class=promptblack>&nbsp;&nbsp;<a href=\"javascript:top.opener.top.fSwitchTabs('"+strTab+"');top.opener.top.framMain.lines.document.data.txtJustification"+i+".focus();\">";
		strErrorContent += errorRegion.mobjGetRegionItem('AP_WEB_LINE').mstrGetPrompt()+i+"</a></font></td><td><font class=promptblack>";
		strErrorContent += top.g_objMessages.mstrGetMesg('AP_WEB_JUSTIFICATION_REQUIRED')+"</font></td></tr>";
       		bShowLinePrompt = true;
     	  }
     	  else
     		strErrorContent += "<tr><td>&nbsp</td><td><font class=promptblack>"+top.g_objMessages.mstrGetMesg('AP_WEB_JUSTIFICATION_REQUIRED')+"</font></td></tr>";

	}
     }
  }// bFullCheck




/* -----------------------------
-- Check required flex fields 
----------------------------- */
if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_DESC_FLEX_NAME')=='Y'){
 if (bFullCheck){    
    /*
    if (!r[nActualLine].flexFields){
	r[nActualLine].flexFields = new Array();
  	if (r[nActualLine].getObjExpenseType()) fAddFlexFields(r[nActualLine], r[nActualLine].getObjExpenseType().mGetstrCode());
    }
    */
    var strTemp = "";
    var objContext, objFlexFields;
    var iNumOfGlobalSegments = 0;
    // validate global segments first
    if ((top.g_strGlobalContext) && (r[nActualLine].getObjExpenseType())){
    objContext = g_objContexts.mobjGetContextWithDefault(top.g_strGlobalContext);
    for (var j=0;j<objContext.miGetNumOfFlexField();j++){
      if (r[nActualLine].flexFields)
        strTemp = objContext.arrFlexFields[j].mValidateFlexField(r[nActualLine].getFlexArrValue(j));
      else 
	strTemp = objContext.arrFlexFields[j].mValidateFlexField("");
      if (strTemp){
      	if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   bAnyError = true;  }
	var strWidgetName = "";
	var strPrompt = objContext.arrFlexFields[j].mGetstrPrompt();
	if (strPrompt){
	   strWidgetName = strPrompt.replace(/ /g,"_");
	   strWidgetName = strWidgetName.replace(/\//g,"_");
        }
      	strErrorContent += top.fGenError(strWidgetName,strTemp,bShowLinePrompt,i, tab);
      	bShowLinePrompt = true;
      }
    }
    
    iNumOfGlobalSegments = objContext.miGetNumOfFlexField();
    }


    // validate context sensitive segments
    if (r[nActualLine].getObjExpenseType()){
    objContext = g_objContexts.mobjGetContextWithDefault(r[nActualLine].getObjExpenseType().mGetstrCode());
    var objRecFF = null;
    for (var j=iNumOfGlobalSegments;j<objContext.miGetNumOfFlexField()+iNumOfGlobalSegments;j++){
	if (r[nActualLine].flexFields) objRecFF = r[nActualLine].flexFields[j];
	if (objRecFF)
      	  strTemp = objContext.arrFlexFields[j-iNumOfGlobalSegments].mValidateFlexField(objRecFF.value);
        else 
	  strTemp = objContext.arrFlexFields[j-iNumOfGlobalSegments].mValidateFlexField("");
      	  if (strTemp){
      		if (!bAnyError){ strErrorContent += top.fShowTitle(tab);   
		    bAnyError = true;  
		}
		var strWidgetName = "";
		var strPrompt = objContext.arrFlexFields[j-iNumOfGlobalSegments].mGetstrPrompt();
	        if (strPrompt){
	   	  strWidgetName = strPrompt.replace(/ /g,"_");
	   	  strWidgetName = strWidgetName.replace(/\//g,"_");
        	}
      		strErrorContent += top.fGenError(strWidgetName ,strTemp,bShowLinePrompt,i, tab);
      		bShowLinePrompt = true;

      	  }
	
    }
    }// if (r[nActualLine].getObjExpenseType())

    
      }//bFullCheck
    }//flex field enabled
   }  // end of for loop
  bAnyError = false;
 } //end of tab loop 
  top.g_strCurrentTab = orig_tab;

   return strErrorContent;

   
} // added by priyai -end of frequiredfieldsentered






/* ------------------------
    GenTaxFunctions
   ------------------------ */

  // This object contains three methods: getID, getStartDate, and 
  // getInactiveDate. This object also contains one array stores every 
  // row in ap_tax_codes table which is web-enabled.


var tax_item_count = 0;
function tax_object(taxCode,taxId,startDate,inactiveDate){
        this.taxCode = taxCode;
	this.taxId = taxId;
        this.startDate = startDate;
        this.inactiveDate = inactiveDate;
}

function tax_array(){
        this.arr = new Array();
}

function T_add(taxCode,taxId,startDate,inactiveDate){
        this.arr[this.arr.length] = new tax_object(taxCode,taxId,startDate,inactiveDate);
}

tax_array.prototype.add = T_add;

// This function is trying to check if the entered start expense date is
// within any period of tax. If there is, then return the taxCode, otherwise
// return null and an error message should be popped up.

function T_get_id(taxCode,start_exp_date){
        for (var i=0;i<this.arr.length;i++){
	 if ((this.arr[i].startDate!="") && (this.arr[i].inactiveDate!="")){
          if ((this.arr[i].taxCode == taxCode) && (top.fStringToDate(this.arr[i].startDate,false)<=top.fStringToDate(start_exp_date,false)) && (top.fStringToDate(this.arr[i].inactiveDate,false) >= top.fStringToDate(start_exp_date,false))){
          return this.arr[i].taxId;
          exit;
          }
	 }
	 else if (this.arr[i].startDate!=""){
	  if ((this.arr[i].taxCode == taxCode) && (top.fStringToDate(this.arr[i].startDate,false)<=top.fStringToDate(start_exp_date,false))){
          return this.arr[i].taxId;
          exit;
	 }
        }
	}
        return "";
}
tax_array.prototype.getID = T_get_id;

function T_getSupCountryIndex(strSupCountry){
	return 0;
	// add codes here ...
}
tax_array.prototype.getSupCountryIndex = T_getSupCountryIndex;

function T_getTaxCodeIndex(strTaxCode,strTaxID){
	if (strTaxCode!=""){
	  for (var i=0;i<this.arr.length;i++){
	  if (this.arr[i].taxCode == strTaxCode)
	  return i;
	  }
	}
	else{
	  for (var i=0;i<this.arr.length;i++){
	  if (this.arr[i].taxId == strTaxID)
	  return i;
	  }
	}
	
	return -1;
}
tax_array.prototype.getTaxCodeIndex = T_getTaxCodeIndex;

// This function is trying to retrieve the start date for a valid date range
// for a passed tax code.

function T_get_start_date(taxCode){
        var start_date = "";
         for (var i=0;i<this.arr.length;i++){
          if (this.arr[i].taxCode == taxCode) {
            if ((this.arr[i].startDate!="") && (start_date=="")){
                start_date = top.fStringToDate(this.arr[i].startDate,false);
            }
            else
            if (top.fStringToDate(this.arr[i].startDate,false)<=start_date){
                start_date = top.fStringToDate(this.arr[i].startDate,false);
            }
          }
         }
        if ((start_date!="") && (start_date!=null)){
                return top.fDateToString(start_date);}
        else {return "";}
}
tax_array.prototype.getStartDate = T_get_start_date;

// This function is trying to retrieve the end date for a valid date range
// for a passed tax code.

function T_get_inactive_date(taxCode){
        var inactive_date = "";
         for (var i=0;i<this.arr.length;i++){
          if (this.arr[i].taxCode == taxCode) {
            if ((this.arr[i].inactiveDate!="") && (inactive_date=="")){
                inactive_date = top.fStringToDate(this.arr[i].inactiveDate,false);
            }
            else
            if (top.fStringToDate(this.arr[i].inactiveDate,false)>=inactive_date){
               inactive_date = top.fStringToDate(this.arr[i].inactiveDate,false);
            }
          }
         }
        if ((inactive_date!="") && (inactive_date!=null)){
                return top.fDateToString(inactive_date);}
        else {return "";}

}

tax_array.prototype.getInactiveDate = T_get_inactive_date;

      
function fFocusOnHeaderFrame(){
  // Make sure approver's name is saved
  var f = framMain.header.document.reportHeaderForm;
  if (f) if (f.AP_WEB_FULLNAME) fChangeApprover(f.AP_WEB_FULLNAME.value,f.AP_WEB_EMPID.value);
}


function fGenHeaderFrame(bCCEnabled){
	var headerFrame = top.framMain.header;
	var d = headerFrame.document;
 	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_EXP_HEADER');
	top.g_bHeaderFrameLoaded = false;
        d.open();
	d.writeln("	<HEAD><SCRIPT LANGUAGE=\"JavaScript\">");
	d.writeln("	  function populateEmployeeName(strFullName){");
	d.writeln("	  if (strFullName) top.fChangeApprover(strFullName,'');");
	d.writeln("	  }");
	d.writeln("	<\/SCRIPT></HEAD>");
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<html dir=\""+top.g_strDirection+"\">");
        top.fGenerateStyleCodes(headerFrame);
	if (top.g_bCCEnabled)
	  d.writeln("<body class=color3 onLoad = \"top.g_bHeaderFrameLoaded = true;\" onFocus=\"top.fCheckModal();\">");
	else
	  d.writeln("<body class=colorg5 onLoad = \"top.g_bHeaderFrameLoaded = true; top.fRecoverEmployee(top.fGetObjHeader().getObjEmployee().mGetstrEmployeeID());\" onFocus=\"top.fCheckModal();\">");	
	d.writeln("<div align=center>");
        d.writeln("<form name=reportHeaderForm>");
        d.writeln("<center>");
        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% align=center>");
	d.writeln("  <TR>");
        d.writeln("          <TD class=color2 ROWSPAN=2 valign=top width=1 align=left><IMG SRC='"+top.g_strImgDir+"APWCTTLS.gif' width=5 height=20></TD>");
        d.writeln("          <TD class=color6 Width=800 HEIGHT=1><IMG SRC='"+top.g_strImgDir+"APWPX6.gif' height=1 width=500></TD>");
        d.writeln("          <TD class=color2 ROWSPAN=2 valign=top width=1 align=right><IMG SRC='"+top.g_strImgDir+"APWCTTRS.gif' width=5 height=20></TD>");
        d.writeln("  </TR>");
        d.writeln("  <TR><TD  valign=top class=color2>"); 
        d.writeln("  <font class=containertitle>"+tempRegion.mobjGetRegionItem('AP_WEB_NEW_EXPREPORT').mstrGetPrompt()+"</font>");
        d.writeln("  </TD></TR>");
        d.writeln("  <TR><TD class=color6 COLSPAN=3 HEIGHT=1>"); 
        d.writeln("          <img src="+top.g_strImgDir+"APWPX6.gif></TD></TR>");
        d.writeln("  <TR><TD class=colorg5 COLSPAN=3 valign=top>"); 
        d.writeln("          <font class=helptext>&nbsp;<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>"+tempRegion.mobjGetRegionItem('AP_WEB_REQ_FLD').mstrGetPrompt()+"</font>");
        d.writeln("  </td></tr>");
        d.writeln("  <tr><td class=colorg5 colspan=3 height=100 valign=top>");

        d.writeln("          <table border=0 cellpadding=0 cellspacing=0 width=100%>");
        d.writeln("          <tr>");
	d.writeln("                  <td align=right nowrap>");
	if (top.objEnvironment.bEmployeeNameReq)
	d.writeln("                          <img src=\""+top.g_strImgDir+"APWREQUI.gif\">");
	d.writeln("		             <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_EMPLOYEE').mstrGetPrompt()+"&nbsp;</font></td>");
        d.writeln("                  <td nowrap>");

   /* ---------------------
          Employee Name
      --------------------- */
   	d.writeln(g_emp_string);
   	d.writeln("		</TD>");

   /*  -------------------------------
   	 Cost Center
    ------------------------------- */
        d.writeln("                  <td align=right nowrap>");
	if (top.objEnvironment.bCostCenterReq)
	d.writeln("                          <img src=\""+top.g_strImgDir+"APWREQUI.gif\">");
	d.writeln("		             <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_COSTCTR').mstrGetPrompt()+"&nbsp;</font></td>");
	d.writeln("                  <td nowrap><font class=promptblack><input type=text onFocus = \"javascript:top.fFocusOnHeaderFrame();\" size=10 name=txtCostCenter value=\"" + fGetObjHeader().getCostCenter() + "\"");
	d.writeln(" onChange=\"javascript:top.fChangeCostCenter(value);\"></font></td>");
	d.writeln("          </tr>");

    /* --------------------------------
    --- Reimbursement Currency
    -------------------------------- */

        d.writeln("          <tr>");
        d.writeln("                  <td align=right nowrap>");
	d.writeln("		              <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_REIMBCURR').mstrGetPrompt()+"&nbsp;</font></td>");


    
  if (top.objEnvironment.bMultiCurrency){
       d.writeln("                  <td nowrap><select name=popReimbursCurrency onChange=\"top.fChangeReimbursCurrency(this.options[this.selectedIndex].value);\" onFocus = \"javascript:top.fFocusOnHeaderFrame();\">"+top.fGenReimbCurrPopOptions(true,fGetObjHeader().getReimbursCurr())+"</select></td>");  
  }
  else{         
	 var iIndex = fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
         d.writeln("                  <td nowrap><FONT class=datablack>"+g_arrCurrency[iIndex].mGetName()+" ( "+fGetObjHeader().getReimbursCurr()+" )</FONT></td>");
  }    


    /*--------------------
    	Expense Template
      --------------------*/

        d.writeln("                  <td align=right nowrap>");
	d.writeln("		               <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_EXP_TEMP').mstrGetPrompt()+"&nbsp;</font></td>");
        d.write("                  <td nowrap><select name=popExpTemplate onChange = \"javascript:top.fChangeExpTemplate(this.options[this.selectedIndex].value);\" onFocus = \"javascript:top.fFocusOnHeaderFrame();\">");
	for (var i=0;i<top.g_arrExpenseTemplate.length;i++){
	   if (top.g_arrExpenseTemplate[i].id == fGetObjHeader().getObjExpTemplate().id)
	     d.write(" <option value= "+ top.g_arrExpenseTemplate[i].id+" SELECTED>"+ top.g_arrExpenseTemplate[i].name);
	   else
	     d.write(" <option value= "+ top.g_arrExpenseTemplate[i].id+">"+ top.g_arrExpenseTemplate[i].name);
	}
	 
	d.writeln("      </select>");
	d.writeln("</td>");
	d.writeln("          </tr>");

    /* --------------------------
    	Overriding Approver
       -------------------------- */

	if (!g_l_suppressOverrider){
	
        d.writeln("          <tr>");
        d.writeln("                  <td align=right nowrap>");
	if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_OVERRIDE_APPR_REQ')=='Y')
	d.writeln("                          <img src=\""+top.g_strImgDir+"APWREQUI.gif\">");
	d.writeln("		               <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_OVERRIDE_APPROVER').mstrGetPrompt()+"&nbsp;</font></td>");
	d.writeln("                  <td align=absmiddle nowrap colspan=3><font class=promptblack><input type=text name=\"AP_WEB_FULLNAME\" onBlur = \"javascript:var d = eval(top.framMain.header.document.reportHeaderForm);top.fChangeApprover(d.AP_WEB_FULLNAME.value, d.AP_WEB_EMPID.value);\" value=\""+fGetObjHeader().getApprover()+"\" size=30></font>");
	

	d.writeln(top.g_lov_string);
         
	d.writeln("		     </td>");
        d.writeln("          </tr>");


	  if (g_v_approverID +"" != "") {
	  d.writeln("<input type=hidden name=\"AP_WEB_EMPID\" value = g_v_approverID>");
	  fGetObjHeader().setApproverID (top.g_v_approverID);}
	  else d.writeln("<input type=hidden name=\"AP_WEB_EMPID\">");

	}


    /* ---------------
    	  Purpose
    ------------------*/
        d.writeln("          <tr>");
        d.writeln("                  <td align=right nowrap>");
	if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_PURPOSE_REQUIRED')=='Y')
	d.writeln("                          <img src=\""+top.g_strImgDir+"APWREQUI.gif\">");
	d.writeln("		               <font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_PURPOSE').mstrGetPrompt()+"&nbsp;</font></td>");

	d.writeln("                  <td colspan=3 nowrap><font class=promptblack><input type=text name=\"txtPurpose\" maxlength=240 onChange = \"top.fChangePurpose(this.value);\" onFocus = \"javascript:top.fFocusOnHeaderFrame();\" value=\""+fGetObjHeader().getPurpose()+"\" size=50></font></td>");
	
	d.writeln("          </tr>");
        d.writeln("          </table>");

        d.writeln("  </td></tr>");
        d.writeln("  <TR class=colorg5>");
	if (bCCEnabled){
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWCTBL.gif></TD>");
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=1></TD>");
        d.writeln("          <TD align=right valign=bottom><IMG SRC="+top.g_strImgDir+"APWCTBR.gif></TD>");
	}
	else {
	d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=5></TD>");
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=1></TD>");
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=1 width=5></TD>");
	}
        d.writeln("  </TR>");
	d.writeln("  <TR><TD colspan=3><br><br></TD></TR>");
        d.writeln("</table></center></form></div></body></html>");
        
        d.close();
 } // end of fGenHeaderFrame 






