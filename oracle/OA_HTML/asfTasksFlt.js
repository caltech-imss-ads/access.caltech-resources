/*********************************************************************
 * Javascript functions for To Do Create/Update
 *********************************************************************/
 <!-- $Header: asfTasksFlt.js 115.1 2000/10/19 22:58:02 pkm ship      $ -->

/**
 * handle click on checkBox for Private Flag
 */

function setPrivateFlag(secObjName, theForm)
{

  var checkBox = secObjName + ".CONDITION.VALUE3";

  var privateFlag = checkBox;

  if(theForm.elements[checkBox].checked)
  {
    theForm.elements[privateFlag].value = 'Y';
  }
  else
  {
    theForm.elements[privateFlag].value = 'N';
  }
}

