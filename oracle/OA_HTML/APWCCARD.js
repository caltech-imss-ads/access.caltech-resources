/* This file contains credit card related functions */
/* $Header: APWCCARD.js 115.25 2001/02/06 19:07:47 pkm ship      $ */
/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on Remove Selected Lines button
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fRemoveReceipts(bDetail,isCreditCard)
{  
   if (!Nav4) fIEOnChangeTrigger();
   var msg = g_objMessages.mstrGetMesg('AP_WEB_CCARD_REMOVE_RECEIPTS1');
   var ai = framMain.lines.fGetCheckedLines();
   if (ai.length > 0)
   {
      msg = msg.replace(/&VALUE/, ai.length);
      if (!confirm(msg)) return;
      if (isCreditCard)
          var aObj = objExpenseReport.cCardReceipts;
      else
          var aObj = objExpenseReport.receipts;

      var aiItemized;
      var iRecIndex;

      if (!bDetail) {//not called from detail window
      	for (i = 0; i<ai.length; i++)
      	{   if (aObj[ai[i]] != null) {
      	       aiItemized = fGetItemizedReceipts(aObj[ai[i]].itemizeId);
      	       if ((aObj[ai[i]].itemizeId != null) && (aObj[ai[i]].itemizeId != "") && (aiItemized.length > 1)) {
      		  if (confirm(g_objMessages.mstrGetMesg('AP_WEB_CCARD_REMOVE_RECEIPTS2'))) {
      			for (var j=0; j < aiItemized.length; j++)
      			   aObj[aiItemized[j]] = null;
      		  }
      	       }
      	       else {
		  aObj[ai[i]] = null;
      	       }
      	    }
      	} //end for
        framMain.lines.g_iCurrentLine = framMain.lines.fGetCount() - 1;
     }
     else { //called from detail window
      	 var nLine = top.g_winDetail.iLineNum; 
	 iRecIndex = top.objExpenseReport.getNthReceipt(nLine);
	 if (aObj[iRecIndex] != null) {
      	       if (aObj[iRecIndex].itemizeId != null) {
      		  aiItemized = fGetItemizedReceipts(aObj[iRecIndex].itemizeId);
      		  if (aiItemized.length > 1) {
      		     if (confirm(g_objMessages.mstrGetMesg('AP_WEB_CCARD_REMOVE_RECEIPTS2'))) {
      			for (var j=0; j < aiItemized.length; j++)
      			   aObj[aiItemized[j]] = null;
      		     }
      		  }
      	       }
      	       else {
		  aObj[iRecIndex] = null;
      	       } //end else
      	    if (nLine >1) 
		 framMain.lines.g_iCurrentLine = nLine - 1;
	    else framMain.lines.g_iCurrentLine = 1;

      	    framMain.lines.g_iCurrentReceipt = top.objExpenseReport.getNthReceipt(framMain.lines.g_iCurrentLine);
      	    }//end if
     } //end else

   if (isCreditCard)
      {   fDrawCCardLines(true,framMain.lines.g_iCurrentLine);
          framMain.framButtons.setTotalInfo(framMain.lines.fGetCount(), framMain.lines.fGetTotal());
      }   
      
   if (top.g_winDetail != null){
      if (!top.g_winDetail.closed){
	  iRecIndex = top.fGetObjExpReport().getNthReceipt(framMain.lines.g_iCurrentLine);
	  top.g_winDetail.iLineNum = framMain.lines.g_iCurrentLine;
       	  top.g_winDetail.iRecIndex = iRecIndex;
          top.fGenerateDetailBody(iRecIndex,framMain.lines.g_iCurrentLine);
	  top.fGenerateDetailButtons();
	  top.fUpdatePopShowReceipt();
	  top.g_winDetail.bRunPopDetail = true;
	  top.fPopulateReceiptDetail(top.g_winDetail.iRecIndex);
	  top.g_winDetail.focus();
	} //end if closed
   } //end if != null
 } //end if
}//end function fRemoveReceipts


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on Get Charge button
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fOpenCCardTrxnWindow() 
{
    var cardPrograms = g_objCCardData.getCardProgNames();
    var objAvailTrxns, objTrxns, ntrxns = 0;
    for (var i=0; i<cardPrograms.length; i++)
    {
         objTrxns = fGetAvailableTransactions(cardPrograms[i]);
         ntrxns += objTrxns.getNumberOfTransactions();
         if (cardPrograms[i] == g_selectedCardProg) 
                 objAvailTrxns = objTrxns;
    }
    if (fGetDisputedCharges(g_selectedCardProg))
        var nDispute = fGetDisputedCharges(g_selectedCardProg).length;
    else
        var nDispute = 0;
    if (ntrxns > 0 || nDispute > 0)
    {   g_winCCardTrxn = window.open("","","width=800,height=430,resizable=no,status=yes, menubar=no");
        if (top.g_winCCardTrxn.opener == null) top.g_winCCardTrxn.opener = self; 
        fDrawCCardTrxnWin(objAvailTrxns);
        top.g_winCCardTrxn.focus();
    }
    else
        alert(g_objMessages.mstrGetMesg('AP_WEB_CCARD_NO_TRANSACTIONS'));
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the Get Charge screen dynamically. Defined in the  
  top level of the Enter Receipt window.
Input: 
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawCCardTrxnWin(objAvailTrxns){
        var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT_HEADER');
        var d = top.g_winCCardTrxn.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<TITLE>" + objRegion.mobjGetRegionItem('AP_WEB_CCARD_IMPORT_TITLE').mstrGetPrompt()+"</TITLE>");
        d.writeln("<head><script>");
        d.writeln("function fGetSelectedTrxns() { return framTrx.fGetSelectedTrxns();}");
        d.writeln("function fDisputeTab() { return framHeader.fDisputeTab();}");
        d.writeln("<\/script></head>");
        d.writeln("<frameset cols=\"3,*,3\" border=0 framespacing=0 frameborder=no onUnload=\"opener.g_ICWindowOpen=false;\" onLoad=\"opener.g_ICWindowOpen=true;\">");
        d.writeln("    <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("		src=\"javascript:top.opener.top.fBlankPage('336699')\""); 
        d.writeln("     frameborder=no name=leftborder marginwidth=0 marginheight=0 scrolling=no>");
        d.writeln("    <frameset rows = \"25%,*,20%\" border=0 framespacing=0 frameborder=no>");
        d.writeln("       <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("	   	src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("         name=framHeader marginwidth=0 frameborder=no scrolling=no>");
        d.writeln("       <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("	   	src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("         frameborder=no name=framTrx  marginwidth=0 scrolling=auto>");
        d.writeln("       <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("		src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("         frameborder=no name=framButtons marginwidth=0 scrolling=no>");
        d.writeln("     </frameset>");
        d.writeln("     <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("		src=\"javascript:top.opener.top.fBlankPage('336699')\"");
        d.writeln("      frameborder=no name=rightborder marginwidth=0 marginheight=0 scrolling=no>");
        d.writeln("</frameset>");
        d.writeln("</html>");
        d.close();
 
        if (fGetPaymentScenario() != g_cIndividual)
           fDrawCCardTrxnTab(false);
        else
           fDrawCCardTrxnHdr();
        fDrawCCardTrxns(objAvailTrxns);
        fDrawCCardTrxnBtn(false);
 	if (isWindows95 && isIE5){
	  fGenBlueBorder('g_winCCardTrxn','leftborder');
	  fGenBlueBorder('g_winCCardTrxn','rightborder');
	}
}



/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the FramHeader frame of the Credit Card Cransactions window 
  dynamically. Defined in the top level of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawCCardTrxnHdr(){
var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT_HEADER');
var cardPrograms = g_objCCardData.getCardProgNames();
var d = top.g_winCCardTrxn.framHeader.document;
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\"><HEAD><script>");
        d.writeln("function fSetCardProgram(cardProgram)");
	d.writeln("{  var sel = document.forms[0].popCardProg;");
	d.writeln("   for (i=0; i<sel.length; i++)");
	d.writeln("        if (sel.options[i].text == cardProgram)");
	d.writeln("        {   sel.options[i].selected = true;");
	d.writeln("            return;");
	d.writeln("        }");
	d.writeln("}");
        d.writeln("<\/script>");
d.writeln("<link rel=stylesheet type=text/css href=" + g_strCssDir + "apwscabo.css>");
d.writeln("</head>");
d.writeln("<body class=color3>");
d.writeln("<form name=curr>");
d.writeln("<!--beginning of no-tab container code-->");
d.writeln("<CENTER>");
d.writeln("<br>");
d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>");
d.writeln("<TR class=colorg5><TD ROWSPAN=2 valign=top align=left width=1><IMG SRC=" + g_strImgDir + "APWCTTL.gif></TD>");
d.writeln("		<TD class=color6 Width=1000 HEIGHT=1><IMG SRC=" + g_strImgDir + "APWDBPXW.gif></TD>");
d.writeln("		<TD ROWSPAN=2 valign=top width=1><IMG SRC=" + g_strImgDir + "APWCTTR.gif></TD></TR>");
d.writeln("<TR><TD height=100 class=colorg5>");
d.writeln("             <table>");
d.writeln("             <tr><td colspan=2 height=20>" + g_objMessages.mstrGetMesg('AP_WEB_CCARD_TAB_MESSAGE3') + "</td></tr>");
d.writeln("             <tr><td valign=top>" + objRegion.mobjGetRegionItem('AP_WEB_CCARD_VIEW_TRXNS_FOR').mstrGetPrompt() + "</td>");
d.writeln("                 <td><select name=popCardProg onChange=\"javascript:top.opener.fChangeCCardProgram(options[selectedIndex].text);\">");
d.writeln("                     <option selected>" + g_selectedCardProg);
   for (var i=0; i<cardPrograms.length; i++)
        if (cardPrograms[i] != g_selectedCardProg)
            d.writeln("                     <option>" + cardPrograms[i]);
d.writeln("                     </select></td></tr>");
d.writeln("             </table></TD></TR>");
d.writeln("</TABLE></CENTER>");
d.writeln("</form>");
d.writeln("</BODY>");
d.writeln("</HTML>");
        d.close();
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the FramHeader frame of the Credit Card Cransactions window 
  dynamically. Defined in the top level of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawCCardTrxnTab(disputeTab)
{
 var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT_HEADER');
 var cardPrograms = g_objCCardData.getCardProgNames();
 var d = top.g_winCCardTrxn.framHeader.document;
 d.open();
 d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
 d.writeln("<html dir=\""+top.g_strDirection+"\"><head><script>");
        d.writeln("function fSetCardProgram(cardProgram)");
	d.writeln("{  var sel = document.forms[0].popCardProg;");
	d.writeln("   for (i=0; i<sel.length; i++)");
	d.writeln("        if (sel.options[i].text == cardProgram)");
	d.writeln("        {   sel.options[i].selected = true;");
	d.writeln("            return;");
	d.writeln("        }");
	d.writeln("}");
 d.writeln("function fDisputeTab() { return " + disputeTab + ";}");
 d.writeln("<\/script>");
 d.writeln("<LINK REL=stylesheet TYPE=text/css href=" + g_strCssDir + "apwscabo.css></head>");
 d.writeln("<body class=color3 link=blue vlink=blue alink=#ff0000><br>");
 d.writeln("<Table width=100% cellpadding=0 cellspacing=0 border=0>");
 d.writeln("  <tr class=color3>");
 d.writeln("    <td rowspan=3><img src=" + g_strImgDir + "APWPX3.gif height=1 width=1></td>");
 d.writeln("    <td rowspan=3 nowrap><font class=containertitle>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_IMPORT_TITLE').mstrGetPrompt() + "</font></td>");
 d.writeln("      <!--Width of this cell is set to 10000 to force cells with contents to hug the left and right margins.  This should only be done with cells which cannot possibly contain enough content to cause the page to scroll.-->");
 d.writeln("    <td rowspan=3 width=10000></td>");
 d.writeln("      <!-- One of these lines required for each tab present.  Add or remove for different numbers of tabs-->");
 d.writeln("    <td colspan=3><img src=" + g_strImgDir + "APWPX3.gif height=6></td>");
 d.writeln("    <td colspan=3><img src=" + g_strImgDir + "APWPX3.gif height=6></td>");
 d.writeln("      <!-- spacer-->");
 d.writeln("    <td rowspan=3><img src=" + g_strImgDir + "APWPX3.gif></td></tr>");
 d.writeln("  <tr class=color3>");
 if (!disputeTab)
 {
   d.writeln("       <!-- First tab, gray borders-->");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWGTL.gif></td>");
   d.writeln("    <td class=color6 height=1><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWGTR.gif></td>");
   d.writeln("       <!-- Second tab, blue borders-->");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWIPTL.gif></td>");
   d.writeln("    <td class=color4 height=1><img src=" + g_strImgDir + "APWPX4.gif></td>");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWIPTR.gif></td></tr>");
   d.writeln("  <tr>");
   d.writeln("       <!-- Tab titles listed here, one row for each tab.  Leave the font tags inside the anchor tags-->");
   d.writeln("    <td class=colorg5 nowrap><FONT CLASS=graytabtext>"+ objRegion.mobjGetRegionItem('AP_WEB_CCARD_CURRENT_CHARGES').mstrGetPrompt() + "</font></a></td>");
   d.writeln("    <td class=color4 nowrap><a href=\"javascript:top.opener.fSwitchImportWindow(true);\"><FONT CLASS=othertabtext>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_DISPUTED_CHARGES').mstrGetPrompt() + "</font></td></tr>");
   d.writeln("  <tr>");
   d.writeln("    <td rowspan=3 class=colorg5 valign=top><img src=" + g_strImgDir + "APWCTTL.gif></td>");
   d.writeln("    <td colspan=2 class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("       <!-- Cells to define the bottom of each tab.  Gray for the gray tabs, white for the purple tabs-->");
   d.writeln("    <td colspan=3 class=colorg5><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td colspan=3 class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td rowspan=3 class=colorg5 valign=top width=3><img src=" + g_strImgDir + "APWCTTR.gif></td></tr>");
 }
 else
 {
   d.writeln("       <!-- First tab, blue borders-->");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWIPTL.gif></td>");
   d.writeln("    <td class=color4 height=1><img src=" + g_strImgDir + "APWPX4.gif></td>");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWIPTR.gif></td>");
   d.writeln("       <!-- Second tab, gray borders-->");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWGTL.gif></td>");
   d.writeln("    <td class=color6 height=1><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td rowspan=2><img src=" + g_strImgDir + "APWGTR.gif></td></tr>");
   d.writeln("  <tr>");
   d.writeln("       <!-- Tab titles listed here, one row for each tab.  Leave the font tags inside the anchor tags-->");
   d.writeln("    <td class=color4 nowrap><a href=\"javascript:top.opener.fSwitchImportWindow(false);\"><FONT CLASS=othertabtext>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_CURRENT_CHARGES').mstrGetPrompt() + "</font></a></td>");
   d.writeln("    <td class=colorg5 nowrap><FONT CLASS=graytabtext>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_DISPUTED_CHARGES').mstrGetPrompt() + "</font></td></tr>");
   d.writeln("  <tr>");
   d.writeln("    <td rowspan=3 class=colorg5 valign=top><img src=" + g_strImgDir + "APWCTTL.gif></td>");
   d.writeln("    <td colspan=2 class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("       <!-- Cells to define the bottom of each tab.  Gray for the gray tabs, white for the purple tabs-->");
   d.writeln("    <td colspan=3 class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td colspan=3 class=colorg5><img src=" + g_strImgDir + "APWPX6.gif></td>");
   d.writeln("    <td rowspan=3 class=colorg5 valign=top width=3><img src=" + g_strImgDir + "APWCTTR.gif></td></tr>");
 }
 d.writeln("       <!-- This row contains the help text   The Colspan for this row must be [number of tabs] * 3 + 2-->");
 d.writeln("  <tr class=colorg5>");
 d.writeln("    <td colspan=8 valign=top>");
 if (disputeTab)
     d.writeln("      <FONT CLASS=helptext>" + g_objMessages.mstrGetMesg('AP_WEB_CCARD_TAB_MESSAGE2') + "</td></tr>");
 else
     d.writeln("      <FONT CLASS=helptext>" + g_objMessages.mstrGetMesg('AP_WEB_CCARD_TAB_MESSAGE1') + "</td></tr>");
 d.writeln("  <tr class=colorg5>");
 d.writeln("    <td colspan=8 height=200 valign=top> ");
 d.writeln("      <form>");
 d.writeln("      <table>");
 d.writeln("        <tr class=colorg5>");
 d.writeln("          <td valign=top>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_VIEW_TRXNS_FOR').mstrGetPrompt() + "</td>");
 d.writeln("          <td><select name=popCardProg onChange=\"javascript:top.opener.fChangeCCardProgram(options[selectedIndex].text);\">");
 // the number of listed items should less than 85 otherwise the frame won't be generated for Mac
 d.writeln("                     <option selected>" + g_selectedCardProg);
   for (var i=0; i<cardPrograms.length; i++)
        if (cardPrograms[i] != g_selectedCardProg)
            d.writeln("                     <option>" + cardPrograms[i]);
 
 d.writeln("                     </select></td></tr>");
 d.writeln("      </table></form></td></tr>");
 d.writeln("</table>");
 d.writeln("</body>");
 d.writeln("</html>");
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the frame to display credit card transactions dynamically. 
  Defined in the top level of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawCCardTrxns(objAvailTrxns){
        var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT');
        var currency = objExpenseReport.header.getReimbursCurr();
        var cat = new Array(g_cPersonal, g_cBusiness);
        var d = top.g_winCCardTrxn.framTrx.document;
        d.open();
 	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head><script>");
        d.writeln("function fGetSelectedTrxns()");
	d.writeln("{  var aEls = document.data.elements;");
	d.writeln("   var as = new Array();");
	d.writeln("   for (i=0; i<aEls.length; i++)");
	d.writeln("       if (aEls[i].type == \"checkbox\" && aEls[i].checked)");
	d.writeln("           as[as.length] =  aEls[i].value;");
	d.writeln("   return as;");
	d.writeln("}");
       
        d.writeln(" var objTrxns = top.opener.g_objCCardData.getTrxns(top.opener.g_selectedCardProg);");
        d.writeln("function fSelectAllTrxns(bValue)");
        d.writeln("{  var aEls = document.data.elements;");
        d.writeln("   for (i=0; i<aEls.length; i++) ");
        d.writeln("       if (aEls[i].type == \"checkbox\") ");
        d.writeln("           aEls[i].checked = bValue; ");
	d.writeln("}");
        if (fGetPaymentScenario() != g_cIndividual)
        {
	   d.writeln("function fChangeCategory(sId, sCategory)");
	   d.writeln("{  var t = objTrxns.getTrx(sId);");
	   d.writeln("   if (t != null) t.setCategory(sCategory); ");
	   d.writeln("}");
        }
        d.writeln("<\/script>");

	d.writeln("<LINK REL=stylesheet TYPE=text/css href=" + g_strCssDir + "apwscabo.css></head>");
        d.writeln("<BODY class=colorg5>");
        d.writeln("<div align=center>");
        d.writeln("<form name=data>");
        d.writeln("<center>");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% align=center>");

        // generate table header
        d.writeln("<tr>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("<td></td>");
        d.writeln("<td class=color1 width=100%><img src=" + g_strImgDir + "APWPX1.gif height=1 width=1></td>");
        d.writeln("<td></td>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td align=left><img src=" + g_strImgDir + "APWRTCTL.gif></td>");
        d.writeln("<td class=tableheader width=800 nowrap><img src=" + g_strImgDir + "APWPX4.gif width=700 height=1></td>");
        d.writeln("<td align=right><img src=" + g_strImgDir + "APWRTCTR.gif></td>");

        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td class=color1><img src=" + g_strImgDir + "APWPX1.gif></td>");
        d.writeln("<td class=tablesurround colspan=3>");

        d.writeln("<table cellpadding=1 cellspacing=0 border=0 width=100%>");
        d.writeln("<tr>");
        d.writeln("<td class=tableheader>" + objRegion.mobjGetRegionItem('AP_WEB_SELECT').mstrGetPrompt()+"</td>");
        if (fGetPaymentScenario() != g_cIndividual) d.writeln("<td class=tableheader>" + objRegion.mobjGetRegionItem('AP_WEB_CCARD_CATEGORY').mstrGetPrompt() + "</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_DATE').mstrGetPrompt() + "</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt() + "</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetPrompt() + "</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_LOCATION').mstrGetPrompt() + "</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_BILLED_AMT').mstrGetPrompt() + "(" + currency + ")</td>");
        d.writeln("</tr>");

        objAvailTrxns.reset();
        var t = objAvailTrxns.nextTrxn();        // generate lines
        var i = 0;
        while (t != null)
      { i++;
        d.writeln("<tr>");
        d.writeln("<td class=tablerow align=right>"+i+"<input type=checkbox name=cbx"+i+" class=tablerow value=" + t.getId() + "></td>");
        if (fGetPaymentScenario() != g_cIndividual) 
        {
 	    d.writeln("<td class=tablerow><select  name=popCategory"+i+" onChange=\"fChangeCategory(" + t.getId() + ", options[selectedIndex].text);\"><option selected>" + t.getCategory());
            for (var j=0; j<cat.length; j++)
              if (cat[j] != t.getCategory())
                 d.writeln("<option>" + cat[j]);
            d.writeln("</select></td>");
        }
        d.writeln("<td class=tablerow>" + t.getDate() + "</td>");
        d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getTrxAmt(),currency) + " " + t.getTrxCurr() + "</td>");
        d.writeln("<td class=tablerow>" + t.getMerchant() + "</td>");
        d.writeln("<td class=tablerow>" + t.getCity() + "</td>");
        d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getAmt(),currency) + "</td>");
        d.writeln("</tr>");
        d.writeln("<TR></TR>");
        t = objAvailTrxns.nextTrxn();
       }

        d.writeln("</table></td>");

        d.writeln("<td class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("<td><img src=" + g_strImgDir + "APWRTCBL.gif></td>");
        d.writeln("<td class=tableheader width=1000><img src=" + g_strImgDir + "APWPX3.gif></td>");
        d.writeln("<td><img src=" + g_strImgDir + "APWRTCBR.gif></td>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td></td>");
        d.writeln("<td class=color6 width=1000><img src=" + g_strImgDir + "APWPX6.gif></td>");
        d.writeln("<td></td>");
        d.writeln("</tr>");
        d.writeln("</table>");
        d.writeln("</center>");
        d.writeln("</form>");
        d.writeln("</div>");
        d.writeln("</BODY></HTML>");
        d.close();
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the frame to display disputed charges dynamically. 
  Defined in the top level of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawDisputedCharges(aObjDispute){
        var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT');
        var currency = objExpenseReport.header.getReimbursCurr();
        var d = top.g_winCCardTrxn.framTrx.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head><script>");

        d.writeln("function fGetSelectedTrxns()");
	d.writeln("{  var aEls = document.data.elements;");
	d.writeln("   var ai = new Array();");
	d.writeln("   for (i=0; i<aEls.length; i++)");
	d.writeln("       if (aEls[i].type == \"checkbox\" && aEls[i].checked)");
	d.writeln("           ai[ai.length] =  aEls[i].value;");
	d.writeln("   return ai;");
	d.writeln("}");
        d.writeln("<\/script>");

	d.writeln("<LINK REL=stylesheet TYPE=text/css href=" + g_strCssDir + "apwscabo.css></head>");
        d.writeln("<BODY class=colorg5>");
        d.writeln("<div align=center>");
        d.writeln("<form name=data>");
        d.writeln("<center>");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% align=center>");

        // generate table header
        d.writeln("<tr>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("<td></td>");
        d.writeln("<td class=color1 width=100%><img src=" + g_strImgDir + "APWPX1.gif height=1 width=1></td>");
        d.writeln("<td></td>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td align=left><img src=" + g_strImgDir + "APWRTCTL.gif></td>");
        d.writeln("<td class=tableheader width=800 nowrap><img src=" + g_strImgDir + "APWPX4.gif width=700 height=1></td>");
        d.writeln("<td align=right><img src=" + g_strImgDir + "APWRTCTR.gif></td>");

        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td class=color1><img src=" + g_strImgDir + "APWPX1.gif></td>");
        d.writeln("<td class=tablesurround colspan=3>");

        d.writeln("<table cellpadding=1 cellspacing=0 border=0 width=100%>");
        d.writeln("<tr>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_SELECT').mstrGetPrompt()+"</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_DATE').mstrGetPrompt()+"</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt()+"</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetPrompt()+"</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_LOCATION').mstrGetPrompt()+"</td>");
        d.writeln("<td class=tableheader>"+objRegion.mobjGetRegionItem('AP_WEB_CCARD_BILLED_AMT').mstrGetPrompt()+"(" + currency + ")</td>");
        d.writeln("</tr>");

        var currency = objExpenseReport.header.getReimbursCurr();
        var t, t1; 
      for (var i=0; i<aObjDispute.length; i++)
      { 
        d.writeln("<tr>");
        if (aObjDispute[i].creditTrxn == null) 
        {  t = aObjDispute[i].paymentTrxn;
           d.writeln("<td class=tablerow align=right>"+(i+1)+"<input type=checkbox name=select"+i+" class=tablerow value="+i+"></td>");
           d.writeln("<td class=tablerow>" + t.getDate() + "</td>");
           d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getTrxAmt(),currency) + " " + t.getTrxCurr() + "</td>");
      	   d.writeln("<td class=tablerow>" + t.getMerchant() + "</td>");
      	   d.writeln("<td class=tablerow>" + t.getCity() + "</td>");
      	   d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getAmt(),currency) + "</td>");
        }
        else
        {  t = aObjDispute[i].paymentTrxn;
           t1 = aObjDispute[i].creditTrxn;
           d.writeln("<td class=tablerow align=right>"+(i+1)+"<input type=checkbox name=cbx"+i+" class=tablerow value="+i+"></td>");
           d.writeln("<td class=tablerow>" + t.getDate() + "<br>" + t1.getDate()+ "</td>");
           d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getTrxAmt(),currency) + " " + t.getTrxCurr() + "<br>" + fMoneyFormat(t1.getTrxAmt(),currency) + " " + t1.getTrxCurr() + "</td>");
      	   d.writeln("<td class=tablerow>" + t.getMerchant() + "<br>" + t1.getMerchant() + "</td>");
      	   d.writeln("<td class=tablerow>" + t.getCity() + "<br>" + t1.getCity() + "</td>");
      	   d.writeln("<td class=tablerowr>" + fMoneyFormat(t.getAmt(),currency) + "<br>" + fMoneyFormat(t1.getAmt(),currency) + "</td>");
        }
        d.writeln("</tr>");
        d.writeln("<TR></TR>");
       }

        d.writeln("</table></td>");

        d.writeln("<td class=color6><img src=" + g_strImgDir + "APWPX6.gif></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("<td><img src=" + g_strImgDir + "APWRTCBL.gif></td>");
        d.writeln("<td class=tableheader width=1000><img src=" + g_strImgDir + "APWPX3.gif></td>");
        d.writeln("<td><img src=" + g_strImgDir + "APWRTCBR.gif></td>");
        d.writeln("<td rowspan=2></td>");
        d.writeln("</tr>");
        d.writeln("<tr>");
        d.writeln("<td></td>");
        d.writeln("<td class=color6 width=1000><img src=" + g_strImgDir + "APWPX6.gif></td>");
        d.writeln("<td></td>");
        d.writeln("</tr>");
        d.writeln("</table>");
        d.writeln("</center>");
        d.writeln("</form>");
        d.writeln("</div>");
        d.writeln("</BODY></HTML>");
        d.close();
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS code that will draw the framButtons frame of the Credit Card Cransactions window 
  dynamically. Defined in the top level of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
  None
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDrawCCardTrxnBtn(bDispute){
var objRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CCARD_IMPORT_BUTTONS');
var d = top.g_winCCardTrxn.framButtons.document;
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\"><HEAD>");
d.writeln("<link rel=stylesheet type=text/css href=" + g_strCssDir + "apwscabo.css>");
d.writeln("<BODY class=color3 onResize = \"history.go(0)\" >");
d.writeln("<TABLE width=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>");
d.writeln(" <TR class=colorg5>");
d.writeln("  <TD align=left valign=bottom><IMG SRC=" + g_strImgDir + "APWCTBL.gif height=5 width=5></TD>");
d.writeln("  <TD>");
d.writeln("     <table width=100% cellpadding=0 cellspacing=0 border=0>");
d.writeln("      <tr>");
d.writeln("       <td rowspan=3 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=5 width=5></td>");
d.writeln("       <td colspan=2 class=colorg5 height=5 width=1000><img src=" + g_strImgDir + "APWPXG5.gif height=2></td>");
d.writeln("       <td rowspan=3 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=5 width=5></td>");
d.writeln("      </tr>");
d.writeln("      <tr>");
d.writeln("       <td class=colorg5 nowrap>");

d.writeln("          <table><tr><td>");
d.writeln("          <SCRIPT LANGUAGE=\"JAVASCRIPT\">");
if (bDispute) {
   d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_CLEAR').mstrGetPrompt()+"\",");
   d.writeln("                    \"APWBRNDL.gif\",");
   d.writeln("                    \"APWBRNDR.gif\",");
   d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_CLEAR').mstrGetPrompt()+"\",");
   d.writeln(" 		   \"javascript:top.opener.fClearDisputedCharges();\",");
   d.writeln("                    parent.v_lang,");
   d.writeln("                    false,");
   d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
}
else {
   d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_SELECT_ALL').mstrGetPrompt()+"\",");
   d.writeln("                    \"APWBRNDL.gif\",");
   d.writeln("                    \"APWBRNDR.gif\",");
   d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_SELECT_ALL').mstrGetPrompt()+"\",");
   d.writeln(" 		   \"javascript:parent.framTrx.fSelectAllTrxns(true);\",");
   d.writeln("                    parent.v_lang,");
   d.writeln("                    false,");
   d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
   d.writeln("             <td><SCRIPT LANGUAGE=\"JAVASCRIPT\">");
   d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_DESELECT_ALL').mstrGetPrompt()+"\",");
   d.writeln("                    \"APWBRNDL.gif\",");
   d.writeln("                    \"APWBRNDR.gif\",");
   d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_DESELECT_ALL').mstrGetPrompt()+"\",");
   d.writeln(" 		   \"javascript:parent.framTrx.fSelectAllTrxns(false);\",");
   d.writeln("                    parent.v_lang,");
   d.writeln("                    false,");
   d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");

   if (fGetPaymentScenario() != g_cIndividual) // draw the buttons
   {
      d.writeln("             <td>&nbsp</td>");
      d.writeln("             <td><SCRIPT LANGUAGE=\"JAVASCRIPT\">");
      d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_DISPUTE').mstrGetPrompt()+"\",");
      d.writeln("                    \"APWBRNDL.gif\",");
      d.writeln("                    \"APWBRNDR.gif\",");
      d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_DISPUTE').mstrGetPrompt()+"\",");
      d.writeln(" 		   \"javascript:top.opener.fDisputeCharges();\",");
      d.writeln("                    parent.v_lang,");
      d.writeln("                    false,");
      d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
      d.writeln("             <td><SCRIPT LANGUAGE=\"JAVASCRIPT\">");
      d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_IMPORT').mstrGetPrompt()+"\",");
      d.writeln("                    \"APWBRNDL.gif\",");
      d.writeln("                    \"APWBRNDR.gif\",");
      d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_CCARD_IMPORT').mstrGetPrompt()+"\",");
      d.writeln(" 		   \"javascript:top.opener.fImportCharges();\",");
      d.writeln("                    parent.v_lang,");
      d.writeln("                    false,");
      d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
  }
}
d.writeln("                </tr></table>");

d.writeln("       </td>");
d.writeln("       <td class=colorg5 align=right nowrap></td>");
d.writeln("      </tr>");
d.writeln("      <tr>");
d.writeln("       <td colspan=2 class=colorg5><img src=" + g_strImgDir + "APWPXG5.gif height=1></td>");
d.writeln("      </tr></TABLE></TD>");
d.writeln("  <TD align=right valign=bottom><IMG SRC=" + g_strImgDir + "APWCTBR.gif height=5 width=5></TD></TR>");
d.writeln("</TABLE>");
d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
d.writeln("    <TR><td height=5></td></TR>");
d.writeln("    <TR><TD align=right>");
d.writeln("          <table><tr><td><SCRIPT LANGUAGE=\"JAVASCRIPT\">");

if (fGetPaymentScenario() != g_cIndividual) // draw the buttons
{
      d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_CLOSE').mstrGetPrompt()+"\",");
      d.writeln("                    \"APWBRNDL.gif\",");
      d.writeln("                    \"APWBRNDR.gif\",");
      d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_CLOSE').mstrGetPrompt()+"\",");
      d.writeln(" 		   \"javascript:top.close();\",");
      d.writeln("                    parent.v_lang,");
      d.writeln("                    false,");
      d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
}
else
{
      d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
      d.writeln("                    \"APWBRNDL.gif\",");
      d.writeln("                    \"APWBRNDR.gif\",");
      d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_OK').mstrGetPrompt()+"\",");
      d.writeln(" 		   \"javascript:top.opener.fImportCharges();top.close();\",");
      d.writeln("                    parent.v_lang,");
      d.writeln("                    false,");
      d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
      d.writeln("             <td><SCRIPT LANGUAGE=\"JAVASCRIPT\">");
      d.writeln("             document.write(top.opener.fDynamicButton(\""+objRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"\",");
      d.writeln("                    \"APWBRNDL.gif\",");
      d.writeln("                    \"APWBRNDR.gif\",");
      d.writeln("                    \""+objRegion.mobjGetRegionItem('AP_WEB_CANCEL').mstrGetPrompt()+"\",");
      d.writeln(" 		   \"javascript:top.close();\",");
      d.writeln("                    parent.v_lang,");
      d.writeln("                    false,");
      d.writeln("                    false,parent.framButtons));<\/SCRIPT></td>");
}
d.writeln("</tr></table></td></tr></table>");
d.writeln("</BODY>");
d.writeln("</HTML>");
        d.close();
}



/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on OK button in the 
  Credit Card Transactions window. Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fImportCharges()
{  
   var aId = g_winCCardTrxn.fGetSelectedTrxns();
   if (aId.length == 0) return;

   var objTrxns = g_objCCardData.getTrxns(g_selectedCardProg);

   if (objExpenseReport.getCreditCardProg() != g_selectedCardProg && 
       objExpenseReport.getTotalReceipts("CC") > 0) {
       if (confirm(g_objMessages.mstrGetMesg('AP_WEB_CCARD_IMPORT_MESSAGE1'))) 
           objExpenseReport.deleteReceipts("CC");
       else
           return;
   }

   var count = 0;
   var templ = top.objExpenseReport.header.getObjExpTemplate();
   for (i=0; i<aId.length; i++)
   {  
      t = objTrxns.getTrx(aId[i]);
      if (t.getCategory() != g_cPersonal)
      { 
	var et_id = templ.getExpTypeID(t.getFolioType());
	var objE = templ.getExpTypeObj(et_id);
	objExpenseReport.addCCardReceipt(t.getDate(), 1, t.getTrxAmt(),
	t.getTrxAmt(), false, t.getTrxCurr(), t.getAmt()/t.getTrxAmt(),
	objE,"",
	aId[i], t.getMerchant(), null, "", t.getCategory(), "", "");
        count++;
      }
   }
   if (count == 0) return; // no business trxns
   objExpenseReport.setCreditCardProg(g_selectedCardProg);

   fDrawCCardLines(); //redraw the Credit Card Tab
   top.framMain.framButtons.setTotalInfo(top.framMain.lines.fGetCount(), fMoneyFormat(top.framMain.lines.fGetTotal(), objExpenseReport.header.getReimbursCurr()));
   if (fGetPaymentScenario() != g_cIndividual) 
   {      objTrxns = fGetAvailableTransactions(g_selectedCardProg);
          fDrawCCardTrxns(objTrxns);
          g_winCCardTrxn.focus();
   }
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on Dispute button in the 
  Credit Card Transactions window. Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fDisputeCharges()
{  
   var objTrxns = g_objCCardData.getTrxns(g_selectedCardProg);
   aId = g_winCCardTrxn.fGetSelectedTrxns();
   for (i=0; i<aId.length; i++)
      objTrxns.getTrx(aId[i]).setCategory(g_cDispute);

   if (aId.length > 0) 
   {
      objTrxns.fFlagDisputedCreditCharges();
      objTrxns = fGetAvailableTransactions(g_selectedCardProg);
      fDrawCCardTrxns(objTrxns);
   }
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on Dispute button in the 
  Credit Card Transactions window. Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fClearDisputedCharges()
{  
   var objTrxns = fGetDisputedCharges(g_selectedCardProg);
   ai = g_winCCardTrxn.fGetSelectedTrxns();
   for (i=0; i<ai.length; i++)
   {  if ( objTrxns[ai[i]].creditTrxn == null)
          objTrxns[ai[i]].paymentTrxn.setCategory(g_cBusiness);
      else {
          objTrxns[ai[i]].paymentTrxn.setCategory(g_cClear);
          objTrxns[ai[i]].creditTrxn.setCategory(g_cClear);
      }
   }
   if (ai.length > 0) 
   {  objTrxns = g_objCCardData.getTrxns(g_selectedCardProg);
      objTrxns.fFlagDisputedCreditCharges(); 
      objTrxns = fGetDisputedCharges(g_selectedCardProg);
      fDrawDisputedCharges(objTrxns);
   }
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users click on the tab in the 
  Credit Card Transactions window. Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/

function fSwitchImportWindow(bDispute)
{
   if (bDispute) 
   {
      var objTrxns = fGetDisputedCharges(g_selectedCardProg);
      if (objTrxns.length > 0) 
      {
        fDrawCCardTrxnTab(true);
        fDrawDisputedCharges(objTrxns);
        fDrawCCardTrxnBtn(bDispute);
      }
      else {
        alert(g_objMessages.mstrGetMesg('AP_WEB_CCARD_NO_DISPUTED'));
        g_winCCardTrxn.focus();
      }
   }
   else
   {
      var objTrxns = fGetAvailableTransactions(g_selectedCardProg);
      fDrawCCardTrxnTab(false);
      fDrawCCardTrxns(objTrxns);
      fDrawCCardTrxnBtn(false);
   }
}


/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that returns the indexes of all the itemized receipts for an itemizeId. 
Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fGetItemizedReceipts(itemizeId)
{  var outArr = new Array();
   var aObj = objExpenseReport.cCardReceipts;
   for (var i=0; i<aObj.length; i++)
        if (aObj[i] != null && aObj[i].itemizeId == itemizeId)
              outArr[outArr.length] = i;
   return outArr;
}




/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that called when users change card program in the 
  Credit Card Transactions window. Defined in the top frame of the Enter Receipt window.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fChangeCCardProgram(cardProgName)
{  
   if ((fGetPaymentScenario() != g_cIndividual) && g_winCCardTrxn.fDisputeTab()) {
      var objTrxns = fGetDisputedCharges(cardProgName);
      var nTrxns = objTrxns.length;
   }
   else {
      var objTrxns = fGetAvailableTransactions(cardProgName);
      var nTrxns = objTrxns.getNumberOfTransactions();
   }
   if (nTrxns > 0) 
   {
       g_selectedCardProg = cardProgName;
       if ((fGetPaymentScenario() != g_cIndividual) && g_winCCardTrxn.fDisputeTab())
           fDrawDisputedCharges(objTrxns);
       else
           fDrawCCardTrxns(objTrxns);
       g_winCCardTrxn.focus();
   }
   else {
      g_winCCardTrxn.framHeader.fSetCardProgram(g_selectedCardProg);
      alert(g_objMessages.mstrGetMesg('AP_WEB_CCARD_NO_TRANSACTIONS'));
      g_winCCardTrxn.focus();
   }
}





/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that returns the payment scenario.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fGetPaymentScenario() { return g_objProfileOptions.mvGetProfileOption('SSE_CC_PAYMENT_DUE_FROM');}



/*
Written by: 
  Quan Le
Purpose: 
  To generate JS function that saves change in transaction category.
Input: 
  None
Output:
  None
Input Output:
Assumption:
  <script> tag was opened and will be closed outside of this.
Date:
  8/19/99
*/
function fSaveCategory(){
        var t = top.framSaveReport.document.formSaveCategory;
        t.p_savedCategories.value = g_objCCardData.getDataChanged();
        t.submit();
}



