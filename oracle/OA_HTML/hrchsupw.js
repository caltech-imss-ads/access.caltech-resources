//-- $Header: hrchsupw.js 115.2 2001/07/22 20:10:22 pkm ship        $ -->
//-- =========================================================================
//Change History
//Version  Date         Author      Bug #    Remarks
// -------  -----------  ----------- -------- ----------------------------
//115.2    22-JUL-2001  VBALA        1880357 Added check for p_no_of_reports
//                                           == 1 or > 1
//110.3    09-NOV-2000  VKOTHARI     1496186 Changed procedure validate data
//                                           to not to display warning if 
//                                           one or more direct reports' 
//                                           manager is unchanged.
//110.2    06-FEB-2000  VKOTHARI             fixed the array syntax in validate
//                                           data procedure.
//110.1    04-FEB-2000  VKOTHARI             Added logic for Termination
//110.0    02/02/2000   VKOTHARI             Created  for change supervisor
//                                              module.
//


//function to assign one supervisor for all direct reports
function assign_direct_reports(
  p_sup_name , 
  p_sup_id )
{
  var i = 0 ; 
 if (  document.supervisor_form.p_no_of_reports.value > 1 )
  {
    for ( i=0 ; i<document.supervisor_form.p_supervisor_name.length; i++ )
    {
      document.supervisor_form.p_supervisor_name[i].value = p_sup_name ;
      document.supervisor_form.p_supervisor_id[i].value = p_sup_id ;
    }
  }
  else
  {
    if ( document.supervisor_form.p_no_of_reports.value == 1 )
    {
       document.supervisor_form.p_supervisor_name.value = p_sup_name ;
       document.supervisor_form.p_supervisor_id.value =  p_sup_id ;
    }
  }
}

function delete_emp(p_row)
{

  if ( confirm(message_array[5]) == false  )
    return ; 
  
  document.supervisor_form.p_result_code.value = "DELETE" ; 
  document.supervisor_form.p_delete_row.value = p_row ; 
  document.supervisor_form.submit(); 
}

function add_emp()
{
  document.supervisor_form.p_result_code.value = "ADD" ;
  document.supervisor_form.submit() ; 
}
function validate_date(p_row_no)
{
  var verified_date ; 
  

  // check if there are only one direct report, in that case 
  // we can not use array notation 

  if (p_row_no < 100 )
  {
    if ( document.supervisor_form.p_no_of_reports.value ==  1 ) 
    {
      if ( is_blank(document.supervisor_form.p_effective_date.value) )
      {
        alert ( message_array[1]); 
        document.supervisor_form.p_effective_date.focus();
        return ; 
      }
      else
      { 
         verified_date =datecheck(
                     document.supervisor_form.p_effective_date.value,
                     NLSformat);
         if ( verified_date.indexOf('?') != -1 ) 
         {
            alert (message_array[2]);
            document.supervisor_form.p_effective_date.focus();
            return ; 
          }
      }
    }
    else  if ( document.supervisor_form.p_no_of_reports.value >  1 )  
    {
      if (is_blank(document.supervisor_form.p_effective_date[p_row_no].value) )
      {
        alert ( message_array[1]); 
        document.supervisor_form.p_effective_date[p_row_no].focus();
        return ; 
      }
      else 
      { 
        verified_date =datecheck(
                     document.supervisor_form.p_effective_date[p_row_no].value,
                     NLSformat);
        if ( verified_date.indexOf('?') != -1 ) 
        {
          alert (message_array[2]);
          document.supervisor_form.p_effective_date[p_row_no].focus();
          return ; 
        }
      }
    }
  }
  return ; 
}

function single_effective_date(pdate)
{
 var i ; 
 i = 1 ; 

 if ( document.supervisor_form.p_no_of_reports.value > 1 )
 {
   for ( i=0 ; i<document.supervisor_form.p_effective_date.length; i++ )
   {
     document.supervisor_form.p_effective_date[i].value =
     document.supervisor_form.p_single_effective_date.value ; 
   }
 }
 else  if ( document.supervisor_form.p_no_of_reports.value == 1 )
 {
    document.supervisor_form.p_effective_date.value =
    document.supervisor_form.p_single_effective_date.value ;
 }
 return ; 
}

function new_emp(
  p_emp_name , 
  p_emp_id, 
  row)
{
  var ln_emp_count ; 

  ln_emp_count = document.supervisor_form.p_emp_count.value ; 
  if ( ln_emp_count == 1 ) 
  {
    document.supervisor_form.p_emp_name.value = p_emp_name ; 
    document.supervisor_form.p_emp_id.value = p_emp_id ; 
  }
  else if ( ln_emp_count > 1 ) 
  {
    document.supervisor_form.p_emp_name[row-101].value = p_emp_name; 
    document.supervisor_form.p_emp_id[row-101].value = p_emp_id; 
  }
}

//function that is executed whenever a new supervisor 
//is selected from LOV 

function new_supervisor(
  p_sup_full_name, 
  p_sup_per_id, 
  p_sup_bus_grp_id,
  p_sup_ins_prof, 
  p_sup_ord_name)
{

  i = document.supervisor_form.p_current_row.value ; 

  if (i > 100) 
  {
    // that means we are selecting a new emp
    new_emp(
      document.supervisor_form.P_SUP_FULL_NAME.value,p_sup_per_id, 
      i); 
    return ; 
  }
  document.supervisor_form.P_SUP_PER_ID.value = p_sup_per_id ; 


  // if selected employee's supervisor is changed 
  if ( i == 0 ) 
  {
    document.supervisor_form.p_selected_person_sup_name.value = 
    document.supervisor_form.P_SUP_FULL_NAME.value ; 
    document.supervisor_form.p_selected_person_sup_id.value = p_sup_per_id ; 
  }


  // if single supervisor selected 
  if (i == 1 ) 
  {
    document.supervisor_form.p_single_supervisor_name.value =
    document.supervisor_form.P_SUP_FULL_NAME.value ; 
    document.supervisor_form.p_single_supervisor_id.value = p_sup_per_id ;
    assign_direct_reports ( p_sup_full_name , p_sup_per_id ) ; 
  }
  if ( i > 1 ) 
  {
    if ( document.supervisor_form.p_no_of_reports.value > 1 ) 
    {
      document.supervisor_form.p_supervisor_name[i-2].value = 
      document.supervisor_form.P_SUP_FULL_NAME.value ; 
      document.supervisor_form.p_supervisor_id[i-2].value = p_sup_per_id ; 
    }
    else 
    { 
      if ( document.supervisor_form.p_no_of_reports.value == 1 )
      {
        document.supervisor_form.p_supervisor_name.value =  
        document.supervisor_form.P_SUP_FULL_NAME.value ; 
        document.supervisor_form.p_supervisor_id.value = p_sup_per_id ; 
      }
    }
  }
  document.supervisor_form.P_SUP_FULL_NAME.value = "";
}

<!-- Modified ICX LOV code
function myLOV(
   c_row_no
  ,c_attribute_app_id
  ,c_attribute_code
  ,c_region_app_id
  ,c_region_code
  ,c_form_name
  ,c_frame_name
  ,c_where_clause
  ,c_js_where_clause) 
 {

    document.supervisor_form.p_current_row.value = c_row_no ; 
    lov_win = window.open("icx_util.LOV?c_attribute_app_id=" +
      c_attribute_app_id + "&c_attribute_code=" + c_attribute_code +
     "&c_region_app_id=" + c_region_app_id + "&c_region_code=" +
     c_region_code + "&c_form_name=" + c_form_name + "&c_frame_name=" +
     c_frame_name + "&c_where_clause=" + c_where_clause +
     "&c_js_where_clause=" + c_js_where_clause,"LOV",
     "resizable=yes,dependent=yes,menubar=yes,scrollbars=yes,width=780,height=300");
    
    lov_win.opener = self;
    lov_win.focus()
}

function single_supervisor()
{
  if ( document.supervisor_form.p_no_of_reports.value > 1 )
  {
    for ( i=0 ; i<document.supervisor_form.p_supervisor_name.length; i++ )
    {
      document.supervisor_form.p_supervisor_name[i].value = 
      document.supervisor_form.p_single_supervisor_name.value ; 
    }
  }
  else 
  {
    if ( document.supervisor_form.p_no_of_reports.value == 1 )
    {
       document.supervisor_form.p_supervisor_name.value = 
       document.supervisor_form.p_single_supervisor_name.value ; 
       document.supervisor_form.p_supervisor_id.value = 
                  document.supervisor_form.P_SUP_PER_ID.value; 
    }
  }
}



// function to go back to previous page
// Invoke by the back button
// Submits the form with p_result_code set to PREVIOUS
// Transitions to the previous activity
function back_button_onClick (msg, p_target) 
{

     if (confirm(msg)) 
     {
         top.clear_container_bottom_frame(); 
        document.supervisor_form.p_result_code.value = 'PREVIOUS';
                document.supervisor_form.target= p_target;
        document.supervisor_form.submit();
     }
}

// function to validate supervisor form 
// if validation succeeds , form will be submitted 
// if error, appropriate message will be displayed 

function validate_data()
{
   var i ; 
   var mesg ; 
   var empty_supervisor = "N" ; 
   var empty_emp = "N" ;
   var empty_date = "N" ; 
   var sup_unchanged_warning = "N" ; 
   var from_term ; 

   from_term = document.dummy_form.from_term.value ; 
   
   // check if the config option is N
   if ( document.dummy_form.blank_supervisor.value == "N" ) 
   {
     if (from_term =="N")
       if (is_blank(document.supervisor_form.p_selected_person_sup_name.value))
       empty_supervisor = "Y" ; 
   }   

   if ( document.supervisor_form.p_no_of_reports.value ==  1 )  
   {
     if (document.dummy_form.blank_supervisor.value == "N" &&
        is_blank(document.supervisor_form.p_supervisor_name.value ))
     {
        empty_supervisor= "Y" ; 
     }
     if ((document.supervisor_form.p_supervisor_name.value ==
          document.supervisor_form.p_selected_emp_name.value)) 
       sup_unchanged_warning = "Y" ; 
   }
   else if ( document.supervisor_form.p_no_of_reports.value >  1 ) 
   {
     for ( i = 0 ; i< document.supervisor_form.p_supervisor_name.length; i++)
     {
       if (document.dummy_form.blank_supervisor.value == "N" &&
         is_blank(document.supervisor_form.p_supervisor_name[i].value ))
       {
         empty_supervisor = "Y" ; 
       }
       if ((document.supervisor_form.p_supervisor_name[i].value ==
           document.supervisor_form.p_selected_emp_name.value))
         sup_unchanged_warning = "Y" ;    
     }
   }
    // now check for empty employee 
   if (from_term == "N")
   {
      if (document.supervisor_form.p_emp_count.value == 1 )
      {
         if ( is_blank(document.supervisor_form.p_emp_name.value))
           empty_emp = "Y" ; 
         if (is_blank(document.supervisor_form.p_emp_effective_date.value))
           empty_date = "Y" ; 
      }
      else if (document.supervisor_form.p_emp_count.value >1 )
      {
        for ( i = 0 ; i< document.supervisor_form.p_emp_name.length; i++)
        {
          if (is_blank(document.supervisor_form.p_emp_name[i].value)) 
            empty_emp = "Y" ; 

          if(is_blank(document.supervisor_form.p_emp_effective_date[i].value)) 
            empty_date = "Y" ; 
        }
      }
   }
   if ( empty_supervisor == "Y" || empty_emp=="Y" || empty_date=="Y")
   {
     mesg = message_array[6] ; 
     if ( empty_supervisor =="Y" ) 
       mesg = mesg + "\n"+message_array[3]; 
     if ( empty_emp=="Y")
       mesg = mesg + "\n"+message_array[8] ; 
     if ( empty_date =="Y") 
       mesg = mesg + "\n"+message_array[9] ;
     alert ( mesg ) ; 
     return false ; 
   }
/*
   else 
   {
     if ( sup_unchanged_warning=="Y" )
     {
       if (confirm(message_array[4]))
         return true ; 
       else 
         return false ; 
     }

   }
*/ 
   return true ; 

}

// this function is invoked when user clicks next button
// it validates and then submits form 

function next_button_onClick()
{

  if ( validate_data()==true)
  { 

    top.clear_container_bottom_frame(); 
    document.supervisor_form.p_result_code.value = 'NEXT' ; 

//    document.supervisor_form.p_action_value.value = 'SUBMIT' ;
    document.supervisor_form.submit(); 
  }
}


function main_menu_gif_onClick(msg)
{

 document.supervisor_form.p_result_code.value="MAIN_MENU";
    if (confirm(msg))
    {
      document.supervisor_form.submit();
    }
}
  

function set_exit_form_changed()
{
  document.supervisor_form.p_form_changed.value="Y" ; 
}


<!-- ----------------------------------------------------------------------
<!-- --------------------- cancel_button_onClick --------------------------
<!-- ----------------------------------------------------------------------
//Display a confirmation
//message.  If confirmed to exit, exit to Person Search.

function cancel_button_onClick (msg)
{
  document.supervisor_form.p_result_code.value="CANCEL";
    if (confirm(msg))
    {
      top.clear_container_bottom_frame(); 
      document.supervisor_form.target= "container_middle" ; 
      document.supervisor_form.submit();
    }
}









































