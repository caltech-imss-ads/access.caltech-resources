<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : HRVWSapi.js
// Description : API to Handle views refresh requirements
//
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------  -----------  ------- ------- ---------------------------------
// pbrimble    27-Mar-2000  110.06  1249645 Ensure that myList flag is reset 
//                                          when writing the detail frame.
// pbrimble    22-Mar-2000  110.05  1244910 set default of currTab to 
//                                          'EMPLOYMENT'
// pzclark     16-Mar-2000  110.04  1237692 Changed reference to frame
//                                          container_middle to
//                                          container_middle_view.
// pzclark     08-Mar-2000  110.03  1226758 Added var searchMode which is set
//                                          true when the 'View Summary Details
//                                          of Selected People' option is used
//                                          from the search results screen.
// pbrimble    20-Feb-2000  110.02          Added function to pop-up kiosk
//                                          views window from MEE header.
// pbrimble    02-Dec-1999  110.01  1193653 Added Functions for the Flex and
//                                          Comp windows.  Updated RefreshViews
//                                          functionality to always refresh the
//                                          view - to stop 'slow webserver' bug
//                                          When setting the mode we now clear
//                                          the tabs to stop users clicking
//                                          on a tab while the detail is being
//                                          loaded.
// pbrimble &
// pzclark     02-Dec-1999  110.00          Created.
// ==========================================================================
// $Header: HRVWSapi.js 115.3 2000/04/21 11:32:43 pkm ship        $
//
// HRVWSapi.js custom API for handling views refresh requirements

// Global variables

//declare variable dispHref as a new String Object with default value of 'BLANK'
var dispHref     = 'BLANK';
var currTab      = 'EMPLOYMENT';
var currMode     = 'DIRECTS';
var myList       = 'FALSE';
var personSearch = 'FALSE';
var searchMode   = 'FALSE'; //added version 110.03
var currPerson   = 'BLANK';
var personList   = 'BLANK';
var popup_window = new Object()
var sortColumn   = 'Employee_Name';
var sortOrder    = 'ASC';

popup_window.open = false


// Function to write the mode when it is changed
function setNewMode(index){
    clearViewsHeader()
    if (index == 0){
      writeCurrMode('DIRECTS');
    }else if (index == 1){
      writeCurrMode('ALL');
    }else{
      writeCurrMode('SUMMARY');
    }
}

// Function to refresh detail frame if detail not BLANK
function refreshDetail () {
    if (dispHref != 'BLANK') {
      //if (top.container_middle_view.container_display.location.href != dispHref) {
      //  top.container_middle_view.container_display.location.href = dispHref;
      //}
      top.container_middle_view.container_display.location.href = dispHref;
    }
}

// Function to refresh detail frame with relevant sorting if detail is not BLANK
function sortDetail (p_URL, p_sort_column) {
    var vMode
    if (dispHref != 'BLANK') {
      if (p_sort_column) {
        if (p_sort_column == sortColumn) {
          if (sortOrder == 'ASC') {
            sortOrder = 'DESC'
          } else {
            sortOrder = 'ASC'
          }
        } else {
          sortColumn = p_sort_column
          sortOrder  = 'ASC'
        }
        if (myList == 'TRUE') {
          vMode = 'MYLIST'
        } else if (searchMode == 'TRUE') {
          vMode = 'SEARCH'
        } else {
          vMode = currMode
        }
      }
      top.container_middle_view.container_display.location.href
      = (p_URL + '?p_person_id=' + currPerson + '&p_package=' + currTab
         + '&p_mode=' + vMode + '&p_person_list=' + personList
         + '&p_sort_column=' + sortColumn + '&p_sort_order=' + sortOrder);
    }
}

// Function to set the current body URL - currently not used
function redispDetail() {
    dispHref = top.container_middle_view.container_display.location.href
}

// Function to update display frame based on entered URL
function writeUrl(p_URL, p_person, p_mode) {
    var vMode
    searchMode   = 'FALSE'
    myList       = 'FALSE'
    personSearch = 'FALSE'

    if (p_mode == 'SUMMARY' || p_mode == 'DETAIL' || p_mode == 'MYLIST' || p_mode == 'SEARCH_VIEW') {
      vMode = p_mode
      if (p_mode == 'MYLIST') {
        myList = 'TRUE'
      }
      if (p_mode == 'SEARCH_VIEW') {
        personSearch = 'TRUE'
        currMode     = 'SUMMARY'//added version 110.03
      }
    } else {
      vMode = currMode
    }
    currPerson = p_person
    sortColumn = 'Employee_Name'
    sortOrder  = 'ASC'
    top.container_middle_view.container_display.location.href
      = (p_URL + '?p_person_id=' + p_person + '&p_package=' + currTab + '&p_mode=' + vMode + '&p_person_list=' + personList)
}

// Function to update display frame based on entered URL
function displaySearchResults(p_URL, p_mode, p_person_list) {
    personList = p_person_list;
    currMode   = 'SUMMARY'//added version 110.03
    searchMode = 'TRUE'   //added version 110.03
    myList     = 'FALSE'
    top.container_middle_view.container_display.location.href = p_URL;
}

// Function to update the search frame based on entered URL
function displayPersonSearch(p_URL) {
    top.container_middle_view.search_frame.location.href = p_URL;
}

// Function to update the hierarchy tree display based on entered URL
function writeTreeUrl(p_URL, p_mode) {
    var vMode
    if (p_mode == 'SUMMARY') {
      vMode = p_mode
    } else {
      vMode = currMode
    }
    top.container_middle_view.search_frame.location.href
      = (p_URL + '&p_mode=' + vMode)
}

// Function to update the hierarchy tree display based on entered URL
function writeMiddleUrl(p_URL, p_mode) {
    top.container_middle_view.location.href = (p_URL + '&p_mode=' + currMode)
}


// Function to update currMode variable
function writeCurrMode(p_currMode) {
    currMode      = p_currMode
    writeUrl('hr_mee_views_web.display',currPerson,'DIRECTS/ALL')
}

// Function to update currPerson variable
function writeCurrPerson(p_currPerson) {
    if (currPerson == 'BLANK') {
      currPerson  = p_currPerson
    }
}

// Function to update dispHref variable - currently not used
function writeDispHref(p_Href) {
    if (dispHref == 'BLANK') {
      dispHref = p_Href
      refreshDetail ()
    }
}

// Function to clear container_top
function clearViewsHeader () {
output = top.get_dark_blue_page()
top.container_top.document.write (output)
}

// Function to refresh container_top and store the current body URL
function refreshViewsHeader (p_container_top_frame, p_curr_tab, p_mode) {
    var vMode
    if (p_container_top_frame) {
      if (p_mode == 'SUMMARY' || p_mode == 'DETAIL') {
          vMode = p_mode
      } else if (myList == 'TRUE') {
        vMode = 'MYLIST'
      } else if (searchMode == 'TRUE'){ //added version 110.03
        vMode = 'SEARCH'
      } else if (personSearch == 'TRUE') {
        vMode = 'SEARCH_VIEW'
      } else {
        vMode = currMode
      }
      vTopURL = (p_container_top_frame + '&p_mode=' + vMode + '&p_person_list=' + personList)
      //vTopURL = (p_container_top_frame + '&p_mode=' + vMode)
      if (top.container_top.location.href != vTopURL) {
        top.container_top.location.href = vTopURL
      }
    }
    if (p_curr_tab) {
      currTab  = p_curr_tab;
    }
  dispHref = top.container_middle_view.container_display.location.href;
}

// Function to refresh container_bottom frame and display
function refreshViews (p_container_bottom_frame) {
    if (top.container_bottom.location.href != p_container_bottom_frame) {
      top.container_bottom.location.href = p_container_bottom_frame;
    }
  //setTimeout('refreshDetail()',1000)
  refreshDetail()
}

// Function to create a popup detail window.
var popup_window = new Object();
popup_window.open = false;

function navWindow(p_url, p_name, p_height, p_width){
  var attributes = "height="+p_height+",width="+p_width+",resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
  popup_window.win = window.open(p_url, "popup_window", attributes);
  if (popup_window.win != null) {
    if (popup_window.win.opener == null){
        popup_window.win.opener = self
    }
    popup_window.win.focus()
  }
}

// Function to create the flex window.
var flex_window = new Object();
flex_window.open = false;

function navFLEXWindow(p_url, p_name, p_height, p_width){
  var attributes = "height="+p_height+",width="+p_width+",resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
  flex_window.win = window.open(p_url, "flex_window", attributes);
  if (flex_window.win != null) {
    if (flex_window.win.opener == null){
        flex_window.win.opener = self
    }
    flex_window.win.focus()
  }
}

// Function to create the comp window.
var comp_window = new Object();
comp_window.open = false;

function navCOMPONENTSWindow(p_url, p_name, p_height, p_width){
  var attributes = "height="+p_height+",width="+p_width+",resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
  comp_window.win = window.open(p_url, "comp_window", attributes);
  if (comp_window.win != null) {
    if (comp_window.win.opener == null){
        comp_window.win.opener = self
    }
    comp_window.win.focus()
  }
}

// Function to create the Main window.
var main_window = new Object();
main_window.open = false;

function runViewsModule(p_url){
  var attributes = "height=500,width=800,resizable=yes,scrollbars=yes,menubar=no,toolbar=no";
  main_window.win = window.open(p_url, "main_window", attributes);
  if (main_window.win != null) {
    if (main_window.win.opener == null){
        main_window.win.opener = self
    }
    main_window.win.focus()
  }
}

// ? is this needed ?

function help_window(){
   help_win = window.open('/OA_HTML/US/icxcfgmn.htm', "help_win","resizable=yes,scrollbars=yes,toolbar=yes,width=450,height=250")}

