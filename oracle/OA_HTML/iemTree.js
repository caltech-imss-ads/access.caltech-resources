/*
<!-- 
 +============================================================================+
 |      Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA        |
 |                         All rights reserved.                               |
 +============================================================================+
 |  FILENAME:  iemTree.js                                                     |
 +============================================================================+	
 |  DESCRIPTION:                                                              |
 |    iemTree.js is a javascript that draws a graphical tree.                 |
 |	The tree can contain one or more nodes that can be expanded and          |
 | 	de-expanded by clicking the "expand" plus/minus icon to the left of      |
 |	each node.                                                               |
 |	Called by: iemAddFd.jsp,iemRemFd.jsp,iemTree.jsp                         | 
 +============================================================================+
 |  HISTORY                                                                   |
 |    [01/13/99, bgreenfi] added folder state saving                          |
 |    [01/17/99, bgreenfi] fixed folder state saving - highlighted folder     |
 |                         state was not being updated properly               |
 |    [01/19/00, bgreenfi] gets Messages from Database                        |
 |    [01/21/00, bgreenfi] Added folder message Count                         |
 |    [01/31/00, bgreenfi] Added initKBTree function + tree displays "header" |
 |                         banner and "methods" banner only if specified      |
 |    [02/01/00, mrabatin] Changed reference to top to parent                 |
 |    [02/08/00, bgreenfi] Header-Banner Font Color changed to White          |
 |    [02/14/00, bgreenfi] setTreeImgs() fixed to draw "Minus" (Expanded)     |
 |                         node-Icons for folders w/ No Children              |
 |    [02/14/00, bgreenfi] setTreeImgs() fixed to draw NO node-Icons          |
 |                         for folders w/ No Children,                        |
 |                         -Also opens the folder-Icon for the selected fold  |
 |    [02/17/00, acastro]  GUI changes to add/remove header                   |
 |    [02/23/00, bgreenfi] initKBTree() checks for null Message Arrays        |
 |    [03/14/00, acastro]  BUG 1236508: keeping status of root folder as 'a', |
 |                         changing totalfolders to folderList.length in      |
 |                         initTree                                           |
 |    [03/30/00, acastro]  Initial accessibility compliance changes           |
 |    [04/12/00, juyeh  ]  Modified to delete the method text                 | 
 |    [05/10/00, bgreenfi] -fixed name of IMGSRC_NODEEND file                 |
 |                         -changed "../media" to "/OA_MEDIA/"                |
 |                         -popupNewFoldName() checks for dup folder name     |
 |                         -added 5th msg param to initTree()                 |
 |    [06/01/00, jizheng]  remove escape                                      |                 
 |    [10/31/00, ukari]    Cabo Compliance: Images, Bin Headers, Tree Links   | 
 |    [01/04/01, ukari]    Obsoleted by iemCTree.js (Cabo), iemMTree.js       | 
 +============================================================================+
-->
<!-- $Header: iemTree.js 115.14 2001/01/04 14:46:25 pkm ship   $ -->
