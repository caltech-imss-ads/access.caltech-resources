/* $Header: APWFREVW.js 115.22 2001/02/19 18:11:46 pkm ship   $ */
//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
/*
	Filename:	final_review.js
	Author:	Ron Langi

	Description:	R11i Final Review
			This code will reside in apwutili.js
			to invoke final review:
				top.fGenFinalReviewFrameset();
				top.fShowWeeklySummary(initial, byExpenseType);
			to invoke confirmation page:
				top.fGenConfirmFrameset();
				top.fShowConfirm(expenseReportNumber, byExpenseType);
				

	server dependencies:
	--------------------
	AK prompts
	FND messages
	what the payment scenario is
		(top.fGetPaymentScenario())
	whether the user is a projects user 
		(top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled())
	minimum receipt amount requiring a receipt
		(top.fGetRequireReceiptAmount(receipt.getObjExpenseType().mGetstrName()))

	open items:
	-----------
	cosmetic changes based on new UI from Chip

	the following are differences between the different pay scenarios:
	------------------------------------------------------------------
	Expense Summary
	Personal expenses are excluded in Individual pay scenario

code review changes:
--------------------
replace 2ndary window logic with framMain logic
add Akita's DST handler
sort weekly summary table by sunday
separate logic to build weekly arrays
remove unnecessary eval()s
use fDynamicButton() need cabo for others
removed layers

*/

	// messages used in final review window
	// None


//-----------------------------------------------------------------------------
// Function: fGetFinalReviewPrompts
// Desc:	get final review prompts
//-----------------------------------------------------------------------------
function fGetFinalReviewPrompts() {
        // regions used in final review window
        var tmpRegion = top.g_objRegions.mobjGetRegion('AP_WEB_FINAL_REVIEW');

	// prompts used in final review window
	strAP_WEB_FR_REVIEW_ER = tmpRegion.mobjGetRegionItem('AP_WEB_FR_REVIEW_ER').mstrGetPrompt();
	strAP_WEB_FR_SELECT_SUBMIT = tmpRegion.mobjGetRegionItem('AP_WEB_FR_SELECT_SUBMIT').mstrGetPrompt();
	strAP_WEB_FR_SELECT_BACK = tmpRegion.mobjGetRegionItem('AP_WEB_FR_SELECT_BACK').mstrGetPrompt();
	strAP_WEB_FR_GEN_INFO = tmpRegion.mobjGetRegionItem('AP_WEB_FR_GEN_INFO').mstrGetPrompt();
	strAP_WEB_FR_NAME = tmpRegion.mobjGetRegionItem('AP_WEB_FR_NAME').mstrGetPrompt();
	strAP_WEB_FR_COST_CENTER = tmpRegion.mobjGetRegionItem('AP_WEB_FR_COST_CENTER').mstrGetPrompt();
	strAP_WEB_FR_PURPOSE = tmpRegion.mobjGetRegionItem('AP_WEB_PURPOSE').mstrGetPrompt();
	strAP_WEB_FR_LINES_REQ_REC = tmpRegion.mobjGetRegionItem('AP_WEB_EXPLINES_REQREC').mstrGetPrompt();
	strAP_WEB_FR_EXP_DATES = tmpRegion.mobjGetRegionItem('AP_WEB_EXPDATES').mstrGetPrompt();
 
	strAP_WEB_FR_ET_SUMMARY = tmpRegion.mobjGetRegionItem('AP_WEB_FR_ET_SUMMARY').mstrGetPrompt();
	strAP_WEB_FR_EG_SUMMARY = tmpRegion.mobjGetRegionItem('AP_WEB_FR_EG_SUMMARY').mstrGetPrompt();

	strAP_WEB_FR_WEEKLY_ET = tmpRegion.mobjGetRegionItem('AP_WEB_FR_WEEKLY_ET').mstrGetPrompt();
	strAP_WEB_FR_WEEKLY_EG = tmpRegion.mobjGetRegionItem('AP_WEB_FR_WEEKLY_EG').mstrGetPrompt();

	strAP_WEB_FR_SUN = tmpRegion.mobjGetRegionItem('AP_WEB_FR_SUN').mstrGetPrompt();
	strAP_WEB_FR_MON = tmpRegion.mobjGetRegionItem('AP_WEB_FR_MON').mstrGetPrompt();
	strAP_WEB_FR_TUE = tmpRegion.mobjGetRegionItem('AP_WEB_FR_TUE').mstrGetPrompt();
	strAP_WEB_FR_WED = tmpRegion.mobjGetRegionItem('AP_WEB_FR_WED').mstrGetPrompt();
	strAP_WEB_FR_THU = tmpRegion.mobjGetRegionItem('AP_WEB_FR_THU').mstrGetPrompt();
	strAP_WEB_FR_FRI = tmpRegion.mobjGetRegionItem('AP_WEB_FR_FRI').mstrGetPrompt();
	strAP_WEB_FR_SAT = tmpRegion.mobjGetRegionItem('AP_WEB_FR_SAT').mstrGetPrompt();
	strAP_WEB_FR_TOTALS = tmpRegion.mobjGetRegionItem('AP_WEB_FR_TOTALS').mstrGetPrompt();

	strAP_WEB_FR_COMPANY_CC = tmpRegion.mobjGetRegionItem('AP_WEB_FR_COMPANY_CC').mstrGetPrompt();
	strAP_WEB_FR_BUSINESS = tmpRegion.mobjGetRegionItem('AP_WEB_FR_BUSINESS').mstrGetPrompt();
	strAP_WEB_FR_OOP = tmpRegion.mobjGetRegionItem('AP_WEB_FR_OOP').mstrGetPrompt();
	strAP_WEB_FR_PERSONAL = tmpRegion.mobjGetRegionItem('AP_WEB_FR_PERSONAL').mstrGetPrompt();
	strAP_WEB_FR_DATE = tmpRegion.mobjGetRegionItem('AP_WEB_DATE').mstrGetPrompt();
	strAP_WEB_FR_RECEIPT_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt();
	strAP_WEB_FR_EXCH_RATE = tmpRegion.mobjGetRegionItem('AP_WEB_FR_EXCH_RATE').mstrGetPrompt();
	strAP_WEB_FR_EXPENSE_TYPE = tmpRegion.mobjGetRegionItem('AP_WEB_EXPTYPE').mstrGetPrompt();
	strAP_WEB_FR_MERCHANT = tmpRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetPrompt();
	strAP_WEB_FR_JUSTIF = tmpRegion.mobjGetRegionItem('AP_WEB_JUST').mstrGetPrompt();
	strAP_WEB_FR_PROJ_NUM = tmpRegion.mobjGetRegionItem('AP_WEB_PA_PROJECT_NUMBER').mstrGetPrompt();
	strAP_WEB_FR_TASK_NUM = tmpRegion.mobjGetRegionItem('AP_WEB_PA_TASK_NUMBER').mstrGetPrompt();
	strAP_WEB_FR_REC_REQ = tmpRegion.mobjGetRegionItem('AP_WEB_FR_REC_REQ').mstrGetPrompt();
	strAP_WEB_FR_REIMBURS_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetPrompt();
	strAP_WEB_FR_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_FR_AMOUNT').mstrGetPrompt();
	strAP_WEB_FR_BUS_SUBTOTAL = tmpRegion.mobjGetRegionItem('AP_WEB_FR_BUS_SUBTOTAL').mstrGetPrompt();
	strAP_WEB_FR_OOP_SUBTOTAL = tmpRegion.mobjGetRegionItem('AP_WEB_FR_OOP_SUBTOTAL').mstrGetPrompt();
	strAP_WEB_FR_PER_SUBTOTAL = tmpRegion.mobjGetRegionItem('AP_WEB_FR_PER_SUBTOTAL').mstrGetPrompt();

	strAP_WEB_FR_EXP_SUMMARY = tmpRegion.mobjGetRegionItem('AP_WEB_EXP_SMRY').mstrGetPrompt();

		// Individual Pay Scenario
	strAP_WEB_FR_CC_EXPENSES = tmpRegion.mobjGetRegionItem('AP_WEB_FR_CC_EXPENSES').mstrGetPrompt();
	strAP_WEB_FR_OOP_EXPENSES = tmpRegion.mobjGetRegionItem('AP_WEB_FR_OOP_EXPENSES').mstrGetPrompt();
	strAP_WEB_FR_TOTAL_EXPENSES = tmpRegion.mobjGetRegionItem('AP_WEB_FR_TOTAL_EXPENSES').mstrGetPrompt();

		// Both Pay Scenario
	strAP_WEB_FR_COMPANY_PAY_CC = tmpRegion.mobjGetRegionItem('AP_WEB_FR_COMPANY_PAY_CC').mstrGetPrompt();
	strAP_WEB_FR_EMP_PAY_CC = tmpRegion.mobjGetRegionItem('AP_WEB_FR_EMP_PAY_CC').mstrGetPrompt();

		// Company Pay Scenario - You Owe Company/Company Owes You
	strAP_WEB_FR_EMP_OWE_COMPANY = tmpRegion.mobjGetRegionItem('AP_WEB_FR_EMP_OWE_COMPANY').mstrGetPrompt();
	strAP_WEB_FR_REIMBURS_EMP = tmpRegion.mobjGetRegionItem('AP_WEB_FR_REIMBURS_EMP').mstrGetPrompt();

	strAP_WEB_FR_VIEW_EG = tmpRegion.mobjGetRegionItem('AP_WEB_FR_VIEW_EG').mstrGetPrompt();
	strAP_WEB_FR_VIEW_ET = tmpRegion.mobjGetRegionItem('AP_WEB_FR_VIEW_ET').mstrGetPrompt();

	strAP_WEB_FR_COMPANY_CC_SUBTOTAL = tmpRegion.mobjGetRegionItem('AP_WEB_FR_COMPANY_CC_SUBTOTAL').mstrGetPrompt();

}

//-----------------------------------------------------------------------------
// Function: fFRCalcDailyAmount
// Desc:	Calculate daily amount
//-----------------------------------------------------------------------------

function fFRCalcDailyAmount(objR){
   var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
   var l_reimbcurr_precision =0;
   var daily_amount = 0;
   if (objR){
   var action = top.fDetermineConversion(objR.getReceiptCurr(), fGetObjHeader().getReimbursCurr(),objR);


   if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;

   if (action == g_reimbEqualsRec) {
	 if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y')
     	   daily_amount = fRound( objR.getDailyRate() / objR.getExchRate(), l_reimbcurr_precision);
   	 else
           daily_amount = fRound( fMultiply(objR.getDailyRate(), objR.getExchRate()), l_reimbcurr_precision);
    } 
    else if (action == g_reimbEurRecFixed) {
         fixed_rate = fGetFixedRate(objR.getReceiptCurr());
         daily_amount = eval(objR.getDailyRate()) / eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecEur) {
         fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         daily_amount = eval(objR.getDailyRate()) *  eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecFixed) {
         var rate1 = fGetFixedRate(objR.getReceiptCurr());
         var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         daily_amount = fMultiply((eval(objR.getDailyRate())/rate1), rate2);
            }
    }
    return daily_amount;
}

//-----------------------------------------------------------------------------
// Function: fShowWeeklySummary
// Desc:	show weekly summary
//-----------------------------------------------------------------------------
function fShowWeeklySummary(initial, byExpenseType) {
        if (initial) {
		top.fGetFinalReviewPrompts();
        	top.fDrawFinalReviewFooter(top.framMain.footer);
        	top.fDrawFinalReviewSummary(top.framMain.summary, true, 'right', 'colorg5');
	 }
	 top.fDrawFinalReviewHeader(top.framMain.header, byExpenseType);
	 top.fDrawNavFrame("finalReview", byExpenseType);
	 top.fDrawFinalReviewBody(top.framMain.body, byExpenseType);
}

//-----------------------------------------------------------------------------
// Function: fGenFinalReviewFrameset
// Desc:	draw frameset and frames
//-----------------------------------------------------------------------------
function fGenFinalReviewFrameset(){
	 var d = top.framMain.document;
	 d.open();

         // BUG: 1479799
         if (isWindows95 && isIE5) 
           d.writeln("              <frameset rows=\"20,200,1,1\" border=0 framespacing=0>");
        else { 
           d.writeln("              <frameset rows=\"20,200,85,10\" border=0 framespacing=0>");
        }
        d.writeln("            	  		scrolling=no>");
        d.writeln("              		<frame src=\"javascript:top.fBlankPage('cccccc');\"");
        d.writeln("              			name=header");
        d.writeln("             	 		marginwidth=0");
        d.writeln("             	 		frameborder=0 noresize");
        d.writeln("            	  		scrolling=no>");
        d.writeln("              		<frame");
        d.writeln("                      		src=\"javascript:top.fBlankPage('cccccc');\" ");
        d.writeln("                      		frameborder=0");
        d.writeln("                      		name=body");
        d.writeln("                      		marginwidth=10");
        d.writeln("                      		scrolling=auto noresize>");
        d.writeln("              		<frame");
        d.writeln("                      		src=\"javascript:top.fBlankPage('cccccc');\" ");
        d.writeln("                      		frameborder=0");
        d.writeln("                      		name=summary");
        d.writeln("                      		marginwidth=10");
        d.writeln("                      		scrolling=auto noresize>");
        d.writeln("              		<frame src=\"javascript:top.fBlankPage('cccccc');\"");
        d.writeln("              			name=footer");
        d.writeln("             	 		marginwidth=0");
        d.writeln("             	 		frameborder=0");
        d.writeln("            	  		scrolling=no noresize>");

        d.writeln("  	    	</frameset>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fShowConfirm
// Desc:	show confirmation page
//-----------------------------------------------------------------------------
function fShowConfirm(expenseReportNumber, byExpenseType) {
 	 top.fGetFinalReviewPrompts();

	 top.fDrawToolbar(true,true,true,false,'top.framMain.body'); // Disable Save and Reload buttons
	 top.fDrawConfirmFooter(top.framMain.footer);
	 top.fDrawConfirmHeader(top.framMain.header, byExpenseType);
	 top.fDrawNavFrame("confirmation", byExpenseType);
	 top.fDrawConfirmBody(top.framMain.body, byExpenseType);
	 top.g_strFunc = "confirm";

}

//-----------------------------------------------------------------------------
// Function: fGenConfirmFrameset
// Desc:	draw confirmation page frameset and frames
//-----------------------------------------------------------------------------
function fGenConfirmFrameset(){
	 var d = top.framMain.document;
	 d.open();
        d.writeln("              <frameset rows=\"20,*,10\" border=0 framespacing=0>");
        d.writeln("            	  		scrolling=no>");
        d.writeln("              		<frame src=\"javascript:top.fBlankPage('cccccc');\"");
        d.writeln("              			name=header");
        d.writeln("             	 		marginwidth=0");
        d.writeln("             	 		frameborder=0");
        d.writeln("            	  		scrolling=no noresize>");
        d.writeln("              		<frame");
        d.writeln("                      		src=\"javascript:top.fBlankPage('ffffff');\" ");
        d.writeln("                      		frameborder=0");
        d.writeln("                      		name=body");
        d.writeln("                      		marginwidth=10");
        d.writeln("                      		scrolling=auto noresize>");
        d.writeln("              		<frame src=\"javascript:top.fBlankPage('cccccc');\"");
        d.writeln("              			name=footer");
        d.writeln("             	 		marginwidth=0");
        d.writeln("             	 		frameborder=0");
        d.writeln("            	  		scrolling=no noresize>");
        d.writeln("  	    	</frameset>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawFinalReviewTop
// Desc:	draw top frame
//-----------------------------------------------------------------------------
function fDrawFinalReviewTop(frame) {  
	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body bgcolor=#336699>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

	 d.writeln("<!-- beginning of toolbar code -->");
	 d.writeln("<table width=100% cellpadding=0 cellspacing=0 border=0 class=color3>");
	 d.writeln("	<tr><td height=5></td></tr> <!-- spacer row -->");
	 d.writeln("	<tr>");
	 d.writeln("		<td>");
	 d.writeln("		  <!--inner table to define toolbar-->");
	 d.writeln("			  <table cellpadding=0 cellspacing=0 border=0>");
	 d.writeln("				<tr>");
	 d.writeln("					<td rowspan=3><img src="+top.g_strImgDir+"APWGTBL.gif></td>");
	 d.writeln("					<td class=color6 height=1 colspan=3><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	 d.writeln("					<td rowspan=3><img src="+top.g_strImgDir+"APWGTBR.gif></td>");
	 d.writeln("				</tr>");
	 d.writeln("				<tr>");
	 d.writeln("					<td class=colorg5 nowrap height=30 align=middle>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbhome src="+top.g_strImgDir+"APWIWHOM.gif align=middle border=0></a>");
	 d.writeln("						<img src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	 d.writeln("					</td>");
	 d.writeln("					<td class=colorg5 nowrap height=30 align=middle>");
	 d.writeln("						<font class=paneltitle>&nbsp; New Expense Report &nbsp;</font>");
	 d.writeln("					</td>");
	 d.writeln("					<td class=colorg5 nowrap height=30 align=middle>");
	 d.writeln("						<img src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbsave src="+top.g_strImgDir+"APWIWSAV.gif align=absmiddle border=0></a>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbprint src="+top.g_strImgDir+"APWIWPRT.gif align=absmiddle border=0></a>");
	 d.writeln("						<img src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbrefresh src="+top.g_strImgDir+"APWIWRLD.gif align=absmiddle border=0></a>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbstop src="+top.g_strImgDir+"APWIWSTP.gif align=absmiddle border=0></a>");
	 d.writeln("						<img src="+top.g_strImgDir+"APWIWDVD.gif align=absmiddle>");
	 d.writeln("						<a href=\"#\">");
	 d.writeln("						<img name=tbhelp src="+top.g_strImgDir+"APWIWHLP.gif align=absmiddle border=0></a>");
	 d.writeln("					</td>");
	 d.writeln("				</tr>");
	 d.writeln("				<tr>");
 	 d.writeln("					<td class=color3 height=1 colspan=3><img src="+top.g_strImgDir+"APWPX3.gif></td>");
	 d.writeln("				</tr>");
	 d.writeln("			</table>");
	 d.writeln("		</td>");
	 d.writeln("		<td rowspan=5 width=100% align=right valign=top><img src="+top.g_strImgDir+"APWLWAPP.gif></td>");
	 d.writeln("	</tr>");
	 d.writeln("	<tr><td height=5></td></tr> <!-- spacer row -->");
	 d.writeln("</table>");
	 d.writeln("<!-- end of toolbar code -->");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawFinalReviewHeader
// Desc:	draw header frame
//-----------------------------------------------------------------------------
function fDrawFinalReviewHeader(frame, byExpenseType) {  
	 var reviewRegion = top.g_objRegions.mobjGetRegion('AP_WEB_FINAL_REVIEW');

	 var strHeader = "";
	 if (top.fGetPaymentScenario() == g_cIndividual) {
		 strHeader = "- " + reviewRegion.mobjGetRegionItem('AP_WEB_INDIV_PAY').mstrGetPrompt();
	 } else if (top.fGetPaymentScenario() == g_cBoth) {
		 strHeader = " - "+ reviewRegion.mobjGetRegionItem('AP_WEB_BOTH_PAY').mstrGetPrompt();
	 } else if (top.fGetPaymentScenario() == g_cCompany) {
		 strHeader = " - "+reviewRegion.mobjGetRegionItem('AP_WEB_COMPANY_PAY').mstrGetPrompt();
	 } else {
		 strHeader = " - "+reviewRegion.mobjGetRegionItem('AP_WEB_NO_CARD').mstrGetPrompt();
	 }

	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body class=colorg5>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

        d.writeln("<!--beginning of final review header-->");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=100% class=colorg5>");
        d.writeln("	<tr>");
        d.writeln("		<td class=color2 rowspan=2 valign=top width=1 align=left><IMG SRC="+top.g_strImgDir+"APWCTTLS.gif width=5 height=20></td>");
        d.writeln("		<td class=color6 width=1000 height=1><IMG SRC="+top.g_strImgDir+"APWPX6.gif height=1 width=500></td>");
        d.writeln("		<td class=color2 rowspan=2 valign=top width=1 align=right><IMG SRC="+top.g_strImgDir+"APWCTTRS.gif width=5 height=20></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");

	 d.writeln("		<td valign=top class=color2>");
	 if (byExpenseType) {
	        d.writeln("		<font class=containertitle color=#ffffff>"+strAP_WEB_FR_ET_SUMMARY+strHeader+"</font>");
	 } else {
	        d.writeln("		<font class=containertitle color=#ffffff>"+strAP_WEB_FR_EG_SUMMARY+strHeader+"</font>");
	 }
	 d.writeln("		</td>");

        d.writeln("	</tr>");
        d.writeln("</table>");
        d.writeln("<!--end of final review header-->");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawConfirmHeader
// Desc:	draw header frame
//-----------------------------------------------------------------------------
function fDrawConfirmHeader(frame, byExpenseType) { 
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CONFIRM_WIN');
	var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body class=color3>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

        d.writeln("<!--beginning of final review header-->");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=100% class=color2>");
        d.writeln("	<tr>");
        d.writeln("		<td class=color2 rowspan=2 valign=top width=1 align=left><IMG SRC="+top.g_strImgDir+"APWCTTLS.gif width=5 height=20></td>");
        d.writeln("		<td class=color6 width=1000 height=1><IMG SRC="+top.g_strImgDir+"APWPX6.gif height=1 width=500></td>");
        d.writeln("		<td class=color2 rowspan=2 valign=top width=1 align=right><IMG SRC="+top.g_strImgDir+"APWCTTRS.gif width=5 height=20></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");

	 d.writeln("		<td valign=top class=color2>");
	 if (byExpenseType) {
	        d.writeln("		<font class=containertitle color=#ffffff>"+tempRegion.mobjGetRegionItem('AP_WEB_CONFIRMATION').mstrGetPrompt()+"</font>");
	 } else {
	        d.writeln("		<font class=containertitle color=#ffffff>"+tempRegion.mobjGetRegionItem('AP_WEB_CONFIRMATION').mstrGetPrompt()+"</font>");
	 }
	 d.writeln("		</td>");

        d.writeln("	</tr>");
        d.writeln("</table>");
        d.writeln("<!--end of final review header-->");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fBuildWeeklyArrays
// Desc:	build weekly arrays
//-----------------------------------------------------------------------------
function fBuildWeeklyArrays(arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypes, arrWeeklyExpenseGroups) {  
	 var DAY = 1000 * 60 * 60 * 24;

	 // create a weekly receipt array containing an array for each day of the week
	 // which contains pointers to receipts that fall on that day
	 // expanding occurrences into single receipts
	 // and build weekly summary expense types and expense groups arrays
	 for (var r=0; r<2; r++) {
		var receipts;
		if (r == 0) {
			receipts = top.objExpenseReport.oopReceipts;
		} else {
			// do not process credit card receipt if no card program scenario
			if (top.fGetPaymentScenario() == '')
				continue;
			receipts = top.objExpenseReport.cCardReceipts;
		}
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				// add a receipt for all occurrences
				for (var j=0; j<receipts[i].occurs; j++) {
					var added = false;
					var dayObj;
					// add to correct day array depending on which occurrence
					var receiptDate = top.fStringToDate(receipts[i].date);
					receiptDate.setTime(receiptDate.getTime() + (j*DAY));
					for (var k=0; k<arrWeeklyReceipts.length; k++) {
						dayObj = arrWeeklyReceipts[k].arrReceipts[receiptDate.getDay()];						// week exists so add the receipt to the existing week
						if (arrWeeklyReceipts[k].Sun == top.fGetSunday(top.fDateToString(receiptDate))) {
							// just point to top receipt array
							dayObj[dayObj.length] = new Object();
							dayObj[dayObj.length-1] = receipts[i];
							added = true;
							break;
						}
					}
					// week doesn't exist so create a new week and add the receipt
					if (!added) {
						arrSortBySunday[arrSortBySunday.length] = new Object();
						arrSortBySunday[arrSortBySunday.length-1] = top.fStringToDate(top.fGetSunday(top.fDateToString(receiptDate))).getTime();
						arrSortBySunday.sort();
						arrWeeklyReceipts[arrWeeklyReceipts.length] = new Object();
						arrWeeklyReceipts[arrWeeklyReceipts.length-1].Sun = top.fGetSunday(top.fDateToString(receiptDate));
						arrWeeklyReceipts[arrWeeklyReceipts.length-1].arrReceipts = new Array();
						for (var k=0; k<7; k++ ) {
							arrWeeklyReceipts[arrWeeklyReceipts.length-1].arrReceipts[k] = new Array();
						}
						dayObj = arrWeeklyReceipts[arrWeeklyReceipts.length-1].arrReceipts[receiptDate.getDay()];
						// just point to top receipt array
						dayObj[dayObj.length] = new Object();
						dayObj[dayObj.length-1] = receipts[i];
					}
				}
				// add to weekly expense types array
				var exists = false;
					for (var l=0; l<arrWeeklyExpenseTypes.length; l++) {
					if (receipts[i].getObjExpenseType().mGetstrName() == arrWeeklyExpenseTypes[l]) {
						exists = true;
						break;
					}
				}
				if (!exists) {
					arrWeeklyExpenseTypes[arrWeeklyExpenseTypes.length] = new Object;
					arrWeeklyExpenseTypes[arrWeeklyExpenseTypes.length-1] = receipts[i].getObjExpenseType().mGetstrName();
					arrWeeklyExpenseTypes.sort();
				}
				// add to weekly expense groups array
				exists = false;
				for (var l=0; l<arrWeeklyExpenseGroups.length; l++) {
					if (receipts[i].expenseGroup == arrWeeklyExpenseGroups[l]) {
						exists = true;
						break;
					}
				}
				if (!exists) {
					arrWeeklyExpenseGroups[arrWeeklyExpenseGroups.length] = new Object;
					arrWeeklyExpenseGroups[arrWeeklyExpenseGroups.length-1] = receipts[i].expenseGroup;
					arrWeeklyExpenseGroups.sort();
				}
			}
		}
	 }
}

//-----------------------------------------------------------------------------
// Function: fDrawFinalReviewBody
// Desc:	draw body
//-----------------------------------------------------------------------------
function fDrawFinalReviewBody(frame, byExpenseType) {
	 var arrSortBySunday = new Array();
	 var arrWeeklyReceipts = new Array();
	 var arrWeeklyExpenseTypes = new Array();
	 var arrWeeklyExpenseGroups = new Array();
	 top.fBuildWeeklyArrays(arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypes, arrWeeklyExpenseGroups);

	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body bgcolor=#cccccc>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

	 if (byExpenseType) {
		 top.fDrawGeneralInfo(d);
		 top.fDrawWeeklySummary(d, arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypes, byExpenseType);

		 // do not process credit card receipt if no card program scenario
		 if (top.fGetPaymentScenario() != '')
			 top.fDrawReceiptSummary(d, g_cBusiness);

		 top.fDrawReceiptSummary(d, 'oop');

		 // do not display Personal expenses if it is an Individual Pay scenario or no card program
		 if (top.fGetPaymentScenario() != g_cIndividual &&  top.fGetPaymentScenario() != '')
			 top.fDrawPersonalSummary(d);
	 } else {
		 top.fDrawGeneralInfo(d);
		 top.fDrawWeeklySummary(d, arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseGroups, false);
	 }

        d.writeln("<!--end of final review body-->");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        // BUG: 1479799.  Cannot draw summary frame.  Include summary in body.
        if (isWindows95 && isIE5)
          top.fDrawFinalReviewSummary(frame, false, 'right', 'colorg5');
        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawConfirmBody
// Desc:	draw body
//-----------------------------------------------------------------------------
function fDrawConfirmBody(frame, byExpenseType) {
	 var arrSortBySunday = new Array();
	 var arrWeeklyReceipts = new Array();
	 var arrWeeklyExpenseTypes = new Array();
	 var arrWeeklyExpenseGroups = new Array();
	 top.fBuildWeeklyArrays(arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypes, arrWeeklyExpenseGroups);

	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body bgcolor=white>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

	 if (byExpenseType) {
		 top.fDrawConfirmGeneralInfo(d);
         	 top.fDrawFinalReviewSummary(frame, false, 'left', 'white');
		 top.fDrawWeeklySummary(d, arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypes, byExpenseType);

		 // do not process credit card receipt if no card program scenario
		 if (top.fGetPaymentScenario() != '')
			 top.fDrawReceiptSummary(d, g_cBusiness);

		 top.fDrawReceiptSummary(d, 'oop');

		 // do not display Personal expenses if it is an Individual Pay scenario or no card program
		 if (top.fGetPaymentScenario() != g_cIndividual &&  top.fGetPaymentScenario() != '')
			 top.fDrawPersonalSummary(d);
	 } else {
		 top.fDrawGeneralInfo(d);
         	 top.fDrawFinalReviewSummary(frame, false, 'right', 'colorg5');
		 top.fDrawWeeklySummary(d, arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseGroups, false);
	 }

        d.writeln("<!--end of final review body-->");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawGeneralInfo
// Desc:	draw general info
//-----------------------------------------------------------------------------
function fDrawGeneralInfo(d) {
        d.writeln("<!--General Information-->");
        d.writeln("<table border=0 width=98% background-color=#ffffff>");
        d.writeln("	<tr>");
        d.writeln("		<td><font class=promptblack>"+strAP_WEB_FR_REVIEW_ER+"  "+strAP_WEB_FR_SELECT_SUBMIT+"  "+strAP_WEB_FR_SELECT_BACK+"</font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_GEN_INFO+"</font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><image src="+top.g_strImgDir+"APWPX3.gif width=98% height=2></td>");
        d.writeln("	</tr>");
        d.writeln("</table>");

        d.writeln("<!--beginning of final review body-->");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% class=colorg5>");
        d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("		<td><center><table cellpadding=0 cellspacing=0 border=0 width=75% class=colorg5>");
        d.writeln("			<tr>");
        d.writeln("				<td align=right><font class=promptblack>"+strAP_WEB_FR_NAME+" </font></td>");
        d.writeln("				<td align=left><font class=datablack><b>"+top.objExpenseReport.header.getObjEmployee().mGetEmployeeName()+" ("+top.objExpenseReport.header.getObjEmployee().mGetEmployeeNum()+") </b></font></td>");
        d.writeln("				<td align=right><font class=promptblack>"+strAP_WEB_FR_COST_CENTER+" </font></td>");
	d.writeln("				<td align=left><font class=datablack><b>"+top.objExpenseReport.header.getCostCenter()+"</b></font></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
        d.writeln("				<td align=right><font class=promptblack>"+strAP_WEB_FR_PURPOSE+" </font></td>");
        d.writeln("				<td align=left><font class=datablack><b>"+top.objExpenseReport.header.purpose+" </b></font></td>");
        d.writeln("				<td align=right><font class=promptblack>"+strAP_WEB_FR_LINES_REQ_REC+" </font></td>");
        d.writeln("				<td align=left><font class=datablack><b>"+top.fGetNumLinesReqReceipt()+"</b></font></td>");
        d.writeln("			</tr>");
        d.writeln("				<tr>");
        d.writeln("				<td align=right><font class=promptblack>"+strAP_WEB_FR_EXP_DATES+" </font></td>");
        d.writeln("				<td align=left><font class=datablack><b>"+top.fGetStartExpenseDate()+" - "+top.fGetEndExpenseDate()+"</b></font></td>");
        d.writeln("				<td align=right>&nbsp;</td>");
        d.writeln("				<td align=left>&nbsp;</td>");
        d.writeln("			</tr>");
        d.writeln("		</table></center></td>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("	</tr>");
        d.writeln("</table>");
	
}

//-----------------------------------------------------------------------------
// Function: fDrawConfirmGeneralInfo
// Desc:	draw confirmation general info
//-----------------------------------------------------------------------------
function fDrawConfirmGeneralInfo(d) {
var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_CONFIRM_WIN');
        d.writeln("<!--General Information-->");
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% class=color6>");
	d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
	d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td align=left><font class=alerttitle>"+tempRegion.mobjGetRegionItem('AP_WEB_EXP_REPORT').mstrGetPrompt()+ top.g_strRepNum_prefix +top.objExpenseReport.header.reportHeaderID+"</font></td>");
	d.writeln("		<td align=right><font class=alerttitle>"+tempRegion.mobjGetRegionItem('AP_WEB_CONFIRMATION').mstrGetPrompt()+"</font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td align=left><font class=promptblack>"+strAP_WEB_FR_NAME+" </font>");
        d.writeln("		<font class=datablack><b>"+top.objExpenseReport.header.getObjEmployee().mGetEmployeeName()+" ("+top.objExpenseReport.header.getObjEmployee().mGetEmployeeNum()+") </b></font></td>");
        d.writeln("		<td align=right><font class=promptblack>"+strAP_WEB_FR_COST_CENTER+" </font>");
        d.writeln("		<font class=datablack><b>"+top.objExpenseReport.header.getCostCenter()+"</b></font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td align=left><font class=promptblack>"+strAP_WEB_FR_PURPOSE+" </font>");
        d.writeln("		<font class=datablack><b>"+top.objExpenseReport.header.purpose+" </b></font></td>");
        d.writeln("		<td align=right><font class=promptblack>"+strAP_WEB_FR_LINES_REQ_REC+" </font>");
        d.writeln("		<font class=datablack><b>"+top.fGetNumLinesReqReceipt()+"</b></font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td align=left><font class=promptblack>"+strAP_WEB_FR_EXP_DATES+" </font>");
        d.writeln("		<font class=datablack><b>"+top.fGetStartExpenseDate()+" - "+top.fGetEndExpenseDate()+"</b></font></td>");
        d.writeln("	</tr><tr><td colspan=2>&nbsp;</td></tr>");
        d.writeln("</table>");
	d.writeln("<table class=colorg5 border=1 width=98%>");
	d.write("<tr><td><img src="+top.g_strImgDir+"APWISBCF.gif align=absmiddle>");
	var arrTokenNames = new Array();
	var arrTokenVals = new Array();
	arrTokenNames[0] = "&INVOICENUM";
	arrTokenVals[0] = " "+top.g_strRepNum_prefix + top.objExpenseReport.header.reportHeaderID;
	var strTemp = top.g_objMessages.mstrGetMesg("AP_WEB_SUBMIT_HEADER");
	strTemp = strTemp.replace(arrTokenNames[0],arrTokenVals[0]);
	d.writeln("<font class=datablack>"+strTemp+"</font></td></tr>");
		
	d.writeln("</table>");
	d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% class=color6>");
	d.writeln("	<tr>");
        d.writeln("		<td class=promptblack>");
	d.writeln(top.g_objMessages.mstrGetMesg("AP_WEB_EXP_SUB_INSTRUCTIONS"));
	d.writeln("		</td>");
	d.writeln("	</tr>");
	d.writeln("</table>");
}


//-----------------------------------------------------------------------------
// Function: fDrawWeeklySummary
// Desc:	draw weekly summary
//-----------------------------------------------------------------------------
function fDrawWeeklySummary(d, arrWeeklyReceipts, arrSortBySunday, arrWeeklyExpenseTypesOrGroups, byExpenseType) {
        d.writeln("<!--beginning of weekly summary-->");
        d.writeln("<table border=0 width=98% background-color=#ffffff>");
        d.writeln("	<tr>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
	 if (byExpenseType) {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_WEEKLY_ET+"</font></td>");
	 } else {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_WEEKLY_EG+"</font></td>");
	 }
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><image src="+top.g_strImgDir+"APWPX3.gif width=98% height=2></td>");
        d.writeln("	</tr>");
        d.writeln("</table>");

	 // draw a weekly summary per week that contains an expense receipt
	 for (var h=0; h<arrSortBySunday.length; h++) {
		for (var i=0; i<arrWeeklyReceipts.length; i++) {
			if (arrSortBySunday[h] == top.fStringToDate(arrWeeklyReceipts[i].Sun).getTime())
				break;
		}
	 	var sun = arrWeeklyReceipts[i].Sun;
		var mon = top.fDateAddDay(sun,1);
		var tue = top.fDateAddDay(mon,1);
		var wed = top.fDateAddDay(tue,1);
		var thu = top.fDateAddDay(wed,1);
	 	var fri = top.fDateAddDay(thu,1);
	 	var sat = top.fDateAddDay(fri,1);

        	// draw top curves
        	d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% align=center>");
        	d.writeln("<tr>");
        	d.writeln("	<td rowspan=2></td>");
        	d.writeln("	<td></td>");
        	d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        	d.writeln("	<td></td>");
        	d.writeln("	<td rowspan=2></td>");
        	d.writeln("</tr>");

        	d.writeln("<tr>");
        	d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCTL.gif></td>");
        	d.writeln("	<td width=675 height=1 class=tableheader nowrap><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        	d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCTR.gif></td>");
        	d.writeln("</tr>");

	 	// draw rows
        	d.writeln("<tr>");
        	d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        	d.writeln("	<td height=1 colspan=3 class=tableheader><table cellpadding=0 cellspacing=1 border=0 width=100% class=tablesurround align=center>");
        	d.writeln("			<tr>");
        	d.writeln("				<td class=tableheader align=center><br></td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_SUN+"<br>"+sun+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_MON+"<br>"+mon+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_TUE+"<br>"+tue+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_WED+"<br>"+wed+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_THU+"<br>"+thu+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_FRI+"<br>"+fri+"</td>");
        	d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_SAT+"<br>"+sat+"</td>");
        	d.writeln("				<td class=tableheader align=center><br>"+strAP_WEB_FR_TOTALS+"</td>");
        	d.writeln("			</tr>");

		var allSunTotal = 0;
		var allMonTotal = 0;
		var allTueTotal = 0;
		var allWedTotal = 0;
		var allThuTotal = 0;
		var allFriTotal = 0;
		var allSatTotal = 0;
		var allWeeklyTotal = 0;

		for (var j=0; j<arrWeeklyExpenseTypesOrGroups.length; j++ ) {
			var sunTotal = top.fGetDayTotal(arrWeeklyReceipts[i],0,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var monTotal = top.fGetDayTotal(arrWeeklyReceipts[i],1,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var tueTotal = top.fGetDayTotal(arrWeeklyReceipts[i],2,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var wedTotal = top.fGetDayTotal(arrWeeklyReceipts[i],3,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var thuTotal = top.fGetDayTotal(arrWeeklyReceipts[i],4,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var friTotal = top.fGetDayTotal(arrWeeklyReceipts[i],5,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			var satTotal = top.fGetDayTotal(arrWeeklyReceipts[i],6,arrWeeklyExpenseTypesOrGroups[j], byExpenseType);
			
			allSunTotal += sunTotal;
			allMonTotal += monTotal;
			allTueTotal += tueTotal;
			allWedTotal += wedTotal;
			allThuTotal += thuTotal;
			allFriTotal += friTotal;
			allSatTotal += satTotal;

        		d.writeln("			<!-- generate one row for each expense type or group -->");
        		d.writeln("			<tr>");
			if (byExpenseType)
	        		d.writeln("				<td class=tableheader>"+arrWeeklyExpenseTypesOrGroups[j]+"</td>");
			else
	        		d.writeln("				<td class=tableheader>"+arrWeeklyExpenseTypesOrGroups[j]+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(sunTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(monTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(tueTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(wedTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(thuTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(friTotal)+"</td>");
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(satTotal)+"</td>");
			var weeklyTotal = eval(sunTotal)+eval(monTotal)+eval(tueTotal)+eval(wedTotal)+eval(thuTotal)+eval(friTotal)+eval(satTotal);
			allWeeklyTotal += weeklyTotal;
        		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(weeklyTotal)+"</td>");
        		d.writeln("			</tr>");
		}

        	d.writeln("			<tr>");
        	d.writeln("				<td height=1 colspan=9 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
        	d.writeln("			</tr>");

        	d.writeln("			<tr>");
        	d.writeln("				<td class=tablerowr><font class=datablack>&nbsp;"+strAP_WEB_FR_TOTALS+"</font></td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allSunTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allMonTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allTueTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allWedTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allThuTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allFriTotal)+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allSatTotal)+"</td>");
       	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(allWeeklyTotal)+"</td>");
        	d.writeln("			</tr>");

        	d.writeln("	</table></td>");

        	d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        	d.writeln("</tr>");

        	// draw bottom curves
        	d.writeln("<tr>");
        	d.writeln("	<td rowspan=2></td>");
        	d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCBL.gif></td>");
        	d.writeln("	<td width=675 height=1 class=tableheader><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        	d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCBR.gif></td>");
        	d.writeln("	<td rowspan=2></td>");
        	d.writeln("</tr>");

        	d.writeln("<tr>");
        	d.writeln("	<td></td>");
        	d.writeln("	<td width=800 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        	d.writeln("	<td></td>");
        	d.writeln("</tr>");
        	d.writeln("</TABLE>");
	 	d.writeln("<br>");
	 }

        d.writeln("<!--end of weekly summary-->");
}

//-----------------------------------------------------------------------------
// Function: fDrawReceiptSummary
// Desc:	draw receipt summary
//-----------------------------------------------------------------------------
function fDrawReceiptSummary(d, receiptType) {
	 // if no receipts to draw then return
	 if (!top.fAnyReceipts(receiptType))
		return;

        d.writeln("<!--beginning of receipt summary -->");
        d.writeln("<table border=0 width=98% background-color=#ffffff>");
        d.writeln("	<tr>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
	 if (receiptType == g_cBusiness && top.fGetPaymentScenario() != g_cIndividual) {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_COMPANY_CC+" "+strAP_WEB_FR_BUSINESS+"</font></td>");
	 } else if (receiptType == g_cBusiness && top.fGetPaymentScenario() == g_cIndividual) {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_CC_EXPENSES+"</font></td>");
	 } else if (receiptType == 'oop') {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_OOP+"</font></td>");
	 } else {
	        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_OOP+"</font></td>");
	 }
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><image src="+top.g_strImgDir+"APWPX3.gif width=98% height=2></td>");
        d.writeln("	</tr>");
        d.writeln("</table>");

        // draw top curves
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% align=center>");
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCTL.gif></td>");
        d.writeln("	<td width=675 height=1 class=tableheader nowrap><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCTR.gif></td>");
        d.writeln("</tr>");

	 // draw rows
        d.writeln("<tr>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td height=1 colspan=3 class=tableheader><table cellpadding=0 cellspacing=1 border=0 width=100% height=100% class=tablesurround align=center>");
        d.writeln("			<tr>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_DATE+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_RECEIPT_AMOUNT+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_EXCH_RATE+"</td>");
	 d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_EXPENSE_TYPE+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_MERCHANT+"</td>");
         if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled() == 'N'){
		 // for non-projects users
	        d.writeln("			<td class=tableheader align=center>"+strAP_WEB_FR_JUSTIF+"</td>");
	 } else {
		 // for projects users
	        d.writeln("			<td class=tableheader align=center>"+strAP_WEB_FR_PROJ_NUM+"</td>");
	        d.writeln("			<td class=tableheader align=center>"+strAP_WEB_FR_TASK_NUM+"</td>");
	 }
      	 d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_REC_REQ+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_REIMBURS_AMOUNT+"<br>("+top.objExpenseReport.header.getReimbursCurr()+")</td>");
        d.writeln("			</tr>");
        d.writeln("			<!-- generate one row for each receipt -->");
	 var receipts;
	 if (receiptType == g_cBusiness) {
		 receipts = top.objExpenseReport.cCardReceipts;
	 } else if (receiptType == 'oop') {
		 receipts = top.objExpenseReport.oopReceipts;
	 }
	 for (var i=0; i<receipts.length; i++) {
	   if (receipts[i] != null) {
	     var action = fDetermineConversion(receipts[i].getReceiptCurr(), fGetObjHeader().getReimbursCurr(),receipts[i]); 
        		 d.writeln("			<tr>");
		        d.writeln("				<td class=tablerowr>"+top.fDateToString(top.fStringToDate(receipts[i].date))+"</td>");
			 // if occurs > 1 show it
			 if (receipts[i].occurs > 1) {
		        	d.writeln("				<td class=tablerowr>"+receipts[i].occurs+" x "+top.fMoneyFormatDailyRate(receipts[i])+" ("+receipts[i].getReceiptCurr()+")</td>");
			 } else {
		        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormatExpensedAmount(receipts[i])+" ("+receipts[i].getReceiptCurr()+")</td>");
			 }

		// check if fixed rate applied
		if ((action == top.g_reimbEurRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbFixedRecFixed)) 
			d.writeln("				<td class=tablerow>"+top.g_objMessages.mstrGetMesg("AP_WEB_FIXED")+"</td>");
		else
			d.writeln("				<td class=tablerow>"+receipts[i].getExchRate()+"</td>");

		       
			if (receipts[i].getObjExpenseType().mGetstrName() != null && receipts[i].getObjExpenseType().mGetstrName() != "")
			    d.writeln("				<td class=tablerowl>"+receipts[i].getObjExpenseType().mGetstrName()+"</td>");
			else
			    d.writeln("				<td class=tablerowl>&nbsp;</td>");
			if (receipts[i].merchant != null && receipts[i].merchant != "")
		            d.writeln("				<td class=tablerowl>"+receipts[i].merchant+"</td>");
			else
		            d.writeln("				<td class=tablerowl>&nbsp;</td>");
			 if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled() == 'N'){
				 // for non-projects users
				if (receipts[i].justification != null && receipts[i].justification != "")
			            d.writeln("			<td class=tablerowl>"+receipts[i].justification+"</td>");
				else
			            d.writeln("			<td class=tablerowl>&nbsp;</td>");
			 } else {
				 // for projects users
				if (receipts[i].projectNumber != null && receipts[i].projectNumber != "")
			            d.writeln("			<td class=tablerowl>"+receipts[i].projectNumber+"</td>");
				else
			            d.writeln("			<td class=tablerowl>&nbsp;</td>");
				if (receipts[i].taskNumber != null && receipts[i].taskNumber != "")
			            d.writeln("			<td class=tablerowl>"+receipts[i].taskNumber+"</td>");
				else
			            d.writeln("			<td class=tablerowl>&nbsp;</td>");
			 }
			 if (top.fReceiptRequired(receipts[i])) {
			        d.writeln("			<td class=tablerow><img src="+top.g_strImgDir+"APWICHEK.gif border=0></td>");
			 } else {
			        d.writeln("			<td class=tablerow>&nbsp;</td>");
			 }
	        	 d.writeln("				<td class=tablerowr>"+top.fMoneyFormatReimbursAmount(receipts[i])+"</td>");
		        d.writeln("			</tr>");
		}
	 }
        d.writeln("			<tr>");
	 if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled() == 'N'){
	  // for non-projects users
	        d.writeln("			<td height=1 colspan=8 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	 } else {
		 // for projects users
	        d.writeln("			<td height=1 colspan=9 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	 }
        d.writeln("			</tr>");
        d.writeln("			<tr>");
	 if (receiptType == g_cBusiness) {
		 if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled() == 'N'){
			 // for non-projects users
		        d.writeln("				<td class=tablerowr colspan=7>");
		 } else {
			 // for projects users
		        d.writeln("				<td class=tablerowr colspan=8>");
		 }
		 if (top.fGetPaymentScenario() != g_cIndividual) {
			 d.writeln("				<font class=datablack>"+strAP_WEB_FR_BUS_SUBTOTAL+"&nbsp;</font></td>");
		 } else {
			 d.writeln("				<font class=datablack>"+strAP_WEB_FR_COMPANY_CC_SUBTOTAL+"&nbsp;</font></td>");
		 }
	        d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(top.fGetFinalReviewSubtotal(g_cBusiness))+"</td>");
	 } else if (receiptType == 'oop') {
		 if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled() == 'N'){
			 // for non-projects users
		        d.writeln("				<td class=tablerowr colspan=7>");
		 } else {
			 // for projects users
		        d.writeln("				<td class=tablerowr colspan=8>");
		 }
		 d.writeln("				<font class=datablack>"+strAP_WEB_FR_OOP_SUBTOTAL+"&nbsp;</font></td>");
	        d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(top.fGetFinalReviewSubtotal('oop'))+"</td>");
	 }
        d.writeln("			</tr>");
        d.writeln("	</table></td>");

        d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("</tr>");

        // draw bottom curves
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCBL.gif></td>");
        d.writeln("	<td width=675 height=1 class=tableheader><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCBR.gif></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td></td>");
        d.writeln("	<td width=800 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("</tr>");
        d.writeln("</TABLE>");

        d.writeln("<!--end of receipt summary-->");     
}

//-----------------------------------------------------------------------------
// Function: fGetPersonalCCReceipts
// Desc:	get excluded portions of credit card receipts
//-----------------------------------------------------------------------------
function fGetPersonalCCReceipts() {  
		var personalReceipts = new Array();
		var receipts;
		// do not process credit card receipt if no card program scenario
		if (top.fGetPaymentScenario() == '')
			return null;
		receipts = top.objExpenseReport.cCardReceipts;

		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				if (eval(receipts[i].getReceiptAmount()) > eval(receipts[i].getExpensedAmount())) {
					var exists = false;
					var k;
					for (var j=0; j<personalReceipts.length; j++) {
						if (personalReceipts[j].getItemizeId() == receipts[i].getItemizeId()) {
							exists = true;
							k = j;
							break;
						}
					}
					if (exists) {
						personalReceipts[k].setExpensedAmount(eval(personalReceipts[k].getExpensedAmount()) + eval(receipts[i].getExpensedAmount()));
					} else {
						k = personalReceipts.length;
						personalReceipts[k] = new receiptObject();
						personalReceipts[k].setItemizeId(receipts[i].getItemizeId());
						personalReceipts[k].setDate(receipts[i].getDate());
						personalReceipts[k].setExpensedAmount(receipts[i].getExpensedAmount());
						personalReceipts[k].setReceiptAmount(receipts[i].getReceiptAmount());
						personalReceipts[k].setReceiptCurr(receipts[i].getReceiptCurr());
						personalReceipts[k].setExchRate(receipts[i].getExchRate());
						personalReceipts[k].setMerchant(receipts[i].getMerchant());
					}
				}
			}
		}
		return personalReceipts;
}

//-----------------------------------------------------------------------------
// Function: fDrawPersonalSummary
// Desc:	draw personal summary
//-----------------------------------------------------------------------------
function fDrawPersonalSummary(d) {
	 // excluded portion of business receipts are personal
	 var receipts = top.fGetPersonalCCReceipts();
	 // personal charges
	 var trxns = new top.creditCardTransactions();
	 trxns = top.fGetPersonalTransactions();

	 // if no personal charges to draw then return
	 if (receipts.length == 0 && trxns == null)
		return;

        d.writeln("<!--beginning of personal summary -->");
        d.writeln("<table border=0 width=98% class=#ffffff>");
        d.writeln("	<tr>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
	 d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_COMPANY_CC+" "+strAP_WEB_FR_PERSONAL+"</font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><image src="+top.g_strImgDir+"APWPX3.gif width=98% height=2></td>");
        d.writeln("	</tr>");
        d.writeln("</table>");

        // draw top curves
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=98% align=center>");
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCTL.gif></td>");
        d.writeln("	<td width=675 height=1 class=tableheader nowrap><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCTR.gif></td>");
        d.writeln("</tr>");

	 // draw rows
        d.writeln("<tr>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td height=1 colspan=3 class=tableheader><table cellpadding=0 cellspacing=1 border=0 width=100% height=100% class=tablesurround align=center>");
        d.writeln("			<tr>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_DATE+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_RECEIPT_AMOUNT+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_EXCH_RATE+"</td>");
        d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_MERCHANT+"</td>");
	 d.writeln("				<td class=tableheader align=center>"+strAP_WEB_FR_AMOUNT+"<br>("+top.objExpenseReport.header.getReimbursCurr()+")</td>");
        d.writeln("			</tr>");
        d.writeln("			<!-- generate one row for each receipt -->");

	 // personal charges
	 if (trxns != null) {
		 trxns.reset();
		 var t = trxns.nextTrxn();
		 while (t != null) {
	      		d.writeln("			<tr>");
		       d.writeln("				<td class=tablerowr>"+top.fDateToString(top.fStringToDate(t.date))+"</td>");
			d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(t.trxAmt)+" ("+t.trxCurrency+")</td>");
		       d.writeln("				<td class=tablerow>"+top.fMoneyFormat(eval(t.amt)/eval(t.trxAmt))+"</td>");
		       d.writeln("				<td class=tablerowl>"+t.merchant+"</td>");
	        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(t.amt)+"</td>");
		       d.writeln("			</tr>");
			t = trxns.nextTrxn();
		 }
	 }
	 // excluded portions of cc receipts
	 for (var i=0; i<receipts.length; i++) {
	      var action = fDetermineConversion(receipts[i].getReceiptCurr(), fGetObjHeader().getReimbursCurr(),receipts[i]);
      		d.writeln("			<tr>");
	       d.writeln("				<td class=tablerowr>"+top.fDateToString(top.fStringToDate(receipts[i].date))+"</td>");
		d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(eval(receipts[i].getReceiptAmount()) - eval(receipts[i].getExpensedAmount()))+" ("+receipts[i].getReceiptCurr()+")</td>");

	// check if fixed-rate applied
	if ((action == top.g_reimbEurRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbFixedRecFixed)) 
	       d.writeln("				<td class=tablerow>"+top.g_objMessages.mstrGetMesg("AP_WEB_FIXED")+"</td>");
	else
	       d.writeln("				<td class=tablerow>"+receipts[i].getExchRate()+"</td>");
	 
	       d.writeln("				<td class=tablerowl>"+receipts[i].merchant+"</td>");
        	d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(eval(receipts[i].getReceiptAmount()) - eval(receipts[i].getExpensedAmount()))+"</td>");
	       d.writeln("			</tr>");
	 }

        d.writeln("			<tr>");
	 d.writeln("				<td height=1 colspan=5 class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
        d.writeln("			</tr>");
        d.writeln("			<tr>");
	 d.writeln("				<td class=tablerowr colspan=4><font class=datablack>"+strAP_WEB_FR_PER_SUBTOTAL+"&nbsp;</font></td>");
	 d.writeln("				<td class=tablerowr>"+top.fMoneyFormat(top.fGetFinalReviewSubtotal(g_cPersonal))+"</td>");
        d.writeln("			</tr>");
        d.writeln("	</table></td>");

        d.writeln("	<td height=1 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("</tr>");

        // draw bottom curves
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCBL.gif></td>");
        d.writeln("	<td width=675 height=1 class=tableheader><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCBR.gif></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td></td>");
        d.writeln("	<td width=800 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("</tr>");
        d.writeln("</TABLE>");

        d.writeln("<!--end of personal summary-->");     
}

//-----------------------------------------------------------------------------
// Function: fDrawFinalReviewSummary
// Desc:	draw summary
//-----------------------------------------------------------------------------
function fDrawFinalReviewSummary(frame, isAlone, strAlign, strStyle) { 
	 var ccBusinessTotal = eval(top.fGetFinalReviewSubtotal(g_cBusiness));
	 var oopTotal = eval(top.fGetFinalReviewSubtotal('oop'));
	 var ccPersonalTotal = eval(top.fGetFinalReviewSubtotal(g_cPersonal));
	 var l_style = "";
	 if (strStyle == 'white') l_style = "white";
	 var firstPrompt = '';
	 var firstValue = 0;
	 var secondPrompt = '';
	 var secondValue = 0;
	 var thirdPrompt = '';
	 var thirdValue = 0;
	 var fourthPrompt = '';
	 var fourthValue = 0;
	 var currencyPrompt3 = top.objExpenseReport.header.getReimbursCurr();
	 var currencyPrompt4 = '&nbsp;';
	 if (top.fGetPaymentScenario() == g_cIndividual || top.fGetPaymentScenario() == null) {
		firstPrompt = strAP_WEB_FR_CC_EXPENSES;
		firstValue = ccBusinessTotal;
		secondPrompt = strAP_WEB_FR_OOP_EXPENSES;
		secondValue = oopTotal;
		thirdPrompt = strAP_WEB_FR_TOTAL_EXPENSES;
		thirdValue = eval(ccBusinessTotal) + eval(oopTotal);
	 } else if (top.fGetPaymentScenario() == g_cBoth) {
		firstPrompt = strAP_WEB_FR_COMPANY_PAY_CC;
		firstValue = ccBusinessTotal;
		secondPrompt = strAP_WEB_FR_OOP_EXPENSES;
		secondValue = oopTotal;
		thirdPrompt = strAP_WEB_FR_TOTAL_EXPENSES;
		thirdValue = eval(ccBusinessTotal) + eval(oopTotal);
		fourthPrompt = strAP_WEB_FR_EMP_PAY_CC;
		fourthValue = ccPersonalTotal;
	 	currencyPrompt4 = top.objExpenseReport.header.getReimbursCurr();
	 } else if (top.fGetPaymentScenario() == g_cCompany) {
		if (eval(ccPersonalTotal) > eval(oopTotal)) {
			firstPrompt = strAP_WEB_FR_PERSONAL;
			firstValue = ccPersonalTotal;
			secondPrompt = strAP_WEB_FR_OOP_EXPENSES;
			secondValue = oopTotal;
			thirdPrompt = strAP_WEB_FR_EMP_OWE_COMPANY;
			thirdValue = eval(ccPersonalTotal) - eval(oopTotal);
		} else {
			firstPrompt = strAP_WEB_FR_OOP_EXPENSES;
			firstValue = oopTotal;
			secondPrompt = strAP_WEB_FR_PERSONAL;
			secondValue = ccPersonalTotal;
			thirdPrompt = strAP_WEB_FR_REIMBURS_EMP;
			thirdValue = eval(oopTotal) - eval(ccPersonalTotal);
		}
	 } else {
		secondPrompt = strAP_WEB_FR_OOP_EXPENSES;
		secondValue = oopTotal;
		thirdPrompt = strAP_WEB_FR_TOTAL_EXPENSES;
		thirdValue = eval(oopTotal);
	 }

	 var d = frame.document;

	if (isAlone) {
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body class="+strStyle+">");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");
	}

        d.writeln("<!--beginning of Expense Summary -->");
        d.writeln("<table border=0 width=98% class="+strStyle+">");
        d.writeln("	<tr>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><font class=datablack>"+strAP_WEB_FR_EXP_SUMMARY+"</font></td>");
        d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td><image src="+top.g_strImgDir+"APWPX3.gif width=98% height=2></td>");
        d.writeln("	</tr>");

        d.writeln("	<tr align="+strAlign+">");
	 d.writeln("		<td><table cellpadding=0 cellspacing=0 border=0 class="+strStyle+">");

	 if (top.fGetPaymentScenario() != '') {
	 // 1st row
	 d.writeln("			<tr>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+firstPrompt+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("				<td class=tablerowlgrey"+l_style+">&nbsp; </td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+top.fMoneyFormat(firstValue)+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("			</tr>");
	 }

	 // 2nd row
	 d.writeln("			<tr>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+secondPrompt+"</font></td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 if (top.fGetPaymentScenario() == g_cCompany) {
	 d.writeln("				<td class=tablerowgrey"+l_style+"> - </td>");
	 } else if (top.fGetPaymentScenario() != '') {
	 d.writeln("				<td class=tablerowgrey"+l_style+"> + </td>");
	 } else {
	 d.writeln("				<td class=tablerowgrey"+l_style+">&nbsp;</td>");
	 }
	 d.writeln("				<td class=tablerowlgrey"+l_style+">&nbsp; </td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+top.fMoneyFormat(secondValue)+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("			</tr>");

	 // line row
	 d.writeln("			<tr>");
	 d.writeln("				<td height=1 colspan=8 bgcolor=#000000><img src="+top.g_strImgDir+"APWPX3.gif width=98% height=1></td>");
	 d.writeln("			</tr>");

	 // 3rd row
	 d.writeln("			<tr>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+thirdPrompt+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("				<td class=tablerowlgrey"+l_style+">&nbsp; </td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='datablack'>"+top.fMoneyFormat(thirdValue)+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>"); 
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='datablack'>"+currencyPrompt3+"</font></td>");
	 d.writeln("			</tr>");

	 // 4th row
	 if (top.fGetPaymentScenario() == g_cBoth) {
	 d.writeln("			<tr>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='promptblack'>"+fourthPrompt+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>");
	 d.writeln("				<td class=tablerowlgrey"+l_style+">&nbsp; </td>");
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='datablack'>"+top.fMoneyFormat(fourthValue)+"</font></td>");
	 d.writeln("				<td colspan=2 class=tablerowrgrey"+l_style+">&nbsp;</td>"); 
	 d.writeln("				<td class=tablerowrgrey"+l_style+"><font class='datablack'>"+currencyPrompt4+"</font></td>");
	 d.writeln("			</tr>");
	 }

 	 d.writeln("		</table></td>");
        d.writeln("	</tr>");

        d.writeln("</table>");
        d.writeln("<!--end of Expense Summary-->");

	if (isAlone) {
        d.writeln("</CENTER>");
        d.writeln("</form>");
        d.writeln("</BODY></HTML>");
        d.close();
	}
}

//-----------------------------------------------------------------------------
// Function: fDrawConfirmFooter
// Desc:	draw confirmation footer
//-----------------------------------------------------------------------------
function fDrawConfirmFooter(frame) {  
	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body class=color3>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

        d.writeln("<table width=100% border=0 cellpadding=0 cellspacing=0>");
        d.writeln("	<tr class=color6>");
        d.writeln("          <td align=left><IMG SRC="+top.g_strImgDir+"APWCTBL.gif height=5 width=5></td>");
        d.writeln("          <td><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif height=5></td>");
        d.writeln("          <td align=right><IMG SRC="+top.g_strImgDir+"APWCTBR.gif height=5 width=5></td>");
        d.writeln("	</tr>");

        d.writeln("</table>");


        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}


//-----------------------------------------------------------------------------
// Function: fDrawFinalReviewFooter
// Desc:	draw footer
//-----------------------------------------------------------------------------
function fDrawFinalReviewFooter(frame) {  
	 var d = frame.document;
        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frame);
        d.writeln("</HEAD>");
        d.writeln("<body bgcolor=#336699>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");

        d.writeln("<table width=100% border=0 cellpadding=0 cellspacing=0>");
        d.writeln("	<tr class=colorg5>");
        d.writeln("          <td align=left><IMG SRC="+top.g_strImgDir+"APWCTBL.gif height=5 width=5></td>");
        d.writeln("          <td><IMG SRC="+top.g_strImgDir+"APWDBPXC.gif height=5></td>");
        d.writeln("          <td align=right><IMG SRC="+top.g_strImgDir+"APWCTBR.gif height=5 width=5></td>");
        d.writeln("	</tr>");

        d.writeln("</table>");


        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fGetSunday
// Desc:	given a date string return the sunday of that week
//-----------------------------------------------------------------------------
function fGetSunday(dateStr) {
	var DAY = 1000 * 60 * 60 * 24;
	var sun = top.fStringToDate(dateStr);
	sun.setTime(sun.getTime() - (sun.getDay()*DAY));

	// The following is used to handle daylight saving
	// 1. When it is one hour ahead ie in April
	if (sun.getHours() == 23){
		sun.setTime(sun.getTime() + 3600000);
	}
	// 2. When it is one hour behind ie in October
	else if (sun.getHours() == 1){
		sun.setTime(sun.getTime() - 3600000);
	}

	return top.fDateToString(sun);
}

//-----------------------------------------------------------------------------
// Function: fDateAddDay
// Desc:	given a date string and number of days to be added return that day
//-----------------------------------------------------------------------------
function fDateAddDay(dateStr, numDay) {
	var DAY = 1000 * 60 * 60 * 24;
	var nextday = top.fStringToDate(dateStr);
	nextday.setTime(nextday.getTime() + (DAY*numDay));

	// The following is used to handle daylight saving
	// 1. When it is one hour ahead ie in April
	if (nextday.getHours() == 23){
		nextday.setTime(nextday.getTime() + 3600000);
	}
	// 2. When it is one hour behind ie in October
	else if (nextday.getHours() == 1){
		nextday.setTime(nextday.getTime() - 3600000);
	}

	return top.fDateToString(nextday);
}

//-----------------------------------------------------------------------------
// Function: fGetDayTotal
// Desc:	for a given week, day and expense type or group, calculate the totals
//-----------------------------------------------------------------------------
function fGetDayTotal(arrWeeklyReceipts, day, expenseTypeOrGroup, byExpenseType) {
	var roundingError = 0;
	var currentDay;
	var dayTotal = 0;
	var dayObj;
	dayObj = arrWeeklyReceipts.arrReceipts[day];
	for (var k=0; k<dayObj.length; k++) {
		if ((byExpenseType && dayObj[k].getObjExpenseType().mGetstrName() == expenseTypeOrGroup) || 
		    (!byExpenseType && dayObj[k].expenseGroup == expenseTypeOrGroup)) {

			dayTotal += top.fFRCalcDailyAmount(dayObj[k]);

			currentDay = top.fDateAddDay(arrWeeklyReceipts.Sun, day);
			if (top.fHasRoundingIssue(dayObj[k], currentDay)) {
			   dayTotal -= top.fGetRoundingError(dayObj[k]);
			}
		}
	}
	return dayTotal;
}

//-----------------------------------------------------------------------------
// Function: fHasRoundingIssue
// Desc:	does a receipt have multiple occurrences and
//		receipt is on its last occurrence and
//		receipt has a rounding issue
//-----------------------------------------------------------------------------
function fHasRoundingIssue(receipt, currentDay) {
	var DAY = 1000 * 60 * 60 * 24;

	if (eval(receipt.occurs) == 1) return false;
	if (currentDay != top.fDateAddDay(receipt.date, eval(receipt.occurs)-1)) return false;
	if (top.fGetRoundingError(receipt) == 0) return false;
	return true;
}

//-----------------------------------------------------------------------------
// Function: fGetRoundingError
// Desc:	compute the rounding error for a receipt
//-----------------------------------------------------------------------------
function fGetRoundingError(receipt) {
	var roundingError = eval(receipt.getReimbursAmount()) % eval(receipt.occurs);
	if (roundingError != 0) {
		var ind = top.fGetCurrencyIndex(top.objExpenseReport.header.getReimbursCurr());
		var l_reimbcurr_precision =0;
		if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;
		roundingError /= eval(receipt.occurs);
		return (top.fRound(roundingError, l_reimbcurr_precision)-eval(roundingError))*eval(receipt.occurs);
	} else {
		return 0;
	}
}

//-----------------------------------------------------------------------------
// Function: fReceiptRequired
// Desc:	does a line require a receipt
//-----------------------------------------------------------------------------
function fReceiptRequired(receipt) {
	// all receipts in foreign currencies require a receipt
	if (receipt.getReceiptCurr() != top.objExpenseReport.header.reimbursCurr)
		return true;
	// minimum receipt amount requiring a receipt
	if (top.fGetRequireReceiptAmount(receipt.getObjExpenseType().mGetstrName()))
	  if (receipt.getExpensedAmount() > eval(top.fGetRequireReceiptAmount(receipt.getObjExpenseType().mGetstrName())))
		return true;

	return false;
}

//-----------------------------------------------------------------------------
// Function: fGetNumLinesReqReceipt
// Desc:	get number of lines requiring a receipt
//-----------------------------------------------------------------------------
function fGetNumLinesReqReceipt() {
	var numLinesReqReceipt = 0
	for (var r=0; r<2; r++) {
		var receipts;
		if (r == 0) {
			receipts = top.objExpenseReport.oopReceipts;
		} else {
			receipts = top.objExpenseReport.cCardReceipts;
		}
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				if (top.fReceiptRequired(receipts[i])) {
					numLinesReqReceipt++;
				}
			}
		}
	}
	return numLinesReqReceipt;
}

//-----------------------------------------------------------------------------
// Function: fGetFinalReviewSubtotal
// Desc:	compute the final review summary subtotals
//-----------------------------------------------------------------------------
function fGetFinalReviewSubtotal(receiptType) {
	var subtotal = 0;
	if (receiptType == g_cBusiness) {
		var receipts = top.objExpenseReport.cCardReceipts;
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				 subtotal = eval(subtotal) + eval(receipts[i].getReimbursAmount());
			}
		}
	} else if (receiptType == 'oop') {
		var receipts = top.objExpenseReport.oopReceipts;
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				 subtotal = eval(subtotal) + eval(receipts[i].getReimbursAmount());
			}
		}
	} else if (receiptType == g_cPersonal) {
		 // excluded portion of business receipts are personal
		 var receipts = top.fGetPersonalCCReceipts();
		 // excluded portions of cc receipts
		 if (receipts)
		 for (var i=0; i<receipts.length; i++) {
			if (receipts != null) {
				subtotal = eval(subtotal) + eval(receipts[i].getReceiptAmount()) - eval(receipts[i].getExpensedAmount());
			}
		 }
		 // personal charges
		 var trxns = new top.creditCardTransactions();
		 trxns = top.fGetPersonalTransactions();
		 if (trxns != null) {
			 trxns.reset();
			 var t = trxns.nextTrxn();
			 while (t != null) {
				subtotal = eval(subtotal) + eval(t.amt);
				t = trxns.nextTrxn();
			 }
		 }
	}
	return subtotal;
}

//-----------------------------------------------------------------------------
// Function: fGetStartExpenseDate
// Desc:	get the starting expense date
//-----------------------------------------------------------------------------
function fGetStartExpenseDate() {
	var startDate = new Date();
	var receiptDate = new Date();
	var initialDate = true;

	for (var r=0; r<2; r++) {
		var receipts;
		if (r == 0) {
			receipts = top.objExpenseReport.oopReceipts;
		} else {
			receipts = top.objExpenseReport.cCardReceipts;
		}
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				receiptDate = top.fStringToDate(receipts[i].date);
				if (initialDate) {
					startDate = receiptDate;
					initialDate = false;
				}
				if (receiptDate.getTime() < startDate.getTime()) startDate = receiptDate;
			}
		}
	}
	return top.fDateToString(startDate);
}

//-----------------------------------------------------------------------------
// Function: fGetEndExpenseDate
// Desc:	get the ending expense date
//-----------------------------------------------------------------------------
function fGetEndExpenseDate() {
	var endDate = new Date();
	var receiptDate = new Date();
	var initialDate = true;
	var DAY = 1000 * 60 * 60 * 24;

	for (var r=0; r<2; r++) {
		var receipts;
		if (r == 0) {
			receipts = top.objExpenseReport.oopReceipts;
		} else {
			receipts = top.objExpenseReport.cCardReceipts;
		}
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				// account for occurrences
				receiptDate = top.fStringToDate(receipts[i].date);
				receiptDate.setTime(receiptDate.getTime() + ((receipts[i].occurs-1)*DAY));
				if (initialDate) {
					endDate = receiptDate;
					initialDate = false;
				}
				if (receiptDate.getTime() > endDate.getTime()) endDate = receiptDate;
			}
		}
	}
	return top.fDateToString(endDate);
}

//-----------------------------------------------------------------------------
// Function: fAnyReceipts
// Desc:	are there any receipts for a given receipt type
//-----------------------------------------------------------------------------
function fAnyReceipts(receiptType) {
	if (receiptType == g_cBusiness) {
		var receipts = top.objExpenseReport.cCardReceipts;
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				return true;
			}
		}
	} else if (receiptType == 'oop') {
		var receipts = top.objExpenseReport.oopReceipts;
		for (var i=0; i<receipts.length; i++) {
			if (receipts[i] != null) {
				return true;
			}
		}
	}
	return false;
}


/*
	end of R11i Final Review
*/

