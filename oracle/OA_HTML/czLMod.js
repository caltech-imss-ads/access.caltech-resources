/* $Header: czLMod.js 115.9 2000/12/10 16:10:34 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function ListModel (selectionMode)
{
  this.type = 'ListModel';
  this.items = new Array();
  this.maxColWidth = null;
  this.selectionMode = selectionMode;
}

function ListModel_setSelectionMode (sel)
{
  if (sel != this.selectionMode) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'SelectionMode', value:sel})) {
      this.selectionMode = sel;
    }	
  }	
}

function ListModel_getSelectionMode ()
{
  return this.selectionMode;
}

function ListModel_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  if (isVisible == null) {
    isVisible = true;
  }
  if (isSelected == null) {
    isSelected = false; 
  }
  item.setVisible (isVisible);
  item.setSelected (isSelected);
  
  this.items[this.items.length] = item;
  item.index = this.items.length - 1;
  //TODO: Resequence the items array if index is specified;
    
  item.addListener ('propertyChangeCallback', this, 'super');
  
  this.notifyListeners ('objectAddedCallback', item);
}

function ListModel_propertyChangeCallback (change)
{
  var ret = true;
  if (change.property == 'selected') {
    if (change.value) {
      switch (this.selectionMode) {
      case (2): //none
        ret = false;
        break;
      case (0): //multi
        ret = true;
        break;
      case (1): //single
        this.clearSelection();
        ret = true;
        break;
      }
    } else {
      ret = true;
    }
  }
  return ret;
}

function ListModel_clearSelection()
{
  var item = null;
  for (var i = 0; i < this.items.length; i++) {
    item = this.items[i];
    if (item.isSelected()) item.setSelected (false);
  }
}

function ListModel_removeItem (key)
{
  this.notifyListeners('objectRemovedCallback', null);
}

function ListModel_removeItemAtIndex (index)
{

  this.notifyListeners('objectRemovedCallback', null);
}

function ListModel_getItemCount ()
{
  return this.items.length;
}

function ListModel_getMaxColumnDataDescriptors (colDescs)
{
  var colName;
  var dataDescs = new Array ();
  var descsLen = colDescs.length;
  if (this.maxColWidth) {
    for (var k = 0; k < descsLen; k++) {
      colName = colDescs[k].name;
      dataDescs[k] = new ColumnDataDescriptor (colName, null, this.maxColWidth[colName]);
      if (colName == 'descImg') {
        //put one image src to calculate pref width and height;
        if (this.items.length > 0) {
          dataDescs[k].value = this.items[0].getDescImage();
        } 
      }
    }
  } else {
    var maxLenArr = new Array ();
    var item = null;
    var itemLen = this.items.length;
    var strValue = '';
    for (var k = 0; k < descsLen; k++) {
      colName = colDescs[k].name;
      dataDescs[k] = new ColumnDataDescriptor (colName);
      maxLenArr[k] = 0;
    }
    for (var i = 0; i < itemLen; i++) {
      item = this.items[i];
      for (var j =0; j < descsLen; j++) {
        strValue = item.getValue(colDescs[j].name)
          if (strValue) {
            strLen = strValue.length;
          } else {
            strLen = 0;
          }
        if (strLen > maxLenArr[j]) {
          maxLenArr[j] = strLen;
          //dataDescs[j].value = ('' + strValue);
          dataDescs[j].width = strLen;
        }
      }
    }
  }
  return (dataDescs);
}

function ListModel_getItem (key)
{

}

function ListModel_getItemAtIndex (index)
{
  return this.items[index];
}

function ListModel_setMaxColWidth (colName, colWd)
{
  if (! this.maxColWidth)
    this.maxColWidth = new Array ();
  this.maxColWidth[colName] = colWd;
}

function ListModel_getMaxColWidth (colName)
{
  return (this.maxColWidth[colName]);
}

function ListModel_destroy ()
{
  if (this.items) {
    var len = this.items.length;
    for (var i=0; i<len; i++) {
      this.items[i].destroy ();
      
      this.items[i] = null;
      delete this.items[i];
    }
    this.items.length = 0;
    this.items = null;
    delete this.items;
    
    this.clearQueue ();
  }
}


HTMLHelper.importPrototypes(ListModel, Listener);

//public methods
ListModel.prototype.addItem = ListModel_addItem;
ListModel.prototype.removeItem = ListModel_removeItem;
ListModel.prototype.removeItemAtIndex = ListModel_removeItemAtIndex;
ListModel.prototype.getItemCount = ListModel_getItemCount;
ListModel.prototype.getItem = ListModel_getItem;
ListModel.prototype.getItemAtIndex = ListModel_getItemAtIndex;
ListModel.prototype.setSelectionMode = ListModel_setSelectionMode;
ListModel.prototype.getSelectionMode = ListModel_getSelectionMode;
ListModel.prototype.getMaxColumnDataDescriptors = ListModel_getMaxColumnDataDescriptors;
ListModel.prototype.setMaxColWidth = ListModel_setMaxColWidth;
ListModel.prototype.getMaxColWidth = ListModel_getMaxColWidth;
ListModel.prototype.destroy = ListModel_destroy;

//private methods
ListModel.prototype.clearSelection = ListModel_clearSelection;
ListModel.prototype.propertyChangeCallback = ListModel_propertyChangeCallback;

ListModel.SELECT_MULTI = 0;
ListModel.SELECT_SINGLE = 1;
ListModel.SELECT_NONE = 2;

