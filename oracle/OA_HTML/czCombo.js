/* $Header: czCombo.js 115.28 2001/07/13 09:53:14 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/
 
function ComboBox ()
{
  this.parentConstructor = Base;		
  this.parentConstructor ();

  this.width = 100;
  this.height = 20;
  this.backgroundColor = 'black';
  this.listModel = new ListModel (ListModel.SELECT_SINGLE);

  this.built = false;
  this.dropdown = null;
  this.dropdownClass = 'List';
  this.editCell = null;
  this.editCellClass = 'ListItem';
  this.btnImage = null;

  //register for events;
  this.listModel.addListener ('propertyChangeCallback', this);
  this.listModel.addListener ('objectAddedCallback', this);
  this.listModel.addListener ('objectRemovedCallback', this);
}

function ComboBox_updateDone()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.dropdown.updateDone();
  }
}

function ComboBox_buildControl ()
{
  if (this.built) return;
  this.built = true;

  eval(this.self +'=this');
  var ctl = this.editCell = eval ('new ' + this.editCellClass + '();');
  ctl.setName (this.name + 'xEdit');
  ctl.setDimensions (1, 1, this.width-16, this.height-2);
  ctl.setParentObject (this);
  ctl.enableKeyBoardEvents();
  ctl.setTabindex(this.tabindex);
  ctl.setBackgroundColor (this.itemBackColor);
  ctl.setSelectedBackColor (this.itemBackColor);
  ctl.setActiveBackColor (this.itemBackColor);
  ctl.setWindowName(this.windowName);
  ctl.setSpanClass (this.spanClass);
  if (this.selItemModel)
    ctl.setModelItem (this.selItemModel);
  else
    ctl.setModelItem (null);
  ctl.addListener ('onKeyDownCallback', this);

  ctl = this.btnImage = new ImageButton ();
  ctl.setName (this.name + 'xImg');
  ctl.setDimensions (this.width-17, 0, 16, this.height);
  ctl.setParentObject (this);
  ctl.stretch = true;
  if(ctl.arrowOn == null){
    ctl.arrowOn = IMAGESPATH+'czDArr.gif';
  }
  if(ctl.arrowOff == null){
    ctl.arrowOff = IMAGESPATH+'czDArr1.gif'
  }
  ctl.setImages (ctl.arrowOn, ctl.arrowOff);
  ctl.addListener('mouseupCallback',Arrow_MouseUpCallback);
  ctl.addListener('mousedownCallback',Arrow_MouseDownCallback);

  //Register with child objects to listen to events;
  this.btnImage.addListener ('mouseupCallback', this);
  
  //create dropdown list;
  ctl = this.dropdown = eval ('new ' + this.dropdownClass + '();');
  if(this.read)
    ctl.readOnly(this.read);
  ctl.setName (this.name + 'xLst');
  ctl.setDimensions (this.left, this.top+this.height+1, this.width, 10);
  ctl.setReference (this.reference);
  ctl.setBackgroundColor (this.itemBackColor);
  ctl.setActiveBackColor (this.activeBackColor);
  ctl.setSelectedBackColor (this.selectedBackColor);
  ctl.setSpanClass (this.spanClass);
  ctl.font = this.font;
  ctl.setVisibleRowCount (5);
  ctl.setModel (this.listModel);
  ctl.setEventManager (this.eventManager);
  ctl.mode = List.MODE_DROPDOWN;
  this.hideDropDown();
  if (this.moreInitializeDropdown) {
    this.moreInitializeDropdown();
  }
  this.dropdown.addListener ('actionMouseUp', this);
  this.dropdown.addListener ('actionOnChange', this);
}

function ComboBox_propertyChangeCallback (change)
{
  if (change.property == 'selected') {
    if (change.value == true) {
      this.editCell.setModelItem (change.source);
    }
    this.hideDropDown();
    //remove if this is still registered with event mgr; 
    this.eventManager.removeListener ('docMouseUp', this);
    //notify listeners;
    this.notifyListeners ('selectionChangeCallback', change, this);
  }
  return (true);
}

function ComboBox_objectAddedCallback (object)
{
  this.notifyListeners ('objectAddedCallback', object, this);
  return true;
}

function ComboBox_moreSetWidth (wd)
{
  if (this.built) {
    var iWidth = wd - this.btnImage.width;
    this.editCell.setWidth (iWidth - 1);
    this.btnImage.setLeft (iWidth);
    this.dropdown.setWidth (this.width);
  }
}

function ComboBox_moreSetHeight (ht)
{
  if (this.built) {
    this.editCell.setHeight (ht-2);
    this.btnImage.setHeight (ht);
    this.dropdown.setTop (this.top + this.height + 1);
  }
}

function ComboBox_setBackgroundColor (col)
{
  this.itemBackColor = col;
  if (this.built) {
    this.editCell.setBackgroundColor (col);
    this.dropdown.setBackgroundColor (col);
  }
}

function ComboBox_getBackgroundColor ()
{
  return (this.editCell.getBackgroundColor ());
}

function ComboBox_moreCSS ()
{
  if (! this.built) {
    this.buildControl ();
  }
  var CSSStr = '';
  this.editCell.setWindowName(this.windowName);
  this.dropdown.setWindowName(this.windowName);
  CSSStr += this.editCell.generateCSS() + '\n';
  CSSStr += this.btnImage.generateCSS() + '\n';
  CSSStr += this.dropdown.generateCSS();
  if (this.unSatIcon)
    CSSStr += this.unSatIcon.generateCSS () + '\n';  
  return CSSStr;
}

function ComboBox_render()
{
  var sBuffer = '';

  sBuffer += '<DIV ';
  if (this.objId != '')
    sBuffer += 'ID=' + this.objId;
  
  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';
  
  sBuffer +='>' + '\n';
  sBuffer += this.editCell.render ();
  sBuffer += this.btnImage.render ();
  sBuffer += '</DIV>' + '\n';
  sBuffer += this.dropdown.render ();
  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';
  return sBuffer;
}

function ComboBox_onLaunchCall()
{
    this.updateDone();
}

function ComboBox_moreLaunch (em)
{			
  this.editCell.launch (em);
  this.btnImage.launch (em);
  this.dropdown.launch (em);
  if (this.unSatIcon)
    this.unSatIcon.launch ();
}

function ComboBox_docMouseUp (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var eSrcName = (ns4) ? eSrc.name : eSrc.id;
    if (eSrcName == ('IMG-'+this.btnImage.name))
      return (true);
  }
  if (this.windowName) {
    var xpos = (ns4) ? e.pageX : (e.clientX+self._hwnd[this.windowName].document.body.scrollLeft);
    var ypos = (ns4) ? e.pageY : (e.clientY+self._hwnd[this.windowName].document.body.scrollTop);
  } else {
    var xpos = (ns4) ? e.pageX : (e.clientX + document.body.scrollLeft);
    var ypos = (ns4) ? e.pageY : (e.clientY + document.body.scrollTop);
  }
  //Check this x and y coordiante in inside the list;
  //if inside dont do anything else hide dropdown if it is open;
  if (this.dropdown) {
    if (! this.dropdown.isMouseInside (xpos, ypos)) {
      if (this.dropdown.opened) {
        this.hideDropDown();
	this.eventManager.removeListener ('docMouseUp', this);
      }
    }
  }
  return (true);	
}

function ComboBox_mouseupCallback (e, source)
{
  if (source.name == this.btnImage.name) {
    if (this.dropdown) {
      //Event occured on arrow image bring the dropdown;
      if (!this.dropdown.opened) {
        this.showDropDown ();
        //add to event manager listener;
        this.eventManager.addListener ('docMouseUp', this);
      } else {
        //drop down already open hide it;
        this.hideDropDown();
        this.eventManager.removeListener ('docMouseUp', this);
      }
    }
  }
  return (true);
}

function ComboBox_showDropDown ()
{
  /*
    var winHt;
  if (ie4) {
    winHt = eval (this.reference + '.body.offsetHeight');
  } else if (ns4) {
    var str = this.reference.replace (/document/i, 'window');
    winHt = eval (str + '.innerHeight');
  }
  if (winHt < (this.dropdown.top + this.dropdown.height)) {
    this.dropdown.moveTo (this.dropdown.left, this.top - this.dropdown.height-1);
  }
  */
  //Layout the dropdown columns if it is not done yet;
  if ((! this.bDropdownLaidout) && (navigator.platform.indexOf('MacPPC') == -1 )) {
    this.dropdown.fixDimensions ();
    this.bDropdownLaidout = true;
  }
  if(this.dropdown.unWantedItemsList.length > 0)
  {
     if(this.dropdown.oldHeight == null )
     {
       this.dropdown.oldHeight = this.dropdown.getHeight();
     }
     if(this.dropdown.unWantedItemsList.length > this.dropdown.activeItemsListScroll.length)
       this.dropdown.setHeight(this.dropdown.activeItemsListScroll.length*22+15);
     else
      this.dropdown.setHeight(this.dropdown.oldHeight);
  }
  else
  {
     if(this.dropdown.oldHeight != null )
     {
       this.dropdown.setHeight(this.dropdown.oldHeight);
       this.dropdown.oldHeight = null;
     }
  }
  this.dropdown.show ();
  this.dropdown.setZIndex (8888);
  this.dropdown.opened = true;
}

function ComboBox_hideDropDown ()
{
  this.dropdown.hide ();
  this.dropdown.setZIndex (0);
  this.dropdown.opened = false;
}

function ComboBox_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  if (isSelected) {
    this.selItemModel = item;
  }
  this.listModel.addItem (rtid, item, index, isVisible, isSelected);
  item.addListener ('propertyChangeCallback', this);
  return (item);
}

function ComboBox_setSelectedIndex (index) 
{
  if (this.dropdown)
    this.dropdown.setSelectedIndex (index);
}

function ComboBox_getSelectedIndex ()
{
  if (this.dropdown)
    return (this.dropdown.getSelectedIndex ());
}

function ComboBox_removeAll ()
{
  if (this.dropdown)
    this.dropdown.removeAll();
}

function ComboBox_removeItem (index)
{
  if (this.dropdown)
    this.dropdown.removeItem (index);
}

function ComboBox_getItemCount ()
{
  if (this.dropdown)
    return (this.dropdown.getItemCount());
}

function ComboBox_getItems ()
{
  if (this.dropdown) {
    return (this.dropdown.getItems());
  } else {
    return null;
  }
}

function ComboBox_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function ComboBox_getActiveBackColor ()
{
  return (this.activeBackColor);
}

function ComboBox_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function ComboBox_getSelectedBackColor ()
{
  return (this.selectedBackColor);
}

function ComboBox_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;

  this.font = fnt;	
  this.spanClass = this.name + 'SpnNml';
  
  style = this.getStyleObject (this.spanClass, true);
  this.setFontAttributes (style);
}

function ComboBox_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
  stylObj.setAttribute ('padding-left', 5);
}

function ComboBox_setEditCellClass (cls) {
  if (this.launched) return;
  this.editCellClass = cls;
}

function ComboBox_getEditCellClass () {
  return this.editCellClass;
}

function ComboBox_setDropdownClass (cls) {
  if (this.launched) return;
  this.dropdownClass = cls;
}

function ComboBox_getDropdownClass () {
  return this.dropdownClass;
}

function ComboBox_getSelectedItem () {
  if (this.dropdown)
    return this.dropdown.getSelectedItem();
}

function ComboBox_objectRemovedCallback (obj) {
  if (this.built)
    this.editCell.setModelItem (null);
}

function ComboBox_actionMouseUp (modelItem, modelView)
{
  this.notifyListeners ('actionMouseUp', modelItem, modelView);
  return true;
}

function ComboBox_actionOnChange (modelItem, modelView)
{
  this.notifyListeners ('actionOnChange', modelItem, modelView);
  return true;
}

function ComboBox_hitTest (e)
{
  if (e && this.dropdown.opened) {
    return this.dropdown.hitTest (e)
  }
  return (null);
}

function ComboBox_moreDestroy ()
{
  if (this.editCell) {
    this.editCell.destroy ();
    this.editCell = null;
    delete this.editCell;
    
    this.btnImage.destroy ();
    this.btnImage = null;
    delete this.btnImage;
  }  
  if (this.dropdown) {
    this.dropdown.destroy ();
    this.drodown = null;
    delete this.dropdown;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
}

function ComboBox_setEventManager (em)
{
  this.eventManager = em;
}

function ComboBox_onKeyDownCallback (e, obj)
{
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 9:
    //tab key is pressed. Let the event bubble up
    if (this.dropdown.opened) {
      //bring the focus to the first visible item in the dropdown
    }
    return;
  case 38:
    this.keyboardScrollUp(false);
    break;
  case 40:
    this.keyboardScrollDown(false);
    break;
  case 46:
    //del key press. Remove this item from the dropdown and sent a event
    var curItem = this.editCell.getModelItem();
    if (curItem) {
      this.editCell.setModelItem(null);
      this.notifyListeners('actionMouseUp', curItem, curItem);      
    }
    break;
  }
  if (ie4) {
    e.returnValue = false;
  }
}

function ComboBox_keyboardScrollUp(bPgUp)
{
  var curItem = this.editCell.getModelItem();
  if (curItem) {
    var curIndex = curItem.index;
    if (curIndex == 0) {
      //we reached the first item no more to scroll up
      return;
    }
    var newIndex = 0;
    var newItem = null;
    if (bPgUp) {
      //TODO: do a page level scroll
    } else {
      //do a step scroll
      if (curIndex > 1)
        newIndex = curIndex-1;
    }
    while (newIndex >= 0) {
      var itm = this.listModel.items[newIndex];
      this.startTimer(itm);
      if (itm.isVisible()) {
        this.editCell.setModelItem(itm);
        break;
      } else
        newIndex--;
    }
  }
}

function ComboBox_keyboardScrollDown(bPgDown)
{
  var curItem = this.editCell.getModelItem();
  var len = this.listModel.items.length;
  var newIndex = 0;
  if (curItem) {
    var curIndex = curItem.index;
    if (curIndex == (len-1)) {
      //we reached end no more to scroll
      return;
    }
    if (bPgDown) {
      //TODO: do a page level scroll down

    } else {
      //do a step scroll
      newIndex = curIndex + 1;
    }
  }
  while (newIndex < len) {
    var itm = this.listModel.items[newIndex];
    this.startTimer(itm);
    if (itm.isVisible()) {
      this.editCell.setModelItem(itm);
      break;
    } else
      newIndex++;
  }
}

function ComboBox_startTimer(itm)
{
  if (this.timeoutHdl)
    clearTimeout (this.timeoutHdl);

  this.selItemModel = itm;
  var id = this.selItemModel.getName();
  this.timeoutHdl = setTimeout(this.self + '.synchSelection("'+id+'");', 500);
}

function ComboBox_synchSelection(id)
{
  if (this.selItemModel.getName() == id) {
    clearTimeout (this.timeoutHdl);
    //send event to server
    var itm = this.selItemModel;
    this.notifyListeners('actionMouseUp', itm, itm);
  }
}

HTMLHelper.importPrototypes(ComboBox,Base);

//public methods
ComboBox.prototype.setEditCellClass = ComboBox_setEditCellClass;
ComboBox.prototype.getEditCellClass = ComboBox_getEditCellClass;
ComboBox.prototype.setDropdownClass = ComboBox_setDropdownClass;
ComboBox.prototype.getDropdownClass = ComboBox_getDropdownClass;
ComboBox.prototype.addItem = ComboBox_addItem;
ComboBox.prototype.removeItem = ComboBox_removeItem;
ComboBox.prototype.removeAll = ComboBox_removeAll;
ComboBox.prototype.getSelectedIndex = ComboBox_getSelectedIndex;
ComboBox.prototype.setSelectedIndex = ComboBox_setSelectedIndex;
ComboBox.prototype.getSelectedItem = ComboBox_getSelectedItem;
ComboBox.prototype.getItemCount = ComboBox_getItemCount;
ComboBox.prototype.setBackgroundColor = ComboBox_setBackgroundColor;
ComboBox.prototype.getBackgroundColor = ComboBox_getBackgroundColor;	
ComboBox.prototype.setActiveBackColor = ComboBox_setActiveBackColor;
ComboBox.prototype.getActiveBackColor = ComboBox_getActiveBackColor;
ComboBox.prototype.setSelectedBackColor = ComboBox_setSelectedBackColor;
ComboBox.prototype.getSelectedBackColor = ComboBox_getSelectedBackColor;
ComboBox.prototype.setFont = ComboBox_setFont;
ComboBox.prototype.render = ComboBox_render;
ComboBox.prototype.hitTest = ComboBox_hitTest;
ComboBox.prototype.setEventManager = ComboBox_setEventManager;

//private methods
ComboBox.prototype.buildControl = ComboBox_buildControl;
ComboBox.prototype.moreSetWidth = ComboBox_moreSetWidth;
ComboBox.prototype.moreSetHeight = ComboBox_moreSetHeight;
ComboBox.prototype.moreCSS = ComboBox_moreCSS;
ComboBox.prototype.moreLaunch = ComboBox_moreLaunch;
ComboBox.prototype.mouseupCallback = ComboBox_mouseupCallback;
ComboBox.prototype.showDropDown = ComboBox_showDropDown;
ComboBox.prototype.hideDropDown = ComboBox_hideDropDown;
ComboBox.prototype.objectAddedCallback = ComboBox_objectAddedCallback;
ComboBox.prototype.propertyChangeCallback = ComboBox_propertyChangeCallback;
ComboBox.prototype.objectRemovedCallback = ComboBox_objectRemovedCallback;
ComboBox.prototype.setFontAttributes = ComboBox_setFontAttributes;
ComboBox.prototype.moreDestroy = ComboBox_moreDestroy;
ComboBox.prototype.keyboardScrollUp = ComboBox_keyboardScrollUp;
ComboBox.prototype.keyboardScrollDown = ComboBox_keyboardScrollDown;
ComboBox.prototype.startTimer = ComboBox_startTimer;
ComboBox.prototype.synchSelection = ComboBox_synchSelection;

ComboBox.prototype.docMouseUp = ComboBox_docMouseUp;
ComboBox.prototype.actionMouseUp = ComboBox_actionMouseUp;
ComboBox.prototype.actionOnChange = ComboBox_actionOnChange;
ComboBox.prototype.onKeyDownCallback = ComboBox_onKeyDownCallback;
ComboBox.prototype.updateDone = ComboBox_updateDone;
ComboBox.prototype.onLaunchCall = ComboBox_onLaunchCall;





