<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : hrmecmnw.js
// Description : This file contains all the common JavaScript routines
//               used in MEE modules.
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------- ----------   ------- ------- ---------------------------------
// bdivvela    06-May-2002  110.20 2265464  To print frame through JS, 
//					    we should focus the target frame
//					    before calling print.
// bdivvela    06-Dec-2001  110.19  2123092 Modified the function 
//                                          common_print_onClick enabling
//                                          printing in IE.
// lma         21-Dec-2000  110.18  1414461 Added checkNumericFields
//                                          and isNumber
// pbrimble    16-Mar-2000  110.16  1237692 Added p_mode parameter to the
//                                          reload_gif_onClick and
//                                          stop_gif_onClick functions and
//                                          updated ref in common_print_onClick
// pbrimble    01-Dec-1999  110.15          Moved main_menu_gif_onClick function
//                                          from embedded place in the PLSQL
//                                          packages to this library.  Updated
//                                          Print routines to support views -
//                                          print_gif_onClick now calls new
//                                          function common_print_onClick.
//
// jxtan       12-Nov-1999  110.14  1073692 fixed in reload_gif_onClick
//                                          fixed stop_gif_onClick
// lma         03-Nov-1999  110.13          Fix a bug in function
//                                          checkMandatoryFields.
// lbandaru    28-Oct-1999  110.12          Added checkModal() function to
//                                          get_gray_page and get_dark_blue_page
//                                          functions.
// fychu       14-Jul-1999  110.11  924954  Added errorExists variable for use
//                                          by hrasdspw.js.
//
// lbandaru    09-Jul-1999  110.10          Added submit_in_progress variable.
// lbandaru    02-Jul-1999  110.09          Removed PRINT_FLAG in prinf function
// rradhakr    02-Jul-1999  110.08          Added the US directory
// rradhakr    01-Jul-1999  110.07          Added help_gif_file function
// rajayara    30-JUN-1999  110.06          get_gray_page()
// lbandaru    22-Jun-1999  110.05          Corrected typo (contianer) in
//                                          clear_contianer_bottom_frame
// lbandaru    21-Jun-1999  110.04          Modified clear_contianer_bottom_frame
// fychu       21-Jun-1999  110.03          Added scrolling parameter to
//                                          openSubWindow function.
//
// lbandaru    21-Jun-1999  110.02          Modified the reload_gif_onClick
//                                          function to use soft reload.
// lbandaru    20-Jun-1999  110.01          Added functions for print, reload
//                                          and stop.
// lbandaru    17-Jun-1999  110.00          Created.
// ==========================================================================
// $Header: hrmecmnw.js 115.6 2002/05/06 05:21:07 pkm ship      $

// ------------------- Global Variables -------------------

// Detect  browser type and version
var Nav4 = ((navigator.appName == "Netscape") &&
            (parseInt(navigator.appVersion) == 4));
var browserVer = parseInt(navigator.appVersion);

// Flag to check whether we have submitted anything to server.
var submit_in_progress = "N";

// Flag to check whether we error exists which is detected by hrasdspw.js
var errorExists = "";

// Place holder for sub window.
var subWindow;

var Win32;

if (Nav4) {
  Win32 = ((navigator.userAgent.indexOf("Win") != -1) &&
           (navigator.userAgent.indexOf("Win16") == -1));
} else {
  Win32 = ((navigator.userAgent.indexOf("Windows") != -1) &&
           (navigator.userAgent.indexOf("Windows 3.1") == -1));
}

// ------------------- Function Definitions -------------------

// Function which does nothing. Used in print function.
function do_nothing () {
  return true;
}

function print_gif_onClick () {
  common_print_onClick ('STANDARD')
}

function do_print(p_mode)
{

      if (p_mode == 'STANDARD') 
      {
// fix 2265464 
top.contaier_middle.focus();        
top.container_middle.print();
      }
      else
      {
// fix 2265464 
top.container_middle_view.container_display.focus();        
top.container_middle_view.container_display.print();
      }

}


function common_print_onClick (p_mode) {
 
 // Bug fix 2123092  Start
 
  if (Nav4) {
             do_print(p_mode);  
            }
      else if (Win32)
           {
           do_print(p_mode);
           }
  
  else {
        alert (FND_MESSAGES['HR_CANNOT_PRINT_WEB']);
       }
   // Bug fix 2123092 End    
}


// This function will reload the the container middle frame.
function reload_gif_onClick (p_mode) {
    if (Nav4) {
      if (p_mode == 'STANDARD') {
        top.container_middle.history.go(0);
      } else {
        top.container_middle_view.container_display.history.go(0);
      }
    }
}


// This function will stop reloading the the container middle frame.
function stop_gif_onClick (p_mode) {
  if (Nav4) {
    if (p_mode == 'STANDARD') {
      void top.container_middle.stop();
    } else {
      void top.container_middle_view.stop();
    }

  }
  else if (Win32) {
    if (p_mode == 'STANDARD') {
    top.container_middle.focus();
    // requires same <OBJECT> as printing
    //IEControl.ExecWB(23, 0);
    } else {
    top.container_middle_view.focus();
    // requires same <OBJECT> as printing
    //IEControl.ExecWB(23, 0);
    }
  }
}


// This function will popup the help window.
var helpWindow;
function help_gif_onClick () {
 var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  p_url = '/OA_HTML/US/hrmeehlp.htm';
  p_scrolling='Y';

 if (!helpWindow||helpWindow.closed){
        helpWindow=window.open(p_url,"","status=yes,menubar=yes,scrollbars=yes,toolbar=yes,location=yes,resizable=yes,alwaysRaised=yes,HEIGHT=500,WIDTH=500");
       helpWindow.opener=window;
 }
 else {
     helpWindow.focus();
 }
}



// Function to generate a blank page with dark blue color.
function get_dark_blue_page () {
  return "<HTML><BODY BGCOLOR=#336699 onFocus=\"top.checkModal()\"></BODY></HTML>"
}

// Function to generate a blank page with gray color.
function get_gray_page () {
  return "<HTML><BODY BGCOLOR=#CCCCCC  onFocus=\"top.checkModal()\"></BODY></HTML>"
}


// This function will clear the container bottom frame so that buttons are
// not displayed when processing is going on in the container middle frame.
function clear_container_bottom_frame () {
  parent.container_bottom.document.open();
  parent.container_bottom.document.write (
    "<HTML>" +
    "<HEAD>" +
    "</HEAD>" +
    "<BODY BGCOLOR=#336699>" +
    "  <TABLE  WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>" +
    "  <TR BGCOLOR=#CCCCCC>" +
    "    <TD ALIGN=LEFT><IMG SRC=" + FND_IMAGES['FNDCTBL'] + " BORDER=0></TD>" +
    "    <TD WIDTH=100%><IMG SRC=" + FND_IMAGES['FNDPXG5'] + " BORDER=0></TD>" +
    "    <TD ALIGN=RIGHT><IMG SRC=" + FND_IMAGES['FNDCTBR'] + " BORDER=0></TD>" +
    "  </TR>" +
    "</TABLE>" +
    "</BODY>" +
    "</HTML>"
  );
  parent.container_bottom.document.close();
}


// Function to refresh container_top and container_bottom frames.
function refresh_frames (p_container_top_frame, p_container_bottom_frame) {
  if (p_container_top_frame) {
    if (top.container_top.location.href != p_container_top_frame) {
      top.container_top.location.href = p_container_top_frame;
    }
  }
  if (p_container_bottom_frame) {
    if (top.container_bottom.location.href != p_container_bottom_frame) {
      top.container_bottom.location.href = p_container_bottom_frame;
    }
  }
}


// Function to check whether the given string is blank.
function is_blank (str) {
  for (var i = 0; i < str.length; i++) {
    var c = str.charAt(i);
    if ((c != ' ') && (c != '\n') && (c != '\t'))
      return false;
  }
  return true;
}


//Required to open a sub window
function openSubWindow(p_url, p_width, p_height, p_scrolling) {
  var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  if (p_scrolling == "Y")
    {
     scrollbar_attr = ",scrollbars=yes";
    }
  else
    {
     scrollbar_attr = ",scrollbars=no";
    }

  if (!subWindow || subWindow.closed) {
    // center on the main window
    openleft = parseInt(window.screenX + ((window.outerWidth - p_width) / 2));
    opentop = parseInt(window.screenY + ((window.outerHeight - p_height) / 2));
    var attr = "screenX=" + openleft + ",screenY=" + opentop;
    attr += ",resizable=no,width=" + p_width + ",height=" + p_height +
            scrollbar_attr;

    subWindow = window.open(p_url, 'sub_window', attr);
    subWindow.opener = window;
  }
  else {
    subWindow.focus();
  }
}

function isNumber(num,decimal_point)
{
  var decimal = false ;
  var str ;

  str = num.toString() ;

  for ( var i=0; i<str.length ; i++ )
  {
    var onechar = str.charAt(i)
    if ( i==0 && onechar =="-")
    {
      continue ;
    }
    if (decimal_point != null) 
    {
      if ( onechar == decimal_point  && !decimal)
      {
      decimal=true ;
      continue ;
      }
    }
    if ( onechar <"0" || onechar >"9" )
    {
      return false ;
    }

  }
  return true ;
}

function checkNumericFields(input, numericFields, fieldPrompts, msg, decimalPoint) {
  var fieldCheck   = true;
  var fieldsNeeded = "\n" + msg + "\n\n\t";
  for(var fieldNum=0; fieldNum < numericFields.length; fieldNum++) {
     // Check if it is a multi-row form.
     var e = input.elements[numericFields[fieldNum]];
     var obj_length = e.length;
     var obj_value = e.value;
     if ( obj_length < 1 || obj_value != null) {
         if ((obj_value == "") || (obj_value == null) || is_blank(obj_value)) {
                ;
         }
         else if (isNumber(obj_value, decimalPoint) == false) {
                        fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                        fieldCheck = false;
                     }
     } else {
                 for ( var i = 0; i < e.length; i++) {
                   if (e[i].selected) {
                     if ((e[i].value == "") ||
                            (e[i].value == null) || is_blank(e[i].value)) {
                        continue;
                     }
                     if (isNumber(e[i].value, decimalPoint) == false) {
                        fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                        fieldCheck = false;
                     }
                     break;
                   }
                }
             }
     }
     // ALL REQUIRED FIELDS HAVE BEEN ENTERED
     if (fieldCheck == true) {
         return true;
     }
     // SOME REQUIRED FIELDS ARE MISSING VALUES
     alert(fieldsNeeded);
     return false;
 }


function checkMandatoryFields(input, requiredFields, fieldPrompts, msg) {
  var fieldCheck   = true;
  var fieldsNeeded = "\n" + msg + "\n\n\t";
  for(var fieldNum=0; fieldNum < requiredFields.length; fieldNum++) {
     // Check if it is a multi-row form.
     var e = input.elements[requiredFields[fieldNum]];
     var obj_length = e.length;
     var obj_value = e.value;
     if ( obj_length < 1 || obj_value != null) {
         if ((obj_value == "") || (obj_value == null) || is_blank(obj_value)) {
                fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                fieldCheck = false;
         }
     } else {
                 for ( var i = 0; i < e.length; i++) {
                   if (e[i].selected) {
                     if ((e[i].value == "") ||
                            (e[i].value == null) || is_blank(e[i].value)) {
                        fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                        fieldCheck = false;
                     }
                     break;
                   }
                }
             }
     }
     // ALL REQUIRED FIELDS HAVE BEEN ENTERED
     if (fieldCheck == true) {
         return true;
     }
     // SOME REQUIRED FIELDS ARE MISSING VALUES
     alert(fieldsNeeded);
     return false;
 }




// Roll over image functions for onMouseOver and onMouseOut JavaScript events.
function set_onMouse_gif (p_document_name, p_image_name, p_gif_file) {
  if (browserVer >= 3 ) {
    eval(p_document_name + '.' + p_image_name + '.src=' + '"' + p_gif_file+'"');
  }
  else {
    return true;
  }
}

// The following function is invoked from the
// header frame when the user clicks on the
// ''Main Menu'' gif in the toolbar.
function main_menu_gif_onClick (p_msg) {
  top.location.href = 'hr_util_disp_web.hr_main_menu_page';
}

<!-- Done hiding from browsers which cannot handle JavaScript. -->
