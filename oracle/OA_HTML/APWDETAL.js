//  +================================================+
//  |    Copyrightx(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWDETAL.js                       |
//  |                                                |
//  | Description: Client-side Javascript functions  |
//  |              for Detail window		     |     
//  |                                                |
//  | History:     Created 03/08/2000	             |
//  |              		                     |
//  |                                                |
//  +================================================+
/* $Header: APWDETAL.js 115.55 2001/02/13 13:01:18 pkm ship  $ */

function fOpenDetail(strTab,nLine) {
	var iRecIndex = top.objExpenseReport.getNthReceipt(nLine);
	if ((top.g_winDetail == null) || (top.g_winDetail.closed)){
           top.g_winDetail=open('','Receipt_Detail','resizable=yes,menubar=no');
          	if (top.g_winDetail.opener == null) top.g_winDetail.opener = self; 
          	else {}
	}
	top.g_bDetailFrameLoaded = false;
	top.fGenerateDetailWindow();
	top.g_winDetail.iRecIndex = iRecIndex;
	top.g_winDetail.iLineNum = nLine;
	top.fGenerateDetailBorder();
	if (isWindows95 && isIE5) {
	   top.fGenBlueBorder('g_winDetail','leftborder');
	   top.fGenBlueBorder('g_winDetail','rightborder');
	   top.fGenerateFlexHeader();
	}
	top.fGenerateDetailBody(iRecIndex,nLine);
	top.fGenerateDetailButtons();
	top.fFocusOnLine(nLine);
	if (iRecIndex<0) iRecIndex = -1;
	if (strTab == 'OOP' || (strTab == 'CC' && top.framMain.lines.g_iCurrentLine==nLine) && (nLine != 1)) { //For CC, fPopulateReceiptDetail is called in fFocusOnLine unless top.framMain.lines.g_iCurrentLine==nLine) && (nLine != 1)
	     top.g_winDetail.bRunPopDetail = true;
	     top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.fPopulateReceiptDetail('+iRecIndex+')');
	} //end if fIsOOP()
        top.fUpdatePopShowReceipt();
        top.g_winDetail.focus();      
	
} //end function fOpenDetail

function fCloseDetail() { 
	if (!top.Nav4) top.fGetObjDetail().fDetailIEOnChangeTrigger();
	top.g_strPreviousExpType = "-1";
	if (g_winDetail.close) g_winDetail.close();
}

function fGenerateFlexHeader(){
d = top.g_winDetail.framFlexHeader.document;
d.open();
d.writeln(top.fFlexHeader('cccccc'));
d.close();
}

function fGenerateDetailBorder(){
d = top.g_winDetail.top_border.document;
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
top.fGenerateStyleCodes(eval(top.g_winDetail.top_border));
d.writeln("<BODY class=color3>");
d.writeln("<CENTER>");
d.writeln("<br>");
d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=color3>");
d.writeln("<TR class=colorg5>");
d.writeln("		<TD ROWSPAN=2 valign=top align=left width=1><IMG SRC="+top.g_strImgDir+"APWCTTL.gif></TD>");
d.writeln("		<TD class=color6 Width=1000 HEIGHT=1><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD>");
d.writeln("		<TD ROWSPAN=2 valign=top width=1><IMG SRC="+top.g_strImgDir+"APWCTTR.gif></TD>");
d.writeln("</TR>");
d.writeln("<TR>");
d.writeln("		<TD height=100 class=colorg5><img src="+top.g_strImgDir+"APWDBPXC.gif></TD>");
d.writeln("</TR>");
d.writeln("</TABLE>");
d.writeln("</CENTER>");
d.writeln("</BODY>");
d.writeln("</HTML>");
d.close();

d = top.g_winDetail.flexFields.document;
d.open();
d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
top.fGenerateStyleCodes(eval(top.g_winDetail.flexFields));
d.writeln("<BODY class=colorg5>");
d.writeln("</BODY>");
d.close();
}


function fChangeReceiptDetail(nLine) {
	var iRecIndex = top.objExpenseReport.getNthReceipt(nLine);
	r = top.fGetReceiptObj(iRecIndex);
        top.fFocusOnLine(nLine,"");
	top.g_winDetail.iLineNum = nLine;
	top.g_winDetail.iRecIndex = iRecIndex;
	iLineNum = nLine;
        top.framMain.lines.g_iCurrentLine = nLine;

	top.fGenerateDetailBody(iRecIndex,nLine);
	top.g_winDetail.bRunPopDetail = true;
	top.fPopulateReceiptDetail(iRecIndex); 
	top.fUpdatePopShowReceipt();

} //end function fChangeReceiptDetail

function fPopulateReceiptDetail(iRecIndex){
	if (!top.g_winDetail.bRunPopDetail) return;
	else top.g_winDetail.bRunPopDetail = false;
	var i;
	if (top.g_winDetail != null){
	  if (!top.g_winDetail.closed){
            var t = top.fGetObjDetail().framReceiptDetail.document.formDetail;
	    var strExpTypeId = "";
	    var tempPopList = eval('top.g_winDetail.detailButtonsFrame.document.formDetailButtons.popShowReceipt'); 
	    var objTarget = top.fGetReceiptObj(iRecIndex);
	    if (objTarget){
		top.g_winDetail.iRecIndex = iRecIndex; 
		var curr = objTarget.getReceiptCurr();
		// Populate Receipt or Credit Card info here
        	t.txtDate.value = objTarget.getDate();
		t.txtReceiptRate.value = top.fMoneyFormat(objTarget.getDailyRate()+"",objTarget.getReceiptCurr());
		t.txtOccurs.value = objTarget.getOccurs();
		t.txtJustification.value = objTarget.getJustification();
		if (objTarget.getGroup())
		      t.txtGroup.value = objTarget.getGroup();

		if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled()){
		  // for bug 1363873
		  if (t.AP_WEB_PA_PROJECT_NUMBER)	t.AP_WEB_PA_PROJECT_NUMBER.value = objTarget.getNAProjectNumber();
		  if (t.AP_WEB_PA_TASK_NUMBER) t.AP_WEB_PA_TASK_NUMBER.value = objTarget.getNATaskNumber();
		} // if employee is project enabled
		
		if (top.fIsOOP()) {
		   t.txtSubtotal.value = top.fMoneyFormat(objTarget.getExpensedAmount(),curr)+"";
		   t.popReceiptCurrency.selectedIndex = top.fGetCurrencyIndex(objTarget.getReceiptCurr());
		} //end if (top.fIsOOP()) 
		i = 0;
		if (objTarget.getObjExpenseType()){
		  i = fGetSelectedIndex(t.popExpType.options, objTarget.getObjExpenseType().mGetstrName());
		  strExpTypeId = objTarget.getObjExpenseType().mGetID();
		}
		if (i>0) top.g_winDetail.fDetailSetExpenseType(i);
		top.g_winDetail.fDetailSetReceiptMissing(objTarget.getIsMissing());

		var bIsReq = 'N';
		if (objTarget.getObjExpenseType()) bIsReq = objTarget.getObjExpenseType().mGetbJustifRequired();
     		if (bIsReq == 'Y')
        	    top.fDetailSetReqImg();
        	else top.g_winDetail.framReceiptDetail.document.reqimg.src = top.g_strImgDir+"APWPXG5.gif";

		// for bug 1318730
		if (top.g_winDetail.framReceiptDetail.document.tax_reqimg){
		  // for bug 1303435
		  if (objTarget.getAmountIncludesTax())
			top.g_winDetail.framReceiptDetail.document.tax_reqimg.src = top.reqimg1.src;
		  else 
			top.g_winDetail.framReceiptDetail.document.tax_reqimg.src = top.g_strImgDir+"APWPXG5.gif";
		}

		if (top.fIsOOP()) {
		   t.txtReimbursAmount.value = top.fMoneyFormat(objTarget.getReimbursAmount(),curr)+"";
		   var action = top.fDetermineConversion(objTarget.getReceiptCurr(), top.objExpenseReport.header.getReimbursCurr(),objTarget);
		   if ((action == top.g_reimbEurRecFixed) || (action == top.g_reimbFixedRecEur) || (action == top.g_reimbFixedRecFixed)) 
		     t.txtExchRate.value = top.g_objMessages.mstrGetMesg("AP_WEB_FIXED");
    		   else t.txtExchRate.value = objTarget.getExchRate();
		} //end if (top.fIsOOP())

		// Populate Tax info here
	        if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_TAX_ENABLE')=='Y'){
		   top.fPopulateTaxValues(iRecIndex);
		}

	  	if (!top.fIsOOP()) { //ccard
		  // Populate DescFlex info here
		  if (top.g_bDescFlexEnable){
		    if (!objTarget.flexFields || objTarget.flexFields.length == 0)  {
			  objTarget.flexFields = new Array();
			  if (objTarget.getObjExpenseType())
			     fAddFlexFields(objTarget,objTarget.getObjExpenseType().mGetstrCode());
		    } //end if !objTarget.flexFields
		    var expType = t.popExpType.options[t.popExpType.selectedIndex].value;
		    if (expType) 
			  top.fWaitUntilFrameLoaded('top.g_bFlexFrameLoaded','top.fPaintFlex(' + expType + ', ' + iRecIndex + ')');
		    else {
                          if (top.g_firstOpenDetail == 1)
                             top.g_firstOpenDetail = 2;
                          else 
			     top.fWaitUntilFrameLoaded('top.g_bFlexFrameLoaded','top.fPaintFlex("", ' + iRecIndex + ')');
                    }
	  	  }   //end if top.g_bDescFlexEnabled
		}//end if ccard
	 }// if (objTarget)
	 else 
	 {
		t.txtDate.value = "";
		t.txtReceiptRate.value = "";
		t.txtOccurs.value = "";
		t.txtJustification.value = "";

		if (top.fIsOOP()) {
		   t.txtSubtotal.value = "";
		   var ind = top.fGetCurrencyIndex(top.objExpenseReport.header.getReimbursCurr());
		   if (ind < 0) ind = 0;

		   t.popReceiptCurrency.selectedIndex = ind;
		   t.txtReimbursAmount.value = "";
     		   t.txtExchRate.value = "";
		}
		t.txtGroup.value = "";
		t.popExpType.selectedIndex = 0;
		top.g_winDetail.fDetailSetReceiptMissing(false);
		//populate project task info
		if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled()){
		   t.AP_WEB_PA_PROJECT_NUMBER.value = "";
		   t.AP_WEB_PA_TASK_NUMBER.value = "";
		}

		// Populate Tax info here
		if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_TAX_ENABLE')=='Y'){
		  top.fPopulateTaxValues(iRecIndex);
		}
		//Bug 1486715: no longer needed fPaintFlex, which calls fDrawFlexBlank
		//This is called in fOpenDetail and fDetailAddReceipt
	 } //end else 
	} //end if (!top.g_winDetail.closed)
       } //end if (top.g_winDetail != null)
} //end function fPopulateReceiptDetail


function fPrevOrNextReceipt(bNext){
   if (!top.Nav4) top.fGetObjDetail().fDetailIEOnChangeTrigger();
   var nLine;
   if (bNext)
      nLine = top.g_winDetail.iLineNum + 1;
   else
      nLine = top.g_winDetail.iLineNum - 1;
   var iRecIndex = top.objExpenseReport.getNthReceipt(nLine);
   r = top.fGetReceiptObj(iRecIndex);

   if (iRecIndex>=0){
	fChangeReceiptDetail(nLine);
  } //end if (iRecIndex>=0)
   else {
     if (bNext)
        alert(top.g_objMessages.mstrGetMesg("AP_WEB_AT_LAST_RECEIPT"));
     else
        alert(top.g_objMessages.mstrGetMesg("AP_WEB_AT_FIRST_RECEIPT"));
     top.g_winDetail.focus();
   } //end else (iRecIndex>=0)
} //end function fPrevOrNextLine

function fChangePopShowReceipt(){
     if (top.g_winDetail != null){
      if (!top.g_winDetail.closed){
	if (!top.Nav4) top.fGetObjDetail().fDetailIEOnChangeTrigger();
     var tempPopList = eval('top.g_winDetail.detailButtonsFrame.document.formDetailButtons.popShowReceipt');
     var nPopShowReceiptIndex = tempPopList.selectedIndex+1;
     // Store flex field values in receipt object, move this logic to onChange for each flex field
	top.fFocusOnLine(nPopShowReceiptIndex,"");
	top.g_winDetail.iLineNum = nPopShowReceiptIndex;
	top.g_winDetail.iRecIndex = top.objExpenseReport.getNthReceipt(nPopShowReceiptIndex);
        var r = top.fGetReceiptObj(top.g_winDetail.iRecIndex);
	top.fGenerateDetailBody(top.g_winDetail.iRecIndex,top.g_winDetail.iLineNum);
	if (fIsOOP()){
	  top.g_winDetail.bRunPopDetail = true;
	  top.fPopulateReceiptDetail(top.g_winDetail.iRecIndex);
	}
	else{
	  top.g_winDetail.bRunPopDetail = true;
	  top.fWaitUntilFrameLoaded('top.g_bDetailFrameLoaded','top.fPopulateReceiptDetail(top.g_winDetail.iRecIndex)');
	}
	top.fUpdatePopShowReceipt();
       }
     }

}



function fUpdatePopShowReceipt(){
     if (top.g_winDetail != null)
      if (!top.g_winDetail.closed){
     	var tempPopList = eval('top.g_winDetail.detailButtonsFrame.document.formDetailButtons.popShowReceipt'); 
     	tempPopList.selectedIndex = top.g_winDetail.iLineNum - 1; 
     
     }
}


function fGenerateDetailButtons(){
	var frameTarget = eval('top.g_winDetail.detailButtonsFrame');
	if (top.g_winDetail != null){
	if (!top.g_winDetail.closed){
        var d = eval('top.g_winDetail.detailButtonsFrame.document');
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_DETAIL_BUTTONS');
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<html dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(frameTarget);
	d.writeln("<Script Language=\"JavaScript\">");
	d.writeln("var detailButtonsFrameLoaded = false;");
	d.writeln("<\/Script>");
        d.writeln("</HEAD>");
	d.writeln("<BODY  onLoad=\" self.defaultStatus=top.OAW_message; return true;\" class=color3 onResize = \"history.go(0)\" onFocus=\"top.opener.top.fCheckModal();\">");
	d.writeln("<form name=formDetailButtons>");
	d.writeln("<TABLE width=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>");
	d.writeln("<TR class=colorg5>");
	d.writeln("  <TD align=left valign=bottom><IMG SRC="+top.g_strImgDir+"APWCTBL.gif height=5 width=5></TD>");
	d.writeln("  <TD>");
	d.writeln("  <table width=90% cellpadding=0 cellspacing=0 border=0 align=right>");
	d.writeln("<tr>");
        d.writeln("<td rowspan=3 class=colorg5><img src="+top.g_strImgDir+"APWPXG5.gif height=5 width=5></td>");
	d.writeln("<td colspan=4 class=colorg5 height=5 width=1000><img src="+top.g_strImgDir+"APWPXG5.gif height=2></td>");
	d.writeln("<td rowspan=3 class=colorg5><img src="+top.g_strImgDir+"APWPXG5.gif height=5 width=5></td>");
	d.writeln(" </tr>");
	 d.writeln("<tr>");
        d.writeln("<td class=colorg5 nowrap><img src="+top.g_strImgDir+"APWPXG5.gif height=33 width=1></td>");
	d.writeln("<td class=colorg5 align=left>");
	d.writeln("<table cellpadding=0 cellspacing=0 border=0>");
	d.writeln("  <tr>");
	
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("     <td></td>");
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("     <td width=5 rowspan=5></td>");
	if (top.fIsOOP()){
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("     <td></td>");
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("     <td width=5 rowspan=5></td>");
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("     <td></td>");
	d.writeln("     <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("     <td width=10 rowspan=5></td>");
	}
	d.writeln("     <td width=5 rowspan=5></td>");
	d.writeln("  </tr>");
        d.writeln("  <tr>");
        
	d.writeln("     <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	if (top.fIsOOP()){
	d.writeln("     <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln("     <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	
	}
	d.writeln("  </tr>");
        d.writeln("  <tr>");
	if (fIsOOP()){
	d.writeln("    <td class=colorg5 height=20 nowrap><a href=\"javascript:top.opener.top.fDuplicateLines(true);\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_DUPLICATE_RECEIPT').mstrGetPrompt()+"</font></td>");
	d.writeln("    <td class=colorg5 height=20 nowrap><a href=\"javascript:top.opener.top.g_winDetail.fDetailClearReceipt();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CLEAR_RECEIPT').mstrGetPrompt()+"</font></td>");
	d.writeln("    <td class=colorg5 height=20 nowrap><a href=\"javascript:top.opener.top.g_winDetail.fDetailAddReceipt();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_ADD_RECEIPT').mstrGetPrompt()+"</font></td>");
	}
	else {
	d.writeln("    <td class=colorg5 height=20 nowrap><a href=\"javascript:top.opener.top.fRemoveReceipts(true,true);\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CLEAR_RECEIPT').mstrGetPrompt()+"</font></td>");
	
	}
	d.writeln(" </tr>");
        d.writeln(" <tr>");
	d.writeln("    <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	if (top.fIsOOP()){
	d.writeln("    <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln("    <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	}
	d.writeln(" </tr>");
	d.writeln(" <tr>");
	d.writeln("   <td></td>");
	if (top.fIsOOP()){
	d.writeln("   <td></td>");
	d.writeln("   <td></td>");
	}
	d.writeln(" </tr>");
	d.writeln(" </table>");
	d.writeln(" </td>");



	d.writeln(" <TD width=15% align=center>");
	d.writeln("     <TABLE cellpadding=0 cellspacing=0 border=0>");
	d.writeln("	  <TR>");
        d.writeln("	      <TD colspan=3 align=center>");
	d.writeln("		<font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_SHOW_RECEIPT').mstrGetPrompt()+"</font>");
	d.writeln("	      </TD>");
	d.writeln("	  </TR>");
	d.writeln("	  <TR><TD align=right>");
	d.writeln("		<a href=\"javascript:top.opener.top.fPrevOrNextReceipt(false);\"><img src="+top.g_strImgDir+"APWARRWL.gif border=0></a>");
	d.writeln("		</TD>");
	d.writeln("	  	<TD>");
	d.writeln("	    	<select name=popShowReceipt onChange=\"top.opener.top.fChangePopShowReceipt();\">");
	for (var i=1;i<=top.objExpenseReport.getTotalReceipts(top.g_strCurrentTab);i++){
	d.write("		<option value='"+i+"'>"+i);
	}
	
	d.writeln("		</select></font>");
	d.writeln("	      	</TD>");
	d.writeln("       	<TD align=left>");
	d.writeln("<a href=\"javascript:top.opener.top.fPrevOrNextReceipt(true);\"><img src="+top.g_strImgDir+"APWARRWR.gif border=0></a>");
	d.writeln("	    </TD></TR></TABLE>");
	
	
	d.writeln(" </TD>");



	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("    <td colspan=3 class=colorg5><img src="+top.g_strImgDir+"APWPXG5.gif height=1></td>");
	d.writeln("  </tr>");
	d.writeln("</TABLE>");
	d.writeln("</TD><TD align=right valign=bottom><IMG SRC="+top.g_strImgDir+"APWCTBR.gif height=5 width=5></TD>");
	d.writeln("</TR>");
	d.writeln("</TABLE>");
	d.writeln("<TABLE width=100% cellpadding=0 cellspacing=0 border=0>");
	d.writeln("    <TR><td height=5></td></TR>");
	d.writeln("    <TR>");
	d.writeln("    <TD align=right>");
	d.writeln("    <table cellpadding=0 cellspacing=0 border=0>");
	d.writeln("      <tr>");
	d.writeln("       <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("        <td></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("       <td width=15 rowspan=5></td>");
	d.writeln("       <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDL.gif></td>");
	d.writeln("        <td></td>");
	d.writeln("        <td rowspan=5><img src="+top.g_strImgDir+"APWBRNDR.gif></td>");
	d.writeln("        <td width=3 rowspan=5></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("       <td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln("<td class=color6><img src="+top.g_strImgDir+"APWPX6.gif></td>");
	d.writeln(" </tr>");
	d.writeln(" <tr>");
	d.writeln("      <td class=colorg5 height=20 nowrap><a href=\"javascript: this.window.close(); parent.window.opener.focus();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_VIEW_LIST').mstrGetPrompt()+"</font></td>");
	d.writeln("<td class=colorg5 height=20 nowrap><A href=\"javascript:top.opener.top.fCloseDetail();\"><font class=buttontext>"+tempRegion.mobjGetRegionItem('AP_WEB_CLOSE').mstrGetPrompt()+"</font></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("        <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln(" <td class=colorg3><img src="+top.g_strImgDir+"APWPXG3.gif></td>");
	d.writeln("  </tr>");
	d.writeln("  <tr>");
	d.writeln("       <td></td>");
	d.writeln("	 <td></td>");
	d.writeln("  </tr>");
	d.writeln("  </table></td>");
	d.writeln("</tr>");
	d.writeln("</table>");

	d.writeln("</form>");
	d.writeln("</BODY>");
	d.close();
	}
	}
}

function fDetailSetReqImg() {
  top.g_winDetail.framReceiptDetail.document.reqimg.src = top.reqimg1.src;
} //end function fDetailSetReqImg

function fGenDetailFuncs(){
	var d = eval('top.g_winDetail.document');
/* ----------------------------
-- Detail Change Occurs
----------------------------- */

	d.writeln("	function fDetailChangeOccurs(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
      	d.writeln("	if (top.opener.top.fIsOOP()) var txtReceiptAmount = framReceiptDoc.formDetail.txtSubtotal;");
      	d.writeln("	var txtReceiptRate =framReceiptDoc.formDetail.txtReceiptRate;");
      	d.writeln("	var txtReceiptOccurs = framReceiptDoc.formDetail.txtOccurs;");
	d.writeln("	var r = null;");
	
      	d.writeln("	var curr = \"\";");
      	d.writeln("	if (value!=\"\"){");
        d.writeln("	   if ((!top.opener.top.fCheckPosNum(value, true)) || (!top.opener.top.fIsInt(value)))");
	d.writeln("	   {");
	d.writeln("	  	txtReceiptOccurs.focus();");
	d.writeln("	  	txtReceiptOccurs.value = \"\";");
	d.writeln("	  	return;");
	d.writeln("	   }");
	d.writeln("	   else{");
	d.writeln("	  	top.opener.top.fUpdateLineStatus(top.iLineNum,false);");
	d.writeln("		r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
  	d.writeln("		var occurs = Math.round(value);");
	d.writeln("		txtReceiptOccurs.value = occurs;");
	d.writeln("		r.setOccurs(occurs);");
	d.writeln("  		curr = r.getReceiptCurr();");
	d.writeln("  		if (txtReceiptRate.value != \"\"){");
 	d.writeln("  		  if (top.opener.top.fIsOOP()){");
	d.writeln("  		    txtReceiptAmount.value = top.opener.top.fMoneyFormat(top.opener.top.fMultiply(eval(txtReceiptRate.value),eval(r.getOccurs())),curr);");
	d.writeln(" 		    r.setExpensedAmount(txtReceiptAmount.value);");
	d.writeln("		    if (r.itemizeId == null)");
//	d.writeln("		       r.setReceiptAmount(r.getReimbursAmount());");
	d.writeln("		       r.setReceiptAmount(r.getExpensedAmount());");
	d.writeln("  		    top.fDetailChangeReceiptAmount(r.getExpensedAmount(),top.iLineNum);");
	d.writeln("		  }");
	d.writeln("  		  else { // fix expensed amount, change daily rate");
	d.writeln("		    top.fDetailSetRate(top.opener.top.fMoneyFormat(r.getDailyRate()+\"\",curr));");
	d.writeln("  		  } //end else");
				
	d.writeln("  		} //end if (txtReceiptRate.value != \"\")");
	d.writeln("	  }// else");
      	d.writeln("     } //end if (value!=\"\")");
	d.writeln("	}// end of function");

	d.writeln("	function fDetailSetOccurs(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtOccurs.value = value;");
	d.writeln("	}");
	

 
/* -----------------------------
-- Detail Change Date
----------------------------- */

	d.writeln("	function fDetailChangeDate(value){");
	d.writeln("	var txtDetailDateField = framReceiptDetail.document.formDetail.txtDate;");
      	d.writeln("	var r = null;");
     	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var objDate = null;");
	d.writeln("     objDate = top.opener.top.fStringToDate(txtDetailDateField.value,true);");
     	d.writeln("	if (txtDetailDateField.value != \"\"){");
       	d.writeln("		if (objDate != null){");
	d.writeln("		  top.opener.top.g_strDay = objDate.getDate();");
       	d.writeln("		  top.opener.top.g_strMonth = objDate.getMonth();");
       	d.writeln("		  top.opener.top.g_strYear = objDate.getFullYear();");
	d.writeln("		  r.setDate(txtDetailDateField.value);")
       	d.writeln("		}");
       	d.writeln("		else{");
	d.writeln("		  txtDetailDateField.value = \"\";");
	d.writeln("	          txtDetailDateField.focus();");
	d.writeln("		  txtDetailDateField.select(); ");
	d.writeln("		}");
 	d.writeln("	top.opener.top.framMain.lines.fSetDate(txtDetailDateField.value,top.iLineNum);");
	d.writeln("	}");
        d.writeln("}");

	d.writeln("	function fDetailSetDate(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtDate.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Receipt Amount
----------------------------- */
	if (top.fIsOOP()){ // this function only generated for OOP since receipt amount in CC detail window is display only
	d.writeln("function fDetailChangeReceiptAmount(value){");
	d.writeln("if (top.opener.top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_CREDIT_LINES')=='N'){");
	d.writeln("if (!top.opener.top.fCheckPosNum(value,false)){");
	d.writeln("var nTempAmount = top.opener.top.fMoneyFormat(\"0\",top.opener.top.fGetObjHeader().getReimbursCurr());");
	d.writeln("  fDetailSetReceiptAmount(nTempAmount,top.iLineNum);");
	d.writeln("  return;");
	d.writeln("  }");
	d.writeln("}");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	var r = null;");
	d.writeln("	var curr = \"\";");
     	d.writeln("	var txtReceiptAmount = eval(framReceiptDoc.formDetail.txtSubtotal);");
    	d.writeln("	if (value != \"\") {");
      	d.writeln("	if (!top.opener.top.fCheckNum(value, false)){");
        d.writeln("		txtReceiptAmount.focus();");
        d.writeln("		return;");
      	d.writeln("	}");
      	d.writeln("	else{// user entered a value");
	d.writeln("	  var reimbursableField = eval(framReceiptDoc.formDetail.txtReimbursAmount);");
        d.writeln("	  top.opener.top.fUpdateLineStatus(top.iLineNum,false);");
	d.writeln("	  r = eval(top.opener.top.objExpenseReport.oopReceipts[top.iRecIndex]);");
	d.writeln("	  curr = r.getReceiptCurr();");
	d.writeln("	  txtReceiptAmount.value = top.opener.top.fMoneyFormat(value,curr);");
	d.writeln("	if (framReceiptDoc.formDetail.txtOccurs.value != \"\") ");
	d.writeln("	  fDetailSetRate(top.opener.top.fMoneyFormat(value/framReceiptDoc.formDetail.txtOccurs.value,curr));");

	d.writeln("	  r.setExpensedAmount(value);");
//	d.writeln("	  if (r.itemizeId == null) r.setReceiptAmount(r.getReimbursAmount());");
	d.writeln("	  if (r.itemizeId == null) r.setReceiptAmount(value);");
	d.writeln("	    txtReceiptAmount.focus();");
        d.writeln("	    top.opener.top.framMain.lines.fSetReceiptAmount(r.getExpensedAmount(),top.iLineNum);");
	d.writeln("	    var reimbursCurr = top.opener.top.objExpenseReport.header.getReimbursCurr();");
	d.writeln("	    var strTotal = top.opener.top.fCalcReimbursAmount(top.iRecIndex) + \"\";");
	d.writeln("	    r.setReimbursAmount(strTotal);");
	d.writeln("	    reimbursableField.value = top.opener.top.fMoneyFormat(strTotal,reimbursCurr);");
	d.writeln("	    top.opener.top.fPopulateLineTotal(top.iLineNum);");
      	d.writeln("	  } //end else");
    	d.writeln("	} //end if (value != \"\")");
	d.writeln("} //end function fDetailChangeReceiptAmount");

	d.writeln("	function fDetailSetReceiptAmount(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtSubtotal.value = value;");
	d.writeln("	}"); 
	

	d.writeln("	function fDetailSetReimbursAmount(value){");
	d.writeln("	   var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	   framReceiptDoc.formDetail.txtReimbursAmount.value =  top.opener.top.fMoneyFormat(value,top.opener.top.fGetObjHeader().getReimbursCurr());");
	d.writeln("	} //end function fDetailSetReimbursAmount"); 
	}


/* -----------------------------
-- Detail Change Rate
----------------------------- */

	d.writeln("function fDetailChangeRate(value){");
	d.writeln("if (top.opener.top.g_objProfileOptions.mvGetProfileOption('AP_WEB_ALLOW_CREDIT_LINES')=='N'){");
	d.writeln("if (!top.opener.top.fCheckPosNum(value,false)){");
	d.writeln("var nTempAmount = top.opener.top.fMoneyFormat(\"0\",top.opener.top.fGetObjHeader().getReimbursCurr());");
	d.writeln("  fDetailSetRate(nTempAmount);");
	d.writeln("  return;");
	d.writeln("  }");
	d.writeln("}");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	var r = null;");
	d.writeln("	var curr = \"\";");
      	d.writeln("	if (top.opener.top.fIsOOP()) var txtReceiptAmount = eval(framReceiptDoc.formDetail.txtSubtotal);");
      	d.writeln("	var txtReceiptRate = eval(framReceiptDoc.formDetail.txtReceiptRate);");
      	d.writeln("	var txtReceiptOccurs = eval(framReceiptDoc.formDetail.txtOccurs);");
	d.writeln("		top.opener.top.fUpdateLineStatus(top.iLineNum,false);");
	d.writeln("		r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("		curr = r.getReceiptCurr();");
	d.writeln("	  	if (txtReceiptOccurs.value == \"\"){");
	d.writeln("    		txtReceiptOccurs.value = '1';");
	d.writeln("    		r.setOccurs(txtReceiptOccurs.value);");
	d.writeln("  	  	}");
	d.writeln("  		if (top.opener.top.fIsOOP()){");
	d.writeln("  		  txtReceiptAmount.value = top.opener.top.fMoneyFormat(top.opener.top.fMultiply(eval(value), eval(r.getOccurs())),curr);");
	d.writeln("  		  r.setExpensedAmount(txtReceiptAmount.value);");
//	d.writeln("		  if (r.itemizeId == null) r.setReceiptAmount(r.getReimbursAmount());");
	d.writeln("		  if (r.itemizeId == null) r.setReceiptAmount(r.getExpensedAmount());");
        d.writeln("		  top.fDetailChangeReceiptAmount(r.getExpensedAmount(),top.iLineNum);");
	d.writeln("		}");
	d.writeln("  		else { //ccard");
	d.writeln(" 		  if (value != 0){");
	d.writeln("			  r.setOccurs(r.getExpensedAmount()/value);");
	d.writeln("			  top.fDetailSetOccurs(top.opener.top.fMoneyFormat(r.getOccurs()+\"\",curr));");
	d.writeln("  		  }");
	d.writeln("  		} //end else");
	d.writeln("}");

	d.writeln("	function fDetailSetRate(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtReceiptRate.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Receipt Currency
----------------------------- */
	d.writeln("function fDetailChangeReceiptCurr(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	var r = null;");
	d.writeln("	var popCurrency = eval(framReceiptDoc.formDetail.popReceiptCurrency);");
     	d.writeln("	var strCurrency = popCurrency.options[popCurrency.selectedIndex].value;     ");
	d.writeln("	var txtReceiptAmount = eval(framReceiptDoc.formDetail.txtSubtotal);");
     	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	r.setReceiptCurr(strCurrency);");
	d.writeln("	r.setExchRate(1);");
	d.writeln("	var tempRate = top.opener.top.fGetAndSetExchRate(r);");
	d.writeln("	fDetailSetExchRate(tempRate);");
	d.writeln("	r.setReimbursAmount(top.opener.top.fCalcReimbursAmount(top.iRecIndex));");
	d.writeln("	fDetailSetReimbursAmount(r.getReimbursAmount());");
	d.writeln("	top.opener.top.framMain.lines.fSetReimbursAmount(r.getReimbursAmount(),top.iLineNum);");
      	//d.writeln("	top.fDetailChangeReceiptAmount(txtReceiptAmount.value,top.iRecIndex,top.iLineNum);");
	d.writeln("	if (top.opener.top.fIsOOP()) top.opener.top.framMain.lines.fSetCurrency(popCurrency.selectedIndex,top.iLineNum);");
	d.writeln("}");

	d.writeln("function fDetailSetReceiptCurr(selectedIndex){");
	d.writeln("	var framReceiptDoc =framReceiptDetail.document;");
	d.writeln("	framReceiptDoc.formDetail.popReceiptCurrency.selectedIndex = selectedIndex;");
	d.writeln("	var txtReceiptAmount = eval(framReceiptDoc.formDetail.txtSubtotal);");
	d.writeln("	fDetailChangeReceiptAmount(txtReceiptAmount.value,top.iRecIndex,top.iLineNum);");
	d.writeln("}");


/* -----------------------------
-- Detail Change Expense Type
----------------------------- */

	d.writeln("function fDetailChangeExpenseType(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	var r = null;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var popExpType = eval(framReceiptDoc.formDetail.popExpType);");
	d.writeln("  	var objE = top.opener.top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(value);");
 	// for bug 1363873
	d.writeln("  if (top.opener.top.fGetObjExpReport().header.getObjEmployee().mGetbIsProjectEnabled())");
	d.writeln("	if (objE.strExpenditureType != \"\"){");
	d.writeln("	  var txtProject = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_PROJECT_NUMBER\");");
      	d.writeln("	  var txtTask = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_TASK_NUMBER\");");
	d.writeln("	  if (txtProject) txtProject.value = r.getProjectNumber();");
	d.writeln("	  if (txtTask) txtTask.value = r.getTaskNumber();");
	if (top.fIsOOP()){
	d.writeln("	  top.opener.top.framMain.lines.fSetProjectNumber(r.getProjectNumber(),top.iLineNum);");
	d.writeln("	  top.opener.top.framMain.lines.fSetTaskNumber(r.getTaskNumber(),top.iLineNum);");
	}
	else{
	d.writeln("	  top.opener.top.framMain.lines.fSetProject(r.getProjectNumber(),top.iLineNum);");
	d.writeln("	  top.opener.top.framMain.lines.fSetTask(r.getProjectNumber(),top.iLineNum);");
	}
	d.writeln("	}");
	d.writeln("	else{");
	d.writeln("	  var txtProject = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_PROJECT_NUMBER\");");
      	d.writeln("	  var txtTask = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_TASK_NUMBER\");");
	d.writeln("	if ((txtProject) || (txtTask))");
	d.writeln("	  if ((r.getProjectNumber() == '') && (r.getTaskNumber() == '')){");
	d.writeln("	    txtProject.value = top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("	    txtTask.value = top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	if (top.fIsOOP()){
	d.writeln("	    top.opener.top.framMain.lines.fSetProjectNumber(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	d.writeln("	    top.opener.top.framMain.lines.fSetTaskNumber(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	}
	else{
	d.writeln("	    top.opener.top.framMain.lines.fSetProject(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	d.writeln("	    top.opener.top.framMain.lines.fSetTask(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	}
	d.writeln("	  }");
	d.writeln("	  else{ ");
	d.writeln("	    if (confirm(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_WRN'))){");
	d.writeln("	         txtProject.value = top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("		 txtTask.value = top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');");
	d.writeln("		 r.setProjectNumber('');");
	d.writeln("		 r.setTaskNumber('');");
	if (top.fIsOOP()){ 
	d.writeln("	         top.opener.top.framMain.lines.fSetProjectNumber(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	d.writeln("	         top.opener.top.framMain.lines.fSetTaskNumber(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);}");
	}
	else{
	d.writeln("	         top.opener.top.framMain.lines.fSetProject(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);");
	d.writeln("	         top.opener.top.framMain.lines.fSetTask(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA'),top.iLineNum);}");
	}
	d.writeln("	    else{");
	d.writeln("		if (r) if (r.objExpenseType) top.opener.top.fRecoverExpenseType(r.objExpenseType.ID,eval('top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.popExpType'));");
	d.writeln("		else top.opener.top.fRecoverExpenseType('',eval('top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.popExpType'));");
	d.writeln("		return;");
	d.writeln("	    }");
	d.writeln("	  }");
	d.writeln("	}");
	d.writeln("	top.opener.top.framMain.lines.fSetExpenseType(popExpType.selectedIndex,top.iLineNum);");
	d.writeln("	var objE = top.opener.top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(popExpType.options[popExpType.selectedIndex].value);");
	d.writeln("	r.setExpenseType(objE);");
	d.writeln("	r.setAmountIncludesTax(objE.bAITFlag == 'Y');");
	d.writeln("	top.opener.top.framMain.lines.fSetAmtIncTax(r.getAmountIncludesTax(),top.iLineNum);");
	d.writeln("	fDetailSetAmtIncTax(r.getAmountIncludesTax());");
	d.writeln("	if (objE.strDefaultTaxCode != \"\"){");
	d.writeln("	  var e = eval(framReceiptDoc.formDetail.popTaxCode);");
	d.writeln("	  if (e){ ");
	d.writeln("		e.selectedIndex = 0;");
	d.writeln("		var jj = 0;");
	d.writeln("		for (var j=0;j<e.options.length;j++){");
	d.writeln("		  if (e.options[j].text == objE.strDefaultTaxCode){");
	d.writeln("		    jj = j;");
	d.writeln("		    fDetailSetTaxCode(j);");
	d.writeln("		    j=e.options.length;");
	d.writeln("		  }//end if  ");
	d.writeln("		}//end for");
	d.writeln("		r.setTaxCode(objE.strDefaultTaxCode);");
	d.writeln("		top.opener.top.framMain.lines.fSetTaxCode(jj,top.iLineNum);");
	d.writeln("	  }");
	d.writeln("	}");
	d.writeln("	else{");
	d.writeln("	  var e = eval(framReceiptDoc.formDetail.popTaxCode);");
	d.writeln("	  if (e) e.selectedIndex = 0;"); //fDetailSetTaxCode(0);");
	d.writeln("	  r.setTaxCode(\"\");");
	d.writeln("	  top.opener.top.framMain.lines.fSetTaxCode(0,top.iLineNum);");	
	d.writeln("	}");


	d.writeln("    if (!r.flexFields)");
	d.writeln("        r.flexFields = new Array();");
	d.writeln("	r.flexFields.length = 0;");
     	d.writeln("	var strExpType = top.opener.top.fGetExpTypeCode(popExpType.options[popExpType.selectedIndex].value);");
	d.writeln("	top.opener.top.fAddFlexFields(r,strExpType);");
	d.writeln("	var bIsReq = 'N';");
	d.writeln("	if (r.getObjExpenseType()) bIsReq = r.getObjExpenseType().mGetbJustifRequired();");
     	d.writeln("	if (bIsReq == 'Y')");
        d.writeln("	    top.framReceiptDetail.document.reqimg.src = top.opener.top.reqimg1.src;");
        d.writeln("	else top.framReceiptDetail.document.reqimg.src = top.opener.top.g_strImgDir+\"APWPXG5.gif\";");

	// for bug 1303435
	d.writeln("	if (top.framReceiptDetail.document.tax_reqimg){");
	d.writeln("        if (r.getAmountIncludesTax()) top.framReceiptDetail.document.tax_reqimg.src = top.opener.top.reqimg1.src; ");
	d.writeln("	   else top.framReceiptDetail.document.tax_reqimg.src = top.opener.top.g_strImgDir+\"APWPXG5.gif\";");
	d.writeln("	}");

	d.writeln("	top.fDetailSetExpenseType(popExpType.selectedIndex);");
	d.writeln("} //end function fDetailChangeExpenseType");

	d.writeln("function fDetailSetExpenseType(selectedIndex){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	framReceiptDoc.formDetail.popExpType.selectedIndex = selectedIndex;");
	d.writeln("	var popExpType = eval(framReceiptDoc.formDetail.popExpType);");
	d.writeln("	if (top.opener.top.g_bDescFlexEnable){");
     	d.writeln("	var g_bFlexFrameLoadedLocalFlag = false;");
        d.writeln("	top.opener.top.g_bFlexFrameLoadedLocalFlag = top.opener.top.g_bFlexFrameLoaded;");
	d.writeln("	if (top.iRecIndex>=0)");
	d.writeln("	  top.opener.top.fPaintFlex(popExpType.options[popExpType.selectedIndex].value,top.iRecIndex); ");
     	
	d.writeln("	}");
	d.writeln("}");



/* -----------------------------
-- Detail Change Justification
----------------------------- */

	d.writeln("function fDetailChangeJustification(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	var r = null;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var detailJustificationField = eval(framReceiptDoc.formDetail.txtJustification);");
	d.writeln("	r.setJustification(value);");
 	d.writeln("	top.opener.top.framMain.lines.fSetJustification(value,top.iLineNum);");
	d.writeln("}");

	d.writeln("	function fDetailSetJustification(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtJustification.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Group
----------------------------- */

	d.writeln("function fDetailChangeGroup(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	var r = null;");
	d.writeln("		r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var groupField = framReceiptDoc.formDetail.txtGroup;");
  	d.writeln("	r.setGroup(value);");
	d.writeln("	top.opener.top.framMain.lines.fSetExpGroup(value,top.iLineNum);");
	d.writeln("}");

	d.writeln("	function fDetailSetGroup(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtGroup.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Receipt Missing
----------------------------- */

	d.writeln("function fDetailClickReceiptMissing(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	var r = null;");
	d.writeln("		r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var receiptMissingField =  eval(framReceiptDoc.formDetail.cbxReceiptMissing);");
 	d.writeln("	r.setIsMissing(receiptMissingField.checked);");
	d.writeln("	top.opener.top.framMain.lines.fSetReceiptMissing(receiptMissingField.checked,top.iLineNum);");
	d.writeln("}");

	d.writeln("	function fDetailSetReceiptMissing(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.cbxReceiptMissing.checked = value;");
	d.writeln("	}");

/* -----------------------------
-- Detail Change Exchange Rate
----------------------------- */

	d.writeln("function fDetailChangeExchRate(value){");
	d.writeln("	if (value!=\"\"){");
        d.writeln("	  if (!top.opener.top.fCheckPosNum(value, true)){");
	d.writeln("	  fDetailSetExchRate(\"\");");
	d.writeln("	  return;");
	d.writeln("	  }");
	d.writeln("	}");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	var r = null;");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var curr = \"\";");
	d.writeln("	var reimbursCurr = top.opener.top.objExpenseReport.header.getReimbursCurr();");
     	d.writeln("	var receiptCurr = \"\";");
     	d.writeln("	if (r) ");
     	d.writeln("	   receiptCurr = r.getReceiptCurr();");
	d.writeln("     var rateField = eval(framReceiptDoc.formDetail.txtExchRate);");
	d.writeln("	var txtReceiptAmount = eval(framReceiptDoc.formDetail.txtSubtotal);");
     	d.writeln("	var reimbursableField = eval(framReceiptDoc.formDetail.txtReimbursAmount);");
	d.writeln("	 var action = top.opener.top.fDetermineConversion(receiptCurr,reimbursCurr,r);");
	d.writeln("	var strTotal = \"\";");
	
     	d.writeln("if ((action == top.opener.top.g_reimbFixedRecFixed) || (action == top.opener.top.g_reimbFixedRecEur) || (action == top.opener.top.g_reimbEurRecFixed)) {");
	d.writeln("  if (rateField.value != top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_FIXED')) {");
	d.writeln("    alert(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_DONOT_CHANGE_RATE'));");
        d.writeln("    rateField.value = top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_FIXED');");
	d.writeln("    return false;");
	d.writeln("  }");
     	d.writeln("} //end if (action == )");
     	d.writeln("	if (value+\"\"!=\"\")");
     	d.writeln("	  if (top.opener.top.fIsNum(value,true)){");
        d.writeln("		if ((reimbursCurr == receiptCurr) && (value != \"1\")){");
        d.writeln("		  alert(top.opener.top.g_objMessages.mstrGetMesg(\"AP_WEB_RATE_ONE_WRN\"));");
	d.writeln("    rateField.value = \"1\";");
	d.writeln("		  return false;");
        d.writeln("		}");
        d.writeln("	        else {");
        d.writeln("	 	    top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
        d.writeln("		    r.setExchRate(value);");
	d.writeln("		    top.fDetailChangeReceiptAmount(txtReceiptAmount.value,top.iLineNum);");
        d.writeln("	  }//end else");
    	d.writeln("}");
	d.writeln("}//end function fDetailChangeExchRate");

	d.writeln("	function fDetailSetExchRate(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtExchRate.value = value;");
	d.writeln("	}");



/* -----------------------------
-- Detail Focus On Project Field
----------------------------- */
	d.writeln("function fDetailFocusOnProject(){");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	if (r){");
	d.writeln("	  txtProject = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_PROJECT_NUMBER\");");
	d.writeln("	  if (txtProject)");
	d.writeln("	    if (!r.isProjectEnabled()) txtProject.blur();");
	d.writeln("	}");
	d.writeln("}");

/* -----------------------------
-- Detail Focus On Task Field
----------------------------- */
	d.writeln("function fDetailFocusOnTask(){");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	if (r){");
	d.writeln("	  txtTask = eval(\"top.opener.top.g_winDetail.framReceiptDetail.document.formDetail.AP_WEB_PA_TASK_NUMBER\");");
	d.writeln("	  if (txtTask)");
	d.writeln("	    if (!r.isProjectEnabled()) txtTask.blur();");
	d.writeln("	}");
	d.writeln("}");

/* -----------------------------
-- Detail Change Project
----------------------------- */
	d.writeln("function fDetailChangeProjectNumber(value){");
	d.writeln("	var detailDoc = framReceiptDetail.document;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	r.setProjectNumber(value);");
	if (top.fIsOOP())
 	d.writeln("	top.opener.top.framMain.lines.fSetProjectNumber(value,top.iLineNum);");
	else
	d.writeln("	top.opener.top.framMain.lines.fSetProject(value,top.iLineNum)");
	d.writeln("}");

	d.writeln("	function fDetailSetProjectNumber(value){");
	d.writeln("	var detailDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	detailDoc.formDetail.AP_WEB_PA_PROJECT_NUMBER.value = value;");
	d.writeln("	}");



/* -----------------------------
-- Detail Change Task
----------------------------- */
	d.writeln("function fDetailChangeTaskNumber(value){");
	d.writeln("	var detailDoc = framReceiptDetail.document;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	r.setTaskNumber(value);");
	if (top.fIsOOP())
 	d.writeln("	top.opener.top.framMain.lines.fSetTaskNumber(value,top.iLineNum);");
	else
 	d.writeln("	top.opener.top.framMain.lines.fSetTask(value,top.iLineNum);");
	d.writeln("}");

	d.writeln("	function fDetailSetTaskNumber(value){");
	d.writeln("	var detailDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	detailDoc.formDetail.AP_WEB_PA_TASK_NUMBER.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Populate Project Number
----------------------------- */
	
	d.writeln("function LOV(a,b,c,d,e,f,g,h) {");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("     iRecIndex =  top.opener.top.fGetObjExpReport().getNthReceipt(top.iLineNum);");
    	d.writeln("	var r = top.opener.top.fGetReceiptObj(iRecIndex);");
	d.writeln("	if (r){ if (!r.isProjectEnabled()) return;}");
        d.writeln("      if((b==\"AP_WEB_PA_TASK_NUMBER\")&&(framReceiptDoc.formDetail.AP_WEB_PA_PROJECT_NUMBER.value == \"\")) {");
	d.writeln("      alert(top.opener.top.g_objMessages.mstrGetMesg(\"AP_WEB_TASK_LOV_REQ_PROJ\")); return;");
        d.writeln("           }");
        d.writeln("      top.opener.top.LOV(a,b,c,d,e,f,g,h);");
        d.writeln("}");


/* -----------------------------
-- Detail Change Tax Code
----------------------------- */
	d.writeln("function fDetailChangeTaxCode(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	r.setTaxCode(value); ");
	d.writeln("	r.setTaxID(top.opener.top.MyTaxes.getID(value,r.getDate()));");
	d.writeln("	var popTaxCode = eval(framReceiptDoc.formDetail.popTaxCode);");
     	d.writeln("	top.opener.top.framMain.lines.fSetTaxCode(popTaxCode.selectedIndex,top.iLineNum);");
	d.writeln("}");

	d.writeln("	function fDetailSetTaxCode(selectedIndex,value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	if ((selectedIndex) && (framReceiptDoc.formDetail.popTaxCode)){ framReceiptDoc.formDetail.popTaxCode.selectedIndex = selectedIndex;");
	d.writeln("}");
	d.writeln("	else {}}");

	d.writeln("	function fDetailFocusOnTaxCode(){");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	  if (r){");
	d.writeln("  	    var objE = r.getObjExpenseType();");
	d.writeln("  	    if (objE)");
	d.writeln("    		if (objE.bUpdateDefaultTaxCode == 'N'){");
	d.writeln("		  var f = framReceiptDetail.document.formDetail.popTaxCode;");
        d.writeln("        	  if (f) f.blur();");
	d.writeln("    		}");
	d.writeln("  	  }");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Country Of Supply
----------------------------- */
	d.writeln("function fDetailChangeCountryOfSup(value){");
	d.writeln("	var framReceiptDoc = framReceiptDetail.document;");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var popSupplyCountry = eval(framReceiptDoc.formDetail.popSupplyCountry);");
     	d.writeln("	top.opener.top.framMain.lines.fSetCountryOfSup(popSupplyCountry.selectedIndex,top.iLineNum);");
	d.writeln("	r.setSupCountry(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetCountryOfSup(selectedIndex,value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	if (selectedIndex) framReceiptDoc.formDetail.popSupplyCountry.selectedIndex = selectedIndex;");
	d.writeln("	else {}}");


/* -----------------------------
-- Detail Change Doc Number
----------------------------- */
	d.writeln("function fDetailChangeDocNum(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var txtDocNum = eval(framReceiptDoc.formDetail.txtDocNum);");
     	d.writeln("	top.opener.top.framMain.lines.fSetDocNum(txtDocNum.value,top.iLineNum);");
	d.writeln("	r.setDocNum(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetDocNum(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtDocNum.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Reg Number
----------------------------- */
	d.writeln("function fDetailChangeTaxRegNum(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var txtTaxRegNum = eval(framReceiptDoc.formDetail.txtTaxRegNum);");
     	d.writeln("	top.opener.top.framMain.lines.fSetTaxRegNum(txtTaxRegNum.value,top.iLineNum);");
	d.writeln("	r.setTaxRegNum(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetTaxRegNum(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtTaxRegNum.value = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change AIT
----------------------------- */
	d.writeln("function fDetailChangeAmtIncTax(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var cbxAmtIncTax = eval(framReceiptDoc.formDetail.cbxAmtIncludesTax);");
     	d.writeln("	top.opener.top.framMain.lines.fSetAmtIncTax(cbxAmtIncTax.checked,top.iLineNum);");
	d.writeln("	r.setAmountIncludesTax(value);");
	// for bug 1043415
	d.writeln("	if (top.framReceiptDetail.document.tax_reqimg){");
	d.writeln("        if (r.getAmountIncludesTax()) top.framReceiptDetail.document.tax_reqimg.src = top.opener.top.reqimg1.src; ");
	d.writeln("	   else top.framReceiptDetail.document.tax_reqimg.src = top.opener.top.g_strImgDir+\"APWPXG5.gif\";");
	d.writeln("	}");
	d.writeln("}");

	d.writeln("	function fDetailSetAmtIncTax(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	if ((top.opener.top.g_bEnableAmtInclTax == 'Y') && (top.opener.top.g_bUpdateableAmtInclTax == 'Y')) framReceiptDoc.formDetail.cbxAmtIncludesTax.checked = value;");
	d.writeln("	}");


/* -----------------------------
-- Detail Change Reference
----------------------------- */
	d.writeln("function fDetailChangeReference(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var txtMerchRef = eval(framReceiptDoc.formDetail.txtMerchRef);");
	d.writeln("	top.opener.top.framMain.lines.fSetReference(value,top.iLineNum);");
	d.writeln("	r.setMerchRef(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetReference(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtMerchRef.value = value;");
	d.writeln("	}");

	
/* -----------------------------
-- Detail Change Tax Payer ID
----------------------------- */
	d.writeln("function fDetailChangeMerchantTaxId(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var txtTaxPayerID = eval(framReceiptDoc.formDetail.txtTaxPayerID);");
	d.writeln("	top.opener.top.framMain.lines.fSetMerchantTaxId(value,top.iLineNum);");
	d.writeln("	r.setTaxPayerID(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetMerchantTaxId(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtTaxPayerID.value = value;");
	d.writeln("	}");
        

/* -----------------------------
-- Detail Change Merchant Name
----------------------------- */
	d.writeln("function fDetailChangeMerchName(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	top.opener.top.fUpdateLineStatus(top.iLineNum,true);");
	d.writeln("	var r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var txtMerchName = eval(framReceiptDoc.formDetail.txtMerchName);");
	d.writeln("	top.opener.top.framMain.lines.fSetMerchName(value,top.iLineNum);");
	d.writeln("	r.setMerchant(value);");
	d.writeln("}");

	d.writeln("	function fDetailSetMerchName(value){");
	d.writeln("	var framReceiptDoc = eval('top.opener.top.g_winDetail.framReceiptDetail.document');");
	d.writeln("	framReceiptDoc.formDetail.txtMerchName.value = value;");
	d.writeln("	}");

/* -----------------------------
-- Detail Store Global Segment Values
----------------------------- */
	d.writeln("function fStoreGlobalDFFValues(){");
	d.writeln("	var p_strContext = top.opener.top.g_strGlobalContext;");
	d.writeln("	objContext = top.opener.top.g_objContexts.mobjGetContextWithDefault(p_strContext);");
	d.writeln("	if (objContext)");
	d.writeln("	fStoreSegmentValues(objContext,0);");

	d.writeln("}");

/* -----------------------------
-- Detail Store Context Sensitive Segment Values
----------------------------- */
	d.writeln("function fStoreSensitiveDFFValues(){");
	d.writeln("	var p_strContext = \"\";");
	d.writeln("	var iNumOfGlobalSeg = 0;");
	d.writeln("	if (top.opener.top.g_strGlobalContext){p_strContext = top.opener.top.g_strGlobalContext;");
	d.writeln("	var objGlobalContext = top.opener.top.g_objContexts.mobjGetContextWithDefault(p_strContext);");
	d.writeln("	if (objGlobalContext) iNumOfGlobalSeg = objContext.miGetNumOfFlexField();");
	d.writeln("	else iNumOfGlobalSeg = 0;}");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	if (r.getObjExpenseType()){p_strContext = r.getObjExpenseType().mGetstrCode();");
	d.writeln("	objContext = top.opener.top.g_objContexts.mobjGetContextWithDefault(p_strContext);");
	d.writeln("	fStoreSegmentValues(objContext,iNumOfGlobalSeg);}");
	d.writeln("}");

/* -----------------------------
-- Detail Store Segment Values
----------------------------- */
	d.writeln("function fStoreSegmentValues(objContext,iBaseIndex){");
	d.writeln("	var objFlexField");
	d.writeln("	r = top.opener.top.fGetReceiptObj(top.iRecIndex);");
	d.writeln("	var strStyle, strValue, tempField;");
	d.writeln("	for (var i=0;i<objContext.miGetNumOfFlexField();i++){");
	d.writeln("	objFlexField = objContext.arrFlexFields[i];");
	d.writeln("	if (objContext.mGetstrWidgetName(objFlexField.mGetstrPrompt())){");
	d.writeln("	tempField = eval('top.flexFields.document.flexForm.'+objContext.mGetstrWidgetName(objFlexField.mGetstrPrompt(),iBaseIndex));");
	d.writeln("	strStyle = objFlexField.mGetiObjectType(objFlexField.mGetstrPrompt())");
	d.writeln("	strValue = top.opener.top.fGetWidgetValue(tempField, strStyle);");
	d.writeln("	}");
	d.writeln("	if (r) r.setFlexValues(objFlexField.mGetstrPrompt(), strValue);");
	d.writeln("	}");
	d.writeln("}");
      

/* -----------------------
-- Detail Clear Receipt
----------------------- */
	// Only for Out of Pocket
	d.writeln("function fDetailClearReceipt(){");
	d.writeln("var actualIndex = top.opener.top.objExpenseReport.getNthReceipt(top.iLineNum);");
	d.writeln("var r = top.opener.top.fGetReceiptObj(actualIndex);")
	d.writeln("if (r) {");
;	d.writeln("  top.opener.top.objExpenseReport.oopReceipts[actualIndex].taxInfo = null;");
	d.writeln("  top.opener.top.objExpenseReport.oopReceipts[actualIndex].flexFields = null;");
	d.writeln("  top.opener.top.objExpenseReport.oopReceipts[actualIndex] = null;");
	d.writeln("} //end if");

        d.writeln("var newLine = top.opener.top.framMain.lines.g_iCurrentLine - 1;");
 	d.writeln("if (newLine < 1) newLine = 1;");
	d.writeln("top.opener.top.framMain.lines.g_iCurrentLine = newLine;");
	
        d.writeln("top.opener.top.fCreateMoreLines(0,false);"); 
	d.writeln("top.opener.top.framMain.lines.fUpdateButtonsFrame();");
	d.writeln("selectField = eval(\"top.opener.top.framMain.lines.document.data.cbxSelect\"+newLine);");
        d.writeln("selectField.checked = true;");
    	
	d.writeln("top.iLineNum = newLine;");
	d.writeln("top.iRecIndex = top.opener.top.objExpenseReport.getNthReceipt(newLine);");
	d.writeln("top.opener.top.fGenerateDetailButtons();");
	d.writeln("top.opener.top.fUpdatePopShowReceipt();");
  	d.writeln("top.bRunPopDetail = true;");
	d.writeln("top.opener.top.fPopulateReceiptDetail(top.iRecIndex);");
	d.writeln("} //end function fDetailClearReceipt");

	// This is the function to force onChange event got triggered for IE
	d.writeln("function fDetailIEOnChangeTrigger() { ");
	d.writeln("  var detail = eval('top.opener.top.g_winDetail.framReceiptDetail.document.formDetail');");
	d.writeln("  detail.popExpType.focus();");
	d.writeln("  var actualIndex = top.opener.top.objExpenseReport.getNthReceipt(top.iLineNum);");
	d.writeln("  var r = top.opener.top.fGetReceiptObj(actualIndex);")
	d.writeln("  var n = 0; ");
	d.writeln("  if (r && r.flexFields) {");
	d.writeln("  	  for (var i=0; i < r.flexFields.length; i++) {");
	d.writeln("	     if (n > 1) break; ");
	d.writeln("  	     if (r.flexFields[i] != null) {");
	d.writeln("		 var flexPrompt = r.flexFields[i].mGetName();");
	d.writeln("		 var flexName = top.opener.top.ReplaceInvalidChars(flexPrompt);");
	d.writeln("   	         var flexForm = eval('top.flexFields.document.flexForm'); ");
	d.writeln("  		 var flexField;");
	d.writeln("  		 if (flexForm) flexField = eval('top.flexFields.document.flexForm.'+ flexName);");

	d.writeln("  		 if (flexField && flexField.type != \"hidden\") {flexField.blur();");
	d.writeln("		    flexField.focus();");
	d.writeln("		    n = n + 1;");
	d.writeln("		 } ");
	d.writeln("  	     } //end if");
	d.writeln("	   } //end for");
	d.writeln("  	} //end if r && r.flexFields");
	d.writeln("} //end function fDetailIEOnChangeTrigger");
	
/* -----------------------
-- Detail Add Receipt
----------------------- */
	// Only for Out of Pocket
	if (top.fIsOOP()){
	d.writeln("function fDetailAddReceipt(){");
	d.writeln("  	if (!top.opener.top.Nav4) ");
	d.writeln("        fDetailIEOnChangeTrigger();"); 
    	// Store flex field values in receipt object
	d.writeln("	var nNewLine =  top.opener.top.objExpenseReport.getTotalReceipts('OOP') + 1;");
    	d.writeln("	top.opener.top.fFocusOnLine(nNewLine);");
    	d.writeln("	top.opener.top.fUpdateLineStatus(nNewLine, false);");
	d.writeln("	top.iRecIndex = top.opener.top.objExpenseReport.getNthReceipt(nNewLine);");
	d.writeln("	top.iLineNum = nNewLine;");
    	d.writeln("	top.opener.top.fGenerateDetailBody(top.iRecIndex,top.iLineNum);");
    	d.writeln("     if (!(top.opener.top.objExpenseReport.getNthReceipt(nNewLine) <0 &&  top.g_winDetail != null && !top.g_winDetail.closed)) {");
    	d.writeln("	  top.opener.top.fGenerateDetailButtons();");
   	d.writeln("	  top.opener.top.fUpdatePopShowReceipt();");
	d.writeln("	  var flexForm = eval('top.flexFields.document');");
    	d.writeln(" 	  flexForm.open(); ");
	d.writeln("	  flexForm.writeln(top.opener.top.fBlankPage('cccccc')); ");
	d.writeln("	  flexForm.close(); ");
   	d.writeln("	} //end if ");
	d.writeln("	top.bRunPopDetail = true;");
    	d.writeln("	top.opener.top.fPopulateReceiptDetail(top.iRecIndex);");
	d.writeln("}");
	} //end if


}

function fGenerateDetailBody(iReceiptIndex,iLineNum)
   {
	var detailFrame = eval('top.g_winDetail.framReceiptDetail');
        var lReimbursCurrency = top.objExpenseReport.header.getReimbursCurr();
	var d = eval('detailFrame.document');
	var tempRegion = top.g_objRegions.mobjGetRegion('AP_WEB_DETAIL');
	var taxRegion = top.g_objRegions.mobjGetRegion('AP_WEB_TAX');
	d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<html dir=\""+top.g_strDirection+"\">");
        d.writeln("<HEAD>");
        top.fGenerateStyleCodes(detailFrame);
	d.writeln("<Script Language=\"JavaScript\">");
	d.writeln("var r = null;");
 	d.writeln("r = top.opener.top.fGetReceiptObj(" +iReceiptIndex +");");
	d.writeln("top.opener.top.g_bDetailFrameLoaded  = false;");
	
	d.writeln("  function populateProjectNumberField(projectNumber) {");
	d.writeln("	top.fDetailChangeProjectNumber(projectNumber);");
	d.writeln("	var txtProjectID = eval('top.opener.top.framMain.lines.document.data.AP_WEB_PA_PROJECT_ID');");
	d.writeln("	if (txtProjectID.value != \"\") ");
	d.writeln("	   if (top.opener.top.fIsNum(txtProjectID.value,false)) {");
	d.writeln("	 	if (top.opener.top.fIsOOP())");
	d.writeln("	 	   top.opener.top.framMain.lines.fChangeProjectNumber(projectNumber,txtProjectID.value, top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	 	else //ccard");
	d.writeln("	 	   top.opener.top.framMain.lines.fChangeProject(projectNumber,txtProjectID.value, top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	   }");
	d.writeln("	  else {");
	d.writeln("	      if (top.opener.top.fIsOOP()) ");
	d.writeln("	         top.opener.top.framMain.lines.fChangeProjectNumber(projectNumber,\"\", top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	      else ");
	d.writeln("	         top.opener.top.framMain.lines.fChangeProject(projectNumber,\"\", top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	   } //end else");
	d.writeln("	top.opener.top.framMain.lines.document.data.AP_WEB_PA_PROJECT_ID.value = \"\";");

	d.writeln("  } //end function populateProjectNumberField");
                   
	d.writeln("  function populateTaskNumberField(taskNumber) { ");
	d.writeln("	top.fDetailChangeTaskNumber(taskNumber);");
	d.writeln("	var txtTaskID = eval('top.opener.top.framMain.lines.document.data.AP_WEB_PA_TASK_ID');");
	d.writeln("	if (txtTaskID.value != \"\")");
	d.writeln("	   if (top.opener.top.fIsNum(txtTaskID.value,false)) {");
	d.writeln("	      if (top.opener.top.fIsOOP()) ");
	d.writeln("	        top.opener.top.framMain.lines.fChangeTaskNumber(taskNumber,txtTaskID.value,top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	      else //ccard");
	d.writeln("	        top.opener.top.framMain.lines.fChangeTask(taskNumber,txtTaskID.value,top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	   }");
	d.writeln("	 else {");
	d.writeln("	      if (top.opener.top.fIsOOP()) ");
	d.writeln("	         top.opener.top.framMain.lines.fChangeTaskNumber(taskNumber,\"\", top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	      else //ccard");
	d.writeln("	         top.opener.top.framMain.lines.fChangeTask(taskNumber,\"\", top.opener.top.framMain.lines.g_iCurrentLine);");
	d.writeln("	  } //end else");
        d.writeln("	top.opener.top.framMain.lines.document.data.AP_WEB_PA_TASK_ID.value = \"\";");           
	d.writeln("  } //end function populateTaskNumberField");
	
	

	d.writeln("<\/Script>");
        d.writeln("<\/HEAD>");
        d.writeln("<BODY class=colorg5 onLoad = \"top.opener.top.fWaitUntilFrameLoaded(eval('top.opener.top.g_bDetailFrameLoaded')+'','');top.opener.top.g_bDetailFrameLoaded = true;\" onUnlaod = \"top.opener.top.g_winDetail = null;\">");
	//d.writeln("<BODY class=colorg5>")
	d.writeln("<form name=formDetail>");
	d.writeln("<!--beginning of no-tab container code-->");
	d.writeln("<CENTER>");
	d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 WIDTH=100% class=colorg5>");
	d.writeln("  <TR align=center>");
	d.writeln("	  <TD class=colorg5 COLSPAN=3>");
	d.writeln("	  <center>");
	d.writeln("	    <table border=0 cellpadding=0 cellspacing=0 width=90%>");
	d.writeln("	      <tr>");
	var strTemp = "";
	var arrTokenNames = new Array();
	var arrTokenVals = new Array();
	arrTokenNames[0] = "&REQFLD";
	arrTokenVals[0] = "<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>";
	strTemp = top.g_objMessages.mstrGetMesg("AP_WEB_ENTER_RECEIPT_INFO");
	strTemp = strTemp.replace(arrTokenNames[0],arrTokenVals[0]);
	d.writeln("	      	<td colspan=4><font class=helptext>"+strTemp);
	d.writeln("	</td>");
	d.writeln("	      </tr>");
        d.writeln("		<tr>");
	d.writeln("		  <td class=colorg5 COLSPAN=4 valign=top> ");
	d.writeln("		  <br>");
	d.writeln("		  <font class=datablack>&nbsp;&nbsp;"+tempRegion.mobjGetRegionItem('AP_WEB_RECEIPT_INFO').mstrGetPrompt()+"</font><br>");
	d.writeln("		  <hr width = 100%>");
	d.writeln("		  </td>");
	d.writeln("		</tr>"); 
	
/*------------------------------
-- Date and Expense Type
------------------------------ */

	d.writeln("	      <tr>");
 	d.writeln("	      	<td align=right nowrap>");
	if (tempRegion.mobjGetRegionItem('AP_WEB_START_DATE').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("	      	<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_START_DATE').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		<td nowrap><font class=promptblack><input type = \"text\" name=txtDate onChange = \"top.fDetailChangeDate(this.value,"+iReceiptIndex+", "+iLineNum+");\" size=10></font> ");
	d.writeln("		<a href=\"javascript:top.opener.top.fOpenCalWin('top.framMain.lines.document.data.txtDate"+top.framMain.lines.g_iCurrentLine+"','top.fOnClickDate("+iLineNum+")');\"><img src="+top.g_strImgDir+"APWICLDR.gif border=no align=absmiddle></a></td>");
	d.writeln("		  <td align=right nowrap>");
	if (tempRegion.mobjGetRegionItem('AP_WEB_EXPTYPE').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_EXPTYPE').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack><select name=popExpType onChange=\"javascript:top.fDetailChangeExpenseType(this.options[this.selectedIndex].value);\">");
	d.writeln(top.fGenExpPopOptions()+"</select></font></td>");
	d.writeln("	       </tr>");

/* ---------------------------------
-- Days
--------------------------------- */

	d.writeln("		<tr>");
	d.writeln("		<td align=right nowrap>");
	if (tempRegion.mobjGetRegionItem('AP_WEB_DAYS').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_DAYS').mstrGetPrompt()+"</font>&nbsp</td>");
	d.writeln("		<td nowrap><font class=promptblack><input type = \"text\" name=txtOccurs size=10 onChange=\"top.fDetailChangeOccurs(this.value);\" ></font>");


/* ---------------------------------
-- Daily Rate
--------------------------------- */

	
	if (tempRegion.mobjGetRegionItem('AP_WEB_DAILYRATE').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<font class=promptblack>X ");
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("&nbsp;"+tempRegion.mobjGetRegionItem('AP_WEB_DAILYRATE').mstrGetPrompt()+"</font>&nbsp;");
	d.writeln("		<font class=promptblack><input type = \"text\" name=txtReceiptRate onChange = \"top.fDetailChangeRate(this.value,"+iReceiptIndex+", "+iLineNum+");\" size=10></font>");
	
	d.writeln("	</td>");


/* ---------------------------------
-- Receipt Amount
--------------------------------- */

	if (fIsOOP()){
	  d.writeln("		  <td align=right nowrap>");
	if (tempRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt()+"</font>&nbsp;</td>");
	  d.writeln("		  <td><font class=promptblack><input type = \"text\" name=txtSubtotal onChange = \"javascript:top.fDetailChangeReceiptAmount(this.value,"+iReceiptIndex+", "+iLineNum+");\" size=10></font></td>");
	}
	else{
	  d.writeln("		  <td align=right nowrap>");
	  if (tempRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	  d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt()+"</font>&nbsp;</td>");
	  if (top.fGetReceiptObj(iReceiptIndex))
	  d.writeln("		  <td><font class=promptblack>"+top.fGetReceiptObj(iReceiptIndex).getExpensedAmount()+"</font></td>");
	}
	d.writeln("	       </tr>");
		

/* ----------------------------
-- Currency Conversion
---------------------------- */
        //1 strLeftCurr = 10 l_strRightCurr

	var strGenPopList = "<select name=popReceiptCurrency onChange=\"top.fDetailChangeReceiptCurr(this.selectedIndex,"+iReceiptIndex+", "+iLineNum+");\">"+top.fGenRecCurrPopOptions(false)+"</select>";
        var strLeftCurr = lReimbursCurrency; //1 strLeftCurr = (left of the exchange rate text)
	var strRightCurr = lReimbursCurrency; //Right of the Exchange Rate Text

	if (top.g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y') {
	  strLeftCurr = lReimbursCurrency;
	  if (fIsOOP())  //OOP
	      strRightCurr = strGenPopList;
	  else
	      strRightCurr = top.objExpenseReport.cCardReceipts[iReceiptIndex].receiptCurr;
	}
	else {  //display inverse rate is no
	  if (fIsOOP()){ //OOP, generate poplist for currencies
	    strLeftCurr = strGenPopList;
	    strRightCurr = lReimbursCurrency;
	  }
	  else {//ccard, display the receipt currency
	    strLeftCurr = top.objExpenseReport.cCardReceipts[iReceiptIndex].receiptCurr;
	    strRightCurr = lReimbursCurrency;
	  }
	} //end else

	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap><font class=promptblack>1 "+ strLeftCurr +" = &nbsp;</font></td>");
	
	if (fIsOOP()){
	     d.writeln("		  <td align=left nowrap><font class=promptblack><input type = \"text\" name=txtExchRate width=5 size=6 onChange=\"top.fDetailChangeExchRate(this.value);\"> " + strRightCurr +"</font></td>");

/* ----------------------------
-- Reimbursable Amount
---------------------------- */
	  d.writeln("		  <td align=right nowrap>");
	  if (tempRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	  d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetPrompt()+"("+lReimbursCurrency+")</font>&nbsp;</td>");
	  d.writeln("		  <td align=left nowrap><font class=promptblack><input type = \"text\" name=txtReimbursAmount size=10 onFocus=\"this.blur()\"></font></td>");
	}
	else if (top.fGetReceiptObj(iReceiptIndex)){
	  d.writeln("		  <td align=left nowrap><font class=promptblack>"+top.fGetReceiptObj(iReceiptIndex).getExchRate() +"&nbsp;"+strRightCurr+"</font></td>");
	  d.writeln("		  <td align=right nowrap>");
	  if (tempRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetRequiredFlag() == 'Y')
	    	d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	  d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetPrompt()+"("+lReimbursCurrency+")</font>&nbsp;</td>");
	  d.writeln("		  <td align=left nowrap>"+top.objExpenseReport.cCardReceipts[iReceiptIndex].getReimbursAmount()+"</td>");
	}
	d.writeln("		</tr>");


/* ------------------------------
-- Justification
------------------------------ */

	d.writeln("	       <tr>");
	d.writeln("	         <td align=right><IMG src="+top.g_strImgDir+"APWPXG5.gif name=reqimg height=18 width=18><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_JUST').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		 <td colspan=3><font class=promptblack><input type = \"text\" size=40 name=txtJustification onChange = \"javascript:top.fDetailChangeJustification(this.value,"+iReceiptIndex+", "+iLineNum+");\"></font></td>");
	d.writeln("	        </tr>");

/* -----------------------------
-- Receipt Missing and Group
----------------------------- */

	d.writeln("	       <tr>");
	d.writeln("	       	<td></td>");
	d.writeln("		<td><input type=\"checkbox\" name=cbxReceiptMissing onClick = \"javascript:top.fDetailClickReceiptMissing();\"><font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_RECPT_MISSING').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td align=right nowrap>");
	if (tempRegion.mobjGetRegionItem('AP_WEB_EXP_GROUP').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_EXP_GROUP').mstrGetPrompt()+"</font>&nbsp;</td>");
	d.writeln("		  <td><font class=promptblack><input type = \"text\" name=txtGroup size=10 onChange = \"javascript:top.fDetailChangeGroup(this.value,"+iReceiptIndex+", "+iLineNum+");\"></font></td>");
	d.writeln("	       </tr>");


/* -----------------------------
-- Project and Task fields
----------------------------- */
	if (top.objExpenseReport.header.getObjEmployee().mGetbIsProjectEnabled()){
	d.writeln("		<tr>");

	d.writeln("		  <td align=right nowrap>");
	//if (tempRegion.mobjGetRegionItem('AP_WEB_PA_PROJ_NUM').mstrGetRequiredFlag() == 'Y')
	//d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	//d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_PA_PROJ_NUM').mstrGetPrompt()+"</font>&nbsp;</td>");
	d.writeln("		<font class=promptblack>Project Number</font>&nbsp;</td>");
	d.writeln("		  <td><font class=promptblack><input type=text name=AP_WEB_PA_PROJECT_NUMBER size=25 onFocus = \"javascript:top.fDetailFocusOnProject();\" onChange = \"javascript:top.fDetailChangeProjectNumber(this.value);\"></font>");
	d.writeln(top.g_strDetailProjLOV);
	d.writeln("		  <INPUT TYPE=\"hidden\" NAME=\"AP_WEB_PA_PROJECT_ID\" VALUE=\"\">");
	d.writeln("		</td></tr>");

	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	//if (tempRegion.mobjGetRegionItem('AP_WEB_PA_TASK_NUM').mstrGetRequiredFlag() == 'Y')
	//d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	//d.writeln("		<font class=promptblack>"+tempRegion.mobjGetRegionItem('AP_WEB_PA_TASK_NUM').mstrGetPrompt()+"</font>&nbsp;</td>");
	d.writeln("		<font class=promptblack>Task Number</font>&nbsp;</td>");
	d.writeln("		  <td><font class=promptblack><input type=text name=AP_WEB_PA_TASK_NUMBER size=25 onFocus = \"javascript:top.fDetailFocusOnTask();\" onChange = \"javascript:top.fDetailChangeTaskNumber(this.value);\"></font>");
	d.writeln(top.g_strDetailTaskLOV);
	d.writeln("		  <INPUT TYPE=\"hidden\" NAME=\"AP_WEB_PA_TASK_ID\" VALUE=\"\">");
	d.writeln("		  </td></tr>");		  
}

/* -----------------------------
-- Tax Information
----------------------------- */
	if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_TAX_ENABLE')=='Y'){
	d.writeln("		<tr>");
	d.writeln("		  <td class=colorg5 COLSPAN=4 valign=top> ");
	d.writeln("		  <br>");
	d.writeln("		  <font class=datablack>&nbsp;&nbsp;"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_INFO').mstrGetPrompt()+"</font><br>");
	d.writeln("		  <hr width = 100%>");
	d.writeln("		  </td>");
	d.writeln("		</tr>");

	
	
/* -----------
-- Tax Code
----------- */
	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	d.write("		<IMG src="+top.g_strImgDir+"APWPXG5.gif name=tax_reqimg height=18 width=18>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_CODE').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack>");
	d.writeln("		     <select name=popTaxCode onChange = \"top.fDetailChangeTaxCode(this.options[this.selectedIndex].value);\" onFocus = \"top.fDetailFocusOnTaxCode()\">");
	d.writeln(top.fGenPopTaxCode());
	d.writeln("		</select></font></td>");
	

/* -----------
-- AIT
----------- */
   if ((g_bEnableAmtInclTax == 'Y') && (g_bUpdateableAmtInclTax == 'Y')) {
	d.writeln("		<td><font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_AIT').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><input type=\"checkbox\" name=cbxAmtIncludesTax onClick = \"top.fDetailChangeAmtIncTax(this.checked);\"></td>");
   }
   else
    if (g_bEnableAmtInclTax == 'Y'){
	d.writeln("		  <td><input type=\"hidden\" name=cbxAmtIncludesTax onClick = \"top.fDetailChangeAmtIncTax(this.checked);\"></td>");
    }
 
	d.writeln("</tr>");
	

if (g_bEnableNewTaxFields == 'Y'){

/* ----------------------
-- Merchant Name
---------------------- */
	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack>");
	if (fIsOOP()){
	  d.writeln("<input type = \"text\" name=txtMerchName size=10 ");
	  d.writeln("		  onChange = \"top.fDetailChangeMerchName(this.value);\">");
	}
	else
        d.writeln(""+top.objExpenseReport.cCardReceipts[iReceiptIndex].merchant+"");
	d.writeln("		</font></td>");

/* --------------
-- Sup Country
-------------- */
	
        d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_TAX_COUNTRY').mstrGetRequiredFlag() == 'Y')
	d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_COUNTRY').mstrGetPrompt()+"</font></td>");
	d.writeln("		  <td align=left><font class=promptblack><select name=popSupplyCountry onChange = \"top.fDetailChangeCountryOfSup(this.options[this.selectedIndex].value);\">");
	d.writeln(top.fGenCountryPoplist());
	d.writeln("</select></font></td>");
	d.writeln("		</tr>");	
	


/* -----------------------
-- Tax Regigstration Num
----------------------- */

	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_TAX_REGNUM').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_REGNUM').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack><input type = \"text\" size=10 name=txtTaxRegNum ");
	d.writeln("		  onChange = \"top.fDetailChangeTaxRegNum(this.value);\">");
	d.writeln("		</font></td>");


/* ---------------
-- Tax Payer ID
--------------- */

	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_MERCH_TAXPAYER').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_MERCH_TAXPAYER').mstrGetPrompt()+"</font>&nbsp</td>");
	d.writeln("		  <td><font class=promptblack><input type = \"text\" name=txtTaxPayerID size=10 onChange = \"top.fDetailChangeMerchantTaxId(this.value);\">");
	d.writeln("		</font></td>");
	d.writeln("		</tr>");
	
/* ---------------
-- Merch Reference
--------------- */

	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_TAX_REF').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_REF').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack><input type = \"text\" size=20 name=txtMerchRef ");
	d.writeln("		  onChange = \"top.fDetailChangeReference(this.value);\">");
	d.writeln("		</font></td>");

	
/* ----------------
-- Merch Doc Num
---------------- */

	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_TAX_DOCNUM').mstrGetRequiredFlag() == 'Y')
	d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_TAX_DOCNUM').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack><input type = \"text\" name=txtDocNum size=10 onChange = \"top.fDetailChangeDocNum(this.value);\">");

	d.writeln("		  </font></td></tr>");

  } // new tax fields are enables
  else
   if (!fIsOOP()){
	/* ----------------------
	-- Merchant Name
	---------------------- */
	d.writeln("		<tr>");
	d.writeln("		  <td align=right nowrap>");
	if (taxRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetRequiredFlag() == 'Y')
	  d.writeln("		<img src="+top.g_strImgDir+"APWREQUI.gif align=absmiddle>");
	d.writeln("		<font class=promptblack>"+taxRegion.mobjGetRegionItem('AP_WEB_MERCHANT').mstrGetPrompt()+"&nbsp</font></td>");
	d.writeln("		  <td><font class=promptblack>");
	if (fIsOOP()){
	  d.writeln("<input type = \"text\" name=txtMerchName size=10 ");
	  d.writeln("		  onChange = \"top.fDetailChangeMerchName(this.value);\">");
	}
	else
        d.writeln(""+top.objExpenseReport.cCardReceipts[iReceiptIndex].merchant+"");
	d.writeln("		</font></td>");
	d.writeln("		</tr>");
   }

}


	d.writeln("		<tr>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		</tr>");
       
	
	d.writeln("		<tr>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		  <td></td>");
	d.writeln("		</tr>");
	d.writeln("	</table>");
	d.writeln("	</center>");
	d.writeln("	</TD>");
	d.writeln("</TR>");
	d.writeln("</TABLE>");
	d.writeln("</CENTER>");
	d.writeln("</form>");
	d.writeln("</body>");
	d.writeln("</html>");

	d.close();
} // end of fGenerateDetailBody


function fGenerateDetailWindow(){
	var titleReg = 	top.g_objRegions.mobjGetRegion('AP_WEB_DETAIL');
        var d = eval('top.g_winDetail.document');
        d.open();
	d.writeln("	<title>"+titleReg.mobjGetRegionItem('AP_WEB_RECEIPT_DETAIL').mstrGetPrompt()+"&nbsp</title>");
	d.writeln("	<HEAD><SCRIPT LANGUAGE=\"JavaScript\">");
	d.writeln("	iRecIndex = 0;");
	d.writeln("	iLineNum = 0;");
	d.writeln("	bRunPopDetail = false;");
	top.fGenDetailFuncs(); 

	d.writeln("	<\/SCRIPT></HEAD>");
        d.writeln("     <frameset cols=\"3,*,3\" border=0>");
        d.writeln("             <frame  ");
        if (!(isWindows95 && isIE5)) d.writeln("             src=\"javascript:top.opener.top.fBlankPage('336699')\""); 
        d.writeln("             frameborder=no");
        d.writeln("             name=leftborder");
        d.writeln("             marginwidth=0");
        d.writeln("             marginheight=0");
        d.writeln("             scrolling=no>");

	if (top.g_objProfileOptions.mvGetProfileOption('AP_WEB_TAX_ENABLE')=='Y')
        d.writeln("             <frameset rows = \"5%,*,35,20%,90\" border=0 framespacing=0>");
	else
        d.writeln("             <frameset rows = \"5%,*,35,30%,90\" border=0 framespacing=0>");
        d.writeln("             <frame ");
	if (!(isWindows95 && isIE5)) d.writeln("		src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("             name=top_border");
        d.writeln("             marginwidth=0");
        d.writeln("             frameborder=no");
        d.writeln("             scrolling=no>");
        d.writeln("             <frame ");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("                     frameborder=no");
        d.writeln("                     name=framReceiptDetail");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=auto>");
	d.writeln("             <frame");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fFlexHeader('cccccc')\"");
        d.writeln("                     frameborder=no");
        d.writeln("                     name=framFlexHeader");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=no>");
        d.writeln("             <frame");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fBlankFlexFrame()\" ");
        d.writeln("                     frameborder=no");
        d.writeln("                     name=flexFields");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=auto>");
	d.writeln("             <frame");
        if (!(isWindows95 && isIE5)) d.writeln("                     src=\"javascript:top.opener.top.fBlankPage('cccccc')\"");
        d.writeln("                     frameborder=no");
        d.writeln("                     name=detailButtonsFrame");
        d.writeln("                     marginwidth=0");
        d.writeln("                     scrolling=no>");
        d.writeln("     </frameset>");
        d.writeln("     <frame  ");
        if (!(isWindows95 && isIE5)) d.writeln("      src=\"javascript:top.opener.top.fBlankPage('336699')\" ");
        d.writeln("      frameborder=no");
        d.writeln("      name=rightborder");
        d.writeln("      marginwidth=0");
        d.writeln("      marginheight=0");
        d.writeln("      scrolling=no>");
        d.writeln("   </frameset>");
        d.close();
   }//end function fGenerateDetailWindow



