/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: POREVENT.js 115.8 1999/11/04 13:22:53 pkm ship     $ */

/*************document initializations*************
 Document initialization handlers initializes
 for window body documents.  They should be called
 by setting the onload handler in the body tag of 
 html source.
 example: <body onload="initEditLines(document);">

 Document initialization handlers mainly sets the
 event handlers for the form elements in the document.

 All sample files mentioned in this file are availible 
 in /home/marychu/javascript/demo/
**************************************************/

/* event type : document onload
 * UI template: por_edit_req_content
 * sameple usage: see por_edit_lines_content.html
 */
function initEditReq(doc)
{
  var header=getLayer("header_content"+suffix);
  var lines=getLayer("lines_content"+suffix);

  if(top.currentIndex.selectedIndex==0)
  {
    hide(header);
    show(lines);
  }
  else
  {
    hide(lines);
    show(header);
  }

  //  moveObject(header, 8, 0);
  //  moveObject(lines, 8, 0);

  initDocument(doc);
  //  currentTemplate="por_edit_req_content"; 

  var forms=allForms["POR_LINES_R"];

  if(forms)
  {
    for(var i=0; i<forms.length; i++)
    {
      forms[i].unitPrice=
        (parseFloat(forms[i]["POR_EXTENDED_PRICE"].value)/parseFloat(forms[i]["POR_QUANTITY_ORDERED"].value));
      
      //      alert(forms[i].unitPrice);
      forms[i].index=i;
      setElementHandler(forms[i], "POR_QUANTITY_ORDERED", "onBlur", 
                        new Function("top.quantityOrderedChanged(this)"),true);
      setElementHandler(forms[i], "POR_EXTENDED_PRICE", "onFocus", 
                        new Function("this.blur();"));
      forms[i].elements["POR_EXTENDED_PRICE"].value= 
        roundDecimal(forms[i].elements["POR_EXTENDED_PRICE"].value,
                     AMOUNT_DIGIT); 
    } // end of for(...i<forms.length...)

    // toolbar stuff
  }
    top.printFrame="por_content";
//  switchContentLayer(top.currentIndex);
  
  doc.loaded=true;              // need to set loaded variable so
                                // dynamic por_edit_req_bottom can coordinate
  if(findFrame(top, "por_bottom").document.loaded)// if bottom frame
    // has been loaded
  {
    setTotal();
  }

  //  findFrame(top, "por_toolbar").document.images.STOP.src= "FNDPX3.gif";
  
  if(!doc.errorMessage && mWin!=null && !mWin.closed)
  {
	mWin.close();
  }
} // end of initEditLines

    
/* event type : document onload
 * UI template: por_distributions_content
 * sameple usage: see por_edit_distributions_content.html
 */
function initEditDistributions(doc)
{
  initDocument(doc);
//   currentTemplate="por_distributions";
  
  var forms=allForms["POR_DIST_LINES_R"];
  for(var i=0; i<forms.length; i++)
  {
//     if(forms[i].POR_PERCENTAGE)
//       forms[i].POR_PERCENTAGE.oldVal=forms[i].POR_PERCENTAGE.value;
//     if(forms[i].POR_AMOUNT)
//       forms[i].POR_AMOUNT.oldVal=forms[i].POR_AMOUNT.value;
//     if(forms[i].POR_QUANTITY)
//       forms[i].POR_QUANTITY.oldVal=forms[i].POR_QUANTITY.value;

  // Bug 933394, changing the onChange's to onBlur cuz that's what it's
  // supposed to be
    setElementHandler(forms[i], "POR_PERCENTAGE", "onBlur", 
                      new Function("percentageChanged(this)"),true);
    setElementHandler(forms[i], "POR_AMOUNT", "onBlur", 
                      new Function("amountChanged(this)"),true);
    setElementHandler(forms[i], "POR_QUANTITY", "onBlur", 
                      new Function("quantityChanged(this)"),true);
    percentageChanged(forms[i].POR_PERCENTAGE);
  }
  var totalLine=forms[forms.length-1];
  if(totalLine.POR_PERCENTAGE) 
    totalLine.POR_PERCENTAGE.value= roundDecimal('100', PERCENT_DIGIT);
  if(totalLine.POR_AMOUNT) 
    totalLine.POR_AMOUNT.value= roundDecimal(selectedLineAmount, AMOUNT_DIGIT);
  if(totalLine.POR_QUANTITY)
    totalLine.POR_QUANTITY.value= roundDecimal(selectedLineQuantity, 
                                               QUANTITY_DIGIT);
  
  for(var i=0; i<totalLine.elements.length; i++)
  {
    totalLine.elements[i][getHandlerName("onFocus")]=
      new Function("this.blur();");
  }
}

function initLineDetails(doc) 
{
  initDocument(doc);
  
  var forms= allForms["POR_LINE_DETAILS_R"];
  for(var i=0; i<forms.length; i++)
  {
    var cForm= forms[i];
    if(cForm.POR_UNIT_PRICE || cForm.POR_QUANTITY_ORDERED || 
       cForm.POR_EXTENDED_PRICE || cForm.POR_SERVICE_COST ||
       cForm.POR_RATE_PER_UNIT)
    {
      setElementHandler(cForm, "POR_QUANTITY_ORDERED", "onBlur", 
                        new Function("return top.detailQuantityChanged(this,true)"), true);
      setElementHandler(cForm, "POR_SERVICE_COST", "onBlur", 
                        new Function("return checkProperFormat(this)"), true);

      setElementHandler(cForm, "POR_UNIT_PRICE", "onBlur", 
                        new Function("return unitPriceChanged(this);"), true);
      setElementHandler(cForm, "POR_RATE_PER_UNIT", "onBlur", 
                        new Function("return unitPriceChanged(this);"), true);

      setElementHandler(cForm, "POR_EXTENDED_PRICE", "onFocus", 
                        new Function("this.blur()"));
      
      if(cForm.POR_EXTENDED_PRICE && cForm.POR_QUANTITY_ORDERED)
      {
        cForm.unitPrice=     
          (cForm["POR_EXTENDED_PRICE"].value)/(cForm["POR_QUANTITY_ORDERED"].value);
        cForm["POR_EXTENDED_PRICE"].value=roundDecimal(cForm["POR_EXTENDED_PRICE"].value,
                                                       AMOUNT_DIGIT);
      }
      break;
    } //end of if(cForm.POR_UNIT_PRICE || cForm.POR_QUANTITY_ORDERED...)
  } //end of for(var i=0; i<forms.length; i++)..

  var win= findFrame(top, "req_line_details_content");
  if (win.top.goToAttachment)
  {
    win.location.hash= 'attachmentGroup';
  }
} // end of initLineDetails()

function initFavoriteLists(doc)
{
  initDocument(doc);
  top.allForms["POR_LINES_R"]=null;
  top.allForms["POR_HEADER_R"]=null;
}

function initNonCatalogItems(doc)
{
  initDocument(doc);

  var forms= allForms["POR_SPECIAL_ORDER_GOODS_R"];
  for(var i=0; i<forms.length; i++)
  {
    setElementHandler(forms[i], "POR_QUANTITY_ORDERED", "onBlur", 
                      new Function("return checkPositive(this);"), true);
    setElementHandler(forms[i], "POR_UNIT_PRICE", "onBlur", 
                      new Function("return checkProperFormat(this);"), true);
    if (forms[i].POR_LINE_TYPE_CODE)
      forms[i].POR_LINE_TYPE_CODE.value= "goods";
    if (forms[i].POR_QUANTITY_ORDERED)
      forms[i].POR_QUANTITY_ORDERED.oldVal=1;
  }
  var forms= allForms["POR_SPECIAL_ORDER_RATE_R"];
  for(var i=0; i<forms.length; i++)
  {
    setElementHandler(forms[i], "POR_QUANTITY_ORDERED", "onBlur", 
                      new Function("return checkPositive(this);"), true);
    setElementHandler(forms[i], "POR_RATE_PER_UNIT", "onBlur", 
                      new Function("return checkProperFormat(this);"), true);
    if (forms[i].POR_LINE_TYPE_CODE)
      forms[i].POR_LINE_TYPE_CODE.value= "rate";
    if (forms[i].POR_QUANTITY_ORDERED)
      forms[i].POR_QUANTITY_ORDERED.oldVal=1;
  }

  var forms= allForms["POR_SPECIAL_ORDER_AMOUNT_R"];
  for(var i=0; i<forms.length; i++)
  {
    setElementHandler(forms[i], "POR_SERVICE_COST", "onBlur", 
                      new Function("return checkProperFormat(this);"), true);
    if (forms[i].POR_LINE_TYPE_CODE)
      forms[i].POR_LINE_TYPE_CODE.value= "amount";
    if (forms[i].POR_QUANTITY_ORDERED)
      forms[i].POR_QUANTITY_ORDERED.oldVal=1;
  }
//  doc.selectedItemType="goods";
  top.printFrame="content_middle";

//  por_main.content.content_middle.goods();
// findFrame(top, "content_middle").goods();

 if (top.findFrame(top,"content_top").document.loaded)
 {
 if (top.findFrame(top,"content_top").document.forms[0].itemType[0].checked)
   findFrame(top, "content_middle").goods();
 if (top.findFrame(top,"content_top").document.forms[0].itemType[1].checked)
   findFrame(top, "content_middle").rateBased();
 if (top.findFrame(top,"content_top").document.forms[0].itemType[2].checked)
   findFrame(top, "content_middle").amountBased();
 }
 else
   findFrame(top, "content_middle").goods();

 top.allForms["POR_LINES_R"]=null;
 top.allForms["POR_HEADER_R"]=null;
}


function initMassUpdate(doc)
{
  initDocument(doc);
}

function initUserPreferences(doc)
{
  initDocument(doc);
}

function initApproverList(doc)
{
  initDocument(doc);
  var forms= allForms["POR_APPROVERS_R"];
  for(var i=0; i<forms.length; i++)
  {
    setElementHandler(forms[i], "POR_SEQUENCE_NUM", "onBlur", 
                      new Function("checkPostiveInt(this)"));
  }
}



/***********************Form element event handlers*********************
 Validation and event handling routine for form elements.  

 If validation needs to be done when the value an element is changed by
 user, it is recommended that the onBlur instead of onChange, since onChange
 is used for setting the form.updated field.

 Element event handlers usually requires a input of the element itself.  This
 could be referred to by the "this" keyword.
*/


/*
 * Sets the event handler an element of a form.  This function can logically
 * handle the following:
 * - sets the handler iff the element exist (since it may be turned off)
 * - determines the spelling of the handler depending of browser version
 */
function setElementHandler(form,        // form object
                           elementName, // string of element name
                           handlerName, // string of name of event handler ; 
                                        // eg "onClick" (nav style)
                           handler,     // function eg new Function("this.blur");
                           setOldValue) 
{
  var elem=form.elements[elementName];
  if(elem)
  {
    handlerName= getHandlerName(handlerName);
    elem[handlerName]= handler; 
    if(setOldValue)
    {
      elem.oldVal= (isNaN(elem.value) || (elem.value<0)) ? 0 : elem.value;
    }
  }
}

function getHandlerName(navName)
{
  return (IS_IE) ? navName.toLowerCase() : navName;
}

/* 
 * Checks to see if the value of the element is a positive number and if
 * not, sets the number back to its old value.
 * 
 * event type : element onBlur
 * UI template: This function is not yet used as a event handler
 * sameple usage: see quantityOrderChanged()
 */
function checkPositive(elem, noZero)
{
  var qty= elem.value;
  if(elem.oldVal<0|| (noZero&&elem.oldVal==0)|| isNaN(elem.oldVal))
    elem.oldVal=1;
  if(qty<0 || (noZero&& qty==0) || isNaN(qty))
  {
    elem.value=elem.oldVal;
    getActiveWindow().alert(FND_MESSAGES["ICX_POR_ALRT_POSITIVE_NUMBER"]);
    elem.focus();
    return false;
  }
  elem.oldVal=elem.value;
  return true;
}

function checkProperFormat(elem, noZero)
{
  var qty= elem.value;
  if(elem.oldVal<0|| (noZero&&elem.oldVal==0)|| isNaN(elem.oldVal))
    elem.oldVal=1;
  if(qty<0 || (noZero&& qty==0) || isNaN(qty))
  {
    elem.value=elem.oldVal;
    getActiveWindow().alert(FND_MESSAGES["ICX_POR_ALRT_IMPROPER_FORMAT"]);
    elem.focus();
    return false;
  }
  elem.oldVal=elem.value;
  return true;
}

function checkPositiveInteger(elem)
{
  var qty= elem.value;
  if(qty<0 || isNaN(qty) ||  Math.floor(qty) != qty)
  {
    elem.value=elem.oldVal;
    getActiveWindow().alert(FND_MESSAGES["ICX_POR_ALRT_POSITIVE_INTEGER"]);
    elem.focus();
    return false;
  }
  elem.oldVal=elem.value;
  return true;
}

/* 
 * Checks to see if the value of the element is a positive number and if
 * not, sets the number back to its old value. Also sets the extend price
 * of the line.
 * 
 * event type : element  onBlur ("POR_QUANTITY_ORDERED")
 * UI template: por_edit_req_content
 * sameple usage: see initEditLines() (above)
 */
function quantityOrderedChanged(elem)
{
  var qty= elem.value;
  if(checkPositive(elem,true))
  {
    elem.form.elements["POR_EXTENDED_PRICE"].value= 
      roundDecimal(qty*elem.form.unitPrice, AMOUNT_DIGIT); 
    if(elem.form.name=="POR_LINES_R")
      top.setTotal();
  }
}

function detailQuantityChanged(elem)
{
  if(checkPositive(elem,true) && elem.form.POR_EXTENDED_PRICE) 
  {
    elem.form.POR_EXTENDED_PRICE.value= 
      roundDecimal(elem.value*elem.form.unitPrice, AMOUNT_DIGIT);
    return true;
  }
  return false;
}

function unitPriceChanged(elem)
{
  if(checkProperFormat(elem) && elem.form.POR_EXTENDED_PRICE)
  {
     elem.form.unitPrice= elem.value;
     elem.form.POR_EXTENDED_PRICE.value= 
       roundDecimal(elem.value*elem.form.POR_QUANTITY_ORDERED.value,AMOUNT_DIGIT);
     return true;
  }
  return false;
}

function getLastDistribution()
{
  var forms=allForms["POR_DIST_LINES_R"];
  for(var i=forms.length-2; i>=0; i--)
  {
    if(!isEmpty(forms[i]["POR_PERCENTAGE"].value))
    {
      return forms[i];
    }
  }
  return null;
}

/* 
 * Checks to see if the value of the element is a positive number and if
 * not, sets the number back to its old value.  Then sets the related
 * values in the same row.  Finally, sets the totals for the region.
 * 
 * event type : element onBlur ("POR_PERCENTAGE"])
 * UI template: por_distributions_content
 * sameple usage: see initEditDistributions()
 */
function percentageChanged(elem) 
{
  if(isEmpty(elem.value))
  {
    elem.form.elements.POR_AMOUNT.value="";
    elem.form.elements.POR_QUANTITY.value="";
    return;
  }
  // before doing any auto update for the other 2 fields, check to see
  // if the field exist before updating it.
  var useAmount=elem.form.elements["POR_AMOUNT"];
  var useQuantity=elem.form.elements["POR_QUANTITY"];
  if(checkPositive(elem))
  {
    if(useAmount)
    {
      elem.form.elements["POR_QUANTITY"].value= 
      elem.form.elements["POR_QUANTITY"].oldVal= 
        roundDecimal(elem.value*selectedLineQuantity/100,
                     QUANTITY_DIGIT);
    }
    if(useQuantity)
    {
      elem.form.elements["POR_AMOUNT"].value= 
      elem.form.elements["POR_AMOUNT"].oldVal= 
        roundDecimal(elem.value*selectedLineAmount/100, AMOUNT_DIGIT);
    }
    elem.value= roundDecimal(elem.value,PERCENT_DIGIT);
    setDistributionTotal();

    var forms=allForms["POR_DIST_LINES_R"];
    var totalLine= forms[forms.length-1];
    var lastLine= getLastDistribution();
    // check to see if the percentage totals up to 100 percent.  if
    // so, check if the quantity and amount also adds up to total, if
    // not, favor the last line so that the values add up to total
    if(parseFloat(totalLine.POR_PERCENTAGE.value)== 100)
    {
      if(useAmount)
      {
        var totalAmount=parseFloat(totalLine.POR_AMOUNT.value);
        if(totalAmount!=selectedLineAmount)
        {
          lastLine.POR_AMOUNT.value= 
            roundDecimal(parseFloat(lastLine.POR_AMOUNT.value)+
                         (selectedLineAmount-totalAmount), AMOUNT_DIGIT);
          totalLine.POR_AMOUNT.value=roundDecimal(selectedLineAmount, AMOUNT_DIGIT);
        }
      }
      if(useQuantity)
      {
        var totalQuantity=parseFloat(totalLine.POR_QUANTITY.value);
        if(totalQuantity!=selectedLineQuantity)
        {
          lastLine.POR_QUANTITY.value= 
            roundDecimal(parseFloat(lastLine.POR_QUANTITY.value)+
                         (selectedLineQuantity-totalQuantity),QUANTITY_DIGIT);
          totalLine.POR_QUANTITY.value=roundDecimal(selectedLineQuantity,
                                                    QUANTITY_DIGIT);
        }
      } // end if( useQuantity)
      
    } // end of if(parseFloat(totalLine.POR_QUANTITY.value)==...
  }
}

/* 
 * Checks to see if the value of the element is a positive number and if
 * not, sets the number back to its old value.  Then sets the related
 * values in the same row.  Finally, sets the totals for the region.
 * 
 * event type : element onBlur ("POR_QUANTITY")
 * UI template: por_distributions_content
 * sameple usage: see initEditDistributions()
 */
function quantityChanged(elem)
{
  if(isEmpty(elem.value))
  {
    elem.form.POR_AMOUNT.value="";
    elem.form.POR_PERCENTAGE.value="";
    return;
  }

  
  if(!isEmpty(elem.value) && checkPositive(elem))
  {
    var useAmount=elem.form.elements["POR_AMOUNT"];
    var percent= 
      elem.form.elements["POR_PERCENTAGE"].value= 
      elem.form.elements["POR_PERCENTAGE"].oldVal=
      roundDecimal(100*elem.value/selectedLineQuantity, PERCENT_DIGIT);

    if(useAmount)
    {
      elem.form.elements["POR_AMOUNT"].value= 
      elem.form.elements["POR_AMOUNT"].oldVal= 
        roundDecimal(percent*selectedLineAmount/100, AMOUNT_DIGIT);
    }
    
    elem.value= roundDecimal(elem.value,QUANTITY_DIGIT);
    setDistributionTotal();
  
    // handle rounding errors
    var forms=allForms["POR_DIST_LINES_R"];
    var totalLine= forms[forms.length-1];
    var lastLine= getLastDistribution();
    
    if(parseFloat(totalLine.POR_QUANTITY.value)== selectedLineQuantity)
    {
      var totalPercent=parseFloat(totalLine.POR_PERCENTAGE.value);
      if(totalPercent!=100)
      {
        lastLine.POR_PERCENTAGE.value= 
          roundDecimal(parseFloat(lastLine.POR_PERCENTAGE.value)+
                       (100-totalPercent), PERCENT_DIGIT);
        totalLine.POR_PERCENTAGE.value="100.00000";
      }
      
      if(useAmount)
      {
        var totalAmount=parseFloat(totalLine.POR_AMOUNT.value);
        if(totalAmount!=selectedLineAmount)
        {
          lastLine.POR_AMOUNT.value= 
            roundDecimal(parseFloat(lastLine.POR_AMOUNT.value)+
                         (selectedLineAmount-totalAmount), AMOUNT_DIGIT);
          totalLine.POR_AMOUNT.value= roundDecimal(selectedLineAmount, AMOUNT_DIGIT);
        }
      } // end of if (useAmount)
    } // end of if(parseFloat(totalLine.POR_QUANTITY.value)==...
  } // end of if(checkPositive..)
} // end of function quantityChanged()


/* 
 * Checks to see if the value of the element is a positive number and if
 * not, sets the number back to its old value.  Then sets the related
 * values in the same row.  Finally, sets the totals for the region.
 * 
 * event type : element onBlur ("POR_AMOUNT")
 * UI template: por_distributions_content
 * sameple usage: see initEditDistributions()
 */
function amountChanged(elem)
{
  if(isEmpty(elem.value))
  {
    elem.form.POR_PERCENTAGE.value="";
    elem.form.POR_QUANTITY.value="";
    return;
  }
  
  if(!isEmpty(elem.value) && checkProperFormat(elem))
  {
    var useQuantity=elem.form.elements["POR_QUANTITY"];

    var percent=
      elem.form.elements["POR_PERCENTAGE"].value= 
      elem.form.elements["POR_PERCENTAGE"].oldVal= 
      roundDecimal(100*elem.value/selectedLineAmount, PERCENT_DIGIT);

    if(useQuantity)
    {
      elem.form.elements["POR_QUANTITY"].value= 
      elem.form.elements["POR_QUANTITY"].oldVal= 
        roundDecimal(percent*selectedLineQuantity/100, QUANTITY_DIGIT);
    }
    
    elem.value= roundDecimal(elem.value, AMOUNT_DIGIT);
    setDistributionTotal();

    var forms=allForms["POR_DIST_LINES_R"];
    var totalLine= forms[forms.length-1];
    var lastLine= getLastDistribution();
    if(parseFloat(totalLine.POR_AMOUNT.value)== selectedLineAmount)
    {
      var totalPercent=parseFloat(totalLine.POR_PERCENTAGE.value);
      if(totalPercent!=100)
      {
        lastLine.POR_PERCENTAGE.value= 
          roundDecimal(parseFloat(lastLine.POR_PERCENTAGE.value)+
                       (100-totalPercent), PERCENT_DIGIT);
        totalLine.POR_PERCENTAGE.value="100.00000";
      }
      if(useQuantity)
      {
        var totalQuantity=parseFloat(totalLine.POR_QUANTITY.value);
        if(totalQuantity!=selectedLineQuantity)
        {
          elem.form.POR_QUANTITY.value= 
            roundDecimal(parseFloat(lastLine.POR_QUANTITY.value)
                         +(selectedLineQuantity-totalQuantity),QUANTITY_DIGIT);
          totalLine.POR_QUANTITY.value=roundDecimal(selectedLineQuantity, 
                                                    QUANTITY_DIGIT);
        }
      } //end of if(useQuantity)
    } // end of if(parseFloat(totalLine.POR_QUANTITY.value)==...
  } // end of if(checkPositive..)
}

/* 
 * Sets the total values of POR_REQ_LINE_DISTRIBUTIONS_R after one of the
 * allocated values has changed.
 *
 * event type : not a event handler.  subfunction for other distribution
 *              event handlers.
 * UI template: 
 * sameple usage: see amountChanged()
 */
function setDistributionTotal()
{
  var forms=allForms["POR_DIST_LINES_R"];
  var totalQ=0;
  var totalP=0;
  var totalA=0;
  for(var i=0; 
      i<forms.length-1; 
      i++)
  {
    if(!isEmpty(forms[i]["POR_PERCENTAGE"].value))
    {
      totalP += parseFloat(forms[i]["POR_PERCENTAGE"].value);
      totalQ += parseFloat(forms[i]["POR_QUANTITY"].value);
      totalA += parseFloat(forms[i]["POR_AMOUNT"].value);
    }
  }
  var totalLine=forms[forms.length-1];  
  totalLine["POR_PERCENTAGE"].value= roundDecimal(totalP, PERCENT_DIGIT);
  totalLine["POR_AMOUNT"].value= roundDecimal(totalA, AMOUNT_DIGIT);
  totalLine["POR_QUANTITY"].value= roundDecimal(totalQ, QUANTITY_DIGIT);
}

/************************** Submission Routines **************************
 Routines to submit one or more regions to Web Server.  See submitRequest()
 in porlib.js for more info on how that works.
*/

function checkSelectedLines()
{
  var forms= top.allForms["POR_LINES_R"];
  if(forms)
  {
    for(var i=0; i<forms.length; i++)
    {
      if(forms[i].POR_SELECT_BOX && forms[i].POR_SELECT_BOX.checked)
      {
        return true;
      }
    }
  }

  alert(top.FND_MESSAGES['ICX_POR_ALRT_MUST_SELECT_LINES']);
  return false;
}


/* 
 * Send request to delete selected lines.
 *
 * event type : button (Delete) clicked
 * UI template: por_edit_req_buttons
 * sameple usage: see por_edit_lines_buttons.html
 */
function deleteClicked()
{

  if (checkSubmitFlag())
    return;

  if(checkSelectedLines())
  {
//    setSubmitFlag();
    submitRequest(
      "requisition",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "deleteLines", 
    "deleteLines",
      "",
    new Array("POR_HEADER_R", "POR_LINES_R", "selectedLinesList"),
    "por_hidden");
  }
//  "por_content");
}

/* 
 * Send request to copy selected lines.
 *
 * event type : button (Copy) clicked
 * UI template: por_edit_req_buttons
 * sameple usage: see por_edit_lines_buttons.html
 */
function copyClicked()
{
  if (checkSubmitFlag())
    return;

  if(checkSelectedLines())
  {
//    setSubmitFlag();
    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",  // url
      "copyLines",                // action
      "copyLines",                // function
      "",                         // general Data
      new Array("POR_HEADER_R", "POR_LINES_R", "selectedLinesList"),  // formNames
      "por_hidden");                 // target
  }//    "por_content");                 // target
}


/* 
 * Send request to navigate to requsition summary page
 *
 * event type : button (Final Review) clicked
 * UI template: por_edit_req_buttons
 * sameple usage: see por_edit_lines_buttons.html
 */
function finishReqClicked()
{
    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "finalReview",
      "finalReview",
      "",
      new Array("POR_HEADER_R", "POR_LINES_R"),
    "por_main");
}

/* 
 * Send request to navigate to requsition summary page
 *
 * event type : button (Final Review) clicked
 * UI template: por_edit_req_buttons
 * sameple usage: see por_edit_lines_buttons.html
 */
function changeMultipleLinesClicked()
{
  if (checkSubmitFlag())
    return;

  if(checkSelectedLines())
  {
//    openModalWindow("", "massUpdate");
    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
//    "updateMultipleLines",
//    "updateMultipleLines",
      "updateReqBeforeMassUpdate",
      "updateRequisition",
      "",
//      new Array("POR_HEADER_R", "POR_LINES_R", "selectedLinesList"),
      new Array("POR_HEADER_R", "POR_LINES_R"),
      "por_hidden");
//    "massUpdate");
  }
}

/* 
 * Send request to cancel Requisition
 *
 * event type : button (Cancel) clicked
 * UI template: multiple templates
 * sameple usage: see por_edit_lines_buttons.html
 */
function cancelClicked()
{

  if (checkSubmitFlag())
    return;

  winWidth=400
  winHeight=200
  winAttributes = "menubar=no,location=no,toolbar=no," + 
    "width=" + winWidth + ",height=" + winHeight + 
	",screenX=" + (screen.width - winWidth)/2 + ",screenY=" + (screen.height - winHeight)/2 + 
    ",resizable=yes,scrollbars=yes"
  openModalWindow("/OA_HTML/PORARCLF.htm", "cancelReqConfirmation", winAttributes);

}

function discardReqClicked()
{
  if(modalOpen())
  {
    mWin.close();
  }
  
    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "discardReq",
      "discardReq",
      "",
      new Array(),
      "por_hidden");
}


function saveReq()
{
  if(modalOpen())
  {
    mWin.close();
  }

  // bug 1028794
  // what if called at the non-catalog page?
  if(!top.getTop().checkTextArea("POR_HEADER_R")) return;


  top.getTop().submitRequest(
    "requisition",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "saveReqCancelDialog",
    "saveRequisition",
    "",
    new Array("POR_HEADER_R", "POR_LINES_R"),
    "por_hidden");
}



function addMoreItemsClicked()
{
  if (checkSubmitFlag())
    return;

  // bug 1028794
  if(!checkTextArea("POR_HEADER_R")) return;


  submitRequest(
    "requisition",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "addMoreItems",
    "addMoreItems",
    "",
    new Array("POR_HEADER_R", "POR_LINES_R"),
    "por_main"
    );
}

/* 
 * Send request to add an Special Item
 *
 * event type : button (Add to Requisition) clicked
 * UI template: por_non_catalog_item_content_bottom
 * sameple usage: see por_edit_lines_buttons.html
 */
function addSpecialOrderClicked()
{
    submitRequest(
      "por_non_catalog_item_buttons",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "addSpecialItem", 
    "addItems",
      "",
    new Array("POR_HEADER_R", "POR_LINES_R"),
    "por_content");
}


// bug 1028794
function textAreaChanged(formName, inputFieldName, textAreaName, maxLength)
{
  if(formName.elements[inputFieldName])
  {
	var text = formName.elements[inputFieldName].value;
  	if(text.length >maxLength)
  	{
	// had to replace twice because we have two &FIELD_NAME in the message.
		alert((top.getTop().FND_MESSAGES["ICX_POR_ALRT_TEXTAREA_TOO_LONG"].replace("&FIELD_NAME", textAreaName)).replace("&FIELD_NAME", textAreaName));
    		return false;	
   // formName.elements[inputFieldName].value=text.substring(0, maxLength);
   	}
   }
   return true;
}


// to be put into PORCUTIL.js

function checkTextArea(formName)
{
 
  var forms=top.getTop().allForms[formName];
  if(forms)
  {
//    getActiveWindow().alert("forms.length=" + forms.length);
    var isOk=true;
    if(formName == "POR_HEADER_R")
    {
   	for(var i=0; i<forms.length; i++)
    	{
	   if(forms[i].elements["POR_DESCRIPTION"] || 
		forms[i].elements["POR_NOTE_TO_APPROVER"] ||
		forms[i].elements["POR_NOTE_TO_BUYER"])
	   {
	   	isOk = (
	   	(textAreaChanged(forms[i], "POR_DESCRIPTION", "Description", 240)) &&
	   	(textAreaChanged(forms[i], "POR_NOTE_TO_APPROVER", "To Approver", 240)) &&
	   	(textAreaChanged(forms[i], "POR_NOTE_TO_BUYER", "To Buyer", 240)));
		break;
	   }
    	}
     }
     else if((formName == "POR_SPECIAL_ORDER_GOODS_R") ||
	(formName == "POR_SPECIAL_ORDER_RATE_R") ||
	(formName == "POR_SPECIAL_ORDER_AMOUNT_R"))
     {
	for(var i=0; i<forms.length; i++)
    	{
	   if(forms[i].elements["POR_ITEM_DESCRIPTION"])
	   {
		isOk = textAreaChanged(forms[i], "POR_ITEM_DESCRIPTION", "Item Description", 240);
		break;
	   }
        }
     }
/*
     else if(formName == "POR_EDIT_ATTACHMENTS_R")
     { 
	for(var i=0; i<forms.length; i++)
    	{
	   if(forms[i].elements["POR_TEXT"])
	   {
		isOk = textAreaChanged(forms[i], "POR_TEXT", 80);
		break;
	   }   
        }
     }
*/
     else if(formName == "POR_LINE_DETAILS_R")
     {
	for(var i=0; i<forms.length; i++)
    	{
	   if(forms[i].elements["POR_NOTE_TO_SUPPLIER"] ||
		forms[i].elements["POR_ITEM_DESCRIPTION"])
	   {
		isOk = (textAreaChanged(forms[i], "POR_ITEM_DESCRIPTION", "Item Description", 240) && textAreaChanged(forms[i], "POR_NOTE_TO_SUPPLIER", "To Supplier", 240)) ;
		break;
	   }
        }
     }
     else if(formName == "POR_RECEIVE_ORDERS_HEADER_R")
     {
	for(var i=0; i<forms.length; i++)
    	{
	   if(forms[i].elements["POR_COMMENTS"])
	   {
		isOk = textAreaChanged(forms[i], "POR_COMMENTS", "Comments", 240);
		break;
	   }
        }
     }
     return isOk;
  }
  return true;
}

function finalReviewClicked()
{
  if (checkSubmitFlag())
    return;

  // bug 1028794
  if(!checkTextArea("POR_HEADER_R")) return;


  if(getLineCount() >0)
  {
    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "saveReqFinalReview",
      "saveRequisition",
      "",
      new Array("POR_HEADER_R", "POR_LINES_R"),
      "por_hidden");
  }
  else
  {
    top.alert(FND_MESSAGES['ICX_POR_ALRT_CANNOT_SUBMIT']);
  }
}

function saveAndSubmitReq()
{
  if (checkSubmitFlag())
    return;

  // bug 1028794
  if(!checkTextArea("POR_HEADER_R")) return;


  if(top.jumpActionOrigin=="wfNotification" && top.jumpActionMode!="resubmit")
  {
      submitRequest(
        "requisition",
        "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
        "saveReqNotifications",
        "saveRequisition",
        "",
        new Array("POR_HEADER_R", "POR_LINES_R"),
        "por_hidden");
  }
  else if(getLineCount() >0)
  {
    // displays warning message when requisition is submitted
    winWidth  = 400;
    winHeight = 200;
    winAttributes = "menubar=no,location=no,toolbar=no," + 
        "width=" + winWidth + ",height=" + winHeight + 
        ",screenX=" + (screen.width - winWidth)/2 + ",screenY=" + (screen.height - winHeight)/2 + 
        ",resizable=yes,scrollbars=yes"
        openModalWindow("/OA_HTML/PORWRSMF.htm", "submitReqWarning", winAttributes);

    submitRequest(
      "requisition",
      "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
      "saveAndSubmitReq",
      "saveAndSubmitReq",
      "",
      new Array("POR_HEADER_R", "POR_LINES_R"),
      "por_hidden");
  }
  else
  {
    top.alert(FND_MESSAGES['ICX_POR_ALRT_CANNOT_SUBMIT']);
  }
}

// Approvers
function deleteApprover(ind)
{
  var approverId= getElementValue(ind, "POR_APPROVERS_R",
                                  "POR_APPROVER_ID");
  submitRequest("requisition",
                "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order",
                "deleteApprover",
                "deleteApprover",
                FIELD_DELIMITER+ "approverId"+
                VALUE_DELIMITER+ approverId,
                new Array("POR_APPROVERS_R"),
                "approvers_content");
}

function refreshList()
{
  submitRequest("requisition",
                "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order",
                "refreshApprovers",
                "refreshApprovers",
                FIELD_DELIMITER+ "approverId"+
                VALUE_DELIMITER+ approverId,
                new Array("POR_APPROVERS_R"),
                "approvers_content");
}


/***** About line indexes *****

 Some region attributes, like account distribution and line details, 
 requires actions to be done on a particular line.  In those cases, the
 line index needs to be passed to the handler handling the event.  

 Since providing the line index is not supported by the Display Manager, the 
 following work-around is used:
 
 See por_edit_lines_content (link for FNDWTEMP.gif) for example on how to use 
 this feature.  See initDocument in porlib.js to see the coding for the 
 feature.

*/
 
/* 
 * Send request to show account distributions window,
 *
 * event type : Account Distributions clicked
 * UI template: por_edit_req_content
 * sameple usage: see por_edit_lines_content.html
 */
function showDistributions(lineIndex)
{
  if (checkSubmitFlag())
    return;

  var lineNumber= getElementValue(lineIndex, "POR_LINES_R",
                                  "POR_LINE_NUM");
  
  top.selectedLine= lineNumber;
  var currentF=allForms["POR_LINES_R"][lineIndex];
  top.selectedLineQuantity= 
    parseFloat(currentF.elements["POR_QUANTITY_ORDERED"].value);
  top.selectedLineAmount= 
    parseFloat(currentF.elements["POR_EXTENDED_PRICE"].value);
  
  if(checkMandatory("POR_HEADER_R") &&
     checkMandatory("POR_LINES_R"))
  {
//  openModalWindow("", "accountDistributions");
    submitRequest(
      "requisition",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
//  "displayDistributions",
//  "displayDistributions",
    "updateReqBeforeAcctDist",
    "updateRequisition",
//  FIELD_DELIMITER + "lineNum" + VALUE_DELIMITER + lineNumber,
    "", 
    new Array("POR_HEADER_R", 
              "POR_LINES_R"),
    "por_hidden");
//  "accountDistributions");
  }
}

/* 
 * Send request to show Edit details window 
 *
 * event type : Details
 * UI template: por_edit_req_content
 * sameple usage: see por_edit_lines_content.html
 */


/* 
 * Send request to show Template Info window
 *
 * event type : Details
 * UI template: por_edit_req_content
 * sameple usage: see por_edit_lines_content.html
 */
/*  -- replaced by Matt's routine
function showTemplate(lineIndex)
{
  alert(lineIndex);
}
*/

function sendDistributions(functionName)
{
  var dist= allForms["POR_DIST_LINES_R"];
  if (parseFloat(dist[dist.length-1].POR_PERCENTAGE.value)!=100)
  {
    getActiveWindow().alert(FND_MESSAGES["ICX_POR_ALRT_DIST_NOT_HUNDRED"]);
    return;
  }

  // remove the last form element
  for(var i=0; i<dist.length; i++)
  {
    dist[i].updated= (dist[i].POR_PERCENTAGE.value >0);
  }
  dist[dist.length-1].updated= false;
  
  submitRequest(
    "requisition",
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder",
    "applyDistributions",
    functionName,
//    "applyDistributions",
    FIELD_DELIMITER+ "lineNum"+
    VALUE_DELIMITER+ top.allForms["POR_DIST_HEADER_R"][0].elements["POR_LINE_NUM"].value,
    new Array("POR_DIST_LINES_R"),
    "acc_dist_hidden");
//    "por_content");
}
  
/******************************************************************************
 * functions for menu bar
 ******************************************************************************/

function printContent()
{
  content= findFrame(top,'por_content');
  if(content.print)
  {
    content.print();
  }
  else  // print function not defined
  {
    alert(FND_MESSAGES["ICX_POR_ALRT_CANNOT_PRINT"]);
  }
}

function refreshWindow(win)
{
  if(!win)
  {
    win=top;
  }
  
  if(win.frames.length==0)
    win.location.reload();
  else
  for(var j=0; j<win.frames.length; j++)
  {
    refreshWindow(win.frames[j]);
  }
}


// navigates to the edit req page.  used in both add items and final review
// this function is also called by the proc server, so notify them if
// there's any api change.
function editReq()
{

  if (checkSubmitFlag())
    return;
//alert("editReq()");
// bug 1028794
//  if(!top.checkTextArea("POR_SPECIAL_ORDER_GOODS_R")) return;

  if(top.currentIndex.tabs[top.currentIndex.selectedIndex].onSwitchOut())
  {
    setSubmitFlag();
    findFrame(top, "por_main").location=
      ("/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN="
       + top.getReqToken("editReq", "editReq", "requisition"));
  }
}

// position and setRuner are used to set the total line on add items
// or viewReqs page
// position sets the position of the layer that contains the total
// line
// setRunner sets its content

// enter the name of the frame that contains the 
function position(frameName, layerName) 
{
  if(!layerName) layerName="total";
 // following are hard-coded dimensions that is platform dependent
 	var objWidth = 330
	var navTop = 10
	var ieTop = 16
        var win= top.findFrame(top, frameName);
	var obj=top.getLayer("total");;
        
	if (top.IS_NAV) {
          var docWidth = win.innerWidth;
          var docHeight = win.innerHeight;
          obj.moveTo(docWidth/2, navTop);
          //          obj.resizeTo(docWidth/2, docHeight);
            } else { // IE stuff -- to be perfected
          var docWidth = document.body.clientWidth;
          obj.pixelLeft = docWidth/2;
          obj.pixelTop = ieTop;
	}
	setRunner("total", top.getLineCount(),
                  roundDecimal(top.getReqTotal(), AMOUNT_DIGIT));
	obj.visibility = "visible"; 
	obj.zIndex=5;
}

function setRunner(layerName, val1, val2) {
        var label1= window.top.FND_MESSAGES["ICX_POR_PRMPT_TOTAL_LINES_ADD"];
        var label2= window.top.FND_MESSAGES["ICX_POR_PRMPT_REQ_TOTAL"];
        var str= 
          "<FONT CLASS='PROMPTBLACK'>" + label1 + "&nbsp;&nbsp;" +
          "</FONT><FONT CLASS='DATABLACK'>" + val1 + "&nbsp;&nbsp;&nbsp;</FONT>"+
          "<FONT CLASS='PROMPTBLACK'>" + label2 +  "&nbsp;&nbsp;" +
          "</FONT><FONT CLASS='DATABLACK'>" + val2 +
          "&nbsp;&nbsp;&nbsp;</FONT>";

        if(IS_NAV)
	var str = 
          "<html><body><table width=50% height=25%><tr>" +
          "<td align='right' valign='top'>"+ str+ 
          "</td></tr></table></body></html>";
        if(IS_NAV)
        {
          setContent(top.getLayer(layerName), str);
        }
        else
        {
          setContent(getLayer(layerName+"_ie"), str);
          var ie2= getLayer(layerName+"_ie2");
          if(ie2!=null)
          {
            setContent(ie2, str);
          }
        }
}


function refreshMain()
{
  if(top.findFrame(top, 'searchHeader'))
    top.searchHeaderURL=top.findFrame(top,'searchHeader').location.href;
  //  alert(top.searchHeaderURL);
  if(top.findFrame(top, 'searchFooter'))
     top.searchTailURL=top.findFrame(top,'searchFooter').location.href;
  //  alert(top.searchTailURL);
  top.findFrame(top, 'por_main').location.reload();
}

if(IS_NAV)
{
 origWidth = innerWidth;
 origHeight = innerHeight;
}
function windowResized()
{
  if (innerWidth != origWidth
      || innerHeight != origHeight) 
  {
    origWidth = innerWidth;
    origHeight = innerHeight;
    top.findFrame(top, 'por_main').location.reload();
  }
}

window.onResize=windowResized;

function addSpecialOrderToReq()
{
  addSpecialOrder("Req");
}
function addSpecialOrderToFavorite()
{
  addSpecialOrder("Fav");
}

function addSpecialOrder(dest)
{
  if (checkSubmitFlag())
    return;

  // bug 1028794
  // if(!top.checkTextArea("POR_SPECIAL_ORDER_GOODS_R")) return;


  var itemType= top.findFrame(top, "content_middle").document.selectedItemType;
  if(itemType== "goods")
  {
    top.allForms["POR_SPECIAL_ORDER_R"]=top.allForms["POR_SPECIAL_ORDER_GOODS_R"];
  // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_GOODS_R")) return;

    if (!top.checkMandatory("POR_SPECIAL_ORDER_GOODS_R")) return;
  }
  else if(itemType== "rate")
  {
    top.allForms["POR_SPECIAL_ORDER_R"]=top.allForms["POR_SPECIAL_ORDER_RATE_R"];
  // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_RATE_R")) return;

    if (!top.checkMandatory("POR_SPECIAL_ORDER_RATE_R")) return;
  }
  else if(itemType== "amount")
  {
    top.allForms["POR_SPECIAL_ORDER_R"]=top.allForms["POR_SPECIAL_ORDER_AMOUNT_R"];
  // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_AMOUNT_R")) return;
    if (!top.checkMandatory("POR_SPECIAL_ORDER_AMOUNT_R")) return;
  }

    var action="addSpecialItem";
    var func="addSpecialItem";
    var url="/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.Order";

    if(dest=="Fav")
    {
      var action="favListContent";
      var func="addToFav";
      var url="/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.FavList";
    }
  
//  setSubmitFlag();
  
  top.submitRequest("requisition",
                    url,
                    action,
                    func,
                    "",
                    new Array("POR_SPECIAL_ORDER_R"), 
                    "por_hidden");
}	  

function displaySearch()
{
  if(top.searchStyle == "advanced")
  {
    displayAdvancedSearch();
  }
  else //if(top.searchStyle == "simple")
  {
    displaySimpleSearch();
  }
}

function displayAdvancedSearch()
{   
  if (top.checkSubmitFlag())
    return;

  top.searchStyle="advanced";
  top.submitRequest("requisition",
                    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ReqsQueryOrder",
                    "viewReqsAdvancedSearch",
                    "viewReqsAdvancedSearch",
                    "",
                    new Array(), 
                    "por_content");
}


function displaySimpleSearch()
{   
  if (top.checkSubmitFlag())
    return;

  top.searchStyle="simple";

  top.setSubmitFlag();
  top.findFrame(top, "por_content").location=
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ReqsQueryOrder?REQ_TOKEN=template"+
    top.VALUE_DELIMITER+"requisition"+top.FIELD_DELIMITER+
    "function"+top.VALUE_DELIMITER+"viewReqsAutoQuery"+top.FIELD_DELIMITER+
    "action"+top.VALUE_DELIMITER+"viewReqsAutoQuery";

}

function displayReceiptsSearch()
{
  if (top.checkSubmitFlag())
    return;

  if (top.rcptSearchStyle =="advanced")
  {
   displayConfirmReceiptsAdvSearch();
  }
 else
  {
   top.rcptSearchStyle ="simple";
  top.findFrame(top, "por_content").location=
    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt?REQ_TOKEN=template"+
    top.VALUE_DELIMITER+"requisition"+top.FIELD_DELIMITER+
    "function"+top.VALUE_DELIMITER+"viewReceiptsAutoQuery"+top.FIELD_DELIMITER+
    "action"+top.VALUE_DELIMITER+"viewReceiptsAutoQuery";
  }
}


function displayConfirmReceiptsAdvSearch()
{
  if (top.checkSubmitFlag())
    return;

  top.searchStyle="advanced";
  top.submitRequest("requisition",
                    "/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.ConfirmReceipt",
                    "confirmReceiptsAdvSearch",
                    "confirmReceiptsAdvSearch",
                    "",
                    new Array(),
                    "por_content");
}


function pressOK()
 {	
    var hiddenFrame;
    var frame = top.getTop().findFrame(top,"confirmReceiptsContent");
    var action;
    var form = top.getTop().allForms["POR_RECEIVE_ORDERS_LINES_R"][0];
    top.ro_req_header_id = form.POR_REQ_HEADER_ID.value;
    top.ro_po_header_id = form.POR_PO_HEADER_ID.value;
    //top.ro_requester_id = form.POR_REQUESTER_ID.value;
    top.ro_exp_receipt_date = form.POR_RECEIPT_DATE.value;
    top.ro_dest_org_id = form.POR_ORGANIZATION_ID.value;

   if (top.currentPage == "RECEIVE_ORDERS")  {
	hiddenFrame = "por_hidden";
	action = "receipt_from_ro";
	}
   else if (top.currentPage == "WORKFLOW_NOTIFICATION") {
     hiddenFrame = "receive_orders_hidden";
     action = "receipt_from_wf";
   }
  else {
     hiddenFrame = "receive_orders_hidden";
     action = "receipt_from_vreqs";
   }

   if(linesChecked()) {
   top.getTop().submitRequest(
    "requisition",
    "/OA_JAVA_SERV/oracle.apps.icx.por.apps.ConfirmReceipt", // url
    action,                          // action
    "createReceipt",                          // function
    "",                                             // general Data
    new Array("POR_RECEIVE_ORDERS_LINES_R"),
  // formNames
    hiddenFrame);
  }
}

function linesChecked()
        {
          var forms= top.getTop().allForms["POR_RECEIVE_ORDERS_LINES_R"];
          if(forms)
          {
            for(var i=0; i<forms.length; i++)
            {
              if(forms[i].POR_RECEIVED && forms[i].POR_RECEIVED.checked)
                        return true;
             }
          }
          alert(top.getTop().FND_MESSAGES['ICX_POR_ALRT_MUST_SELECT_LINES']);
          return false;
        }

function changeMultipleLines()
         {
             if (linesChecked()) {
                top.getTop().openModalWindow("", "por_receipt_detail");
                top.getTop().submitRequest("requisition",
                      "/OA_JAVA_SERV/oracle.apps.icx.por.apps.ConfirmReceipt",
                      "receiptDetail",
                      "receiptDetail",
                      top.getTop().FIELD_DELIMITER + "detailIndex" +
                      top.getTop().VALUE_DELIMITER +
                      "-9999",
                      new Array(),
                      "por_receipt_detail");
                }
	 }

function addToCartWithAddDesc( item, pricenum, adddesc){
	if (top.checkSubmitFlag())
     	   return;
  	top.setSubmitFlag();
	 var detailIndex = top.itemDetailIndex;
	 var form = top.getTop().allForms['POR_FAVORITE_LIST_R'][detailIndex];
	 form.updated = true;
	 form.POR_SELECT_BOX.checked = true;
         top.submitRequest("requisition",
           "/" + top.JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.FavList",
           "addFavListItem", //action
           "addFavListItem", //function
           "",
           new Array("POR_FAVORITE_LIST_R"),
           "por_hidden");
	 form.POR_SELECT_BOX.checked = false;
  }



//**************************************************************
// Event handlers for Info Template
//**************************************************************
// function sumbitInfoTemplate() N
//(also need to update POR_LINES_R to indicate
// function cancel
// function infoTemplateConfirmed

// sys admin
// templateNameChange (back to middle tier.
// createNewTemplate (just clear all forms.
// okClicked (attribute sequence should be unique, 
// attributeSequenceChanged (must be positive int)
