/* $Header: czStack.js 115.5 2000/12/10 16:10:42 pkm ship     $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99   CK Jeyaprakash, K MacClay  Created.                    |
 |                                                                           |
 +===========================================================================*/

/**
* Class Stack
*/

function Stack ()
{
  this.entries = new Array ();
}

function Stack_push (stackEntry)
{
  var len = this.entries.length;
  this.entries[len] = stackEntry;
}

function Stack_pop ()
{
  var len = this.entries.length;
  if (len != 0) {
    var entry = this.entries[len-1];
    
    this.entries[len-1] = null;
    delete this.entries [len-1];
    this.entries.length--;
    
    return (entry);
  } else
    return null;
}

function Stack_peek ()
{
  return (this.entries[this.entries.length-1]);
}

function Stack_isEmpty ()
{
  return (this.entries.length == 0);
}

/**
* Public Methods
*/
Stack.prototype.push  = Stack_push;
Stack.prototype.pop   = Stack_pop;
Stack.prototype.peek  = Stack_peek;
Stack.prototype.isEmpty = Stack_isEmpty;
