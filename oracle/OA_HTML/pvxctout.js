//  $Header: pvxctout.js 115.8 2000/11/07 14:06:38 pkm ship      $
//===========================================================================+ |
// |      Copyright (c) 2000 Oracle Corporation, Redwood Shores, CA, USA       |
// |                         All rights reserved.                              |
// +===========================================================================+
// |  FILENAME                                                                 |
// |      pvxctout.js                                                          |
// |                                                                           |
// |  DESCRIPTION                                                              |
// |      Javascript library for Partner country timeout screen                |
// |                                                                           |  
// | NOTES                                                                     |
// |                                                                           |
// |  DEPENDENCIES                                                             |
// |                                                                           |
// |  HISTORY                                                                  |
// |                                                                           |
// |                                                                           |
// |       14-JUN-2000  VKAZHIPU          created			       |  
// |                                                                           |
// +==========================================================================*/

function go()
{
   document.location="pvxctout.jsp?toutType="+document.TimeoutSetUp.toutType.options[document.TimeoutSetUp.toutType.selectedIndex].value;
}

function statusGo()
{
   document.location="pvxststp.jsp?category="+document.pvxststp.category.value+"&userType="+document.pvxststp.userType.options[document.pvxststp.userType.selectedIndex].value;
}



//******************************************************************
// used to set the hidden values. These hidden values can be used at various place
// This can be used only in multirow block, which follows field naming
// conventions like fieldname.1 for first row, fieldname.2 for second row
//******************************************************************


function setHiddenField(thisForm,from) {
recNum = from;
var selValue="zzzzzzzzz";
formElements = document.forms[thisForm].elements;
numElements = formElements.length;

for (var i = 0; i<numElements; i++)  {

itemAddr = formElements[i].name;
addrArray = itemAddr.split('.');
if(addrArray[1]==recNum && addrArray[0] == 'newcountry')
{
for (var j=0; j < formElements[i].length;j++) {
if (formElements[i].options[j].selected == true) {
selValue=formElements[i].options[j].text;
	}
    }
}



if(addrArray[1]==recNum && addrArray[0] == 'hidefield')
{
 if (selValue!="") {
 formElements[i].value=1;
 }
 else
 {
  formElements[i].value=0;
  }
 
 }
 
 
}
}


function doSubmit(thisForm){
   var canSubmit = true;
   var message="\n";
   var rownum = -1;
   frmElements = document.forms["TimeoutSetUp"].elements;



   if (document.TimeoutSetUp["newcountry.0"].selectedIndex == 0 && document.TimeoutSetUp["newtimeout.0"].value != "")
   {
     alert("Please select a country.");
	canSubmit = false;
	return false;
   }
   if (document.TimeoutSetUp["newcountry.1"].selectedIndex == 0 && document.TimeoutSetUp["newtimeout.1"].value != "")
   {
     alert("Please select a country.");
	canSubmit = false;
	return false;
   }

   for(var i = 0; i< frmElements.length; i++)
   {
     itemAddr = frmElements[i].name;
     addrArray= itemAddr.split('.');
     if (addrArray[0]=='hidefield' && frmElements[i].value==1) 
	{
       rownum = addrArray[1];
     }


       if (frmElements[i].type == 'text' && addrArray[0] != 'newtimeout') 
	  {
           if (!isNumber(frmElements[i])) 
	      {
             canSubmit = false;
		   return false;
	      }
       }


     if (addrArray[1]==rownum) 
	{
       if (addrArray[0]=='newtimeout') 
	  {
	    if (!isNumber(frmElements[i]))
	    {
           canSubmit = false;
		 return false;
	    }
/*       
         if (frmElements[i].value =="") 
	    {
           alert("Please Enter Timeout Value");
           canSubmit = false;
           formElements[i].focus();
         }
*/
       }
       else if (addrArray[0]=='newcountry') 
	  {
         alert("Please select country");
	  }

     }
    
    } //for



	  if (canSubmit) {
	  thisForm.action = 'pvxctoup.jsp';
          thisForm.submit();
            }     
         }


function clearRow(thisForm, from) {
  
  recNum = from;
  
  formElements = document.forms[thisForm].elements;
  numElements = formElements.length;
  for (var i = 0; i<numElements; i++) {
    itemAddr = formElements[i].name;
    addrArray = itemAddr.split('.');
    if (addrArray[1]==recNum)
    { 
     type = formElements[i].type;
     if (type=='text' || type=='textarea' || type=='password' || type=='hidden' || type=='span')
        formElements[i].value = '';
      else if (type=='select-one' || type=='select-multiple' )
      {
        selectOptions = formElements[i].options;
        numOpts = selectOptions.length;
        for (var j = 0; j<numOpts; j++)
          if (selectOptions[j].value=='')
            selectOptions[j].selected = true;
      }
      else if (type=='checkbox' || type=='radio')
        formElements[i].checked = false; 
      else if (type=='button')  // there should probably be a longer list of things to ignore...
        ;
      else
        alert('Error: trying to reset element: ' + itemAddr + ' with type: ' + type);
    }
      
    }
    
}

function isNumber(inNumber) {

                var myval =inNumber.value;
                var numStr="0123456789";
                var charStr;
                var counter = 0;

                var i = 0;

                if (myval.length == 0)
			 {
                  alert("Please enter Timeout value");
			   inNumber.focus();
			   return false;
			 }

                for (i; i < myval.length ;  i++)
                {
                       charStr = myval.substring(i, i+1);
                       if (numStr.indexOf(charStr) != -1)
                           counter ++;
                }
                  if (counter != myval.length)
                  {
		          alert('Please enter a positive whole number.');

                        //inNumber.value="";
                        inNumber.focus();
				    return false;
                  }
			   return true;

  }  



function isNegative(inNumber)
{
  var myval =inNumber.value;
  if (myval < 0)
  {
    alert('Please enter a positive number.');
    //inNumber.value="";
    inNumber.focus();
  }
}

/*
function isNegative(inNumber) {
        var myval =inNumber.value;
        var numStr="0123456789";
        var charStr;
        var counter = 0;
        var i = 0;
                for ( i ; i < myval.length ;  i++)
                {
                        charStr = myval.substring(i, i+1);
                        if (numStr.indexOf(charStr) != -1)
                        counter ++;
                }
                if (counter != myval.length)
                {
 			alert('Only positive numbers allowed');
                        inNumber.value="";
                        inNumber.focus();
                }
  }    

*/

function submitUpdate(thisForm){
  
    thisForm.action = 'pvxctoup.jsp';
    thisForm.submit();
  
}

function validateStatusDuration()
{
    if (document.pvxststp["newDurationType.0"].selectedIndex == 0 && document.pvxststp["newfrom.0"].selectedIndex == 0)
    {
    }
    else if (document.pvxststp["newfrom.0"].selectedIndex == 0 && document.pvxststp["newDurationType.0"].selectedIndex != 0)
    {
      alert("Please select Status");
      return false;
    } 
    else if (document.pvxststp["newDurationType.0"].selectedIndex == 0 && document.pvxststp["newfrom.0"].selectedIndex != 0)
    {
      alert("Please select Duration Type");
      return false;
    }


    if (document.pvxststp["newDurationType.1"].selectedIndex == 0 && document.pvxststp["newfrom.1"].selectedIndex == 0)
    {
      return true; 
    }
    else if (document.pvxststp["newfrom.1"].selectedIndex == 0)
    {
      alert("Please select Status");
      return false;
    }
    else if (document.pvxststp["newDurationType.1"].selectedIndex == 0)
    {
      alert("Please select Duration Type");
      return false;
    }
    return true;
}


function validateStatusFromTo()
{
    if (document.pvxststp["newto.0"].selectedIndex == 0 && document.pvxststp["newfrom.0"].selectedIndex == 0) 
    {
    }
    else if (document.pvxststp["newfrom.0"].selectedIndex == 0 && document.pvxststp["newto.0"].selectedIndex != 0)
    {
      alert("Please select Status From");
      return false;
    }
    else if (document.pvxststp["newto.0"].selectedIndex == 0 && document.pvxststp["newfrom.0"].selectedIndex != 0)
    {
      alert("Please select Status To");
      return false;
    }


    if (document.pvxststp["newto.1"].selectedIndex == 0 && document.pvxststp["newfrom.1"].selectedIndex == 0)
    {
      return true;
    }
    else if (document.pvxststp["newfrom.1"].selectedIndex == 0)
    {
      alert("Please select Status From");
      return false;
    }
    else if (document.pvxststp["newto.1"].selectedIndex == 0)
    {
      alert("Please select Status To");
      return false;
    }
    return true;
}


