/*********************************************************************
 * Javascript functions for Personalization Framework for 11i
 *********************************************************************/
<!-- $Header: asfPerzFmk.js 115.7 2001/04/18 17:04:28 pkm ship    $ -->
function clearEmptyLOV(f,s) 
{
  for(i = 0; i < s;i++)
  {
    lf = document.forms[f].elements[lovIndexes[i]];
    if(lf.value == '') 
    {
      for(j = 0; j < lovIDIndexes[i].length; j++)
      {
        document.forms[f].elements[lovIDIndexes[i][j]].value = '';
      } 
    }
  }
}

function compareLOVValues(f,s) 
{
  for(i = 0; i < s; i++) 
  {
    uv = document.forms[f].elements[lovIndexes[i]].value;
    dv = lovFieldValues[i];
    if((uv != '')&&(uv != dv)) 
    {
      alert(lovNames[i]);
      return false;
    }
  }
  return true;
}

// deprecated. use chkQuery() instead
function checkQuery(errorMsg,f,s) 
{
  var re = /\s*/
  qn = document.forms[f].elements[s+'.OBJ.NAME'].value;
  qn = qn.replace(re,"")  
  if(qn == "") {
    alert(errorMsg);
    return false;
  } else {
    return true;
  }
}

function chkQuery(f,s)
{
  var re = /\s*/
  qn = document.forms[f].elements[s+'.OBJ.NAME'].value;
  qn = qn.replace(re,"")  
  if(qn == "") {
    return false;
  } else {
    return true;
  }
}

// deprecated. don't use
function clearAll(f,s)
{
  elm = document.forms[f].elements;
  for(i = 0; i < elm.length;i++)
  {
    nm = elm[i].name;
    if(isCondition(nm,s) == "Y") 
    {
      if(elm[i].type == "text")
      {
        elm[i].value = "";
      }
      if(elm[i].type == "select-one")
      {
        elm[i].selectedIndex = 0;
      }
      if(elm[i].type == "select-multiple") 
      {
        elm[i].selectedIndex = 0;
      }
      if(elm[i].type == "hidden")
      {
        elm[i].value = "";
      }
      if(elm[i].type == "radio")
      {
        if(elm[i].defaultChecked == true)
        {
          elm[i].checked = true;
        } 
        else 
        {
          elm[i].checked = false;
        }
      }
      if(elm[i].type == "checkbox")
      {
        elm[i].checked = false;
      }
    }
    if(isOrder(nm,s) == "Y")
    {
      if(elm[i].type == "select-one")
      {
        elm[i].selectedIndex = 0;
      }
    }
    if(isSequence(nm,s) == "Y")
    {
      if(elm[i].type == "select-one")
      {
        elm[i].selectedIndex = 0;
      }
    }
    if(isName(nm,s) == "Y")
    {
      elm[i].value = "";
    }
    if(isDefault(nm,s) == "Y")
    {
      elm[i].checked = false;
    }
  }
}

function isCondition(nm,s) 
{
  if(nm.indexOf(s+".CONDITION.VALUE") == -1)
  {  
    return "N";
  } 
  else 
  {
    return "Y"; 
  }
}

function isName(nm,s)
{
  if(nm.indexOf(s+".OBJ.NAME") == -1)
  {  
    return "N";
  } 
  else 
  {
    return "Y"; 
  }
}

function isDefault(nm,s)
{
  if(nm.indexOf(s+".OBJ.DEFAULT") == -1)
  {  
    return "N";
  } 
  else 
  {
    return "Y"; 
  }
}

function isOrder(nm,s)
{
  if(nm.indexOf(s+".ORDER.NAME") == -1)
  {  
    return "N";
  } 
  else 
  {
    return "Y"; 
  }
}

function isSequence(nm,s)
{
  if(nm.indexOf(s+".ORDER.SEQUENCE") == -1)
  {  
    return "N";
  } 
  else 
  {
    return "Y"; 
  }
}

function chkClear(f,s) 
{
  cont = false;
  elm = document.forms[f].elements;
  for(i = 0; i < elm.length; i++) 
  {
    nm = elm[i].name;
    if(isCondition(nm,s) == "Y") 
    {
      if((elm[i].type == "radio")&&(elm[i].value == ""))
      {
        for(j = 0; j < elm.length; j++) 
        {
          if(nm == elm[j].name) 
          {
            if((elm[j].value != "")&&(elm[j].checked == true))
            { 
              cont = true;
            } 
          }
        }
      }
    }  
  }

  for(i = 0; i < elm.length;i++)
  {
    nm = elm[i].name;
    if(isCondition(nm,s) == "Y") 
    {
      if(elm[i].type == "text")
      {
        if(elm[i].value != "")
        {
          cont = true;
        }
      }
      if(elm[i].type == "select-one")
      {
        if(elm[i].selectedIndex != 0)
        {
          cont = true;
        }
      }
      if(elm[i].type == "select-multiple") 
      {
        if(elm[i].selectedIndex != 0)
        {
          cont = true;
        }
      }
      if(elm[i].type == "hidden")
      {
        if(elm[i].value != "")
        {
          cont = true;
        }
      }
      if(elm[i].type == "checkbox")
      {
        if(elm[i].checked != false)
        {
          cont = true;
        }
      }
    }
  }
  
  return cont;
}

// deprecated. use chkClear() instead
function checkClear(f,s)
{
  cont = false;
  elm = document.forms[f].elements;
  for(i = 0; i < elm.length; i++) 
  {
    nm = elm[i].name;
    if(isCondition(nm,s) == "Y") 
    {
      if((elm[i].type == "radio")&&(elm[i].value == ""))
      {
        for(j = 0; j < elm.length; j++) 
        {
          if(nm == elm[j].name) 
          {
            if((elm[j].value != "")&&(elm[j].checked == true))
            { 
              cont = true;
            } 
          }
        }
      }
    }  
  }

  for(i = 0; i < elm.length;i++)
  {
    nm = elm[i].name;
    if(isCondition(nm,s) == "Y") 
    {
      if(elm[i].type == "text")
      {
        if(elm[i].value != "")
        {
          cont = true;
        }
      }
      if(elm[i].type == "select-one")
      {
        if(elm[i].selectedIndex != 0)
        {
          cont = true;
        }
      }
      if(elm[i].type == "select-multiple") 
      {
        if(elm[i].selectedIndex != 0)
        {
          cont = true;
        }
      }
      if(elm[i].type == "hidden")
      {
        if(elm[i].value != "")
        {
          cont = true;
        }
      }
      if(elm[i].type == "checkbox")
      {
        if(elm[i].checked != false)
        {
          cont = true;
        }
      }
    }
  }
  
  return cont;
}
var DOR$_req = new Array("");
var DOR$_count = 0;
var DOR$_scaleNames = new Array("");
var DOR$_scaleValues = new Array("");
// generic functions, should go into the JS library
function doSubmit(ind, box, fm, sname)
{
  for(i = 1; i < ind + 1;i++)
  {
    if(box.length >= i) 
    {
      fm.elements[sname+'.OPTION.NAME'+i].value = box.options[i-1].value;
      if(fm.elements[sname+'.OPTION.SCALE'+i])
        fm.elements[sname+'.OPTION.SCALE'+i].value = '';
      for(j = 1; j < DOR$_scaleNames.length; j++) {
        if(box.options[i-1].value == DOR$_scaleNames[j]) {
          sb = fm.elements['DOR$_scaleValue'+DOR$_scaleValues[j]];
          fm.elements[sname+'.OPTION.SCALE'+i].value =
              sb[sb.selectedIndex].value;
        }
      }
    } else
    {
      fm.elements[sname+'.OPTION.NAME'+i].value = '';
      if(fm.elements[sname+'.OPTION.SCALE'+i])
        fm.elements[sname+'.OPTION.SCALE'+i].value = '';
    }
  }
}
function moveUp(box)
{
  ind = box.selectedIndex;
  if((box.selectedIndex >= 0)&&(box.selectedIndex != 0))
  {
    temp1text = box.options[box.selectedIndex-1].text;
    temp1value = box.options[box.selectedIndex-1].value;
    temp2text = box.options[box.selectedIndex].text;
    temp2value = box.options[box.selectedIndex].value;
    box.options[box.selectedIndex-1] = new Option(temp2text, temp2value);
    box.options[box.selectedIndex] = new Option(temp1text, temp1value);
    box.selectedIndex = ind-1;
  }
}

function moveDown(box)
{
  ind = box.selectedIndex;
  if((box.selectedIndex >= 0) &&(box.selectedIndex != (box.length-1))) 
  {
    temp1text = box.options[box.selectedIndex].text;
    temp1value = box.options[box.selectedIndex].value;
    temp2text = box.options[box.selectedIndex+1].text;
    temp2value = box.options[box.selectedIndex+1].value;
    box.options[box.selectedIndex+1] = new Option(temp1text, temp1value);
    box.options[box.selectedIndex] = new Option(temp2text, temp2value);
    box.selectedIndex = ind+1;
  }
}

function move(from, to, preventRequired)
{
  while (from.selectedIndex >= 0) {
    var moveIt = true;
    selected = from.options[from.selectedIndex];
    if(preventRequired==true) {
      for(var i=0; i<DOR$_req.length; i++) {
        if(DOR$_req[i]==selected.value) {
          moveIt = false;
        }
      }
    }
    if(moveIt==true) {
      to.options[to.length] = new Option(selected.text, selected.value);
      from.options[from.selectedIndex] = null;
    } else {
      from.options[from.selectedIndex].selected = false;
    }
  }
}

function moveAll(from, to, preventRequired)
{
  if(preventRequired == false) {
    for (i = from.length-1 ; i >= 0; i--)
    {
      selected = from.options[i];
      to.options[to.length] = new Option(selected.text, selected.value);
      from.options[i] = null;
    }
  } else {
    for (i = from.length-1 ; i >= 0; i--)
    {
      var moveIt = true;
      selected = from.options[i];
      for(var j=0; j<DOR$_req.length; j++) {
        if(DOR$_req[j]==selected.value) {
          moveIt = false;
        }
      }
      if(moveIt == true) {
        to.options[to.length] = new Option(selected.text, selected.value);
        from.options[i] = null;
      }
    }
  }
}

