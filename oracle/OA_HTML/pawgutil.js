//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawgutil.js                       |
//  |                                                |
//  | Description: Utility functions in web time     |
//  |                                                |
//  | History:     Created by jfeng 07/22/1999       |
//  |                                                |
//  +================================================+
/* $Header: pawgutil.js 115.18 2001/03/23 17:40:43 pkm ship      $ */

// Returns day of week given an int.  1=Sun, 2=Mon, etc.
function fIntToDayOfWeek(p_iDay) {
  if ((p_iDay < 0) || (p_iDay >= C_iWEEKDAYS))
    return "";
  return g_arrstrWeekDayAbbr[p_iDay];
}

// Returns a month given an int.  1=January, 2=February, etc.
function fIntToMonthFull(p_iMonth) {
  if ((p_iMonth < 0) || (p_iMonth >= C_iMONTHS))
    return "";
  return g_arrstrMonthFull[p_iMonth];
}

// Returns a month abbreviation given an int.  1=Jan, 2=Feb, etc.
function fIntToMonth(p_iMonth) {
  if ((p_iMonth < 0) || (p_iMonth >= C_iMONTHS))
    return "";
  return g_arrstrMonth[p_iMonth];
}

// Returns a month given a date.
function fDateToMonthFull(p_dDate) {
  return fIntToMonthFull(p_dDate.getMonth());
}

// Returns a month abbreviation given a date.
function fDateToMonth(p_dDate) {
  return fIntToMonth(p_dDate.getMonth());
}

//  ---------------------------------------------------------------------------
//  Function: fDateToDayOfWeek
//  Description: Given a date, return the short Day of the week that it
//		corresponds to
//  ---------------------------------------------------------------------------
function fDateToDayOfWeek(p_dDate) {
  return fIntToDayOfWeek(p_dDate.getDay());
}

//  ---------------------------------------------------------------------------------
//  Function: fDateAddDay
//  Description: Given a date and the number of days wanted to be
//               added.  Return a date object.
//
//  ---------------------------------------------------------------------------------
function fDateAddDay(p_dDate, p_iNumOfDay){
var l_dReturnDay;

  if (p_iNumOfDay == 0) {
    l_dReturnDay = new Date(p_dDate.getTime());
  }
  else {
    l_dReturnDay = new Date(p_dDate.getTime() + (p_iNumOfDay * 86400000));
  }

  // The following is used to handle daylight saving
  // 1. When it is one hour ahead ie in April
  if (l_dReturnDay.getHours() == 23){
    l_dReturnDay = new Date(l_dReturnDay.getTime() + 3600000);
  }
  // 2. When it is one hour behind ie in October
  else if (l_dReturnDay.getHours() == 1){
    l_dReturnDay = new Date(l_dReturnDay.getTime() - 3600000);
  }

  return l_dReturnDay;

}


function fDayOfWeekToDate(p_nDayOfWeek)
{
  for ( var i=0; i<g_dWeekEndingWeek.length; i++)
  {
     if ( fDateToDayOfWeek(g_dWeekEndingWeek[i]) == fIntToDayOfWeek(p_nDayOfWeek) )
       return g_dWeekEndingWeek[i];
  }
}



//  ---------------------------------------------------------------------------------
//  Function: fDateAddWeek
//  Description: Given a date object and the number of weeks wanted to
//               be added.  Return a date object.
//  ---------------------------------------------------------------------------------
function fDateAddWeek(p_dDate, p_iNumOfWeek){
var l_dReturnDay;

//alert('p_dDate : ' + p_dDate);
  if (p_iNumOfWeek == 0) {
    l_dReturnDay = new Date(p_dDate.getTime());
  }
  else {
    l_dReturnDay = new Date(p_dDate.getTime() + (p_iNumOfWeek * 604800000));
  }

  // The following is used to handle daylight saving
  // 1. When it is one hour ahead ie in April
  if (l_dReturnDay.getHours() == 23){
    l_dReturnDay = new Date(l_dReturnDay.getTime() + 3600000);
  }
  // 2. When it is one hour behind ie in October
  else if (l_dReturnDay.getHours() == 1){
    l_dReturnDay = new Date(l_dReturnDay.getTime() - 3600000);
  }

  return l_dReturnDay;

}

//  ---------------------------------------------------------------------------------
//  Function: fDateToLongString
//  Description: Given the format mask and a date, return a formatted
//               string
//  Parameters:  p_strFormatString - format mask
//               p_objDate         - date wanted to be formatted
//               p_bFormatDay      - flag to notify whether padding
//                                   the day is desirable.
//
//  ---------------------------------------------------------------------------------
function fDateToLongString(p_strFormatString, p_objDate, p_bFormatDay){

  var l_strMonth = fIntToMonth(p_objDate.getMonth());
  var l_iDay     = p_objDate.getDate();
  var l_iMonth   = p_objDate.getMonth()+1;
  var l_iYear    = p_objDate.getFullYear();

  if(p_bFormatDay){
    if(l_iDay < 10) l_iDay = '0' + l_iDay;
  }

  if(l_iMonth < 10) l_iMonth = '0' + l_iMonth;

  if (p_strFormatString == 'DD-MM-RRRR')
    return(l_iDay + '-' + l_iMonth + '-' + l_iYear);
  else if (p_strFormatString == 'DD-MON-RRRR')
    return(l_iDay + '-' + l_strMonth + '-' + l_iYear);
  else if (p_strFormatString == 'DD.MM.RRRR')
    return(l_iDay + '.' + l_iMonth + '.' + l_iYear);
  else if (p_strFormatString == 'DD.MON.RRRR')
    return(l_iDay + '.' + l_strMonth + '.' + l_iYear);
  else if (p_strFormatString == 'DD/MM/RRRR')
    return(l_iDay + '/' + l_iMonth + '/' + l_iYear);
  else if (p_strFormatString == 'DD/MON/RRRR')
    return(l_iDay + '/' + l_strMonth + '/' + l_iYear);
  else if (p_strFormatString == 'MM-DD-RRRR')
    return(l_iMonth + '-' + l_iDay + '-' + l_iYear);
  else if (p_strFormatString == 'MM.DD.RRRR')
    return(l_iMonth + '.' + l_iDay + '.' + l_iYear);
  else if (p_strFormatString == 'MM/DD/RRRR')
    return(l_iMonth + '/' + l_iDay + '/' + l_iYear);
  else if (p_strFormatString == 'RRRR-MM-DD')
    return(l_iYear + '-' + l_iMonth + '-' + l_iDay);
  else if (p_strFormatString == 'RRRR-MON-DD')
    return(l_iYear + '-' + l_strMonth + '-' + l_iDay);
  else if (p_strFormatString == 'RRRR.MM.DD')
    return(l_iYear + '.' + l_iMonth + '.' + l_iDay);
  else if (p_strFormatString == 'RRRR.MON.DD')
    return(l_iYear + '.' + l_strMonth + '.' + l_iDay);
  else if (p_strFormatString == 'RRRR/MM/DD')
    return(l_iYear + '/' + l_iMonth + '/' + l_iDay);
  else if (p_strFormatString == 'RRRR/MON/DD')
    return(l_iYear + '/' + l_strMonth + '/' + l_iDay);

}

//  ---------------------------------------------------------------------------------
//  Function: fDateToShortString
//  Description: Given the format mask and a date, return a formatted
//               string
//  Parameters:  p_strFormatString - format mask
//               p_objDate         - date wanted to be formatted
//               p_bFormatDay      - flag to notify whether padding
//                                   the day is desirable.
//
//  ---------------------------------------------------------------------------------
function fDateToShortString(p_strFormatString, p_objDate, p_bFormatDay){

  var l_strMonth = fIntToMonth(p_objDate.getMonth());
  var l_iDay     = p_objDate.getDate();
  var l_iMonth   = p_objDate.getMonth()+1;

  if(p_bFormatDay){
    if(l_iDay < 10) l_iDay = '0' + l_iDay;
  }

  if(l_iMonth < 10) l_iMonth = '0' + l_iMonth;

  if (p_strFormatString == 'DD-MM-RRRR')
    return(l_iDay + '-' + l_iMonth);
  else if (p_strFormatString == 'DD-MON-RRRR')
    return(l_iDay + '-' + l_strMonth);
  else if (p_strFormatString == 'DD.MM.RRRR')
    return(l_iDay + '.' + l_iMonth);
  else if (p_strFormatString == 'DD.MON.RRRR')
    return(l_iDay + '.' + l_strMonth);
  else if (p_strFormatString == 'DD/MM/RRRR')
    return(l_iDay + '/' + l_iMonth);
  else if (p_strFormatString == 'DD/MON/RRRR')
    return(l_iDay + '/' + l_strMonth);
  else if (p_strFormatString == 'MM-DD-RRRR')
    return(l_iMonth + '-' + l_iDay);
  else if (p_strFormatString == 'MM.DD.RRRR')
    return(l_iMonth + '.' + l_iDay);
  else if (p_strFormatString == 'MM/DD/RRRR')
    return(l_iMonth + '/' + l_iDay);
  else if (p_strFormatString == 'RRRR-MM-DD')
    return(l_iMonth + '-' + l_iDay);
  else if (p_strFormatString == 'RRRR-MON-DD')
    return(l_strMonth + '-' + l_iDay);
  else if (p_strFormatString == 'RRRR.MM.DD')
    return(l_iMonth + '.' + l_iDay);
  else if (p_strFormatString == 'RRRR.MON.DD')
    return(l_strMonth + '.' + l_iDay);
  else if (p_strFormatString == 'RRRR/MM/DD')
    return(l_iMonth + '/' + l_iDay);
  else if (p_strFormatString == 'RRRR/MON/DD')
    return(l_strMonth + '/' + l_iDay);
  
}


//  ---------------------------------------------------------------------------
//  Function: fFormatDecimals
//  Description: Given an input string, return with decimals  
//
//  --------------------------------------------------------------------------
function fJsIndexToGlobalWeekDate(p_iIndex){
  for (i=0; i<top.g_dWeekEndingWeek.length; i++)
    if (top.g_dWeekEndingWeek[i].getDay() == p_iIndex)
      return top.g_dWeekEndingWeek[i];
}


//  ---------------------------------------------------------------------------
//  Function: fFormatDecimals
//  Description: Given an input string, return with decimals  
//
//  ---------------------------------------------------------------------------
function fFormatDecimals(p_strInputString){
  var str = p_strInputString + '';
  var prefix = '';
  var integral = '';
  var decimal = '';

  if (str != ''){
    if (str.indexOf('-') == 0) {                // negative
        prefix = '-';
        str = str.substr(1, str.length-1);
    } else {                                    // positive
        prefix = '';
    }
    if (str.indexOf('.') != -1) {               // w/ decimal
        integral = str.substr(0, str.indexOf('.')) + '';
        if (integral == '') {                   // no integral
                integral = '0';
        }
        str = str.substr(str.indexOf('.')+1, 2);
        if (str.length == 1) {                  // w/o hundredth
                decimal = str + '0';
        } else {                                // w/ hundredth
                decimal = str;
        }
    } else if (str.indexOf('.') == -1) {        // w/o decimal
        integral = str;
        decimal = '00';
    }
    return (prefix + integral + '.' + decimal);
  }
  else
    return '';
}

//----------------------------------------------------
// function: fGetInputWidget
// Description: Return the radio button by the specified index
//-----------------------------------------------------
function fGetInputWidget(p_formDisplay, p_strWidgetName, p_iIndex)
{
  return eval( "p_formDisplay." + p_strWidgetName + p_iIndex );
}

//----------------------------------------------------
// function: fGetLineWidget
// Description: Return line widget used in Enter Hours page
//-----------------------------------------------------
function fGetLineWidget(p_formDisplay, p_strWidgetName, p_iXIndex, p_iYIndex)
{
  return eval( 'p_formDisplay.' + p_strWidgetName + 'X' + p_iXIndex + 'Y' + p_iYIndex );
}

//  ---------------------------------------------------------------------------------
//  Function: fIsNum
//  Description:  Check the input entry to ensure it is numeric
//
//  ---------------------------------------------------------------------------------
function fIsNum(p_strHourEntry){
  var ch=p_strHourEntry.substring(0,1);

  // if the entry has been cleared out, no validation is required.
  if (p_strHourEntry.length == 0) return true;
 
  if ((ch<"0" || "9"<ch) && (ch != ".") && (ch != "-")){
      return false;
  }
  for (var i=1; i<p_strHourEntry.length; i++){
    var ch=p_strHourEntry.substring(i,i+1)
    if ((ch<"0" || "9"<ch) && ch != "."){
      return false;
    }
  }
  // xx.xx.xx and xx. case
  if ((p_strHourEntry.indexOf(".") != p_strHourEntry.lastIndexOf(".")) || 
      (p_strHourEntry.lastIndexOf(".") == (p_strHourEntry.length - 1))){
      return false;
  }
  // -. case
  if (p_strHourEntry == "-."){
      return false;
  }
  // 00xx case
  if ((p_strHourEntry.length > 1) && (p_strHourEntry.substring(0,1) == "0") && 
      (p_strHourEntry.indexOf(".") == -1)){
      return false;
  }

  return true;
}

// This routine is called by the RenderTimecard PLSQL function, and controls
// the initialization of the screen.
function fControlSSToCSInit(p_strMode, p_strAction) {

  // Initialize global variable g_strCurrentAction here
  g_strCurrentAction = p_strAction;
  
  if ((p_strMode == C_strENTER_HOURS) && 
      ((p_strAction == C_strNEW) ||      		// New timecard 
       (p_strAction == C_strMODIFY) || 			// Modify timecard from Save Confirmation
       (p_strAction == C_strMODIFYMODIFY) ||            // Modify timecards from Modify Timecards
       (p_strAction == C_strMODIFYREVERSE) ||           // Modify reverse timecard       
       (p_strAction == C_strMODIFYMODIFYREVERSE) ||           // Modify reverse timecard from Modify Timecards     
       (p_strAction == C_strHISTORYCOPY) ||		// Copy timecard from Timecard History
       (p_strAction == C_strMODIFYCOPY))) {		// Copy timecard from Modify Timecards


     fDrawFrameSetPage(self, "AUTO",
                       "Javascript:top.fDrawTimeEntry()",
                       null,
		       null,  
                       top.C_strENTER_HOURS);
  }

  else if ((p_strMode == C_strREVERSE) && 
      (p_strAction == C_strREVERSE)) {		// Copy timecard from Modify Timecards

     fDrawFrameSetPage(self, "AUTO",
                       null,
                       null,
		       null,  
                       top.C_strREVERSE);
  }
 
  else if (p_strMode == C_strVIEW_ONLY) {
     fDrawFrameSetPage(self, "AUTO",
                       "javascript:top.fControlSSToCSContinue(top.g_objTimecard, top.g_objErrors, top.C_strVIEW_ONLY, top.C_strVIEW_ONLY, top.g_objAuditHistory, top.g_objDeletedAuditHistory)",
                       null,
		       null,  
                       top.C_strVIEW_ONLY);
  }

  else if (p_strMode == C_strFINAL_REVIEW) {
     fDrawFrameSetPage(self, "AUTO",
                       "javascript:top.fControlSSToCSContinue(top.g_objTimecard, top.g_objErrors, top.C_strFINAL_REVIEW, top.C_strFINAL_REVIEW, top.g_objAuditHistory, top.g_objDeletedAuditHistory)",
                       null,  
		       null,
                       top.C_strFINAL_REVIEW);
  }

  else if ( p_strMode == top.C_strUPLOAD ) { // Draw Upload page.
     fDrawFrameSetPage(self, "AUTO",
                       "javascript:top.fInitUpload()",
                       null,  
                       null,  
                       top.C_strUPLOAD);
  }
  else if ( p_strMode == top.C_strAUDIT ) { // Draw Audit Information Screen
     fDrawFrameSetPage(self, "AUTO",
                       "",
                       null,
                       null,
                       top.C_strAUDIT);
  }

  else {
    alert("fControlSSToCSInit: Fatal error cannot recognize action:" + p_strAction + " p_strMode: " + p_strMode);
  }
}

// LOV result callback functions

function fOnClickDefApprLOVResult() {
  
  top.g_formLOV.txtDefApprName.value = top.g_formLOV.PA_WEB_APPR_NAME.value;

  top.g_formLOV.hidDefApprID.value =  top.g_formLOV.PA_WEB_APPR_ID.value;
     

  if (g_strFormLOV == C_strHEADER) {
    fOnChangeApprover(framMainBody.framBodyHeader.document.formEntryBodyHeader);
  }
}

function fOnClickProjectLOVResult() {
var l_bLOVSelect = true;
var l_winDetail;

  if (g_strFormLOV == C_strPREFERENCES) {
    fGetInputWidget(top.g_formLOV, "txtProjNum", g_iProjLOVCurLine).value = 
	top.g_formLOV.PA_WEB_PROJECT_NUMBER.value;
    fGetInputWidget(g_formLOV, "hidProjID", g_iProjLOVCurLine).value = 
	top.g_formLOV.PA_WEB_PROJECT_ID.value;
  }
  else if (g_strFormLOV == C_strENTER_HOURS) {
    fGetInputWidget(top.g_formLOV, "txtProjNum", g_iProjLOVCurLine).value = 
	top.g_formLOV.PA_WEB_PROJECT_NUMBER.value;
    fGetInputWidget(g_formLOV, "hidProjID", g_iProjLOVCurLine).value = 
	top.g_formLOV.PA_WEB_PROJECT_ID.value;

    fOnChangeProject(framMainBody.framBodyContent.document.formEntryBodyContent, l_bLOVSelect);
  }
  else if (g_strFormLOV == C_strDETAILS) {
    l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.txtProjNum.value = 
	top.g_formLOV.PA_WEB_PROJECT_NUMBER.value;
    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.hidProjID.value = 
	top.g_formLOV.PA_WEB_PROJECT_ID.value;

    fOnChangeDetailProject(l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader, l_bLOVSelect);
  }

    g_iProjLOVCurLine = null;
}

function fOnClickTaskLOVResult() {
var l_bLOVSelect = true;
var l_winDetail;

  if (g_strFormLOV == C_strPREFERENCES) {
    fGetInputWidget(top.g_formLOV, "txtTaskNum", g_iTaskLOVCurLine).value = 
	top.g_formLOV.PA_WEB_TASK_NUMBER.value;
    fGetInputWidget(top.g_formLOV, "hidTaskID", g_iTaskLOVCurLine).value = 
	top.g_formLOV.PA_WEB_TASK_ID.value;
  }
  else if (g_strFormLOV == C_strENTER_HOURS) {
    fGetInputWidget(top.g_formLOV, "txtTaskNum", g_iTaskLOVCurLine).value = 
	top.g_formLOV.PA_WEB_TASK_NUMBER.value;
    fGetInputWidget(top.g_formLOV, "hidTaskID", g_iTaskLOVCurLine).value = 
	top.g_formLOV.PA_WEB_TASK_ID.value;
    fOnChangeTask(framMainBody.framBodyContent.document.formEntryBodyContent, l_bLOVSelect);
  }
  else if (g_strFormLOV == C_strDETAILS) {
    l_winDetail = g_objModalWins.mGetobjLastModalWin().mGetwinModal();

    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.txtTaskNum.value =
	top.g_formLOV.PA_WEB_TASK_NUMBER.value;
    l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader.hidTaskID.value =
	top.g_formLOV.PA_WEB_TASK_ID.value;
    fOnChangeDetailTask(l_winDetail.framMainBody.framBodyHeader.document.formDetailBodyHeader, l_bLOVSelect);
  }
    g_iTaskLOVCurLine = null;
}

function fSafeURL(p_strURL) {
var strURL;
var objMsg = new Message();

  // Encoding these reserved and unsafe characters in your URL.
  // Note: "%" encoding has to be the first one.

  strURL = objMsg.fstrReplToken(p_strURL, "%", "%25");

  strURL = objMsg.fstrReplToken(strURL, "&", "%26"); 

  strURL = objMsg.fstrReplToken(strURL, ";", "%3B");

  strURL = objMsg.fstrReplToken(strURL, "/", "%2F");

  strURL = objMsg.fstrReplToken(strURL, "?", "%3F");

  strURL = objMsg.fstrReplToken(strURL, ":", "%3A");

  strURL = objMsg.fstrReplToken(strURL, "@", "%40");

  strURL = objMsg.fstrReplToken(strURL, "=", "%3D");

  strURL = objMsg.fstrReplToken(strURL, "<", "%3C");

  strURL = objMsg.fstrReplToken(strURL, ">", "%3E");

  strURL = objMsg.fstrReplToken(strURL, "\"", "%22");

  strURL = objMsg.fstrReplToken(strURL, "#", "%23");

  strURL = objMsg.fstrReplToken(strURL, "{", "%7B");

  strURL = objMsg.fstrReplToken(strURL, "}", "%7D");

  strURL = objMsg.fstrReplToken(strURL, "|", "%7C");

  strURL = objMsg.fstrReplToken(strURL, "\\", "%5C");

  strURL = objMsg.fstrReplToken(strURL, "^", "%5E");

  strURL = objMsg.fstrReplToken(strURL, "~", "%7E");

  strURL = objMsg.fstrReplToken(strURL, "[", "%5B");

  strURL = objMsg.fstrReplToken(strURL, "]", "%5D");

  strURL = objMsg.fstrReplToken(strURL, "'", "%60");

  return strURL;
}

function fIsUpperCaseOnly(p_strInput) {
  var c;
  for(var index=0; index < p_strInput.length; index++) {
    c = p_strInput.charAt(index);
    if((c>="a")&&(c<="z"))
      return false;
  }
  return true;
}

function fIsNumericOnly(p_strInput) {
  var c;
  for(var index=0; index < p_strInput.length; index++) {
    c = p_strInput.charAt(index);
    if (!(((c >= "0")&&(c <= "9")) ||
          (c == "+") ||
          (c == "-") ||
          (c == ",") ||
          (c == ".")))
      return false;
  }
  return true;
}

function fIsNetscape() {
  return (navigator.appName == "Netscape");
}

function fIsExplorer() {
  return (navigator.appName == "Microsoft Internet Explorer");
}

function fIsIE5() {
  if ((navigator.appName == "Microsoft Internet Explorer") && 
	(navigator.appVersion.indexOf("MSIE 5.0")))
    return true;
  else 
    return false;
}

function fIsWindows95(){
  return (navigator.appVersion.indexOf("Windows 95"));
}

function fReplaceSpaceWithPlus(p_strValue)
{
  var strResult = "";
  var arrStrings = p_strValue.split(' ');
  for ( var i=0; i<arrStrings.length-1; i++ )
  {
    strResult += arrStrings[i] + "+";
  }
  
  strResult += arrStrings[arrStrings.length-1];

  return strResult;
}

