/* $Header: APWITEMZ.js 115.25 2001/03/02 15:35:23 pkm ship      $ */
//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
/*
	Filename:	itemize.js
	Author:	Ron Langi

	Description:	R11i Receipt Itemization
			This code will reside in apwutili.js
			to invoke itemization:
				top.fOpenItemize(mrLine,receiptIndex,isOOP);
			where:
				mrLine is the line in multi-row
				receiptIndex is the index into the receipt array
				isOOP is true if out of pocket; else false

	server dependencies:
	--------------------
	AK prompts
	FND messages

	open items:
	-----------
	should set g_iItemizeSequence when restoring an expense report
	add itemizeId to ap_expense_report_lines_all
	cosmetic changes based on new UI from Chip

code review changes:
--------------------
fOpenItemizeWindow(mrLine,receiptIndex,isOOP)
added fValidateItem(item)
remove fTranslateMRLine(mrLine,isOOP)
remove unnecessary eval()s
eval(top.fMoneyFormat("0.005"))
use fDynamicButton() need cabo for OK/Cancel
move itemization code to itemize window

*/
        // Globals
	g_iItemizeSequence = 1000;		// itemizeId sequence used for grouping sibling receipts
	g_arrItemizeReceipts = null;		// array of sibling receipts in itemize window
        g_selectedItemizeLine = 0; 		// keeps track of the line nuber of the selected line for select checkbox

	        
//-----------------------------------------------------------------------------
// Function: fOpenItemizeWindow
// Desc:	given a multi-row line number, index to the receipt array,
//		and whether the receipt being itemized is out of pocket or not,
//		open the itemize window
//-----------------------------------------------------------------------------
function fOpenItemizeWindow(mrLine,receiptIndex,isOOP) {
	// messages used in itemize window
	strWinItemizeMsg1 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_NEED_RECEIPT');
	strWinItemizeMsg2 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_NEED_DATE');
	strWinItemizeMsg3 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_NEED_AMOUNT');
	strWinItemizeMsg4 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_EXCEED');
	strWinItemizeMsg5 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_CLEAR_ALL');
	strWinItemizeMsg6 = top.g_objMessages.mstrGetMesg('AP_WEB_ITEMIZE_UNDER');

	// regions used in itemize window
	var tmpRegion = top.g_objRegions.mobjGetRegion('AP_WEB_ITEMIZE');

	// prompts used in itemize window
	strAP_WEB_ITMZ_TITLE = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_TITLE').mstrGetPrompt();
	strAP_WEB_ITMZ_HEADER = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_HEADER').mstrGetPrompt();
	strAP_WEB_ITMZ_EXCLUDE = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_EXCLUDE').mstrGetPrompt();
	strAP_WEB_ITMZ_EXPENSE_TYPE = tmpRegion.mobjGetRegionItem('AP_WEB_EXPTYPE').mstrGetPrompt();
	strAP_WEB_ITMZ_DAILY_RATE = tmpRegion.mobjGetRegionItem('AP_WEB_DAILYRATE').mstrGetPrompt();
	strAP_WEB_ITMZ_NO_OF_TIMES = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_NO_OF_TIMES').mstrGetPrompt();
	strAP_WEB_ITMZ_START_DATE = tmpRegion.mobjGetRegionItem('AP_WEB_START_DATE').mstrGetPrompt();
	strAP_WEB_ITMZ_RECEIPT_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_RECAMT2').mstrGetPrompt();
	strAP_WEB_ITMZ_REIMBURS_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_REIMBURSABLE_AMT').mstrGetPrompt();
	strAP_WEB_ITMZ_BUSINESS_EXPENSES_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_TOTAL_BUSINESS_EXP_AMT').mstrGetPrompt();
        strAP_WEB_ITMZ_TOTAL_ITEMIZED_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_TOTAL_ITEMIZED_AMOUNT').mstrGetPrompt();
        strAP_WEB_ITMZ_ORIGINAL_RECEIPT_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_ORIGINAL_RECEIPT_AMOUNT').mstrGetPrompt();
	strAP_WEB_ITMZ_EXCLUDED_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_EXCLUDED_AMOUNT').mstrGetPrompt();
	strAP_WEB_ITMZ_ADD_MORE_LINES = tmpRegion.mobjGetRegionItem('AP_WEB_ITMZ_ADD_MORE_LINES').mstrGetPrompt();
	strAP_WEB_ITMZ_CLEAR = tmpRegion.mobjGetRegionItem('AP_WEB_CLEAR').mstrGetPrompt();
	strAP_WEB_ITMZ_SELECT = tmpRegion.mobjGetRegionItem('AP_WEB_SELECT').mstrGetPrompt();
	strAP_WEB_ITMZ_PERSONAL_AMOUNT = tmpRegion.mobjGetRegionItem('AP_WEB_TOTAL_PERSONAL_EXP_AMT').mstrGetPrompt();

	// Intialize the global variabl 
        top.g_selectedItemizeLine = 0; 

	// validate receipt prior to itemization
	if (isOOP) {
		receipt = top.objExpenseReport.oopReceipts;
	} else {
		receipt = top.objExpenseReport.cCardReceipts;
	}
	if (receipt[receiptIndex] == null) {
		alert(top.strWinItemizeMsg1);
		return;
	}
	if (receipt[receiptIndex].date == null || receipt[receiptIndex].date == "")  {
		alert(top.strWinItemizeMsg2);
		return;
	}
	if (eval(receipt[receiptIndex].getExpensedAmount()) < eval(top.fMoneyFormat("0.005")))  {
		alert(top.strWinItemizeMsg3);
		return;
	}
	top.g_winItemize=open('','Receipt_Itemize','resizable=no,menubar=no,width=650,height=425,onFocus="top.opener.top.fCheckModal();"');
	top.fDrawItemizeFrames();
	top.g_winItemize.fInit(mrLine,receiptIndex,isOOP);
	top.fGetItemizeSiblings();
        top.fDrawItemizeTop();
       if (isWindows95 && isIE5){
	top.fGenBlueBorder('g_winItemize','leftborder');
	top.fGenBlueBorder('g_winItemize','rightborder');
       }
       top.fDrawItemizeLinesBody(false);
       top.fDrawItemizeLinesFooter();
       top.fDrawItemizeButtonsFrame();
       top.g_winItemize.fUpdateItemizeTotal();
}

//-----------------------------------------------------------------------------
// Function: fAddItemizeSibling()
// Desc:	add a new itemize sibling
//-----------------------------------------------------------------------------
function fAddItemizeSibling(receipt) {
	 var i = top.fGetTotalItemizeReceipts();
        top.g_arrItemizeReceipts[i] = new Object();
        top.g_arrItemizeReceipts[i].receipt = new top.receiptObject();
	top.g_arrItemizeReceipts[i].receipt.copy(receipt);
        top.g_arrItemizeReceipts[i].isSelected = false;
}

//-----------------------------------------------------------------------------
// Function: fGetTotalItemizeReceipts
// Desc:	get total number of itemized receipts
//-----------------------------------------------------------------------------
function fGetTotalItemizeReceipts() {
	var numItems = 0;
	for (var i=0; i<top.g_arrItemizeReceipts.length; i++) {
		if (top.g_arrItemizeReceipts[i] != null)
			if (top.g_arrItemizeReceipts[i].receipt != null) numItems++;
	}
	return numItems;
}

//-----------------------------------------------------------------------------
// Function: fCopyItemizeSiblings
// Desc:	copy itemize window receipts to expense report
//-----------------------------------------------------------------------------
function fCopyItemizeSiblings() {
	 var receipt;
	 if (top.g_winItemize.bIsOOPItemize) {
		 receipt = top.objExpenseReport.oopReceipts;
	 } else {
		 receipt = top.objExpenseReport.cCardReceipts;
	 }
	 var itemizeId;
	 if (receipt[top.g_winItemize.iItemizeReceiptIndex] )
	     itemizeId = receipt[top.g_winItemize.iItemizeReceiptIndex].getItemizeId();

	 if (itemizeId != null) {
		 // this has been itemized before
		 // delete previous siblings from top.objExpenseReport
		 for (var i=0; i<receipt.length; i++) {
		   if (receipt[i] != null) {
			if (receipt[i].itemizeId == itemizeId ) {
				receipt[i] = null;
				top.g_iTotalReceipts--;
			}
		   }
		 }
	 } else {
		// this is the first time it's been itemized
		top.g_iItemizeSequence = top.objExpenseReport.getHighestItemizeId();
		itemizeId = ++top.g_iItemizeSequence;
		// delete previous receipt from top.objExpenseReport
		receipt[top.g_winItemize.iItemizeReceiptIndex] = null;
		top.g_iTotalReceipts--;
	 }

	 // add new siblings to top.opener.top.objExpenseReport
	 for (var i=0; i<top.fGetTotalItemizeReceipts(); i++) {
           if (!top.g_winItemize.fCheckManualClear(i)) {
		var j = receipt.length; top.g_iTotalReceipts++;
		receipt[j] = new top.receiptObject();
		receipt[j].copy(top.g_arrItemizeReceipts[i].receipt);
		receipt[j].itemizeId = itemizeId;
           }
	 }
}

//-----------------------------------------------------------------------------
// Function: fGetItemizeSiblings
// Desc:	get all sibling receipts from expense report
//-----------------------------------------------------------------------------
function fGetItemizeSiblings() {
	 var itemizeId = 0;
	 var totalExpensedAmount = 0;
	 var totalReceiptAmount = 0;
	 var receipt;
	 if (top.g_winItemize.bIsOOPItemize) {
		 receipt = top.objExpenseReport.oopReceipts;
	 } else {
		 receipt = top.objExpenseReport.cCardReceipts;
	 }

	 // save ptr to receipt from expense report prior to itemization
	 top.g_winItemize.objItemizeReceipt = receipt[top.g_winItemize.iItemizeReceiptIndex];
	 if (!top.g_winItemize.objItemizeReceipt.getObjExpenseType()) {
	     top.g_winItemize.objItemizeReceipt.setExpenseType(top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(""));
	 }
	 itemizeId = top.g_winItemize.objItemizeReceipt.itemizeId;
	 totalReceiptAmount = top.g_winItemize.objItemizeReceipt.getReceiptAmount();

	 // build receipt array of siblings
	 top.g_arrItemizeReceipts = null;
	 top.g_arrItemizeReceipts = new Array(10);

	 if (itemizeId != null) {
		// this has been itemized before so it has siblings
		for (var i=0; i<receipt.length; i++) {
		  if (receipt[i] != null) {
			if (receipt[i].itemizeId == itemizeId) {
				totalExpensedAmount = eval(totalExpensedAmount) + eval(receipt[i].getExpensedAmount());
				top.fAddItemizeSibling(receipt[i]);
			}
		  }
		}
	 } else {
		// this is the first time it's been itemized so it has no siblings
		totalExpensedAmount = eval(totalExpensedAmount) + eval(receipt[top.g_winItemize.iItemizeReceiptIndex].getExpensedAmount());
		top.fAddItemizeSibling(top.g_winItemize.objItemizeReceipt);
	 }

	 top.g_arrItemizeReceipts[0].isSelected = true;

}

//-----------------------------------------------------------------------------
// Function: fDrawItemizeFrames
// Desc:	draw the itemize window frameset/frames
//-----------------------------------------------------------------------------
function fDrawItemizeFrames(){
	 var d = top.g_winItemize.document;

        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<title>"+top.strAP_WEB_ITMZ_TITLE+"</title>");
        d.writeln("<head>");
	 d.writeln("<SCRIPT language=\"JavaScript\">");
	d.writeln("<!-- hide the script's contents from feeble browsers");
	 d.writeln("	var iTotalItemizeLines = 5;		// total # of lines to display in itemize window");
	 d.writeln("	var iMRLine = 0;			// multi-row line clicked on");
	 d.writeln("	var iItemizeReceiptIndex = 0;	// receipt array index of receipt being itemized");
	 d.writeln("	var bIsOOPItemize = true;		// whether oop is being itemized");
	 d.writeln("	var objItemizeReceipt = null;	// ptr to receipt from expense report selected for itemization");
	 d.writeln("	var winCalculator = null;		// calculator window");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fInit");
	 d.writeln("	// Desc:	initialize itemization window vars");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fInit(mrLine,receiptIndex,isOOP) {");
	 d.writeln("		iMRLine = mrLine;");
	 d.writeln("		iItemizeReceiptIndex = receiptIndex;");
	 d.writeln("		bIsOOPItemize = isOOP;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fGet<widget_id>");
	 d.writeln("	// Desc:	functions to return references to widgets");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fGetcbxSelect(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.cbxSelect'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGetpopExpType(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.popExpType'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGettxtRate(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.txtRate'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGettxtOccurs(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.txtOccurs'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGettxtDate(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.txtDate'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGettxtExpensedAmount(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.txtExpensedAmount'+item);");
	 d.writeln("	}");
	 d.writeln("	function fGettxtReimbursAmount(item) {");
	 d.writeln("		return eval('itemizeLinesBody.document.data.txtReimbursAmount'+item);");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fSetSelectedLine");
	 d.writeln("	// Desc:	set the selected itemize line ");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fSetSelectedLine(item) {");
         d.writeln("       if (item != top.opener.top.g_selectedItemizeLine) {");
         d.writeln("       	if (top.fisNewLine(item)) {");
	 d.writeln("	      		var select = fGetcbxSelect(item);");
         d.writeln("          		select.checked = true;");
         d.writeln("          		var select = top.fGetcbxSelect(top.opener.top.g_selectedItemizeLine);");
         d.writeln("          		select.checked = false;");
         d.writeln("          		if (top.opener.top.g_selectedItemizeLine < (top.opener.top.fGetTotalItemizeReceipts())) ");
         d.writeln("          			top.opener.top.g_arrItemizeReceipts[top.opener.top.g_selectedItemizeLine].isSelected = false;");
         d.writeln("          		top.opener.top.g_selectedItemizeLine = item;");
	 d.writeln("	   	}");
	 d.writeln("	   	else {");
	 d.writeln("	      		var select = fGetcbxSelect(item);");
         d.writeln("          		select.checked = true;");
         d.writeln("          		top.opener.top.g_arrItemizeReceipts[item].isSelected = true;");
         d.writeln("          		var select = top.fGetcbxSelect(top.opener.top.g_selectedItemizeLine);");
         d.writeln("          		select.checked = false;");
         d.writeln("          		if (top.opener.top.g_selectedItemizeLine < (top.opener.top.fGetTotalItemizeReceipts())) ");
         d.writeln("          			top.opener.top.g_arrItemizeReceipts[top.opener.top.g_selectedItemizeLine].isSelected = false;");
         d.writeln("          		top.opener.top.g_selectedItemizeLine = item");
	 d.writeln("	   	}");
	 d.writeln("	   }");
	 d.writeln("	   return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fDefaultLine");
	 d.writeln("	// Desc:	Default date and rate for a new line ");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fDefaultLine(item) {");
         d.writeln("       if (top.fisNewLine(item)) {");
	 d.writeln("	      var occurs = top.fGettxtOccurs(item);");
         d.writeln("          var date = top.fGettxtDate(item);");
         d.writeln("          top.opener.top.fAddItemizeSibling(top.objItemizeReceipt);");
         d.writeln("          var i = top.opener.top.fGetTotalItemizeReceipts()-1;");
	 d.writeln("	      var select = fGetcbxSelect(i);");
         d.writeln("          if (select.checked == true) {");
         d.writeln("               top.opener.top.g_arrItemizeReceipts[i].isSelected = select.checked;");	 
         d.writeln("               top.opener.top.g_selectedItemizeLine = i");
         d.writeln("          }");
         d.writeln("          top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(\"\");");
         d.writeln("          var exptype = top.fGetpopExpType(item);");
         d.writeln("          top.opener.top.g_arrItemizeReceipts[item].receipt.setExpenseType(top.opener.top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(\"\"));");
         d.writeln("          if (occurs.value==\"\") {");
	 d.writeln("		 occurs.value = 1;");
	 d.writeln("		 top.opener.top.g_arrItemizeReceipts[item].receipt.occurs = occurs.value;");
	 d.writeln("	      }");
         d.writeln("          if (date.value==\"\") {");
         d.writeln("             date.value = top.opener.top.fDateToString(top.opener.top.fStringToDate(top.opener.top.g_winItemize.objItemizeReceipt.date, true));");
         d.writeln("             top.opener.top.g_arrItemizeReceipts[item].receipt.date = date.value;");
	 d.writeln("	      }");
	 d.writeln("	   }");
	 d.writeln("	   return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fisNewLine");
	 d.writeln("	// Desc:	return true if new line else return false ");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fisNewLine(item) {");
	 d.writeln("	   if (!top.opener.top.g_arrItemizeReceipts[item]) return true");
	 d.writeln("       else return false;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fCheckEmptyLine");
	 d.writeln("	// Desc:	make sure user doesn't focus on empty line");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fCheckEmptyLine(item) {");
	 d.writeln("		// if empty line refocus to last non-empty line");
	 d.writeln("		if (item > top.opener.top.fGetTotalItemizeReceipts()) {");
	 d.writeln("			var rate = top.fGettxtRate(top.opener.top.fGetTotalItemizeReceipts());");
	 d.writeln("			rate.focus();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		// if the calculator is still open then update the display with the");
	 d.writeln("		// expensed amount of the line focused on");
	 d.writeln("	       if (top.winCalculator) {");
	 d.writeln("			top.fOpenCalculatorWin(item);");
	 d.writeln("		}");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeSelect");
	 d.writeln("	// Desc:	mark a receipt as selected ");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeSelect(item) {");
	 d.writeln("		var select = top.fGetcbxSelect(item);");
	 d.writeln("		// if empty line uncheck it");
	 d.writeln("		if (!top.fCheckEmptyLine(item)) {");
	 d.writeln("			select.checked = false;");
	 d.writeln("	        } else {");
	 d.writeln("		   if (item < top.opener.top.fGetTotalItemizeReceipts()) ");
	 d.writeln("			top.opener.top.g_arrItemizeReceipts[item].isSelected = select.checked;");
	 d.writeln("		}");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeExpenseType");
	 d.writeln("	// Desc:	update the expense type of the receipt");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeExpenseType(item) {");
	 d.writeln("	        top.fDefaultLine(item)");
	 d.writeln("		var exptype = top.fGetpopExpType(item);");
	 d.writeln("		var objE = top.opener.top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(exptype.options[exptype.selectedIndex].value);");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.setExpenseType(objE);");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeDate");
	 d.writeln("	// Desc:	update the date of the receipt");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeDate(item) {");
	 d.writeln("	        top.fDefaultLine(item)");
	 d.writeln("		var date = top.fGettxtDate(item);");
         d.writeln("            if (date.value != \"\") {");
         d.writeln("  			var objDate = top.opener.top.fStringToDate(date.value, true);");
         d.writeln("  			if (objDate == null) {");
	 d.writeln("				date.focus();");
	 d.writeln("				date.select();");
	 d.writeln("				return false;");
	 d.writeln("			}");
	 d.writeln("		}");
         d.writeln("            if (date.value == \"\") {");
	 d.writeln("			top.opener.top.g_arrItemizeReceipts[item].receipt.date = \"\";");
	 d.writeln("			return;");
	 d.writeln("		}");
	 d.writeln("		date.value = top.opener.top.fDateToString(top.opener.top.fStringToDate(date.value, true));");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.date = date.value;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeRate");
	 d.writeln("	// Desc:	update the rate of the receipt");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeRate(item) {");
	 d.writeln("	        top.fDefaultLine(item)");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		var reimbursAmount = top.fGettxtReimbursAmount(item);");
         d.writeln("            if ((rate.value == \"\") || (!top.fSilentValidateRate(item))) {");
	 d.writeln("		   expensedAmount.value = \"\";");
	 d.writeln("		   top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(\"\");");
	 d.writeln("		   top.fUpdateItemizeTotal();");
         d.writeln("               if (rate.value != \"\")  {");
	 d.writeln("		        if (!fValidateItem(item)) {");
	 d.writeln("		            return;");
	 d.writeln("		        }");
	 d.writeln("		   }");
	 d.writeln("	        }");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.occurs = occurs.value;");
         d.writeln("            if ((rate.value != \"\") && (occurs.value != \"\")) {");
	 d.writeln("		    top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(eval(rate.value)*eval(occurs.value));");
	 d.writeln("		}");
	 d.writeln("	");
	 d.writeln("		rate.value = top.opener.top.fMoneyFormat(rate.value,top.opener.top.fGetObjHeader().getReimbursCurr());");
	 d.writeln("		expensedAmount.value = top.opener.top.fMoneyFormatExpensedAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("		reimbursAmount.value = top.opener.top.fMoneyFormatReimbursAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("	");
	 d.writeln("		top.fUpdateItemizeTotal();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeExpensedAmount");
	 d.writeln("	// Desc:	update the receipt amount of the receipt");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeExpensedAmount(item) {");
	 d.writeln("	        top.fDefaultLine(item)");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		var reimbursAmount = top.fGettxtReimbursAmount(item);");
         d.writeln("            if ((expensedAmount.value == \"\") || (!top.fSilentValidateExpensedAmount(item))) {");
	 d.writeln("		   rate.value = \"\";");
	 d.writeln("		   top.fUpdateItemizeTotal();");
         d.writeln("               if (expensedAmount.value != \"\")  {");
	 d.writeln("		         if (!fValidateItem(item)) {");
	 d.writeln("		               return;");
	 d.writeln("		         }");
	 d.writeln("		   }");
	 d.writeln("	        }");
         d.writeln("            if (occurs.value == \"\") {");
	 d.writeln("		   occurs.value = 1;");
	 d.writeln("	        }");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.occurs = occurs.value;");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(expensedAmount.value);");
	 d.writeln("	");
         d.writeln("            if (expensedAmount.value != \"\") {");
	 d.writeln("		     rate.value = top.opener.top.fMoneyFormatDailyRate(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("		}");
	 d.writeln("		expensedAmount.value = top.opener.top.fMoneyFormatExpensedAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("		reimbursAmount.value = top.opener.top.fMoneyFormatReimbursAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("	");
	 d.writeln("		top.fUpdateItemizeTotal();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fChangeOccurs");
	 d.writeln("	// Desc:	update the receipt amount of the receipt");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fChangeOccurs(item) {");
	 d.writeln("	        top.fDefaultLine(item)");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		var reimbursAmount = top.fGettxtReimbursAmount(item);");
         d.writeln("            if ((occurs.value == \"\") || (!top.fSilentValidateOccurs(item))) {");
	 d.writeln("		   expensedAmount.value = \"\";");
	 d.writeln("		   top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(\"\");");
	 d.writeln("		   top.fUpdateItemizeTotal();");
         d.writeln("               if (occurs.value != \"\")  {");
	 d.writeln("		       if (!fValidateItem(item)) {");
	 d.writeln("		           return;");
	 d.writeln("		       }");
	 d.writeln("		   }");
	 d.writeln("	        }");
	 d.writeln("		top.opener.top.g_arrItemizeReceipts[item].receipt.occurs = occurs.value;");
	 d.writeln("  	        // Rate is null or occurs is null");
	 d.writeln("		if (!top.fSilentValidateRate(item) || (occurs.value == \"\"))");
         d.writeln("               top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(\"\");");
	 d.writeln("	        else");
	 d.writeln("		   top.opener.top.g_arrItemizeReceipts[item].receipt.setExpensedAmount(eval(rate.value)*eval(occurs.value));");
	 d.writeln("	");
	 d.writeln("		rate.value = top.opener.top.fMoneyFormat(rate.value,top.opener.top.fGetObjHeader().getReimbursCurr());");
	 d.writeln("		expensedAmount.value = top.opener.top.fMoneyFormatExpensedAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("		reimbursAmount.value = top.opener.top.fMoneyFormatReimbursAmount(top.opener.top.g_arrItemizeReceipts[item].receipt);");
	 d.writeln("	");
	 d.writeln("		top.fUpdateItemizeTotal();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fValidateItem()");
	 d.writeln("	// Desc:	validate amounts for an item");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fValidateItem(item) {");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		if (rate.value == \"\") {");
	 d.writeln("		        alert(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));");
	 d.writeln("			rate.focus();");
	 d.writeln("			rate.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		if (!top.opener.top.fCheckPosNum(rate.value, true)) {");
	 d.writeln("			rate.focus();");
	 d.writeln("			rate.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		if (occurs.value == \"\") {");
	 d.writeln("		        alert(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));");
	 d.writeln("			occurs.focus();");
	 d.writeln("			occurs.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		if (!top.opener.top.fCheckPosNum(occurs.value, true)) {");
	 d.writeln("			occurs.focus();");
	 d.writeln("			occurs.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		if (expensedAmount.value == \"\") {");
	 d.writeln("		        alert(top.opener.top.g_objMessages.mstrGetMesg('AP_WEB_NUMBER_REQUIRED'));");
	 d.writeln("			expensedAmount.focus();");
	 d.writeln("			expensedAmount.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		if (!top.opener.top.fCheckPosNum(expensedAmount.value, true)) {");
	 d.writeln("			expensedAmount.focus();");
	 d.writeln("			expensedAmount.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		var date = top.fGettxtDate(item);");
         d.writeln("  		var objDate = top.opener.top.fStringToDateCheckNull(date.value, true);");
         d.writeln("  		if (objDate == null) {");
	 d.writeln("			date.focus();");
	 d.writeln("			date.select();");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fSilentValidateItem()");
	 d.writeln("	// Desc:	validate amounts for an item, returns true/false");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fSilentValidateItem(item) {");
         d.writeln("            if (!top.fSilentValidateRate(item)) return false;");
         d.writeln("            if (!top.fSilentValidateOccurs(item)) return false;");
         d.writeln("            if (!top.fSilentValidateExpensedAmount(item)) return false;");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fSilentValidateRate()");
	 d.writeln("	// Desc:	validate rate for an item, returns true/false");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fSilentValidateRate(item) {");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		if (!top.opener.top.fSilentCheckPosNum(rate.value, true)) {");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fSilentValidateOccurs()");
	 d.writeln("	// Desc:	validate rate for an item, returns true/false");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fSilentValidateOccurs(item) {");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		if (!top.opener.top.fSilentCheckPosNum(occurs.value, true)) {");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fSilentValidateExpensedAmount()");
	 d.writeln("	// Desc:	validate rate for an item, returns true/false");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fSilentValidateExpensedAmount(item) {");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		if (!top.opener.top.fSilentCheckPosNum(expensedAmount.value, true)) {");
	 d.writeln("			return false;");
	 d.writeln("		}");
	 d.writeln("		return true;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fCheckManualClear()");
	 d.writeln("	// Desc:	Check if all fields are manually cleared");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fCheckManualClear(item) {");
         d.writeln("            var exptype = top.fGetpopExpType(item);");
	 d.writeln("		var rate = top.fGettxtRate(item);");
	 d.writeln("		var occurs = top.fGettxtOccurs(item);");
	 d.writeln("		var expensedAmount = top.fGettxtExpensedAmount(item);");
	 d.writeln("		var date = top.fGettxtDate(item);");
         d.writeln("  		var objDate = top.opener.top.fStringToDate(date.value, false);");
         d.writeln("            var objE = top.opener.top.objExpenseReport.header.getObjExpTemplate().getExpTypeObj(exptype.options[exptype.selectedIndex].value);");
         d.writeln("  		if ((objDate == null) && (rate.value == \"\") && (occurs.value == \"\") && (expensedAmount.value == \"\")){");
         d.writeln("  		   if (top.opener.top.g_arrItemizeReceipts[item].receipt.getObjExpenseType()) {");
         d.writeln("  		      if (top.opener.top.g_arrItemizeReceipts[item].receipt.getObjExpenseType().mGetstrName()==\"\") ");
	 d.writeln("			return true;");
	 d.writeln("		   }");
	 d.writeln("		   else return true;");
	 d.writeln("		}");
	 d.writeln("		return false;");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fIEItemizeOnChangeTrigger()");
	 d.writeln("	// Desc:	");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fIEItemizeOnChangeTrigger(item) {");
	 d.writeln("		var select = top.fGetcbxSelect(item);");
	 d.writeln("		select.focus();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fOKItemize");
	 d.writeln("	// Desc:	commit itemization");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fOKItemize() {");
	 d.writeln("	        if (!top.opener.top.Nav4) ");
	 d.writeln("	           fIEItemizeOnChangeTrigger(top.opener.top.g_selectedItemizeLine);");
         d.writeln("	        for (var i=0; i<top.opener.top.fGetTotalItemizeReceipts(); i++) {");
         d.writeln("			if (!fCheckManualClear(i))");
         d.writeln("				if (!fValidateItem(i)) return;");
         d.writeln("		}");
         d.writeln("	");
         d.writeln("		var totalReceiptAmount=0;");
         d.writeln("		var totalExcludeAmount=0;");
         d.writeln("		var totalBusinessExpensesAmount=0;");
         d.writeln("		var totalOriginalReceiptAmount=0;");
         d.writeln("	");
         d.writeln("		totalReceiptAmount = top.opener.top.fMoneyFormatReceiptAmount(top.objItemizeReceipt);");
         d.writeln("		totalReceiptAmount = top.opener.top.fMoneyFormat(top.opener.top.fItemCalcAmount(eval(totalReceiptAmount),top.objItemizeReceipt));");
         d.writeln("	");
         d.writeln("		totalOriginalReceiptAmount = top.itemizeLinesFooter.document.data.hidOriginalReceiptTotal.value;");
         d.writeln("		totalBusinessExpensesAmount = top.itemizeLinesFooter.document.data.hidBusinessExpensesTotal.value;"); 
         d.writeln("		totalExcludeAmount = top.itemizeLinesFooter.document.data.hidExcludeTotal.value;"); 
         d.writeln("	");
	 d.writeln("		if (eval(totalOriginalReceiptAmount) > eval(totalReceiptAmount)) {");
         d.writeln("			alert(top.opener.top.strWinItemizeMsg4);");
         d.writeln("			return;");
         d.writeln("		}");
         d.writeln("		if (eval(totalBusinessExpensesAmount) < eval(top.opener.top.fMoneyFormat(\"0.005\"))) {");
	 d.writeln("			alert(top.opener.top.strWinItemizeMsg5);");
         d.writeln("			return;");
         d.writeln("		}");
         d.writeln("		if (eval(totalReceiptAmount)-(totalBusinessExpensesAmount) > eval(top.opener.top.fMoneyFormat(\"0.005\"))) {");
	 d.writeln("			if (!confirm(top.opener.top.strWinItemizeMsg6.replace('TOTAL_REIMBURSABLE_AMOUNT',totalBusinessExpensesAmount).replace('TOTAL_PERSONAL_AMOUNT',totalExcludeAmount)))");
         d.writeln("			   return;");
         d.writeln("		}");
         d.writeln("	");
	 d.writeln("		if (!top.fUpdateItemizeTotal()) return;");
	 d.writeln("		top.opener.top.fCopyItemizeSiblings();");
	 d.writeln("		// update the enter receipts tab lines before closing window");
	 d.writeln("		if (top.bIsOOPItemize) {");
	 d.writeln("			top.opener.top.fCreateMoreLines(0, false);");
	 d.writeln("			top.opener.top.framMain.lines.fUpdateButtonsFrame();");
	 d.writeln("		}");
	 d.writeln("		else {");
	 d.writeln("			top.opener.top.fDrawCCardLines(true);");
	 d.writeln("		}");
	 d.writeln("		for (var i=1;i<=top.opener.top.objExpenseReport.getTotalReceipts(top.opener.top.g_strCurrentTab);i++) {");
	 d.writeln("		  var popExpType = eval('top.opener.top.framMain.lines.document.data.popExpType'+i); ");
	 d.writeln("		  if (popExpType) {");
	 d.writeln("                if (top.opener.top.fIsOOP()) ");
	 d.writeln("		        top.opener.top.framMain.lines.fChangeExpType(popExpType.options[popExpType.selectedIndex].value, i);");
	 d.writeln("                else ");
	 d.writeln("			top.opener.top.framMain.lines.fChangeExpenseType(popExpType.options[popExpType.selectedIndex], popExpType.selectedIndex,top.opener.top.fGetObjExpReport().getNthReceipt(i))");
	 d.writeln("		  } //end if popExpType ");
	 d.writeln("	}//end for ");
	 d.writeln("		top.opener.top.fCloseItemizeWindow();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fCancelItemize");
	 d.writeln("	// Desc:	cancel itemization");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fCancelItemize() {");
	 d.writeln("		top.opener.top.fCloseItemizeWindow();");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fOpenCalendarWin");
	 d.writeln("	// Desc:	open itemize calendar window");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fOpenCalendarWin(item) {");
	 d.writeln("		 if (top.fCheckEmptyLine(item)) {");
	 d.writeln("			top.opener.top.fOpenCalWin('top.g_winItemize.itemizeLinesBody.document.data.txtDate'+item,'top.g_winItemize.fChangeDate('+item+')');");
	 d.writeln("		 }");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fOpenCalculatorWin");
	 d.writeln("	// Desc:	open itemize calculator window");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fOpenCalculatorWin(item) {");
	 d.writeln("		// calculator is not open yet");
	 d.writeln("	       if (!top.winCalculator) {");
	 d.writeln("			 top.winCalculator=open(top.opener.top.g_strCssDir+'APWCALCN.htm','ItemizeCalculator','resizable=no,menubar=no,width=250,height=250');");
	 d.writeln("		}");	
	 d.writeln("		// wait until calculator loaded");
	 d.writeln("		var changeCallback = '\"top.opener.top.fChangeExpensedAmount('+item+')\"';");
	 d.writeln("		var closeCallback = '\"top.opener.top.fCloseCalculatorWin()\"';");
	 d.writeln("			top.opener.top.fWaitUntilFrameLoaded('top.g_winItemize.winCalculator.bIsLoaded','top.g_winItemize.winCalculator.Init(top.g_winItemize.itemizeLinesBody.document.data.txtExpensedAmount'+item+', '+changeCallback+', '+closeCallback+')');");
	 d.writeln("	}");

	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	// Function: fCloseCalculatorWin");
	 d.writeln("	// Desc:	close itemize calculator window");
	 d.writeln("	//-----------------------------------------------------------------------------");
	 d.writeln("	function fCloseCalculatorWin() {");
	 d.writeln("		if (top.winCalculator) {");
	 d.writeln("			if (top.winCalculator.open) top.winCalculator.close();");
	 d.writeln("			top.winCalculator = null;");
	 d.writeln("		}");
	 d.writeln("		top.opener.top.g_winItemize.focus();");
	 d.writeln("	}");

        d.writeln("	//-----------------------------------------------------------------------------");
        d.writeln("	// Function: fCalcItemizeTotal");
        d.writeln("	// Desc:	calculate totals on every change event to update dynamic totals");
        d.writeln("	//-----------------------------------------------------------------------------");
        d.writeln("	function fCalcItemizeTotal() {");
        d.writeln("		 var totalReceiptAmount=0;");
        d.writeln("		 var totalExcludeAmount=0;");
        d.writeln("		 var totalBusinessExpensesAmount=0;");
        d.writeln("		 var totalOriginalReceiptAmount=0;");
        d.writeln("	");
        d.writeln("	        for (var i=0; i<top.opener.top.fGetTotalItemizeReceipts(); i++) {");
        d.writeln("			var expensedAmount = top.opener.top.g_arrItemizeReceipts[i].receipt.getExpensedAmount();");
        d.writeln("			if ((expensedAmount != \"\") && (top.fSilentValidateExpensedAmount(i))) {");
        d.writeln("				totalBusinessExpensesAmount = eval(totalBusinessExpensesAmount) + eval(expensedAmount);");
        d.writeln("			}");
        d.writeln("		 }");
        d.writeln("	");
        d.writeln("		 totalBusinessExpensesAmount = top.opener.top.fMoneyFormat(totalBusinessExpensesAmount);");
        d.writeln("		 totalReceiptAmount = top.opener.top.fMoneyFormatReceiptAmount(top.objItemizeReceipt);");
        d.writeln("		 if (eval(totalBusinessExpensesAmount) < eval(totalReceiptAmount)) {;");
        d.writeln("		    totalExcludeAmount = eval(totalReceiptAmount) - eval(totalBusinessExpensesAmount);");
        d.writeln("		 }");
        d.writeln("		 else {");
        d.writeln("		    totalExcludeAmount = 0;");
        d.writeln("		 }");
        d.writeln("		 totalExcludeAmount = top.opener.top.fMoneyFormat(totalExcludeAmount);");
        d.writeln("		 totalOriginalReceiptAmount = top.opener.top.fMoneyFormat(top.opener.top.fItemCalcAmount(eval(totalBusinessExpensesAmount) + eval(totalExcludeAmount),top.objItemizeReceipt));");
        d.writeln("	");
        d.writeln("		 // conversion exchange rate");
        d.writeln("		 top.itemizeLinesFooter.document.data.hidOriginalReceiptTotal.value = totalOriginalReceiptAmount;"); 
        d.writeln("		 top.itemizeLinesFooter.document.data.hidExcludeTotal.value = top.opener.top.fMoneyFormat(top.opener.top.fItemCalcAmount(totalExcludeAmount,top.objItemizeReceipt));"); 
        d.writeln("		 top.itemizeLinesFooter.document.data.hidBusinessExpensesTotal.value = top.opener.top.fMoneyFormat(top.opener.top.fItemCalcAmount(totalBusinessExpensesAmount,top.objItemizeReceipt));");
        d.writeln("	");
        d.writeln("		 return true;");
        d.writeln("	}");

        d.writeln("	//-----------------------------------------------------------------------------");
        d.writeln("	// Function: fUpdateItemizeTotal");
        d.writeln("	// Desc:	dynamically update itemize total layer");
        d.writeln("	//-----------------------------------------------------------------------------");
        d.writeln("	function fUpdateItemizeTotal() {");
        d.writeln("		 var result = top.fCalcItemizeTotal();");
        d.writeln("	        var Nav4 = top.opener.top.Nav4;");
        d.writeln("	        var navTop = 12;");
        d.writeln("		var ieTop = 12;");
        d.writeln("	");
        d.writeln("	        var layerName = \"layItemizeTotal\";");
        d.writeln("		 var val1 = top.itemizeLinesFooter.document.data.hidOriginalReceiptTotal.value;");
        d.writeln("		 var val2 = top.itemizeLinesFooter.document.data.hidExcludeTotal.value;");
        d.writeln("		 var val3 = top.itemizeLinesFooter.document.data.hidBusinessExpensesTotal.value;");
  	d.writeln("		 var msgText = top.opener.top.strAP_WEB_ITMZ_BUSINESS_EXPENSES_AMOUNT +  val1  + top.opener.top.objExpenseReport.header.getReimbursCurr();");
        d.writeln("	        var msg = \"\";");
        d.writeln("	");
        d.writeln("		 msg += \"<table cellpadding=0 cellspacing=0 border=0>\";");
        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='promptblack'>\"+top.opener.top.strAP_WEB_ITMZ_BUSINESS_EXPENSES_AMOUNT+\"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='datablack'>\" + val3 + \"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"</tr>\";");

        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='promptblack'>\"+top.opener.top.strAP_WEB_ITMZ_PERSONAL_AMOUNT+\"</font></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='datablack'>\" + val2 + \"</font></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"</tr>\";");
        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td height=1 colspan=7 bgcolor=#000000><img src=\"+top.opener.top.g_strImgDir+\"APWPX3.gif width=98% height=1></td>\";");
        d.writeln("		 msg += \"</tr>\";");

        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='promptblack'>\"+top.opener.top.strAP_WEB_ITMZ_TOTAL_ITEMIZED_AMOUNT+\"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='datablack'>\" + val1 + \"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey></td>\";");
        d.writeln("		 msg += \"<td class=tablerowlgrey><font class='datablack'>"+top.objExpenseReport.header.getReimbursCurr()+"</font></td>\";");
        d.writeln("		 msg += \"</tr>\";");

        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"</tr>\";");

        d.writeln("		 msg += \"<tr>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='promptblack'>\"+top.opener.top.strAP_WEB_ITMZ_ORIGINAL_RECEIPT_AMOUNT+\"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey><font class='datablack'>\"+top.opener.top.fMoneyFormatReceiptAmount(top.opener.top.g_winItemize.objItemizeReceipt) + \"</font></td>\";");
        d.writeln("		 msg += \"<td colspan=2 class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowrgrey>&nbsp;</td>\";");
        d.writeln("		 msg += \"<td class=tablerowlgrey><font class='datablack'>\"+top.opener.top.g_winItemize.objItemizeReceipt.getReceiptCurr()+\"</font></td>\";");
        d.writeln("		 msg += \"</tr>\";");

        d.writeln("	  	 msg += \"</table>\";");
        d.writeln("	        var objWidth=msgText.length * 7 + 10;");
        d.writeln("	        if (Nav4) {");
        d.writeln("	                var docWidth = top.itemizeLinesFooter.self.innerWidth;");
        d.writeln("	                var obj = top.itemizeLinesFooter.document.layers[layerName];");
        d.writeln("	                obj.document.write(msg);");
        d.writeln("	                obj.document.close();");
        d.writeln("	                obj.left = docWidth-objWidth;");
        d.writeln("	                obj.top = navTop;");
        d.writeln("	        } else {");
        d.writeln("	                top.itemizeLinesFooter.document.all[layerName].innerHTML = msg;");
        d.writeln("	                var docWidth = top.itemizeLinesFooter.document.body.clientWidth + 10;");
        d.writeln("	                var obj = top.itemizeLinesFooter.document.all[layerName].style;");
        d.writeln("	                obj.pixelLeft = docWidth-1.1*objWidth;");
        d.writeln("	                obj.pixelTop = ieTop;");
        d.writeln("	        }");
        d.writeln("	");
        d.writeln("	        obj.visibility = \"visible\";");
        d.writeln("		return result;");
        d.writeln("	}");
	d.writeln("<!-- done hiding from old browsers -->");
	 d.writeln("</SCRIPT>");
        d.writeln("</head>");

        d.writeln("	<frameset cols=\"3,*,3\" border=0>");
// left frame
        d.writeln("		<frame  ");
	if (!(isWindows95 && isIE5)) d.writeln("			src=\"javascript:top.opener.top.fBlankPage('336699')\""); 
        d.writeln("			frameborder=no");
        d.writeln("			name=leftborder");
        d.writeln("			marginwidth=0");
        d.writeln("			marginheight=0");
        d.writeln("			scrolling=no>");
// middle frame
        d.writeln("		<frameset rows=\"50,155,87,60\" border=0 framespacing=0>");
        d.writeln("			<frame ");
	if (!(isWindows95 && isIE5)) d.writeln("				src=\"javascript:top.opener.top.fBlankPage('336699');\"");
        d.writeln("				name=itemizeTop");
        d.writeln("				marginwidth=0");
        d.writeln("				frameborder=no");
        d.writeln("				scrolling=no>");
        d.writeln("              <frame");
	if (!(isWindows95 && isIE5)) d.writeln("                      src=\"javascript:top.opener.top.fBlankPage('336699');\" ");
        d.writeln("                      frameborder=no");
        d.writeln("                      name=itemizeLinesBody");
        d.writeln("                      marginwidth=10");
        d.writeln("                      scrolling=auto>");
        d.writeln("              <frame");
	if (!(isWindows95 && isIE5)) d.writeln("                      src=\"javascript:top.opener.top.fBlankPage('cccccc');\" ");
        d.writeln("                      frameborder=no");
        d.writeln("                      name=itemizeLinesFooter");
        d.writeln("                      marginwidth=0");
        d.writeln("                      scrolling=no>");
        d.writeln("              <frame");
	if (!(isWindows95 && isIE5)) d.writeln("                      src=\"javascript:top.opener.top.fBlankPage('336699');\" ");
        d.writeln("                      frameborder=no");
        d.writeln("                      name=itemizeButtons");
        d.writeln("                      marginwidth=0");
        d.writeln("                      scrolling=no>");
        d.writeln("      	</frameset>");
// right frame
        d.writeln("      	<frame  ");
	if (!(isWindows95 && isIE5)) d.writeln("      		src=\"javascript:top.opener.top.fBlankPage('336699');\" ");
        d.writeln("      		frameborder=no");
        d.writeln("      		name=rightborder");
        d.writeln("      		marginwidth=0");
        d.writeln("      		marginheight=0");
        d.writeln("      		scrolling=no>");
        d.writeln("   </frameset>");
        d.close();
}

//-----------------------------------------------------------------------------
// Function: fDrawItemizeTop
// Desc:	draw itemize window top frame
//-----------------------------------------------------------------------------
function fDrawItemizeTop() {  
	 var d = top.g_winItemize.itemizeTop.document;

        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head>");
        top.fGenerateStyleCodes(top.g_winItemize.itemizeTop);
        d.writeln("</head>");

        d.writeln("<body bgcolor=#336699>");
        d.writeln("<form name=data>");
        d.writeln("<CENTER>");
        d.writeln("<br>");

        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 border=0 WIDTH=100% bgcolor=#cccccc>");
        d.writeln("<TR bgcolor=#cccccc>");
        d.writeln("          <TD align=left><IMG SRC="+top.g_strImgDir+"APWCTTL.gif></TD>");
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXW.gif></TD>");
        d.writeln("          <TD align=right><IMG SRC="+top.g_strImgDir+"APWCTTR.gif></TD>");
        d.writeln("</TR>");
        d.writeln("</TABLE>");

        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 border=0 WIDTH=100% class=colorg5>");
        d.writeln("<TR bgcolor=#cccccc>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("</TR>");
        d.writeln("<TR bgcolor=#cccccc>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>"); 
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("          <TD>&nbsp;</TD>");
        d.writeln("</TR>");
        d.writeln("<TR bgcolor=#cccccc>");
        d.writeln("          <td height=35 colspan=5><img src="+top.g_strImgDir+"APWPXG5.gif></td>");                       
        d.writeln("</TR>");
        d.writeln("</TABLE>");

        d.writeln("</CENTER>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();
} // end of fDrawItemizeTop

//-----------------------------------------------------------------------------
// Function: fClearItemize
// Desc:	clear the itemize lines and draw the lines body 
//-----------------------------------------------------------------------------
function fClearItemize(){
  top.fClearSelectedItemizeLines();
  top.fDrawItemizeLinesBody(false);
  top.g_winItemize.fUpdateItemizeTotal();
}

//-----------------------------------------------------------------------------
// Function: fClearSelectedItemizeLines
// Desc:	clear the selected itemize lines 
//-----------------------------------------------------------------------------
function fClearSelectedItemizeLines(){
  var j=0;
  var TotalItemizeReceipts=top.fGetTotalItemizeReceipts();

  for (var i=0; i<TotalItemizeReceipts; i++) {
      if ((!top.g_arrItemizeReceipts[i].isSelected) && (!top.g_winItemize.fCheckManualClear(i))){ 
         top.g_arrItemizeReceipts[j].receipt.copy(top.g_arrItemizeReceipts[i].receipt);
         if (j==0) {	 
         	top.g_arrItemizeReceipts[j].isSelected = true;
        	top.g_selectedItemizeLine = 0; 
	 }
	 else {
         	top.g_arrItemizeReceipts[j].isSelected = false;
	 }
         j++;
      }
  }

  for (var i=j; i<TotalItemizeReceipts; i++) { 
        delete top.g_arrItemizeReceipts[i];
  }
}

//-----------------------------------------------------------------------------
// Function: fDrawItemizeLinesBody
// Desc:	draw lines for itemize window dynamically;
//		specify if more lines are needed
//-----------------------------------------------------------------------------
function fDrawItemizeLinesBody(moreLines){
	var d = top.g_winItemize.itemizeLinesBody.document;

        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head>");
        top.fGenerateStyleCodes(top.g_winItemize.itemizeLinesBody);
        d.writeln("</head>");

        d.writeln("<BODY bgcolor=#cccccc>");
        d.writeln("<form name=data>");
        d.writeln("<center>");

        // draw top curves
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=100% align=center>");
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCTL.gif></td>");
	d.writeln("	<td width=600 class=tableheader nowrap><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCTR.gif></td>");
        d.writeln("</tr>");

        // draw itemize lines body/lines
        d.writeln("<tr valign=middle>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td colspan=3 class=tableheader><table cellpadding=0 cellspacing=0 border=0 width=100% align=center>");
        d.writeln("	<tr>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_SELECT+"</td>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_EXPENSE_TYPE+"</td>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_DAILY_RATE+"</td>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_NO_OF_TIMES+"</td>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_START_DATE+"</td>");
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_RECEIPT_AMOUNT+" ("+top.g_winItemize.objItemizeReceipt.getReceiptCurr()+")</td>");
	 // hide the reimbursAmount if receipt currency equals reimbursAmount currency
	 if (top.objExpenseReport.header.getReimbursCurr() != top.g_winItemize.objItemizeReceipt.getReceiptCurr()) {
        d.writeln("		<td class=tableheader>"+top.strAP_WEB_ITMZ_REIMBURS_AMOUNT+" ("+top.objExpenseReport.header.getReimbursCurr()+")</td>");
	 }
        d.writeln("	</tr>");
 
	 if (moreLines) {
		// Add More Lines
		top.g_winItemize.iTotalItemizeLines += 5;
	 } else {
		// initial draw - enough lines for siblings?
		if (top.fGetTotalItemizeReceipts() > top.g_winItemize.iTotalItemizeLines) {
			top.g_winItemize.iTotalItemizeLines = top.fGetTotalItemizeReceipts() + 5;
		} else {
			if (top.fGetTotalItemizeReceipts() <= 5 ) 
				top.g_winItemize.iTotalItemizeLines = 5;
		}
	 }

	 // draw lines
	 // hide the reimbursAmount if receipt currency equals reimbursAmount currency
        for (var i=0; i<top.g_winItemize.iTotalItemizeLines; i++) {
        	d.writeln("	<tr>");
       	d.writeln("		<td class=tablerow><input type=checkbox name=cbxSelect"+i+" onFocus=\"top.fCheckEmptyLine("+i+");\" onClick=\"top.fChangeSelect("+i+");\" tabindex=-1></td>");
		d.writeln("		<td class=tablerowl><select name=popExpType"+i+" onFocus=\"top.fCheckEmptyLine("+i+"); top.fSetSelectedLine("+i+");\" onChange=\"top.fChangeExpenseType("+i+");\">");
		if (i<top.fGetTotalItemizeReceipts() && (top.g_arrItemizeReceipts[i].receipt.getObjExpenseType()) ) {
			  d.writeln(top.fGenExpPopOptions(top.g_arrItemizeReceipts[i].receipt.getObjExpenseType().mGetstrName()));
		} else {
			d.writeln(top.fGenExpPopOptions());
		}
		d.writeln("		</select></td>");
		d.writeln("		<td class=tablerowl><input type=text name=txtRate"+i+" size=7 align=right onFocus=\"top.fCheckEmptyLine("+i+"); top.fSetSelectedLine("+i+");\" onChange=\"top.fChangeRate("+i+");\"></td>");
		d.writeln("		<td class=tablerowl><input type=text name=txtOccurs"+i+" size=3 onFocus=\"top.fCheckEmptyLine("+i+"); top.fSetSelectedLine("+i+");\" onChange=\"top.fChangeOccurs("+i+");\"></td>");
	 	d.writeln("		<td class=tablerowl><input type=text name=txtDate"+i+" size=10 onFocus=\"top.fCheckEmptyLine("+i+"); top.fSetSelectedLine("+i+");\" onChange=\"top.fChangeDate("+i+");\"><a href=\"javascript:if (top.fCheckEmptyLine("+i+")) top.fOpenCalendarWin("+i+");\"><img src="+top.g_strImgDir+"APWICLDR.gif border=no align=absmiddle></a></td>");
       	d.writeln("		<td class=tablerowl><input type=text name=txtExpensedAmount"+i+" size=7 align=right onFocus=\"top.fCheckEmptyLine("+i+"); top.fSetSelectedLine("+i+");\" onChange=\"top.fChangeExpensedAmount("+i+");\"><a href=\"javascript:if (top.fCheckEmptyLine("+i+")) top.fOpenCalculatorWin("+i+");\"><img src="+top.g_strImgDir+"APWICURT.gif border=no align=absmiddle></a></td>");
	 	if (top.objExpenseReport.header.getReimbursCurr() != top.g_winItemize.objItemizeReceipt.getReceiptCurr()) {
       		d.writeln("		<td class=tablerowl><input name=txtReimbursAmount"+i+" size=7 align=right onFocus=\"this.blur()\" tabindex=-1 Disabled></td>");
		} else {
       		d.writeln("		<td class=tablerowl>&nbsp;<input type=hidden name=txtReimbursAmount"+i+" size=7 align=right tabindex=-1 Disabled></td>");
		}
       	d.writeln("	</tr>");
	}

        d.writeln("	</table></td>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("</tr>");

        // draw bottom curves
        d.writeln("<tr>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("	<td align=left><img src="+top.g_strImgDir+"APWRTCBL.gif></td>");
        d.writeln("	<td width=600 class=tableheader><img src="+top.g_strImgDir+"APWPX4.gif></td>");
        d.writeln("	<td align=right><img src="+top.g_strImgDir+"APWRTCBR.gif></td>");
        d.writeln("	<td rowspan=2></td>");
        d.writeln("</tr>");

        d.writeln("<tr>");
        d.writeln("	<td></td>");
        d.writeln("	<td bgcolor=#000000 height=1><img src="+top.g_strImgDir+"APWPX1.gif></td>");
        d.writeln("	<td></td>");
        d.writeln("</tr>");
        d.writeln("</TABLE>");

        d.writeln("</CENTER>");
        d.writeln("</form>");
        d.writeln("</BODY></HTML>");
        d.close();

	// fill in lines with siblings after body has been drawn
	for (var i=0; i<top.fGetTotalItemizeReceipts(); i++) {
		// set select
		if (top.g_arrItemizeReceipts[i].isSelected) {
			var select = top.g_winItemize.fGetcbxSelect(i);
			select.checked = true;
		}
		// set expense type - already done by fGenExpPopOptions()
		var rate = top.g_winItemize.fGettxtRate(i);
		var occurs = top.g_winItemize.fGettxtOccurs(i);
		var date = top.g_winItemize.fGettxtDate(i);
		var expensedAmount = top.g_winItemize.fGettxtExpensedAmount(i);
		var reimbursAmount = top.g_winItemize.fGettxtReimbursAmount(i);
		occurs.value = top.g_arrItemizeReceipts[i].receipt.occurs;
		date.value = top.fDateToString(top.fStringToDate(top.g_arrItemizeReceipts[i].receipt.date, true));
		expensedAmount.value = top.fMoneyFormatExpensedAmount(top.g_arrItemizeReceipts[i].receipt);
		reimbursAmount.value = top.fMoneyFormatReimbursAmount(top.g_arrItemizeReceipts[i].receipt);
                if (expensedAmount.value != "") {
			rate.value = top.fMoneyFormatDailyRate(top.g_arrItemizeReceipts[i].receipt);
 		}
		else 	rate.value = "";
	}

}// end of fDrawItemizeLinesBody

//-----------------------------------------------------------------------------
// Function: fDrawItemizeLinesFooter
// Desc:	draw itemize window footer frame
//-----------------------------------------------------------------------------
function fDrawItemizeLinesFooter(){
	 var d = top.g_winItemize.itemizeLinesFooter.document;

        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head>");
        top.fGenerateStyleCodes(top.g_winItemize.itemizeLinesFooter);
        d.writeln("</head>");

        d.writeln("<BODY bgcolor=#336699 onLoad=\"javascript:top.fUpdateItemizeTotal()\">");
        d.writeln("<form name=data>");
        d.writeln("<center>");
        d.writeln("<table bgcolor=#cccccc cellpadding=0 cellspacing=0 border=0 width=100%>");

        d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("		<td height=45 class=tablerowrgrey>&nbsp;</td>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("	</tr>");

        d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("		<td height=25 class=tablerowrgrey>&nbsp;</td>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("	</tr>");

        d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("		<td class=tablerowlgrey>");
        d.writeln("<table bgcolor=#cccccc cellpadding=0 cellspacing=0 border=0>");
        d.writeln("     <tr>");
        d.writeln("             <td >");
        d.writeln(                     top.fDynamicButton(top.strAP_WEB_ITMZ_CLEAR,
                                                'APWBRNDL.gif',
                                                'APWBRNDR.gif',
                                                top.strAP_WEB_ITMZ_CLEAR,
                                                'javascript:top.opener.top.fClearItemize(true)',
                                                'US',
                                                false,
                                                false,
                                                'top.g_winItemize.itemizeLinesFooter'));
        d.writeln("            </td>");
        d.writeln("            <td>&nbsp;</td>");
        d.writeln("            <td>");
	d.writeln(			top.fDynamicButton(top.strAP_WEB_ITMZ_ADD_MORE_LINES,
						'APWBRNDL.gif',
						'APWBRNDR.gif',
						top.strAP_WEB_ITMZ_ADD_MORE_LINES,
						'javascript:top.opener.top.fDrawItemizeLinesBody(true)',
						'US',
						false,
						false,
						'top.g_winItemize.itemizeLinesFooter'));
        d.writeln("             </td>");
        d.writeln("     </tr>");
        d.writeln("</table>");
	d.writeln("		</td>");
        d.writeln("		<td>&nbsp;</td>");
        d.writeln("	</tr>");

        d.writeln("</table>");

        d.writeln("<TABLE CELLPADDING=0 CELLSPACING=0 border=0 WIDTH=100%>");
	d.writeln("	<TR bgcolor=#cccccc>");
        d.writeln("          <TD align=left><IMG SRC="+top.g_strImgDir+"APWCTBL.gif></TD>");
        d.writeln("          <TD><IMG SRC="+top.g_strImgDir+"APWDBPXB.gif></TD>");
        d.writeln("          <TD align=right><IMG SRC="+top.g_strImgDir+"APWCTBR.gif></TD>");
        d.writeln("	</TR>");

        d.writeln("</TABLE>");

	 // used for dynamic text updates
	 //d.writeln("<INPUT TYPE=\"hidden\" NAME=\"hidExpensedTotal\" VALUE=\""+top.fMoneyFormat("0.00")+"\">");
	 d.writeln("<INPUT TYPE=\"hidden\" NAME=\"hidOriginalReceiptTotal\" VALUE=\""+top.fMoneyFormat("0.00")+"\">");
	 d.writeln("<INPUT TYPE=\"hidden\" NAME=\"hidExcludeTotal\" VALUE=\""+top.fMoneyFormat("0.00")+"\">");
	 //d.writeln("<INPUT TYPE=\"hidden\" NAME=\"hidReimbursTotal\" VALUE=\""+top.fMoneyFormat("0.00")+"\">");
	 d.writeln("<INPUT TYPE=\"hidden\" NAME=\"hidBusinessExpensesTotal\" VALUE=\""+top.fMoneyFormat("0.00")+"\">");

        d.writeln("</center>");
        d.writeln("</form>");

	 // used for dynamic text updates
	 d.writeln("<DIV ID=\"layItemizeTotal\" STYLE=\"position:absolute; visibility:hidden; align:right\">");
	 d.writeln("</DIV>");

        d.writeln("</BODY></HTML>");
        d.close();

}// end of fDrawItemizeLinesFooter

//-----------------------------------------------------------------------------
// Function: fDrawItemizeButtonsFrame
// Desc:	draw itemize window buttons frame
//-----------------------------------------------------------------------------
function fDrawItemizeButtonsFrame(){
	 var d = top.g_winItemize.itemizeButtons.document;

        d.open();
	d.writeln("<BASE HREF=\""+top.g_strBaseHref+"\">");
        d.writeln("<HTML dir=\""+top.g_strDirection+"\">");
        d.writeln("<head>");
        top.fGenerateStyleCodes(top.g_winItemize.itemizeButtons);
        d.writeln("</head>");

        d.writeln("<BODY bgcolor=#336699>");
        d.writeln("<form name=data>");
        d.writeln("<center>");

	 // need CABO button function
        d.writeln("<table cellpadding=0 cellspacing=0 border=0 width=100%>");
        d.writeln("	<tr>");
        d.writeln("		<td>&nbsp;</td>");
	 d.writeln("	</tr>");
        d.writeln("	<tr>");
        d.writeln("		<td align=right>");
	 d.writeln("		<TABLE border=0 cellpadding=0 cellspacing=0>");
	 d.writeln("			<TR><TD rowspan=5><A href=\"javascript:top.fOKItemize();\"><IMG src="+top.g_strImgDir+"APWBRNDL.gif border=no></A></TD>");
	 d.writeln("			<TD></TD>");
	 d.writeln("			<TD height=20 width=12 rowspan=5><A href=\"javascript:top.fOKItemize();\"><IMG src="+top.g_strImgDir+"APWBRNDR.gif border=no></A></TD>");
	 d.writeln("			<TD rowspan=5>&nbsp;</TD>");
	 d.writeln("			<TD rowspan=5><A href=\"javascript:top.fCancelItemize();\"><IMG src="+top.g_strImgDir+"APWBRNDL.gif border=no></A></TD>");
	 d.writeln("			<TD></TD>");
	 d.writeln("			<TD height=20 width=12 rowspan=5><A href=\"javascript:top.fCancelItemize();\"><IMG src="+top.g_strImgDir+"APWBRNDR.gif border=no></A></TD>");

	 d.writeln("			<TR><TD height=1 bgcolor=#ffffff><IMG width=1 height=1 src="+top.g_strImgDir+"APWDBPXW.gif></TD>");
	 d.writeln("			<TD height=1 bgcolor=#ffffff><IMG width=1 height=1 src="+top.g_strImgDir+"APWDBPXW.gif></TD></TR>");

	 d.writeln("			<TR><TD height=20 align=center valign=center bgcolor=#cccccc nowrap><A  href=\"javascript:top.fOKItemize();\" class=buttontext>OK</A></TD>");
	 d.writeln("			<TD height=20 align=center valign=center bgcolor=#cccccc nowrap><A  href=\"javascript:top.fCancelItemize();\" class=buttontext>Cancel</A></TD></TR>");

	 d.writeln("			<TR><TD height=1 bgcolor=#666666><IMG width=1 height=1 src="+top.g_strImgDir+"APWDBPXB.gif></TD>");
	 d.writeln("			<TD height=1 bgcolor=#666666><IMG width=1 height=1 src="+top.g_strImgDir+"APWDBPXB.gif></TD></TR>");

	 d.writeln("			<TR><TD></TD>");
	 d.writeln("			<TD></TD></TR>");
	 d.writeln("			</TD>");

	 d.writeln("			</TD>");
	 d.writeln("		</TR></TABLE>");
        d.writeln("		</td>");
        d.writeln("	</tr>");

        d.writeln("</table>");
        d.writeln("</center>");
        d.writeln("</form>");

        d.writeln("</BODY></HTML>");
        d.close();

}// end of fDrawItemizeButtonsFrame

//-----------------------------------------------------------------------------
// Function: fCloseItemizeWindow
// Desc:	close itemization window
//-----------------------------------------------------------------------------
function fCloseItemizeWindow() { 
	top.g_winItemize.fCloseCalculatorWin();
	if (top.g_winItemize.open) top.g_winItemize.close();
	top.focus();
}

function fItemCalcAmount(value,objR){
   var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
   var l_reimbcurr_precision =0;
   var result = 0;
   if (objR){
   var action = top.fDetermineConversion(objR.getReceiptCurr(), fGetObjHeader().getReimbursCurr(),objR);


   if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;

   if (action == g_reimbEqualsRec) {
	 if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y')
     	   result = fRound( value / objR.getExchRate(), l_reimbcurr_precision);
   	 else
           result = fRound( fMultiply(value, objR.getExchRate()), l_reimbcurr_precision);
    } 
    else if (action == g_reimbEurRecFixed) {
         fixed_rate = fGetFixedRate(objR.getReceiptCurr());
         result = eval(value) / eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecEur) {
         fixed_rate = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         result = eval(value) *  eval(fixed_rate);
    } 
    else if (action == g_reimbFixedRecFixed) {
         var rate1 = fGetFixedRate(objR.getReceiptCurr());
         var rate2 = fGetFixedRate(fGetObjHeader().getReimbursCurr());
         result = fMultiply((eval(value)/rate1), rate2);
            }
    }
    return result;
}


/*
	end of R11i Receipt Itemization
*/

