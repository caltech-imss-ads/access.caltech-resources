/* $Header: czStyles.js 115.9 2000/12/10 16:10:43 pkm ship     $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99   CK Jeyaprakash, K MacClay  Created.                    |
 |                                                                           |
 +===========================================================================*/
 
function Styles(name)
{
  if (name) {
    if (Styles.instances[name]) 
      return(Styles.instances[name]);
    else {
      this.styleHash = null;
      Styles.instances[name] = this;
    }
  } else {
    if (self._styles == null) {
      self._styles = this;
      this.styleHash = null;
    } else
      return (self._styles);
  }	
}

function Styles_generateCSS(wm)
{
  var cSS ="";

  for(var stylName in this.styleHash)
    cSS += this.styleHash[stylName].generateCSS(wm)+'\n';

  return cSS;
}

function Styles_getStyle(name)
{
  if(!name) 
    return null;	
  
  if(this.styleHash) {
    if(this.styleHash[name])
      return this.styleHash[name];
  } else {
    return null;
  }
}

function Styles_addStyle(styl)
{
  if(this.styleHash == null)
    this.styleHash = new Array();

  this.styleHash[styl.getName()] = styl;
}

function Styles_createStyle(name,parent)
{
  var styl = new Style(name,parent);
  if(styl)
    this.addStyle(styl);

  return styl;
}

function Styles_clearInstance (name)
{
  if (Styles.instances[name]) {
    Styles.instances[name].destroy ();

    Styles.instances[name] = null;
    delete Styles.instances[name];
  }
}

function Styles_destroy ()
{
  if (! this.bDestroyed) {
    if (this.styleHash) {
      for (var id in this.styleHash) {
        this.styleHash[id].destroy ();
        
        this.styleHash[id] = null;
        delete this.styleHash[id];
      }
      this.styleHash = null;
      delete this.styleHash;
    }
    this.bDestroyed = true;
  }
}

//Class property (private);
Styles.instances = new Array ();

//Class Methods
Styles.clearInstance = Styles_clearInstance;

//public methods
Styles.prototype.generateCSS 	= Styles_generateCSS;
Styles.prototype.getStyle 	= Styles_getStyle;
Styles.prototype.addStyle 	= Styles_addStyle;
Styles.prototype.createStyle	= Styles_createStyle;
Styles.prototype.destroy	= Styles_destroy;

/**
*  Style class
*/
function Style(name,parent)
{	
  if(name) this.name = name;
  else return null;

  this.attributes = new Array();
  if(parent) {
    var parstyle = this.getStyle(parent);
    for (var attr in parstyle.attributes)
      this.attributes[attr] = parstyle.attributes[attr];
  }
}

function Style_getAttribute(attrName)
{
  if(this.attributes[attrName]) 
    return this.attributes[attrName];
  else return null;
}

function Style_setAttribute(attrName,v)
{
  this.attributes[attrName] = v;
}

function Style_getName()
{
  return this.name;
}

function Style_generateCSS()
{

  var attrs = this.attributes;

  var cSS = "." + this.name + " { ";
  if (attrs['font-family'])
    cSS += "font-family:" + attrs['font-family'] + "; ";
  if (attrs['font-style'])
    cSS += "font-style:" + attrs['font-style'] + "; ";
  if (attrs['font-size'])
    cSS += "font-size:" + attrs['font-size'] + "; ";
  if (attrs['font-weight'])
    cSS += "font-weight:" + attrs['font-weight'] + "; ";
  if (attrs['color'])
    cSS += "color:" + attrs['color'] + ";";
  if (attrs['text-decoration'])
    cSS += "text-decoration:" + attrs['text-decoration'] + "; ";
  if (attrs['text-align'])
    cSS += "text-align:" + attrs['text-align'] + "; ";
  if (attrs['padding-left'])
    cSS += "padding-left:" + attrs['padding-left'] + "; ";
  if (attrs['padding-right'])
    cSS += "padding-right:" + attrs['padding-right'] + "; ";
  if (attrs['padding-top'])
    cSS += "padding-top:" + attrs['padding-top'] + "; ";
  if (attrs['padding-bottom'])
    cSS += "padding-bottom:" + attrs['padding-bottom'] + "; ";

  cSS += " }";
  return cSS;
}

function Style_destroy ()
{
  if (! this.bDestoyed) {
    if (this.attributes) {
      Utils.clearCollection (this.attributes);
      this.attributes = null;
      delete this.attributes;
    }
    this.bDestroyed = true;
  }
}

Style.prototype.getName 	= Style_getName;
Style.prototype.getAttribute 	= Style_getAttribute;
Style.prototype.setAttribute 	= Style_setAttribute;
Style.prototype.generateCSS 	= Style_generateCSS;
Style.prototype.destroy 	= Style_destroy;








