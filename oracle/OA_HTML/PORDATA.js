/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: PORDATA.js 115.7 2001/06/25 19:18:36 pkm ship      $ */

top.IS_TOP=true;

TOOLTIP_IN_DELAY=1000;
TOOLTIP_OUT_DELAY=1000;

reqSessionId=0;
reqHeaderId=0;

PERCENT_DIGIT=5;
QUANTITY_DIGIT=5;
allForms= new Array();
stateInfo= new Array();

//!! data below this are temporary, to be provided by middle tier
PROFILE_OVERRIDE_REQUESTER= 'NO';
PROFILE_OVERRIDE_LOCATION= 'NO';
if(!FND_MESSAGES)
{
  FND_MESSAGES= new Array();
}

JAVASCRIPT_MESSAGE = "javascriptMessage" + VALUE_DELIMITER;
ERROR_ATTR = "errorAttr" + VALUE_DELIMITER;

ATTRIBUTE_DEPENDENCY= new Array();

//pzheng, for transaction currency update
ATTRIBUTE_DEPENDENCY["POR_UNIT_PRICE"]= ["POR_CURRENCY_UNIT_PRICE"];

/*
ATTRIBUTE_DEPENDENCY["POR_PROJECT"]= ["POR_PROJECT_ID", "POR_TASK_ID",
                                     "POR_TASK",
                                     "POR_EXPENDITURE_TYPE"];

ATTRIBUTE_DEPENDENCY["POR_TASK"]= ["POR_TASK_ID"];
*/
// pzheng, break the dependency for bug 1714690
ATTRIBUTE_DEPENDENCY["POR_PROJECT"]= ["POR_TASK",
                                     "POR_EXPENDITURE_TYPE"];

//??  contact id supplier id
ATTRIBUTE_DEPENDENCY["POR_SUPPL_NAME"]=["POR_SUPPL_SITE",
                                       "POR_SUPPL_CONTACT",
                                       "POR_SUPPL_CONTACT_PHONE"];

ATTRIBUTE_DEPENDENCY["POR_DELIVER_TO_LOCATION"]=["POR_DELIVER_TO_LOCATION_ID",
                                                "POR_DELIVER_TO_ORG",
                                                "POR_DELIVER_TO_ORG_ID",
						"POR_DELIVER_TO_SUBINVENTORY",
						"POR_SUBINVENTORY"];

ATTRIBUTE_DEPENDENCY["POR_REQUESTER"]=["POR_REQUESTER_ID",
                                      "POR_REQUESTER_PHONE",
                                      "POR_REQUESTER_FAX",
                                      "POR_REQUESTER_EMAIL"];

ATTRIBUTE_DEPENDENCY["POR_DELIVER_TO_SUBINVENTORY"]=["POR_DELIVER_TO_SUBINVENTORY_ID"];


// Bug 903689 - this array contains a list of forms that may be more than once on a page,
// but should be submitted together, even if only one of them is updated.
// Bug 940581 - onChange event on approval list window is not firing, force sending all
// forms to the server
// FORM_SUBMIT_ALL = new Array('POR_LINE_DETAILS_R', 'POR_APPROVERS_R');

// Bug 966189 - This seems to be a general issue with IE 4 - adding all single row forms
// to this array
FORM_SUBMIT_ALL = new Array('POR_LINE_DETAILS_R',
                            'POR_APPROVERS_R', 
 			    'POR_HEADER_R', 
			    'POR_INFO_TEMPLATE_R', 
                            'POR_MASS_UPDATE_R', 
                            'POR_EDIT_ATTACHMENTS_R','BILLING_CHARGE_ACCOUNT','BILLING_SINGLE_ROW','BILLING_LINE_DETAILS');


CHECK_OUT_PAGES = new Array(
	'PORDLVSR', 'PORDLVMR', 'PORDLVED', 'PORDLVEO', 'PORDLVSL',
	'PORBLNSR', 'PORBLNMR', 'PORBLNED', 'PORBLNEO', 'PORBLNSL',
	'PORBRVAC', 'PORBEDAC', 'PORBMAC',  'PORITMPS', 'PORITMPM',
	'PORNOTES', 'PORATTCH', 'PORAPPRV', 'PORAPPNO', 'PORAPADD', 'PORADAPR', 'PORSAVED',
	'PORREVPR', 'PORREVCO', 'PORREVEX'
);


/* SHOPPING_CART_PAGES
 * -------------------
 * This is a global array of template names that are in the checkout 
 * approver's checkout flow.  This is used by isShoppingCartPage() to see if 
 * the user is currently in a shopping cart page.  if so, the shopping cart 
 * icon will be active.
 */
SHOPPING_CART_PAGES = new Array(
	'PORCARTF', 'PORCARTE', 'PORAPRSC', 'PORCNFRM', 'POREXCHK',
	'PORDLVSR', 'PORDLVMR', 'PORDLVED', 'PORDLVEO', 'PORDLVSL',
	'PORBLNSR', 'PORBLNMR', 'PORBLNED', 'PORBLNEO', 'PORBLNSL',
	'PORBRVAC', 'PORBEDAC', 'PORBMAC', 'PORNOTES', 'PORATTCH', 
        'PORAPPRV', 'PORAPPNO', 'PORAPADD', 'PORADAPR', 'PORSAVED',
	'PORREVPR', 'PORREVCO', 'PORREVEX'
);


// Arrays for Image maps for Tabs
MAP_SHAPE = new Array();
MAP_COORDS = new Array();

// Arrays for Train Tracks
TRAIN_STD_CHK = new Array('ICX_POR_TRAIN_SHOPPING_CART',
                          'ICX_POR_TRAIN_DELIVERY',
                          'ICX_POR_TRAIN_BILLING',
                          'ICX_POR_TRAIN_NOTES',
                          'ICX_POR_TRAIN_APPROVERS',
                          'ICX_POR_TRAIN_REVIEW_SUBMIT');
TRAIN_POWER_CHK = new Array('ICX_POR_TRAIN_SHOPPING_CART',
                                'ICX_POR_TRAIN_POWER_CHK',
                                'ICX_POR_TRAIN_APPROVERS',
                                'ICX_POR_TRAIN_REVIEW_SUBMIT');
TRAIN_EXP_CHK = new Array('ICX_POR_TRAIN_SHOPPING_CART',
                          'ICX_POR_TRAIN_EXPRESS_CHK',
                          'ICX_POR_TRAIN_REVIEW_SUBMIT');
TRAIN_RECEIPT = new Array('ICX_POR_TRAIN_SELECT_ITEMS',
                          'ICX_POR_TRAIN_RECEIPT_INFO',
                          'ICX_POR_TRAIN_REVIEW_SUBMIT');
TRAIN_RETURN = new Array('ICX_POR_TRAIN_FIND_RECEIPT', 
                          'ICX_POR_TRAIN_RETURN_INFO',
                          'ICX_POR_TRAIN_REVIEW_SUBMIT');
