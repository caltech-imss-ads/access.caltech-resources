//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJT4.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |                                                |
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |              APWOBJCT.js is split into 4 files |
//  |              APWOBJT1.js, APWOBJT2.js ,        |
//  |              APWOBJT3.js, APWOBJT4.js          |
//  |                                                |
//  +================================================+
/* $Header: APWOBJT4.js 115.0 2000/11/02 18:06:26 pkm ship        $ */
/*===========================================================================*
 *  Currency Info Class                                                      *
 *===========================================================================*/

function objCurrencyInfo(currency, name, precision,
                                minimumAcctUnit,
                                euroRate, datEffective)
{
  this.currency = currency;
  this.name = name;
  this.precision = precision;
  this.minimumAcctUnit = minimumAcctUnit;
  this.euroRate = euroRate;
  this.datEffective = datEffective;

}

objCurrencyInfo.prototype.mGetCurrency = Currency_mGetCurrency;
objCurrencyInfo.prototype.mGetName = Currency_mGetName;
objCurrencyInfo.prototype.mGetPrecision = Currency_mGetPrecision;
objCurrencyInfo.prototype.mGetMinimumAcctUnit = Currency_mGetMinimumAcctUnit;
objCurrencyInfo.prototype.mGetEuroRate = Currency_mGetEuroRate;
objCurrencyInfo.prototype.mGetdatEffective = Currency_mGetdatEffective;
objCurrencyInfo.prototype.mSetCurrency = Currency_mSetCurrency;
objCurrencyInfo.prototype.mSetName = Currency_mSetName;
objCurrencyInfo.prototype.mSetPrecision = Currency_mSetPrecision;
objCurrencyInfo.prototype.mSetMinimumAcctUnit = Currency_mSetMinimumAcctUnit;
objCurrencyInfo.prototype.mSetEuroRate = Currency_mSetEuroRate;
objCurrencyInfo.prototype.mSetEffectiveDate = Currency_mSetEffectiveDate;

function Currency_mGetCurrency(){	
	return this.currency;
}

function Currency_mGetName(){	
	return this.name;
}

function Currency_mGetPrecision(){	
	return this.precision;
}

function Currency_mGetMinimumAcctUnit(){	
	return this.minimumAcctUnit;
}

function Currency_mGetEuroRate(){	
	return this.euroRate
}

function Currency_mGetdatEffective(){	
	return this.datEffective
}

function Currency_mSetCurrency(currency){	
	this.currency = currency;
}

function Currency_mSetName(name){	
	this.name = name;
}

function Currency_mSetPrecision(precision){	
	this.precision = precision;
}

function Currency_mSetMinimumAcctUnit(minimumAcctUnit){	
	this.minimumAcctUnit = minimumAcctUnit;
}

function Currency_mSetEuroRate(euroRate){	
	this.euroRate = euroRate;
}

function Currency_mSetEffectiveDate(datEffective){	
	this.datEffective = datEffective;
}

/*===========================================================================*
 *  Value Class                                                              *
 *===========================================================================*/

// This object stores info of an item in a poplist for one DFF segment 
function Value(p_strCode, p_strDesc) {
  this.strDesc = p_strDesc;
  this.strCode = p_strCode;
}

Value.prototype.mGetstrDesc = mGetstrDesc;
Value.prototype.mGetstrCode = mGetstrCode;
Value.prototype.mSetrDesc = mSetDesc;
Value.prototype.mSetCode = mSetCode;

function mGetstrDesc() {  
	return this.strDesc;
}

function mGetstrCode() {  
	return this.strCode;
}

function mSetDesc(strDesc) {  
	this.strDesc = strDesc;
}

function mSetCode(strCode) {  
	this.strCode = strCode;
}

/*===========================================================================*
 *  Values Class - Container class of Value Objects                          *
 *===========================================================================*/

function Values(p_strName) {
  this.strName = p_strName;
  this.arrValue = new Array();
}

Values.prototype.mAddValue = Values_mAddValue;
Values.prototype.miGetNumOfValues = Values_miGetNumOfValues;
Values.prototype.mobjFindValue = Values_mobjFindValue;
Values.prototype.mGetstrName = Values_mGetstrName;

function Values_mGetstrName() {
  return this.strName;
}

function Values_mAddValue(p_objValue) {
  this.arrValue[this.arrValue.length] = p_objValue;
}

function Values_miGetNumOfValues() {
  return this.arrValue.length;
}

function Values_mobjFindValue(p_strCode) {

  for(var i=0; i<this.arrValue.length; i++) {
    if (this.arrValue[i].mGetstrCode() == p_strCode)
      return this.arrValue[i].mGetstrDesc();
  }
  return "";
}

/*===========================================================================*
 *  CollectionOfValues Class                                                 *
 *===========================================================================*/

function CollectionOfValues() {
  this.arrValues = new Array();
}

CollectionOfValues.prototype.mobjAddValues = mobjAddValues;
CollectionOfValues.prototype.mobjGetValues = mobjGetValues;

function mobjAddValues(p_objValues) {
  this.arrValues[p_objValues.mGetstrName()] = p_objValues;
  return p_objValues;
}

function mobjGetValues(p_strName) {
  var undefined;

  var objValues = this.arrValues[p_strName];
  if (objValues == undefined)
    return null;
  return objValues;
}

/*===========================================================================*
 *  Flexfield Class                                                          *
 *===========================================================================*/

function FlexField(p_strPrompt, p_bIsRequired, p_strDefaultValue, p_iObjectType,
                 p_iTextBoxLength, p_objPoplistValues, p_bIsNumericOnly, 
                 p_bIsUpperCaseOnly) {
  
  this.strPrompt = p_strPrompt;
  this.bIsRequired = p_bIsRequired;
  this.strDefaultValue = p_strDefaultValue;
  this.iObjectType = p_iObjectType;
  this.iTextBoxLength = p_iTextBoxLength;
  this.objPoplistValues = p_objPoplistValues;
  this.bIsNumericOnly = p_bIsNumericOnly;
  this.bIsUpperCaseOnly = p_bIsUpperCaseOnly;
}

FlexField.prototype.mGetstrPrompt = mGetstrPrompt;
FlexField.prototype.mGetbIsRequired = mGetbIsRequired;
FlexField.prototype.mGetstrDefaultValue = mGetstrDefaultValue;
FlexField.prototype.mGetiObjectType = mGetiObjectType;
FlexField.prototype.mGetiTextBoxLength = mGetiTextBoxLength;
FlexField.prototype.mGetobjPoplistValues = mGetobjPoplistValues;
FlexField.prototype.mGetiColumn = mGetiColumn;
FlexField.prototype.mGetbIsNumericOnly = mGetbIsNumericOnly;
FlexField.prototype.mGetbIsUpperCaseOnly = mGetbIsUpperCaseOnly;
FlexField.prototype.mValidateFlexField = mValidateFlexField;
FlexField.prototype.mSetPrompt = mSetPrompt;
FlexField.prototype.mSetIsRequired = mSetIsRequired;
FlexField.prototype.mSetDefaultValue = mSetDefaultValue;
FlexField.prototype.mSetObjectType = mSetObjectType;
FlexField.prototype.mSetTextBoxLength = mSetTextBoxLength;
FlexField.prototype.mSetColumn = mSetColumn;
FlexField.prototype.mSetIsNumericOnly = mSetIsNumericOnly;
FlexField.prototype.mSetIsUpperCaseOnly = mSetIsUpperCaseOnly;


function mGetstrPrompt() {  
	return this.strPrompt;
}

function mGetbIsRequired() {  
	return this.bIsRequired;
}

function mGetstrDefaultValue() {  
	return this.strDefaultValue;
}

function mGetiObjectType() {  
	return this.iObjectType;
}

function mGetiTextBoxLength() {  
	return this.iTextBoxLength;
}

function mGetobjPoplistValues() {  
	return this.objPoplistValues;
}

function mGetiColumn() {  
	return this.iColumn;
}

function mGetbIsNumericOnly() {  
	return this.bIsNumericOnly;
}

function mGetbIsUpperCaseOnly() {  
	return this.bIsUpperCaseOnly;
}

function mSetPrompt(strPrompt) {  
	this.strPrompt = strPrompt;
}

function mSetIsRequired(bIsRequired) {  
	this.bIsRequired = bIsRequired;
}

function mSetDefaultValue(strDefaultValue) {  
	this.strDefaultValue = strDefaultValue;
}

function mSetObjectType(iObjectType) {  
	this.iObjectType = iObjectType;
}

function mSetTextBoxLength(iTextBoxLength) {  
	this.iTextBoxLength = iTextBoxLength;
}

function mSetColumn(iColumn) {  
	this.iColumn = iColumn;
}

function mSetIsNumericOnly(bIsNumericOnly) {  
	this.bIsNumericOnly = bIsNumericOnly;
}

function mSetIsUpperCaseOnly(bIsUpperCaseOnly) {  
	this.bIsUpperCaseOnly = bIsUpperCaseOnly;
}

function mValidateFlexField(p_strValue) {

  if ((this.mGetbIsRequired()) && (p_strValue == "")) {
    var strErrorMsg1 = top.g_objMessages.mstrGetMesg("AP_WEB_FLEX_FIELD_REQUIRED");
    strErrorMsg1 = strErrorMsg1.replace("&FLEXFIELD_NAME",this.mGetstrPrompt());
    strErrorMsg1 = strErrorMsg1.replace("&FLEXFIELD_NAME",this.mGetstrPrompt());
  }
  if ((this.mGetbIsNumericOnly()) && (!fIsNumericOnly(p_strValue))) {
    var strErrorMsg2 =  top.g_objMessages.mstrGetMesg("AP_WEB_CUSTOM_FIELD_NUMONLY");
    strErrorMsg2 = strErrorMsg2.replace("&CUSTFIELD", this.mGetstrPrompt());
  }
  if ((this.mGetbIsUpperCaseOnly()) && (!fIsAlphaUpperCaseOnly(p_strValue))) {
    var strErrorMsg3 =  top.g_objMessages.mstrGetMesg("AP_WEB_CUSTOM_FIELD_ALPHAUPPER");
    strErrorMsg3 = strErrorMsg3.replace("&CUSTFIELD", this.mGetstrPrompt());
  }

  var strErrorMsg = "";
  if (strErrorMsg1) strErrorMsg += strErrorMsg1+"<BR>";
  if (strErrorMsg2) strErrorMsg += strErrorMsg2+"<BR>";
  if (strErrorMsg3) strErrorMsg += strErrorMsg3+"<BR>";
  return strErrorMsg;

}

/*===========================================================================*
 *  Context Class                                                            *
 *===========================================================================*/

function Context(p_strContextName) {
  this.strContextName = p_strContextName;
  this.arrFlexFields = new Array();
}

Context.prototype.mGetstrContextName = mGetstrContextName;
Context.prototype.mGetobjFlexFields = mGetobjFlexFields;
Context.prototype.mGetstrWidgetName = mGetstrWidgetName;
Context.prototype.mGetstrSegStyle = mGetstrSegStyle;
Context.prototype.mCopyFlexField = mCopyFlexField;
Context.prototype.mAddFlexField = mAddFlexField;
Context.prototype.mobjGetFlexField = mobjGetFlexField;
Context.prototype.miGetNumOfFlexField = miGetNumOfFlexField;


function mAddFlexField(p_objFlexField) {
  this.arrFlexFields[this.arrFlexFields.length] = p_objFlexField;
}

function mobjGetFlexField(p_strPrompt) {
  for(var i=0; i<this.arrFlexFields.length; i++)
    if (this.arrFlexFields[i].strPrompt == p_strPrompt)
      return this.arrFlexFields[i];
  return null;
}

function mCopyFlexField(p_arrFlexFields) {
  for(var i=0; i<p_arrFlexFields.length; i++)
    this.mAddFlexField(p_arrFlexFields[i]);
}

function miGetNumOfFlexField() {
  return this.arrFlexFields.length;
}

function mGetstrContextName() {
  return this.strContextName;
}

function mGetobjFlexFields() {
  return this.arrFlexFields;
}

function mGetstrWidgetName(strSegName,iBaseIndex){
  for (var i=0;i<this.miGetNumOfFlexField();i++){
	if (this.arrFlexFields[i].mGetstrPrompt() == strSegName){
	  var strTemp = this.arrFlexFields[i].mGetstrPrompt();
	  strTemp = top.ReplaceInvalidChars(strTemp);
	  return strTemp;
	  }
  }
  return "";
}

function mGetstrSegStyle(strSegName){
  for (var i=0;i<this.objFlexFields.miGetNumOfFlexField();i++){
	if (this.arrFlexFields[i].mGetstrPrompt() == strSegName)
	  return this.arrFlexFields[i].mGetiObjectType();
  }
  return "";
}

/*===========================================================================*
 *  Contexts Class                                                           *
 *===========================================================================*/

function Contexts() {
  this.arrContext = new Array();
}

Contexts.prototype.mobjAddContext = mobjAddContext;
Contexts.prototype.mobjGetContext = mobjGetContext;
Contexts.prototype.mobjGetContextWithDefault = mobjGetContextWithDefault;


function mobjAddContext(p_objContext) {
  this.arrContext[p_objContext.mGetstrContextName()] = p_objContext;
  return p_objContext;
}

function mobjGetContext(p_strName) {
  var undefined;
  var objContext = this.arrContext[p_strName];
  if (objContext == undefined)
    return null;
  return objContext;
}

function mobjGetContextWithDefault(p_strName) {
  var objContext = this.mobjGetContext(p_strName);

  if (objContext == null)  
    objContext = this.mobjGetContext(C_strGLOBALCONTEXTNAME);
  return objContext;
}

/*===========================================================================*
 *  Employee Class                                                           *
 *===========================================================================*/

function empObject(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum){
	this.strEmployeeName = strEmployeeName;
	this.strEmployeeID = strEmployeeID;
	this.strCostCenter = strCostCenter;
	this.bProjectEnabled = bProjectEnabled;
	this.strEmployeeNum = strEmployeeNum;
}


empObject.prototype.mGetstrEmployeeName = mGetstrEmployeeName;
empObject.prototype.mGetstrEmployeeID = mGetstrEmployeeID;
empObject.prototype.mGetstrCostCenter = mGetstrCostCenter;
empObject.prototype.mGetbProjectEnabled = mGetbProjectEnabled;
empObject.prototype.mGetstrEmployeeNum = mGetstrEmployeeNum;
empObject.prototype.mSetEmployeeName = mSetEmployeeName;
empObject.prototype.mSetEmployeeID = mSetEmployeeID;
empObject.prototype.mSetCostCenter = mSetCostCenter;
empObject.prototype.mSetProjectEnabled = mSetProjectEnabled;
empObject.prototype.mSetEmployeeNum = mSetEmployeeNum;
empObject.prototype.mGetstrCostCenter = mGetstrCostCenter;
empObject.prototype.mGetbIsProjectEnabled = mGetbIsProjectEnabled;
empObject.prototype.mGetEmployeeName = mGetEmployeeName;
empObject.prototype.mGetEmployeeNum = mGetEmployeeNum;
	
function mGetstrEmployeeName(){	
	return this.strEmployeeName;
}

function mGetstrEmployeeID(){	
	return this.strEmployeeID;
}

function mGetstrCostCenter(){	
	return this.strCostCenter;
}

function mGetbProjectEnabled(){	
	return this.bProjectEnabled;
}

function mGetstrEmployeeNum(){	
	return this.strEmployeeNum;
}

function mSetEmployeeName(strEmployeeName){	
	this.strEmployeeName =  strEmployeeName;
}

function mSetEmployeeID(strEmployeeID){	
	this.strEmployeeID = strEmployeeID;
}

function mSetCostCenter(strCostCenter){	
	this.strCostCenter = strCostCenter;
}

function mSetProjectEnabled(bProjectEnabled){	
	this.bProjectEnabled = bProjectEnabled;
}

function mSetEmployeeNum(strEmployeeNum){  
	this.strEmployeeNum = strEmployeeNum;
}

function mGetstrCostCenter(){	
	return this.strCostCenter;
}

function mGetbIsProjectEnabled(){	
	return this.bProjectEnabled;
}

function mGetEmployeeName(){	
	return this.strEmployeeName;	
}

function mGetEmployeeNum(){	
	return this.strEmployeeNum;
}

/*===========================================================================*
 *  Employees Class                                                          *
 *===========================================================================*/

function Employees(){
  this.arrEmployee = new Array();
}
new Employees();

Employees.prototype.mobjGetEmployee = mobjGetEmployee;
Employees.prototype.mnGetNumOfEmployees = mnGetNumOfEmployees;
Employees.prototype.mobjAddEmployee = mobjAddEmployee;

function mnGetNumOfEmployees() {
  return this.arrEmployee.length;
}


function mobjGetEmployee(strID,strName) {
  if (strID){
    for (var i=0;i<this.mnGetNumOfEmployees();i++){
	if (this.arrEmployee[i].mGetstrEmployeeID() == strID)
	  return this.arrEmployee[i];
    }
  }
  else if (strName){
    for (var i=0;i<this.mnGetNumOfEmployees();i++){
	if (this.arrEmployee[i].mGetstrEmployeeName() == strName)
	  return this.arrEmployee[i];
    }
  }
  return null;
}

function mobjAddEmployee(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum){
	this.arrEmployee[this.arrEmployee.length] = new empObject(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum);
}



























