//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawcbbtn.js                       |
//  |                                                |
//  | Description: This is the standalone JavaScript |
//  |              function for rendering buttons in |
//  |              the blue and gray areas for the   |
//  |              entire applucation.               |
//  |                                                |
//  | History:     Created by Anitha Andra           |
//  |                                                |
//  +================================================+
/* $Header: pawcbbtn.js 115.2 2000/09/01 13:39:08 pkm ship      $ */


var b_enabled = true;
var b_disabled = false;
var b_normal = false;
var b_default = true;
var b_wide_gap = "W";
var b_narrow_gap = "N";
var b_no_gap = "";
var b_locator = "L";
var wya_steps = 4;
var drop_steps = 0;
var wya_max_chars = 60;
var loaded=1

function makebuttons () 
{
  var output = "";
  var row = new Array("","","","","");
  var p_type="";
  var p_command="";
  var p_text="";
  var p_default=false;
  var p_enabled=true;
  var p_gap="N";
  var p_current=1;
  
  if (!makebuttons.arguments) return output;
  
  for (i = 0; i<=makebuttons.arguments.length; i++) 
  {
    if (!makebuttons.arguments[i]) break;  //End of arguments
    p_type = makebuttons.arguments[i]; i++;  // First argument
    if (p_type == b_locator)  //Is first argument locator??
	{
      p_text = makebuttons.arguments[i]; i++;
      p_current = makebuttons.arguments[i];   //Next 2 parameters for locator
      p_text = formatwya(p_text, p_current);      //Format locator string given the current location

 	// Output html	  
      row[0] +=  "<td rowspan=5 ><img  border=0 src='" + g_strImagePath + "FNDBWHRL.gif'></td>" +
          "<td class=darkshadow><img  border=0 src='" + g_strImagePath + "FNDPXG2.gif'></td>" +
          "<td rowspan=5 ><img  border=0 src='" + g_strImagePath + "FNDBWHRR.gif'></td>" +
          "<td width=5 rowspan=5></td>";
      row[1] +=  "<td></td>";
      row[2] +=  "<td height=20 nowrap><font class=promptwhite>" + p_text + "</font></td>";
      row[3] +=  "<td></td>";
      row[4] +=  "<td class=color4><img  border=0 src='" + g_strImagePath + "FNDPXG4.gif'></td>"
    }
    else 
    {
      p_text = makebuttons.arguments[i]; i++;  // Is first argument button??
      p_command = buildHrefb(makebuttons.arguments[i]); i++;  //Next 4 parameters for button
      p_enabled = makebuttons.arguments[i]; i++;
      p_default = makebuttons.arguments[i]; i++;
      p_gap = makebuttons.arguments[i];
	  
      row[0] +=   "<td rowspan=5>" + p_command + "><img border=0 src='" + g_strImagePath;

        	//Gray area vs blue area
      row[0] +=   (p_default) ? ((p_type.charAt(0)=="R") ? "FNDBRNDL.gif" : 
        "FNDBSQRL.gif") :
        ((p_type.charAt(0)=="R") ? "PAGBRNDL.gif" : "FNDBSQRL.gif");
      row[0] +=   "' border=no></a></td>";
      row[0] +=   (p_default) ? "<td class=darkshadow><img border=0 src='" + g_strImagePath + "FNDPXG2.gif'></td>" : "<td height=1></td>";
      row[0] +=   "<td rowspan=5>" + p_command + "><img border=0 src='" + g_strImagePath;
      row[0] +=   (p_default) ? ((p_type.charAt(1)=="R") ? "FNDBRNDR.gif" : 
        "FNDBSQRR.gif") :
        ((p_type.charAt(1)=="R") ? "PAGBRNDR.gif" : "FNDBSQRR.gif");
      row[0] +=   "' border=no></a></td>";


	//Gap between buttons
      if (p_gap == "N" || p_gap == "W") 
	  {
        row[0] +=  (p_gap == "N") ? "<td width=5 rowspan=5></td>" : "<td width=15 rowspan=5></td>"; 
	  }
      row[1] +=   "<td class=highlight><img border=0 src='" + g_strImagePath + "FNDPX6.gif'></td>";
      row[2] +=  "<td class=button height=20 nowrap>";
      if (p_enabled) 
	  {
        row[2] +=  p_command + " class=buttontext>" + p_text + "</a></td>"; 
      } 
	  else 
	  {
        row[2] +="  <font class=disabledbuttontext>" + p_text + "</font></td>"; 
      }
      row[3] +=  "<td class=shadow><img border=0 src='" + g_strImagePath + "FNDPXG3.gif'></td>";
      row[4] +=  (p_default) ? "<td class=darkshadow><img border=0 src='" + g_strImagePath + "FNDPXG2.gif'></td>" : "<td height=1></td>";
    }  
  }
  output += "<table cellpadding=0 cellspacing=0 border=0>";
  output += "<tr>" + row[0] + "</tr><tr>" + row[1] + "</tr><tr>" + row[2];
  output += "</tr><tr>" + row[3] + "</tr><tr>" + row[4] + "</tr></table>";
  return output;
}
function buildHrefb (Ref) 
{
  if (!Ref) 
    { 
    return "<a href='#'" 
    }
  if (Ref.indexOf(".htm") == -1) 
    { 
    return "<a href='javascript:" + Ref + "' " 
//    return "<a href='" + Ref + "' " 
    }
  if (Ref.indexOf("target") == -1) 
  { 
    return "<a href=" + Ref + " target=_top" 
  }
  return "<a href=" + Ref;
}  

function formatwya (p_text, p_current, p_drop_step) 
{
  var hold_current = p_current;
  var howmany = (wya_steps > 1) ? wya_steps - 1 : 1;
  if (p_drop_step) 
  {
    drop_steps ++
    howmany = (howmany > 1) ? howmany - drop_steps : 1;
  }
  var wya = ""
  if (!p_text) return wya;
  if (!isNaN(p_current)) p_current = p_current - 1;
  var optarray = p_text.split(">");
  wya += "<a title='On step " + hold_current + " of " + optarray.length +"'>";
  var lastindex = optarray.length - 1
  var w_start = 0;
  var w_end = (lastindex < howmany) ? lastindex : howmany;
  if (lastindex > howmany) 
  {
    if (p_current < howmany) 
	{ 
      w_start = 0; 
      w_end = howmany;
    }
    else 
	{
      if (p_current > lastindex-howmany) 
	  {
        w_start = lastindex - howmany; 
        w_end = lastindex;
      }
      else 
	  {
        w_start = p_current - 1; 
        w_end = p_current + (howmany - 1);
      }  
    }  
  }
  var check_length = 0;
  if (w_start > 0) 
  {
    wya += " ... > ";
    check_length = 6;
  }
  for (var x=w_start; x<=w_end; x++) 
  {
    if (x == p_current) 
	{ 
      wya += "<b>" + optarray[x] + "</b>";
    }
    else 
	{
      wya += optarray[x];
    }
    check_length = check_length + optarray[x].length
    if (x < w_end) 
	{ 
      wya += "&nbsp;&gt;&nbsp;";
      check_length = check_length + 3;
    }
  }
  if (w_end < optarray.length - 1) 
  {
    wya += " > ... ";
    check_length = check_length + 6;
  }
  wya += "</a>";
  if (check_length > wya_max_chars) 
  {
    return formatwya (p_text, hold_current, true);
  } 
  else 
  {
    return wya;
  }
}




















