/* $Header: ibemcsht.js 115.2 2000/05/08 14:36:57 pkm ship      $ */
function addToList(src, dst, emptyText)
{
  //
  // If there are no items in source (i.e. the src has only "-1" entry),
  // do nothing, just return
  //
  if(src.options.length == 1 && 
     src.options[0].value == -1)
  {
    return;
  }

  var newSrcOpts = new Array(src.options.length);

  //
  // Go thru the source list and copy all the selected items from source
  // to destination. Also mark the
  //
  for(var i = 0; i < src.options.length; i++)
  {
    if(src.options[i].selected)
    {
      if(dst.options.length == 1 &&
         dst.options[0].value == -1)
      {
        dst.options[0] = null;
      }

      dst.options[dst.options.length] = new Option(src.options[i].text,
                                                   src.options[i].value);
    }

    // copy into the new list and mark the ones to be deleted later
    newSrcOpts[i] = new Option(src.options[i].text, src.options[i].value);
    newSrcOpts[i].selected = src.options[i].selected;
  } /* end for i */

  //
  // Remove all the entries from the source (src)
  //
  while(src.options.length > 0)
  {
    src.options[0] = null
  }

  //
  // Copy back the non-selected entries into source (src) from newSrcOpts
  //
  var j = 0;
  for(var i = 0; i < newSrcOpts.length; i++)
  {
    if(newSrcOpts[i].selected == false)
    {
      src.options[j] = new Option(newSrcOpts[i].text, newSrcOpts[i].value);
      j++;
    }
  }

  //
  // If the list is empty, then set it with "-1" entry
  //
  if(src.options.length == 0)
  {
    src.options[0] = new Option(emptyText, -1);
  }
}

function addAllToList(src, dst, emptyText)
{
  //
  // If there are no items in source (i.e. the src has only "-1" entry),
  // do nothing, just return
  //
  if(src.options.length == 1 && 
     src.options[0].value == -1)
  {
    return;
  }

  var newSrcOpts = new Array(src.options.length);

  //
  // Go thru the source list and copy all the selected items from source
  // to destination. Also mark the
  //
  for(var i = 0; i < src.options.length; i++)
  {
    // if the destination has a "-1" entry, remove it (as it is going to be
    // filled by a non "-1" entry
    if(dst.options.length == 1 &&
       dst.options[0].value == -1)
    {
      dst.options[0] = null;
    }

    dst.options[dst.options.length] = new Option(src.options[i].text,
                                                 src.options[i].value);

    // copy into the new list and mark the ones to be deleted later
    newSrcOpts[i] = new Option(src.options[i].text, src.options[i].value);
    newSrcOpts[i].selected = src.options[i].selected;
  } /* end for i */

  //
  // Remove all the entries from the source (src)
  //
  while(src.options.length > 0)
  {
    src.options[0] = null
  }

  // Put "-1" entry in the source (src) list
  src.options[0] = new Option(emptyText, -1);
}
