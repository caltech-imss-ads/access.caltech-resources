/* $Header: czAll.js 115.83 2001/08/10 10:48:45 pkm ship    $  */
var _client_version = "BLD-16-25";
/*  czHtmHpr.js 115.4 2001/05/22 15:10:00 cjeyapra ship $ */

function _importPrototypes()
{
  if(arguments[0]) {
    for(var i=1; i<arguments.length;i++) {
      if(arguments[i])
        if(arguments[i].prototype) {
          for(var prop in arguments[i].prototype)
            arguments[0].prototype[prop] = arguments[i].prototype[prop];
        } else
          alert ("Import Prototypes: INVALID_CLASS");
    }
  }
}

/*
cross browser functions to manipulate HTML elements
*/

function HTMLHelper()
{
  return HTMLHelper;
}

function _setHTMLObjDimensions(htmlObj,lt,tp,wd,ht,clipRgn)
{
  if(lt!=null) htmlObj.left = lt;
  if(tp!=null) htmlObj.top = tp;
  if(wd!=null) htmlObj.width = wd;
  if(ht!=null) htmlObj.height = ht;
  
  if(clipRgn != 'undefined' && clipRgn != null) {
    if (ns4) {	 
      var clipv = clipRgn.split("rect(")[1].split(")")[0].split("px");
      htmlObj.clip.top = clipv[0];
      htmlObj.clip.right = clipv[1];
      htmlObj.clip.bottom = clipv[2];
      htmlObj.clip.left = clipv[3];
    } else if (ie4) {
      var clipv = clipRgn.split("rect(")[1].split(")")[0].split("px");
      htmlObj.clip = "rect("+clipv[0]+"px "+clipv[1]+"px "+clipv[2]+"px "+clipv[3]+"px)"; 
    }
  }
}

function _setHTMLBGColor(htmlObj,color)
{
  if(ns4) {
    if(!color || color=='transparent')
      htmlObj.bgColor = null;
    else
      htmlObj.bgColor = color;
  } else if(ie4) {
    if(!color)
      htmlObj.backgroundColor ='transparent';
    else
      htmlObj.backgroundColor = color;
  }
}

function _setHTMLVisibility(htmlObj,bShow)
{
  if(bShow)  
    var show = "inherit";
  else 
    var show = (ns4)? "hide" : "hidden";
  
  if(htmlObj) 
    htmlObj.visibility = show;
}

function _setHTMLFGColor (htmlObj, color)
{
  htmlObj.color = color;
}

function _writeToLayer(lyr, strContent)
{
  if(lyr) {	 
    if (ns4) {
      lyr.document.open ();
      lyr.document.write(strContent);
      lyr.document.close();
    } else if(ie4) {
      lyr.innerHTML = "";
      lyr.innerHTML = strContent;
    }
  }
}

//creating new layer
function _createLayer(id, parLayer,frm,content, styleClass)
{
  var lyr=null;
  var parDoc = null;
  
  if(frm == null) 
    frm = self;
  
  if(ns4) {
    if(parLayer)
      parDoc = parLayer.document;
    else 
      parDoc = frm.document;
    
    if(parLayer)
      lyr = parDoc.layers[id] = new Layer(100,parLayer);
    else 
      lyr = parDoc.layers[id] = new Layer(100,frm);
    
    if(content) {
      lyr.document.open();
      lyr.document.write(content);
      lyr.document.close();
    }
  } else if(ie4) {
    if(parLayer) 
      parDoc = parLayer;
    else 
      parDoc = frm.document.body;
    
    var txtCont = '<DIV ID='+id;
    if(styleClass)
      txtCont +=' CLASS ='+styleClass;
    txtCont +=' style="position:absolute;';
    txtCont += '"> ';
    if(content)
      txtCont += content;
    txtCont += '</DIV>';
    
    parDoc.insertAdjacentHTML('BeforeEnd',txtCont)
      lyr = parDoc.all[id];
  }
  return lyr;
}

function _destroyLayer(lyr)
{
  if(ns4) {
    //lyr.visibility = 'hide';
    //delete lyr;
    delete document.layers['base1'];
    //lyr = null;
  } else if(ie4) {
    lyr.style.visibility = 'hidden';
    lyr.innerHTML = "";
    lyr.outerHTML = "";
  }

}

//Class methods
HTMLHelper.setDimensions 	= _setHTMLObjDimensions;
HTMLHelper.setBackgroundColor 	= _setHTMLBGColor;
HTMLHelper.setForegroundColor 	= _setHTMLFGColor;
HTMLHelper.setVisible		= _setHTMLVisibility;
HTMLHelper.createLayer		= _createLayer;
HTMLHelper.destroyLayer		= _destroyLayer;
HTMLHelper.writeToLayer		= _writeToLayer;
HTMLHelper.importPrototypes	= _importPrototypes;


/*  czLtner.js 115.9 2000/11/07 19:34:32 tabbott ship $ */

function Listener()
{
}

function Listener_addListener(sub,target,type)
{
  if(!this.Listeners) 
    this.Listeners = new Array();
  if (!this.Listeners[sub])
    this.Listeners[sub] = new Array();
  
  var currListener = this.Listeners[sub];	
  if(type =='super'){
    for(var i=currListener.length; i>0; i--)
      currListener[i] = currListener[i-1];
    currListener[0] = target;
  }
  else 
    currListener[currListener.length] = target;
}

function Listener_removeListener(sub,target)
{
  var currListener;
  if(this.Listeners) {
    if(this.Listeners[sub]) {
      currListener = this.Listeners[sub];
      var i=0;
      var len = currListener.length;
      while (i < len) {
        if (currListener[i] == target) {
          for (var j=i; j<len-1; j++) {
            currListener[j] = currListener[j+1];
          }
          currListener[len-1] = null;
          currListener.length-- ;
          break;
        } else
          i++
      }
    }
  }
}

function Listener_notifyListeners(sub,e,src)
{

  var result = true;

  if(!src)
    src = this;
  
  if(this.Listeners) {
    if (this.Listeners[sub]) {
      var listeners = this.Listeners[sub];
      //before starting to notify make a copy of listener references;
      var curListeners = new Array ();
      var len = listeners.length;
      for (var j=0; j < len; j++) {
        curListeners[j] = listeners[j];
      }
      for(var i = 0; i < len; i++) {
        if(result) {
          if('object' == typeof curListeners[i]) {
            if(eval('curListeners[i].'+sub))
              result = eval('curListeners[i].'+sub+'(e, src)');
          }
          else if ('function' == typeof curListeners[i]) {		
            if(curListeners[i])
              result = curListeners[i](e,src);
          }
          else 
            result = eval(curListeners[i]);
        }
        else break;
      }
    }
  }
  return result;
} 

function Listener_clearQueue () 
{
  var id;
  for (id in this.Listeners) {
    if (this.Listeners[id]) {
      var len = this.Listeners[id].length;
      for (var i=0; i<len; i++) {
        this.Listeners[id][i] = null;
        delete this.Listeners[id][i];
      }
      this.Listeners[id].length = 0;
    }
    this.Listeners[id] = null;
    delete this.Listeners[id];
  }
  this.Listeners = null;
  delete this.Listeners;
}

Listener.prototype.addListener 		= Listener_addListener;
Listener.prototype.removeListener 	= Listener_removeListener;
Listener.prototype.notifyListeners 	= Listener_notifyListeners;
Listener.prototype.clearQueue = Listener_clearQueue;

/*  czError.js 115.8 2001/05/22 15:09:58 cjeyapra ship $ */
 
function Error()
{
  return null;
}

function Error_processError(str)
{
  if(!self._DEBUG) 
    return;
  
  if(!self._TRACE) {
    str = arguments.caller.callee.name +'>>'+str;
  } else {		
    var strSource="";
    var args = arguments;
    while(args.caller)
    {
      strSource =args.caller.callee.name +' >> '+strSource;
      args = args.caller;
    }
    if(strSource)
      str = strSource + '>>'+str;
  }
  alert(str);
}

Error.processError = Error_processError;


/*  czBase.js 115.22 2001/05/23 20:00:29 cjeyapra ship $ */

if(!self.HTMLHelper) {
  alert('HelperFuncs.js not included');
}

function Base() 
{
  this.self = "Base" + Base.baseCount;
  Base.baseCount++;
  this.visibility = true;

  this.htmlObj = null;
  this.block = null;
  this._3DLook = false;
}

function Base_setReference(r)
{
  this.reference = r;
  if (this.border) {
    this.border.setReference (r);
    if (ns4) {
      this.reference = r + '.layers["' + this.border.name +'"].document';
    }
  }
  if (this.moreSetReference) {
    this.moreSetReference(this.reference);
  }
}

function Base_readOnly(bFlag)
{
   this.read = bFlag;
}

function Base_updateDone()
{
  //empty function
}

function Base_hideFeature()
{
  //empty function
}

function Base_setNoBorder(bBorder)
{
   this.noBorder = bBorder;
}

function Base_setLeft(lt)
{
  var oldLt;
  if(lt!=null) {
    if(isNaN(lt)) {
      alert (lt + " Not a Number");
      return;
    }
    oldLt = this.left;
    this.left = lt;
    if (this.border) {
      oldLt = this.border.left;
      this.border.setLeft (lt);
      this.left = this.borderWidth;
    }
    if (this.launched) {
      if (this.glue) 
        this.glue.setLeft(this, this.left);
      else 
        HTMLHelper.setDimensions(this.getHTMLObj(), this.left, null);
    }
    if(this.prefSystem && oldLt!= lt)
      this.prefSystem.notify(this,'left',lt);
    
    if (this.moreSetLeft)
      this.moreSetLeft (lt);
  }
}

function Base_setTop(tp)
{
  var oldTp;
  if(tp!=null) {
    if(isNaN(tp)) {
      alert (tp + " Not a Number");
      return;
    }
    oldTp = this.top;
    this.top = tp;
    if (this.border) {
      oldTp = this.border.top;
      this.border.setTop (tp);
      this.top = this.borderWidth;
    }
    if (this.launched) {
      if (this.glue)
        this.glue.setTop(this, this.top);
      else 
        HTMLHelper.setDimensions(this.getHTMLObj(),null, this.top);
    }
    if(this.prefSystem && oldTp!= tp)
      this.prefSystem.notify(this,'top',tp);
    
    if (this.moreSetTop)
      this.moreSetTop (tp);
  }
}

function Base_setName (nm)
{
  //name cannot be changed after launch (design time property);
  if (this.launched) {
    alert ("Read only property");
    return;
  }
  this.objId = this.name = nm;
  
  if (this.border)
    this.border.setName ('BDR-' + this.name);
  
  if (this.moreSetName) {
    this.moreSetName(this.name); 
  }
}

function Base_setWindowName (wndName)
{
  this.windowName = wndName;
}

function Base_setHeight(ht)
{
  if(ht!=null) {
    if(isNaN(ht)) {
      alert (ht + " Not a Number");
      return;
    }
    //Height should not be negative;
    if (ht < 0) {
      ht = 0;
    }
    var oldht = this.height;
    this.height = ht;
    if (this.border) {
      this.height = ht - this.borderWidth*2;
      this.border.setHeight (ht);
    }
    if(this.launched) {
      var clipRgn;
      if(!this.noclip)
        clipRgn = "rect(0px "+this.width+"px "+this.height+"px 0px)";
      
      HTMLHelper.setDimensions(this.getHTMLObj(),null,null,null,ht,clipRgn);
      
      if(oldht != ht) {
        if(this.prefSystem)
          this.prefSystem.notify(this,'height',ht);
        if(this.onresizecall)
          this.onresizecall(this);
      }
    }
    if (this.moreSetHeight)
      this.moreSetHeight (this.height);
  }
}

function Base_setWidth(wd)
{
  if (wd!=null) {
    if(isNaN(wd)) {
      alert (wd + " Not a Number");
      return;
    }
    //Width should not be negative;
    if (wd < 0) {
      wd = 0;
    }
    var oldwd = this.width;
    this.width = wd;
    if (this.border) {
      this.width = wd - this.borderWidth * 2;
      this.border.setWidth (wd);
    } 
    if(this.launched) {
      var clipRgn;
      if(!this.noclip)
        clipRgn = "rect(0px "+this.width+"px "+this.height+"px 0px)";
      
      HTMLHelper.setDimensions(this.getHTMLObj(),null,null,wd,null,clipRgn);
      
      if(oldwd !=wd) {
        if(this.onresizecall) 
          this.onresizecall(this);
        if(this.prefSystem)
          this.prefSystem.notify(this,'width',wd);
      }
    }
    if (this.moreSetWidth)
      this.moreSetWidth (this.width);
  }
}

function Base_setDimensions(lt, tp, wd, ht)
{
  if(lt!=null)
    this.setLeft(lt);
  if(tp!=null)
    this.setTop(tp);
  if(wd!=null)
    this.setWidth(wd);
  if(ht!=null)
    this.setHeight(ht);
}

function Base_createBorder (borderWidth, borderColor)
{
  if (this.launched) return;
  this.bBorder = true;
  this.borderVisible = true;
  
  if (borderWidth != null)
    this.borderWidth = borderWidth;
  else
    this.borderWidth = 1;
  
  if (borderColor != null)
    this.borderColor = borderColor;
  else if (this.color)
    this.borderColor = this.color
  else
    this.borderColor = 'black';
  
  if (ns4) {
    this.border = new Base ();
    this.border.setName ('BDR-' + this.name);
    this.border.setBackgroundColor (this.borderColor);
    if (! this.visibility) {
      this.border.visibility = false;
    }
    this.visibility = true;
    if(this.border)
      this.bCaptionWrap = false;
    
    //resize this controls diemensions and properties;
    this.setDimensions (this.left, this.top, this.width, this.height);
    this.setParentObject (this.parentObj);
    this.setReference (this.reference);
  }
}

function Base_setBorderVisible (bVis)
{
  if (!this.bBorder) 
    return;
  this.borderVisible = bVis;
  return;
  
  if (ns4) {
    if (bVis)
      this.border.show ();
    else
      this.border.hide ();
    return;
  } else if (ie4) {
    if (this.launched) {
      this.getHTMLObj ();
      if (bVis) {
        this.htmlObj.borderWidth = this.borderWidth;
        this.htmlObj.borderColor = this.borderColor;
        this.htmlObj.borderStyle = 'solid';
      } else {
        this.borderWidth = 0;
        this.htmlObj.borderWidth = 0;
      }
    }
  }
}

function Base_setBorderWidth (wd)
{
  this.borderWidth = wd;
  if (this.launched && ie4) {
    this.getHTMLObj ();
    this.htmlObj.borderWidth = wd;
  }
}

function Base_setBorderColor (col)
{
  this.borderColor = col;
  if (this.launched && ie4) {
    this.getHTMLObj ();
    this.htmlObj.borderColor = col;
  }
}

function Base_setParentObject(pObj)
{
  this.parentObj = pObj;
  if (this.border) {
    this.border.setParentObject (pObj);
    this.parentObj = this.border;
  }
}

function Base_setBackgroundColor(c)
{
  if(c!=null) {
    this.backgroundColor = c;
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),c);
    
    if (this.moreSetBackgroundColor) {
      this.moreSetBackgroundColor (c);
    }
  }
}

function Base_setColor (c)
{
  this.color = c;
  if (this.launched) {
    if (ie4)
      HTMLHelper.setForegroundColor(this.getHTMLObj(), c);
    else if (ns4)
      return;
    
    if (this.moreSetColor) {
      this.moreSetColor(c);
    }
  }
}

function Base_setValue(v)
{
  this.value = v;
  
  var sBuffer =  '<SPAN ID="SPAN-'+this.objId+'"';
  if(this.spanClass)
    sBuffer += ' CLASS="'+this.spanClass+'"';
  sBuffer += ' >' + this.value + '</SPAN>';
  
  if (this.launched) {
    if (this.getHTMLObj())
      if(ns4){
        this.block.document.open();
        this.block.document.write(sBuffer);
        this.block.document.close();
      } else if(ie4) this.block.innerHTML = sBuffer;
  }
}

function Base_getValue()
{
  return this.value;
}

function Base_setZIndex (index)
{
  this.zIndex = index;
  if (this.border)
    this.border.setZIndex (index);
  
  if (this.launched)
    this.getHTMLObj ().zIndex = index;
}

function Base_getZIndex ()
{
  return (this.zIndex);
}

function Base_getBackgroundColor()
{
  return this.backgroundColor;
}

function Base_getColor (c)
{
  return this.color;
}

function Base_getName()
{
  return this.name;
}

function Base_getId()
{
  return this.objId;
}

function Base_getReference()
{
  if (this.border) {
    return (this.border.reference);
  } else
    return (this.reference);
}

function Base_getLeft(bAbs)
{
  var lt=0;
  if (this.border) {
    if(bAbs && this.border.parentObj) {
      lt = this.border.parentObj.getLeft(true);
      return (this.border.left + lt);
    } else
      return (this.border.left);
  } else if(bAbs && this.parentObj) 
    lt = this.parentObj.getLeft(true);
  
  return(this.left+lt);
}

function Base_getTop(bAbs)
{
  var tp = 0;
  if (this.border) {
    if(bAbs && this.border.parentObj) {
      tp = this.border.parentObj.getTop(true);
      return (this.border.top + tp);
    } else
      return (this.border.top);
  } else if(bAbs && this.parentObj) 
    tp = this.parentObj.getTop(true);
  
  return(this.top+tp);		
}

function Base_getHeight()
{
  if (this.border)
    return (this.border.height);
  else
    return (this.height);
}

function Base_getWidth()
{
  if (this.border)
    return (this.border.width);
  else
    return (this.width);
}

function Base_getParentObject()
{
  if (this.border)
    return (this.border.parentObj);
  else
    return this.parentObj;
}

function Base_getHTMLObj()
{
  if(this.htmlObj == null) 
    this.connect();

  return this.htmlObj;
}

function Base_generateCSS()
{
  var CSSStr = "";

  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";

  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";

  if(this.height != null)
    CSSStr += "height:" + this.height + "; ";
      
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height)
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";
  
  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";
  
  //write border style attribute only IE browser;
  if (ie4 && this.bBorder) {
    CSSStr += "border-style:solid; ";
    if (this.borderWidth)
      CSSStr += "border-width:" + this.borderWidth + "; ";
    else
      CSSStr += "border-width:1; ";
    
    if (this.borderColor)
      CSSStr += "border-color:" + this.borderColor + "; ";
    else
      CSSStr += "border-color:black; ";
  }
  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  
  CSSStr += "}";
  
  if (ns4 && this.bBorder) {
    CSSStr += "\n" + this.border.generateCSS ();
  }  
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();
  
  return(CSSStr);
}

function Base_render()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if ( this.alignment != null )
    sBuffer += ' ALIGN="' +this.alignment+ '"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
  } else if (this.bEnableKeyPressEvt) {
    sBuffer += ' onkeypress="javascript:doOnKeyPress(null,\''+ this.self + '\')"';    
  }
  sBuffer +='>';

  if(this.innerRender)
    sBuffer += this.innerRender();
  else if (this.value)
    sBuffer += this.value;

  sBuffer += '</DIV>';
  
  if (this.border)
    sBuffer += '\n</DIV>';
  
  return (sBuffer);
}

function Base_runtimeRender(target, parentObj, em)
{
  if(this.parentObj) {
    if(this.parentObj.block==null)
      this.parentObj.connect();
    var obj = this.parentObj.block;
  }
  else
    var obj = null;
  
  var lyr = HTMLHelper.createLayer(this.name, obj, target);
  
  if(ns4)
    this.objId = lyr.id;

  this.block = lyr;
  
  if(ns4)
    this.htmlObj = lyr;
  else if(ie4)
    this.htmlObj = this.block.style;
  
  this.launched = true;
  this.setDimensions(this.left, this.top, this.width, this.height);
  this.setBackgroundColor(this.backgroundColor);
  
  if(this.visibility == false)
    this.hide();	
  else if(this.parentObj)
    this.htmlObj.visibility = 'inherit';
  else
    this.show();

  var sBuffer;
  if(this.innerRender) {
    sBuffer = this.innerRender(true);
    if(sBuffer != null)
      HTMLHelper.writeToLayer(this.block, sBuffer);
  }
  if (em) {
    this.launch (em);
  } else if(self.EventManager) {
    //If this control is placed in a different window, then get the event;
    //manager associated to that window;
    if ((this.windowName != null) && (this.windowName != 'undefined'))
      this.launch (self.EventManager(wndName));
    else
      this.launch (self.EventManager ());
  } else
    alert('EventManager.js not included');
}

function Base_launch(em)
{
  if(em) {
    this.eventManager = em;
    em.add(this);

    if (this.tabindex)
      em.addTabComp (this);
  }
  this.launched = true;
  
  if (this.border)
    this.border.launch (em);
  
  if(this.moreLaunch)
    this.moreLaunch(em);
  
  if(this.onLaunchCall)
    this.onLaunchCall();
}

function Base_write(strHtml)
{
  if (!this.launched) 
    return;
  if (this.block == null) 
    this.connect();
  
  HTMLHelper.writeToLayer(this.block, strHtml);
}

function Base_doMouseOver(e)
{
  if(this.tip && self.Tooltip)
    Tooltip.show(this.tip);
  
  return this.notifyListeners('mouseoverCallback',e, this);
}

function Base_doMouseOut(e)
{
  return this.notifyListeners('mouseoutCallback', e, this);
}

function Base_doMouseDown(e)
{
  return this.notifyListeners('mousedownCallback', e, this);
}

function Base_doMouseUp(e)
{
  if(this.tabManager)
    this.tabManager.gotFocus(this);

  return this.notifyListeners('mouseupCallback', e, this);
}

function Base_doMouseMove(e)
{
  return this.notifyListeners('mousemoveCallback', e, this);
}

function Base_doDoubleClick (e)
{
  this.notifyListeners ('doubleclickCallback', e, this);
  return (this.notifyListeners('ondoubleclick', e, this));
}

function Base_doOnFocus (e)
{
  return this.notifyListeners ('onFocusCallback', e , this);
}

function Base_doOnBlur (e)
{
  return this.notifyListeners ('onBlurCallback', e, this);
}

function Base_doOnKeyDown (e)
{
  return this.notifyListeners ('onKeyDownCallback', e, this);
}

function Base_doOnKeyPress (e)
{
  return this.notifyListeners ('onKeyPressCallback', e, this);
}

function Base_doOnKeyUp (e)
{
  return this.notifyListeners ('onKeyUpCallback', e, this);
}

function Base_moveTo(x, y)
{
  this.setLeft(x);
  this.setTop(y);
}

function Base_moveBy(x, y)
{
  var newLeft= this.left;
  var newTop = this.top;
  
  if(x) 
    newLeft +=x;
  if(y) 
    newTop +=y;

  this.moveTo(newLeft, newTop);
}

function Base_show() 
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function Base_hide() 
{
  this.visibility = false;
  if (this.border)
    this.border.visibility = false;
  
  if (this.launched) {
    if (this.border) {
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.visibility);
      return;
    }
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function Base_isVisible()
{
  if(this.visibility == null) 
    return true;
  return (this.visibility);
}

function Base_clipTo(t,r,b,l) 
{
  if (null == t) 
    t = this.clipValues('t');
  if (null == r) 
    r = this.clipValues('r');
  if (null == b) 
    b = this.clipValues('b');
  if (null == l) 
    l = this.clipValues('l');
  if (ns4) {
    this.getHTMLObj().clip.top = t;
    this.getHTMLObj().clip.right = r;
    this.getHTMLObj().clip.bottom = b;
    this.getHTMLObj().clip.left = l;
  }
  else if (ie4) 
    this.getHTMLObj().clip = "rect("+t+"px "+r+"px "+b+"px "+l+"px)";
  this.clipMode = true;
}

function Base_clipBy(t,r,b,l) 
{
  this.clipTo(this.clipValues('t')+t,this.clipValues('r')+r,this.clipValues('b')+b,this.clipValues('l')+l)
}

function Base_clipValues(which) {
  var retVal;
  if (ie4)
    var clipv = this.getHTMLObj().clip.split("rect(")[1].split(")")[0].split("px");
  switch(which)
    {
    case "t": 
      retVal =  (ns4)? this.getHTMLObj().clip.top : Number(clipv[0]);
      break;
    case "r":
      retVal = (ns4)? this.getHTMLObj().clip.right : Number(clipv[1]);
      break;
    case "b":
      retVal = (ns4)? this.getHTMLObj().clip.bottom : Number(clipv[2]);
      break;
    case "l": 
      retVal = (ns4)? this.getHTMLObj().clip.left : Number(clipv[3]);
      break;
    }
  return retVal;
}

function Base_slideTo(endx,endy,inc,speed,fn) 
{
  var distx =0;
  var disty =0;
  if(endx != null) distx = endx-this.left;
  if(endy != null) disty = endy-this.top;
  Base.slideStart(this,endx,endy,distx,disty,inc,speed,fn);
}

function Base_slideBy(distx,disty,inc,speed,fn) 
{
  var endx = this.left;
  var endy = this.top;
  if(distx!=null) endx += distx;
  if(disty!=null) endy += disty;
  Base.slideStart(this,endx,endy,distx,disty,inc,speed,fn);
}

function Base_slideStop()
{
  this.slideActive = false;
  if(this.slideTimeoutId) {
    clearTimeout(this.slideTimeoutId);
    delete this.slideTimeoutId;
  }
}

//Hiliting the basewidget with a color
function Base_hilite(hiliteColor)
{
  if (!this.launched) 
    return;
  if (!hiliteColor) 
    hiliteColor = 'yellow';
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), hiliteColor);
  if(!this.backgroundColor)
    this.backgroundColor ='transparent';
}

//Dehiliting the basewidget with a color
function Base_unhilite()
{
  if (!this.launched) 
    return;
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), this.backgroundColor);
}

function Base_isInside(x, y, t, r, b, l) 
{
  var tp,right,bottom,left;

  tp = this.top;
  right = this.left+this.width;
  bottom = this.top+this.height;
  left = this.left;

  if (r !=null) 
    right = r;
  if (b !=null) 
    bottom = b;
  if (t !=null) 
    tp =t;
  if (l !=null) 
    left =l;

  if (x > left) 
    if(x < right) 
      if(y > tp) 
        if(y < bottom) 
          return true;
  return false;
}

function Base_positionTo(obj, side, gap)
{
  if (gap == null) 
    gap = 0;

  switch(side)
    {
    case 2:
      this.setLeft(obj.left + obj.width+gap);
      if(this.top==null) 
        this.setTop(obj.top);
      break;
    case 4:
      this.setLeft(obj.left - this.width-gap);
      if(this.top==null ) 
        this.setTop(obj.top);
      break;
    case 1:
      this.setTop(obj.top - this.height-gap);
      if(this.left==null) 
        this.setLeft(obj.left);
      break;
    case 3:
      this.setTop(obj.top + obj.height+gap);
      if(this.left==null) 
        this.setLeft(obj.left);
      break;
    }
}

function Base_connect(block)
{
  var ref;
  if(block) {
    this.htmlObj = this.block = block;
    if(ie4)
      this.htmlObj = block.style;
    return;
  }
  if(this.parentObj) {
    if(!this.parentObj.block) 
      this.parentObj.connect();
    
    ref = 'this.parentObj.block';
  }
  else 
    ref = this.reference;
  if (ns4) {
    this.block = eval(ref + ".layers['" + this.objId + "']");
    if(this.block == null)
      alert ("Connect:Invalid Reference");
    else
      this.htmlObj = this.block;
  } else if (ie4) {
    this.block = eval(ref + ".all['"+ this.objId + "']");
    if(this.block == null)
      alert ("Connect:Invalid Reference");
    else
      this.htmlObj = this.block.style;
  }
}

function Base_copy(obj)
{
  var nubase = new Object();

  for(prop in obj) {
    eval("nubase."+prop+" = obj."+prop);
  }
  nubase.name = nubase.name + 'c'+obj.numCopy;
  nubase.name = nubase.name + 'c'+obj.numCopy;
  obj.numCopy++;
  return nubase;
}

//private method to get style object from the styles collection
function Base_getStyleObject (styleName, bCreate)
{
  var styl;
  //Try to get the syle object from the styles collection for the specified;
  //style name. If not found and bCreate is true then create a new one;
  if (styleName) {
    styl = Styles(this.windowName).getStyle (styleName);
    if (styl != null)
      return (styl);
    else if (bCreate) {
      styl = Styles(this.windowName).createStyle (styleName);
      return (styl);
    }
  }
  if (! this.divClass) {
    styl = Styles(this.windowName).getStyle (this.name + 'Style');
    if (styl == null) 
      styl = Styles(this.windowName).createStyle (this.name + 'Style');
    
    this.divClass = this.name + '_Style';
  } else {
    styl = Styles(this.windowName).getStyle (this.divClass);
    if ((styl == null) && bCreate)
      styl = Styles(this.windowName).createStyle (this.divClass);
  }
  
  return styl;
}

function Base_setFont (font)
{
  if (this.launched)
    return;
  
  this.spanClass = this.name+'Style';
  var style = this.getStyleObject (this.spanClass, true);
  this.font = font;
  style.setAttribute ('font-family', font.family);
  style.setAttribute ('font-size', font.size);
  style.setAttribute ('font-style', font.style);
  style.setAttribute ('font-weight', font.weight);
}

function Base_setAlignment (align)
{
  if (this.launched)
    return;
  
  this.spanClass = this.name+'Style';
  var style = this.getStyleObject (this.spanClass, true);
  this.alignment = align; 
  style.setAttribute ('text-align', align);
}

function Base_getFont ()
{
  return (this.font);
}

function Base_setPadding (pdgLt, pdgTp, pdgRt, pdgBot)
{
  if (this.launched)
    return;
  this.divClass = this.name + 'DivStyle';
  var style = this.getStyleObject(this.divClass, true);
  if (pdgLt != null)
    style.setAttribute('padding-left', pdgLt);
  if (pdgTp != null)
    style.setAttribute('padding-top', pdgTp);
  if (pdgRt != null)
    style.setAttribute('padding-right', pdgRt);
  if (pdgBot != null)
    style.setAttribute('padding-bottom', pdgBto);
}

function Base_setStyleValue (styleAttr, val)
{
  if (this.launched)
    return;
  var style = this.getStyleObject ();
  if (style == null) {
    alert ("Style object not found");
    return;
  }
  style.setAttribute (styleAttr, val)
}

function Base_getStyleValue (styleAttr)
{
  if (!this.divClass) 
    return null;
  
  var style = this.getStyleObject ();
  if (style == null) {
    alert ("Style object not found");
    return;
  }
  return (style.getAttribute (styleAttr));
}

function Base_set3DLook (bVal)
{
  this._3DLook = bVal;
}

function Base_is3DLookSet ()
{
  return this._3DLook;
}

function Base_setFlatLook (bVal)
{
  this._flatLook = bVal;
}

function Base_isFlatLookSet ()
{
  return this._flatLook;
}

function Base_setTabindex (tabindex)
{
  this.tabindex = tabindex;
}

function Base_getTabindex()
{
  return this.tabindex;
}

function Base_enableKeyPressEvents()
{
  eval(this.self +'=this');
  this.bEnableKeyPressEvt = true;
}

function Base_enableKeyBoardEvents()
{
  eval(this.self +'=this');
  this.bEnableKeyBoardEvt = true;
}

function Base_setFocus()
{
  if (this.launched) {
    this.getHTMLObj();
    this.block.focus();
  }
}

function Base_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
      return (this.name);
    }
  }
  return (null);
}

function Base_destroy ()
{
  if (! this.bDestroyed) {
    this.htmlObj = null;
    delete this.htmlObj;
    
    this.block = null;
    delete this.block;
    
    this.eventManager = null;
    delete this.eventManger;
    
    this.clearQueue ();
    
    if (this.moreDestroy) {
      this.moreDestroy ();
    }
    this.bDestroyed = true;
  }
}

//Extend from Listener class
HTMLHelper.importPrototypes(Base,Listener);

//Class Method
Base.baseCount 	= 0; 
Base.slideStart	= Base_slideStart; 
Base.slide	= Base_slide;

//Instance Methods
Base.prototype.setReference 	= Base_setReference;
Base.prototype.setDimensions 	= Base_setDimensions;
Base.prototype.setLeft 		= Base_setLeft;
Base.prototype.setTop 		= Base_setTop;
Base.prototype.setWidth 	= Base_setWidth;
Base.prototype.setHeight 	= Base_setHeight;
Base.prototype.setParentObject 	= Base_setParentObject;
Base.prototype.setName 		= Base_setName;
Base.prototype.setWindowName = Base_setWindowName;
Base.prototype.getName 		= Base_getName;
Base.prototype.getId 		= Base_getId;
Base.prototype.getReference 	= Base_getReference;
Base.prototype.getLeft 		= Base_getLeft;
Base.prototype.getTop 		= Base_getTop;
Base.prototype.getWidth 	= Base_getWidth;
Base.prototype.getHeight	= Base_getHeight;
Base.prototype.getParentObject 	= Base_getParentObject;
Base.prototype.render 		= Base_render;
Base.prototype.runtimeRender 	= Base_runtimeRender;
Base.prototype.launch 		= Base_launch;
Base.prototype.doMouseOver 	= Base_doMouseOver;
Base.prototype.doMouseOut 	= Base_doMouseOut;
Base.prototype.doMouseDown	= Base_doMouseDown;
Base.prototype.doMouseUp 	= Base_doMouseUp;
Base.prototype.doMouseMove 	= Base_doMouseMove;
Base.prototype.doDoubleClick 	= Base_doDoubleClick;
Base.prototype.doOnFocus 	= Base_doOnFocus;
Base.prototype.doOnBlur 	= Base_doOnBlur;
Base.prototype.doOnKeyDown 	= Base_doOnKeyDown;
Base.prototype.doOnKeyPress 	= Base_doOnKeyPress;
Base.prototype.doOnKeyUp 	= Base_doOnKeyUp;
Base.prototype.generateCSS 	= Base_generateCSS;
Base.prototype.setBackgroundColor = Base_setBackgroundColor;
Base.prototype.getBackgroundColor = Base_getBackgroundColor;
Base.prototype.moveTo 		= Base_moveTo;
Base.prototype.moveBy 		= Base_moveBy;
Base.prototype.slideTo 		= Base_slideTo;
Base.prototype.slideBy 		= Base_slideBy;
Base.prototype.slideStop 	= Base_slideStop;
Base.prototype.clipTo 		= Base_clipTo;
Base.prototype.clipBy 		= Base_clipBy;
Base.prototype.clipValues	= Base_clipValues;
Base.prototype.show 		= Base_show;
Base.prototype.hide 		= Base_hide;
Base.prototype.hilite 		= Base_hilite;
Base.prototype.unhilite 	= Base_unhilite;
Base.prototype.isInside 	= Base_isInside;
Base.prototype.isVisible 	= Base_isVisible;
Base.prototype.positionTo 	= Base_positionTo;
Base.prototype.connect 		= Base_connect;
Base.prototype.setValue 	= Base_setValue;
Base.prototype.getValue 	= Base_getValue;
Base.prototype.getHTMLObj 	= Base_getHTMLObj;
Base.prototype.write 		= Base_write;
Base.prototype.setColor 	= Base_setColor;
Base.prototype.setNoBorder 	= Base_setNoBorder;
Base.prototype.getColor 	= Base_getColor;
Base.prototype.setZIndex 	= Base_setZIndex;
Base.prototype.getZIndex 	= Base_getZIndex;
Base.prototype.setFont 		= Base_setFont;
Base.prototype.setAlignment     = Base_setAlignment;
Base.prototype.getFont 		= Base_getFont;
Base.prototype.setPadding       = Base_setPadding;
Base.prototype.setStyleValue	= Base_setStyleValue;
Base.prototype.getStyleValue	= Base_getStyleValue;
Base.prototype.updateDone = Base_updateDone;
Base.prototype.hideFeature = Base_hideFeature;
Base.prototype.createBorder = Base_createBorder;
Base.prototype.setBorderVisible	= Base_setBorderVisible;
Base.prototype.setBorderWidth = Base_setBorderWidth;
Base.prototype.setBorderColor = Base_setBorderColor;
Base.prototype.set3DLook = Base_set3DLook;
Base.prototype.is3DLookSet = Base_is3DLookSet;
Base.prototype.setFlatLook = Base_setFlatLook;
Base.prototype.isFlatLookSet = Base_isFlatLookSet;
Base.prototype.setTabindex = Base_setTabindex;
Base.prototype.getTabindex = Base_getTabindex;
Base.prototype.enableKeyPressEvents = Base_enableKeyPressEvents;
Base.prototype.enableKeyBoardEvents = Base_enableKeyBoardEvents;

Base.prototype.hitTest = Base_hitTest;
Base.prototype.destroy = Base_destroy;
Base.prototype.readOnly = Base_readOnly;
Base.prototype.setFocus = Base_setFocus;

//Private Method
Base.prototype.getStyleObject	= Base_getStyleObject;

//actual sliding functions
function Base_slide(obj,dx,dy,endx,endy,num,i,speed,fn) 
{
  if(!obj.slideActive)
    return;
  if(i++ < num) 
    {
      obj.moveBy(dx,dy);


      var res = obj.notifyListeners('onSlide',speed);
      if(res>1)
        speed = res;
      if (obj.slideActive) 
        {
          eval(obj.self +'= obj');
          obj.slideTimeoutId = setTimeout("Base_slide("+obj.self+","+dx+","+dy+","+endx+","+endy+","+num+","+i+","+speed+",\""+fn+"\")",speed);

        }
    }
  else 
    {	
      obj.moveTo(endx,endy);
      obj.slideActive = false;
      obj.slideProgress = false;
      obj.notifyListeners('onSlide',speed);
      if(obj.slideTimeoutId)
        delete obj.slideTimeoutId;
      if(fn) 
        eval(fn);
      obj.notifyListeners('onSlideEnd',speed);
    }
}

//start sliding operation
function Base_slideStart(obj, endx,endy,distx,disty,inc,speed,fn) 
{
  obj.slideStop();
  if (!inc) 
    inc = 10;
  if (!speed) 
    speed = 20;
  var num = Math.sqrt(Math.pow(distx,2) + Math.pow(disty,2))/inc;
  if (num==0) 
    return;
  var dx =distx/num;
  var dy =disty/num;

  if (!fn) 
    fn = null;
  obj.slideActive = true;

  Base_slide(obj, dx,dy,endx,endy,num,1,speed,fn);
}


/*  czUtils.js 115.14 2001/05/04 13:25:21 cjeyapra ship $ */

function Utils ()
{

}

function Utils_clearCollection (coll)
{
  var len = 0;
  len = coll.length;
  if (len) {
    //coll is index based;
    for (var i=0; i<len; i++) {
      coll[i] = null;
      delete coll[i];
    }
    coll.length = 0;
  } else {
    //coll may be associative;
    for (var id in coll) {
      coll[id] = null;
      delete coll[id];
    }
  }
}

/**
 * Returns the actual image size as an array [width, height].
 * For wrong imgSrc or if no imgTestLayer is in czSrc window then it will
 * return null
 */

function Utils_getImageSize (imgSrc)
{
  var width, height, strHtml ='';
  var doc, block;
  if (ie4) {
    if (! document.all["imgTestLayer"]) 
      return null;
  } else if (ns4) {
    if (! document.layers["imgTestLayer"])
      return null;
  }
  strHtml += '<IMG src="' + imgSrc + '" BORDER=0></IMG>';
  if (ie4) {
    doc = document.all["imgTestLayer"];
    doc.innerHTML = strHtml;
    width = document.all["imgTestLayer"].scrollWidth;
    height = document.all["imgTestLayer"].scrollHeight;
  } else if (ns4) {
    doc = document.layers["imgTestLayer"].document;
    block = document.layers["imgTestLayer"];
    doc.open ();
    doc.writeln (strHtml);
    doc.close ();
    //NS adds a 4 pixel padding when reporting the values
    width = block.clip.width - 4;
    height = block.clip.height - 4;
  }    
  return (new Array (width, height));
}

// Class Methods
Utils.clearCollection = Utils_clearCollection;
Utils.getImageSize = Utils_getImageSize;
 
/**
* Class Font
*/

function Font (family, size, style, weight)
{
  //list of font names;
  if (family)
    this.family = family;
  else	
    this.family = null;
  //Font size;
  if (size)
    this.size = size;
  else
    this.size = null;
  //Font style;
  if (style)
    this.style = style;
  else
    this.style = null;
  //Font weight;
  if (weight)
    this.weight = weight;
  else
    this.weight = null;
  
  this.bPropsChanged = true;
}

function Font_setFamily (family)
{
  this.family = family
    this.bPropsChanged = true;
}

function Font_getFamily ()
{
  return (this.family)
}

function Font_setSize (size)
{
  this.size = size;
  this.bPropsChanged = true;
}

function Font_getSize ()
{
  return (this.size);
}

function Font_setStyle (style)
{
  this.style = style;
  this.bPropsChanged = true;
}

function Font_getStyle ()
{
  return (this.style);
}

function Font_setWeight (weight)
{
  this.weight = weight;
  this.bPropsChanged = true;
}

function Font_getWeight ()
{
  return (this.weight);
}

/**
*  @param - seed (optional) characters
*/
function Font_getCharWidth (seed, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (seed);
    return (this.charWidth);
  } else
    return (this.charWidth);
}

/**
*  @param - seed (optional) characters
*/
function Font_getCharHeight (seed, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (seed);
    return (this.charHeight);
  } else
    return (this.charHeight);
}

function Font_getTextWidth (text, bOverride)
{
  if (this.bPropsChanged || bOverride) {
    this.determineFontProperties (text);
    return (this.textWidth);
  } else
    return (this.textWidth);
}

function Font_getTextHeight (text, bOverride)
{
  return (this.getCharHeight (text, bOverride));
}

/**
*  @param - seed (optional) characters
*/
function Font_determineFontProperties (seed)
{
  var width, height, strHtml ='';
  var doc, block;
  if (ie4) {
    if (! document.all.fontTestLayer) return;
  } else if (ns4) {
    if (! document.layers["fontTestLayer"])
      return;
  }
  if (ie4) {
    strHtml += '<NOBR ID=fontTest STYLE="position:absolute; left:0; top:0; ';
    strHtml += 'font-family:' + this.family + '; font-size:' + this.size + '; ';
    strHtml += 'font-style:' + this.style + '; font-weight:' + this.weight + ';">';
  } else if (ns4) {
    strHtml += '<NOBR CLASS="fontTest">';
  }
  if (seed)
    strHtml += seed + '</NOBR>';
  else
    strHtml += 'aArRsStTwWzZ</NOBR>';
  if (ie4) {
    doc = document.all["fontTestLayer"];
    doc.innerHTML = strHtml;
    width = document.all["fontTest"].scrollWidth;
    height = document.all["fontTest"].scrollHeight;
  } else if (ns4) {
    doc = document.layers["fontTestLayer"].document;
    block = document.layers["fontTestLayer"];
    with (document.classes.fontTest.NOBR) {
      fontFamily = this.family;
      fontSize = this.size;
      fontStyle = this.style;
      fontWeight = this.weight;
    }
    doc.open ();
    doc.writeln (strHtml);
    doc.close ();
    width = block.clip.width;
    height = block.clip.height;
  }
  if (seed) {
    this.charWidth = Math.round (width/seed.length);
    this.textWidth = Math.round (width);
  } else
    this.charWidth = Math.round (width/12);
  this.charHeight = height;
  this.bPropsChanged = false;
}

Font.prototype.setFamily = Font_setFamily;
Font.prototype.getFamily = Font_getFamily;
Font.prototype.setSize = Font_setSize;
Font.prototype.getSize = Font_getSize;
Font.prototype.setStyle = Font_setStyle;
Font.prototype.getStyle = Font_getStyle;
Font.prototype.setWeight = Font_setWeight;
Font.prototype.getWeight = Font_getWeight;
Font.prototype.getCharWidth = Font_getCharWidth;
Font.prototype.getCharHeight = Font_getCharHeight;
Font.prototype.getTextWidth = Font_getTextWidth;
Font.prototype.getTextHeight = Font_getTextHeight;
Font.prototype.determineFontProperties = Font_determineFontProperties;

/**
*	Column Descriptor Class
*/

function ColumnDescriptor (name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring)
{
  this.name = name;
  // types: label, inputtext, icon;
  this.type = type;
  this.index = index;

  // pixel dimensions;
  this.defHeight = ht;
  this.defWidth = wd; 
  this.defVisible = true;
  this.spring = false;
  
  if (pdgLt != null) {
    this.pdgLeft = pdgLt;
  } else {
    this.pdgLeft = 5;
  }  
  if (pdgTp != null) {
    this.pdgTop = pdgTp;
  } else {
    this.pdgTop = 0;
  }
  if (valign) {
    this.valign = valign;
  } else {
    this.valign = 'center';
  }
  if (halign) {
    this.halign = halign;
  } else {
    this.halign = 'left';
  }
  if (defvis != null) {
    this.defVisible = defvis;
  }
  if (spring != null) {
    this.spring = spring;
  }
}

function ColumnDescriptor_setPrefWidth (wd)
{
  this.prefWidth = wd;
  this.availWidth = wd;
}

function ColumnDescriptor_getPrefWidth ()
{
  if (isNaN (this.prefWidth)) {
    return (this.defWidth);
  } else {
    return (this.prefWidth);
  }
}

function ColumnDescriptor_setPrefHeight (ht)
{
  this.prefHeight = ht;
  this.availHeight = ht;
}

function ColumnDescriptor_getPrefHeight ()
{
  return (this.prefHeight);
}

function ColumnDescriptor_setAvailWidth (wd)
{
  this.availWidth = wd;
}

function ColumnDescriptor_getAvailWidth ()
{
  if (isNaN (this.availWidth)) {
    return (this.getPrefWidth());
  } else {
    return (this.availWidth);
  }
}

function ColumnDescriptor_setAvailHeight (ht)
{
  this.availHeight = ht;
}

function ColumnDescriptor_getAvailHeight ()
{
  return (this.availHeight);
}

function ColumnDescriptor_setLeft (lt)
{
  this.left = lt;
}

function ColumnDescriptor_getLeft ()
{
  return (this.left);
}

ColumnDescriptor.prototype.setPrefWidth = ColumnDescriptor_setPrefWidth;
ColumnDescriptor.prototype.getPrefWidth = ColumnDescriptor_getPrefWidth;
ColumnDescriptor.prototype.setPrefHeight = ColumnDescriptor_setPrefHeight;
ColumnDescriptor.prototype.getPrefHeight = ColumnDescriptor_getPrefHeight;
ColumnDescriptor.prototype.setAvailWidth = ColumnDescriptor_setAvailWidth;
ColumnDescriptor.prototype.getAvailWidth = ColumnDescriptor_getAvailWidth;
ColumnDescriptor.prototype.setAvailHeight = ColumnDescriptor_setAvailHeight;
ColumnDescriptor.prototype.getAvailHeight = ColumnDescriptor_getAvailHeight;
ColumnDescriptor.prototype.setLeft = ColumnDescriptor_setLeft;
ColumnDescriptor.prototype.getLeft = ColumnDescriptor_getLeft;


function ColumnDataDescriptor (name, value, wd)
{
  this.name = name;
  this.value = '';
  this.width = 0; // data width (generally,  in chars);
  
  if (value) {
    this.value = value;
  }
  if (wd) {
    this.width = wd; 
  }
}


/*  czContr.js 115.10 2000/12/05 18:15:53 cjeyapra ship $ */
 
function Container()
{
  this.parentConstructor = Base;
  this.parentConstructor();

  this.children = null;
}

function Container_add() 
{
  if(arguments) {
    if (this.children == null) 
      this.children = new Array();
    
    for (var i=0; i<arguments.length; i++) {
      if(arguments[i]) {
        this.children[arguments[i].name] = arguments[i];
        if(arguments[i].setParentObject) 
          arguments[i].setParentObject(this);
        else
          arguments[i].parentObj = this;
        
        arguments[i].setReference(this.reference + (ns4?'.layers["'+this.objId+'"].document':''));
        if(this.launched)
          arguments[i].runtimeRender(null, this, this.eventManager);
      }	
    }
  }
}

function Container_contains(obj) 
{
  if(this.children) {
    for(var childName in this.children)
      if(this.children[childName]== obj)
        return true;
  }
  return false;
}

function Container_getComponent(name) 
{
  if(this.children)
    if(this.children[name])
      return this.children[name];
    else
      return null;
}

function Container_setReference(ref)
{
  if(ref) {
    this.reference = ref;
    if (this.children) {
      var sChildRef = this.reference + (ns4?'.layers["'+this.objId+'"].document':'');
      for(var i in this.children) 
        this.children[i].setReference(sChildRef); 
    }
  }
}

function Container_innerRender(bRuntime)
{
  var sBuffer;
  if(! bRuntime) {
    sBuffer ="";
    if(this.content)
      sBuffer = this.content;
    if(this.children) {
      for (var i in this.children) {
        this.children[i].parentType = this.type;
        sBuffer += '\n' + this.children[i].render();
      }
    }
  } else {
    if(this.content)
      HTMLHelper.writeToLayer(this.block,this.content);

    if ((! this.children) && this.buildControl) {
      this.buildControl ();
      if (this.launched) return;
    }
    if(this.children) {
      for (var i in this.children)
        this.children[i].runtimeRender(null, this, this.eventManager);
    }
  }
  return sBuffer;
}

function Container_moreLaunch(em) 
{
  if(this.children) {
    for (var childName in this.children) 
      this.children[childName].launch(em);
  }
}

function Container_moreCSS() 
{
  var CSSStr="";
  if ((! this.children) && this.buildControl) {
    this.buildControl ();
  }
  if(this.children) {
    for (var childName in this.children) {
      CSSStr += this.children[childName].generateCSS() + '\n'; 
    }
  }
  return(CSSStr);
}

function Container_cropToObjects(dir)
{
  var maxX=0;
  var maxY=0;
  var tempX;
  var tempY;

  if (this.children) {
    for(var i in this.children) {
      tempX = this.children[i].left+this.children[i].width;
      tempY = this.children[i].top+this.children[i].height;
      if (tempX > maxX) maxX = tempX;
      if (tempY > maxY) maxY = tempY;
    }
    if(dir == null) {
      if(maxX)
        this.setWidth(maxX);
      if(maxY)
        this.setHeight(maxY);
    } else if(dir == 'h') {
      if(maxX)
        this.setWidth(maxX);
    } else if(dir == 'v') {
      if(maxY)
        this.setHeight(maxY);
    }
  }
  if(this.launched) {
    if(this.block == null)
      this.connect();
    var wd = ns4?this.block.clip.width:this.block.scrollWidth;
    var ht = ns4?this.block.clip.height:this.block.scrollHeight;
    if(wd > this.width) 
      this.setWidth(wd);
    if(ht > this.height)
      this.setHeight(ht);
  }
}

function Container_moreDestroy ()
{
  if (this.children) {
    for (var id in this.children) {
      this.children[id].destroy ();
      
      this.children[id] = null;
      delete this.children[id];
    }
    this.children = null;
    delete this.children;
  }
}

HTMLHelper.importPrototypes(Container,Base);

Container.prototype.constructor 	= Container;
Container.prototype.innerRender 	= Container_innerRender;
Container.prototype.moreCSS 		= Container_moreCSS;
Container.prototype.moreLaunch 		= Container_moreLaunch;
Container.prototype.add 		= Container_add;
Container.prototype.addComponent 	= Container_add;
Container.prototype.getComponent 	= Container_getComponent;
Container.prototype.contains	 	= Container_contains;
Container.prototype.setReference 	= Container_setReference;
Container.prototype.cropToObjects 	= Container_cropToObjects;
Container.prototype.moreDestroy = Container_moreDestroy;

/*  czCmpMgr.js 115.7 2000/11/07 19:34:05 tabbott ship $ */
 
function ComponentManager(wndName)
{
  this.compHash = new Array();
  this.superCompHash = new Array();
  if (wndName) {
    if (Styles.instances[wndName]) {
      Styles.clearInstance (wndName);
    }
    this.stylesInstance = new Styles (wndName);
  } else
    this.stylesInstance = new Styles ();
}

function ComponentManager_addComponent()
{
  if(arguments) {
    for (var i=0; i<arguments.length; i++) {
      if(arguments[i]) {
        if(arguments[i].enclosure)
          this.superCompHash[arguments[i].getName()] = arguments[i];
        else
          this.compHash[arguments[i].getName()] = arguments[i];
      }
      if(self.documentLoaded)
        arguments[i].runtimeRender();
    }
  }
}

function ComponentManager_addStyle(styl)
{
  if(styl)
    this.stylesInstance.addStyle(styl);

}

function ComponentManager_getStyle(name)
{	
  return (this.stylesInstance.getStyle(name));
}

function ComponentManager_removeComponent(componentId)
{
  //empty
}

function ComponentManager_populateComponents(dataSrc)
{
  for (var i in this.compHash) {
    if(this.compHash[i].populate) 
      this.compHash[i].populate(dataSrc);
  }
}

function ComponentManager_generateCSS(docObj)
{
  if (! docObj) {
    docObj = document;
  }
  docObj.writeln ('<STYLE TYPE="text/css">');
  for(var j in this.superCompHash) {
    docObj.writeln (this.superCompHash[j].generateCSS(this));
  } 
  for (var i in this.compHash) {
    docObj.writeln (this.compHash[i].generateCSS(this));
  }
  docObj.writeln (this.stylesInstance.generateCSS(this));
  docObj.writeln ('</STYLE>');
}

function ComponentManager_renderComponents(docObj)
{
  if (! docObj) {
    docObj = document;
  }
  for(var j in this.superCompHash) {
    docObj.writeln (this.superCompHash[j].render(this));
  }
  for (var i in this.compHash) {
    docObj.writeln (this.compHash[i].render(this));
  }
}

function ComponentManager_launchComponents(em, bLateBind)
{
  for(var j in this.superCompHash)
    this.superCompHash[j].launch(em);
  for (var i in this.compHash)
    this.compHash[i].launch(em);
  
  if (bLateBind) return;
  em.init();
  self.documentLoaded = true;
}

function ComponentManager_destroy ()
{
  if (! this.bDestroyed) {
    var id;
    for (id in this.superCompHash) {
      this.superCompHash[id].destroy();
      
      this.superCompHash[id] = null;
      delete this.superCompHash[id];
    }
    this.superCompHash = null;
    delete this.superCompHash;

    for (id in this.compHash) {
      this.compHash[id].destroy ();
      
      this.compHash[id] = null;
      delete this.compHash[id];
    }
    this.compHash = null;
    delete this.compHash;
    
    this.bDestroyed = true;
  }
}

ComponentManager.prototype.addComponent 	= ComponentManager_addComponent;
ComponentManager.prototype.addStyle 		= ComponentManager_addStyle;
ComponentManager.prototype.getStyle 		= ComponentManager_getStyle;
ComponentManager.prototype.removeComponent 	= ComponentManager_removeComponent;
ComponentManager.prototype.populateComponents 	= ComponentManager_populateComponents;
ComponentManager.prototype.generateCSS 		= ComponentManager_generateCSS;
ComponentManager.prototype.renderComponents = ComponentManager_renderComponents;
ComponentManager.prototype.launchComponents = ComponentManager_launchComponents;
ComponentManager.prototype.destroy = ComponentManager_destroy;

/*  czEvtMgr.js 115.17 2001/06/19 19:58:24 cjeyapra ship $ */
 
function EventManager(name)
{
  if (name) {
    if (EventManager.instances[name]) 
      return(EventManager.instances[name]);
    else 
      EventManager.instances[name] = this;
  } else {
    if(self.singleEMInstance == null)
      self.singleEMInstance = this;
    else 
      return self.singleEMInstance;
  }
  this.locked = false;
  this.components = new Array();
  this.tabComps = new Array();
  
  this.onmouseup = _doc_onmouseup;
  this.onmousedown = _doc_onmousedown;
  this.onmousemove = _doc_onmousemove;
  this.onmouseover = _doc_onmouseover;
  this.onmouseout = _doc_onmouseout;
  this.ondblclick = _doc_ondblclick;
  this.onkeydown = _doc_onkeydown;
  this.onkeyup = _doc_onkeyup;
  this.onfocus = _window_onfocus;
  
  this.active = false;
  this.mouseover_id = null;
  this.msdown = false;
  
  EventManager.Count++;
  this.self = 'EventManager'+EventManager.Count;
}

function EventManager_init(doc)
{	
  if (doc)
    this.doc = doc;
  else
    this.doc = document;
  
  if (ns4) {
    //check for NS4.0; 
    this.doc.captureEvents(Event.MOUSEUP | Event.MOUSEDOWN | 
                           Event.MOUSEMOVE| Event.MOUSEOVER | Event.MOUSEOUT | 
                           Event.DBLCLICK | Event.KEYDOWN | Event.KEYUP);
  }
  eval(this.self + '=this');
  
  this.doc.onmouseup = new Function('event','return '+this.self+ '.onmouseup(event);');
  this.doc.onmousedown = new Function('event','return '+this.self+ '.onmousedown(event);');
  this.doc.onmousemove = new Function('event','return '+this.self+ '.onmousemove(event);');
  this.doc.onmouseover = new Function('event','return '+this.self+ '.onmouseover(event);');
  this.doc.onmouseout  = new Function('event','return '+this.self+ '.onmouseout(event);');
  this.doc.onkeydown = new Function('event','return '+this.self+ '.onkeydown(event);'); 
  this.doc.onkeyup = new Function('event','return '+this.self+ '.onkeyup(event);');
  this.doc.ondblclick  = new Function('event','return '+this.self+ '.ondblclick(event);');
}

function EventManager_lock()
{
  this.locked = true;
}

function EventManager_unLock()
{
  this.locked = false;
}

function EventManager_add()
{
  for (var i=0; i<arguments.length; i++) {
    if(arguments[i])
      this.components[arguments[i].getId()] = arguments[i];
  }
}

function EventManager_getElementID(e)
{
  var id = null;
  if (ns4) {
    //check for ns4;
    id = e.target.id;
    if(!id) if(e.target.name)
      id = e.target.name;
  }
  else if (ie4) //check for ie4
    id = e.srcElement.id;

  if(id) {
    var dashPos = id.indexOf('-');
    while(dashPos != -1) {
      id = id.slice(dashPos + 1);
      dashPos = id.indexOf('-');
    }
  }
  return id;
}

function EventManager_clearInstance (name)
{
  if (name) {
    if (EventManager.instances[name]) {
      EventManager.instances[name].destroy ();

      EventManager.instances[name] = null;
      delete EventManager.instances[name];
    }
  }
}


function EventManager_setCompMgr ( compMgr )
{
  this.compMgr = compMgr;
}

function EventManager_hitTest (e)
{
  var objId;
  if ( this.compMgr ) {
    for (objId in this.compMgr.compHash) {
      var objId = this.compMgr.compHash[objId].hitTest (e);
      if (objId) {
        return objId;
      }
    }
  }
  return null;
}

function EventManager_addTabComp(comp)
{
  if (comp.tabindex > 0)
    this.tabComps[comp.tabindex] = comp;
}

//move focus from comp to next 
function EventManager_focusForward(e, comp)
{
  var tabInd = comp.tabindex;
  if (this.tabComps[tabInd+1]) {
    this.tabComps[tabInd+1].setFocus();
    e.returnValue = false;
  }
}

//move focus backwards from comp to previous
function EventManager_focusBackward(e, comp)
{
  var tabInd = comp.tabindex;
  if (tabInd <= 1) {
    this.doc.parentWindow.focus();
    return;
  }
  if (this.tabComps[tabInd-1]) {
    this.tabComps[tabInd-1].setFocus();
    e.returnValue = false;
  } else
    this.doc.parentWindow.focus();
}

function EventManager_destroy ()
{
  if (! this.bDestroyed) {
    this.tabComps = null;
    delete this.tabComps;

    var id;
    for (id in this.components) {
      if (this.components[id].destroy) {
        this.components[id].destroy ();
      }
      this.components[id] = null;
      delete this.components[id];
    }
    this.components = null;
    delete this.components;
    
    this.compMgr = null;
    delete this.compMgr;
    
    this.bDestroyed = true;
  }
}

/***********************
//global event handlers
************************/
function _doc_onmouseup(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }

  var res =true;
  if(ie4) e = this.doc.parentWindow.event;

  res = this.notifyListeners('docMouseUp',e);
  if (res) {
    if (! this.mouseover_id) {
      if (ns4) {
        this.mouseover_id = this.hitTest (e); 
      } else if (ie4) {
        this.mouseover_id = EventManager.getElementID(e);
      }
    }
  }
  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])
      res = this.components[this.mouseover_id].doMouseUp(e);

  this.msdown = false;
  return res;
}		

function _doc_onmousedown(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }

  var res = true;
  if(ie4) e = this.doc.parentWindow.event;
  this.msdown = true;

  res= this.notifyListeners('docMouseDown',e);
  if (res) {
    if (! this.mouseover_id) {
      if (ns4) {
        this.mouseover_id = this.hitTest (e);
      } else if (ie4) {
        this.mouseover_id = EventManager.getElementID(e);
      }
    }
  }
  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])			
      res = this.components[this.mouseover_id].doMouseDown(e);

  return res;
}

function _doc_onmouseover(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    //	return false;
  }
  var res = true;
  if(ie4) e = this.doc.parentWindow.event;
  
  res = this.notifyListeners('docMouseOver',e);
  
  this.mouseover_id = EventManager.getElementID(e);
  if (res && this.mouseover_id)
    if(this.components[this.mouseover_id])		
      res=this.components[this.mouseover_id].doMouseOver(e);
  return res;
}

function _doc_onmouseout(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    //	return false;
  }

  var res =true;
  if(ie4) e = this.doc.parentWindow.event;
  
  if(self.Tooltip)
    Tooltip.hide();
  
  res = this.notifyListeners('docMouseOut',e);	
  if(res && this.mouseover_id)
    if(this.components[this.mouseover_id]) {
      res = this.components[this.mouseover_id].doMouseOut(e);
    }
  this.mouseover_id = null;
  return res;
}

function _doc_onmousemove(e)
{
  if(this.locked) {
    if (ns4) _check_msg_window();
    return false;
  }

  var res=true;
  if(ie4) e = this.doc.parentWindow.event;
  
  this.lastX = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
  this.lastY = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

  res = this.notifyListeners('docMouseMove',e);	
  if(res && this.mouseover_id) {
    if(this.components[this.mouseover_id]) 
      res = this.components[this.mouseover_id].doMouseMove(e);
  }
  return res;	 
}

function _doc_ondblclick (e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  var res = true;

  if (ie4) e = this.doc.parentWindow.event;

  res = this.notifyListeners('docDoubleClick',e);	

  if (res && this.mouseover_id) 
    if(this.components[this.mouseover_id])			
      res = this.components[this.mouseover_id].doDoubleClick(e);

  return res;
}

function _doc_onkeydown (e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  if(ie4)e = this.doc.parentWindow.event;

  if(ns4) 
    var key = e.which;
  else 
    var key = e.keyCode;

  this.notifyListeners ('onKeyDownCallback', e);
  return true;
}

function _doc_onkeyup(e)
{
  if(this.locked) {
    _check_msg_window();
    return false;
  }
  if(ie4)e = this.doc.parentWindow.event;

  if(ns4) 
    var key = e.which;
  else 
    var key = e.keyCode;

  this.notifyListeners ('onKeyUpCallback', e);

  switch(key){
  case 13:
    this.notifyListeners('docEnterKeyUp',e);
    break;
  case 9:
  case 20:
  case 84:
    this.notifyListeners('docTabKeyUp',e);
    break;
  }
  return true;
}

function _window_onfocus(e)
{
  if (ns4) return;
  e = this.doc.parentWindow.event;
  if (! e.shiftKey) {
    var len = this.tabComps.length;
    for (var i=0; i<len; i++) {
      if (this.tabComps[i] && this.tabComps[i].resetFocus)
        this.tabComps[i].resetFocus();
    }
  }
}

/*************************************/

//Extending from Listener implementation
HTMLHelper.importPrototypes(EventManager,Listener);

//static methods
EventManager.Count =0;
EventManager.instances = new Array ();
EventManager.getElementID = EventManager_getElementID;
EventManager.clearInstance = EventManager_clearInstance;

EventManager.prototype.lock = EventManager_lock;
EventManager.prototype.unLock = EventManager_unLock;
EventManager.prototype.add = EventManager_add;
EventManager.prototype.init = EventManager_init;
EventManager.prototype.hitTest = EventManager_hitTest;
EventManager.prototype.setCompMgr = EventManager_setCompMgr;
EventManager.prototype.destroy = EventManager_destroy;

EventManager.prototype.addTabComp = EventManager_addTabComp;
EventManager.prototype.focusForward = EventManager_focusForward;
EventManager.prototype.focusBackward = EventManager_focusBackward;

/*  czStyles.js 115.9 2000/11/07 19:34:39 tabbott ship $ */
 
function Styles(name)
{
  if (name) {
    if (Styles.instances[name]) 
      return(Styles.instances[name]);
    else {
      this.styleHash = null;
      Styles.instances[name] = this;
    }
  } else {
    if (self._styles == null) {
      self._styles = this;
      this.styleHash = null;
    } else
      return (self._styles);
  }	
}

function Styles_generateCSS(wm)
{
  var cSS ="";

  for(var stylName in this.styleHash)
    cSS += this.styleHash[stylName].generateCSS(wm)+'\n';

  return cSS;
}

function Styles_getStyle(name)
{
  if(!name) 
    return null;	
  
  if(this.styleHash) {
    if(this.styleHash[name])
      return this.styleHash[name];
  } else {
    return null;
  }
}

function Styles_addStyle(styl)
{
  if(this.styleHash == null)
    this.styleHash = new Array();

  this.styleHash[styl.getName()] = styl;
}

function Styles_createStyle(name,parent)
{
  var styl = new Style(name,parent);
  if(styl)
    this.addStyle(styl);

  return styl;
}

function Styles_clearInstance (name)
{
  if (Styles.instances[name]) {
    Styles.instances[name].destroy ();

    Styles.instances[name] = null;
    delete Styles.instances[name];
  }
}

function Styles_destroy ()
{
  if (! this.bDestroyed) {
    if (this.styleHash) {
      for (var id in this.styleHash) {
        this.styleHash[id].destroy ();
        
        this.styleHash[id] = null;
        delete this.styleHash[id];
      }
      this.styleHash = null;
      delete this.styleHash;
    }
    this.bDestroyed = true;
  }
}

//Class property (private);
Styles.instances = new Array ();

//Class Methods
Styles.clearInstance = Styles_clearInstance;

//public methods
Styles.prototype.generateCSS 	= Styles_generateCSS;
Styles.prototype.getStyle 	= Styles_getStyle;
Styles.prototype.addStyle 	= Styles_addStyle;
Styles.prototype.createStyle	= Styles_createStyle;
Styles.prototype.destroy	= Styles_destroy;

/**
*  Style class
*/
function Style(name,parent)
{	
  if(name) this.name = name;
  else return null;

  this.attributes = new Array();
  if(parent) {
    var parstyle = this.getStyle(parent);
    for (var attr in parstyle.attributes)
      this.attributes[attr] = parstyle.attributes[attr];
  }
}

function Style_getAttribute(attrName)
{
  if(this.attributes[attrName]) 
    return this.attributes[attrName];
  else return null;
}

function Style_setAttribute(attrName,v)
{
  this.attributes[attrName] = v;
}

function Style_getName()
{
  return this.name;
}

function Style_generateCSS()
{

  var attrs = this.attributes;

  var cSS = "." + this.name + " { ";
  if (attrs['font-family'])
    cSS += "font-family:" + attrs['font-family'] + "; ";
  if (attrs['font-style'])
    cSS += "font-style:" + attrs['font-style'] + "; ";
  if (attrs['font-size'])
    cSS += "font-size:" + attrs['font-size'] + "; ";
  if (attrs['font-weight'])
    cSS += "font-weight:" + attrs['font-weight'] + "; ";
  if (attrs['color'])
    cSS += "color:" + attrs['color'] + ";";
  if (attrs['text-decoration'])
    cSS += "text-decoration:" + attrs['text-decoration'] + "; ";
  if (attrs['text-align'])
    cSS += "text-align:" + attrs['text-align'] + "; ";
  if (attrs['padding-left'])
    cSS += "padding-left:" + attrs['padding-left'] + "; ";
  if (attrs['padding-right'])
    cSS += "padding-right:" + attrs['padding-right'] + "; ";
  if (attrs['padding-top'])
    cSS += "padding-top:" + attrs['padding-top'] + "; ";
  if (attrs['padding-bottom'])
    cSS += "padding-bottom:" + attrs['padding-bottom'] + "; ";

  cSS += " }";
  return cSS;
}

function Style_destroy ()
{
  if (! this.bDestoyed) {
    if (this.attributes) {
      Utils.clearCollection (this.attributes);
      this.attributes = null;
      delete this.attributes;
    }
    this.bDestroyed = true;
  }
}

Style.prototype.getName 	= Style_getName;
Style.prototype.getAttribute 	= Style_getAttribute;
Style.prototype.setAttribute 	= Style_setAttribute;
Style.prototype.generateCSS 	= Style_generateCSS;
Style.prototype.destroy 	= Style_destroy;









/*  czPrompt.js 115.13 2001/05/22 15:10:03 cjeyapra ship $ */

function Prompt()
{
  this.parentConstructor = Base;
  this.parentConstructor();

  this.height = 25;
  this.width = 100;
  this.bCaptionWrap = false;
}

function Prompt_innerRender()
{
  var sBuffer="";
  if(this.caption) {
    if (this.bCaptionWrap) {
      sBuffer += '<SPAN ';
    } else {
      sBuffer +=  '<NOBR ' ;
    }
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.name+ '>'+this.caption;
    if (this.bCaptionWrap) {
      sBuffer += '</SPAN>';
    } else {
      sBuffer +=  '</NOBR>' ;
    }
  }
  return sBuffer;
}

function Prompt_setValue (v)
{
  this.setCaption (v);
}

function Prompt_getValue (v)
{
  return (this.caption);
}

function Prompt_setCaption(v)
{
  var oldValue = this.caption;
  this.caption = v;

  if(this.launched) {
    var sBuffer = '';
    if (this.bCaptionWrap) {
      sBuffer += '<SPAN ';
    } else {
      sBuffer +=  '<NOBR ' ;
    }
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.objId+ '>'+this.caption;

    if (this.bCaptionWrap) {
      sBuffer += '</SPAN>';
    } else {
      sBuffer +=  '</NOBR>' ;
    }
    if(this.block == null)
      this.connect();
    
    if(this.block)
      HTMLHelper.writeToLayer(this.block,sBuffer);
  }
  if (this.caption != oldValue) {
    this.notifyListeners ('captionChangeCallback', oldValue, this);
  }
}

function Prompt_getCaption()
{
  return this.caption;
}

function Prompt_setSpanClass (classId, update)
{
  this.spanClass = classId;
  if (this.launched && update) {
    this.setCaption (this.caption);
  }
}

function Prompt_getSpanClass ()
{
  return (this.spanClass);
}

function Prompt_setCaptionWrapping (bFlag)
{
  this.bCaptionWrap = bFlag;
  //to allow wrapping of caption do not set clip on layer
  this.noclip = false;
}

function Prompt_getCaptionWrapping ()
{
  return (this.bCaptionWrap);
}

//Extend from Base
HTMLHelper.importPrototypes(Prompt,Base);

Prompt.prototype.constructor = Prompt;
Prompt.prototype.innerRender = Prompt_innerRender;
Prompt.prototype.setValue = Prompt_setValue;
Prompt.prototype.getValue = Prompt_getValue;
Prompt.prototype.setCaption = Prompt_setCaption;
Prompt.prototype.getCaption = Prompt_getCaption;
Prompt.prototype.setCaptionWrapping = Prompt_setCaptionWrapping;
Prompt.prototype.getCaptionWrapping = Prompt_getCaptionWrapping;
Prompt.prototype.setSpanClass = Prompt_setSpanClass;
Prompt.prototype.getSpanClass = Prompt_getSpanClass;


/*  czInpTxt.js 115.24 2001/06/05 20:26:29 snadkarn ship $ */

function InputText()
{
  this.parentConstructor = Prompt;
  this.parentConstructor();

  this.value = '';
  this.EDITABLE = false;
  this.editMode = false;
}

function InputText_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function InputText_onLaunchCall()
{
  if (this.standalone) {
    if(this.hFeature)
      this.hide();
    else
      this.show();
  }
  eval(this.self +'=this');
}

function InputText_render ()
{
  if (this.EDITABLE)
    this.addListener ('mouseupCallback', this);

  var sBuffer = "";
  if (ie4) {
    sBuffer = '<INPUT ID=' + this.objId;
    if (this.tabindex)
      sBuffer += ' tabindex="'+ this.tabindex + '"';
    if (this.spanClass)
      sBuffer += ' CLASS=' + this.spanClass;
    sBuffer += this.renderHelper();
    sBuffer += '>';
    return sBuffer;
  }

  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if ( this.alignment != null )
    sBuffer += ' ALIGN="' +this.alignment+ '"';

  sBuffer +='>';
  sBuffer += this.innerRender();
  sBuffer += '</DIV>';
  
  if (this.border)
    sBuffer += '\n</DIV>';
  
  return (sBuffer);
}


function InputText_innerRender()
{
  var sBuffer="";
  if (this.alwaysShowInpTxt) {
    //always show the input text
    sBuffer += '<FORM NAME="editForm" onSubmit="javascript:return false;">';
    sBuffer += '<INPUT NAME="'+this.objId+'" TYPE="text"';
    if (this.width)
      sBuffer += ' SIZE=' + Math.floor((this.width-10)/8);
    sBuffer += this.renderHelper();
    sBuffer +='></INPUT></FORM>';
    return sBuffer;
  }
  if(this.value != null) {
    sBuffer =  '<NOBR ID="SPN-'+this.name+'"' ;
    if(this.spanClass)
      sBuffer += ' CLASS="'+this.spanClass+'"';
    sBuffer += '>'+this.value + '</NOBR>';  
  }
  return sBuffer;
}

function InputText_renderHelper()
{
  var sBuffer = '';
  sBuffer += ' TYPE="text" VALUE="';
  if (this.value != null)
    sBuffer += this.value + '"';
  else
    sBuffer += '""';
  
  if (this.EDITABLE) {
    this.addListener ('onFocusCallback', this);
    this.addListener ('onBlurCallback', this);
    this.addListener ('onKeyPressCallback', this);
    if (ns4)
      var e = "event";
    else
      var e = null;
    sBuffer += ' onfocus="javascript:doOnFocus('+ e +',\''+ this.self + '\')"';
    sBuffer += ' onblur="javascript:doOnBlur('+ e +',\''+ this.self + '\')"';
    sBuffer += ' onkeypress="javascript:doOnKeyPress('+ e + ',\''+ this.self + '\')"';
  } else
    sBuffer += ' readonly=true';

  return sBuffer;
}

function InputText_doFocus(e)
{
  var sBuffer="";
  self.editObj = this;

  this.oldVal = this.value;
  
  if(this.editMode)
    return;

  if(this.block ==null)
    this.connect();		
  
  var obj = this.block;
  
  if (ns4 && (! this.alwaysShowInpTxt)) {
    sBuffer +='<FORM NAME ="editForm" onSubmit="javascript:return false;" ><INPUT NAME="'+this.objId+'" TYPE="text" ';
    
    if (this.width) {
      this.size = Math.floor((this.width-20) / 8);
    }
  
    sBuffer += ' SIZE ='+this.size;
  
    if (! this.windowName)
      sBuffer += ' onblur="javascript:'+this.self+'.doBlur()" ';
  
    sBuffer +=' ></INPUT></FORM>';

    if (this.windowName) {
      this.eventManager.addListener('docMouseDown', this, 'super');
      this.eventManager.addListener('docEnterKeyUp',this);
    }
    HTMLHelper.writeToLayer(obj,sBuffer);
  }

  var txtelem = null;  
  if (ns4) 
    txtelem = eval("obj.document.editForm."+this.objId);
  else if (ie4)
    txtelem = this.block;
  
  if(txtelem) {
    if(this.value != null)
      txtelem.value = this.value;
    else
      txtelem.value = "";
    
    if(ns4) {
      var hwnd = null;
      if (this.windowName) {
        hwnd = self._hwnd[this.windowName];
      } else {
        hwnd = self;
      }
      if (hwnd) {
        var scrTop = hwnd.pageYOffset;
        var scrLeft = hwnd.pageXOffset;
        if (! this.alwaysShowInpTxt)
          txtelem.focus();
        hwnd.scroll(scrLeft,scrTop);
      }
    }
    txtelem.select();
    this.textElement = txtelem;
    this.editMode = true;
  }  
}

function InputText_doBlur()
{
  if(this.editMode && this.textElement) {
    this.editMode = false;
    var obj = this.block;
    var newValue = this.textElement.value;
    var setVal = newValue;
    var gAmp = /&/g;
    newValue = newValue.replace(gAmp,'&amp;');
    var gLess = /</g;
    newValue = newValue.replace(gLess,'&lt;');
    var gGreat = />/g;
    newValue = newValue.replace(gGreat,'&gt;');
    var gQuot = /"/g;
    newValue = newValue.replace(gQuot,'&quot;');

    var res = true;
    
    if(newValue != this.value)
      res = this.notifyListeners('onchangeCallback',newValue,this);
    
    if(res)
      this.setValue(setVal);
    else 
      this.setValue(this.value);
        
    if (ns4 && (! this.alwaysShowInpTxt)) {
      if (this.oldHeight) {
        this.setHeight (this.oldHeight);
      }
      if (this.windowName) {
        this.eventManager.removeListener('docEnterKeyUp',this);
        this.eventManager.removeListener ('docMouseDown', this);
      }
    }
    this.notifyListeners('onBlur', newValue, this);
    this.textElement = null;
    self.editObj = null;
  }
}

function InputText_onFocusCallback (e)
{
  this.doFocus(e);
  if (this.parentObj && (this.parentObj.type == 'CountedOptionItem')) {
    this.parentObj.parentObj.setFocussedItem(this.parentObj);
  }
  return true;
}

function InputText_onBlurCallback (e)
{
  this.doBlur(e);
  return true;
}

function InputText_onKeyPressCallback (e)
{
  var key = (ns4)? e.which :e.keyCode;
  if (key == 13) {
    this.doBlur (e);
  }
  return true;
}

function InputText_docEnterKeyUp(e)
{
  if(this.editMode == true)
    this.doBlur(e);

  return true;
}

function InputText_docMouseDown (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var id = eSrc.id;
    if (! id) {
      if (eSrc.name)
        id = eSrc.name;
    }
    if ((id == this.name) || (id == this.objId))
      return (true);
    else
      this.doBlur (e);
  }
  else
    this.doBlur (e);
  return (true);
}

function InputText_mouseupCallback(e,src)
{
  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;
  
  if(src.EDITABLE && src.editMode == false) {
    if (ns4) {
      var ht = this.getHeight ();
      if (ht < 28) {
        this.oldHeight = ht;
        this.setHeight (28);
      }
    }
    src.doFocus(e);
  }
  return true;
}

function InputText_setValue(v)
{
  this.value = v;
  if (this.launched) {    
    if(this.block == null)
      this.connect();
    
    if (ie4 && this.block) {
      this.block.value = v;
      return;
    } else if (ns4 && this.alwaysShowInpTxt) {
      if (! this.textElement)
        this.textElement = eval("this.block.document.editForm."+this.objId);
      this.textElement.value = v;
      return;
    }
    var sBuffer =  '<NOBR '; 
    if(this.spanClass)
      sBuffer += ' CLASS='+this.spanClass;
    sBuffer += ' ID=SPN-'+this.name+'>'+this.value + '</NOBR>';
    
    if(this.block)
      HTMLHelper.writeToLayer(this.block,sBuffer);
  }
}

function InputText_getValue()
{
  return this.value;
}

//Extend from Prompt
HTMLHelper.importPrototypes(InputText,Prompt);

InputText.prototype.constructor 	= InputText;
InputText.prototype.render 	        = InputText_render;
InputText.prototype.innerRender 	= InputText_innerRender;
InputText.prototype.mouseupCallback     = InputText_mouseupCallback;
InputText.prototype.doFocus 		= InputText_doFocus;
InputText.prototype.doBlur 		= InputText_doBlur;
InputText.prototype.onFocusCallback     = InputText_onFocusCallback;
InputText.prototype.onBlurCallback      = InputText_onBlurCallback;
InputText.prototype.onKeyPressCallback  = InputText_onKeyPressCallback;
InputText.prototype.docEnterKeyUp 	= InputText_docEnterKeyUp;
InputText.prototype.docMouseDown        = InputText_docMouseDown;
InputText.prototype.setValue 		= InputText_setValue;
InputText.prototype.setCount 		= InputText_setValue;
InputText.prototype.getValue 		= InputText_getValue;
InputText.prototype.hideFeature 	= InputText_hideFeature;
InputText.prototype.onLaunchCall 	= InputText_onLaunchCall;
InputText.prototype.renderHelper 	= InputText_renderHelper;


/*  czImgBtn.js 115.18 2001/07/06 18:57:45 snadkarn ship $ */

function ImageButton()
{
  this.parentConstructor = Base;
  this.parentConstructor();
  this.srcs = new Array();
  this.states = 0;
  this.stretch = false;
  this.toolTipText = '';

  this.addListener('mousemoveCallback',this);
  this.addListener('mouseupCallback',this);
  this.addListener('mousedownCallback',this);
}

function ImageButton_setImages()
{
  for(var i=0; i<arguments.length;i++) {
    if(arguments[i]) {
      this.srcs[i] = arguments[i];
    }
  }
  this.src = this.srcs[0];	
}


function ImageButton_setSrc(s)
{
  if(s) 
    this.src = s;
  else 	
    return;

  if(this.launched && this.getHTMLObj()) {
    if (ns4)
      this.block.document.images[0].src = s;
    else if (ie4) 	
      this.block.all['IMG-' + this.name].src = s;
  }
}

function ImageButton_toggle(stateNum){

  if(stateNum!=null) {
    if (this.state == stateNum) 
      return;	
    else 
      this.state = stateNum;
    
    this.setSrc(this.srcs[this.state]);
  }
}

function ImageButton_setImgOnSrc(imgOnSrc)
{
  this.imgOnSrc = imgOnSrc;
}

function ImageButton_setImgOffSrc(imgOffSrc)
{
  this.imgOffSrc = imgOffSrc;	
  this.setSrc(imgOffSrc);
}

function ImageButton_setToolTipText(toolTipText)
{
  this.toolTipText=toolTipText;
  if(this.launched && this.getHTMLObj()) {
    if (ns4)
      this.block.document.images[0].alt = toolTipText;
    else if (ie4) 	
      this.block.all['IMG-' + this.name].alt = toolTipText;
  }    
}

function ImageButton_innerRender()
{
  var sBuffer;
  var sBuffTerm = '';
  if ((this.type == 'button') || (this.active && ie4) || 
      (this.sclBtn && ie4)) {
    sBuffer = '<A href="javascript:void(0)"';
    sBuffer += ' NAME="A-'+ this.objId + '"';
    if (this.imgTabindex)
      sBuffer += ' tabindex="' + this.imgTabindex + '"';
    else
      sBuffer += ' tabindex="-1"';

    eval(this.self +'=this');
    if (ns4)
      var e = "event";
    else
      var e = null;
    if (this.sclBtn && ie4) {
      //for handling scroll bar arrow buttons
      sBuffer += ' onkeydown="javascript:doOnKeyDown('+ e +',\'' + this.self + '\')"';
      sBuffer += ' onkeyup="javascript:doOnKeyUp('+ e +',\'' + this.self + '\')"';
    } else if ((this.type == 'button') || (this.active && ie4)) {
      //For handling active images like state icons
      sBuffer += ' onkeypress="javascript:doOnKeyPress('+ e +',\'' + this.self + '\')"';
    }
    if ((this.type == 'button') && ie4)
      this.addListener ('onKeyPressCallback', this);

    sBuffer += '><IMG ID=IMG-'+this.objId +' NAME=IMG-'+this.objId+' BORDER=0';
    sBuffTerm = '</A>';      
  } else
    sBuffer = '<IMG ID=IMG-'+this.objId +' NAME=IMG-'+this.objId;	

  if(this.src) 
    sBuffer+=' SRC="'+ this.src +'"';

  if (this.stretch)
    sBuffer += ' HEIGHT='+ this.height + ' WIDTH='+ this.width;

  if (this.toolTipText!='')
    sBuffer+=' ALT="'+this.toolTipText+'" ';

  sBuffer += '></IMG>'+sBuffTerm;

  return sBuffer;
}

function ImageButton_mousemoveCallback(e)
{
  var em = this.eventManager;
  if(em.msdown && (this.states > 0)) {
    if(ie4)
      e.returnValue = false;
    else
      return false;
  }
  return true;
}

function ImageButton_mousedownCallback(e)
{
  if ((ns4 && e.which!=1) || (ie4 && e.button!=1)) 
    return true;
  
  if(this.states>0) {
    this.out = false;
    if(this.states == 1) 
      this.currState = 1;
    else if(this.states >1) 
      this.currState = (this.state+1)%this.states;
    
    this.prevState = this.state;
    this.toggle(this.currState);
  }
  return true;
}

function ImageButton_mouseupCallback(e)
{
  if ((navigator.platform.indexOf("MacPPC") == -1 ) && ((ns4&& e.which!=1) || (ie4 && e.button!=1))) 
    return true;
  
  if(this.states>0) {
    var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
    var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;
    var absx = this.getLeft('abs');
    var absy = this.getTop('abs');
    var inside = false;
    inside = this.isInside(x,y,absy,absx+this.width,absy+this.height,absx);
    
    if(!inside) {
      this.toggle(this.prevState);
    } else {
      if(this.states == 1)
        this.toggle(0);
    }
  }
  this.notifyListeners('onClick', e, this);
  return true;
}

function ImageButton_onKeyPressCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  if (key == 13) {
    //IE window scrolls, this has to be there to prevent scrolling of the window
    if (ie4)
      e.returnValue = false;

    this.notifyListeners('onClick', e, this);
  }
  return true;
}

function ImageButton_setFocus()
{
  if (this.launched && this.getHTMLObj() && ie4) {
    if ((this.type == 'button') || this.active || this.sclBtn) {
      this.block.all['A-' + this.objId].focus();
    }
  }
}

function ImageButton_setTabindex(ind)
{
  this.tabindex = null;
  this.imgTabindex = ind;
}

function ImageButton_getTabindex()
{
  return this.imgTabindex;
}

//Extending from Base
HTMLHelper.importPrototypes(ImageButton,Base);

ImageButton.prototype.constructor 	 = ImageButton;
ImageButton.prototype.setSrc 		 = ImageButton_setSrc;
ImageButton.prototype.setToolTipText 	 = ImageButton_setToolTipText;
ImageButton.prototype.innerRender 	 = ImageButton_innerRender;
ImageButton.prototype.setImages 	 = ImageButton_setImages;
ImageButton.prototype.toggle 		 = ImageButton_toggle;
ImageButton.prototype.mousemoveCallback  = ImageButton_mousemoveCallback;
ImageButton.prototype.mouseupCallback 	 = ImageButton_mouseupCallback;
ImageButton.prototype.mousedownCallback  = ImageButton_mousedownCallback;
ImageButton.prototype.onKeyPressCallback = ImageButton_onKeyPressCallback;
ImageButton.prototype.setImgOnSrc 	 = ImageButton_setImgOnSrc;
ImageButton.prototype.setImgOffSrc 	 = ImageButton_setImgOffSrc;
ImageButton.prototype.setFocus           = ImageButton_setFocus;
ImageButton.prototype.setTabindex        = ImageButton_setTabindex;
ImageButton.prototype.getTabindex        = ImageButton_getTabindex;

/*  czCrvBtn.js 115.21 2001/07/06 18:57:56 snadkarn ship $ */
 
function CurvedButton()
{
  this.parentConstructor = Container;
  this.parentConstructor();

  this.height = 20;
  this.width = 100;
  this.curvedLeft = 11;
  this.curvedRight = 11;
  
  this.caption='';
  this.toggleButton = false;
  this.state = 0;
  this.button = true;
  this.built = false;
  this.toolTipText='';
  this.addListener('mousedownCallback',this);
  this.addListener('mouseupCallback',this);
  this.addListener('mousemoveCallback',this);
  this.enableKeyPressEvents();
  this.addListener('onKeyPressCallback', this);
}

function CurvedButton_buildControl ()
{
  if (this.built) return;

  var curWidth = 0;
  //create left curve image;
  if (this.curvedLeft) {
    var img = this.img1 = new ImageButton ();
    img.setName ('LImg-'+this.name);
    img.setDimensions (0, 0, this.curvedLeft, this.height);
    img.stretch = true;
    img.setImages (IMAGESPATH+'czButL0.gif',IMAGESPATH+'czButL0.gif'); 
    curWidth += this.curvedLeft;
  }
  //create right curve image;
  if (this.curvedRight) {
    var img = this.img2 = new ImageButton ();
    img.setName ('RImg-'+this.name);
    img.setDimensions (this.width - this.curvedRight, 0, this.curvedRight, this.height);
    img.stretch = true;
    img.setImages(IMAGESPATH+'czButR0.gif',IMAGESPATH+'czButR0.gif')
  }
  //create center image;
  var cenWd = this.width - (this.curvedLeft + this.curvedRight);
  var img = this.img3 = new ImageButton ();
  img.setName ('CImg-'+this.name);
  img.setDimensions (curWidth, 0, cenWd, this.height);
  img.stretch = true;
  img.setImages(IMAGESPATH+'czButC0.gif',IMAGESPATH+'czButC0.gif');
  
  //create a prompt for writing caption;
  var capt = this.capt = new Prompt ();
  this.setAlignment('center');
  capt.setName ('Captn-'+this.name);
  capt.setDimensions (curWidth, 4, cenWd, this.height-2);
  capt.setCaption (this.caption);
  if (this.spanClass) {
    capt.setSpanClass (this.spanClass);
  }
  //create a top layer;
  this.topLay = new ImageButton();
  this.topLay.setName ('TLay-'+this.name);
  this.topLay.setDimensions (0, 0, this.width, this.height);
  this.topLay.stretch = true;
  this.topLay.setImages (IMAGESPATH+'czblank.gif');
  this.topLay.setToolTipText (this.toolTipText);
  this.topLay.type = 'button';

  //add comps to container;
  if(this.img1)
    this.add(this.img1);

  if(this.img2)
    this.add(this.img2);

  this.add(this.img3);
  this.add(this.capt);
  this.add(this.topLay);

  this.built = true;
}

function CurvedButton_toggle(bVal)
{
  if(bVal) {
    if(this.img1) 
      this.img1.toggle(1);
    if(this.img2) 
      this.img2.toggle(1);
    if(this.img3) 
      this.img3.toggle(1);

    if(this.capt) {
      if(this.selectedStyle) {
        this.capt.spanClass = this.selectedStyle;
        this.setCaption(this.caption);
      }
    }
    this.state =1;
  } else {
    if(this.img1)
      this.img1.toggle(0);
    if(this.img2)
      this.img2.toggle(0);
    if(this.img3)
      this.img3.toggle(0);

    if(this.capt) {
      if(this.unselectedStyle) {
        this.capt.spanClass = this.unselectedStyle;
        this.setCaption(this.caption);
      }
    }
    this.state =0;
  }
}

function CurvedButton_setToolTipText(toolTipText)
{
  this.toolTipText=toolTipText;
}

function CurvedButton_setLeftImages(leftImg0,leftImg1)
{
  if(this.img1)
    this.img1.setImages(leftImg0,leftImg1);
}

function CurvedButton_setRightImages(rightImg0,rightImg1)
{
  if(this.img2)
    this.img2.setImages(rightImg0,rightImg1);
}

function CurvedButton_setCenterImages(img0,img1)
{
  if(this.img3)
    this.img3.setImages(img0,img1);
}

function CurvedButton_setLeftCurveWidth(val)
{
  if (isNaN(val)) {
    alert ("CurvedButton:setLeftCurveWidth:" + val + " Not a number");
    return;
  }
  this.curvedLeft = val;
}

function CurvedButton_setRightCurveWidth(val)
{
  if (isNaN(val)) {
    alert ("CurvedButton:setRightCurveWidth:" + val + " Not a number");
    return;
  }
  this.curvedRight = val;
}

function CurvedButton_moreLaunch(em)
{
  //Launch the child comps, but don't add it to EvtMgr;
  if(this.children) {
    for (var childName in this.children) 
      this.children[childName].launch();
  }  
}

function CurvedButton_mousemoveCallback(e)
{
  var em = this.eventManager;
  if(em.msdown && (this.states > 0)) {
    if(ie4)
      e.returnValue = false;
    else
      return false;
  }
  return true;
}

function CurvedButton_mousedownCallback(e, source)
{

  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;

  if(this.toggleButton)
    this.toggle(1-this.state);
  else if(this.button)
    this.toggle(1);		
  return true;
}

function CurvedButton_mouseupCallback(e, source)
{
  if ((navigator.platform.indexOf("MacPPC") == -1 ) && ((ns4 && e.which!=1) || (ie4 && e.button!=1))) 
    return true;

  if(this.toggleButton || this.button) {
    var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
    var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;
    var absx = this.getLeft('abs');
    var absy = this.getTop('abs');
    var inside = false;
    inside = this.isInside(x,y,absy,absx+this.width,absy+this.height,absx);

    if(! this.toggleButton)
      this.toggle(0);

    if(!inside && this.toggleButton)
      this.toggle(1-this.state);
    else 
      this.toggle(this.state);

    this.notifyListeners('onClick',e, this);
  }
  return true;
}

function CurvedButton_setCaption(v)
{
  if(v) {
    if (this.built)
      this.capt.setCaption(v);
    this.caption = v;
  }
}

function CurvedButton_getCaption()
{
  if(this.capt)
    return this.capt.getCaption();
}

function CurvedButton_setBackgroundColor(c)
{
  return true;
}

function CurvedButton_onKeyPressCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  if (key == 13) {
    if (ie4)
      e.returnValue = false;
    
    this.toggle(0);
    this.notifyListeners('onClick', e, this);
  }
  return true;
}

HTMLHelper.importPrototypes(CurvedButton,Container);

CurvedButton.prototype.constructor 	= CurvedButton;
CurvedButton.prototype.toggle 		= CurvedButton_toggle;
CurvedButton.prototype.setToolTipText 	= CurvedButton_setToolTipText;
CurvedButton.prototype.setCaption 	= CurvedButton_setCaption;
CurvedButton.prototype.getCaption 	= CurvedButton_getCaption;
CurvedButton.prototype.setLeftImages 	= CurvedButton_setLeftImages;
CurvedButton.prototype.setRightImages 	= CurvedButton_setRightImages;
CurvedButton.prototype.setCenterImages 	= CurvedButton_setCenterImages;
CurvedButton.prototype.mousedownCallback= CurvedButton_mousedownCallback;
CurvedButton.prototype.mouseupCallback	= CurvedButton_mouseupCallback;
CurvedButton.prototype.mousemoveCallback = CurvedButton_mousemoveCallback;
CurvedButton.prototype.moreLaunch = CurvedButton_moreLaunch;
CurvedButton.prototype.setBackgroundColor = CurvedButton_setBackgroundColor;
CurvedButton.prototype.setLeftCurveWidth = CurvedButton_setLeftCurveWidth;
CurvedButton.prototype.setRightCurveWidth = CurvedButton_setRightCurveWidth;
CurvedButton.prototype.onKeyPressCallback = CurvedButton_onKeyPressCallback;

//private method
CurvedButton.prototype.buildControl = CurvedButton_buildControl;

/*  czSclBar.js 115.13 2001/05/10 22:19:41 cjeyapra ship $ */

function ScrollBar()
{
  this.parentConstructor = Container;
  this.parentConstructor();

  this.backgroundColor = 'lightgrey';
  this.dir ='v';
  this.curValue = 0;
  this.minValue = 0;
  this.maxValue = 100;
  this.range = 100;
  this.cornerLength = 0;
  this.useSizedBar = false;
  this.arrow1 = null;
  this.arrow2 = null;
  this.bar = null;
  this.box = null;
  
  this.cycle = 1;
  this.currSec = 1;
  this.enable = true;
  this.step = 1;
  this.page = 10;	
  this.scrollPercentage = 0;
}

function ScrollBar_drawCorner(length)
{
  //where 'length' is generally the thickness of the opposing scrollbar;
  if (length > 0) {
    this.cornerLength = length;
  } else {
    this.cornerLength = 0;
  }
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);	
}

function ScrollBar_getAdjustedLength()
{
  if (this.dir == 'h') {
    return this.width - this.cornerLength;
  } else {
    return this.height - this.cornerLength;
  }
}
function ScrollBar_updateDimensions()
{
  if (this.built) {
    if (this.dir == 'h') {
      this.box.setLeft (this.height);
      this.box.setTop (0);
      this.box.setHeight (this.height);
      this.box.setWidth (this.getAdjustedLength()  - 2*this.height);
      this.bar.setLeft (this.arrow1.width);
      this.arrow1.setWidth (this.height);
      this.arrow2.setLeft (this.getAdjustedLength() - this.arrow2.width);
      this.bardrag.lockRegion(this.bar,0,this.box.width,this.box.height,0);
    } else {
      this.box.setTop (this.width);
      this.box.setLeft (0);
      this.box.setHeight (this.getAdjustedLength() - 2*this.width);
      this.box.setWidth (this.width);
      this.bar.setTop (this.arrow1.height);
      this.arrow1.setWidth (this.width);
      this.arrow2.setTop (this.getAdjustedLength() - this.arrow2.height);
      this.bardrag.lockRegion(this.bar,0,this.box.width,this.box.height,0);
    }
  }
}

function ScrollBar_attachTo(content)
{
  this.content = content;
}

function ScrollBar_reset()
{
  this.setValue(0);
}

function ScrollBar_setValues(v, min, max, step, page, bRiseEvent)
{
  this.curValue = v;
  this.minValue = min;
  this.maxValue = max;
  this.step = step;
  this.page = page;
  
  //the range is the absolute magnitude of min and max;
  var range = Math.abs(this.maxValue - this.minValue);
  if (range != this.range) {
    this.range = range;
  }
  
  //round to the nearest step;
  this.curValue = (v) - (v % this.step);
  this.scrollPercentage = (this.curValue / this.range);

  //position the scroll 'bar';
  if (this.bar) {
    if (this.dir == 'h') {
      this.bar.setLeft(this.scrollPercentage *(this.box.width-this.bar.width));
    } else {
      this.bar.setTop(this.scrollPercentage *(this.box.height-this.bar.height));
    }
  }
  if (bRiseEvent) {
    this.notifyListeners('scrollbarCallback', this);
  }
}

function ScrollBar_setMinValue(v)
{
  this.setValues(this.curValue, v, this.maxValue, this.step, this.page);
}

function ScrollBar_getMinValue()
{
  return this.minValue;
}

function ScrollBar_setMaxValue(v)
{
  if (v <= 0) {
    v = 1;
  }
  this.setValues(this.curValue, this.minValue, v, this.step, this.page);
}

function ScrollBar_getMaxValue()
{
  return this.maxValue;
}

function ScrollBar_setValue(v)
{
  this.setValues(v, this.minValue, this.maxValue, this.step, this.page, true);
}

function ScrollBar_getValue()
{
  return this.curValue;
}

function ScrollBar_setStepSize(v)
{
  this.setValues(this.curValue, this.minValue, this.maxValue, v, this.page);
}

function ScrollBar_getStepSize()
{
  return this.step;
}

function ScrollBar_setPageSize(v)
{
  this.setValues(this.curValue, this.minValue, this.maxValue, this.step, v);
}

function ScrollBar_getPageSize()
{
  return this.page;
}

function ScrollBar_buildControl ()
{
  if(this.built) return;
  else this.built = true;

  var arrow1,arrow2,bar,box;
  
  if(this.dir) {
    if(this.dir == 'h') {
      //left arrow;
      if(this.Arrow1ONImage == null)
	this.Arrow1ONImage = IMAGESPATH +'czLSArr.gif';
      if(this.Arrow1OFFImage == null)
        this.Arrow1OFFImage = IMAGESPATH +'czLSArr1.gif';
      arrow1 = new ImageButton();
      arrow1.setName (this.name+'xHArw1');
      arrow1.setDimensions (0, 0, this.height, this.height);
      arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);
      arrow1.stretch = true;
      arrow1.what = 'HArrow1';
      
      //right arrow;
      if(this.Arrow2ONImage == null)
        this.Arrow2ONImage = IMAGESPATH + 'czRSArr.gif';
      if(this.Arrow2OFFImage == null)
        this.Arrow2OFFImage = IMAGESPATH + 'czRSArr1.gif';
      arrow2 = new ImageButton();
      arrow2.setName (this.name+'xHArw2');
      arrow2.setDimensions (this.width-this.height, 0, this.height, this.height);
      arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);
      arrow2.stretch = true;
      arrow2.what = 'HArrow2';
      
      //horizontal box;
      box = new Container();
      box.setName (this.name+'xHBox');
      box.setDimensions (this.height, 0, this.width-this.height*2, this.height);
      box.setBackgroundColor ("#E0E0E0");
      
      //horizontal bar inside the box;
      if(this.BarONImage == null)
        this.BarONImage = IMAGESPATH + 'czHBar.gif';
      bar = new ImageButton();
      bar.setName (this.name+'xHBar');
      bar.setDimensions (0, 0, this.height*2, this.height);
      bar.setImages(this.BarONImage,this.BarOFFImage);
      bar.stretch = true;
      bar.what = 'HBar';
    } else {
      //add a up-arrow;
      if(this.Arrow1ONImage == null)
        this.Arrow1ONImage = IMAGESPATH +'czUSArr.gif';
      if(this.Arrow1OFFImage == null)
        this.Arrow1OFFImage = IMAGESPATH +'czUSArr1.gif';  
      arrow1 = new ImageButton();
      arrow1.setName (this.name+'xVArw1');
      arrow1.setDimensions (0, 0, this.width, this.width);
      arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);
      arrow1.stretch = true;
      arrow1.what = 'VArrow1';
      
      //add a down-arrow;
      if(this.Arrow2ONImage == null)
        this.Arrow2ONImage = IMAGESPATH + 'czDSArr.gif';
      if(this.Arrow2OFFImage == null)
        this.Arrow2OFFImage = IMAGESPATH + 'czDSArr1.gif';
      arrow2 = new ImageButton();
      arrow2.setName (this.name+'xVArw2');
      arrow2.setDimensions (0, this.height-this.width, this.width, this.width);
      arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);
      arrow2.stretch = true;
      arrow2.what = 'VArrow2';
      
      //add a vertical box;
      box = new Container();
      box.setName (this.name+'xVBox');
      box.setDimensions (0, this.width, this.width, this.height-2*this.width);
      box.setBackgroundColor ("#E0E0E0");
      
      //add a vertival bar inside the box;
      if(this.BarONImage == null)
        this.BarONImage = IMAGESPATH +'czVBar.gif';
      bar = new ImageButton();
      bar.setName (this.name+'xVBar');
      bar.setDimensions (0, 0, this.width, this.width*2);
      bar.setImages(this.BarONImage,this.BarOFFImage);
      bar.stretch = true;
      bar.what = 'VBar';
      if (this.dir == null) this.dir = 'v';
    }
  }
  arrow1.addListener('mousedownCallback',Arrow_MouseDownCallback);
  arrow2.addListener('mousedownCallback',Arrow_MouseDownCallback);
  arrow1.addListener('mouseupCallback',Arrow_MouseUpCallback);
  arrow2.addListener('mouseupCallback',Arrow_MouseUpCallback);

  arrow1.addListener('mouseoutCallback',Arrow_MouseOutCallback);
  arrow2.addListener('mouseoutCallback',Arrow_MouseOutCallback);
  arrow1.addListener('mousemoveCallback', Arrow_MouseMoveCallback);
  arrow2.addListener('mousemoveCallback', Arrow_MouseMoveCallback);

  box.addListener('mousedownCallback',ScrollBox_MouseDownCallback);
  box.addListener('mouseupCallback',ScrollBox_MouseUpCallback);
  box.addListener('mouseoutCallback',ScrollBox_MouseOutCallback);

  var bardrag = new Drag('BarDrag', this.eventManager);
  bardrag.add(bar);
  bardrag.lockRegion(bar,0,box.width,box.height,0);
  bardrag.on();
  this.bardrag = bardrag;
  
  bar.resizable =false;
  //KAM bar.slideInit();
  
  bar.addListener('onDragging', ScrollBar_draggingCallback);
  bar.addListener('onDragStart', ScrollBar_dragStartCallback);
  bar.addListener('onDragEnd', ScrollBar_dragEndCallback);

  this.add(box,arrow1,arrow2);
  box.add(bar);
  this.bar = bar;
  this.box = box;
  this.arrow1 = arrow1;
  this.arrow2 = arrow2;
  this.bar.addListener('onSlide',Bar_onSlide);

  this.updateDimensions ();
  this.setStepSize(this.step);	
}

function ScrollBar_scrollSteps(steps)
{
  this.setValue(this.curValue + this.step * steps);
}

function ScrollBar_setImages(a1On,a1Off,a2On,a2Off,barOn,barOff,boxBG)
{
  if(a1On) 
    this.Arrow1ONImage = a1On;
  if(a1Off) 
    this.Arrow1OFFImage = a1Off;
  this.arrow1.setImages(this.Arrow1ONImage,this.Arrow1OFFImage);

  if(a2On) 
    this.Arrow2ONImage = a2On;
  if(a2Off) 
    this.Arrow2OFFImage = a2Off;
  this.arrow2.setImages(this.Arrow2ONImage,this.Arrow2OFFImage);

  if(barOn) 
    this.BarONImage = barOn;
  if(barOff) 
    this.BarOFFImage = barOff;
  this.bar.setImages(this.BarONImage,this.BarOFFImage);

  if(boxBG) 
    this.BoxBGImage = boxBG;
}

function ScrollBar_setCycle(numCycle)
{
  if(numCycle) 
    this.cycle = numCycle;
}

function ScrollBar_getCycle()
{
  return this.cycle;
}

function ScrollBar_getScrollPercentage()
{
  return this.scrollPercentage;
}

function ScrollBar_setScrollPercentage(perc)
{	
  var scrVal=0;

  if(perc > 1)  
    perc =1;
  if(perc < 0) 
    perc = 0;

  if(perc == 1) {
    scrVal = this.maxValue;
  } else if(this.range) {
    scrVal = (perc*(this.range) + this.minValue);
  }
  
  this.setValue(scrVal);
}		

function ScrollBar_moreSetHeight(ht)
{
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);
}

function ScrollBar_moreSetWidth(wd)
{
  this.updateDimensions();
  //force a recalc of values;
  this.setValues (this.curValue, this.minValue, this.maxValue, this.step, this.page);
}

function ScrollBar_boundedValue(value) {
  if (value > this.maxValue) {
    return this.maxValue;  
  } else if (value < this.minValue) { 
    return this.minValue;
  } else {
    return value;
  }
}

function ScrollBar_setEventManager (em)
{
  this.eventManager = em;
}

function ScrollBar_scrollDown()
{
  var retVal = true;
  var boundVal = this.boundedValue (this.curValue+1);
  if (boundVal != (this.curValue+1))
    retVal = false;

  this.setValue(boundVal);

  return (retVal);
}

function ScrollBar_scrollUp()
{
  var retVal = true;
  var boundVal = this.boundedValue (this.curValue-1);
  if (boundVal != (this.curValue-1))
    retVal = false;
  
  this.setValue(boundVal);
  
  return (retVal);
}

//construction
HTMLHelper.importPrototypes(ScrollBar,Container);

ScrollBar.prototype.constructor 	= ScrollBar;

//public methods
ScrollBar.prototype.setMinValue = ScrollBar_setMinValue;
ScrollBar.prototype.setMaxValue = ScrollBar_setMaxValue;
ScrollBar.prototype.getMinValue = ScrollBar_getMinValue;
ScrollBar.prototype.getMaxValue = ScrollBar_getMaxValue;
ScrollBar.prototype.setValue = ScrollBar_setValue;
ScrollBar.prototype.getValue = ScrollBar_getValue;
ScrollBar.prototype.setImages = ScrollBar_setImages;
ScrollBar.prototype.setStepSize = ScrollBar_setStepSize;
ScrollBar.prototype.getStepSize = ScrollBar_getStepSize;
ScrollBar.prototype.setPageSize = ScrollBar_setPageSize;
ScrollBar.prototype.getPageSize = ScrollBar_getPageSize;
ScrollBar.prototype.reset = ScrollBar_reset;
ScrollBar.prototype.setScrollPercentage = ScrollBar_setScrollPercentage;
ScrollBar.prototype.getScrollPercentage = ScrollBar_getScrollPercentage;
ScrollBar.prototype.setCycle = ScrollBar_setCycle;
ScrollBar.prototype.getCycle = ScrollBar_getCycle;
ScrollBar.prototype.moreSetHeight = ScrollBar_moreSetHeight;
ScrollBar.prototype.moreSetWidth = ScrollBar_moreSetWidth;
ScrollBar.prototype.drawCorner = ScrollBar_drawCorner;
ScrollBar.prototype.setEventManager = ScrollBar_setEventManager;

//private methods
ScrollBar.prototype.buildControl = ScrollBar_buildControl;
ScrollBar.prototype.setValues = ScrollBar_setValues;
ScrollBar.prototype.scrollSteps = ScrollBar_scrollSteps;
ScrollBar.prototype.updateDimensions = ScrollBar_updateDimensions;
ScrollBar.prototype.getAdjustedLength = ScrollBar_getAdjustedLength;
ScrollBar.prototype.boundedValue = ScrollBar_boundedValue;

ScrollBar.prototype.scrollDown = ScrollBar_scrollDown;
ScrollBar.prototype.scrollUp = ScrollBar_scrollUp;

/*---------------------------------------------
Arrow Callback functions
---------------------------------------------*/
function Arrow_MouseUpCallback(e,obj)
{
  if(obj.parentObj.bar != null )
    obj.parentObj.bar.slideStop();
  obj.toggle(0);
  self.recObj = null;
  clearTimeout(self.timeId);
  self.timeId = null;
  return true;
}

function Arrow_MouseMoveCallback(e,obj)
{
  if ( self.timeId && obj.eventManager.msdown == true){
    obj.toggle(0);
    self.recObj = null;
    clearTimeout(self.timeId);
    self.timeId = null;
  }
  return true;
}

function Arrow_MouseOutCallback(e,obj)
{
  if ( obj.eventManager.msdown == true && self.timeId ){
    obj.toggle(0);
    self.recObj = null;
    clearTimeout(self.timeId);
    self.timeId = null;
  }
  return true;
}

function Arrow_MouseDownCallback(e,obj)
{
  self.recObj = null;
  Arrow_recursiveMouseDown(obj);
}

function Arrow_recursiveMouseDown(obj)
{
  if ( self.recObj )
    obj = self.recObj;

  if (obj == null) {
    clearTimeout (self.timeId);
    return true;
  }
  var par = obj.parentObj;
  var box = par.box;
  var bar = par.bar;
  
  obj.toggle(1);

  if(obj.what=='HArrow1') {
    if(bar.left <=0) {
      if(par.onScrollEnd) 
        par.onScrollEnd(0);
    } else { 
      par.setValue(par.curValue - par.step);
    }
  }
  
  if(obj.what=='VArrow1') {
    if(bar.top <=0) {
      if(par.onScrollEnd) 
        par.onScrollEnd(0);
      if (par.curValue > par.minValue) {
        par.setValue (par.curValue - par.step);
      }
    } else 
      par.setValue(par.curValue - par.step);
  }
  
  if(obj.what=='HArrow2') {
    if(bar.left >=(box.width-bar.width)) {
      if(par.onScrollEnd) 
        par.onScrollEnd(1);
    }
    else 
      par.setValue(par.curValue + par.step);
  }
  
  if(obj.what=='VArrow2') {
    if(bar.top >= (box.height-bar.height)) {
      if(par.onScrollEnd) 
        par.onScrollEnd(1);
      if (par.curValue < par.maxValue) {
        par.setValue (par.curValue + par.step);
      }
    } else 
      par.setValue(par.curValue + par.step);
  }
  
  self.recObj = obj;
  self.timeId = setTimeout("Arrow_recursiveMouseDown()", 100);

  return true;
}

function Arrow_onKeyDownCallback (e, sclBar)
{
  self.recObj = null;
  var key = (ns4)? e.which :e.keyCode;
  switch (key) {
  case 37:
  case 38:
    Arrow_recursiveMouseDown(sclBar.arrow1);
    break;
  case 39:
  case 40:
    Arrow_recursiveMouseDown(sclBar.arrow2);
    break;
  }
  switch (key) {
  case 37:
  case 39:
  case 38:
  case 40:
    if (ie4) {
      e.cancelBubble = true;
      e.returnValue = false;
    }
  }
  return true;
}

function Arrow_onKeyUpCallback (e, sclBar)
{
  var key = e.keyCode;
  switch (key) {
  case 37:
  case 38:
    Arrow_MouseUpCallback(e, sclBar.arrow1);
    break;
  case 39:
  case 40:
    Arrow_MouseUpCallback(e, sclBar.arrow2);
    break;
  }
  switch (key) {
  case 37:
  case 39:
  case 38:
  case 40:
    if (ie4)
      e.returnValue = false;
    break;
  }
  return true;
}

/*
Callback function for bar sliding
*/

function Bar_onSlide(speed,obj)
{
  var perc;
  var SBW = obj.parentObj.parentObj;
  
  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);
  
  if(SBW.contents)
    SBW.setScrollPercentage(perc);

  if(SBW.onScrolling) 
    SBW.onScrolling(perc,this);

  if(speed >50)
    speed = speed -20;
  return speed;
}


function ScrollBar_draggingCallback(e,obj)
{
  /*var perc;
  var SBW = obj.parentObj.parentObj;

  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);

  SBW.scrollContentTo(perc,false);
  */
}

function ScrollBar_dragEndCallback(e,obj)
{
  var perc;
  var SBW = obj.parentObj.parentObj;

  if(SBW.dir =='h') 
    perc = (SBW.bar.left) /(SBW.box.width - SBW.bar.width);
  else
    perc = (SBW.bar.top) /(SBW.box.height - SBW.bar.height);

  obj.toggle(0);

  SBW.setScrollPercentage(perc);
  
  if(SBW.onScrolling) 
    SBW.onScrolling(perc,this);

  return true;
}

function ScrollBar_dragStartCallback(e,obj)
{
  obj.toggle(1);

  return true;
}

/*---------------------------------------------
ScrollBox callback functions
---------------------------------------------*/

function ScrollBox_MouseUpCallback(e,box)
{
  var bar = box.parentObj.bar;
  bar.slideStop();
  self.barObj = null;
  self.barE = null;
  self.x = null;
  self.y = null;
  self.scrollDir = null;

  clearTimeout(self.barTimeId);
  self.barTimeId = null;
}

function ScrollBox_MouseOutCallback(e,box)
{
  if ( box.eventManager.msdown == true && self.barTimeId ){
    var bar = box.parentObj.bar;
    bar.slideStop();
    self.barObj = null;
    self.barE = null;
    self.x = null;
    self.y = null;

    clearTimeout(self.barTimeId);
    self.barTimeId = null;

  }
}

function ScrollBox_MouseDownCallback(e,box)
{
   self.barObj = null;

   var x = box.lastX = ns4?e.layerX:e.offsetX;
   var y = box.lastY = ns4?e.layerY:e.offsetY;
   var par = box.parentObj;	
   var bar = par.bar;
   if ( par.dir == 'h' ){
     if ( x > bar.left )
       self.scrollDir = 'right';
     else
       self.scrollDir = 'left';
   }else {
     if ( y > bar.top )
       self.scrollDir = 'down';
     else
       self.scrollDir = 'up';
   }     
   ScrollBox_recursiveMouseDown(e,box);
}

function ScrollBox_recursiveMouseDown(e,box)
{
  //window.status = 'mouseup on ' + box.objId;
  if ( self.barObj && self.barE ){
    e = self.barE;
    box = self.barObj;
  }
if( e && box ){
  if ( self.x && self.y && ie4 ){
    x = self.x;
    y = self.y;
  }else {
    var x = box.lastX = ns4?e.layerX:e.offsetX;
    var y = box.lastY = ns4?e.layerY:e.offsetY;
    self.x = x;
    self.y = y;
  }

  var par = box.parentObj;	
  var bar = par.bar;

  if(par.dir =='h') {
    //window.status = x + ' ' + bar.width/2 + bar.left;
    if (x < bar.width/2 + bar.left && self.scrollDir == 'left') {
        par.setValue(par.boundedValue(par.curValue - par.page));
    } else if (x > bar.width/2 + bar.left && self.scrollDir == 'right') {
        par.setValue(par.boundedValue(par.curValue + par.page));
    }
  } else {
    if (y <  bar.height/2 + bar.top && self.scrollDir == 'up') {
      par.setValue(par.boundedValue(par.curValue - par.page));
    } else if( y >  bar.height/2 + bar.top && self.scrollDir == 'down'){ 
      par.setValue(par.boundedValue(par.curValue + par.page));
    }
  }
  self.barObj = box;
  self.barE = e;
}
  self.barTimeId = setTimeout("ScrollBox_recursiveMouseDown()",70);
}

/*  czDrag.js 115.11 2001/05/22 15:09:57 cjeyapra ship $ */

function Drag(name, evtMgr) {
  
  if(name) {
    if(Drag.instances[name]) 
      return(Drag.instances[name]);
    else 
      Drag.instances[name] = this;
  }
  this.obj = null;
  this.dragList = null;
  this.active = false;
  this.zIndex = 0;
  this.reSort = true;
  this.lastX = null;
  this.lastY = null;
  if (evtMgr != null) {
    this.emg = evtMgr;
  } else if(self.EventManager)
    this.emg = new EventManager();
}

function Drag_add() 
{
  for (var i=0; i<arguments.length; i++) {
    if(arguments[i]) {
      if (this.dragList == null) 
        this.dragList = new Array();
      this.dragList[this.dragList.length]= arguments[i];
      arguments[i].resizable = true; //change back to false later;
      arguments[i].draggable = true;
      this.zIndex += 1;
      if(this.active) {
        arguments[i].addListener('mousedownCallback',this,'super');
      }
    }
  }
}


function Drag_remove() 
{
  if(this.dragList) {
    for (var i=0; i<arguments.length; i++) {
      for (var j=0; j<this.dragList.length; j++) {
        if (this.dragList[j]==arguments[i]) {
          if(this.active) {
            arguments[i].removeListener('mousedownCallback',this);
          }
          for (var k=j;k<=this.dragList.length-2;k++) 
            this.dragList[k] = this.dragList[k+1];
          this.dragList[this.dragList.length-1] = null;
          this.dragList[this.dragList.length-1].resizable = false;
          this.dragList[this.dragList.length-1].draggable = false;
          this.dragList.length -= 1;
          break
        }
      }
    }
  }
}

function Drag_off()
{
  if(!this.active) 
    return;

  if(this.dragList) {
    for(var i=0; i<this.dragList.length; i++) {
      if(this.dragList[i]) {
        this.dragList[i].removeListener('mousedownCallback',this);
      }
    }
    this.emg.removeListener('docMouseMove',this);
    this.emg.removeListener('docMouseUp',this);		
    this.active = false;
  }	
}

function Drag_on(emg)
{
  if(this.active) {
    if(emg) {
      if(this.emg != emg) {
        this.emg.removeListener('docMouseMove',this);
        this.emg.removeListener('docMouseUp',this);		
      }
    } else
      return true;
  }
  if(this.dragList) {
    if(emg)
      this.emg = emg;
    for(var i=0; i<this.dragList.length; i++) {
      if(this.dragList[i]) {
        this.dragList[i].addListener('mousedownCallback',this,'super');
      }
    }	
    this.emg.addListener('docMouseMove',this);
    this.emg.addListener('docMouseUp',this);		
    this.active = true;
  }
}

function Drag_removeAll() 
{
  if(this.dragList!=null) {
    for (var i=0; i<this.dragList.length; i++) {
      this.dragList[i] = null;
      this.dragList[i].resizable = false;
      this.dragList[i].draggable = false;
    }
    this.dragList = null;
  }
}

function Drag_setGrab(element,top,right,bottom,left) 
{ 
  element.dragGrab = new Array(top,right,bottom,left)
}


function Drag_lockXPos(block)
{
  block.lock ='x';
}

function Drag_lockYPos(block)
{
  block.lock ='y';
}

function Drag_lockRegion(block,top,right,bottom,left)
{
  block.lock ='region';
  block.lockTop = top;
  block.lockRight = right;
  block.lockBottom = bottom;
  block.lockLeft = left;
}

function Drag_reSort(sort)
{
  this.reSort = sort;
}

function Drag_mousedownCallback(e,obj) 
{
  //if other than left mouse button exit this function;
  if ((ns4&& e.which!=1) || (ie4 && e.button!=1)) 
    return true;

  if(obj.resizable) {
    if(this.resizeStart(e,obj)) {
      if (this.obj.onResizeStart) 
        this.obj.onResizeStart(e,this.obj);
      this.obj.notifyListeners('onResizeStart',e);
      return false;
    }
  }
  if(obj.draggable) {
    if (this.dragStart(e,obj)) {
      if (this.obj.onDragStart) 
        this.obj.onDragStart(e,this.obj);
      this.obj.notifyListeners('onDragStart',e);
      return false;
    }
  }		
  return true;		
}

function Drag_mousemoveCallback(e,obj) 
{
  if (this.obj) {
    if ( this.obj.eventManager.msdown == true )
       this.dragActive = true;
    if(this.resizeActive) {
      this.reSizeMove(e,this.obj);	 
      if (this.obj.onResizing) 
        this.obj.onResizing(e,this.obj);
      this.obj.notifyListeners('onResizing',e);
    }
    if(this.dragActive) {
      this.dragMove(e,this.obj);
      if (this.obj.onDragging) 
        this.obj.onDragging(e,this.obj);
      this.obj.notifyListeners('onDragging',e);
    }
    if(ie4)
      e.returnValue = false;
  }
  return true;
}

function Drag_mouseupCallback(e,obj) 
{
  if(this.obj) {
    if(this.resizeActive) {
      if (this.obj.onResizeEnd) 
        this.obj.onResizeEnd(e,this.obj);
      this.obj.notifyListeners('onResizeEnd',e);
      this.resizeActive = false;
    }
    if(this.dragActive) {
      if (this.obj.onDragDrop) 
        this.obj.onDragDrop(e,this.obj);
      this.obj.notifyListeners('onDragEnd',e);
      this.dragActive = false;
    }else
      this.obj.toggle(0);
    this.obj = null;
    return false;
  }
  return true;
}

function Drag_dragStart(e,obj) 
{
  if(obj.dragGrab) {
    var layerX = (ns4)? e.layerX : e.offsetX;
    var layerY = (ns4)? e.layerY : e.offsetY;

    if(!obj.isInside(layerX,layerY,obj.dragGrab[0],obj.dragGrab[1],obj.dragGrab[2],obj.dragGrab[3]))
      return false;
  }
  var x = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
  var y = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

  this.obj = obj;
  //this.dragActive = true;

  this.lastX = x;
  this.lastY = y;

  if (this.reSort) {
    this.zIndex++;
    if(obj.glue) {
      var glueList = obj.glue.glueArray;
      for(var i=0; i<glueList.length;i++)
        glueList[i].getHTMLObj().zIndex = this.zIndex;
    } else
      obj.getHTMLObj().zIndex = this.zIndex; 
  }
  return true;		
}

function Drag_dragMove(e, obj) 
{
  var x = (ns4)? e.pageX : e.clientX+document.body.scrollLeft;
  var y = (ns4)? e.pageY : e.clientY+document.body.scrollTop;
  var obj = this.obj;
  
  var dx = x- this.lastX;
  var dy = y- this.lastY;
  
  if(obj.lock) {
    switch(obj.lock) {
    case 'x':
      dx =0;
      break;
    case 'y':
      dy =0;
      break;
    case 'region':
      if((obj.left+ dx) < obj.lockLeft) 
        dx = obj.lockLeft- obj.left;
      else if ((obj.left+ obj.width + dx) > obj.lockRight) 
        dx = obj.lockRight - (obj.left+ obj.width);
      if((obj.top + dy) < obj.lockTop) 
        dy = obj.lockTop- obj.top;
      else if((obj.top + obj.height + dy) > obj.lockBottom) 
        dy = obj.lockBottom - (obj.top+ obj.height);
      break;
    }
  }
  obj.moveBy(dx,dy);
  this.lastX = x;
  this.lastY = y;

  return true;
}

function Drag_reSizeMove(e,obj)
{
  var X = (ns4)? e.pageX : e.clientX;
  var Y = (ns4)? e.pageY : e.clientY;
  
  if(this.resizeSide == 2)  {
    if((obj.width+ X-this.lastX)>5) 
      obj.setWidth(obj.width+ X-this.lastX);
  } else if(this.resizeSide == 3) {
    if((obj.height+ Y-this.lastY)>5) 
      obj.setHeight(obj.height+ Y-this.lastY);
  }
  this.lastX = X;
  this.lastY = Y;

  return true;
}

function Drag_resizeStart(e,obj)
{
  var layerX = (ns4)? e.layerX : e.offsetX;
  var layerY = (ns4)? e.layerY : e.offsetY;

  if (obj.isInside(layerX,layerY,0,obj.width,obj.height,0)) {
    if (!obj.isInside(layerX,layerY,0,obj.width-5,obj.height-5,0)) {
      if(layerX>(obj.width-5)) {
        if(obj.lock)
          if(obj.lock == 'x') return false;
        this.resizeSide = 2;
      } else {
        if(obj.lock) 
          if(obj.lock == 'y') return false;
        this.resizeSide = 3;
      }
      this.resizeActive = true;
      this.lastX = (ns4)? e.pageX : e.clientX+ document.body.scrollLeft;
      this.lastY = (ns4)? e.pageY : e.clientY+ document.body.scrollTop;

      this.obj = obj;
      return true;
    }
  }
  return false;
}

function Drag_clearInstance (name)
{
  if (Drag.instances[name]) {
    Drag.instances[name].destroy ();
    Drag.instances[name] = null;
    delete Drag.instances[name];
  }
}

function Drag_destroy ()
{
  if (this.dragList) {
    Utils.clearCollection (this.dragList);
    this.dragList = null;
    delete this.dragList;
  }
}

Drag.clearInstance = Drag_clearInstance;

Drag.instances 			= new Array();
Drag.prototype.add 		= Drag_add;
Drag.prototype.on 		= Drag_on;
Drag.prototype.off 		= Drag_off;
Drag.prototype.remove 		= Drag_remove;
Drag.prototype.removeAll 	= Drag_removeAll
Drag.prototype.setGrab 		= Drag_setGrab;
Drag.prototype.reSort 		= Drag_reSort;
Drag.prototype.lockXPos		= Drag_lockXPos;
Drag.prototype.lockYPos 	= Drag_lockYPos;
Drag.prototype.lockRegion 	= Drag_lockRegion;
Drag.prototype.reSizeMove 	= Drag_reSizeMove;
Drag.prototype.resizeStart 	= Drag_resizeStart;
Drag.prototype.dragStart 	= Drag_dragStart;
Drag.prototype.dragMove 	= Drag_dragMove;
Drag.prototype.mousedownCallback= Drag_mousedownCallback;
Drag.prototype.docMouseUp 	= Drag_mouseupCallback;
Drag.prototype.docMouseMove 	= Drag_mousemoveCallback;
Drag.prototype.destroy = Drag_destroy;


/*  czList.js 115.36 2001/07/06 21:24:45 snadkarn ship $ */

function List ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.cells = new Array();
  this.activeItemsList = new Array();
  this.activeItemsListScroll = new Array();
  this.unWantedItemsList = new Array();
  this.cellsBuilt = false;
  this.className = 'List';
  this.itemBackgroundColor = 'white';
  this.backgroundColor = this.itemBackgroundColor;
  this.activeBackColor = 'lightgrey';
  this.selectedBackColor = 'gray';
  this.cellRenderClass = 'ListItem';
  this.itemHt = 22;
  this.visibleRowCount = 0;
  this.mode = List.MODE_STANDARD;
  this.bLastCellClipped = false;
  this.borderOffset = 1;
  this.sclBarWd = 15;

  this.setModel (new ListModel (ListModel.SELECT_MULTI));

  // currently viewed model item indices;
  this.firstVisibleIndex = 0;
  this.lastVisibleIndex = 0;
}

function List_setModel (listModel)
{
  this.listModel = listModel;
  // register for events;
  this.listModel.addListener ('propertyChangeCallback', this);
  this.listModel.addListener ('objectAddedCallback', this);
  this.listModel.addListener ('objectRemovedCallback', this);
}

function List_buildControl ()
{
  if (this.built) return;

  this.built = true;
  var border = 'black';
  if ( this.noBorder == true)
    border = 'white';
  this.createBorder(this.borderOffset, border);
  this.enableKeyBoardEvents();
}

function List_onLaunchCall()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.updateDone();
  }
}

function List_refreshActiveList()
{
  for (var i=0; i<this.activeItemsList.length; i++)
  {
    this.activeItemsList[i]=this.activeItemsList[i+1];
  }
  this.activeItemsList[i-1]=null;
  this.activeItemsList.length--;
}

function List_updateDone()
{
    Utils.clearCollection(this.activeItemsList);
    Utils.clearCollection(this.activeItemsListScroll);
    if(this.unWantedItemsList.length>0)
    {
      Utils.clearCollection(this.unWantedItemsList);
      this.unWantedWas = true;
    }
    var curActiveItem=null;
  
    for(var i=0; i<this.listModel.items.length; i++)
    {
      curActiveItem=this.listModel.items[i]; 
      if(curActiveItem.getVisibility()==true)
      {
        this.activeItemsList[this.activeItemsList.length]=curActiveItem;
        this.activeItemsListScroll[this.activeItemsListScroll.length]=curActiveItem;
      }else
        this.unWantedItemsList[this.unWantedItemsList.length]=curActiveItem;
    }

    if(this.unWantedItemsList.length>0) 
    {
      this.noChange = false;
      this.needTotalUpdate = false;
    }
    else if(this.activeItemsListScroll.length == this.listModel.items.length && this.unWantedWas)
    {
      this.noChange = true;
      this.needTotalUpdate = true;
    }
    else
    {
      this.noChange = false;
      this.needTotalUpdate = false;
    }
    if(this.activeItemsListScroll.length == this.listModel.items.length && this.needTotalUpdate)
    {
      this.reArrangeItems();
      this.needTotalUpdate = false;
      this.unWantedWas = false;
    }
    else if (this.unWantedItemsList.length > 0 && !this.noChange)
    {
      this.arrangeItems();
      this.noChange=true;
    }
} 

function List_reArrangeItems()
{
  var vScroll = this.getVerticalScrollBarVisibility();
  var visRow = this.visibleRowCount;
  var activeRowCount = this.activeItemsList.length;
  var firstItem = false;

  if(this.activeItemsList.length == this.listModel.items.length)
  {
    for (var i=0; i<this.cells.length; i++)
    {
      if(this.activeItemsList.length>0)
      {
        for(var j=0; j<this.activeItemsList.length; j++)
        {
          var curActiveItem = this.activeItemsList[j];
          this.cells[i].setModelItem(curActiveItem);
          this.refreshActiveList();
          break;
        }
      } else {
         break;
      }
    }
  }
  if(this.getVerticalScrollBarVisibility())
  {
    this.setVerticalScrollBarVisibility(true);  
    if(this.activeItemsListScroll.length == this.listModel.items.length && this.vScroll){
      this.firstVisibleIndex=0;
      var maxVal = (Math.ceil(this.listModel.items.length-this.visibleRowCount)>0)?Math.ceil(this.listModel.items.length-this.visibleRowCount):1;
      this.vScroll.setValues(0,0,maxVal,1,this.visibleRowCount,false);
    }
  }
}

function List_arrangeItems()
{
  var vScroll = this.getVerticalScrollBarVisibility();
  var visRow = this.visibleRowCount;
  var activeRowCount = this.activeItemsList.length;
  var firstItem = false;

  for (var i=0; i<this.cells.length;i++)
  {
    if(this.activeItemsList.length>0)
    {
      for(var j=0; j<this.activeItemsList.length; j++)
      {
        var curActiveItem = this.activeItemsList[j];
        this.cells[i].setModelItem(curActiveItem);
        this.refreshActiveList();
        break;
      }
    }
    else
    {
       this.cells[i].setModelItem(null);
    }
  }

  if(this.getVerticalScrollBarVisibility())
  {

    if(this.activeItemsListScroll.length > this.unWantedItemsList.length)
    {
      if(this.activeItemsListScroll.length < this.visibleRowCount)
        this.setVerticalScrollBarVisibility(false);
      else
        this.setVerticalScrollBarVisibility(true);
    } else
        this.setVerticalScrollBarVisibility(false);

    if(this.activeItemsListScroll.length > this.unWantedItemsList.length && this.vScroll)
    {
      this.firstVisibleIndex=0;
      var maxVal = (Math.ceil(this.activeItemsListScroll.length-this.visibleRowCount)>0)?Math.ceil(this.activeItemsListScroll.length-this.visibleRowCount):1;
      this.vScroll.setValues(0,0,maxVal,1,this.visibleRowCount,false);
    }
  }
}

function List_createScrollBar ()
{
  var sclBarWd = this.sclBarWd;
  // add vertical scrollbar;
  var scroll = this.vScroll = new ScrollBar();
  scroll.setName (this.name + 'VScr');
  scroll.setParentObject (this);
  scroll.setDimensions (this.width-sclBarWd, 0, sclBarWd, this.height);
  scroll.dir = 'v';
  scroll.setZIndex (9999);
  // vertical;
  if (this.listModel.getItemCount() > 0) {
    scroll.setMaxValue (this.listModel.getItemCount() - this.visibleRowCount);
  }
  scroll.setPageSize (this.visibleRowCount);
  scroll.setEventManager (this.eventManager);
  
  //add horizontal scrollbar;
  scroll = this.hScroll = new ScrollBar();
  scroll.setName (this.name + 'HScr');
  scroll.setDimensions (0, this.height-sclBarWd, this.width, sclBarWd);
  scroll.setParentObject (this);
  scroll.dir = 'h';
  scroll.setZIndex (9999);
  scroll.setEventManager (this.eventManager);
  //add scroll bar listeners;
  this.vScroll.addListener ('scrollbarCallback', this);
  this.hScroll.addListener ('scrollbarCallback', this);

  if (this.launched) {
    this.vScroll.runtimeRender (null, this, this.eventManager);
    this.vScroll.setZIndex (9999);
    this.hScroll.runtimeRender (null, this, this.eventManager);
    this.hScroll.setZIndex (9999);
  }
}

function List_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
      //if vertical scroll bar is visible do a hit test on it;
      if (this.vScroll && this.vScroll.isVisible()) {
        if (this.isInside (xpos, ypos, absTop+this.vScroll.top, rt, bt, absLeft+this.vScroll.left))
          return (this.vScroll.box.objId);
      }
      //if horizontal scroll bar is visible do a hit test on it;
      if (this.hScroll && this.hScroll.isVisible()) {
        if (this.isInside (xpos, ypos, absTop+this.hScroll.top, rt, bt, absLeft+this.hScroll.left))
          return (this.hScroll.box.objId);
      }
      //go thru cells and do hit test;
      for (var i=0; i < this.cells.length; i++) {
        var objId = this.cells[i].hitTest(e);
        if (objId)
          return objId;
      }
    }
  }
  return (null);
}

function List_getCellWithModelItem (modelItem)
{
  for (var i = 0; i<this.cells.length; i++) {
    var cell = this.cells[i];
    if (cell.modelItem == modelItem) return cell;
  }
  return null;
}

function List_setCellRenderClass (cls)
{
  if (!this.launched) {
    this.cellRenderClass = cls;
  }
}

function List_getCellRenderClass ()
{
  return this.cellRenderClass;
}

function List_scrollbarCallback (scr)
{
  if (this.cellsBuilt) {
    if (scr.dir == 'v') { 
      //vertical;
      //we could prevent same index rerendering,  but that fools the;
      //fix for bumping up the last modelItem for visibility;
      if (this.launched) {
        var scrVal = scr.getValue();
        this.setFirstVisibleItem (this.listModel.getItemAtIndex(scrVal));
      }
    } else { 
      //horizontal;
      this.adjustIndent (-scr.getValue());
    }
  }
}

function List_adjustIndent (pos) 
{
  var maxIndex = this.cells.length;
  for (var i = 0; i < maxIndex; i++) {
    this.cells[i].setLeft (pos);
  }
}

function List_setFirstVisibleItem (item)
{
  var i = 0;
  var prevCell = null;
  var curCell = null;
  var lastCell = null;
  var reorderedCells = new Array();
  var index = 0;
  var modelLength = 0;
  var lastCellClipped = false;
  
  //grab the last view cell and set its property last cell boolean to false;
  this.cells[this.visibleRowCount-1].bLastCell = false;

  //grab the length of the model,  so we can use it to bounds check in the algorithm;
  if(this.activeItemsListScroll.length>0)
    modelLength=this.activeItemsListScroll.length;
  else
    if (this.listModel) {
      modelLength = this.listModel.getItemCount();
  }
  //if item is an integer,  we will scroll by that amount positive or negative;
  //if item is an object,  we will scroll to that objects index;
  if (isNaN(item)) {
    index = item.index;
  } else {
    index = this.firstVisibleIndex + item;
  }
  var offset = index - this.firstVisibleIndex;
  if (offset > 0 && offset < this.visibleRowCount) {
    //scroll down;
    //shift up all pre-rendered items;
    var j = 0;
    for (i = offset; i <= this.visibleRowCount; i++) {
	curCell = this.cells[j];
      // recover the item height for a clipped cell;
      if (curCell.listBorderClipped) {
        curCell.setHeight (this.itemHt);
        curCell.listBorderClipped = false;
      }
      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[this.firstVisibleIndex+i]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(this.firstVisibleIndex+i));
        lastCell = curCell;
      } else {
        curCell.setModelItem (null);
      }
	j++;
    }
    
    //render whats left;
    prevCell = curCell;	
    var adjust = index + (this.visibleRowCount - offset);
    for (i = 0; i < offset; i++) {
     	curCell = this.cells[this.cells.length-1];

      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[adjust + i]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(adjust + i));
        lastCell = curCell;
      } else {
        curCell.setModelItem (null);
      }
    }  
  } else if (offset < 0 && Math.abs(offset) < this.visibleRowCount) {
    offset = Math.abs(offset);
    //scroll up;
    var j = 0;
    var adjust = (this.visibleRowCount - offset);
    for (i = 0; i < this.visibleRowCount; i++) {
      curCell = this.cells[i];
      if (i < modelLength) {
        if(this.activeItemsListScroll.length>0)
          curCell.setModelItem (this.activeItemsListScroll[index+j]);
        else
          curCell.setModelItem (this.listModel.getItemAtIndex(index+j));
      } else {
        curCell.setModelItem (null);
      }
      // recover the item height for a clipped cell;
      if (curCell.listBorderClipped) {
        curCell.setHeight (this.itemHt);
        curCell.listBorderClipped = false;
      }			
      j++;
    }
  } else {
    //just render everything...;
    for (i = 0; (i < this.visibleRowCount) && (i < modelLength); i++) {
      curCell = this.cells[i];	
      if(this.activeItemsListScroll.length>0)
        curCell.setModelItem (this.activeItemsListScroll[i + index]);
      else
        curCell.setModelItem (this.listModel.getItemAtIndex(i + index));
    }
  }
  this.firstVisibleIndex = index;
  if (curCell.modelItem != null) {
    this.lastVisibleIndex = curCell.modelItem.index;
    this.adjustLastCellHeight ();
  } else {
    this.lastVisibleIndex = 0;
  }
  
  //if the horizontal bar is in the way,  move up another cell;
  if (isNaN(item)) { 
    //no adjustment in this case;
    if ((this.lastVisibleIndex == (modelLength-1))  &&  (this.firstVisibleIndex != 0) &&
        ((this.hScroll.isVisible() && this.bLastCellClipped ) || lastCellClipped || this.bLastCellClipped)) {
      this.setFirstVisibleItem (1); //scroll down 1
    }
  }
}

function List_updateScrollBarVisibility()
{
  if ((! this.vScroll) && (! this.hScroll)) {
    if (this.getVerticalScrollBarVisibility() || this.getHorizontalScrollBarVisibility()) {
      //create scroll bar;
      this.createScrollBar ();
    }
  }
  if (this.vScroll && this.hScroll) {
    var bDrawnCorner = (this.vScroll.cornerLength > 0);
    //vertical;
    if (this.getVerticalScrollBarVisibility()) {
      this.vScroll.show();
    } else {
      this.vScroll.hide();
    }
    //horizontal;
    if (this.getHorizontalScrollBarVisibility()) {
      this.hScroll.show();
    } else {
      this.hScroll.hide();
    }
    if (!bDrawnCorner && (this.hScroll.isVisible() && this.vScroll.isVisible())) {
      this.vScroll.drawCorner (this.hScroll.height);
      this.hScroll.setWidth (this.width - this.vScroll.width);
    } else if (bDrawnCorner && (! this.hScroll.isVisible())) {
      this.vScroll.drawCorner (0);
    }
  }
}

function List_updateVerticalScrollValues () {
  if (this.listModel.getItemCount() < this.visibleRowCount) 
    return;
  if (this.vScroll) {
    this.vScroll.setMaxValue (this.listModel.getItemCount() - this.visibleRowCount);
    if (this.cellsBuilt) {	
      this.vScroll.setPageSize (this.visibleRowCount);
    }
  }
}

function List_updateHorizontalScrollValues () {
  if (this.vScroll && this.hScroll) {
    var wd = (this.vScroll.isVisible()) ? this.vScroll.width : 0;
    this.hScroll.setMaxValue ((Math.abs(this.cells[0].width - this.width) + wd));
  }
}

function List_setVerticalScrollBarVisibility(show)
{
  if(show)
  {
    if ( this.vScroll )
    { 
      this.vScroll.show();
      this.vScroll.setZIndex(9999);
      this.vScroll.arrow1.show();
      this.vScroll.arrow2.show();
      this.vScroll.box.show();
      this.vScroll.bar.show();
      if(this.getHorizontalScrollBarVisibility())
      {
        this.hScroll.setWidth(this.hScroll.oldWidth);
      }
    }
  }
  else
  {
    if ( this.vScroll )
    { 
      this.vScroll.arrow1.hide();
      this.vScroll.arrow2.hide();
      this.vScroll.box.hide();
      this.vScroll.bar.hide();
      this.vScroll.hide();
      this.vScroll.setZIndex(0000);
      if(this.getHorizontalScrollBarVisibility())
      {
        this.hScroll.oldWidth = this.hScroll.getWidth();
        var diffWidth = this.getWidth()-this.hScroll.oldWidth;
        this.hScroll.setWidth(this.getWidth());
      }
    }
  }
}

function List_getVerticalScrollBarVisibility()
{
  if (! this.built) return false;
  
  if (this.listModel && this.cellsBuilt) {
    var count = this.listModel.getItemCount();
    if ((count > this.visibleRowCount) || ((count == this.visibleRowCount) &&
                                           this.bLastCellClipped)) {
      return true;
    }
  }
  return false;
}

function List_getHorizontalScrollBarVisibility()
{
  if (! this.built) return false;

  return ((this.cells.length > 0) && (this.cells[0].width > this.width));
}

function List_propertyChangeCallback(change)
{
  this.notifyListeners('objectPropertyChangeCallback', change, this);
  return true;
}

function List_objectAddedCallback (object)
{
  if (this.cellsBuilt) {
    this.updateScrollBarVisibility();
    this.updateVerticalScrollValues ();
    this.updateHorizontalScrollValues ();
  }
  this.notifyListeners ('objectAddedCallback', object, this);
  return true;
}

function List_objectRemovedCallback (object)
{
  this.notifyListeners ('objectRemovedCallback', object, this);
  return true;
}

function List_objectPropertyChangeCallback (change, eSrc)
{
  return true;
}

function List_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  this.listModel.addItem (rtid, item, index, isVisible, isSelected);
  
  item.addListener ('propertyChangeCallback', this);
  
  return (item);
}

function List_removeItem (index)
{
  this.listModel.removeItemAtIndex (index);
}

function List_removeAll ()
{
  this.listModel.removeAll ();
}

function List_moreSetWidth (wd)
{
  for (var i=0; i<this.cells.length; i++)
    this.cells[i].setWidth (wd);
  
  if (this.vScroll) {
    this.vScroll.setLeft (this.width - this.vScroll.width);
  }
  if (this.hScroll) {
    this.hScroll.setWidth (this.width);
  }
  this.updateScrollBarVisibility();
}

function List_moreSetHeight(ht)
{
  if (this.vScroll) {
    this.vScroll.setHeight (this.height);
  }
  if (this.hScroll) {
    this.hScroll.setTop (this.height - this.hScroll.height);
  }
  this.updateScrollBarVisibility();
}

function List_setBackgroundColor (col)
{
  if(col!=null) {
    this.backgroundColor = col;
    this.itemBackgroundColor = col;
    
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
    
    for (var i=0; i<this.cells.length; i++) {
      this.cells[i].setBackgroundColor (col);
    }
  }
}

function List_getBackgroundColor (col)
{
  return (this.itemBackgroundColor);
}

function List_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function List_getActiveBackColor ()
{
  return (this.activeBackColor);
}

function List_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function List_getSelectedBackColor ()
{
  return (this.selectedBackColor);
}

function List_getVisibleRowCount ()
{
  return (this.visibleRowCount);
}

function List_isMultiSelect ()
{
  return (this.listModel.getSelectionMode() == ListModel.SELECT_MULTI);
}

function List_setMultiSelect (bVal)
{
  if (bVal) {
    this.listModel.setSelectionMode (ListModel.SELECT_MULTI);
  } else {
    this.listModel.setSelectionMode (ListModel.SELECT_SINGLE);
  }
}

function List_getSelectedIndex ()
{
  return this.listModel.getSelectedItems();
}

function List_setSelectedIndex (index)
{
  this.listModel.selectItem (index);
}

function List_getSelectedItem ()
{
  return this.listModel.getSelectedItems();
}

function List_getSelectedCount () {
  return this.listModel.getSelectedCount();
}

function List_getItemCount ()
{
  return this.listModel.getItemCount ();
}

function List_getItemAtIndex (index)
{
  return this.listModel.getItemAtIndex (index);
}

function List_generateCSS ()
{
  if (! this.built) {
    this.buildControl ();
    this.buildCellRenderers ();
  }
  var CSSStr = "";

  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";

  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";

  if(this.height != null)
    CSSStr += "height:" + this.height + "; ";
      
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height) 
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";
  
  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";
  
  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  
  CSSStr += "}";

  if (this.border) {
    CSSStr += '\n' + this.border.generateCSS ();
  }
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();
  
  return(CSSStr);
}

function List_moreCSS()
{
  var CSSStr = '';
  if ((navigator.platform.indexOf('MacPPC') != -1 ) || (this.mode == List.MODE_STANDARD)) {
    this.fixDimensions ();
  }
  for (var i=0; i < this.cells.length; i++) {
    CSSStr += this.cells[i].generateCSS() + "\n";
  }
  if (this.vScroll)
    CSSStr += this.vScroll.generateCSS();
  if (this.hScroll)
    CSSStr += this.hScroll.generateCSS();
  if (this.unSatIcon)
    CSSStr += this.unSatIcon.generateCSS () + '\n';
  return CSSStr;
}

function List_render()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.name != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    this.addListener ('onKeyDownCallback', this);
    this.addListener ('onKeyUpCallback', this);
    this.addListener ('onFocusCallback', this);
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
    sBuffer += ' onfocus="javascript:doOnFocus(null,\''+ this.self + '\')"';
  }
  sBuffer +='>';

  for (var i=0; i < this.cells.length; i++) {
    sBuffer += this.cells[i].render () + '\n';
  }
  if (this.vScroll)
    sBuffer += this.vScroll.render ();
  if (this.hScroll)
    sBuffer += this.hScroll.render ();
  sBuffer += '</DIV>' + '\n';
  
  if (this.border)
    sBuffer += '</DIV>' + '\n';

  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';

  return (sBuffer);
}

function List_buildCellRenderers()
{
  if (this.visibleRowCount == 0) {
    this.visibleRowCount = Math.ceil(this.height / this.itemHt);
  } else {
    var count = this.listModel.getItemCount ();
    if ((count > 0) && (count < this.visibleRowCount)) {
      this.visibleRowCount = count;
    } else if ((count == 0) && (this.mode == List.MODE_DROPDOWN)) {
      this.visibleRowCount = 1;
    }
    this.setHeight (this.visibleRowCount * this.itemHt + 2);
  }
  
  var cell;
  var modelItem;
  //Cache the column Descriptor and set this one to all the List Items;
  cell = eval('new ' + this.cellRenderClass + '();');
  this.columnDescriptors = cell.getColumnDescriptors ();
  this.colDescHash = cell.getColumnDescriptorsHash ();

  for (var i = 0; i < this.visibleRowCount; i++) {
    cell = eval('new ' + this.cellRenderClass + '();');
    cell.setName(this.name + 'cell' + i);
    cell.setColumnDescriptors (this.columnDescriptors, this.colDescHash);
    cell.setParentObject(this);
    if (ns4) {
      cell.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    } else if (ie4) {
      cell.setReference (this.reference);
    }
    cell.setDimensions (0, this.itemHt*i, this.width, this.itemHt);
    cell.setBackgroundColor (this.itemBackgroundColor);
    cell.setActiveBackColor (this.activeBackColor);
    cell.setSelectedBackColor (this.selectedBackColor);
    cell.setSpanClass (this.spanClass);
    cell.setColor (this.color);
    cell.listBorderClipped = false;
    cell.index = i;
    this.cells[i] = cell;
    modelItem = this.listModel.getItemAtIndex(i);
    cell.setModelItem(modelItem);
    cell.addListener ('actionMouseUp', this);
  }
  // we only do this ONCE;
  this.cellsBuilt = true;
}

function List_moreLaunch (em)
{	
  var ln = this.cells.length;
  for (var i = 0; i < ln; i++) {
    this.cells[i].launch (em);
  }
  if (this.vScroll)
    this.vScroll.launch (em);
  if (this.hScroll)
    this.hScroll.launch (em);
  if (this.unSatIcon)
    this.unSatIcon.launch ();
}

function List_fixDimensions ()
{
  var itemLen = this.cells.length;
  var cell = null;
  if (itemLen) {
    var colDescs = this.columnDescriptors;
    this.maxDataDescriptors = this.listModel.getMaxColumnDataDescriptors (colDescs);
    
    //determine perferred dimensions for each column;
    this.updatePreferredWidth();

    //determind available dimensions for each column;
    this.updateColumnPosition ();
    
    //update list item dimension and positions;
    this.fixItemDimensions ();
  }
  this.updateScrollBarVisibility();
  this.updateVerticalScrollValues();
  this.updateHorizontalScrollValues();
  
  if (this.mode == List.MODE_DROPDOWN) {
    //adjust the height of the list to fit the visible count;
    if (this.getHorizontalScrollBarVisibility ()) {
      this.setHeight ((this.visibleRowCount * this.itemHt + 2) + this.hScroll.height);
    } else {
      this.setHeight (this.visibleRowCount * this.itemHt + 2);
    }
  } else if (this.mode == List.MODE_STANDARD) {
    //calculate the max visible cell count,  leave any overlapping cells as empty regions;
    var maxCount, listHt;
    if (this.getHorizontalScrollBarVisibility ()) {
      listHt = this.height - this.hScroll.height;
      maxCount = Math.ceil (listHt / this.itemHt);
    } else {
      listHt = this.height;
      maxCount = Math.ceil (listHt / this.itemHt);
    }
    if (maxCount < itemLen) {
      this.visibleRowCount = maxCount;
      for (var i=maxCount; i < itemLen; i++) {
        this.cells[i].hide();
        this.cells[i] = null;
        delete this.cells[i];
      }
      this.cells.length = maxCount;
    }
    this.adjustLastCellHeight ();
    //doubly link each view items in the list;
    for (var i=0; i<maxCount; i++) {
      if (i==0){
        this.cells[i].prevItem = this.cells[maxCount-1];
        this.cells[i].nextItem = this.cells[i+1];
      } else if (i==(maxCount-1)){
        this.cells[i].prevItem = this.cells[i-1];
        this.cells[i].nextItem = this.cells[i-(maxCount-1)];
      } else{
        this.cells[i].prevItem = this.cells[i-1];
        this.cells[i].nextItem = this.cells[i+1];
      }
    }
  }
  this.updateScrollBarVisibility();
  this.updateVerticalScrollValues();
}

function List_fixItemDimensions ()
{
  var itemLen = this.cells.length;
  //set the itemHeight and reposition to the right value;
  if (this.itemHt < this.cellPrefHeight) {
    this.itemHt = this.cellPrefHeight;
    for (var j=0; j<itemLen; j++) {
      this.cells[j].setTop (this.itemHt * j);
      this.cells[j].setHeight(this.itemHt);
    }
  }
  //set the width of each list item;
  if (this.cells[0].width < this.cellPrefWidth) {
    for (var j=0; j<itemLen; j++) {
      this.cells[j].setWidth (this.cellPrefWidth);
    }
  }
  for (var i = 0; i < itemLen; i++) {
    this.cells [i].fixDimensions ();
  }
}

function List_adjustLastCellHeight ()
{
  var lastIndex = this.visibleRowCount - 1;
  var cell = this.cells[lastIndex];
  var listHt;
  if (this.getHorizontalScrollBarVisibility ()) {
  //if (this.hScroll.isVisible ()) {
    listHt = this.height - this.sclBarWd;
  } else {
    listHt = this.height;
  }
  cell.setHeight (listHt - (this.itemHt * lastIndex));

  if (cell.height != this.itemHt) {
    //last cell is clipped;
    this.bLastCellClipped = true;
    cell.listBorderClipped = true;
  }
  cell.bLastCell = true;
}

function List_selectItem (index)
{
  if (this.listModel != null) {
    var litem = this.listModel.getItemAtIndex (index);
    if (litem != null) {
      litem.setSelected (true);
    }
  }
}

function List_deSelectItem (index)
{
  if (this.listModel != null) {
    var litem = this.listModel.getItemAtIndex (index);
    if (litem != null) {
      litem.setSelected (false);
    }
  }
}

function List_setZIndex (index)
{
  this.zIndex = index;
  if (this.border)
    this.border.setZIndex (index);
  
  if (this.launched)
    this.getHTMLObj ().zIndex = index;
  
  //walk thru the list of item and make it visible;
  for (var i=0; i < this.cells.length; i++) {
    this.cells[i].setZIndex (index);
  }
}

function List_show (zIndex)
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }		
  //walk thru the list of item and make it visible;
  for (var i=0; i<this.cells.length; i++) {
    this.cells[i].show();
  }
}

function List_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;
  
  this.font = fnt;
  
  this.spanClass = this.name + 'SpnNml';
  
  style = this.getStyleObject (this.spanClass, true);
  this.setFontAttributes (style);
}

function List_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
  stylObj.setAttribute ('padding-left', 5);
  //stylObj.setAttribute ('padding-right', 5);
}

function List_setPaddingAttributes (stylObj, pad)
{
  stylObj.setAttribute ('padding-left', pad);
  stylObj.setAttribute ('padding-right', pad);
}

function List_setSpanClass (cls)
{
  this.spanClass = cls;
}

function List_setVisibleRowCount (num) {
  if (this.launched) return;
  this.visibleRowCount = num;
}

function List_getVisibleRowCount () {
  return this.visibleRowCount;
}

function List_actionMouseUp (modelItem, modelView)
{
  this.setFocussedItem (modelView, 1);
  this.notifyListeners ('actionMouseUp', modelItem, modelView);
  return true;
}

function List_actionOnChange (modelItem, modelView)
{
  this.notifyListeners ('actionOnChange', modelItem, modelView);
  return true;
}

function List_isMouseInside (xpos, ypos)
{
  var rt = this.getLeft() + this.getWidth();
  var bt = this.getTop() + this.getHeight();
  if ((xpos >= this.getLeft()) && (xpos <= rt) && (ypos >= this.getTop()) 
      && (ypos <= bt)) {
    return (true);
  } else 
    return (false);
}

function List_createBorder (borderWidth, borderColor)
{
  if (this.launched) return;
  
  if (borderWidth != null)
    this.borderWidth = borderWidth;
  else
    this.borderWidth = 1;
  
  if (borderColor != null)
    this.borderColor = borderColor;
  else if (this.color)
    this.borderColor = this.color
  else
    this.borderColor = 'black';
  
  if (ns4) {
    this.bBorder = true;
    this.borderVisible = true;
  }
  this.border = new Base ();
  this.border.setName ('BDR-' + this.name);
  this.border.setBackgroundColor (this.borderColor);
  if (! this.visibility) {
    this.border.visibility = false;
  }
  this.visibility = true;
  
  //resize this controls diemensions and properties;
  this.setDimensions (this.left, this.top, this.width, this.height);
  this.setParentObject (this.parentObj);
  this.setReference (this.reference);
}

function List_updatePreferredWidth ()
{
  // find the preferred pixel dimensions;
  var dataDesc = null;
  var colDesc = null;
  var descsLen = this.maxDataDescriptors.length;
  var prefWidth = 0;
  var prefHeight = 0;
  var charWidth, charHeight;
  var totWidth = 0, maxHt = 0;
  
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  for (var i = 0; i < descsLen; i++) {
    dataDesc = this.maxDataDescriptors[i];
    colDesc = this.columnDescriptors[i];
    if (colDesc.name == 'text') {
      prefWidth = dataDesc.width * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else if (colDesc.name == 'descImg') {
      var prefSize = Utils.getImageSize (dataDesc.value);
      if (prefSize == null) {
        prefSize = new Array (colDesc.defWidth, colDesc.defHeight);
        //prefSize = new Array (103, 29);
      }
      prefWidth = prefSize [0];
      prefHeight = prefSize [1];
      //change the value to some realistic one if the image height is large;
      if (prefHeight > this.height/2)
        prefHeight = 22;
    } else if (colDesc.name == 'state') {
      prefWidth = 20;
      prefHeight = 16;
    } else if (colDesc.name == 'atp') {
      prefWidth = 10 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight; 
    } else if (colDesc.name == 'price') {
      prefWidth = 8 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else if (colDesc.name == 'count') {
      prefWidth = 5 * charWidth + colDesc.pdgLeft;
      prefHeight = charHeight;
    } else {
      prefWidth = 100;
      prefHeight = this.height;
    }
    colDesc.setPrefWidth (prefWidth);
    colDesc.setPrefHeight (prefHeight);
    totWidth += prefWidth;
    if (maxHt < prefHeight) {
      maxHt = prefHeight;
    }
  }
  this.cellPrefWidth = totWidth;
  this.cellPrefHeight = maxHt;
}

function List_updateColumnPosition ()
{
  var colDesc = null;
  var len = this.columnDescriptors.length;
  if (! this.springColDesc) {
    for (var i=0; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.spring) {
        this.springColDesc = colDesc;
      }
    }
  }

  var totWidth = 0;
  var totWidthUpToSpringCell =0;
  var springColDesc = this.springColDesc;

  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors[i];
    if (colDesc.defVisible) {
      if (colDesc == springColDesc) {
        totWidthUpToSpringCell = totWidth;
      }
      colDesc.setLeft (totWidth);
      totWidth += colDesc.getPrefWidth ();
    }
  }
  if (this.width > this.cellPrefWidth) {
    springColDesc.setAvailWidth (springColDesc.getPrefWidth() + (this.width - this.cellPrefWidth));

    //now shift everything else from spring cell;
    totWidth = totWidthUpToSpringCell + springColDesc.getAvailWidth ();
    for (var i=springColDesc.index + 1; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.defVisible) {
        colDesc.setLeft (totWidth);
      }
      totWidth += colDesc.getAvailWidth ();
    }
  } else {
    //use the preferred with for the spring cell as available widht;
    springColDesc.setAvailWidth (springColDesc.getPrefWidth ());
  }
}

function List_showColumn (colName)
{
  var colDesc = this.colDescHash [colName];
  if (colDesc && (! colDesc.defVisible)) {
    colDesc.defVisible = true;
    this.updateColumnPosition ();
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].showColumn (colName);
    }
  }
}

function List_hideColumn (colName)
{
  var colDesc = this.colDescHash [colName];
  if (colDesc && colDesc.defVisible) {
    colDesc.defVisible = false;
    this.updateColumnPosition ();
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].hideColumn (colName);
    }
  }
}

function List_moreDestroy ()
{
  if (this.listModel) {
    this.listModel.destroy ();
    this.listModel = null;
    delete this.listModel;
  }
  if (this.cells) {
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].destroy ();
      
      this.cells[i] = null;
      delete this.cells[i];
    }
    this.cells.length = 0;
    this.cells = null;
    delete this.cells;
  }
  if (this.vScroll) {
    this.vScroll.destroy ();
    this.vScroll = null;
    delete this.vScroll;
  }
  if (this.hScroll) {
    this.hScroll.destroy ();
    this.hScroll = null;
    delete this.hScroll;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
  Utils.clearCollection (this.columnDescriptors);
  Utils.clearCollection (this.colDescHash);

  this.compMgr = null;
  delete this.compMgr;

  this.evtMgr = null;
  delete this.evtMgr;
}

function List_setEventManager (em)
{
  this.eventManager = em;
}

function List_onKeyDownCallback(e, eSrc)
{
  var key = e.keyCode;
  switch (key) {
  case 9:
    var itm = this.focViewItem;    
    if (e.shiftKey) {
      if (itm == null) {
        return;
      }
      //move focus backwards
      if (this.focCellInd == 1) {
        this.focCellInd = this.numFocusPerItem;
        if (itm.index == 0) {
          var success = false;
          //we reached the top visible cell we may have to scroll up
          if (this.vScroll && this.vScroll.isVisible()) {
            success = this.vScroll.scrollUp();
          }
          if (! success) {
            this.focCellInd = 1;
            this.eventManager.focusBackward(e, this);
            return;
          }
        } else
          this.focViewItem = this.cells[itm.index-1];
      } else
        this.focCellInd--;
    } else {
      //move focus forward
      if (itm == null) {
        this.focViewItem = this.cells[0];
        this.focCellInd = 1;
      } else if (this.focCellInd == this.numFocusPerItem) {
        this.focCellInd = 1;
        if (itm.index == this.cells.length-1) {
          var success = false;
          //we reached the last visible cell we may have to scroll down
          if (this.vScroll && this.vScroll.isVisible()) {
            success = this.vScroll.scrollDown();
          }
          if (! success) {
            this.focCellInd = this.numFocusPerItem;
            this.eventManager.focusForward(e, this);
            return;
          }
        } else
          this.focViewItem = this.cells[itm.index+1];
      } else
        this.focCellInd++;
    }
    if (this.focViewItem.getModelItem())
      this.focViewItem.setFocus(this.focCellInd);
    else {
      this.focViewItem = this.cells[this.focViewItem.index - 1];
      this.focCellInd = this.numFocusPerItem;
      this.eventManager.focusForward(e, this);
      return;
    }
    //e.cancelBubble = true;
    e.returnValue = false;
    break;
  case 37:
  case 39:
    if (this.hScroll && this.hScroll.isVisible()) {
      Arrow_onKeyDownCallback (e, this.hScroll);
    }
    break;
  case 38:
  case 40:
    if (this.vScroll && this.vScroll.isVisible()) {
      Arrow_onKeyDownCallback (e, this.vScroll);
    }
    break;
  }
}

function List_onKeyUpCallback(e, eSrc)
{
  var key = (ns4)?e.which : e.keyCode;
  switch (key) {
  case 37:
  case 39:
    if (this.hScroll && this.hScroll.isVisible()) {
      Arrow_onKeyUpCallback (e, this.hScroll);
    }
    break;    
  case 38:
  case 40:
    if (this.vScroll && this.vScroll.isVisible()) {
      Arrow_onKeyUpCallback (e, this.vScroll);
    }
  }
}

function List_setFocussedItem (vItm, cellInd)
{
  this.focViewItem = vItm;
  this.numFocusPerItem = this.focViewItem.getMaxNumFocusCells();
  if (! cellInd) {
    //assign the max cell index
    this.focCellInd = this.numFocusPerItem;
  } else
    this.focCellInd = cellInd;

  this.focViewItem.setFocus(this.focCellInd);
}

function List_onFocusCallback(e, eSrc)
{
  if (! this.focViewItem) {
    //move the focus to the first list item
    this.focViewItem = this.cells[0];
  }
  if (! this.focCellInd)
    this.focCellInd = 1;
  this.focViewItem.setFocus(this.focCellInd);

  this.numFocusPerItem = this.cells[0].getMaxNumFocusCells();
  e.returnValue = true;
  return true;
}

function List_resetFocus ()
{
  this.focViewItem = this.cells[0];
  this.focCellInd = 1;
}

HTMLHelper.importPrototypes(List, Base);

//public methods
List.prototype.refreshActiveList = List_refreshActiveList;
List.prototype.setVerticalScrollBarVisibility = List_setVerticalScrollBarVisibility;
List.prototype.arrangeItems = List_arrangeItems;
List.prototype.reArrangeItems = List_reArrangeItems;
List.prototype.updateDone = List_updateDone;
List.prototype.onLaunchCall = List_onLaunchCall;
List.prototype.moreSetWidth = List_moreSetWidth;
List.prototype.moreSetHeight = List_moreSetHeight;
List.prototype.setZIndex = List_setZIndex;
List.prototype.hitTest = List_hitTest;
List.prototype.createBorder = List_createBorder;
List.prototype.addItem = List_addItem;
List.prototype.removeItem = List_removeItem;
List.prototype.removeAll = List_removeAll;
List.prototype.setBackgroundColor = List_setBackgroundColor;
List.prototype.getBackgroundColor = List_getBackgroundColor;
List.prototype.setActiveBackColor = List_setActiveBackColor;
List.prototype.getActiveBackColor = List_getActiveBackColor;
List.prototype.setSelectedBackColor = List_setSelectedBackColor;
List.prototype.getSelectedBackColor = List_getSelectedBackColor;
List.prototype.show = List_show;
List.prototype.moreLaunch = List_moreLaunch;
List.prototype.selectItem = List_selectItem;
List.prototype.deSelectItem = List_deSelectItem;
List.prototype.isMultiSelect = List_isMultiSelect;
List.prototype.setMultiSelect = List_setMultiSelect;	
List.prototype.getSelectedIndex = List_getSelectedIndex;
List.prototype.setSelectedIndex = List_setSelectedIndex;
List.prototype.getSelectedItem = List_getSelectedItem;
List.prototype.getSelectedCount = List_getSelectedCount;
List.prototype.getItemCount = List_getItemCount;
List.prototype.setCellRenderClass = List_setCellRenderClass;
List.prototype.getCellRenderClass = List_getCellRenderClass;
List.prototype.setVisibleRowCount = List_setVisibleRowCount;
List.prototype.getVisibleRowCount = List_getVisibleRowCount;
List.prototype.setFirstVisibleItem = List_setFirstVisibleItem;
List.prototype.setFont = List_setFont;
List.prototype.setSpanClass = List_setSpanClass;
List.prototype.isMouseInside = List_isMouseInside;
List.prototype.generateCSS = List_generateCSS;
List.prototype.render = List_render;
List.prototype.setEventManager = List_setEventManager;
List.prototype.setModel = List_setModel;

//Protected Methods
List.prototype.updatePreferredWidth = List_updatePreferredWidth;
List.prototype.updateColumnPosition = List_updateColumnPosition;
List.prototype.showColumn = List_showColumn;
List.prototype.hideColumn = List_hideColumn;
List.prototype.moreDestroy = List_moreDestroy;

//private methods
List.prototype.scrollbarCallback = List_scrollbarCallback;
List.prototype.propertyChangeCallback = List_propertyChangeCallback;
List.prototype.objectPropertyChangeCallback = List_objectPropertyChangeCallback;
List.prototype.objectAddedCallback = List_objectAddedCallback;
List.prototype.objectRemovedCallback = List_objectRemovedCallback;
List.prototype.getCellWithModelItem = List_getCellWithModelItem;
List.prototype.buildCellRenderers = List_buildCellRenderers;
List.prototype.updateScrollBarVisibility = List_updateScrollBarVisibility;
List.prototype.updateVerticalScrollValues = List_updateVerticalScrollValues
List.prototype.updateHorizontalScrollValues = List_updateHorizontalScrollValues
List.prototype.moreCSS = List_moreCSS;
List.prototype.setFontAttributes = List_setFontAttributes;
List.prototype.getVerticalScrollBarVisibility = List_getVerticalScrollBarVisibility;
List.prototype.getHorizontalScrollBarVisibility = List_getHorizontalScrollBarVisibility;
List.prototype.adjustIndent = List_adjustIndent;
List.prototype.fixDimensions = List_fixDimensions;
List.prototype.fixItemDimensions = List_fixItemDimensions;
List.prototype.adjustLastCellHeight = List_adjustLastCellHeight;
List.prototype.buildControl = List_buildControl;
List.prototype.createScrollBar = List_createScrollBar;
List.prototype.setFocussedItem = List_setFocussedItem;
List.prototype.resetFocus = List_resetFocus;

//Event Handlers
List.prototype.actionMouseUp = List_actionMouseUp;
List.prototype.actionOnChange = List_actionOnChange;
List.prototype.onKeyDownCallback = List_onKeyDownCallback;
List.prototype.onKeyUpCallback = List_onKeyUpCallback;
List.prototype.onFocusCallback = List_onFocusCallback;

//list mode constants
List.MODE_STANDARD = 0;
List.MODE_DROPDOWN = 1;



/*  czLMod.js 115.9 2000/11/30 19:54:59 cjeyapra ship $ */
 
function ListModel (selectionMode)
{
  this.type = 'ListModel';
  this.items = new Array();
  this.maxColWidth = null;
  this.selectionMode = selectionMode;
}

function ListModel_setSelectionMode (sel)
{
  if (sel != this.selectionMode) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'SelectionMode', value:sel})) {
      this.selectionMode = sel;
    }	
  }	
}

function ListModel_getSelectionMode ()
{
  return this.selectionMode;
}

function ListModel_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  if (isVisible == null) {
    isVisible = true;
  }
  if (isSelected == null) {
    isSelected = false; 
  }
  item.setVisible (isVisible);
  item.setSelected (isSelected);
  
  this.items[this.items.length] = item;
  item.index = this.items.length - 1;
  //TODO: Resequence the items array if index is specified;
    
  item.addListener ('propertyChangeCallback', this, 'super');
  
  this.notifyListeners ('objectAddedCallback', item);
}

function ListModel_propertyChangeCallback (change)
{
  var ret = true;
  if (change.property == 'selected') {
    if (change.value) {
      switch (this.selectionMode) {
      case (2): //none
        ret = false;
        break;
      case (0): //multi
        ret = true;
        break;
      case (1): //single
        this.clearSelection();
        ret = true;
        break;
      }
    } else {
      ret = true;
    }
  }
  return ret;
}

function ListModel_clearSelection()
{
  var item = null;
  for (var i = 0; i < this.items.length; i++) {
    item = this.items[i];
    if (item.isSelected()) item.setSelected (false);
  }
}

function ListModel_removeItem (key)
{
  this.notifyListeners('objectRemovedCallback', null);
}

function ListModel_removeItemAtIndex (index)
{

  this.notifyListeners('objectRemovedCallback', null);
}

function ListModel_getItemCount ()
{
  return this.items.length;
}

function ListModel_getMaxColumnDataDescriptors (colDescs)
{
  var colName;
  var dataDescs = new Array ();
  var descsLen = colDescs.length;
  if (this.maxColWidth) {
    for (var k = 0; k < descsLen; k++) {
      colName = colDescs[k].name;
      dataDescs[k] = new ColumnDataDescriptor (colName, null, this.maxColWidth[colName]);
      if (colName == 'descImg') {
        //put one image src to calculate pref width and height;
        if (this.items.length > 0) {
          dataDescs[k].value = this.items[0].getDescImage();
        } 
      }
    }
  } else {
    var maxLenArr = new Array ();
    var item = null;
    var itemLen = this.items.length;
    var strValue = '';
    for (var k = 0; k < descsLen; k++) {
      colName = colDescs[k].name;
      dataDescs[k] = new ColumnDataDescriptor (colName);
      maxLenArr[k] = 0;
    }
    for (var i = 0; i < itemLen; i++) {
      item = this.items[i];
      for (var j =0; j < descsLen; j++) {
        strValue = item.getValue(colDescs[j].name)
          if (strValue) {
            strLen = strValue.length;
          } else {
            strLen = 0;
          }
        if (strLen > maxLenArr[j]) {
          maxLenArr[j] = strLen;
          //dataDescs[j].value = ('' + strValue);
          dataDescs[j].width = strLen;
        }
      }
    }
  }
  return (dataDescs);
}

function ListModel_getItem (key)
{

}

function ListModel_getItemAtIndex (index)
{
  return this.items[index];
}

function ListModel_setMaxColWidth (colName, colWd)
{
  if (! this.maxColWidth)
    this.maxColWidth = new Array ();
  this.maxColWidth[colName] = colWd;
}

function ListModel_getMaxColWidth (colName)
{
  return (this.maxColWidth[colName]);
}

function ListModel_destroy ()
{
  if (this.items) {
    var len = this.items.length;
    for (var i=0; i<len; i++) {
      this.items[i].destroy ();
      
      this.items[i] = null;
      delete this.items[i];
    }
    this.items.length = 0;
    this.items = null;
    delete this.items;
    
    this.clearQueue ();
  }
}


HTMLHelper.importPrototypes(ListModel, Listener);

//public methods
ListModel.prototype.addItem = ListModel_addItem;
ListModel.prototype.removeItem = ListModel_removeItem;
ListModel.prototype.removeItemAtIndex = ListModel_removeItemAtIndex;
ListModel.prototype.getItemCount = ListModel_getItemCount;
ListModel.prototype.getItem = ListModel_getItem;
ListModel.prototype.getItemAtIndex = ListModel_getItemAtIndex;
ListModel.prototype.setSelectionMode = ListModel_setSelectionMode;
ListModel.prototype.getSelectionMode = ListModel_getSelectionMode;
ListModel.prototype.getMaxColumnDataDescriptors = ListModel_getMaxColumnDataDescriptors;
ListModel.prototype.setMaxColWidth = ListModel_setMaxColWidth;
ListModel.prototype.getMaxColWidth = ListModel_getMaxColWidth;
ListModel.prototype.destroy = ListModel_destroy;

//private methods
ListModel.prototype.clearSelection = ListModel_clearSelection;
ListModel.prototype.propertyChangeCallback = ListModel_propertyChangeCallback;

ListModel.SELECT_MULTI = 0;
ListModel.SELECT_SINGLE = 1;
ListModel.SELECT_NONE = 2;


/*  czLstItm.js 115.21 2001/05/04 13:25:17 cjeyapra ship $ */

function ListItem ()
{
  this.baseConstructor = Prompt;
  this.baseConstructor ();

  this.type = 'ListItem';
  this.backgroundColor = 'white';
  this.activeBackColor = 'lightgrey';
  this.selectedBackColor = 'gray';
  this.modelItem = new ListItemModel ();

  //register listeners;
  this.addListener ('mouseoverCallback', this);
  this.addListener ('mouseoutCallback', this);
  this.addListener ('mouseupCallback', this);
}

function ListItem_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function ListItem_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function ListItem_getModelItem ()
{
  return this.modelItem;
}

function ListItem_setModelItem (model)
{
  if (! this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  if (model) {
    if(this.parentObj)
      this.readOnly(this.parentObj.read);
    this.clearModelListeners();
    this.modelItem = model;
    this.loadModelProperties(model);
    this.registerModelListeners();
  } else {
    // null model. Lock events on this view layer;
    this.modelItem=null;
    this.clearViewAndLockEvents ();
    //this.unhilite ();
  }
}

function ListItem_buildCells()
{
  // construct and position the cells from left to right;
  if (this.columnDescriptors) {
    var colDesc = null;
    this.cellsHash = new Array();
    this.cells = new Array();
    
    colDesc = this.columnDescriptors[0];
    this.cellsHash[colDesc.name] = this;
    this.cells[colDesc.index] = this;
    this.type = 'label';
    
    this.cellsBuilt = true;
  }
}

function ListItem_loadModelProperties(model)
{
  if (model) {
    //Unlock the view to listen for events;
    this.unlockEvents ();

    this.setCellValue ('text', model.getValue('text'));
    this.setSelected (model.isSelected());
  }
}

function ListItem_setCellValue (colName, colValue)
{
  this.setCaption (colValue);
}

function ListItem_setSelected (state)
{
  if (state) {
    //this.hilite (this.selectedBackColor);		
  } else {
    //this.unhilite ();
  }
}

function ListItem_setColumnDescriptors (colDescs, colDescHash)
{
  this.columnDescriptors = colDescs;
  this.colDescHash = colDescHash;
}

function ListItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {
    // default-- the ListItem will later interpret a single text column;
    // to mean that it should render to it's own layer;
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('text', 'label', 0, this.height, this.width, 0, 0, 'top', 'left', true, true);
    this.hashColumnDescriptor(this.columnDescriptors[0]);
  }
  return (this.columnDescriptors);
}

function ListItem_getColumnDescriptorsHash ()
{
  return (this.colDescHash);
}

function ListItem_hashColumnDescriptor(colDesc) 
{
  this.colDescHash[colDesc.name] = colDesc;
}

function ListItem_hashAllColumnDescriptors(colDescs) 
{
  var len = colDescs.length;
  for (var i = 0; i < len; i++) {
    this.hashColumnDescriptor(colDescs[i]);
  }
}

function ListItem_clearViewAndLockEvents()
{
  this.clearView();
  this.lockEvents();  
}

function ListItem_clearView()
{
  this.setValue ('');
}

function ListItem_lockEvents() 
{
  this.locked = true;
}

function ListItem_unlockEvents() 
{
  this.locked = false;
}

function ListItem_registerModelListeners()
{
  if (this.modelItem) {
    if(!this.read)
    {
      //register modelitem listener
      this.modelItem.addListener('propertyChangeCallback', this);
    }
    else
   {
      //remove listeners;
      this.removeListener ('mouseoverCallback', this);
      this.removeListener ('mouseoutCallback', this);
      this.removeListener ('mouseupCallback', this);
   }
  }
}

function ListItem_clearModelListeners()
{
  if (this.modelItem) {
    this.modelItem.removeListener('propertyChangeCallback', this);
  }
}

function ListItem_propertyChangeCallback (change)
{
  if (change.property == 'selected') {
    this.setSelected(change.value);
  } else {
    this.setCellValue(change.property, change.value);
  }
  return true;
}

function ListItem_mouseoverCallback (e, eSrc)
{
  if (this.locked) 
    return true;

  //this.hilite (this.activeBackColor);
  //if (this.modelItem && (!this.modelItem.isSelected())) {
    //this.hilite (this.activeBackColor);
  //}
  return true;
}

function ListItem_mouseoutCallback (e, eSrc)
{
  if (this.locked) 
    return true;
  
  //this.unhilite ();
  //if (this.modelItem && (!this.modelItem.isSelected())) {
    //this.unhilite ();
  //}
}

function ListItem_mouseupCallback (e, eSrc)
{
  if (this.locked)
    return true;

  if (this.modelItem) {
    this.modelItem.setSelected (!this.modelItem.isSelected());
  }
  this.notifyListeners ('actionMouseUp', e, this);
  return true;
}

function ListItem_fixDimensions ()
{
  if (this.cellsHash) {
    var wd = 0;
    var lt = 0;
    var curCell = null, colDesc = null;

    var len = this.columnDescriptors.length;    
    for (var i = 0; i < len; i++) {
      colDesc = this.columnDescriptors [i];
      wd = colDesc.getAvailWidth ();
      lt = colDesc.getLeft ();
      curCell = this.cellsHash[colDesc.name];
      curCell.setWidth (wd);
      curCell.setLeft (lt);
    }
  }
}

function ListItem_showColumn(colName, withLayout)
{
  if (this.cellsHash) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      curCell.show();
      if (withLayout) {
        this.layoutColumns ();
      }
    }
  }  
}

function ListItem_hideColumn(colName, withLayout)
{
  if (this.cellsHash) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      curCell.hide ();
      if (withLayout) {
        this.layoutColumns ();
      }
    }
  }
}

function ListItem_layoutColumns ()
{
  //empty function. Sub class will override
}

function ListItem_innerRender()
{
  var sBuffer = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  sBuffer += '<NOBR ID=SPAN-' + this.name;
  if (this.spanClass) {
    sBuffer += ' CLASS=' + this.spanClass;
  }
  sBuffer += '>';
  if (this.caption)
    sBuffer += this.caption;
  
  sBuffer += '</NOBR>';
  
  return sBuffer;
}

// Extend from Prompt
HTMLHelper.importPrototypes(ListItem, Prompt);

// Public Methods
ListItem.prototype.setActiveBackColor	= ListItem_setActiveBackColor;
ListItem.prototype.setSelectedBackColor = ListItem_setSelectedBackColor;
ListItem.prototype.setSelected = ListItem_setSelected;

// Protected methods
ListItem.prototype.setModelItem	= ListItem_setModelItem;
ListItem.prototype.getModelItem	= ListItem_getModelItem;
ListItem.prototype.innerRender = ListItem_innerRender;
ListItem.prototype.fixDimensions = ListItem_fixDimensions;
ListItem.prototype.setColumnDescriptors = ListItem_setColumnDescriptors;
ListItem.prototype.getColumnDescriptors = ListItem_getColumnDescriptors;
ListItem.prototype.getColumnDescriptorsHash = ListItem_getColumnDescriptorsHash;
ListItem.prototype.setCellValue = ListItem_setCellValue;
ListItem.prototype.layoutColumns = ListItem_layoutColumns;
ListItem.prototype.showColumn = ListItem_showColumn;
ListItem.prototype.hideColumn = ListItem_hideColumn;

// Private methods
ListItem.prototype.buildCells = ListItem_buildCells;
ListItem.prototype.registerModelListeners = ListItem_registerModelListeners;
ListItem.prototype.clearModelListeners = ListItem_clearModelListeners;
ListItem.prototype.loadModelProperties = ListItem_loadModelProperties;
ListItem.prototype.hashColumnDescriptor = ListItem_hashColumnDescriptor;
ListItem.prototype.hashAllColumnDescriptors = ListItem_hashAllColumnDescriptors;

ListItem.prototype.clearViewAndLockEvents = ListItem_clearViewAndLockEvents;
ListItem.prototype.clearView = ListItem_clearView;
ListItem.prototype.lockEvents = ListItem_lockEvents;
ListItem.prototype.unlockEvents = ListItem_unlockEvents;

// Event handlers
ListItem.prototype.mouseoverCallback = ListItem_mouseoverCallback;
ListItem.prototype.mouseoutCallback	= ListItem_mouseoutCallback;
ListItem.prototype.mouseupCallback = ListItem_mouseupCallback;
ListItem.prototype.propertyChangeCallback = ListItem_propertyChangeCallback;

/*  czLItmM.js 115.6 2000/11/07 19:34:57 tabbott ship $ */

function ListItemModel (rtid, desc, isSelected, isVisible)
{
  this.type = 'ListItemModel';
  this.columnHash = new Array ();
  
  if (rtid == null) rtid = 'z' + ListItemModel.count++;
  if (desc == null) desc = '';
  if (isSelected == null) isSelected = false;
  if (isVisible == null) isVisible = true;
  
  this.name = rtid;
  this.selected = isSelected;
  this.visible = isVisible;
  this.setValue ('text', desc);
  this.index = null;
}

function ListItemModel_setName (rtid)
{
  this.name = rtid;
}

function ListItemModel_getName()
{
  return this.name;
}

function ListItemModel_setText(val)
{
  if (val) {
    this.setValue ('text', val);
  } else {
    this.setValue ('text', '');
  }
}

function ListItemModel_setValue (colName, colValue)
{
  var oldVal = this.columnHash[colName];
  if (oldVal != colValue) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:colName, value:colValue})) {
      this.columnHash[colName] = colValue;
    }		
  }
}

function ListItemModel_getValue (colName)
{
  return (this.columnHash[colName]);
}

function ListItemModel_getText()
{
  return this.getValue('text');
}

function ListItemModel_setIndex(val)
{
  if (val != this.index) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'Index', value:val})) {
      this.index = val;
      //it's the container's responsibility to maintain indexing.
    }	
  }
}

function ListItemModel_getIndex()
{
  return this.index;
}

function ListItemModel_setSelected (sel)
{
  if (sel != this.selected) {
    if (this.notifyListeners ('propertyChangeCallback', {source:this, property:'selected', value:sel})) {
      this.selected = sel;
    }	
  }
}

function ListItemModel_isSelected ()
{
  return this.selected;
}

function ListItemModel_setVisible (vis)
{
  if (vis != this.visible) {
    if (this.notifyListeners ('propertyChangeCallback',{source:this, property:'Visible', value:vis})) {
      this.visible = vis;
    }	
  }	
}

function ListItemModel_isVisible ()
{
  return this.visible;
}

function ListItemModel_destroy ()
{
  if (this.columnHash) {
    Utils.clearCollection (this.columnHash);
    this.columnHash = null;
    delete this.columnHash;
    
    this.clearQueue ();
  }
}

HTMLHelper.importPrototypes(ListItemModel,Listener);

//Class members
ListItemModel.count = 0;

//public methods
ListItemModel.prototype.setName = ListItemModel_setName;
ListItemModel.prototype.getName = ListItemModel_getName;
ListItemModel.prototype.setText = ListItemModel_setText;
ListItemModel.prototype.getText = ListItemModel_getText;
ListItemModel.prototype.setIndex = ListItemModel_setIndex;
ListItemModel.prototype.getIndex = ListItemModel_getIndex;
ListItemModel.prototype.setSelected = ListItemModel_setSelected;
ListItemModel.prototype.isSelected = ListItemModel_isSelected;
ListItemModel.prototype.setVisible = ListItemModel_setVisible;
ListItemModel.prototype.isVisible = ListItemModel_isVisible;
ListItemModel.prototype.setValue = ListItemModel_setValue;
ListItemModel.prototype.getValue = ListItemModel_getValue;
ListItemModel.prototype.destroy = ListItemModel_destroy;
/*  czLLItM.js 115.9 2000/12/20 16:24:05 snadkarn ship $ */
 
function LogicItemModel (rtid, text, state, count, price, atp, isVisible)
{
  this.parentConstructor = ListItemModel;
  this.parentConstructor (rtid, text, false, isVisible);
  this.type = 'LogicItemModel';
  
  // may be required... depends on the logical type;
  if (state != null) {
    this.setValue ('state', state);
  } else {
    this.setValue ('state', 'unknown');
  }
  // may be required... depends on the logical type;
  if (count != null) {
    this.setValue ('count', count);
  } else {
    this.setValue ('count', 0);
  }
  // optional;
  if (price != null) {
    this.setValue ('price', price);
  } else {
    this.setValue ('price', '');
  }
  if (atp != null) {
    this.setValue ('atp', atp);
  } else {
    this.setValue ('atp', '');
  }
}

function LogicItemModel_setState (state) 
{
  this.setValue ('state', state);
}

function LogicItemModel_setVisibility (visible) 
{
  this.setValue ('visible', visible);
}

function LogicItemModel_getVisibility ()
{
  return this.getValue ('visible');
}

function LogicItemModel_getState ()
{
  return this.getValue ('state');
}

function LogicItemModel_clearState ()
{
  //set state to unknown temporarily and without throwing events;
  //and let the server to set the actual state;
  this.columnHash['state'] = 'empty';
}

function LogicItemModel_setCount (count)
{
  this.setValue ('count', count);
}

function LogicItemModel_getCount ()
{
  return this.getValue ('count');
}

function LogicItemModel_setPrice (price)
{
  this.setValue ('price', price);
}

function LogicItemModel_getPrice ()
{
  return this.getValue ('price');
}

function LogicItemModel_setATP (atp)
{
  this.setValue ('atp', atp);
}

function LogicItemModel_getATP ()
{
  return this.getValue ('atp');
}

function LogicItemModel_setDescImage (imgSrc)
{
  this.setValue ('descImg', imgSrc);
}

function LogicItemModel_getDescImage ()
{
  return this.getValue ('descImg');
}

HTMLHelper.importPrototypes(LogicItemModel, ListItemModel);

LogicItemModel.prototype.getState = LogicItemModel_getState;
LogicItemModel.prototype.setState = LogicItemModel_setState;
LogicItemModel.prototype.clearState = LogicItemModel_clearState;
LogicItemModel.prototype.getCount = LogicItemModel_getCount;
LogicItemModel.prototype.setCount = LogicItemModel_setCount;
LogicItemModel.prototype.getPrice = LogicItemModel_getPrice;
LogicItemModel.prototype.setPrice = LogicItemModel_setPrice;
LogicItemModel.prototype.getATP = LogicItemModel_getATP;
LogicItemModel.prototype.setATP = LogicItemModel_setATP;	
LogicItemModel.prototype.setVisibility = LogicItemModel_setVisibility;	
LogicItemModel.prototype.getVisibility = LogicItemModel_getVisibility;	
LogicItemModel.prototype.setDescImage = LogicItemModel_setDescImage;
LogicItemModel.prototype.getDescImage = LogicItemModel_getDescImage;

/*  czLList.js 115.22 2001/05/10 22:19:38 cjeyapra ship $ */

function LogicList ()
{
  this.parentConstructor = List;
  this.parentConstructor ();

  this.type = 'LogicList';
  this.cellRenderClass = 'OptionItem';
  this.listModel.setSelectionMode (ListModel.SELECT_NONE);
  this.priceVisible = false;
  this.atpVisible = false;

  //by default Prompt will be used to display description
  this.bUseImg = false;
  //by default we show state icons
  this.bNoIcon = false;
  this.bLogClr = false;
  this.bLogBld = false;
}

function LogicList_addOptionItem (rtid, item, index, state, price, atp, visible, imgSrc)
{
  this.cellRenderClass = 'OptionItem';
  return this.addLogicItem (rtid, item, index, state, null, price, atp, visible, imgSrc);
}

function LogicList_addCountedOptionItem (rtid, item, index, state, count, price, atp, visible, imgSrc)
{
  this.cellRenderClass = 'CountedOptionItem';
  return this.addLogicItem (rtid, item, index, state, count, price, atp, visible, imgSrc);
}

function LogicList_addBomItem (rtid, item, index, state, count, price, atp, visible)
{
  this.cellRenderClass = 'BomItem';
  return this.addLogicItem (rtid, item, index, state, count, price, atp, visible);
}

function LogicList_addLogicItem (rtid, item, index, state, count, price, atp, visible, imgSrc)
{
  if (typeof (item) == 'string') {
    var str = item;
    item = new LogicItemModel ();
    item.setText (str);
  }
  if (rtid) item.setName (rtid);
  if (state) item.setState (state);
  if (count != null) item.setCount (count);
  if (price != null) item.setPrice (price);
  if (atp != null) item.setATP (atp);
  if (imgSrc != "") item.setDescImage (IMAGESPATH + imgSrc);
  item.setVisibility (visible);
  this.listModel.addItem (rtid, item, index);
  return item;
}

function LogicList_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
}

function LogicList_buildCellRenderers()
{
  if (this.visibleRowCount == 0) {
    this.visibleRowCount = Math.ceil(this.height / this.itemHt);
  } else {
    var count = this.listModel.getItemCount ();
    if ((count > 0) && (count < this.visibleRowCount)) {
      this.visibleRowCount = count;
    } else if ((count == 0) && (this.mode == List.MODE_DROPDOWN)) {
      this.visibleRowCount = 1;
    }
    this.setHeight (this.visibleRowCount * this.itemHt + 2);
  }
  
  var cell;
  //Cache the column Descriptor and set this one to all the List Items;
  cell = eval('new ' + this.cellRenderClass + '();');
  this.columnDescriptors = cell.getColumnDescriptors (this.bUseImg, this.bNoIcon);
  this.colDescHash = cell.getColumnDescriptorsHash ();

  for (var i = 0; i < this.visibleRowCount; i++) {
    cell = eval('new ' + this.cellRenderClass + '();');
    cell.setName(this.name + 'Cell' + i);
    cell.setColumnDescriptors (this.columnDescriptors, this.colDescHash);
    cell.setParentObject(this);
    if (ns4) {
      cell.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    } else if (ie4) {
      cell.setReference (this.reference);
    }
    cell.setDimensions (0, this.itemHt*i, this.width, this.itemHt);
    cell.setBackgroundColor (this.itemBackgroundColor);
    cell.setActiveBackColor (this.activeBackColor);
    cell.setSelectedBackColor (this.selectedBackColor);
    cell.setSpanClass (this.spanClass);
    cell.setColor (this.color);
    // logic specific implementation;
    if (cell.setImages) {
      cell.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight)
    }
    cell.setPriceVisible (this.priceVisible);
    cell.setATPVisible (this.atpVisible);
    //set logic state props like no icon, color and boldness
    cell.setLogicStateProps (this.bNoIcon, this.bLogCol, this.bLogBld);
    cell.index = i;
    cell.setWindowName (this.windowName);
    cell.setModelItem (this.listModel.getItemAtIndex(i));
    this.cells[i] = cell;
    cell.setWindowName (this.windowName);
    cell.addListener ('actionMouseUp', this);
    cell.addListener ('actionOnChange', this);
  }
  // we only do this ONCE;
  this.cellsBuilt = true;
}

function LogicList_setPriceVisible (bVis)
{
  this.priceVisible = bVis;
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].setPriceVisible (bVis, true);
  }
  if (this.launched) {
    if (! this.bPriceColUpdated) {
      this.updateNewColumnWidth (this.colDescHash['price']);
      this.updateColumnPosition ();
      this.fixItemDimensions ();
      this.bPriceColUpdated = true;

      this.adjustLastCellHeight ();
      this.updateScrollBarVisibility();
      this.updateHorizontalScrollValues();
      this.updateVerticalScrollValues();
    }
  }
}

function LogicList_setATPVisible (bVis)
{
  this.atpVisible = bVis;
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].setATPVisible (bVis);
  }
  if (this.launched) {
    if (! this.bAtpColUpdated) {
      this.updateNewColumnWidth (this.colDescHash['atp']);
      this.updateColumnPosition ();
      this.fixItemDimensions ();
      this.bAtpColUpdated = true;

      this.adjustLastCellHeight ();
      this.updateScrollBarVisibility();
      this.updateHorizontalScrollValues();
      this.updateVerticalScrollValues();
    }
  }
}

function LogicList_updateNewColumnWidth (colDesc)
{
  var charWidth, charHeight;
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  if (colDesc.name == 'atp') {
    prefWidth = 10 * charWidth + colDesc.pdgLeft;
    prefHeight = charHeight; 
  } else if  (colDesc.name == 'price') {
    prefWidth = 8 * charWidth + colDesc.pdgLeft;
    prefHeight = charHeight;
  }
  colDesc.setPrefWidth (prefWidth);
  colDesc.setPrefHeight (prefHeight);
  
  this.cellPrefWidth += prefWidth;
  if (this.cellPrefHeight < prefHeight) {
    this.cellPrefHeight = prefHeight;
  }
}

function LogicList_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function LogicList_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.getLeft() + this.getWidth() + 8;
    var tpos = this.getTop();
    uicon.setDimensions (lpos, tpos, 16, 16);
    var ref = this.getReference ();
    uicon.setReference (ref);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = ref.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = ref.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicList_setMaxColWidth (colName, colWd)
{
  this.listModel.setMaxColWidth (colName, colWd);
}

function LogicList_useImageForDesc (bVal)
{
  if (this.launched)
    return;

  this.bUseImg = bVal;
}

function LogicList_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;

  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld;
}

HTMLHelper.importPrototypes(LogicList,List);

//public methods
LogicList.prototype.addOptionItem = LogicList_addOptionItem;
LogicList.prototype.addCountedOptionItem = LogicList_addCountedOptionItem;
LogicList.prototype.addBomItem = LogicList_addBomItem;
LogicList.prototype.setImages = LogicList_setImages;
LogicList.prototype.setMaxColWidth = LogicList_setMaxColWidth;
LogicList.prototype.setUnSatisfied = LogicList_setUnSatisfied;
LogicList.prototype.hideFeature = LogicList_hideFeature;
LogicList.prototype.setPriceVisible = LogicList_setPriceVisible;
LogicList.prototype.setATPVisible = LogicList_setATPVisible;
LogicList.prototype.useImageForDesc = LogicList_useImageForDesc;
LogicList.prototype.setLogicStateProps = LogicList_setLogicStateProps;

//override
LogicList.prototype.buildCellRenderers = LogicList_buildCellRenderers;

//private
LogicList.prototype.addLogicItem = LogicList_addLogicItem;
LogicList.prototype.updateNewColumnWidth = LogicList_updateNewColumnWidth;

/*  czLLIt.js 115.29 2001/05/29 19:26:13 snadkarn ship $ */

function LogicItem ()
{
  this.parentConstructor = ListItem;
  this.parentConstructor ();
  
  this.type = 'LogicItem';
  this.columnDescriptors = null;
  this.cells = null;
  this.cellsHash = null;
  this.cellsBuilt = false;
  //Border around cells;
  this.borderWidth = 1;
  this.borderColor = 'black';
  this.priceVisible = false;
  this.atpVisible = false;
  
  this.modelItem = new LogicItemModel ();
}

function LogicItem_moreSetName (nm)
{
  this.modelItem.setName (nm);
  if (this.cellsBuilt) {
    var len = this.cells.length;
    var cell = null;
    for (var i = 0; i < len; i++) {
      cell = this.cells[i];
      if (cell.type != 'inputtext') {
        cell.setName (cell.name + '-' + nm);
      } else {
        cell.setName (nm + 'xC' + i);
      }
    }
  }
}

function LogicItem_moreSetHeight(ht)
{ 
  if (this.cells) {
    var len = this.cells.length;
    var cell = null;
    for (var i = 0; i < len; i++) {
      cell = this.cells[i];  
      if (cell.type == 'icon') {
        cell.setTop (Math.round(Math.abs(this.height-cell.height)/2));
      } else {
        cell.setHeight(this.height);
      }
    }
  }
}

function LogicItem_setColor (col)
{
  this.color = col;
  if (this.launched) {
    if (ie4)
      HTMLHelper.setForegroundColor(this.getHTMLObj(), col);
    else if (ns4)
      return;
  }
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setColor (col);    
    }
  }
}

/*
function LogicItem_moreSetColor (col)
{
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setColor (col);    
    }
  }
}

function LogicItem_moreSetBackgroundColor (col)
{
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].setBackgroundColor (col);    
    }
  }
}
*/

function LogicItem_setSpanClass (cls)
{
  this.spanClass = cls;
  if (this.cellsBuilt) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      if (this.cells[i].type != 'icon') {
        this.cells[i].setSpanClass (cls);
      }
    }
  }
}

function LogicItem_hitTest (e)
{
  if (e) {
    var xpos = (ns4) ? e.pageX : e.clientX;
    var ypos = (ns4) ? e.pageY : e.clientY;
    
    var absLeft = this.getLeft (true);
    var absTop = this.getTop (true);
    var rt = absLeft + this.getWidth ();
    var bt = absTop + this.getHeight ();
    if ((xpos >= absLeft) && (xpos <= rt) && (ypos >= absTop) && (ypos <= bt)) {
	for (var i=0; i < this.cells.length; i++ ) {
  	  var absLeftCell = this.cells[i].getLeft(true);
	  var absTopCell = this.cells[i].getTop(true);
	  var rtCell = absLeftCell + this.cells[i].getWidth();
	  var btCell = absTop + this.cells[i].getHeight();
	  if ( (xpos >= absLeftCell) && (xpos <= rtCell) && (ypos >= absTopCell) && (ypos <= btCell))
	    return this.cells[i].objId;
	}
      }
  }
  return (null);
}

function LogicItem_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
 // var icon = this.cellsHash['state'];
 // if (icon) {
 //   icon.setSrc (this.srcUnknown);
 // }
}

function LogicItem_setCount (cnt)
{
  this.modelItem.setCount(cnt);
}

function LogicItem_getCount ()
{
  return (this.modelItem.getCount());
}

function LogicItem_setCaption (val)
{
  this.modelItem.setText (val);
}

function LogicItem_getCaption ()
{
  return (this.modelItem.getText ());
}

function LogicItem_setState (logicState)
{
  this.modelItem.setState(logicState);
}

function LogicItem_getState ()
{
  return (this.modelItem.getState());
}

function LogicItem_setVisibility (visible)
{
  this.modelItem.setVisibility(visible);
}

function LogicItem_getVisibility ()
{
  return (this.modelItem.getVisibility());
}

function LogicItem_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.left + this.width + 8;
    var tpos = this.top + (this.height - 16)/2;
    uicon.setDimensions (lpos, tpos, 16, 16);
    uicon.setReference (this.reference);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = this.reference.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = this.reference.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicItem_setPriceVisible (bVis, bWithLayout)
{	
  this.priceVisible = bVis;
  if (bVis) {
    if (! this.columnDescriptors) {
      this.getColumnDescriptors ();
    }
    if (! this.colDescHash ['price']) {
      this.updateColumnDescriptors ();
    }
    if (this.cellsBuilt) {
      if (! this.cellsHash ['price']) {
        var colDesc = this.colDescHash['price'];
        var newCell = this.createCell(colDesc);
        newCell.setName (colDesc.name + '-' + this.name);
        newCell.index = colDesc.index;
        this.cells[colDesc.index] = newCell;
        this.cellsHash[colDesc.name] = newCell;
        // add listeners;
        this.registerCellListeners(colDesc, newCell);
        if (this.launched) {
          //create price layer dynamically;
          newCell.runtimeRender (null, this, this.eventManager);
        }
        /*
        if (! bWithLayout) {
          //set left pos;
          var prevCell = this.cells[colDesc.index-1];
          newCell.setLeft (prevCell.getLeft() + prevCell.getWidth());
        }
        */
        this.updateColumnPosition ();
        this.fixDimensions ();
      }
      this.showColumn('price', bWithLayout);
    }
  } else {
    this.hideColumn('price', bWithLayout);
  }
}

function LogicItem_isPriceVisible ()
{
  return this.priceVisible;
}

function LogicItem_setPrice (val)
{
  this.modelItem.setPrice(val);
}

function LogicItem_getPrice ()
{
  return (this.modelItem.getPrice());
}

function LogicItem_setATPVisible (bVis, bWithLayout)
{
  this.atpVisible = bVis;
  if (bVis) {
    if (! this.columnDescriptors) {
      this.getColumnDescriptors ();
    }
    if (! this.colDescHash ['atp']) {
      this.updateColumnDescriptors ();
    }
    if (this.cellsBuilt) {
      if (! this.cellsHash ['atp']) {    
        var colDesc = this.colDescHash['atp'];
        var newCell = this.createCell(colDesc);
        newCell.setName (colDesc.name + '-' + this.name);
        newCell.index = colDesc.index;
        this.cells[colDesc.index] = newCell;
        this.cellsHash[colDesc.name] = newCell;
        // add listeners;
        this.registerCellListeners(colDesc, newCell);
        if (this.launched) {
          //create atp layer dynamically;
          newCell.runtimeRender (null, this, this.eventManager);
          if (this.type == 'BomItem') {
            this.updatePreferredWidth ();
            this.updateColumnPosition();
            this.fixDimensions();
            return;
          }
        }
        if (! bWithLayout) {
          //set left pos;
          var prevCell = this.cells[colDesc.index-1];
          newCell.setLeft (prevCell.getLeft() + prevCell.getWidth() + 2);
        }
      }
    }
    this.showColumn('atp', bWithLayout);
  } else {
    this.hideColumn('atp', bWithLayout);
  }
}

function LogicItem_isATPVisible ()
{
  return (this.atpVisible);
}

function LogicItem_setATP (dat)
{
  this.modelItem.setATP(dat);
}

function LogicItem_getATP ()
{
  return (this.modelItem.getATP());
}

function LogicItem_getImageSrc (logicState)
{
  var src;
  switch (logicState) {
  case 'utrue':
    src = this.srcUTrue;
    break;
  case 'ufalse':
    src = this.srcUFalse;
    break;
  case 'ltrue':
    src = this.srcLTrue;
    break;
  case 'lfalse':
    src = this.srcLFalse;
    break;
  case 'unknown':
    src = this.srcUnknown;
    break;
  }
  return src;
}

function LogicItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    // params -name, type, index, ht, wd, padLt, padTop, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('text', 'label', 0, this.height, 50, 0, 0, 'top', 'left', true, true);

    //this.columnDescriptors[1] = new ColumnDescriptor ('price', 'label', 1, this.height, 20, 0, 0, 'top', 'right', true, false);
    //this.columnDescriptors[2] = new ColumnDescriptor ('atp', 'label', 2, this.height, 20, 0, 0, 'top', 'center', true, false);
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function LogicItem_updateColumnDescriptors ()
{
  if (this.columnDescriptors) {
    var index = this.columnDescriptors.length;
    if (! this.colDescHash['price']) {
      if (this.priceVisible) {
        this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, this.height, 10, 1, 2, 'center', 'right', true, false);
        this.hashColumnDescriptor (this.columnDescriptors[index]);
        index++;
      }
    }
    if (! this.colDescHash['atp']) {
      if (this.atpVisible) {
        this.columnDescriptors[index] = new ColumnDescriptor ('atp', 'label', index, this.height, 10, 1, 2, 'center', 'left', true, false);
        this.hashColumnDescriptor (this.columnDescriptors[index]);
      }
    }
  }
}

function LogicItem_updateColumnPosition ()
{
  var colDesc = null;
  var len = this.columnDescriptors.length;
  if (! this.springColDesc) {
    var i = 0;
    while ((! this.springColDesc) && (i<len)) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.spring) {
        this.springColDesc = colDesc;
      }
      i++;
    }
  }
  var gap = 0;
  var rtMargin = 0;
  if (this._3DLook) {
    gap = 2;
    rtMargin = 1;
  } else if (this._flatLook) {
    gap = 1;
    rtMargin = 1;
  }
  var totWidth = gap;
  var totWidthUpToSpringCell =0;
  var springColDesc = this.springColDesc;

  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors[i];
    if (colDesc.defVisible) {
      if (colDesc == springColDesc) {
        totWidthUpToSpringCell = totWidth;
      }
      colDesc.setLeft (totWidth);
      totWidth += colDesc.getPrefWidth () + gap;
    }
  }
  totWidth -= gap;
  if ((this.width > totWidth) || (this.type == 'BomItem')) {
    springColDesc.setAvailWidth (springColDesc.getPrefWidth() + (this.width - totWidth - rtMargin));

    //now shift everything else from spring cell;
    totWidth = totWidthUpToSpringCell + springColDesc.getAvailWidth () + gap;
    for (var i=springColDesc.index + 1; i<len; i++) {
      colDesc = this.columnDescriptors[i];
      if (colDesc.defVisible) {
        colDesc.setLeft (totWidth);
      }
      totWidth += colDesc.getAvailWidth () + gap;
    }
  } else {
    //use the preferred with for the spring cell as available widht;
    springColDesc.setAvailWidth (springColDesc.getPrefWidth ());
  }
}

function LogicItem_buildCells(colDescs)
{
  // construct and position the cells from left to right
  if (colDescs) {
    var colDesc = null;
    var newCell = null;
    var curCell = null;
    var gap = 0;
    var rtMargin = 0;
    if (this._3DLook) {
      gap = 2;
      rtMargin = 1;
    } else if (this._flatLook) {
      gap = 1;
      rtMargin = 1;
    }
    var cellsWidth = gap;
    var len = this.columnDescriptors.length;
    this.cellsHash = new Array();
    this.cells = new Array();
    if(this.parentObj){
      if(this.parentObj.read)
        this.readOnly(this.parentObj.read);
    }    
    for (var i = 0; i < len; i++) {
      colDesc = this.columnDescriptors[i];
      // maintain the last cell for positioning;
      curCell = newCell; 
      newCell = this.createCell(colDesc);

      if ((this.type == 'BomItem') && (newCell.type == 'label') &&
          (! this.priceVisible)) {
        newCell.setWidth(this.getWidth() - (this.cells[i-1].getLeft() + this.cells[i-1].getWidth() + gap) - rtMargin);
      }

      newCell.index = colDesc.index;
      this.cells[colDesc.index] = newCell;
      this.cellsHash[colDesc.name] = newCell;
      if (newCell.type == 'inputtext') {
        newCell.setName (this.name + 'x' + colDesc.name);
      } else {
        newCell.setName (colDesc.name + '-' + this.name);
      }
      newCell.setLeft (cellsWidth);
      // add listener;
      this.registerCellListeners(colDesc, newCell);
    
      if (colDesc.defaultVisible) {
        newCell.show();
      }
      cellsWidth += newCell.getWidth () + gap;
      if (this.windowName) {
        newCell.windowName = this.windowName;
      }
    }
    this.cellsBuilt = true;
  }
}

function LogicItem_createCell(colDesc)
{
  // cell factory;
  var newCell;
  //var wd = colDesc.defWidth + colDesc.pdgLeft;
  switch (colDesc.type)
  {
    case 'inputtext':
      newCell = new InputText();
      if(!this.read)
        newCell.addListener('mouseupCallback',newCell);
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight(this.height);
      newCell.setTop(0);
      newCell.EDITABLE = true;
      newCell.setBackgroundColor(this.backgroundColor);
      if ((this.type != 'BomItem') || (! (this._3DLook || this._flatLook) ))
        newCell.createBorder(this.borderWidth, this.borderColor);
      //for a flat look bom item create a 0 pixel border in ie4 to hide 3D
      if (ie4 && (this.type == 'BomItem') && this._flatLook)
        newCell.createBorder ('0', this.borderColor);
      if (this._3DLook && (this.type == 'BomItem'))
        newCell.alwaysShowInpTxt = true;
      newCell.setValue('');
      newCell.setSpanClass (this.spanClass);
      newCell.standalone = false;
      newCell.setTabindex (-1);
      break;
    case 'icon':
      // for now,  we only support 16x16 icons
      newCell = new ImageButton();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (colDesc.defHeight);
      newCell.setTop (Math.round ((this.height - colDesc.defHeight)/2));
      newCell.setBackgroundColor(this.backgroundColor);
      // set to a default source
      newCell.setSrc(IMAGESPATH + 'czblank.gif');
      newCell.active = true;
      newCell.addListener ('onKeyPressCallback', this);
      newCell.setWindowName (this.windowName);
      newCell.setPadding (colDesc.pdgLeft, colDesc.pdgTop);
      break;
    case 'label': 
      newCell = new Prompt();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (this.height);
      newCell.setTop (0);
      newCell.setCaption('');
      newCell.setBackgroundColor(this.backgroundColor);
      newCell.setSpanClass (this.spanClass);
      newCell.setWindowName (this.windowName);
      newCell.setPadding (colDesc.pdgLeft, colDesc.pdgTop);
      break;
    case 'descImg':
      newCell = new ImageButton();
      newCell.setName (colDesc.name);
      newCell.setWidth (colDesc.defWidth);
      newCell.setHeight (this.height);
      newCell.setTop (0);
      //create a blank image
      newCell.setSrc(IMAGESPATH + 'czblank.gif');
      break;
  }
  if (this._3DLook) {
    newCell.setTop(2);
    newCell.setHeight(this.height - 3);
    newCell.setBackgroundColor(this.cellBgColor);
  } else if (this._flatLook) {
    newCell.setTop(1);
    newCell.setHeight(this.height - 2);
    newCell.setBackgroundColor(this.cellBgColor);
  }
  newCell.setParentObject(this);
  newCell.type = colDesc.type;
  newCell.colName = colDesc.name;
  
  //TODO: Rework this later
  if (this.moreSetProperties) {
    this.moreSetProperties (newCell);
  }
  return newCell;
}

function LogicItem_loadModelProperties(model)
{
  if (model) {  
    //Unlock the view to listen for events;
    this.unlockEvents ();
    var ttip = null;
    if (ie4)
      ttip = eval("self._" + model.getState() + "ToolTip");
    this.setCellValue('state', this.getImageSrc(model.getState()), ttip);
    this.setCellValue('descImg', model.getDescImage());
    var desc = this.formDescStr(model.getText(), model.getState());
    this.setCellValue('text', desc);
    this.setCellValue('count', model.getCount());
    this.setCellValue('atp', model.getATP());
    this.setCellValue('price', model.getPrice());
    this.setSelected (model.isSelected());
  }
}

function LogicItem_formDescStr (desc, state)
{
  var strHtml = '';
  if (this.bLogCol) {
    strHtml += '<SPAN class="';
    if (state == 'utrue')
      strHtml += UT_COLOR + '" ';
    else if (state == 'ltrue')
      strHtml += LT_COLOR + '" ';
    else if (state == 'ufalse')
      strHtml += UF_COLOR + '" ';
    else if (state == 'lfalse')
      strHtml += LF_COLOR + '" ';
    else
      strHtml += UN_COLOR + '" ';
    
    strHtml += 'ID="c-' + this.objId + '">';
  }
  if (this.bLogBld) {
    strHtml += '<SPAN class="';
    if (state == 'utrue')
      strHtml += UT_BOLD + '" ';
    else if (state == 'ltrue')
      strHtml += LT_BOLD + '" ';
    else if (state == 'ufalse')
      strHtml += UF_BOLD + '" ';
    else if (state == 'lfalse')
      strHtml += LF_BOLD + '" ';
    else
      strHtml += UN_BOLD + '" ';

    strHtml += 'ID="b-' + this.objId + '">';
  }
  strHtml += desc;
  if (this.bLogCol)
    strHtml += '</SPAN>';
  if (this.bLogBld)
    strHtml += '</SPAN>';
  
  return strHtml;
}

function LogicItem_setCellValue (colName, colValue, ttip)
{
  if (this.cellsBuilt) {
    var curCell = this.cellsHash[colName];
    if (curCell) {
      if (! curCell.isVisible ()) {
        curCell.show ();
        //this.showColumn (colName, true);
      }
      switch (curCell.type) {
      case 'inputtext':
        curCell.setValue(colValue);
        break;
      case 'icon':
      case 'descImg':
        curCell.setSrc(colValue);
        if (ttip)
          curCell.setToolTipText(ttip);
        break;
      case 'label':
        curCell.setCaption(colValue);
        break;
      }
    }
  }
}

function LogicItem_moreCSS ()
{
  var CSSStr = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    CSSStr += this.cells[i].generateCSS() + '\n';
  }
  if (this.unSatIcon) {
    CSSStr += this.unSatIcon.generateCSS() + '\n';
  }
  if (this.bNoIcon) {
    this.createTopLayer();
    CSSStr += this.topLay.generateCSS();
  }
  return CSSStr;
}

function LogicItem_createTopLayer ()
{
  this.topLay = new ImageButton();
  this.topLay.setName ('TLay-'+this.name);
  this.topLay.setParentObject (this);
  this.topLay.setSrc (IMAGESPATH + 'czBlank.gif');
  this.topLay.active = true;
  this.topLay.addListener ('onKeyPressCallback', this);
  this.topLay.type = 'toplay';
  this.topLay.stretch = true;
  if (this.type == 'OptionItem') {
    //give the full width of the layer
    this.topLay.setDimensions (0, 0, this.width, this.height);
  } else if ((this.type == 'CountedOptionItem') ||
             (this.type == 'BomItem')) {
    //get the column descriptor of the input text box and then position this
    //top layer next to it
    var colDesc = this.colDescHash['count'];
    var offset = colDesc.getAvailWidth();
    this.topLay.setDimensions (offset, 0, this.width-offset, this.height);
  }
}

function LogicItem_render ()
{
  var sBuffer = "";
  if (this.border) {
    sBuffer += '<DIV ID=' + this.border.objId + '>' + '\n';
  }
  sBuffer += '<DIV ';
  if (this.objId != "")
    sBuffer += ' ID=' + this.objId ;

  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  if (this.tabindex)
    sBuffer += ' tabindex="' + this.tabindex + '"';

  if (this.bEnableKeyBoardEvt) {
    this.addListener('onKeyDownCallback', this, 'super');
    this.addListener('onKeyUpCallback', this, 'super');
    sBuffer += ' onkeydown="javascript:doOnKeyDown(null,\''+ this.self + '\')"';
    sBuffer += ' onkeyup="javascript:doOnKeyUp(null,\''+ this.self + '\')"';
  } else if (this.bEnableKeyPressEvt) {
    this.addListener('onKeyPressCallback', this, 'super');
    sBuffer += ' onkeypress="javascript:doOnKeyPress(null,\''+ this.self + '\')"';    
  }
  sBuffer +='>';

  if(this.innerRender)
    sBuffer += this.innerRender();

  sBuffer += '</DIV>' + '\n';
  
  if (this.border)
    sBuffer += '</DIV>\n';

  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';
  
  return (sBuffer);  
}

function LogicItem_innerRender ()
{
  var sBuffer = '';
  if (this.spanNormal) {
    this.setSpanClass (this.spanNormal);
  }
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    sBuffer += this.cells[i].render () + '\n';
  }
  if (this.topLay)
    sBuffer += this.topLay.render() + '\n';

  return (sBuffer);
}

function LogicItem_moreLaunch (em)
{
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].launch(em);
  }
  if (this.unSatIcon)
    this.unSatIcon.launch ();
  if (this.topLay)
    this.topLay.launch();
}

function LogicItem_hilite(hiliteColor)
{
  if (!this.launched) 
    return;
  if (!hiliteColor) 
    hiliteColor = 'lightgrey';
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), hiliteColor);
  if(!this.backgroundColor)
    this.backgroundColor ='transparent';
  
  if (this.cells) {	
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      if (this.cells[i].type != 'inputtext')
        this.cells[i].hilite(hiliteColor);
    }
  }
}

function LogicItem_unhilite()
{
  if (!this.launched) 
    return;
  HTMLHelper.setBackgroundColor(this.getHTMLObj(), this.backgroundColor);
  
  if (this.cells) {
    var len = this.cells.length;
    for (var i = 0; i < len; i++) {
      this.cells[i].unhilite();
    }
  }
}

function LogicItem_clearView()
{
  if (this.cells) {
    var len  = this.cells.length;
    for (var i=0; i<len; i++) {
      var cell = this.cells[i];
      switch (cell.type) {
      case 'icon':
      case 'descImg':
        cell.setSrc (IMAGESPATH + 'czblank.gif');
        break;
      case 'label':
        cell.setValue ('');
        break;
      case 'inputtext':
        cell.setValue ('');
        if (this.type == 'CountedOptionItem') {
          this.hideColumn(cell.colName);
        }
        break;
      }
    }
  }
}

function LogicItem_layoutColumns() 
{
  var gap = 0;
  if (this._3DLook) {
    gap = 2;
  } else if (this._flatLook) {
    gap = 1;
  }
  if (this.cells) {
    var cellsWidth = gap;
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      var cell = this.cells[i];
      if (cell.isVisible ()) {
        cell.setLeft (cellsWidth);
        cellsWidth += cell.getWidth () + gap;
      }
    }
  }
}

function LogicItem_propertyChangeCallback (change)
{
  if(this.modelItem!=null){
    if (change.property == 'selected') {
      this.setSelected(change.value);
    } else if (change.property == 'state') {
      var ttip = eval("self._" + change.value + "ToolTip");
      this.setCellValue ('state', this.getImageSrc(change.value), ttip); 
      if (this.bLogCol) {
        var descStr = this.formDescStr (this.modelItem.getText(), change.value);
        this.setCellValue ('text', descStr);
      }
    }else {
        this.setCellValue(change.property, change.value);
    }

  }
  return true;
}

function LogicItem_mouseupCallback (e, eSrc)
{
  if (this.locked) {
    if (eSrc.type == 'inputtext')
      return false;
    else
      return true;
  }
  // grow the height of the item to allow for the rendering of;
  // the input form element of inputtext for NS;
  if (eSrc.type == 'inputtext') {
    this.oldHeight = this.height;
    if (ns4 && this.height < 28) {
      this.setHeight (28);
    }	
  } else {
    if ((this.modelItem.getState() != 'utrue') && (this.modelItem.getState() != 'ltrue')) {
      //'guess' the client state until we get server notification.;
      this.modelItem.setState('utrue');
    } else {
      this.modelItem.setState('unknown');
    }
    this.notifyListeners ('actionMouseUp', this.modelItem, this);
  }
  return true;
}

function LogicItem_onchangeCallback(newValue, eSrc)
{
  if (this.locked)
    return true;
  
  if (ns4 && this.oldHeight && (! this._flatLook)) {
    this.setHeight (this.oldHeight); 
  }
  var curCell = eSrc;
  var colDesc = null;
  colDesc = this.columnDescriptors[curCell.index];
  if (colDesc) {
    this.modelItem.setValue(colDesc.name, newValue);
  }
  this.notifyListeners ('actionOnChange', this.modelItem, this);
  return true;
}

function LogicItem_onKeyPressCallback (e, eSrc)
{
  if ((eSrc.type == 'icon') || (eSrc.type == 'toplay') ||
      (eSrc.type == 'LogicCheckBox')) {
    var key = (ns4)? e.which : e.keyCode;
    if (key == 32) {
      if (eSrc.states == 1)
        eSrc.toggle(0);
      //IE window scrolls, this has to be there to prevent scrolling of the window
      if (ie4)
        e.returnValue = false;
     this.mouseupCallback (e, eSrc);
    }
  }
  return true;
}

function LogicItem_registerCellListeners(colDesc, cell) 
{
  if (colDesc.type == 'inputtext') {
    if(!this.read)
    {
      cell.addListener ('mouseupCallback', this, 'super');
      cell.addListener ('onchangeCallback', this);
    }
  }
  //cell.addListener ('mouseoverCallback', this);
  //cell.addListener ('mouseoutCallback', this);
  //cell.addListener ('mouseupCallback', this, 'super');  
}

function LogicItem_setWindowName (wndName)
{
  this.windowName = wndName;
  if (this.cellsBuilt) {
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].windowName = wndName;
    }
  }
}

function LogicItem_moreDestroy ()
{
  if (this.cells) {
    Utils.clearCollection (this.cellsHash);    
    this.cellsHash = null;
    delete this.cellsHash;
    
    var len = this.cells.length;
    for (var i=0; i<len; i++) {
      this.cells[i].destroy ();

      this.cells[i] = null;
      delete this.cells[i];
    }
    this.cells.length = 0;
    this.cells = null;
    delete this.cells;

    Utils.clearCollection (this.columnDescriptors);
    this.columnDescriptors = null;
    delete this.columnDescriptors;

    Utils.clearCollection (this.colDescHash);
    this.colDescHash = null;
    delete this.colDescHash;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
}

function LogicItem_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;
  
  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld
}

function LogicItem_hide()
{
  this.visibility = false;
  if (this.border)
    this.border.visibility = false;

  if (this.launched) {
    var curCell = this.cellsHash['count'];
    if (curCell) {
      curCell.hide();
    }
    if (this.border) {
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.visibility);
      return;
    }
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function LogicItem_show() 
{
  this.visibility = true;
  if (this.border)
    this.border.visibility = true;
  
  if (this.launched) {
    var curCell = this.cellsHash['count'];
    if (curCell) {
      curCell.show();
    }
    if (this.border)
      HTMLHelper.setVisible(this.border.getHTMLObj(), this.border.visibility);
    
    HTMLHelper.setVisible(this.getHTMLObj(), this.visibility);
  }
}

function LogicItem_getMaxNumFocusCells()
{
  return 1;
}

function LogicItem_setFocus(index)
{
  if (index) {
    var maxFoc = this.getMaxNumFocusCells();
    if (this.bNoIcon && (index == maxFoc)) {
      this.topLay.setFocus();
      return;
    }
    var cell = this.cells[index-1];
    if (cell) {
      cell.setFocus();
    }
  } else {
    this.getHTMLObj();
    this.block.focus();
  }
}

// construction
HTMLHelper.importPrototypes(LogicItem,ListItem);

// public methods
LogicItem.prototype.moreSetName = LogicItem_moreSetName;
LogicItem.prototype.moreSetHeight = LogicItem_moreSetHeight;
LogicItem.prototype.setColor = LogicItem_setColor;
//LogicItem.prototype.moreSetColor = LogicItem_moreSetColor;
//LogicItem.prototype.moreSetBackgroundColor = LogicItem_moreSetBackgroundColor;
LogicItem.prototype.setSpanClass = LogicItem_setSpanClass;
LogicItem.prototype.hilite = LogicItem_hilite;
LogicItem.prototype.unhilite = LogicItem_unhilite;
LogicItem.prototype.setImages = LogicItem_setImages;
LogicItem.prototype.setCaption = LogicItem_setCaption;
LogicItem.prototype.getCaption = LogicItem_getCaption;
LogicItem.prototype.setState = LogicItem_setState;
LogicItem.prototype.getState = LogicItem_getState;
LogicItem.prototype.setVisibility = LogicItem_setVisibility;
LogicItem.prototype.getVisibility = LogicItem_getVisibility;
LogicItem.prototype.setCount = LogicItem_setCount;
LogicItem.prototype.getCount = LogicItem_getCount;
LogicItem.prototype.setPrice = LogicItem_setPrice;
LogicItem.prototype.getPrice = LogicItem_getPrice;
LogicItem.prototype.setATP = LogicItem_setATP;
LogicItem.prototype.getATP = LogicItem_getATP;
LogicItem.prototype.setUnSatisfied = LogicItem_setUnSatisfied;
LogicItem.prototype.setPriceVisible = LogicItem_setPriceVisible;
LogicItem.prototype.isPriceVisible = LogicItem_isPriceVisible;
LogicItem.prototype.setATPVisible = LogicItem_setATPVisible;
LogicItem.prototype.isATPVisible = LogicItem_isATPVisible;
LogicItem.prototype.setWindowName = LogicItem_setWindowName;
LogicItem.prototype.setLogicStateProps = LogicItem_setLogicStateProps;

// private methods
LogicItem.prototype.getImageSrc = LogicItem_getImageSrc;
LogicItem.prototype.layoutColumns = LogicItem_layoutColumns;
LogicItem.prototype.registerCellListeners = LogicItem_registerCellListeners;
LogicItem.prototype.updateColumnDescriptors = LogicItem_updateColumnDescriptors;
LogicItem.prototype.updateColumnPosition = LogicItem_updateColumnPosition;
LogicItem.prototype.formDescStr = LogicItem_formDescStr;
LogicItem.prototype.createTopLayer = LogicItem_createTopLayer;
LogicItem.prototype.setFocus = LogicItem_setFocus;
LogicItem.prototype.getMaxNumFocusCells = LogicItem_getMaxNumFocusCells;

// overrides
LogicItem.prototype.hide = LogicItem_hide;
LogicItem.prototype.show = LogicItem_show;
LogicItem.prototype.buildCells = LogicItem_buildCells;
LogicItem.prototype.createCell = LogicItem_createCell;
LogicItem.prototype.loadModelProperties = LogicItem_loadModelProperties;
LogicItem.prototype.setCellValue = LogicItem_setCellValue;
LogicItem.prototype.getColumnDescriptors = LogicItem_getColumnDescriptors;
LogicItem.prototype.moreCSS = LogicItem_moreCSS;
LogicItem.prototype.render = LogicItem_render;
LogicItem.prototype.innerRender = LogicItem_innerRender;
LogicItem.prototype.moreLaunch = LogicItem_moreLaunch;
LogicItem.prototype.clearView = LogicItem_clearView;
LogicItem.prototype.hitTest = LogicItem_hitTest;
LogicItem.prototype.moreDestroy = LogicItem_moreDestroy;

// event handlers
LogicItem.prototype.mouseupCallback = LogicItem_mouseupCallback;
LogicItem.prototype.propertyChangeCallback = LogicItem_propertyChangeCallback;
LogicItem.prototype.onchangeCallback = LogicItem_onchangeCallback;
LogicItem.prototype.onKeyPressCallback = LogicItem_onKeyPressCallback;

/*  czOptItm.js 115.12 2001/05/04 13:25:18 cjeyapra ship $ */

function OptionItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'OptionItem';
}

function OptionItem_getColumnDescriptors (bUseImg, bNoIcon)
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {
    var index = 0;  
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    if (! bNoIcon) {
      this.columnDescriptors[index] = new ColumnDescriptor ('state', 'icon', index, 16, 20, 2, 0, 'top', 'left', true, false);
      index++;
    }
    if (bUseImg) {
      //use image for description;
      this.columnDescriptors[index] = new ColumnDescriptor ('descImg', 'descImg', index, 22, 50, 0, 0, 'top', 'left', true, true);
      
    } else {
      //display description;
      this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, 22, 50, 1, 2, 'top', 'left', true, true);
    }
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

//Extend from ListItem
HTMLHelper.importPrototypes(OptionItem, LogicItem);

// LogicItem overrides
OptionItem.prototype.getColumnDescriptors = OptionItem_getColumnDescriptors;


/*  czCntOpt.js 115.18 2001/05/10 22:19:29 cjeyapra ship $ */

function CountedOptionItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'CountedOptionItem';
}

function CountedOptionItem_getColumnDescriptors (bUseImg, bNoIcon)
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    var index = 0;
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    if (! bNoIcon) {
      this.columnDescriptors[index] = new ColumnDescriptor ('state', 'icon', index, 16, 20, 2, 0, 'top', 'left',true, false);
      index++;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('count', 'inputtext', index, 22, 50, 1, 0, 'top', 'left', true, false);
    index++;
    if (bUseImg) {
      //use image for description;
      this.columnDescriptors[index] = new ColumnDescriptor ('descImg', 'descImg', index, 22, 50, 0, 0, 'top', 'left', true, true);

    } else {
      //display description;
      this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, 22, 50, 1, 2, 'top', 'left', true, true);
    }
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function CountedOptionItem_mouseupCallback (e, eSrc)
{
  if (this.locked) {
    if (eSrc.type == 'inputtext')
      return false;
    else
      return true;
  }
  self.cntObj = this;

  // grow the height of the item to allow for the rendering of;
  // the input form element of inputtext for NS.;
  if (eSrc.type == 'inputtext') {
    if (ns4 && (this.height < 28)) {
      this.changeHeight();
      this.setHeight (28);
      this.eventManager.addListener ('docMouseDown', this);
      this.eventManager.addListener ('docEnterKeyUp', this);
    }	
  }
  else {
    if ((this.modelItem.getState() != 'utrue') && (this.modelItem.getState() != 'ltrue')) {
      this.modelItem.setState('utrue');
    }else{
      this.modelItem.setState('unknown');
    }
    this.notifyListeners ('actionMouseUp', this.modelItem, this);
  }
  return true;
}

function CountedOptionItem_docEnterKeyUp (e)
{
  if (this.oldHeight) {
    this.setHeight (this.oldHeight);
    this.resetHeight();
  }
  this.eventManager.removeListener ('docEnterKeyUp', this);
  return true;
}

function CountedOptionItem_docMouseDown (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var id = eSrc.id;
    if (! id) {
      if (eSrc.name)
        id = eSrc.name;
    }
    if (id == "editBox")
      return (true);
    else {
      //Reverse the old height for NS;
      if (this.oldHeight) {
        this.setHeight (this.oldHeight);
        this.resetHeight();
        this.eventManager.removeListener ('docMouseDown', this);
      }
    }
  }
  else {
    //Reverse the old height for NS;
    if (this.oldHeight) {
      this.setHeight (this.oldHeight);
      this.oldHeight = null;
      this.eventManager.removeListener ('docMouseDown', this);
    }
  }
  return (true);
}

function CountedOptionItem_resetHeight()
{
//  if ( this.listBorderClipped ){
  if (this.bLastCell) {
    this.setTop (this.oldTop);
    this.prevItem.setHeight (this.prevItem.oldHeight);
    this.prevItem.oldHeight = null;
  } else {
    this.nextItem.setTop (this.nextItem.oldTop);
    this.nextItem.setHeight (this.nextItem.oldHeight);
    this.nextItem.oldHeight = null;
    this.nextItem.oldTop = null;
    this.oldHeight = null;
  }
}

function CountedOptionItem_changeHeight(){
  this.oldHeight = this.height;
  this.oldTop = this.top;
  heightDiff = 28 - this.oldHeight;
//  if (this.listBorderClipped){
  if (this.bLastCell) {
    this.prevItem.oldHeight = this.prevItem.height;
    this.prevItem.setHeight(this.prevItem.height - heightDiff );
    this.setTop(this.top - heightDiff );
  } else {
    this.nextItem.oldTop = this.nextItem.top;
    this.nextItem.oldHeight = this.nextItem.height;
    this.nextItem.setTop (this.nextItem.top + heightDiff);
    this.nextItem.setHeight (this.nextItem.height - heightDiff);
  }
}

function CountedOptionItem_getMaxNumFocusCells()
{
  return 2;
}

//Extend from ListItem
HTMLHelper.importPrototypes(CountedOptionItem, LogicItem);

//Private Methods
CountedOptionItem.prototype.resetHeight = CountedOptionItem_resetHeight;
CountedOptionItem.prototype.changeHeight = CountedOptionItem_changeHeight;

// LogicItem overrides
CountedOptionItem.prototype.getColumnDescriptors = CountedOptionItem_getColumnDescriptors;
CountedOptionItem.prototype.getMaxNumFocusCells = CountedOptionItem_getMaxNumFocusCells;

//Event handlers
CountedOptionItem.prototype.mouseupCallback = CountedOptionItem_mouseupCallback;
CountedOptionItem.prototype.docMouseDown = CountedOptionItem_docMouseDown;
CountedOptionItem.prototype.docEnterKeyUp = CountedOptionItem_docEnterKeyUp;

/*  czBomItm.js 115.17 2001/07/02 15:02:57 snadkarn ship $ */

function BomItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'BomItem';
  this.bNoIcon = false;
  this.enableKeyBoardEvents();
  this.focCellInd = 1;
}

function BomItem_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  var ht = 0, wd = 0, pdgLt = 0, pdgTp = 0;
  if (this.columnDescriptors.length == 0) { 
    var index = 0; 
    // params - name, type, index, ht, wd, paddingLeft, paddingTop, valign, halign, defvis, spring;
    if (! this.bNoIcon) {
      wd = 20;
      pdgLt = 2;
      if (this._3DLook) {
        ht = 25;
        pdgTp = 5;
      } else if (this._flatLook) {
        ht = 16;
        pdgTp = 5;
      } else {
        ht = 16;
        pdgTp = 0;
      }
      this.columnDescriptors[index] = new ColumnDescriptor('state', 'icon', index, ht, wd, pdgLt, pdgTp, 'top', 'left', true, false);
      index++;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('count', 'inputtext', index, 22, 50, 1, 0, 'top', 'left', true, false);
    index++;

    ht = 25;
    wd = 50;
    pdgLt = 1;
    if (this._3DLook) {
      pdgTp = 2;
    } else if (this._flatLook) {
      pdgTp = 2;
    } else {
      pdgTp = 4;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, ht, wd, pdgLt, pdgTp, 'top', 'left', true, true);
    index++;

    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 10, pdgLt, pdgTp, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function BomItem_mouseoverCallback (e, eSrc)
{
  return true;
}

function BomItem_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function BomItem_updateDone()
{
  if(this.hFeature)
      this.hide();
  else
      this.show();
}

function BomItem_onLaunchCall()
{
  this.updateDone();
}

function BomItem_mouseoutCallback (e, eSrc)
{
  return true;
}

function BomItem_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, wd, ht)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = wd;
  this.imageHeight = ht;
}

function BomItem_moreCSS ()
{
  var CSSStr = '';
  
  if (!this.cellsBuilt) {
    this.buildCells(this.getColumnDescriptors());
  }
  this.setModelItem (this.modelItem);
  this.updatePreferredWidth ();
  this.updateColumnPosition();
  this.fixDimensions();

  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    CSSStr += this.cells[i].generateCSS() + '\n';
  }
  if (this.unSatIcon) {
    CSSStr += this.unSatIcon.generateCSS() + '\n';
  }
  if (this.bNoIcon) {
    this.createTopLayer();
    CSSStr += this.topLay.generateCSS();
  }
  return CSSStr;
}

//will be set only when it is 3D look
function BomItem_setBorderColor (col)
{
  if (! (this._3DLook || this._flatLook))
    return;

  if (col != null) {
    this.backgroundColor = col;
    
    if(this.launched) 
      HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
  } 
}

function BomItem_setBackgroundColor (col)
{
  if(col != null) {
    this.cellBgColor = col;
    if (! this._3DLook) {
      this.backgroundColor = col;
      if(this.launched) 
        HTMLHelper.setBackgroundColor(this.getHTMLObj(),col);
    }
    if (this.launched) {
      for (var i=0; i<this.cells.length; i++) {
        this.cells[i].setBackgroundColor (col);
      }
    }
  }
}

function BomItem_getBackgroundColor (col)
{
  return (this.itemBgColor);
}

function BomItem_updatePreferredWidth ()
{
  // find the preferred pixel dimensions;
  var colDesc = null;
  var prefWidth = 0;
  var prefHeight = 0;
  var charWidth, charHeight;
  
  if (this.font) {
    charWidth = this.font.getCharWidth ();
    charHeight = this.font.getCharHeight ();
  } else {
    charWidth = 10;
    charHeight = 12;
  }
  if (isNaN(charWidth)) {
    charWidth = 10;
  }
  if (isNaN(charHeight)) {
    charHeight = 12;
  }
  var len = this.columnDescriptors.length;
  for (var i = 0; i < len; i++) {
    colDesc = this.columnDescriptors[i];
    if ((colDesc.name == 'state') || (colDesc.name == 'text'))
      continue;

    if (colDesc.name == 'atp') {
      prefWidth = 11 * charWidth;
      prefHeight = charHeight; 
    } else if (colDesc.name == 'price') {
      prefWidth = 6 * charWidth;
      prefHeight = charHeight;
    } else if (colDesc.name == 'count') {
      prefWidth = 5 * charWidth;
      prefHeight = charHeight;
    }
    colDesc.setPrefWidth (prefWidth);
    colDesc.setPrefHeight (prefHeight);
  }
}

function BomItem_getMaxNumFocusCells()
{
  this.numFocus = 2;
  return this.numFocus;
}

function BomItem_onKeyDownCallback (e, eSrc)
{
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 9:
    if (e.shiftKey) {
      //move focus backwards
      if (this.focCellInd == 1) {
        this.eventManager.focusBackward(e, this);
        return;
      } else {
        this.focCellInd--;
        this.setFocus(null);
      }
    } else {
      //move focus forwards
      if (this.focCellInd == 2) {
        this.eventManager.focusForward(e, this);
        this.focCellInd = 1;
        return;
      } else {
        this.focCellInd++;
        this.setFocus(this.focCellInd);
      }
    }
    e.returnValue = false;
    break;
  case 32:
    this.mouseupCallback (e, eSrc);
    e.returnValue = false;
    break;
  }
  return true;
}

//Extend from ListItem
HTMLHelper.importPrototypes(BomItem, LogicItem);

// class methods
BomItem.getPreferredColumnDescriptors = BomItem.getPreferredColumnDescriptors;

// overrides
BomItem.prototype.setBorderColor = BomItem_setBorderColor;
BomItem.prototype.setBackgroundColor = BomItem_setBackgroundColor;
BomItem.prototype.getBackgroundColor = BomItem_getBackgroundColor;
BomItem.prototype.getColumnDescriptors = BomItem_getColumnDescriptors;
BomItem.prototype.setImages = BomItem_setImages;
BomItem.prototype.moreCSS = BomItem_moreCSS;
BomItem.prototype.hideFeature = BomItem_hideFeature;
BomItem.prototype.updateDone = BomItem_updateDone;
BomItem.prototype.onLaunchCall = BomItem_onLaunchCall;
BomItem.prototype.updatePreferredWidth = BomItem_updatePreferredWidth;
BomItem.prototype.getMaxNumFocusCells = BomItem_getMaxNumFocusCells;

//Event handlers
BomItem.prototype.mouseoverCallback = BomItem_mouseoverCallback;
BomItem.prototype.mouseoutCallback = BomItem_mouseoutCallback;
BomItem.prototype.onKeyDownCallback = BomItem_onKeyDownCallback;

/*  czCombo.js 115.28 2001/07/06 21:25:58 snadkarn ship $ */
 
function ComboBox ()
{
  this.parentConstructor = Base;		
  this.parentConstructor ();

  this.width = 100;
  this.height = 20;
  this.backgroundColor = 'black';
  this.listModel = new ListModel (ListModel.SELECT_SINGLE);

  this.built = false;
  this.dropdown = null;
  this.dropdownClass = 'List';
  this.editCell = null;
  this.editCellClass = 'ListItem';
  this.btnImage = null;

  //register for events;
  this.listModel.addListener ('propertyChangeCallback', this);
  this.listModel.addListener ('objectAddedCallback', this);
  this.listModel.addListener ('objectRemovedCallback', this);
}

function ComboBox_updateDone()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.dropdown.updateDone();
  }
}

function ComboBox_buildControl ()
{
  if (this.built) return;
  this.built = true;

  eval(this.self +'=this');
  var ctl = this.editCell = eval ('new ' + this.editCellClass + '();');
  ctl.setName (this.name + 'xEdit');
  ctl.setDimensions (1, 1, this.width-16, this.height-2);
  ctl.setParentObject (this);
  ctl.enableKeyBoardEvents();
  ctl.setTabindex(this.tabindex);
  ctl.setBackgroundColor (this.itemBackColor);
  ctl.setSelectedBackColor (this.itemBackColor);
  ctl.setActiveBackColor (this.itemBackColor);
  ctl.setWindowName(this.windowName);
  ctl.setSpanClass (this.spanClass);
  if (this.selItemModel)
    ctl.setModelItem (this.selItemModel);
  else
    ctl.setModelItem (null);
  ctl.addListener ('onKeyDownCallback', this);

  ctl = this.btnImage = new ImageButton ();
  ctl.setName (this.name + 'xImg');
  ctl.setDimensions (this.width-17, 0, 16, this.height);
  ctl.setParentObject (this);
  ctl.stretch = true;
  if(ctl.arrowOn == null){
    ctl.arrowOn = IMAGESPATH+'czDArr.gif';
  }
  if(ctl.arrowOff == null){
    ctl.arrowOff = IMAGESPATH+'czDArr1.gif'
  }
  ctl.setImages (ctl.arrowOn, ctl.arrowOff);
  ctl.addListener('mouseupCallback',Arrow_MouseUpCallback);
  ctl.addListener('mousedownCallback',Arrow_MouseDownCallback);

  //Register with child objects to listen to events;
  this.btnImage.addListener ('mouseupCallback', this);
  
  //create dropdown list;
  ctl = this.dropdown = eval ('new ' + this.dropdownClass + '();');
  if(this.read)
    ctl.readOnly(this.read);
  ctl.setName (this.name + 'xLst');
  ctl.setDimensions (this.left, this.top+this.height+1, this.width, 10);
  ctl.setReference (this.reference);
  ctl.setBackgroundColor (this.itemBackColor);
  ctl.setActiveBackColor (this.activeBackColor);
  ctl.setSelectedBackColor (this.selectedBackColor);
  ctl.setSpanClass (this.spanClass);
  ctl.font = this.font;
  ctl.setVisibleRowCount (5);
  ctl.setModel (this.listModel);
  ctl.setEventManager (this.eventManager);
  ctl.mode = List.MODE_DROPDOWN;
  this.hideDropDown();
  if (this.moreInitializeDropdown) {
    this.moreInitializeDropdown();
  }
  this.dropdown.addListener ('actionMouseUp', this);
  this.dropdown.addListener ('actionOnChange', this);
}

function ComboBox_propertyChangeCallback (change)
{
  if (change.property == 'selected') {
    if (change.value == true) {
      this.editCell.setModelItem (change.source);
    }
    this.hideDropDown();
    //remove if this is still registered with event mgr; 
    this.eventManager.removeListener ('docMouseUp', this);
    //notify listeners;
    this.notifyListeners ('selectionChangeCallback', change, this);
  }
  return (true);
}

function ComboBox_objectAddedCallback (object)
{
  this.notifyListeners ('objectAddedCallback', object, this);
  return true;
}

function ComboBox_moreSetWidth (wd)
{
  if (this.built) {
    var iWidth = wd - this.btnImage.width;
    this.editCell.setWidth (iWidth - 1);
    this.btnImage.setLeft (iWidth);
    this.dropdown.setWidth (this.width);
  }
}

function ComboBox_moreSetHeight (ht)
{
  if (this.built) {
    this.editCell.setHeight (ht-2);
    this.btnImage.setHeight (ht);
    this.dropdown.setTop (this.top + this.height + 1);
  }
}

function ComboBox_setBackgroundColor (col)
{
  this.itemBackColor = col;
  if (this.built) {
    this.editCell.setBackgroundColor (col);
    this.dropdown.setBackgroundColor (col);
  }
}

function ComboBox_getBackgroundColor ()
{
  return (this.editCell.getBackgroundColor ());
}

function ComboBox_moreCSS ()
{
  if (! this.built) {
    this.buildControl ();
  }
  var CSSStr = '';
  this.editCell.setWindowName(this.windowName);
  this.dropdown.setWindowName(this.windowName);
  CSSStr += this.editCell.generateCSS() + '\n';
  CSSStr += this.btnImage.generateCSS() + '\n';
  CSSStr += this.dropdown.generateCSS();
  if (this.unSatIcon)
    CSSStr += this.unSatIcon.generateCSS () + '\n';  
  return CSSStr;
}

function ComboBox_render()
{
  var sBuffer = '';

  sBuffer += '<DIV ';
  if (this.objId != '')
    sBuffer += 'ID=' + this.objId;
  
  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';
  
  sBuffer +='>' + '\n';
  sBuffer += this.editCell.render ();
  sBuffer += this.btnImage.render ();
  sBuffer += '</DIV>' + '\n';
  sBuffer += this.dropdown.render ();
  if (this.unSatIcon)
    sBuffer += this.unSatIcon.render () + '\n';
  return sBuffer;
}

function ComboBox_onLaunchCall()
{
    this.updateDone();
}

function ComboBox_moreLaunch (em)
{			
  this.editCell.launch (em);
  this.btnImage.launch (em);
  this.dropdown.launch (em);
  if (this.unSatIcon)
    this.unSatIcon.launch ();
}

function ComboBox_docMouseUp (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var eSrcName = (ns4) ? eSrc.name : eSrc.id;
    if (eSrcName == ('IMG-'+this.btnImage.name))
      return (true);
  }
  if (this.windowName) {
    var xpos = (ns4) ? e.pageX : (e.clientX+self._hwnd[this.windowName].document.body.scrollLeft);
    var ypos = (ns4) ? e.pageY : (e.clientY+self._hwnd[this.windowName].document.body.scrollTop);
  } else {
    var xpos = (ns4) ? e.pageX : (e.clientX + document.body.scrollLeft);
    var ypos = (ns4) ? e.pageY : (e.clientY + document.body.scrollTop);
  }
  //Check this x and y coordiante in inside the list;
  //if inside dont do anything else hide dropdown if it is open;
  if (this.dropdown) {
    if (! this.dropdown.isMouseInside (xpos, ypos)) {
      if (this.dropdown.opened) {
        this.hideDropDown();
	this.eventManager.removeListener ('docMouseUp', this);
      }
    }
  }
  return (true);	
}

function ComboBox_mouseupCallback (e, source)
{
  if (source.name == this.btnImage.name) {
    if (this.dropdown) {
      //Event occured on arrow image bring the dropdown;
      if (!this.dropdown.opened) {
        this.showDropDown ();
        //add to event manager listener;
        this.eventManager.addListener ('docMouseUp', this);
      } else {
        //drop down already open hide it;
        this.hideDropDown();
        this.eventManager.removeListener ('docMouseUp', this);
      }
    }
  }
  return (true);
}

function ComboBox_showDropDown ()
{
  /*
    var winHt;
  if (ie4) {
    winHt = eval (this.reference + '.body.offsetHeight');
  } else if (ns4) {
    var str = this.reference.replace (/document/i, 'window');
    winHt = eval (str + '.innerHeight');
  }
  if (winHt < (this.dropdown.top + this.dropdown.height)) {
    this.dropdown.moveTo (this.dropdown.left, this.top - this.dropdown.height-1);
  }
  */
  //Layout the dropdown columns if it is not done yet;
  if ((! this.bDropdownLaidout) && (navigator.platform.indexOf('MacPPC') == -1 )) {
    this.dropdown.fixDimensions ();
    this.bDropdownLaidout = true;
  }
  if(this.dropdown.unWantedItemsList.length > 0)
  {
     if(this.dropdown.oldHeight == null )
     {
       this.dropdown.oldHeight = this.dropdown.getHeight();
     }
     if(this.dropdown.unWantedItemsList.length > this.dropdown.activeItemsListScroll.length)
       this.dropdown.setHeight(this.dropdown.activeItemsListScroll.length*22+15);
     else
      this.dropdown.setHeight(this.dropdown.oldHeight);
  }
  else
  {
     if(this.dropdown.oldHeight != null )
     {
       this.dropdown.setHeight(this.dropdown.oldHeight);
       this.dropdown.oldHeight = null;
     }
  }
  this.dropdown.show ();
  this.dropdown.setZIndex (8888);
  this.dropdown.opened = true;
}

function ComboBox_hideDropDown ()
{
  this.dropdown.hide ();
  this.dropdown.setZIndex (0);
  this.dropdown.opened = false;
}

function ComboBox_addItem (rtid, item, index, isVisible, isSelected)
{
  if (typeof (item) == 'string') {
    var desc = item;
    item = new ListItemModel ();
    item.setValue ('text', desc);
  }
  if (isSelected) {
    this.selItemModel = item;
  }
  this.listModel.addItem (rtid, item, index, isVisible, isSelected);
  item.addListener ('propertyChangeCallback', this);
  return (item);
}

function ComboBox_setSelectedIndex (index) 
{
  if (this.dropdown)
    this.dropdown.setSelectedIndex (index);
}

function ComboBox_getSelectedIndex ()
{
  if (this.dropdown)
    return (this.dropdown.getSelectedIndex ());
}

function ComboBox_removeAll ()
{
  if (this.dropdown)
    this.dropdown.removeAll();
}

function ComboBox_removeItem (index)
{
  if (this.dropdown)
    this.dropdown.removeItem (index);
}

function ComboBox_getItemCount ()
{
  if (this.dropdown)
    return (this.dropdown.getItemCount());
}

function ComboBox_getItems ()
{
  if (this.dropdown) {
    return (this.dropdown.getItems());
  } else {
    return null;
  }
}

function ComboBox_setActiveBackColor (col)
{
  this.activeBackColor = col;
}

function ComboBox_getActiveBackColor ()
{
  return (this.activeBackColor);
}

function ComboBox_setSelectedBackColor (col)
{
  this.selectedBackColor = col;
}

function ComboBox_getSelectedBackColor ()
{
  return (this.selectedBackColor);
}

function ComboBox_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;

  this.font = fnt;	
  this.spanClass = this.name + 'SpnNml';
  
  style = this.getStyleObject (this.spanClass, true);
  this.setFontAttributes (style);
}

function ComboBox_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
  stylObj.setAttribute ('padding-left', 5);
}

function ComboBox_setEditCellClass (cls) {
  if (this.launched) return;
  this.editCellClass = cls;
}

function ComboBox_getEditCellClass () {
  return this.editCellClass;
}

function ComboBox_setDropdownClass (cls) {
  if (this.launched) return;
  this.dropdownClass = cls;
}

function ComboBox_getDropdownClass () {
  return this.dropdownClass;
}

function ComboBox_getSelectedItem () {
  if (this.dropdown)
    return this.dropdown.getSelectedItem();
}

function ComboBox_objectRemovedCallback (obj) {
  if (this.built)
    this.editCell.setModelItem (null);
}

function ComboBox_actionMouseUp (modelItem, modelView)
{
  this.notifyListeners ('actionMouseUp', modelItem, modelView);
  return true;
}

function ComboBox_actionOnChange (modelItem, modelView)
{
  this.notifyListeners ('actionOnChange', modelItem, modelView);
  return true;
}

function ComboBox_hitTest (e)
{
  if (e && this.dropdown.opened) {
    return this.dropdown.hitTest (e)
  }
  return (null);
}

function ComboBox_moreDestroy ()
{
  if (this.editCell) {
    this.editCell.destroy ();
    this.editCell = null;
    delete this.editCell;
    
    this.btnImage.destroy ();
    this.btnImage = null;
    delete this.btnImage;
  }  
  if (this.dropdown) {
    this.dropdown.destroy ();
    this.drodown = null;
    delete this.dropdown;
  }
  if (this.unSatIcon) {
    this.unSatIcon.destroy ();
    this.unSatIcon = null;
    delete this.unSatIcon;
  }
}

function ComboBox_setEventManager (em)
{
  this.eventManager = em;
}

function ComboBox_onKeyDownCallback (e, obj)
{
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 9:
    //tab key is pressed. Let the event bubble up
    if (this.dropdown.opened) {
      //bring the focus to the first visible item in the dropdown
    }
    return;
  case 38:
    this.keyboardScrollUp(false);
    break;
  case 40:
    this.keyboardScrollDown(false);
    break;
  case 46:
    //del key press. Remove this item from the dropdown and sent a event
    var curItem = this.editCell.getModelItem();
    if (curItem) {
      this.editCell.setModelItem(null);
      this.notifyListeners('actionMouseUp', curItem, curItem);      
    }
    break;
  }
  if (ie4) {
    e.returnValue = false;
  }
}

function ComboBox_keyboardScrollUp(bPgUp)
{
  var curItem = this.editCell.getModelItem();
  if (curItem) {
    var curIndex = curItem.index;
    if (curIndex == 0) {
      //we reached the first item no more to scroll up
      return;
    }
    var newIndex = 0;
    var newItem = null;
    if (bPgUp) {
      //TODO: do a page level scroll
    } else {
      //do a step scroll
      if (curIndex > 1)
        newIndex = curIndex-1;
    }
    while (newIndex >= 0) {
      var itm = this.listModel.items[newIndex];
      this.startTimer(itm);
      if (itm.isVisible()) {
        this.editCell.setModelItem(itm);
        break;
      } else
        newIndex--;
    }
  }
}

function ComboBox_keyboardScrollDown(bPgDown)
{
  var curItem = this.editCell.getModelItem();
  var len = this.listModel.items.length;
  var newIndex = 0;
  if (curItem) {
    var curIndex = curItem.index;
    if (curIndex == (len-1)) {
      //we reached end no more to scroll
      return;
    }
    if (bPgDown) {
      //TODO: do a page level scroll down

    } else {
      //do a step scroll
      newIndex = curIndex + 1;
    }
  }
  while (newIndex < len) {
    var itm = this.listModel.items[newIndex];
    this.startTimer(itm);
    if (itm.isVisible()) {
      this.editCell.setModelItem(itm);
      break;
    } else
      newIndex++;
  }
}

function ComboBox_startTimer(itm)
{
  if (this.timeoutHdl)
    clearTimeout (this.timeoutHdl);

  this.selItemModel = itm;
  var id = this.selItemModel.getName();
  this.timeoutHdl = setTimeout(this.self + '.synchSelection("'+id+'");', 500);
}

function ComboBox_synchSelection(id)
{
  if (this.selItemModel.getName() == id) {
    clearTimeout (this.timeoutHdl);
    //send event to server
    var itm = this.selItemModel;
    this.notifyListeners('actionMouseUp', itm, itm);
  }
}

HTMLHelper.importPrototypes(ComboBox,Base);

//public methods
ComboBox.prototype.setEditCellClass = ComboBox_setEditCellClass;
ComboBox.prototype.getEditCellClass = ComboBox_getEditCellClass;
ComboBox.prototype.setDropdownClass = ComboBox_setDropdownClass;
ComboBox.prototype.getDropdownClass = ComboBox_getDropdownClass;
ComboBox.prototype.addItem = ComboBox_addItem;
ComboBox.prototype.removeItem = ComboBox_removeItem;
ComboBox.prototype.removeAll = ComboBox_removeAll;
ComboBox.prototype.getSelectedIndex = ComboBox_getSelectedIndex;
ComboBox.prototype.setSelectedIndex = ComboBox_setSelectedIndex;
ComboBox.prototype.getSelectedItem = ComboBox_getSelectedItem;
ComboBox.prototype.getItemCount = ComboBox_getItemCount;
ComboBox.prototype.setBackgroundColor = ComboBox_setBackgroundColor;
ComboBox.prototype.getBackgroundColor = ComboBox_getBackgroundColor;	
ComboBox.prototype.setActiveBackColor = ComboBox_setActiveBackColor;
ComboBox.prototype.getActiveBackColor = ComboBox_getActiveBackColor;
ComboBox.prototype.setSelectedBackColor = ComboBox_setSelectedBackColor;
ComboBox.prototype.getSelectedBackColor = ComboBox_getSelectedBackColor;
ComboBox.prototype.setFont = ComboBox_setFont;
ComboBox.prototype.render = ComboBox_render;
ComboBox.prototype.hitTest = ComboBox_hitTest;
ComboBox.prototype.setEventManager = ComboBox_setEventManager;

//private methods
ComboBox.prototype.buildControl = ComboBox_buildControl;
ComboBox.prototype.moreSetWidth = ComboBox_moreSetWidth;
ComboBox.prototype.moreSetHeight = ComboBox_moreSetHeight;
ComboBox.prototype.moreCSS = ComboBox_moreCSS;
ComboBox.prototype.moreLaunch = ComboBox_moreLaunch;
ComboBox.prototype.mouseupCallback = ComboBox_mouseupCallback;
ComboBox.prototype.showDropDown = ComboBox_showDropDown;
ComboBox.prototype.hideDropDown = ComboBox_hideDropDown;
ComboBox.prototype.objectAddedCallback = ComboBox_objectAddedCallback;
ComboBox.prototype.propertyChangeCallback = ComboBox_propertyChangeCallback;
ComboBox.prototype.objectRemovedCallback = ComboBox_objectRemovedCallback;
ComboBox.prototype.setFontAttributes = ComboBox_setFontAttributes;
ComboBox.prototype.moreDestroy = ComboBox_moreDestroy;
ComboBox.prototype.keyboardScrollUp = ComboBox_keyboardScrollUp;
ComboBox.prototype.keyboardScrollDown = ComboBox_keyboardScrollDown;
ComboBox.prototype.startTimer = ComboBox_startTimer;
ComboBox.prototype.synchSelection = ComboBox_synchSelection;

ComboBox.prototype.docMouseUp = ComboBox_docMouseUp;
ComboBox.prototype.actionMouseUp = ComboBox_actionMouseUp;
ComboBox.prototype.actionOnChange = ComboBox_actionOnChange;
ComboBox.prototype.onKeyDownCallback = ComboBox_onKeyDownCallback;
ComboBox.prototype.updateDone = ComboBox_updateDone;
ComboBox.prototype.onLaunchCall = ComboBox_onLaunchCall;






/*  czLCombo.js 115.25 2001/05/10 22:19:36 cjeyapra ship $ */

function LogicComboBox ()
{
  this.parentConstructor = ComboBox;
  this.parentConstructor ();

  this.type = 'LogicComboBox';
  this.dropdownClass = 'LogicList';
  this.editCellClass = 'ComboEditCell';
}

function LogicComboBox_hideFeature(bHide)
{
  this.hFeature=bHide;
}

function LogicComboBox_propertyChangeCallback (change)
{
  if (change.property == 'state') {
    if ((change.value == 'utrue') || (change.value == 'ltrue')) {
      var itm = this.editCell.getModelItem();
      if ( itm !=null ){
      	if ( itm.getState() != 'unknown') {
          //clear the state of the old item temporarily;
          itm.clearState();
      	}
      }
      this.editCell.setModelItem (change.source);
    } else {
      var itm = this.editCell.getModelItem ();
      if (itm != null) {
      	if (itm == change.source && itm != null) {
          this.editCell.setModelItem (null);
      	}
      }
    }
    this.dropdown.hide ();
    //remove if this is still registered with event mgr; 
    this.eventManager.removeListener ('docMouseUp', this);
    this.dropdown.opened = false;
    //notify listeners;
    this.notifyListeners ('selectionChangeCallback', change);
  }
  return true;
}

function LogicComboBox_addOptionItem (rtid, item, index, state, price, atp, visible)
{
  if (typeof (item) == 'string') {
    var str = item;
    item = new LogicItemModel ();
    item.setText (str);
  }
  if (rtid) item.setName (rtid);
  if (state) {
    item.setState (state);
    if ((state == 'utrue') || (state == 'ltrue'))
      this.selItemModel = item;
  }
  if (price != null) item.setPrice (price);
  if (atp != null) item.setATP (atp);
  item.setVisibility(visible);
  this.listModel.addItem (rtid, item, index);
  item.addListener ('propertyChangeCallback', this);
  return item;
}

function LogicComboBox_moreInitializeDropdown ()
{
  this.dropdown.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  
  this.dropdown.setLogicStateProps (this.bNoIcon, this.bLogCol, this.bLogBld);
  this.dropdown.setPriceVisible (this.priceVisible);
  this.dropdown.setATPVisible (this.atpVisible);
}

function LogicComboBox_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
  if (this.dropdown) {
    this.dropdown.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
}

function LogicComboBox_setPriceVisible (bVis)
{
  this.priceVisible = bVis;
  if (this.built) {
    this.editCell.setPriceVisible (bVis);
    this.dropdown.setPriceVisible (bVis);
  }
}

function LogicComboBox_setATPVisible (bVis)
{
  this.atpVisible = bVis;
  if (this.built) {
    this.dropdown.setATPVisible (bVis);
  }
}

function LogicComboBox_setUnSatisfied (bVal)
{
  if (! this.unSatIcon) {
    var uicon = this.unSatIcon = new ImageButton ();
    uicon.setName (this.name+"xUIcn");
    var lpos = this.left + this.width + 8;
    var tpos = this.top + (this.height - 16)/2;
    uicon.setDimensions (lpos, tpos, 16, 16);
    uicon.setReference (this.reference);
    uicon.setSrc (IMAGESPATH + "czasterisk.gif");
    uicon.setToolTipText (self._unsatToolTip);
    if (this.launched) {
      var i = this.reference.lastIndexOf ("document");
      if (i == 0)
        var fr = window;
      else {
        var frStr = this.reference.substr (0, i-1);
        var fr = eval (frStr);
      }
      uicon.runtimeRender (fr, null, this.eventManager);
    }
  }
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.unSatIcon.show ();
  } else {
    this.unSatIcon.hide ();
  }
}

function LogicComboBox_setWindowName (wndName)
{
  this.windowName = wndName;
  if (this.dropdown) {
    this.dropdown.setWindowName (wndName);
  }
}

function LogicComboBox_setMaxColWidth (colName, colWd)
{
  this.listModel.setMaxColWidth (colName, colWd);
}

function LogicComboBox_setLogicStateProps (bNoIcons, bLogCol, bLogBld)
{
  if (this.launched)
    return;

  this.bNoIcon = bNoIcons;
  this.bLogCol = bLogCol;
  this.bLogBld = bLogBld
}


HTMLHelper.importPrototypes(LogicComboBox, ComboBox);

LogicComboBox.prototype.constructor  = LogicComboBox;
LogicComboBox.prototype.setImages = LogicComboBox_setImages;
LogicComboBox.prototype.addOptionItem = LogicComboBox_addOptionItem;
LogicComboBox.prototype.setPriceVisible = LogicComboBox_setPriceVisible;
LogicComboBox.prototype.setATPVisible = LogicComboBox_setATPVisible;
LogicComboBox.prototype.setUnSatisfied = LogicComboBox_setUnSatisfied;
LogicComboBox.prototype.setWindowName = LogicComboBox_setWindowName;
LogicComboBox.prototype.setMaxColWidth = LogicComboBox_setMaxColWidth;
LogicComboBox.prototype.hideFeature = LogicComboBox_hideFeature;
LogicComboBox.prototype.setLogicStateProps = LogicComboBox_setLogicStateProps;

// private methods
LogicComboBox.prototype.moreInitializeDropdown = LogicComboBox_moreInitializeDropdown;
LogicComboBox.prototype.propertyChangeCallback = LogicComboBox_propertyChangeCallback;



/**
* ComboEditCell - Edit cell class for the logic combo box.
*                 Will create a fixed width price column and a variable width
*                 description column (the desc text in it can be truncated).
*/

function ComboEditCell ()
{
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'ComboEditCell';
}

function ComboEditCell_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) { 
    var index = 0; 
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, this.height, this.width, 1, 2, 'top', 'left', true, true);
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, this.height, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function ComboEditCell_moreSetProperties (cell)
{
  if (cell.colName == 'price') {
    cell.noclip = true;
    cell.addListener ('captionChangeCallback', this);
  }
}

function ComboEditCell_captionChangeCallback (oldVal, cell)
{
  if (this.launched) { 			
    //resize the layer to the correct width and height
      var wd = (ns4)? cell.block.clip.width : cell.block.scrollWidth;
    if (cell.width < wd) {
      cell.setWidth (wd + 5);
      cell.setLeft (this.width - cell.width);
      this.layoutColumns ();
    }
  }
}

function ComboEditCell_layoutColumns() 
{
  var curCell = null;
  var lastVisCell = null;
  var springCell = null;
  var springColDesc = null;
  var colDesc = null;  
  var totWidth = this.width;
  var totWidthUpToSpringCell = 0;
  var cellsWidth = 0;
  var len = this.columnDescriptors.length;
  
  for (var i=0; i<len; i++) {
    colDesc = this.columnDescriptors [i];
    curCell = this.cells[i];
    if (curCell.isVisible ()) {
      curCell.setLeft (cellsWidth);
      // currently support one spring column;
      if (colDesc.spring) {
        springCell = curCell;    
        totWidthUpToSpringCell = cellsWidth;
      }
      cellsWidth += curCell.getWidth ();
    }
  }
  if (springCell) {
    cellsWidth -= totWidth;
    springCell.setWidth(springCell.getWidth() - cellsWidth);
    cellsWidth = totWidthUpToSpringCell + springCell.getWidth ();
    for (var i=springCell.index + 1; i<len; i++) {
      curCell = this.cells [i];
      curCell.setLeft (cellsWidth);
      cellsWidth += curCell.getWidth ();
    }
  } 
}

function ComboEditCell_mouseoverCallback (e, eSrc)
{
  return (true);
}

function ComboEditCell_mouseoutCallback (e, eSrc)
{
  return (true);
}

function ComboEditCell_mouseupCallback (e, eSrc)
{
  return (true);
}

// construction
HTMLHelper.importPrototypes(ComboEditCell, LogicItem);

//public methods
ComboEditCell.prototype.moreSetProperties = ComboEditCell_moreSetProperties;

// overrides
ComboEditCell.prototype.getColumnDescriptors = ComboEditCell_getColumnDescriptors;
ComboEditCell.prototype.layoutColumns = ComboEditCell_layoutColumns;

//Event handlers
ComboEditCell.prototype.mouseupCallback = ComboEditCell_mouseupCallback;
ComboEditCell.prototype.mouseoverCallback = ComboEditCell_mouseoverCallback;
ComboEditCell.prototype.mouseoutCallback = ComboEditCell_mouseoutCallback;
ComboEditCell.prototype.captionChangeCallback = ComboEditCell_captionChangeCallback;





/*  czLChkBx.js 115.10 2001/05/10 22:19:35 cjeyapra ship $ */
 
function LogicCheckBox ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.enableKeyPressEvents();
  this.type = 'LogicCheckBox';
}

function LogicCheckBox_hideFeature(bHide)
{
  this.hFeature = bHide;
}

function LogicCheckBox_onLaunchCall()
{
  if(this.hFeature)
    this.hide();
  else{
    if(!this.isVisible())
      this.show();
    this.updateDone();
  }
}

function LogicCheckBox_getColumnDescriptors ()
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    this.columnDescriptors[0] = new ColumnDescriptor ('state', 'icon', 0, 16, 20, 2, 0, 'top', 'left', true, false);
    this.columnDescriptors[1] = new ColumnDescriptor ('text', 'label', 1, 22, 50, 1, 2, 'top', 'left', true, true);
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function LogicCheckBox_setImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, wd, ht)
{
  this.srcUTrue = IMAGESPATH + srcUTrue;
  this.srcUFalse = IMAGESPATH + srcUFalse;
  this.srcLTrue = IMAGESPATH + srcLTrue;
  this.srcLFalse = IMAGESPATH + srcLFalse;
  this.srcUnknown = IMAGESPATH + srcUnknown;
  this.imageWidth = wd;
  this.imageHeight = ht;
}

function LogicCheckBox_mouseoverCallback (e, eSrc)
{
  return true;
}

function LogicCheckBox_mouseoutCallback (e, eSrc)
{
  return true;
}

function LogicCheckBox_moreSetProperties (cell)
{
  if ((cell.colName == 'price') || (cell.colName == 'atp')) {
    cell.noclip = true;
    cell.addListener ('captionChangeCallback', this);
  }
}

function LogicCheckBox_captionChangeCallback (oldVal, cell)
{
  if (this.launched) { 			
    //resize the layer to the correct width and height;
    var wd = (ns4)? cell.block.clip.width : cell.block.scrollWidth;
    if (cell.width < wd) {
      var prefWd = wd + 5;
      cell.setWidth (prefWd);
      var colDesc = this.columnDescriptors[cell.index];
      colDesc.setPrefWidth (prefWd);
      this.updateColumnPosition ();
      this.fixDimensions ();
    }
  }
}

function LogicCheckBox_moreLaunch (em)
{
  var len = this.cells.length;
  for (var i=0; i < len; i++) {
    this.cells[i].launch(em);
  }
  this.setModelItem (this.modelItem);
}

//Extend from ListItem
HTMLHelper.importPrototypes(LogicCheckBox, LogicItem);

// LogicItem overrides
LogicCheckBox.prototype.getColumnDescriptors = LogicCheckBox_getColumnDescriptors;
LogicCheckBox.prototype.setImages = LogicCheckBox_setImages;
LogicCheckBox.prototype.moreSetProperties = LogicCheckBox_moreSetProperties;
LogicCheckBox.prototype.moreLaunch = LogicCheckBox_moreLaunch;
LogicCheckBox.prototype.hideFeature = LogicCheckBox_hideFeature;
LogicCheckBox.prototype.onLaunchCall = LogicCheckBox_onLaunchCall;

//Event handlers
LogicCheckBox.prototype.mouseoverCallback = LogicCheckBox_mouseoverCallback;
LogicCheckBox.prototype.mouseoutCallback = LogicCheckBox_mouseoutCallback;
LogicCheckBox.prototype.captionChangeCallback = LogicCheckBox_captionChangeCallback;


/*  czTree.js 115.23 2001/06/18 19:08:08 cjeyapra ship $ */

function Tree ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.initializeControl ();
  this.nodeColl = new Array ();
  this.totalNodeCount = 0;  
  this.type = 'Tree';
  this.lastNodeBottom = 0;
  this.treeStyle = Tree.STYLE_TREE_LINES_AND_ICONS;
}

function Tree_initializeControl ()
{
  this.zIndex = 0;
  this.treeIcons = new Array ();
  this.treeIcons['pnode'] = IMAGESPATH + 'czPNode.gif';
  this.treeIcons['plast'] = IMAGESPATH + 'czPLNode.gif';
  this.treeIcons['mnode'] = IMAGESPATH + 'czMNode.gif';
  this.treeIcons['mlast'] = IMAGESPATH + 'czMLNode.gif';
  this.treeIcons['tnode'] = IMAGESPATH + 'czTNode.gif';
  this.treeIcons['last']  = IMAGESPATH + 'czLNode.gif';
  this.treeIconWidth = 16;
  this.treeIconHeight = 22;
  this.treeNodeHeight = 22;
}

function Tree_createBorder ()
{
  return (0);
}

function Tree_setReference (ref)
{
  this.reference = ref;
  if (this.root) {
    var n = this.root;
    if (ns4)
      n.setReference ((this.reference + ".layers['" + this.objId + "'].document"));
    else if (ie4)
      n.setReference (this.reference);
  }
}

function Tree_setTreeStyle (iStyle)
{
  if (this.launched) 
    return;
  this.treeStyle = iStyle;
}

function Tree_getTreeStyle ()
{
  return (this.treeStlyle);
}

function Tree_setSelectedBackColor (c)
{
  this.selectedBackColor = c;
  if (this.selectedNode) {
    this.selectedNode.hilite (c);
  }
}

function Tree_getSelectedBackColor (c)
{
  return (this.selectedBackColor);
}

function Tree_createBorder (borderWidth, borderColor)
{
	// override for future use
}

function Tree_setTransparent ()
{
  if (this.launched) return;
  this.bTransparent = true;
}

function Tree_isTransparent ()
{
  return (this.bTrasparent);
}

function Tree_addNode (relative, relationship, key, text, bUnSatisfied, bAvailable, iconNormal, iconSelected)
{
  if (ns4 && this.launched) {
    var ht = this.totalNodeCount * this.treeNodeHeight + 50;
    if (ht > this.height && (window.getUiBroker)) {
      //adding a new node will get clipped in NS now. Do not add a new node
      //get the ui broker and post refresh frames
      var ub = window.getUiBroker();
      if (ub != null) {
        ub.refreshTreeFrame();
	return;
      }
    }
  }
  var n;
  if (relative) {
    if (typeof (relative) == 'string') {
      var relKey = relative;
      relative = this.nodeColl[relKey];
    }
  }
  if (!relative && !relationship) {
    n = this.createRootNode (key, text, iconNormal, iconSelected);
  } else if (!relative) {
    if (this.root) {
      n = this.root.addNode (this.root, relationship, key, text, iconNormal, iconSelected);
    } else  {
      n = this.createRootNode (key, text, iconNormal, iconSelected);
    }
  } else if (relative == this.root) {
    n = this.root.addNode (this.root, relationship, key, text, iconNormal, iconSelected);
  } else if (relationship != Tree.AT_CHILD) {
    n = relative.parent.addNode (relative, relationship, key, text, iconNormal, iconSelected);
  } else {
    n = relative.addNode (relative, relationship, key, text, iconNormal, iconSelected);
  }
  this.nodeColl[n.key] = n;
  n.setRoot (this.root);
  n.setUnSatisfied (bUnSatisfied);
  n.setAvailability (bAvailable);
  n.setWindowName(this.windowName);
  //if (this.font) 
  //  n.setFont (this.font);
  n.addListener ('treeCollapseCallback', this);
  n.addListener ('treeExpandCallback', this);
  n.addListener ('mousedownCallback', this);
  n.addListener ('mouseupCallback', this);
  
  this.notifyListeners ('objectAddedCallback', n,this);
  this.totalNodeCount++;
  return n;
}

function Tree_addNodes (aryNodes)
{
  var len = aryNodes.length;
  for (var i=0; i<len; i++) {
    var n = aryNodes [i];
    this.addNode (n.relative, n.relationship, n.key, n.text, n.iconNormal, n.iconSelected);
  }
}

function Tree_createRootNode (key, text, iconNormal, iconSelected)
{
  this.root = new TreeNode ();
  this.root.setDimensions(0,0,this.width,22);
  this.root.setParentObject(this);
  this.root.setName(key);
  this.root.label.setValue(text);
  this.root.setToolTipText (text);
  this.root.setIconNormal(iconNormal);
  this.root.setIconSelected(iconSelected);
  this.root.setSpanNormal(this.spanNormal);
  this.root.setSpanSelected(this.spanSelected);
  this.root.setBackgroundColor(this.backgroundColor);
  this.root.setExpanded(true);
  this.root.setStyle(this.treeStyle);

  this.setReference (this.reference);
  if (this.launched) {
    this.root.bRoot = true;
    this.root.createLayer (this);
  }
  this.root.setAsRootNode = TreeNode_setAsRootNode;
  this.root.setAsRootNode ();

  return (this.root);
}

function Tree_removeNode (key)
{
  var n = null;
  n = this.nodeColl [key];
  if (n) {
    var nParent = n.getParent ();
    if (nParent) {
      nParent.removeNode (n);
      if (this.selectedNode.key == n.key) {
        this.selectedNode = null;
      }
      this.nodeColl[key] = null;
      delete this.nodeColl[key];
    } else {
      this.root.recursiveDelete ();
      this.root = null;
      delete this.root;
      this.nodeColl = new Array ();
      this.selectedNode = null;
    }
  }
}

function Tree_removeAll ()
{
  if (this.root)
    this.removeNode (this.root.key);
}

function Tree_getItem (key)
{
  if (this.nodeColl[key])
    return (this.nodeColl[key]);
  else
    return null;
}

function Tree_getItems ()
{
  if (this.nodeColl)
    return (this.nodeColl);
  else
    return null;
}

function Tree_setNodeIcons ()
{
  var len = arguments.length;
  for (var i=0; i < len; i = i+2) {
    if (!this.iconsHash) this.iconsHash = new Array();
    this.iconsHash [arguments[i]] = IMAGESPATH + arguments[i+1];
  }
}

function Tree_getNodeIconSrc (key)
{
  return (this.iconsHash[key]);
}

function Tree_getTreeIconSrc (key)
{
  return (this.treeIcons[key]);
}

function Tree_setSelectedNode (node)
{
  if (typeof (node) == 'string') {
    var key = node;
    node = this.getItem (key);
  }
  if (this.selectedNode)
    this.selectedNode.setSelected (false);
  node.setSelected (true);
  this.selectedNode = node;
}

function Tree_getSelectedNode ()
{
  if (this.selectedNode)
    return this.selectedNode;
  else
    return null;
}

function Tree_isSelectedNode (node)
{
  if (typeof (node) == 'string') {
    var key = node;
    node = this.getItem (key);
  }
  return (node.isSelected ());
}

function Tree_generateCSS()
{
  var CSSStr = "";
  
  CSSStr += "#" + this.objId + "{position:absolute; ";
  
  if(this.left != null)
    CSSStr += "left:" + this.left + "; ";
  
  if(this.top != null)
    CSSStr += "top:" + this.top + "; ";
  
  if(this.width != null)	
    CSSStr += "width:" + this.width + "; ";
  
  if(this.height != null) {
    var ht = this.totalNodeCount * this.treeNodeHeight + 30;
    if (this.height < ht) {
      this.height = ht;
    }
    CSSStr += "height:" + this.height + "; "
  }
  if(this.backgroundColor) {
    CSSStr+= "layer-background-color:"+ this.backgroundColor +"; ";
    CSSStr+= "background-color:"+ this.backgroundColor +"; ";
  }
  if(!this.noclip)	
    if(this.width && this.height) 
      CSSStr+= "clip:rect(0, " + (this.width) + ", " + (this.height) + ", 0); ";

  if (this.color)
    CSSStr += "color:" + this.color + "; ";
  
  if(this.zIndex!=null)
    CSSStr+= "z-index:" + this.zIndex + "; ";

  CSSStr += (this.visibility)? "visibility:inherit; " : "visibility:hidden; ";
  CSSStr += "}";
  
  if(this.moreCSS)
    CSSStr += "\n" + this.moreCSS();

  return(CSSStr);
}

function Tree_moreCSS ()
{
  var css = "";
  
  //this.fixTop ();

  if (this.root)
    css += this.root.generateCSS ();

  return (css);
}

function Tree_innerRender ()
{
  var sBuffer = "";
  if (this.root)
    sBuffer += '\n' + this.root.render ();
  
  return (sBuffer);
}

function Tree_moreLaunch (em)
{
  if (this.root) {
    this.root.launch (em);
    this.selectedNode = this.root;
    this.root.setSelected (true);
  }
  this.fixTop();
  if (em) {
    em.addListener ('onKeyDownCallback', this);
    em.addListener ('onKeyUpCallback', this);
  }
}

function Tree_fixTop ()
{
  var key;
  if (this.root) {
    this.lastNodeBottom  = this.root.getHeight ();
    this.root.fixTop ();
  }
}

function Tree_setFont (fnt)
{	
  var style;
  if (this.launched)
    return;
  
  this.font = fnt;
  
  this.spanNormal = this.name + 'SpnNml';
  this.spanSelected = this.name + 'SpnSel';
  
  style = this.getStyleObject (this.spanNormal, true);
  this.setFontAttributes (style);
  
  style = this.getStyleObject (this.spanSelected, true);
  this.setFontAttributes (style);
}

function Tree_setFontAttributes (stylObj)
{
  stylObj.setAttribute ('font-family', this.font.family);
  stylObj.setAttribute ('font-size', this.font.size);
  stylObj.setAttribute ('font-style', this.font.style);
  stylObj.setAttribute ('font-weight', this.font.weight);
}

function Tree_setColor (col)
{
  if (this.launched)
    return;
  this.color = col;
  this.spanNormal = this.name + 'SpnNml';
  var styl = this.getStyleObject (this.spanNormal, true);
  styl.setAttribute ('color', col);
}

function Tree_setSelectedColor (col)
{
  if (this.launched)
    return;
  this.selectedColor = col;
  this.spanSelected = this.name + 'SpnSel';
  var styl = this.getStyleObject (this.spanSelected, true);
  styl.setAttribute ('color', col);
}

function Tree_mousedownCallback (e, src)
{
  var rtnVal = true;
  rtnVal = this.notifyListeners ('mousedownCallback', e, src);

  if (self.editObj){
    if (self.editObj.oldVal != self.editObj.textElement.value){
      self.ub.pendingEvt = true;
      self.ub.pendingObj = src;
      self.ub.evtType = "<click rtid='" + self.ub.formatRuntimeId(src.getName()) +  "'/>";
      self.editObj.doBlur();
      rtnVal = false;
    }
    if (self.cntObj){
      self.cntObj.resetHeight();
      self.cntObj = null;
    }
  }
  if (rtnVal) {
    rtnVal = this.notifyListeners ('beforeNodeClickCallback', e, src);
    if (rtnVal) {
      if (this.selectedNode)
        this.selectedNode.setSelected (false);
      src.setSelected (true);
      this.selectedNode = src;
      rtnVal = this.notifyListeners ('nodeClickCallback', e, src);
    }
  }
  return rtnVal;
}

function Tree_mouseupCallback (e, src)
{
  return (this.notifyListeners ('mouseupCallback', e, src));
}

function Tree_treeCollapseCallback (e, src)
{
  //TODO: Fetch data and update tree
  return true;
}

function Tree_treeExpandCallback (e, src)
{
  //TODO: Fetch data and update tree
  return true;
}

function Tree_onKeyDownCallback (e, eSrc)
{
  var n = null;
  var key = (ns4)? e.which : e.keyCode;
  switch (key) {
  case 38:
    n = this.selectedNode.getPreviousVisibleNode();
    break;
  case 40:
    n = this.selectedNode.getNextVisibleNode ();
    break;
  case 13:
    if (this.selectedNode.children.length) {
      if (this.selectedNode.expanded)
        this.selectedNode.collapse();
      else
        this.selectedNode.expand();
      this.selectedNode.propogateStateChange ();
    }
    break;
  }
  if (n) {
    this.setSelectedNode (n);
    self._tree = this;
    self._treeSelNode = n;
    if (this.timeoutHdl)
      clearTimeout (this.timeoutHdl);
    this.timeoutHdl = setTimeout("Tree_synchNodeSelection()", 500);
  }
  switch (key) {
  case 13:
  case 38:
  case 40:
    if (ie4) {
      e.cancelBubble = true;
      e.returnValue = false;
    }
  }
}

function Tree_onKeyUpCallback (e, eSrc)
{

}

function Tree_synchNodeSelection(nodeId)
{
  var tree = self._tree;
  var selNode = self._treeSelNode;
  if (selNode == tree.getSelectedNode()) {
    selNode.notifyListeners('mouseupCallback', null, selNode);
    self._tree = null;
    self._treeSelNode = null;
    clearTimeout(tree.timeoutHdl);
  }
}

function Tree_moreDestroy ()
{
  if (this.nodeColl) {
    for (var id in this.nodeColl) {
      this.nodeColl[id].destroy ();
      this.nodeColl [id] = null;
      delete this.nodeColl[id];
    }
    this.nodeColl = null;
    delete this.nodeColl;
    if (this.root) {
      this.root.destroy ();
      this.root = null;
      delete this.root;
    }
  }
}

//Static members
Tree.AT_FIRST = 0;
Tree.AT_LAST = 1;
Tree.AT_NEXT = 2;
Tree.AT_PREVIOUS = 3;
Tree.AT_CHILD = 4;

//Static members for tree lines and icon settings
Tree.STYLE_NONE = 0;
Tree.STYLE_TREE_LINES_ONLY = 1;
Tree.STYLE_ICONS_ONLY = 2;
Tree.STYLE_TREE_LINES_AND_ICONS = 3;

//Extend from Base
HTMLHelper.importPrototypes(Tree, Base);

Tree.prototype.constructor = Tree;
Tree.prototype.setReference = Tree_setReference;
Tree.prototype.setFont = Tree_setFont;
Tree.prototype.setTreeStyle = Tree_setTreeStyle;
Tree.prototype.getTreeStyle = Tree_getTreeStyle;
Tree.prototype.setColor = Tree_setColor;
Tree.prototype.setSelectedColor = Tree_setSelectedColor;
Tree.prototype.setSelectedBackColor = Tree_setSelectedBackColor;
Tree.prototype.getSelectedBackColor = Tree_getSelectedBackColor;

//has more overheads use only when requried
Tree.prototype.setTransparent = Tree_setTransparent;
Tree.prototype.isTransparent = Tree_isTransparent;
Tree.prototype.createBorder = Tree_createBorder;

Tree.prototype.addNode = Tree_addNode;
Tree.prototype.addNodes = Tree_addNodes;
Tree.prototype.removeNode = Tree_removeNode;
Tree.prototype.removeAll = Tree_removeAll;
Tree.prototype.getItem = Tree_getItem;
Tree.prototype.getItems = Tree_getItems;
Tree.prototype.setSelectedNode = Tree_setSelectedNode;
Tree.prototype.isSelectedNode = Tree_isSelectedNode;
Tree.prototype.getSelectedNode = Tree_getSelectedNode;

Tree.prototype.setNodeIcons = Tree_setNodeIcons;
Tree.prototype.getNodeIconSrc = Tree_getNodeIconSrc;
Tree.prototype.getTreeIconSrc = Tree_getTreeIconSrc;

Tree.prototype.moreCSS  = Tree_moreCSS;
Tree.prototype.generateCSS  = Tree_generateCSS;
Tree.prototype.innerRender  = Tree_innerRender;
Tree.prototype.moreLaunch = Tree_moreLaunch;
Tree.prototype.moreDestroy = Tree_moreDestroy;

//private methods
Tree.prototype.initializeControl = Tree_initializeControl;
Tree.prototype.createRootNode = Tree_createRootNode;
Tree.prototype.fixTop = Tree_fixTop;
Tree.prototype.setFontAttributes = Tree_setFontAttributes;

Tree.prototype.mousedownCallback = Tree_mousedownCallback;
Tree.prototype.mouseupCallback = Tree_mouseupCallback;
Tree.prototype.onKeyDownCallback = Tree_onKeyDownCallback;
Tree.prototype.onKeyUpCallback = Tree_onKeyUpCallback;
Tree.prototype.treeCollapseCallback = Tree_treeCollapseCallback;
Tree.prototype.treeExpandCallback = Tree_treeExpandCallback;

/*  czTNode.js 115.24 2001/06/22 15:28:38 snadkarn ship $ */

function TreeNode ()
{
  this.parentConstructor = Base;
  this.parentConstructor ();

  this.initializeControl ();
  this.children = new Array ();
  
  this.type = 'TreeNode';
  this.expanded = false;
  this.bRoot = false;
  this.bTLineVisible = true;
  this.bIconVisible = true;
  this.bAvailable = true;
}

function TreeNode_initializeControl ()
{
  this.height = 22;
  this.zIndex = 2;
  
  this.collapser = new ImageButton ();
  this.icon = new ImageButton ();
  this.label = new Prompt ();
  
  this.collapser.setLeft (0);
  this.collapser.setHeight (22);
  this.collapser.setWidth (16);
  this.collapser.setTop (Math.round ((this.height-this.collapser.height)/2));
  this.collapser.setParentObject (this);
  this.collapser.stretch = true;
  
  this.icon.setLeft (this.collapser.left + this.collapser.width);
  this.icon.setWidth (16);
  this.icon.setHeight (22);
  this.icon.setTop (Math.round ((this.height-this.icon.height)/2));
  this.icon.setParentObject (this);
  this.icon.setSrc (IMAGESPATH + 'czfldcls.gif');
  
  this.label.setParentObject (this);
  this.label.setLeft (this.icon.left + this.icon.width + 5);
  this.label.setTop (0);
  this.label.setHeight (this.height);
  this.label.setWidth (20);
  this.label.noclip = true;
  this.label.onresizecall = TreeNode_onResizeCallback;
  
  this.label.addListener ('mousedownCallback', this);
  this.label.addListener ('mouseupCallback', this);
  this.label.addListener ('doubleclickCallback', this);
  this.icon.addListener ('mousedownCallback', this);
  this.icon.addListener ('mouseupCallback', this);
  this.icon.addListener ('doubleclickCallback', this);
  this.collapser.addListener ('mousedownCallback', this);
}

function TreeNode_moreSetName (nm)
{
  this.key = nm; 
  this.label.setName (this.name + 'Lbl');
  if (this.bIconVisible)
    this.icon.setName (this.name + 'Icon');
  if (this.bTLineVisible)
    this.collapser.setName (this.name + 'Clpsr');
}

function TreeNode_setCaption (val)
{
  this.label.setValue (val);
}

function TreeNode_getCaption ()
{
  return (this.label.getValue ());
}

function TreeNode_moreSetBackgroundColor (col)
{
  this.label.setBackgroundColor (col);
  if (this.bTLineVisible)
    this.collapser.setBackgroundColor (col);
  if (this.bIconVisible)
    this.icon.setBackgroundColor (col);
}

function TreeNode_moreSetHeight (ht)
{
  this.label.setHeight (this.height);
  if (this.bTLineVisible)
    this.collapser.setTop (Math.round ((this.height-this.collapser.height)/2));
  if (this.bIconVisible)		
    this.icon.setTop (Math.round ((this.height-this.icon.height)/2));
}

function TreeNode_setStyle (iStyle)
{
  if (iStyle == Tree.STYLE_NONE) {
    this.bIconVisible = false;
    this.bTLineVisible = false;
    this.icon.destroy ();
    this.icon = null;
    this.collapser = null;
    this.label.setLeft (3);
  } else if (iStyle == Tree.STYLE_ICONS_ONLY) {
    this.bTLineVisible = false;
    this.collapser.destroy ();
    this.collapser = null;
    this.icon.setLeft (0);
    this.label.setLeft (this.icon.width + 3);
  } else if (iStyle == Tree.STYLE_TREE_LINES_ONLY) {
    this.bIconVisible = false;
    this.icon.destroy ();
    this.icon = null;
    this.label.setLeft (this.collapser.width + 3);
  }
}

function TreeNode_setSpanNormal (classId)
{
  this.spanNormal = classId;
  this.label.spanClass = classId;
}

function TreeNode_setSpanSelected (classId)
{
  this.spanSelected = classId;
}

function TreeNode_setIconNormal (iconName)
{
  this.iconNormal = iconName;
  if (this.bIconVisible && (! this.bSelected)) {
    var src = this.parentObj.getNodeIconSrc (iconName);
    this.icon.setSrc (src);
  }
}

function TreeNode_setIconSelected (iconName)
{
  this.iconSelected = iconName;
  if (this.bIconVisible && this.bSelected) {
    var src = this.parentObj.getNodeIconSrc (iconName);
    this.icon.setSrc (src);
  }
}

function TreeNode_setIcon (iconName)
{
  if (!this.icon) return;
  var src = this.parentObj.getNodeIconSrc (iconName);
  this.icon.setSrc (src);
}

function TreeNode_setTreeIcon (iconName)
{
  if (!this.collapser) return;
  this.treeIcon = iconName;
  var src = this.parentObj.getTreeIconSrc (iconName);
  this.collapser.setSrc (src);
}

function TreeNode_reverseTreeIcon ()
{
  if (!this.bTLineVisible) return;
  if (this.bRoot) return;
  if (this.treeIcon.charAt (0) == 'm')
    this.treeIcon = this.treeIcon.replace (/m/i, 'p');
  else
    this.treeIcon = this.treeIcon.replace (/p/i, 'm');
  var src = this.parentObj.getTreeIconSrc (this.treeIcon);
  this.collapser.setSrc (src);
}

function TreeNode_addNode (relative, relationship, key, text, iconNormal, iconSelected) 
{
  var left;
  if (this.parentObj.treeStyle == Tree.STYLE_NONE) {
    left = this.left + 5;
  } else if (this.parentObj.treeStyle == Tree.STYLE_ICONS_ONLY) {
    left = (this.left + this.icon.left + this.icon.width);
  } else if (this.parentObj.treeStyle == Tree.STYLE_TREE_LINES_ONLY) {
    if (this.bRoot)
      left = this.left + 5;
    else
      left = (this.left + this.collapser.left + this.collapser.width);
  } else if (this.parentObj.treeStyle == Tree.STYLE_TREE_LINES_AND_ICONS) {
    left = (this.left + this.icon.left + this.icon.width/2) -
    this.parentObj.treeIconWidth/2;
  }


  // get the tree node label width
  var textWidth = 0;

  if ( this.font )
  {
    textWidth = this.font.getTextWidth(text,true)  
  }

  if ( this.width < textWidth )
    this.width = textWidth;

  var tmpWidth = left + textWidth;

  if (tmpWidth > this.parentObj.width)
    this.parentObj.setWidth (tmpWidth + 15);
  
  var n = new TreeNode ();
  n.setDimensions(left,this.top + this.height,this.width,this.height);
  n.setParentObject(this.parentObj);
  n.setName(key);
  n.setIconNormal(iconNormal);
  n.setIconSelected(iconSelected);
  n.setSpanNormal(this.spanNormal);
  n.setSpanSelected(this.spanSelected);
  n.setBackgroundColor(this.backgroundColor);
  n.setReference(this.reference);
  n.setCaption(text);
  n.setToolTipText (text);
  if (!this.expanded) n.hide();
  n.setStyle(this.parentObj.treeStyle);
  
  if (relationship == Tree.AT_FIRST) {
    this.insertNode (0, n);
  } else if (relationship == Tree.AT_LAST) {
    this.insertNode (this.children.length, n);
  } else if (relationship == Tree.AT_NEXT) {
    this.insertNode (relative.index + 1, n);
  } else if (relationship == Tree.AT_PREVIOUS) {
    this.insertNode (relative.index, n);
  } else if (relationship == Tree.AT_CHILD) {
    this.insertNode (this.children.length, n);
  }

  if (this.launched) {
    n.createLayer (this.parentObj);
    this.parentObj.lastNodeBottom = n.parent.top + n.parent.height;
    this.expand();
    this.refresh();
    this.propogateStateChange();
    /*
    if (this.expanded) {
      this.expand();
      this.refresh ();
      //n.parent.fixTop();
      this.propogateStateChange ();
    } else {
      this.refresh();
      n.hide();
    }
    */
  }
  return n;
}

function TreeNode_removeNode (node)
{
  var prev = node.getPrevious ();
  var next = node.getNext ();
  if (this.launched) {
    if (this.bTLineVisible && node.isLastNode() && prev) {
      prev.treeLine = node.treeLine;
      prev.treeLine.setHeight (prev.top -(prev.parent.top + prev.parent.height/2));
      node.treeLine = null;
      delete node.treeLine;
    }
  }
  var index = node.index;
  var len = this.children.length;
  if (index == len-1) {
    this.children[index] = null;
    delete this.children[index];
  } else {
    for (var i=index; i<(len-1); i++) {
      this.children[i] = this.children[i+1];
      this.children[i].index = i;
    }
    this.children[len-1] = null;
    delete this.children[len-1];
  }
  this.children.length--;
  
  if (this.launched) {
    node.recursiveDelete ();
    this.parentObj.lastNodeBottom = node.top;
    if (next) {
      next.moveTo (next.left, this.parentObj.lastNodeBottom);
      if (next.treeLine) {
        next.treeLine.setTop (next.parent.top + next.parent.height);
        next.treeLine.setHeight (next.top - (next.parent.top + next.parent.height));
      }
      this.parentObj.lastNodeBottom += next.height;
      next.fixTop ();
      next.propogateStateChange ();
    } else {
      this.parentObj.lastNodeBottom = this.top + this.height;
      this.fixTop ();
      this.refresh ();
      this.propogateStateChange ();
    }
  }
}

function TreeNode_recursiveDelete ()
{
  if (this.launched) {
    this.hide ();
    if (ie4) {
      this.getHTMLObj().innerHTML = "";
      this.getHTMLObj().outerHTML = "";
    }
    if (this.treeLine)
      this.treeLine.hide ();
  }
  if (this.getChildren()) {
    for (var i=this.children.length-1; i>=0; ) {
      this.children[i].recursiveDelete ();
      this.children[i] = null;
      delete this.children[i];
      this.children.length--;
      i = this.children.length-1;
    }
  }
}

function TreeNode_insertNode (index, node)
{
  var len = this.children.length;
  
  node.setParent (this);
  
  if (index == len) {
    this.children[len] = node;
    this.children[len].index = len;
    return;
  }
  if (index <= 0) {
    index = 0;
  }
  for (var i=(len-1); i >= index; i--) {
    this.children[i+1] = this.children[i];
    this.children[i+1].index = (i+1);
  }
  this.children [index] = node;
  this.children[index].index = index;
}

function TreeNode_createLayer (par)
{	
  var tree = this.parentObj;
  //ToDo: Rework this later whent we inherit this from container;
  //this is set to bypass more lauch method call;
  this.bRuntimeCreation = true;

  this.runtimeRender (null, tree, tree.eventManager);
  this.setZIndex (this.zIndex);
  
  this.label.runtimeRender (null, this, tree.eventManager);
  this.label.setValue (this.label.caption);
  
  if (this.bIconVisible) {
    this.icon.runtimeRender (null, this, tree.eventManager);
  }
  if (this.bTLineVisible) {
    this.collapser.runtimeRender (null, this, tree.eventManager);
  }
  if (this.bTLineVisible && this.isLastNode ()) {
    if (this.getPrevious()) {
      var p = this.getPrevious ();
      this.treeLine = p.treeLine;
      this.treeLine.setHeight (this.top-(this.parent.top + this.parent.height));
      p.treeLine = null;
      delete p.treeLine;
    } else {
      this.createTreeLine ();
      this.treeLine.runtimeRender (null, this.parentObj, tree.eventManager);
      this.treeLine.setZIndex (this.treeLine.zIndex);
    }
  }
}

function TreeNode_createTreeLine ()
{
  this.treeLine = new Base ();
  this.treeLine.setName(this.name + 'tvLine');
  var tp = this.parent.top + this.parent.height/2
  this.treeLine.setDimensions(this.left + this.collapser.width/2 - 1, tp, 1, this.top - tp);
  this.treeLine.setBackgroundColor('#003D84');
  this.treeLine.setParentObject(this.parentObj);
  this.treeLine.setZIndex(1);
  this.treeLine.hide ();
}

function TreeNode_setAsRootNode ()
{
  this.bRoot = true;
  this.collapser = null;
  if (this.bIconVisible) {
    this.icon.setLeft (0);
    this.label.setLeft (this.icon.width + 3);
  } else
    this.label.setLeft (0);
}

function TreeNode_getChildren () 
{
  if (this.children.length == 0)
    return null;
  else
    return (this.children);
}

function TreeNode_getParent () 
{
  if (this.parent)
    return (this.parent);
  else
    return null;
}

function TreeNode_setParent (parent) 
{
  this.parent = parent;
}

function TreeNode_getRoot () 
{
  if (this.root) 
    return this.root;
  else
    return null;
}

function TreeNode_setRoot (root) 
{
  this.root = root;
}

function TreeNode_getPrevious ()
{
  var n = null;
  if (this.parent) {
    if (this.index)
      n = this.parent.children[this.index-1];
  }
  return n;
}

function TreeNode_getNext ()
{
  var n = null;
  if (this.isLastNode())
    return null;

  if (this.parent)
    n = this.parent.children[this.index+1];
  
  return n;
}

function TreeNode_isFirstNode ()
{
  if (this.index == 0) 
    return true;
  return false;
}

function TreeNode_isLastNode ()
{
  if (this.bRoot) return false;
  return (this.index == this.parent.children.length-1);
}

function TreeNode_setExpanded (bExpand) {
  this.expanded = bExpand;
  if (this.launched) {
    this.reverseTreeIcon ();
    if (this.expanded) {
      this.parentObj.lastNodeBottom = this.top + this.height;
      this.fixTop ();
    } else {
      for (var i=0; i<this.children.length; i++) {
        var n = this.children[i];
        if (n.treeLine)
          n.treeLine.hide ();
        n.recursiveHide ();
      }
    }
  }
}

function TreeNode_recursiveHide ()
{
  this.hide ();
  var len  = this.children.length;
  if (this.expanded) {
    for (var i=0; i<len; i++) {
      n = this.children[i];
      if (n.treeLine)
        n.treeLine.hide ();
      n.recursiveHide ();
    }
  }
}

function TreeNode_recursiveShow ()
{
  this.show ();
  if (this.expanded) {
    for (var i=0; i<this.children.length; i++) {
      n = this.children[i];
      n.recursiveShow ();
    }
  }
}

function TreeNode_expand () 
{
  this.setExpanded (true);
}

function TreeNode_collapse () 
{
  this.setExpanded (false);
}

function TreeNode_setSelected (bSel)
{
  if (bSel) {
    var col = this.parentObj.selectedBackColor;
    if (col)
      this.label.hilite (col);
    else
      this.label.hilite ('lightgrey');
    this.label.setSpanClass (this.spanSelected);
    this.label.setValue (this.label.getValue());
    if (this.iconSelected)
      this.setIcon (this.iconSelected);

    //Expand the tree if one of the child node is selected
    var ptNodeAry = new Array();
    var parentNode = null;
    for (var i=0,parentNode = this.getParent(); parentNode != null; i++,parentNode = parentNode.getParent()) {
      ptNodeAry[i] = parentNode;
    }
    for (var i=ptNodeAry.length-1; i>=0; i--) {
      parentNode = ptNodeAry[i];
      if (! parentNode.expanded) {
        parentNode.expand();
        parentNode.propogateStateChange();
        parentNode.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this);
      }
    }
  } else {
    if (this.bSelected) {
      this.label.unhilite ();
      this.label.setSpanClass (this.spanNormal);
      this.label.setValue (this.label.getValue());
      if (this.iconNormal)
        this.setIcon (this.iconNormal);
    }
  }
  this.bSelected = bSel;
}

function TreeNode_isSelected ()
{
  return (this.bSelected);
}

function TreeNode_ensureVisible ()
{
  
}

function TreeNode_setUnSatisfied (bVal)
{
  this.bUnSatisfied = bVal;
  if (bVal) {
    this.setIconNormal ('closed-unsatisfied');
    this.setIconSelected ('open-unsatisfied');
    if (this.bIconVisible && ie4)
      this.icon.setToolTipText (this.getCaption() + " " + self._unsatToolTip);
  } else {
    this.setIconNormal ('closed');
    this.setIconSelected ('open');
    if (this.bIconVisible && ie4)
      this.icon.setToolTipText (this.getCaption());
  }
}

function TreeNode_moreCSS ()
{
  var CSSStr = "";
  
  CSSStr += this.label.generateCSS () + '\n';
  if (this.bIconVisible)
    CSSStr += this.icon.generateCSS () + '\n';
  
  if (this.bTLineVisible && (this.isLastNode()) && (!this.bRoot)) {
    this.createTreeLine ();
    CSSStr += this.treeLine.generateCSS () + '\n';
  }  
  if (this.bTLineVisible && (! this.bRoot))
    CSSStr += this.collapser.generateCSS () + '\n';
  
  for (var i=0; i<this.children.length; i++)
    CSSStr += this.children[i].generateCSS ();
  
  return CSSStr;
}

function TreeNode_render () 
{
  var sBuffer = '';
  
  sBuffer += '<DIV ';
  if (this.objId != '')
    sBuffer += 'ID=' + this.objId;
  
  if(this.divClass) 
    sBuffer += ' CLASS="' +this.divClass+'"';

  sBuffer += '>' + '\n';

  if (this.bTLineVisible && (! this.bRoot))
    sBuffer += this.collapser.render () + '\n';
  if (this.bIconVisible)
    sBuffer += this.icon.render () + '\n';
  sBuffer += this.label.render () + '\n';
  sBuffer += '</DIV>' + '\n';

  if (this.treeLine)
    sBuffer += this.treeLine.render () + '\n';
  
  for (var i=0; i<this.children.length; i++)
    sBuffer += this.children[i].render ();
  
  return sBuffer;
}

function TreeNode_moreLaunch (em)
{
  //ToDo: rework this later;
  if (this.bRuntimeCreation) return;

  if (this.bTLineVisible && (! this.bRoot))
    this.collapser.launch (em);
  
  if (this.bIconVisible)
    this.icon.launch (em);
  
  this.label.launch (em);
  this.label.setCaption (this.label.caption);
  
  if (this.treeLine)
    this.treeLine.launch ();
  
  for (var i=0; i<this.children.length; i++)
    this.children[i].launch (em);
}

function TreeNode_fixTop ()
{
  if (this.children.length) {
    if (this.expanded) {
      var childs = this.children;
      var len = childs.length;
      for (var i=0; i<len; i++) {
        var n = childs [i];
        if (n.isAvailable()) {
          n.moveTo (n.getLeft(), this.parentObj.lastNodeBottom);
          n.show ();
          if (n.treeLine) {
            n.treeLine.setTop (n.parent.top + n.parent.height);
            n.treeLine.setHeight (n.top - (n.parent.top + n.parent.height));
            n.treeLine.show();
          }
          this.parentObj.lastNodeBottom += n.getHeight ();
          n.refresh ();
          if (n.expanded)
            n.fixTop ();
        } else {
          //check if this node has treeLine then it should move;
          //to previous available node;
          if (n.treeLine) {
            var prv = n.getPreviousAvailableNode();
            if (prv) {
              prv.treeLine = n.treeLine;
              n.treeLine = null;
              prv.treeLine.setTop (prv.parent.top + prv.parent.height);
              prv.treeLine.setHeight (prv.top - (prv.parent.top + prv.parent.height));
              prv.treeLine.show();
            } else
              n.treeLine.hide();
          }
          n.hide();
        }
      }
    }
  }
  if (this.parentObj.lastNodeBottom > this.parentObj.height)
    this.parentObj.setHeight (this.parentObj.lastNodeBottom + 30);
}

function TreeNode_refresh ()
{
  if (this.hasAvailableChildren()) {
    if (this.getNextAvailableNode())
      key = 'node';
    else
      key = 'last';
    if (this.expanded) 
      key = 'm' + key;
    else
      key = 'p' + key;
  } else if (this.getNextAvailableNode ())
    key = 'tnode';
  else
    key = 'last';
  this.setTreeIcon (key);
}

function TreeNode_selected(curNode,selNode)
{
  if(selNode == null)
	return false;

  var parNode = selNode.parent;
  var found = true;
  
  if(parNode == null)
    return false;

  if( curNode.getName() == parNode.getName())
      found = true;
  else {
    found = false;
    if(this.selected(curNode,parNode))
      return true;
  }
  return found;
}

function TreeNode_mousedownCallback (e, src) 
{
  if ((src == this.label) || (src == this.icon)) {
    return (this.notifyListeners ('mousedownCallback', e, this));
  } else if (src == this.collapser) {
    if (this.expanded) {
      this.collapse ();
      this.propogateStateChange ();
      if (this.selected(this, this.parentObj.selectedNode)) {
        if (self.ub)
          self.ub.postClientMessage("<click rtid='" + self.ub.formatRuntimeId(this.getName()) +  "'/>");
        this.parentObj.setSelectedNode(this);
      }
      return (this.notifyListeners ('treeCollapseCallback', 'treeCollapseEvent', this));
    } else {
      this.expand ();
      this.propogateStateChange ();
      return (this.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this));
    }
  }
}

function TreeNode_mouseupCallback (e, src)
{
  return (this.notifyListeners ('mouseupCallback', e, this ));
}

function TreeNode_doubleclickCallback (e, eSrc)
{
  if (this.expanded) {
    this.collapse ();
    this.propogateStateChange ();
    return (this.notifyListeners ('treeCollapseCallback', 'treeCollapseEvent', this));
  } else {
    this.expand ();
    this.propogateStateChange ();
    return (this.notifyListeners ('treeExpandCallback', 'treeExpandEvent', this ));
  }
}

function TreeNode_propogateStateChange ()
{
  if (!this.expanded)
    this.parentObj.lastNodeBottom = this.getTop() + this.getHeight ();
  if (this.bRoot) return;
  var n = this.getNextAvailableNode ();
  if (n) {
    n.moveTo (n.getLeft(), this.parentObj.lastNodeBottom);
    if (n.treeLine) {
      n.treeLine.setTop (n.parent.top + n.parent.height);
      n.treeLine.setHeight (n.top - (n.parent.top + n.parent.height));
      n.treeLine.show();
    }
    this.parentObj.lastNodeBottom += n.getHeight ();
    n.fixTop ();
    n.propogateStateChange ();
  } else if (this.getParent())
    this.getParent().propogateStateChange ();
  else return;
}

function TreeNode_onResizeCallback (obj)
{
  var wd = 0;
  var node = obj.getParentObject ();
  if (node.collapser)
    wd += node.collapser.width;
  if (node.icon)
    wd += node.icon.width + 5;
  wd += obj.width;
  if (wd > node.width)
    node.setWidth (wd);
  
  if (node.left + node.width > node.parentObj.width)
    node.parentObj.setWidth (node.left + node.width + 15);
}

function TreeNode_moreDestroy ()
{
  if (this.icon) {
    this.icon.destroy ();
    this.icon = null;
    delete this.icon;
  }
  if (this.collapser) {
    this.collapser.destroy ();
    this.collapser = null;
    delete this.collapser;
  }
  if (this.label) {
    this.label.destroy ();
    this.label = null;
    delete this.label;
  }
  if (this.treeLine) {
    this.treeLine.destroy ();
    this.treeLine = null;
    delete this.treeLine;
  }
}

function TreeNode_setAvailability (bAvail)
{
  if (bAvail != null) {
    this.bAvailable = bAvail;
    if (this.launched) {
      if (! bAvail)
        this.recursiveHide();
      if (this.bRoot) {
        this.parentObj.lastNodeBottom = this.top + this.height; 
        this.fixTop ();
      } else {
        var next = this.parent.getNext();
        //if this is the last available node and if it does not have treeline;
        //get back the tree line object;
        if (! this.getNextAvailableNode()) {
          if (! this.treeLine) {
            var prev = this.getPreviousTreeLineNode();
            if ( prev != null){
              this.treeLine = prev.treeLine;
              prev.treeLine = null;
            } else {
              var nxtTLineNode = this.getNextTreeLineNode();
              //no need to check for null
              this.treeLine = nxtTLineNode.treeLine;
              nxtTLineNode.treeLine = null;
            }
          }
        }
        this.parentObj.lastNodeBottom = this.parent.top + this.parent.height;
        if ( this.parent.isVisible() ){
          this.parent.fixTop();
          this.parent.refresh();
          this.parent.propogateStateChange();
        }
        if (this.parent.bRoot && next)
          next.propogateStateChange();
      }
    }
  }
}

function TreeNode_isAvailable ()
{
  return this.bAvailable;
}

function TreeNode_getPreviousAvailableNode ()
{
  var prv = this.getPrevious();
  while (prv) {
    if (prv.isAvailable())
      return prv;
    else
      prv = prv.getPrevious();
  }
  return null;
}

function TreeNode_getNextAvailableNode ()
{
  var next = this.getNext();
  while (next) {
    if (next.isAvailable())
      return next;
    else
      next = next.getNext();
  }
  return null;
}

function TreeNode_hasAvailableChildren ()
{
  if (this.children.length) {
    var len = this.children.length;
    for (var i=0; i<len; i++) {
      if (this.children[i].isAvailable ())
        return true;
    }
  }
  return false;
}

//returns the node that has the tree line associated with it currently
function TreeNode_getPreviousTreeLineNode ()
{
  var prv = this.getPrevious();
  while (prv) {
    if (prv.treeLine)
      return prv;
    else
      prv = prv.getPrevious();
  }
  return null;
}

function TreeNode_getNextTreeLineNode ()
{
  var nxt = this.getNext();
  while (nxt) {
    if (nxt.treeLine)
      return nxt;
    else
      nxt = nxt.getNext();
  }
}

function TreeNode_setToolTipText(toolTipText)
{
  if (this.icon)
    this.icon.setToolTipText (toolTipText);
}

function TreeNode_getNextVisibleNode(stIndex)
{
  if (this.expanded && this.children.length) {
    var len = this.children.length;
    if (stIndex == null)
      var stIndex = 0;
    for (var i=stIndex; i<len; i++) {
      if (this.children[i].isAvailable ())
        return this.children[i];
    }
  }
  if (this.parent)
    return this.parent.getNextVisibleNode(this.index+1);
}

function TreeNode_getPreviousVisibleNode()
{
  var n = this.getPreviousAvailableNode();
  if (n) {
    while (true) {
      if (n.expanded && n.children.length) {
        var len = n.children.length;
        for (var i=len-1; i>=0; i--) {
          if (n.children[i].isAvailable()) {
            n = n.children[i];
            break;
          }
        }
      } else
        return n;
    }
  }
  if (this.parent)
    return this.parent;

  return null;
}


//Extend from Base
HTMLHelper.importPrototypes(TreeNode, Base);

TreeNode.prototype.constructor = TreeNode;
TreeNode.prototype.setCaption = TreeNode_setCaption;
TreeNode.prototype.getCaption = TreeNode_getCaption;
TreeNode.prototype.setSpanNormal = TreeNode_setSpanNormal;
TreeNode.prototype.setSpanSelected = TreeNode_setSpanSelected;
TreeNode.prototype.setIconNormal = TreeNode_setIconNormal;
TreeNode.prototype.setIconSelected = TreeNode_setIconSelected;
TreeNode.prototype.setStyle = TreeNode_setStyle;

TreeNode.prototype.addNode = TreeNode_addNode;
TreeNode.prototype.removeNode = TreeNode_removeNode;
TreeNode.prototype.getChildren = TreeNode_getChildren;
TreeNode.prototype.getParent = TreeNode_getParent;
TreeNode.prototype.setParent = TreeNode_setParent;
TreeNode.prototype.getRoot = TreeNode_getRoot;
TreeNode.prototype.setRoot = TreeNode_setRoot;
TreeNode.prototype.getPrevious = TreeNode_getPrevious;
TreeNode.prototype.getNext = TreeNode_getNext;
TreeNode.prototype.isFirstNode = TreeNode_isFirstNode;
TreeNode.prototype.isLastNode = TreeNode_isLastNode;
TreeNode.prototype.setExpanded = TreeNode_setExpanded;
TreeNode.prototype.expand = TreeNode_expand;
TreeNode.prototype.collapse = TreeNode_collapse;
TreeNode.prototype.ensureVisible = TreeNode_ensureVisible;
TreeNode.prototype.setSelected = TreeNode_setSelected;
TreeNode.prototype.isSelected = TreeNode_isSelected;
TreeNode.prototype.selected = TreeNode_selected;
TreeNode.prototype.setToolTipText = TreeNode_setToolTipText;
TreeNode.prototype.setUnSatisfied = TreeNode_setUnSatisfied;
TreeNode.prototype.setAvailability = TreeNode_setAvailability;
TreeNode.prototype.isAvailable = TreeNode_isAvailable;

TreeNode.prototype.moreCSS  = TreeNode_moreCSS;
TreeNode.prototype.render  = TreeNode_render;
TreeNode.prototype.moreLaunch = TreeNode_moreLaunch;
TreeNode.prototype.moreDestroy = TreeNode_moreDestroy;

//private methods
TreeNode.prototype.moreSetName = TreeNode_moreSetName;
TreeNode.prototype.moreSetHeight = TreeNode_moreSetHeight;
TreeNode.prototype.moreSetBackgroundColor = TreeNode_moreSetBackgroundColor;
TreeNode.prototype.initializeControl = TreeNode_initializeControl;
TreeNode.prototype.setIcon = TreeNode_setIcon;
TreeNode.prototype.setTreeIcon = TreeNode_setTreeIcon;
TreeNode.prototype.insertNode = TreeNode_insertNode;
TreeNode.prototype.fixTop = TreeNode_fixTop;
TreeNode.prototype.refresh = TreeNode_refresh;
TreeNode.prototype.reverseTreeIcon = TreeNode_reverseTreeIcon;
TreeNode.prototype.recursiveHide = TreeNode_recursiveHide;
TreeNode.prototype.recursiveShow = TreeNode_recursiveShow;
TreeNode.prototype.propogateStateChange = TreeNode_propogateStateChange;
TreeNode.prototype.createLayer = TreeNode_createLayer;
TreeNode.prototype.createTreeLine = TreeNode_createTreeLine;
TreeNode.prototype.recursiveDelete = TreeNode_recursiveDelete;
TreeNode.prototype.getPreviousAvailableNode = TreeNode_getPreviousAvailableNode;
TreeNode.prototype.getNextAvailableNode = TreeNode_getNextAvailableNode;
TreeNode.prototype.hasAvailableChildren = TreeNode_hasAvailableChildren;
TreeNode.prototype.getPreviousTreeLineNode = TreeNode_getPreviousTreeLineNode;
TreeNode.prototype.getNextTreeLineNode = TreeNode_getNextTreeLineNode;
TreeNode.prototype.getNextVisibleNode = TreeNode_getNextVisibleNode;
TreeNode.prototype.getPreviousVisibleNode = TreeNode_getPreviousVisibleNode;

//Event handlers
TreeNode.prototype.mousedownCallback = TreeNode_mousedownCallback;
TreeNode.prototype.mouseupCallback = TreeNode_mouseupCallback;
TreeNode.prototype.doubleclickCallback = TreeNode_doubleclickCallback;

/*  czUIBkr.js 115.77 2001/07/28 13:00:52 snadkarn ship $ */
 
function UiBroker ()
{
  this.sessionId = null;
  this.proxyRef = 'parent.frames.czProxy';
  this.contentMode = 'empty';
  this.contentTarget = 'czContent';
  this.appExited = false;  
  this.pageHistory = new Stack ();
  this.curSeqNum = -1;
  this.readOnly = false;
  this.back= false;
  this.register=false;
  this.fromPublishedPage=true;
  this.isFirstTime=true;
  this.heartMax = null;
  this.atpShown = false;
  this.showOn = true;
  //used mainly in NS to refresh tree frame when things gets clipped
  this.numRefreshTree = 0;
  this.charSetEncoding = "UTF-8";

  //default logic state icons;
  this.bNoStateIcons = false;
  this.srcUTrue = 'czbolut.gif';
  this.srcUFalse = 'czboluf.gif';
  this.srcLTrue = 'czbollt.gif';
  this.srcLFalse = 'czbollf.gif';
  this.srcUnknown = 'czbolun.gif';
  this.imageWidth = 16;
  this.imageHeight = 16;

  //logic state color & boldness;
  this.bUseLogicColor = false;
  this.bUseLogicBold = false;
  
  this.msgTxt = "";
  //UI Frames related counters
  this.wndCnt = 0;
  this.ctrlCnt = 0;
  this.wndCol = "";
  this.ctrlCol = "";
  this.txtCol = "";
  this.uiLook = "blaf";
  this._3DLook = false;

  // back behavour for ie4
  this.cnt=0;
  this.backSeqNum = 0;

  this.tabindex = 1;

  // mac related variable
  this.needRefresh = false;
  this.alreadyPublished = false;
}

//static members added here 'cause it is used down below in functions
UiBroker.NO_BORDER_LOOK = 0;
UiBroker.FLAT_LOOK = 1;
UiBroker._3DLOOK = 2;

function UiBroker_setSessionId (Id)
{
  this.sessionId = Id;
}

function UiBroker_getSessionId ()
{
  return this.sessionId;
}

function UiBroker_readOnlySession (read)
{
  this.readOnly = read;
}

function UiBroker_getReadOnly ()
{
  return this.readOnly;
}

function UiBroker_terminate()
{
  top.close();
}

function UiBroker_setMaxHeartBeat(count)
{
  this.heartMax = count;
  this.heartBeat = 0;
  this.counter = 0;
  if ( self.heartBeatId != null){
    clearTimeout(self.heartBeatId);
    self.heartBeatId = null
  }
  if ( this.heartMax != 0)
    this.startHB();
}

function UiBroker_getMaxHeartBeat()
{
  return this.heartMax;
}

function UiBroker_stopHB()
{
  this.heartBeat = 0;
  this.counter = 0;
  clearTimeout(self.heartBeatId);
  self.heartBeatId = null;
}

function UiBroker_startHB()
{
  if ( this.heartMax != null)
    self.heartBeatId = setInterval("ub.setHeartBeat()",this.heartMax);
}

function UiBroker_setHeartBeat()
{
  this.log ("Idle Time :"+this.heartBeat, null, new Date());
  this.heartBeat = (++this.counter)*10000;
  this.postClientHeartBeat('<heartbeat src="CLIENT"/>');
}

function UiBroker_getHeartBeat()
{
  return this.heartBeat;
}

function UiBroker_reSetHeartBeat()
{
  this.stopHB()
  this.startHB();
}

function UiBroker_postClientHeartBeat(xml)
{
  if (!this.appExited) {
    this.log ("Sending message to server '" + xml + "'", new Date());
    if ( !this.isFirstTime ){
      this.lockEventManagers();
      this.isFirstTime = false;
    }

    if (( (!this.showOn) && (! this.isEventManagerLocked()) ) || (this.msgTxt != "") ) {
      this.log ("Sending message to server '" + xml + "'", new Date());
      this.lockEventManagers();

      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
    
      if ( ns4 )
      {
    	  var sessionId = parent.frames.czHeartBeat.document.form1.sessionId.value;
        if ( sessionId == "" )
	      sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
      }
      else if (ie4)
      {
    	  var sessionId = parent.frames.czHeartBeat.form1.sessionId.value;
        if ( sessionId == "" )
	      sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
      }  

      var msg = "<client-event session-id='" + sessionId + "'>";
      msg += xml + "</client-event>";
      if (ns4) {
        parent.frames.czHeartBeat.document.form1.XMLmsg.value = msg;
        parent.frames.czHeartBeat.document.form1.submit ();
      } else if (ie4) {
        parent.frames.czHeartBeat.form1.XMLmsg.value = msg;
        parent.frames.czHeartBeat.form1.submit ();
      }
      if ( this.heartMax != 0 )
        this.reSetHeartBeat();	      
      return true;
    } 
    else {
      return false;
    }
  }
  return false;
}

function UiBroker_setDefaultLogicImages (srcUTrue, srcUFalse, srcLTrue, srcLFalse, srcUnknown, width, height)
{
  this.srcUTrue = srcUTrue;
  this.srcUFalse = srcUFalse;
  this.srcLTrue = srcLTrue;
  this.srcLFalse = srcLFalse;
  this.srcUnknown = srcUnknown;
  this.imageWidth = width;
  this.imageHeight = height;
}

function UiBroker_setLogicColors (utCol, ltCol, ufCol, lfCol, unCol)
{
  this.bUseLogicColor = true;
  this.utColor = utCol;
  this.ltColor = ltCol;
  this.ufColor = ufCol;
  this.lfColor = lfCol;
  this.unColor = unCol;
}

function UiBroker_setLogicBold (bUtBld, bLtBld, bUfBld, bLfBld, bUnBld)
{
  this.bUseLogicBold = true;
  this.utBold = bUtBld;
  this.ltBold = bLtBld;
  this.ufBold = bUfBld;
  this.lfBold = bLfBld;
  this.unBold = bUnBld;
}

function UiBroker_setNoStateIcons (bVal)
{
  this.bNoStateIcons = bVal;
}

function UiBroker_setMessageTarget (wndName)
{
  this.messageTarget = wndName;
}

function UiBroker_getMessageTarget ()
{
  //In summary mode use czContent frame as the error message target;
  if ((this.contentMode == 'summary') || (this.contentMode == 'empty')) 
    return ('czContent');
  else if (this.contentMode == 'config')
    return (this.messageTarget);
}

function UiBroker_registerRuntimeObject (wndName, obj)
{
  var rtId = null;
  if (obj.getModelItem)
    rtId = obj.getModelItem().getName();
  else 
    rtId = obj.getName ();
  
  if ((obj!=null) && rtId && (wndName != null)) {   
    this.runtimeObjectHash[wndName][rtId] = obj;
    //Do not hook callbacks for tree;
    if (obj.type == 'tree') return;
    
    switch (obj.type) {
    case 'BomItem':
      obj.addListener('actionMouseUp', this);
      obj.addListener('actionOnChange', this);
      break;
      
    case 'CountedOptionItem':
      obj.addListener('actionOnChange', this);
      break;
      
    case 'LogicCheckBox':
      obj.addListener ('actionMouseUp', this);
      break;
      
    case 'text':
      if(!this.getReadOnly()){
        obj.addListener('onchangeCallback', this);
      }
      break;
    case 'numerictext':
      if(!this.getReadOnly()){
        obj.addListener('onchangeCallback', this);
      }
      break;
    case 'image':
      break;
    case 'button':
      if(!this.getReadOnly())
        obj.addListener('onClick', this);
      break;
    case 'TreeNode':
      obj.addListener('mouseupCallback', this);
      break;
    }
  }
}

function UiBroker_deleteComponent(runtimeId)
{
  var obj = this.getRuntimeObject(runtimeId);
  if (obj != null) {
    if (obj.type == 'TreeNode') {
      var parentNode = obj.getParent();
      if (parentNode) {
        parentNode.removeNode(obj);
      }
    }
  }
}

function UiBroker_formatRuntimeId (runtimeId)
{
  return (runtimeId.substring(1, runtimeId.length));
}

function UiBroker_showSummary()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
	this.showOn = true;
      this.clearContentFrame ("czSummary.htm");

	if (this.showOn)
    	   this.showOn = false;

      //this.contentMode = 'summary';
      //this.postClientMessage ('<show-summary/>');
    }
  }
}

function UiBroker_showConfiguration()
{
  if (!this.appExited) {
    this.showOn = true;
    this.cnt = 0;
    this.backSeqNum = 0;
    if (! this.isEventManagerLocked ()) {
      this.clearContentFrame ();
      //reset to the original proxy source...;
      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
      proxy.document.location = this.sessionStartURL;
      this.atpShown = false;

      //this.contentMode = 'config';
      //For future reference, make this post to the 
      //content frame or something.
      //this.postClientMessage ('<show-config/>');
    }
  }
}

function UiBroker_getContentMode()
{
  return this.contentMode;
}

function UiBroker_showATP()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      if ( this.contentMode == 'config' ){
      	this.postClientMessage ('<show-atp/>');
	}
	else if ( this.contentMode == 'summary'){
  		var parentStr = '';
  		for (var i=0; i<this.depthHash[this.contentTarget]; i++) {
    			parentStr += 'parent.';
  		}
  		var doc = eval(parentStr + this.contentTarget);	
		if ( !this.atpShown ){
			doc.showAtp();
			this.atpShown = true;
		}
	}
    }
  }
}

function UiBroker_completeConfiguration () 
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<submit/>');
    }
  }
}

function UiBroker_cancelConfiguration ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<cancel/>');
    }
  }
}

//Future implementation
function UiBroker_configurationHelp ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<help page='+this.navCmd+'/>');
    }
  }
}

function UiBroker_saveConfiguration ()
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      this.postClientMessage('<save/>');
    }
  }
}

function UiBroker_clearContentFrame (withSrc)
{
  var parentStr = '';
  for (var i=0; i<this.depthHash[this.contentTarget]; i++) {
    parentStr += 'parent.';
  }
  var doc = eval(parentStr + this.contentTarget + ".document");	
  //redirect the content form...
  //doc.location = '/html/czCntnt.htm';
  if (withSrc){
    doc.location = SOURCEPATH + withSrc;    
  }
  else 
    doc.location = SOURCEPATH + 'czCntnt.htm';
  this.contentMode = 'empty';

}

function UiBroker_navigateToScreen (newScreen)
{
  if (!this.appExited) {
    if (! this.isEventManagerLocked ()) {
      var msg = "<navigate name='" + newScreen +  "'/>";
      this.postClientMessage(msg);
    }
  }
}

function UiBroker_mouseupCallback (e, eSrc)
{
  var rtId = null;
  if (eSrc.getModelItem)
    rtId = eSrc.getModelItem().getName();
  else
    rtId = eSrc.getName ();
 
  if (rtId) {
    var msg = "<click rtid='" + this.formatRuntimeId(rtId) +  "'/>";
    this.postClientMessage(msg);
  }
  return true;
}

function UiBroker_actionMouseUp (modelItem, modelView)
{
  return this.mouseupCallback (modelItem, modelView);
}

function UiBroker_onClick (e, eSrc)
{
  return this.mouseupCallback (e, eSrc);
}

function UiBroker_onchangeCallback(newValue, eSrc)
{
  var rtId = null;
  if (eSrc.getModelItem)
    rtId = eSrc.getModelItem().getName();
  else
    rtId = eSrc.getName ();
  
  if (rtId) {
    var msg = "<input rtid='" + this.formatRuntimeId(rtId) + "'>";
    if ((eSrc.type == 'BomItem') || (eSrc.type == 'CountedOptionItem'))
      msg += eSrc.getCount ();
    else 
      msg += newValue;
    msg += "</input>";
    this.postClientMessage(msg);
  }
  return true;
}

function UiBroker_actionOnChange (modelItem, modelView)
{
  this.onchangeCallback (modelItem, modelView);
}

function UiBroker_objectAddedCallback (item, itemParent)
{
  if (itemParent.windowName)
    this.registerRuntimeObject (itemParent.windowName, item);
}

function UiBroker_postClientMessage(xml, bOverRideLock)
{
  if (!this.appExited) {
    if ((! this.isEventManagerLocked()) || bOverRideLock) {
      this.log ("Sending message to server '" + xml + "'", new Date());
      this.lockEventManagers();
      
      if (! this.proxy) {
        this.proxy = eval(this.proxyRef);
      }
      var proxy = this.proxy;
      
      this.sessionId = (ns4) ? proxy.document.form1.sessionId.value : proxy.form1.sessionId.value
        var msg = "<client-event session-id='" + this.sessionId + "'>";
      msg += xml + "</client-event>";
      if (ns4) {
        proxy.document.form1.XMLmsg.value = msg;
        proxy.document.form1.submit ();
      } else if (ie4) {
        proxy.form1.XMLmsg.value = msg;
        proxy.form1.submit ();
      }	      
      if ( this.heartMax != 0 )
        this.reSetHeartBeat();
      if (this.isEventManagerLocked ()) {
        this.timeOutId = setTimeout ("ub.sessionTimeout ();", 500000);
        this.intervalId = setInterval ("ub.pollProxy ();", 500);
      }	  	
      return true;
    } 
    else {
      return false;
    }
  }
  return false;
}

function UiBroker_unregisterRuntimeObject (wndName, runtimeId)
{
  if ((wndName != null) && (runtimeId != null)) {
    this.runtimeObjectHash[wndName][runtimeId] = null;
  }
}

function UiBroker_getRuntimeObject (runtimeId)
{
  var wndHash = null;
  var wndName = null;
  var runtimeObject = null;
  for (wndName in this.runtimeObjectHash) {
    wndHash = this.runtimeObjectHash[wndName];
    runtimeObject = wndHash[runtimeId];
    if (runtimeObject != null) 
      return runtimeObject;
  }
  return runtimeObject;
}

function UiBroker_setOptionValues (rtid, state, count, price, atp, visible)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if ( visible )
      ctl.setState (state);
    else
      ctl.setState ('unknown');
    ctl.setCount (count);
    ctl.setPrice (price);
    ctl.setATP (atp);
    ctl.setVisibility(visible);
  }
}

function UiBroker_setFeatureValues (rtid, state, count, price, atp, bUnSatisfied, visibility, captionId)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if (ctl.setState)
      ctl.setState (state);
    if (ctl.setCount)
      ctl.setCount (count);
    if (ctl.setPrice) {
      if (price != "") {
        ctl.setPriceVisible(true);
        ctl.setPrice (price);
      }
    }
    if (ctl.setATP){
      if(atp != ""){
        ctl.setATPVisible(true);
        ctl.setATP (atp);
      }
    }
    if (ctl.setUnSatisfied)
      ctl.setUnSatisfied (bUnSatisfied);

    ctl.hideFeature(!visibility);
    if (visibility){
     if(!ctl.isVisible())
       ctl.show();
    }
    else
     ctl.hide();

    ctl.updateDone();

    var capCtl = null;
    if (captionId != null){
       capCtl = this.getRuntimeObject (captionId);
       if(visibility)
         capCtl.show();
       else
         capCtl.hide();
    }
  }
}

function UiBroker_setUnSatisfactionValue (rtid, bUnSatisfied)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    ctl.setUnSatisfied (bUnSatisfied);
  }
}

//function to change the availability state of a tree node at runtime
function UiBroker_setAvailabilityState (rtid, bAvail)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if ((ctl != null) && ctl.setAvailability) {
    ctl.setAvailability(bAvail);
  }
}

function UiBroker_setNumericValue (rtid, val)
{
  var ctl;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    ctl.setValue (val);
  }
}

//Function to change Image and caption of controls at runtime
function UiBroker_setDisplayValue (rtid, propName, val)
{
  var ctl = null;
  if ((propName != 'image') && (propName != 'caption')) 
    return;
  ctl = this.getRuntimeObject (rtid);
  if (ctl != null) {
    if (propName == 'image' && val != "")
      ctl.setSrc (IMAGESPATH + val);
    else if (propName == 'caption')
      ctl.setCaption (val);
  }
}

function UiBroker_setAppearence (iapp)
{
  this.appearence = iapp;
}

function UiBroker_getAppearence()
{
  return (this.appearence);
}

function UiBroker_setUILook(style)
{
  this.uiLook = style;
}

function UiBroker_getUILook(){
  return this.uiLook;
}

function UiBroker_registerUIFrames(wnd,ref)
{
  if (!this.ctrlFrame) this.ctrlFrame = new Array();
  if (!this.wndFrame) this.wndFrame = new Array();
  
  if ( wnd.isWindow ){
    this.wndFrame[this.wndCnt] = ref;
    this.wndCnt++;
  } else if ( wnd.isControl ){
    this.ctrlFrame[this.ctrlCnt] = ref;
    this.ctrlCnt++;
  }
}

function UiBroker_setWndBGColor(bgCol)
{
  this.wndCol = bgCol;
  var len = this.wndFrame.length;
  for (i=0;i <len; i++)
  {
    eval(this.wndFrame[i]).document.bgColor = bgCol;
  }
}

function UiBroker_setCtrlBGColor(bgCol)
{	
  this.ctrlCol = bgCol;
  var len = this.ctrlFrame.length;
  for (i=0;i<len; i++)
  {
    eval(this.ctrlFrame[i]).document.bgColor = bgCol;
  }
}

function UiBroker_setTxtColor(bgCol)
{	
  this.txtCol = bgCol;
}

function UiBroker_setContextInfo(ctx)
{
  var len = this.wndFrame.length;
  for ( i=0; i< len; i++ ){
    if (eval(this.wndFrame[i]).setContextInformation)
      eval(this.wndFrame[i]).setContextInformation(ctx);
  }
}

function UiBroker_addTarget (wndName, publishRef, relDepth)
{
  if (!this.targetHash) this.targetHash = new Array ();
  if (!this.compMgrHash) this.compMgrHash = new Array ();
  if (!this.evtMgrHash) this.evtMgrHash = new Array ();
  if (!this.runtimeObjectHash) this.runtimeObjectHash = new Array();
  if (!this.loadedHash) this.loadedHash = new Array ();
  if (!this.depthHash) this.depthHash = new Array ();
  if (!this.srcWdwRefHash) this.srcWdwRefHash = new Array();

  this.targetHash[wndName] = publishRef;
  this.compMgrHash[wndName] = new ComponentManager (wndName);
  this.runtimeObjectHash[wndName] = new Array();
  if ((relDepth != null) || (relDepth != 'undefined'))
    this.depthHash[wndName] = relDepth;
  if (wndName != 'czContent') {
    this.loadedHash[wndName] = false;
  }
  this.hasTargetRef = true;

  var parentStr = '';
  for (var i=0; i < relDepth; i++)
    parentStr += 'parent.';

  this.srcWdwRefHash[wndName] = parentStr + "frames." + window.name;  
}

function UiBroker_removeTarget (wndName)
{
  if (!this.hasTargetRef) return;
  this.targetHash[wndName] = null;
  this.compMgrHash[wndName] = null;
  this.runtimeObjectHash[wndName] = null;
  this.evtMgrHash[wndName] = null;
  
  delete this.targetHash[wndName];
  delete this.compMgrHash[wndName];
  delete this.evtMgrHash[wndName];
  delete this.runtimeObjectHash[wndName];
}

function UiBroker_getDepth (wndName)
{
  return (this.depthHash[wndName]);
}

function UiBroker_setDepth (wndName, depth)
{
  if (!this.depthHash) return;
  this.depthHash[wndName] = depth;
}

function UiBroker_createLabel(wndName, rtid, caption, lt, tp, wd, ht)
{
  var cm = this.compMgrHash[wndName];
  var re = /\r\n/gi;
  var cap = caption.replace(re,'<br>');
  var lbl = new Prompt();
  lbl.setName (rtid);
  lbl.setDimensions (lt, tp, wd, ht);
  lbl.setCaption (cap);
  lbl.setReference (this.targetHash[wndName]);
  lbl.setWindowName (wndName);
  lbl.type = 'label';
  lbl.setCaptionWrapping (true);

  cm.addComponent (lbl);
  this.registerRuntimeObject (wndName, lbl);
  
  return lbl;
}

function UiBroker_createLogicComboBox(wndName, rtid, lt, tp, wd, ht,  bUnSatisfied ,visibility,capId)
{
  var cm = this.compMgrHash[wndName];
  var cbo = new LogicComboBox();
  cbo.setName (rtid);
  cbo.setDimensions (lt, tp, wd, ht);
  cbo.setReference (this.targetHash[wndName]);
  cbo.setWindowName (wndName);
  cbo.setEventManager (this.evtMgrHash[wndName]);
  cbo.setBackgroundColor ('white');
  cbo.setActiveBackColor ('lightgrey');
  cbo.setSelectedBackColor ('white');
  if (! this.bNoStateIcons) {
    cbo.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  cbo.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  //cbo.setLogicStateProps (true, true, true);
  cbo.setUnSatisfied (bUnSatisfied);

  cbo.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }
  cbo.type = 'ComboBox';
  cbo.setTabindex(this.tabindex++);

  cm.addComponent (cbo);
  this.registerRuntimeObject (wndName, cbo);

  cbo.addListener ('objectAddedCallback', this);
  cbo.addListener ('actionMouseUp', this);

  cbo.readOnly(this.getReadOnly());

  return cbo;
}

function UiBroker_createLogicList(wndName, rtid, lt, tp, wd, ht,bUnSatisfied, visibility,capId)
{
  var cm = this.compMgrHash[wndName];
  var lst = new LogicList();
  lst.setName (rtid);
  lst.setDimensions (lt, tp, wd, ht);
  lst.setReference (this.targetHash[wndName]);
  lst.setWindowName (wndName);
  lst.setEventManager (this.evtMgrHash[wndName]);
  lst.setBackgroundColor ('white');
  lst.setActiveBackColor ('lightgrey');
  lst.setSelectedBackColor ('white');
  if (! this.bNoStateIcons) {
    lst.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  lst.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  //lst.setLogicStateProps (true, true, true);
  lst.setUnSatisfied (bUnSatisfied);

  lst.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
    capCtl = this.getRuntimeObject (capId);
    if(visibility)
      capCtl.show();
    else
      capCtl.hide();
  }
  lst.type = 'list';
  lst.setTabindex(this.tabindex++);

  cm.addComponent (lst);
  this.registerRuntimeObject (wndName, lst);
  
  lst.addListener ('objectAddedCallback', this);
  lst.addListener ('actionMouseUp', this);
  lst.addListener ('actionOnChange', this);

  lst.readOnly(this.getReadOnly());
  
  return lst;
}

function UiBroker_createLogicCheckBox(wndName, rtid, caption, lt, tp, wd, ht, state, price, atp, bUnSatisfied, visibility, capId)
{
  var cm = this.compMgrHash[wndName];
  var chk = new LogicCheckBox();
  chk.setName (rtid);
  chk.setDimensions (lt, tp, wd, ht);
  chk.setReference (this.targetHash[wndName]);
  chk.setWindowName (wndName);
  chk.setCaption(caption);
  chk.setState (state);
  chk.setPrice (price);
  chk.setATP (atp);
  chk.setUnSatisfied (bUnSatisfied);

  chk.hideFeature(!visibility);
  var capCtl = null;
  if (capId != null){
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }

  chk.type = 'LogicCheckBox';
  chk.setImages (this.srcUTrue, this.scrUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  chk.setTabindex(this.tabindex++);

  cm.addComponent (chk);
  this.registerRuntimeObject (wndName, chk);

  chk.readOnly(this.getReadOnly());
  
  return chk;
}

function UiBroker_createBomItem(wndName, rtid, caption, lt, tp, wd, ht, state, count, price, atp, bUnSatisfied, visibility)
{
  var cm = this.compMgrHash[wndName];
  var itm = new BomItem ();
  itm.setName (rtid);
  itm.setDimensions (lt, tp, wd, ht);
  itm.setReference (this.targetHash[wndName]);
  itm.setWindowName (wndName);
  itm.setCaption(caption);
  itm.setState (state);
  itm.setCount (count);
  itm.setPrice (price);
  itm.setATP (atp);
  itm.setUnSatisfied (bUnSatisfied);
  itm.setBackgroundColor('white');
  if (this.appearence == UiBroker.FLAT_LOOK) {
    itm.setFlatLook(true);
    itm.setBorderColor('black');
  } else if (this.appearence == UiBroker._3DLOOK) {
    itm.set3DLook(true);
    itm.setBorderColor('#c6c3c6');
    //itm.setBorderColor('gray');
  }
  if(!visibility){
    itm.hideFeature(true);
  }
  itm.type = 'BomItem';
  itm.setTabindex(this.tabindex++);
  if (! this.bNoStateIcons) {
    itm.setImages (this.srcUTrue, this.srcUFalse, this.srcLTrue, this.srcLFalse, this.srcUnknown, this.imageWidth, this.imageHeight);
  }
  itm.setLogicStateProps (this.bNoStateIcons, this.bUseLogicColor, this.bUseLogicBold);
  cm.addComponent (itm);
  this.registerRuntimeObject (wndName, itm);

  itm.readOnly(this.getReadOnly());
  
  return itm;
}

function UiBroker_createTree (wndName, rtid, iconsAry, nodeAry, lt, tp, wd, ht)
{
  var cm = this.compMgrHash[wndName];
  if (! lt) {
    if (this.getUILook() == UiBroker.FORMS_LOOK_AND_FEEL)
      lt = 2;
    else
      lt = 25;
  }
  if (! tp)
    tp = 0;
  //get the window width and height of the target window;
  if (this.targetHash) {
    var doc = eval (this.targetHash[wndName]);
    if (ns4) {
      var ref = this.targetHash[wndName];
      var index = ref.lastIndexOf (".document");
      var strWdw = ref.substr (0, index);
      var wdw = eval (strWdw);
    }
  }
  var wndWidth = (ns4) ? wdw.innerWidth : doc.body.clientWidth;
  var wndHeight = (ns4) ? wdw.innerHeight : doc.body.clientHeight;
  /*
  if (! wd){
    if (ns4 && wdw.outerWidth < 700)
      wd = wndWidth + Math.ceil(wdw.innerWidth*0.30);
    else
      wd = wndWidth-lt;
  }
  */
  if (! wd)
    wd = wndWidth
  if (! ht)
    ht = wndHeight;

  //make adjustments for NS browser when things gets clipped
  wd += this.numRefreshTree * wd/4;
  ht += this.numRefreshTree * 1000;

  var tree = new Tree ();
  tree.setName (rtid);
  tree.setDimensions (lt, tp, wd, ht);
  tree.setReference (this.targetHash[wndName]);
  tree.setWindowName (wndName);
  tree.type = 'tree';
  tree.setBackgroundColor ("#cecf9c");
  tree.setSelectedBackColor ("#336699");
  tree.setColor ("#003366");
  tree.setSelectedColor ("#ffffff");

  tree.addListener ('objectAddedCallback', this);
  for (var i=0; i<iconsAry.length; i++) {
    var src = iconsAry[i].src;
    tree.setNodeIcons (iconsAry[i].key, src);
  }
  cm.addComponent (tree);
  this.registerRuntimeObject (wndName, tree);
  if (nodeAry) {
    tree.addNodes (nodeAry);
  }
  var fnt = this.createFontObject("Times New Roman","12pt","normal","normal");
  tree.setFont(fnt);
  return tree;
}

function UiBroker_createImage(wndName, rtid, lt, tp, wd, ht, src)
{
  var cm = this.compMgrHash[wndName];
  var img = new ImageButton();
  img.setName (rtid);
  img.setDimensions (lt, tp, wd, ht);
  img.setReference (this.targetHash[wndName]);
  img.setWindowName (wndName);
  if ( src != "")
    img.setSrc (IMAGESPATH + src);
  img.type = 'image';
  cm.addComponent (img);
  this.registerRuntimeObject (wndName, img);
  
  return img;
}

function UiBroker_createTextInput(wndName, rtid, lt, tp, wd, ht, val, visibility, capId)
{
  var cm = this.compMgrHash[wndName];
  var txt = new InputText();
  txt.setName (rtid);
  txt.setDimensions (lt, tp, wd, ht);
  txt.setReference (this.targetHash[wndName]);
  txt.setWindowName (wndName);
  txt.setValue(val);
  if (this.getReadOnly())
    txt.EDITABLE = false;
  else
    txt.EDITABLE = true;
  txt.type = 'text';
  txt.setBackgroundColor ('white');
  txt.standalone = true;
  txt.hideFeature(!visibility);

  var capCtl = null;
  if (capId != null) {
     capCtl = this.getRuntimeObject (capId);
     if(visibility)
       capCtl.show();
     else
       capCtl.hide();
  }
  txt.setTabindex(this.tabindex++);
  cm.addComponent (txt);
  this.registerRuntimeObject (wndName, txt);
  
  return txt;	
}

function UiBroker_createNumericInput(wndName, rtid, val, lt, tp, wd, ht)
{
  return (this.createTextInput (wndName, rtid, val, lt, tp, wd, ht));
}

function UiBroker_createButton(wndName, rtid, caption, lt, tp, wd, ht, imgSrc)
{
  var cm = this.compMgrHash[wndName];
  var btn;
  if ((imgSrc != null) && (imgSrc != '')) {
    btn = new ImageButton ();
    btn.setImages (IMAGESPATH + imgSrc);
  } else {
    btn = new CurvedButton();
    btn.setLeftCurveWidth (11);
    btn.setRightCurveWidth (11);
    btn.setCaption (caption);
  }
  btn.setName (rtid);
  btn.setDimensions (lt, tp, wd, ht);
  btn.setReference (this.targetHash[wndName]);
  btn.setWindowName (wndName);
  btn.type = 'button';
  btn.setTabindex(this.tabindex++);
  cm.addComponent (btn);
  this.registerRuntimeObject (wndName, btn);
  return btn;	
}

function UiBroker_createFontObject (family, size, style, weight)
{
  return (new Font (family, size, style, weight));
}

function UiBroker_outputToFrame (target, cmdStr, bIsJSOnly)
{
  var wdw = null;
  wdw = eval (target);
  if (wdw != null) {
    var doc = wdw.document;
    doc.open ("text/html");
    if (bIsJSOnly) {
      doc.writeln ("<HTML><HEAD></HEAD><BODY>");
      doc.writeln ("<SCRIPT language=javascript>");
      doc.writeln (cmdStr);
      doc.writeln ("<\/SCRIPT>");
      doc.writeln ("</BODY></HTML>");
    } else {
      doc.writeln (cmdStr);
    }
    doc.close ();
  }
}

function UiBroker_getEventManager (wndName)
{
  return (this.evtMgrHash[wndName]);
}

function UiBroker_getComponentManager (wndName)
{
  return (this.compMgrHash[wndName]);
}

function UiBroker_createMessage ()
{
  var msgTarget = this.getMessageTarget ();
  
  lbl = new Base();
  lbl.setName ('MSG1');
  lbl.setDimensions (0, 0, 0, 0);
  lbl.setBackgroundColor ('white');
  lbl.setZIndex (10000);
  lbl.setReference (this.targetHash[msgTarget]);
  lbl.hide ();
  if (msgTarget != null)
    this.compMgrHash[msgTarget].addComponent (lbl);

  this.messageBox= lbl;
}

var dlg = null;
var dlgOpen = false;

function checkDialog () {
  if (! dlgOpen) {
    if (ub.bShowingMessage) {
      ub.showMessage(ub.msgTxt);
    } else {
      dlgOpen = false;
    }
  }
}

function _check_msg_window()
{
  if (ub.bShowingMessage) {
    if (dlgOpen) {
      dlg.focus();
    } else {
      ub.showMessage(ub.msgTxt);
    }
  }
}

function UiBroker_setMsgTitle(title)
{
  this.messageType = title;
}

function UiBroker_showMessage (msg)
{
  //because of launch pad we have to handle in a spcial way
  if ( ns4 )
    dlg = window.open ("", "msgBox", "width=500,height=250,resizable=yes,scrollbars=yes", true);
  else
    dlg = window.open ("", "_blank", "width=500,height=250,resizable=yes,scrollbars=yes", true);

  dlgOpen = true;

  var doc = dlg.document;
  doc.open ("text/html");
  doc.writeln ("<html>");
  doc.writeln ("<title>"+ this.messageType +"</title>");
  if ( this.wndCol != "" )
    doc.writeln ("<body bgColor='" + this.wndCol + "' onUnload=\"javascript:opener.dlgOpen=false; opener.checkDialog();\">");
  else
    doc.writeln ("<body onUnload=\"javascript:opener.dlgOpen=false; opener.checkDialog();\">");
  if ( this.txtCol != "" )
    doc.writeln ("<FONT color='"+ this.txtCol + "'>"+ msg +"</FONT>");
  else
    doc.writeln (msg);
  doc.writeln ("</body></html>");
  doc.close ();
  dlg.focus();
  this.msgTxt = msg;
  this.lockEventManagers ();
  this.bShowingMessage = true;
  this.atpShown = false;
}

function UiBroker_launchWindow(url)
{
    _wURL = window.open(url,"");
}

function UiBroker_hideMessage ()
{
  this.bShowingMessage = false;
  if (dlgOpen) {
    dlg.close();
    this.msgTxt = "";
  }
}

function UiBroker_showContradiction (msg, idYes, idNo, yesCaption, noCaption)
{
  if (! yesCaption)
    yesCaption = "Yes";
  
  if (! noCaption)
    noCaption = "No";
  
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + yesCaption + '" id="' + idYes + '" name="' + idYes;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idYes + '\');">';
  sBuffer += '&nbsp;&nbsp;&nbsp;';
  sBuffer += '<INPUT type="button" value="' + noCaption + '" id="' + idNo + '" name="' + idNo;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idNo + '\');">';
  sBuffer += '</FORM>';
  sBuffer =  msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Contradiction";
  this.showMessage (sBuffer);
}

function UiBroker_showValidation (msg, idOk, okCaption)
{
  if (! okCaption)
    okCaption = "OK";
  
  //add OK button html tags with the message
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + okCaption + '" id="' + idOk + '" name="' + idOk;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idOk +  '\');">';
  sBuffer += '</FORM>';
  sBuffer = msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Validation";
  this.showMessage (sBuffer);
}

function UiBroker_showConfirmation (msg, idOk, idCancel, okCaption, cancelCaption)
{
  if (! okCaption)
    okCaption = "OK";
  
  if (! cancelCaption)
    cancelCaption = "Cancel";
  
  var sBuffer = '<FORM id=form1 name=form1>';
  sBuffer += '<INPUT type="button" value="' + okCaption + '" id="' + idOk + '" name="' + idOk;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idOk + '\');">';
  sBuffer += '&nbsp;&nbsp;&nbsp;';
  sBuffer += '<INPUT type="button" value="' + cancelCaption + '" id="' + idCancel + '" name="' + idCancel;
  sBuffer += '" onclick="javascript:opener.getUiBroker().messageCallback(\'' + idCancel + '\');">';
  sBuffer += '</FORM>';
  sBuffer = msg + '<DIV ALIGN="CENTER">' + sBuffer + '</DIV>';
  //this.messageType="Confirmation";
  this.showMessage (sBuffer);
}

function UiBroker_showFatalError (msg)
{
  if (! this.appExited) {
    this.showExitMessage(msg);
  }
}

function UiBroker_showExitMessage (msg)
{
  //this.clearContentFrame ();
  this.contentMode = 'empty';
  msg = '<DIV ALIGN="CENTER">' + msg + '</DIV>';
  this.publish('czContent', null, null, msg);
  this.appExited = true;
}

function UiBroker_messageCallback (id)
{
  var xmlMsg;
  xmlMsg = "<click rtid='" + this.formatRuntimeId(id) +  "'/>";
  this.bEvtMgrLocked = false;
  this.postClientMessage (xmlMsg);
  this.hideMessage ();
}

function UiBroker_showPromptInput (rtid, msg, defaultText, idInput, idOk, idCancel)
{
  var xmlMsg = '';
  //for now use regular browsers prompt to gather input;
  if (defaultText == null)
    defaultText = '';
  var result = window.prompt (msg, defaulText);
  if (result == null) {
    //user clicked cancel button send idCancel click to the server;
    xmlMsg = "<click rtid='" + this.formatRuntimeId(idCancel) +  "'/>";
  } else {
    //post input event message with value of the text;
    xmlMsg = "<input rtid='" + this.formatRuntimeId(idInput) + "'>";
    xmlMsg += result + "</input>";
  }
  //this.messageType="Prompt Input";
  this.postClientMessage (xmlMsg);
}

function UiBroker_publish(wndName, headContent, bodyAttributes, bodyContent)
{
  // mac related
  this.alreadyPublished = true;
  this.needRefresh = false;

  if(this.curSeqNum != -1) {
    if (wndName != null) {
      if ((this.getContentMode() == 'config') && (wndName == 'czContent')) {
        this.loadedHash[wndName] = true;
        return;
      }
      this.loadedHash[wndName] = false;
      if (!headContent) {
        if (this.defaultHeadContent)
          headContent = this.defaultHeadContent;
        else
          headContent = "";
      }
      if (!bodyAttributes) bodyAttributes = "";
      if (!bodyContent) bodyContent = "";
    
      //var parentStr = '';
      //for (var i=0; i < this.depthHash[wndName]; i++)
      //  parentStr += 'parent.';

      timeArray [0] = new Array ();
      timeArray[0][0] = new Date ();

      var doc = eval(this.targetHash[wndName]);
      //ToDo: Do some research. IE browser back button doesn't work properly
      //doc.open("text/html", "replace");
      doc.open("text/html");
      doc.writeln("<HTML>");
      doc.writeln("<HEAD>");
      doc.writeln('<META HTTP-EQIV="Content-Type" CONTENT="text/html; charset=' + this.charSetEncoding + '">');
      doc.writeln('<META HTTP-EQUIV="Expires" CONTENT="-1">');
      doc.writeln('<META HTTP-EQUIV="Pragma" CONTENT="no-cache">');
      doc.writeln(headContent);
      //create a message box for on the specified target or on the summary screen;
      //to display error message;
      if ((wndName == this.messageTarget) || (wndName == 'czContent')) {
        this.createMessage ();
      }
    
      timeArray [1] = new Array ();
      timeArray [1][0] = new Date ();
      this.compMgrHash[wndName].generateCSS(doc);
      timeArray[1][1] = new Date ();
      timeArray[1][2] = "Gen and Write Sytle sheet";

      if (this.bUseLogicColor) {
        doc.writeln ('<STYLE type="text/css">');
        if (this.utColor)
          doc.writeln ('.' + UT_COLOR + '{color:' + this.utColor + ';}');
        if (this.ltColor)
          doc.writeln ('.' + LT_COLOR + '{color:' + this.ltColor + '; }');
        if (this.ufColor)
          doc.writeln ('.' + UF_COLOR + '{color:' + this.ufColor + '; }');
        if (this.ufColor)
          doc.writeln ('.' + LF_COLOR + '{color:' + this.lfColor + '; }');
        if (this.unColor)
          doc.writeln ('.' + UN_COLOR + '{color:' + this.unColor + '; }');
       
        //boldness property will be there only if there is logic state color
        if (this.bUseLogicBold) {
          if (this.utBold)
            doc.writeln ('.' + UT_BOLD + '{font-weight:bold;}');
          if (this.ltBold)
            doc.writeln ('.' + LT_BOLD + '{font-weight:bold;}');
          if (this.ufBold)
            doc.writeln ('.' + UF_BOLD + '{font-weight:bold;}');
          if (this.lfBold)
            doc.writeln ('.' + LF_BOLD + '{font-weight:bold;}');
          if (this.unBold)
            doc.writeln ('.' + UN_BOLD + '{font-weight:bold;}');
        }
        doc.writeln ('</STYLE>');
      }
      this.cnt++;
      this.backSeqNum = this.cnt;
      doc.writeln("<script launguage=javascript>");
	doc.writeln("var seq="+ this.cnt +";");
      doc.writeln("</script>");
      doc.writeln("</HEAD>");
      doc.writeln("<BODY " + bodyAttributes + " tabindex=1" + 
                " onload='javascript:if (" + this.srcWdwRefHash[wndName] + ".isLoaded) {" + 
                " var ub = " + this.srcWdwRefHash[wndName] + ".getUiBroker ();" + 
                " ub.launch (\"" + wndName + "\", document, seq);" +
                " ub.loadingFinished (\"" + wndName + "\");" + 
                " ub = null;" +
                " delete ub;" +                
                "}' onfocus='javascript:" + this.srcWdwRefHash[wndName] + 
                ".window_onfocus(\"" + wndName + "\");'>");
      doc.writeln(bodyContent);

      timeArray [2] = new Array ();
      timeArray [2][0] = new Date ();
      this.compMgrHash[wndName].renderComponents(doc);
      timeArray[2][1] = new Date ();
      timeArray[2][2] = "Gen & Write Render String";

      doc.writeln ('<SCRIPT language=javascript>');
      if (ns4) {
        doc.writeln ('window.captureEvents (Event.RESIZE);');
        doc.writeln ('window.onresize='+ this.srcWdwRefHash[wndName] + '.window_resizeCallback;');
      }
      doc.write ("function doOnKeyDown (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyDown(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnKeyPress (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyPress(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnKeyUp (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnKeyUp(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnFocus (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnFocus(e)');");
      doc.writeln ("return res;}");
      doc.write ("function doOnBlur (e, objId) {");
      doc.write ("if (e == null) var e = window.event;");
      doc.write ("var res = eval('" + this.srcWdwRefHash[wndName] + "'+'.'+ objId +'.doOnBlur(e)');");
      doc.writeln ("return res;}");
      doc.writeln ('<\/SCRIPT>');
      doc.writeln("</BODY>");
      doc.writeln("</HTML>");
      this.fromPublishedPage = true;
      doc.close();

      timeArray[0][1] = new Date ();
      timeArray[0][2] = "Write new page";
      //writeTimes ();

    } else { 
        //write to our document;
        this.compMgr.generateCSS();
        this.compMgr.renderComponents();
    }
  }
  else
  {
    _hWnd = this.errorReport = window.open ("", "errorReport",
                                         "width=500,height=250,resizable=yes,scrollbars=yes", true);
    _hWnd.document.open ();
    if(bodyContent == "")
      bodyContent = "Please check the log files for detailed error";
    _hWnd.document.writeln('<html><head><title>Error</title></head><body>'+bodyContent+'</body></html>');
    _hWnd.document.close();
  }
  if ( this.contentMode == 'config' )
    this.contentMode = 'summary';

  if ( this.contentMode == 'summary')
    this.contentMode = 'config';
  else
    this.contentMode = 'summary';

  if (this.showOn)
    this.showOn = false;
}

function UiBroker_publishAll (bodyAttributes)
{
  //before start publishing to all frames first set the loadedHash of all frames as false;
  for (var wnd in this.targetHash)
    this.loadedHash[wnd] = false;
  
  if (this.hasTargetRef) {
    for (var wnd in this.targetHash)
      this.publish (wnd, null, bodyAttributes);
  } else
    this.publish ();
}

function UiBroker_launch(wndName, docObj, seq) 
{
  if ( ie4 ){
    if ( seq < 2 ){
      this.backSeqNum--;
    } else if ( seq < this.backSeqNum ){
      this.goBack();
      return true;
    } else
      this.backSeqNum = seq;
  }
  if (wndName != null) {
    var em = EventManager.instances[wndName];
    //for summary there is no event manager.
    if (em) {
      this.compMgrHash[wndName].launchComponents(em, true);
      em.init (docObj);
    }
  } else
    this.compMgr.launchComponents (this.evtMgr);
}

function UiBroker_loadingFinished (wndName)
{
  if (wndName != null) {
    this.loadedHash[wndName] = true;
    if (this.allFramesLoaded ()) {
      if (parent.frames.czProxy.doModelUpdate) {
        parent.frames.czProxy.doModelUpdate ();
      }
    } else
      this.bFinishedLoading = true;
  }
}

function UiBroker_allFramesLoaded ()
{
  if (this.hasTargetRef) {
    for (var wnd in this.targetHash) {
      if (this.loadedHash[wnd] != null)
        if (! this.loadedHash[wnd])
          return (false);
    }
  } else if (!this.bFinishedLoading)
    return (false);
  return (true);
}

function UiBroker_refresh(wndName) 
{
  if ((wndName != null) && this.hasTargetRef) {
    this.compMgrHash[wndName].destroy ();
    this.compMgrHash[wndName] = null;
    delete this.compMgrHash[wndName];
    
    if (this.evtMgrHash[wndName]) {
      this.evtMgrHash[wndName].destroy ();
      this.evtMgrHash[wndName] = null;
      delete this.evtMgrHash[wndName];
      EventManager.clearInstance (wndName);
    }
    Utils.clearCollection (this.runtimeObjectHash[wndName]);
    
    //TO DO: Redo when we make tree with our own scroll bar;
    //TO DO: fix for scroll drag in multiple frames;
    Drag.clearInstance ('BarDrag');
      
    this.compMgrHash[wndName] = new ComponentManager (wndName);
    this.runtimeObjectHash[wndName] = new Array ();
    
    var em = new EventManager (wndName);
    em.setCompMgr (this.compMgrHash[wndName]);
    this.evtMgrHash[wndName] = em;
  } else {
    this.compMgr = new ComponentManager ();
    this.evtMgr = new EventManager ();
  }
  this.tabindex = 1;
}

function UiBroker_setDefaultHeadContent (content)
{
  this.defaultHeadContent = content;
}

function UiBroker_lockEventManagers ()
{
  var item;
  for (item in this.evtMgrHash) {
    var em = this.evtMgrHash[item];
    em.lock ();
    this.bEvtMgrLocked = true;
  }
}

function UiBroker_unLockEventManagers ()
{
  //clear timeouts;
  this.clearTimeoutHandles ();

  //unlock event managers only when a message window is not shown;
  if (! this.bShowingMessage) {
    var item;
    for (item in this.evtMgrHash) {
      var em = this.evtMgrHash[item];
      em.unLock ();
      this.bEvtMgrLocked = false;
    }
    if (this.pendingEvt) {
      this.pendingEvt = false;
      this.postClientMessage(this.evtType);
      if(this.pendingObj != null)
        this.navigateTo(this.pendingObj.getName());
    }
  } else {
    //A message is shown. Clear the event queue;
    this.pendingEvt = false;
    this.evtType = null;
    this.pendingObj = null;
  }
  if ( this.msgTxt == "" && this.needRefresh && !this.alreadyPublished && this.contentMode == 'config' ){
    this.backSeqNum = 0;
    this.cnt = 0;
    this.refreshTreeFrame();
    this.needRefresh = false;
  }
  this.alreadyPublished = false;
}

function UiBroker_isEventManagerLocked ()
{
  return (this.bEvtMgrLocked);
}

/**
* Mostly this function will get called when showing messages
* @param - , separated list of window name/id
*/
function UiBroker_lockEventManagersExcept ()
{
  //First lock all the event manager, then go thru the argument list and;
  //unlock them;
  this.lockEventManagers ();
  var len = arguments.length;
  for (var i=0; i < len; i++) {
    var evtMgr = this.getEventManager (arguments[i]);
    if (evtMgr != null)
      evtMgr.unLock ();
  }
}

function UiBroker_setSessionStartURL (sessionUrl)
{
  this.sessionStartURL = sessionUrl;
}

function UiBroker_getSessionStartURL ()
{
  return (this.sessionStartURL);
}

function UiBroker_setDebugMode (bMode)
{
  var console;
  this.debugMode = bMode;
  if (!bMode) {
    if (this.dbgWnd != null) {
      console = this.dbgWnd;
      console.document.close ();
      console.close ();
    }
  }
}

function UiBroker_log (msg, stTime, endTime)
{
  var console;
  var consoleDoc;
  if (this.debugMode) {
    if ((this.dbgWnd == null) || (this.dbgWnd.closed)) {
      console = this.dbgWnd = window.open ("", "console", 
                                           "width=500,height=250,resizable=yes,scrollbars=yes", true);
      console.document.open ("text/plain");
    } else {
      console = this.dbgWnd;
    }
    consoleDoc = console.document;
    if (stTime && endTime) {
      //consoleDoc.writeln (this.formatTime (stTime) + "Begin " + msg);
      //consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
      var msec = endTime.valueOf() - stTime.valueOf();
      consoleDoc.writeln ('[' + msec + ']' + "Elapsed " + msg);
    } else if (stTime && (endTime == null)) {
      this.startTime = stTime;
      this.endTime = null;	
      consoleDoc.writeln (this.formatTime (this.startTime) + "Begin " + msg);
    } else if (endTime && (stTime == null)) {
      if (this.startTime) {
        consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
        var msec = endTime.valueOf() - this.startTime.valueOf();
        consoleDoc.writeln ('[' + msec + ']' + "Elapsed " + msg);
      } else {
        consoleDoc.writeln (this.formatTime (endTime) + "End " + msg);
      }
      this.startTime = null;
      this.endTime = null;	
    } else {
      var dt = new Date ();
      consoleDoc.writeln (this.formatTime (dt) + msg);
      this.startTime = null;
      this.endTime = null;	
    }
  }
}

function UiBroker_formatTime (dt)
{
  var timeStr = '[' + dt.getHours () + ':' + dt.getMinutes () + ':';
  timeStr += dt.getSeconds () + ':' + dt.getMilliseconds () + '] ';
  return (timeStr);
}

function UiBroker_registerPage (seqNum, navCmd, page)
{
  this.log ("Sending message to server", null, new Date())
  var retVal = true;
  if (navCmd != '') {
    navCmd = navCmd.replace (/&#60;/, '<');
    if ( navCmd == this.navCmd && seqNum < this.curSeqNum && page == 1){
 	this.showConfiguration();
    }else{
    	this.navCmd = navCmd;
    }
    //this.navCmd = navCmd.substring(navCmd.indexOf("rtid")+6,navCmd.indexOf("'/>"));
  }else
    this.navCmd = '';

  if ((seqNum == this.curSeqNum) && (page == 1))
  {
      this.goBack ();
      //this.back = true;
      this.register = false;
      retVal = false;
      return (retVal);
  }

  if (seqNum == this.curSeqNum)
  {
      //this.refreshCurrentPage ();
      retVal = false;
  }
  else if (seqNum < this.curSeqNum)
  {
      this.goBack ();
      //this.back = true;
      this.register = false;
      retVal = false;
  }
  else if (navCmd != '<show-summary/>')
  {
      //If the navCmd is an empty string, update seqNum and return true
      if (navCmd == '' || page == 1) {
          this.curSeqNum = seqNum;
          return (retVal);
      }
      var entry = this.pageHistory.peek ();
      if (entry)
      {
          if (navCmd != entry) {
            this.pageHistory.push (navCmd);
            this.register = true;
          }
     } else {
        this.pageHistory.push (navCmd);
        this.register = true;
     }
     this.curSeqNum = seqNum;
  }
  return (retVal);
}

function UiBroker_goBack ()
{
  if (this.contentMode == 'summary') {
	this.showConfiguration();
      this.back=false;
  }else{
  	if (this.pageHistory.entries.length > 1)
    	this.pageHistory.pop ();

  	var msg = this.pageHistory.peek ();
  	if (msg)
    		this.postClientMessage(msg);
		if ( this.pageHistory.entries.length > 1)
              this.back=true;
            else
		  this.back=false;
  }
}

function UiBroker_refreshCurrentPage ()
{
  var msg = this.pageHistory.peek ();
  this.postClientMessage(msg);	
}

function UiBroker_navigateTo (runtimeId)
{
  var obj = this.getRuntimeObject (runtimeId);
  if (obj != null) {
    //currently only tree node navigate is supported;
    if (obj.type == 'TreeNode') {
      var tree = obj.getParentObject (obj);
      tree.setSelectedNode (obj);
    }
  }
}

function UiBroker_showOutput(outputId)
{
  var servletUrl = parent.frames.czProxy.location;
  var url = servletUrl + "?generateOutput=" + outputId + "&sessionId=" + this.sessionId;
  var wd = 300;
  var ht = 300;
  if (ie4) {
    wd = top.window.document.body.clientWidth/2;
    ht = top.window.document.body.clientHeight/2;
  } else if (ns4) {
    wd = top.window.innerWidth/2;
    ht = top.window.innerHeight/2;
  }
  var newWindow = window.open(url, "czFcOutput", "menubar=no,resizable=yes,scrollbars=yes,width="+ wd + ",height="+ ht);
}

function UiBroker_showUrl(url)
{
  var newWindow = window.open(url, "czUserOutpu", "menubar=no");
}

function UiBroker_pollProxy ()
{
  var proxy = this.proxy;
  if (ie4) {
    if (proxy.document.readyState == "complete") {
      if (! proxy.doLayout) {
        var msg = proxy.document.body.innerHTML;
        // clear all time out;
        this.clearTimeoutHandles ();
        this.showExitMessage (msg);
      }
    }
  }
}

function UiBroker_sessionTimeout ()
{
  var msg = "Problem sending request or recieving response from the server.";
  this.showExitMessage (msg);
  this.clearTimeoutHandles ();
}

function UiBroker_clearTimeoutHandles ()
{
  if (this.timeOutId != null) {
    clearTimeout (this.timeOutId);
  }
  if (this.intervalId != null) {
    clearInterval (this.intervalId);
  }
}

function UiBroker_refreshTreeFrame ()
{
  this.numRefreshTree++;
  this.postClientMessage ('<refresh-frames/>', true);
}

function UiBroker_setCharacterSetProfile (charSetEnc)
{
  this.charSetEncoding = charSetEnc;
}

function UiBroker_setServletURL (url)
{
  this.servletURL = url;
}

function UiBroker_getServletURL ()
{
  return (this.servletURL);
}

function UiBroker_setStateToolTip(utTip, ufTip, ltTip, lfTip, unTip, unsatTip)
{
  self._utrueToolTip = utTip;
  self._ufalseToolTip = ufTip;
  self._ltrueToolTip = ltTip;
  self._lfalseToolTip = lfTip;
  self._unknownToolTip = unTip;
  self._unsatToolTip = unsatTip;
}

//static members
UiBroker.NO_BORDER_LOOK = 0;
UiBroker.FLAT_LOOK = 1;
UiBroker._3DLook = 2;
UiBroker.FORMS_LOOK_AND_FEEL = 'FORMS';

UiBroker.prototype.setSessionId = UiBroker_setSessionId;
UiBroker.prototype.getSessionId = UiBroker_getSessionId;
UiBroker.prototype.mouseupCallback = UiBroker_mouseupCallback;
UiBroker.prototype.onchangeCallback = UiBroker_onchangeCallback;
UiBroker.prototype.objectAddedCallback = UiBroker_objectAddedCallback;
UiBroker.prototype.actionMouseUp = UiBroker_actionMouseUp;
UiBroker.prototype.actionOnChange = UiBroker_actionOnChange;
UiBroker.prototype.onClick = UiBroker_onClick;

UiBroker.prototype.postClientMessage = UiBroker_postClientMessage;
UiBroker.prototype.addTarget = UiBroker_addTarget;
UiBroker.prototype.removeTarget = UiBroker_removeTarget;
UiBroker.prototype.setMessageTarget = UiBroker_setMessageTarget;
UiBroker.prototype.getMessageTarget = UiBroker_getMessageTarget;

UiBroker.prototype.setDebugMode = UiBroker_setDebugMode;
UiBroker.prototype.log = UiBroker_log;
UiBroker.prototype.formatTime = UiBroker_formatTime;

UiBroker.prototype.getRuntimeObject = UiBroker_getRuntimeObject;
UiBroker.prototype.navigateTo = UiBroker_navigateTo;

UiBroker.prototype.createLabel = UiBroker_createLabel;
UiBroker.prototype.createLogicList = UiBroker_createLogicList;
UiBroker.prototype.createLogicComboBox = UiBroker_createLogicComboBox;
UiBroker.prototype.createLogicCheckBox = UiBroker_createLogicCheckBox;
UiBroker.prototype.createBomItem = UiBroker_createBomItem;
UiBroker.prototype.createTextInput = UiBroker_createTextInput;
UiBroker.prototype.createNumericInput = UiBroker_createNumericInput;
UiBroker.prototype.createImage = UiBroker_createImage;
UiBroker.prototype.createButton = UiBroker_createButton;
UiBroker.prototype.createTree = UiBroker_createTree;
UiBroker.prototype.createBomItem = UiBroker_createBomItem;
UiBroker.prototype.createFontObject = UiBroker_createFontObject;
UiBroker.prototype.setDefaultLogicImages = UiBroker_setDefaultLogicImages;
UiBroker.prototype.setLogicColors = UiBroker_setLogicColors;
UiBroker.prototype.setLogicBold = UiBroker_setLogicBold;
UiBroker.prototype.setNoStateIcons = UiBroker_setNoStateIcons;
UiBroker.prototype.outputToFrame = UiBroker_outputToFrame;
UiBroker.prototype.deleteComponent = UiBroker_deleteComponent;

UiBroker.prototype.setOptionValues = UiBroker_setOptionValues;
UiBroker.prototype.setFeatureValues = UiBroker_setFeatureValues;
UiBroker.prototype.setUnSatisfactionValue = UiBroker_setUnSatisfactionValue;
UiBroker.prototype.setAvailabilityState = UiBroker_setAvailabilityState;
UiBroker.prototype.setNumericValue = UiBroker_setNumericValue;
UiBroker.prototype.setValue = UiBroker_setNumericValue;
UiBroker.prototype.setDisplayValue = UiBroker_setDisplayValue;

UiBroker.prototype.getComponentManager = UiBroker_getComponentManager;
UiBroker.prototype.getEventManager = UiBroker_getEventManager;
UiBroker.prototype.lockEventManagers = UiBroker_lockEventManagers;
UiBroker.prototype.unLockEventManagers = UiBroker_unLockEventManagers;
UiBroker.prototype.isEventManagerLocked = UiBroker_isEventManagerLocked;
UiBroker.prototype.lockEventManagersExcept = UiBroker_lockEventManagersExcept;

UiBroker.prototype.publish = UiBroker_publish;
UiBroker.prototype.publishAll = UiBroker_publishAll;
UiBroker.prototype.refresh = UiBroker_refresh;
UiBroker.prototype.launch = UiBroker_launch;
UiBroker.prototype.launchWindow = UiBroker_launchWindow;
UiBroker.prototype.loadingFinished = UiBroker_loadingFinished;
UiBroker.prototype.allFramesLoaded = UiBroker_allFramesLoaded;
UiBroker.prototype.hasAllFramesFinishedLoading = UiBroker_allFramesLoaded; //deprecated

UiBroker.prototype.setMsgTitle = UiBroker_setMsgTitle;
UiBroker.prototype.createMessage = UiBroker_createMessage;
UiBroker.prototype.showMessage = UiBroker_showMessage;
UiBroker.prototype.hideMessage = UiBroker_hideMessage;
UiBroker.prototype.showContradiction = UiBroker_showContradiction;
UiBroker.prototype.showValidation = UiBroker_showValidation;
UiBroker.prototype.showConfirmation = UiBroker_showConfirmation;
UiBroker.prototype.showPromptInput = UiBroker_showPromptInput;
UiBroker.prototype.showFatalError = UiBroker_showFatalError;
UiBroker.prototype.showExitMessage = UiBroker_showExitMessage;

UiBroker.prototype.showSummary = UiBroker_showSummary;
UiBroker.prototype.showConfiguration = UiBroker_showConfiguration;
UiBroker.prototype.completeConfiguration = UiBroker_completeConfiguration;
UiBroker.prototype.submitConfiguration = UiBroker_completeConfiguration;
UiBroker.prototype.saveConfiguration = UiBroker_saveConfiguration;
UiBroker.prototype.cancelConfiguration = UiBroker_cancelConfiguration;
UiBroker.prototype.configurationHelp = UiBroker_configurationHelp;
UiBroker.prototype.showATP = UiBroker_showATP;
UiBroker.prototype.getContentMode = UiBroker_getContentMode;
UiBroker.prototype.navigateToScreen = UiBroker_navigateToScreen;

UiBroker.prototype.registerPage = UiBroker_registerPage;
UiBroker.prototype.readOnlySession = UiBroker_readOnlySession;
UiBroker.prototype.getReadOnly = UiBroker_getReadOnly;

UiBroker.prototype.refreshTreeFrame = UiBroker_refreshTreeFrame;
UiBroker.prototype.setCharacterSetProfile = UiBroker_setCharacterSetProfile;
UiBroker.prototype.setServletURL = UiBroker_setServletURL;
UiBroker.prototype.getServletURL = UiBroker_getServletURL;
UiBroker.prototype.setStateToolTip = UiBroker_setStateToolTip;

// methods for heartbeat
UiBroker.prototype.postClientHeartBeat = UiBroker_postClientHeartBeat;
UiBroker.prototype.setMaxHeartBeat = UiBroker_setMaxHeartBeat;
UiBroker.prototype.getMaxHeartBeat = UiBroker_getMaxHeartBeat;
UiBroker.prototype.setHeartBeat = UiBroker_setHeartBeat;
UiBroker.prototype.getHeartBeat = UiBroker_getHeartBeat;
UiBroker.prototype.startHB = UiBroker_startHB;
UiBroker.prototype.stopHB = UiBroker_stopHB;
UiBroker.prototype.reSetHeartBeat = UiBroker_reSetHeartBeat;

//private methods
UiBroker.prototype.registerRuntimeObject = UiBroker_registerRuntimeObject;
UiBroker.prototype.unregisterRuntimeObject = UiBroker_unregisterRuntimeObject;
UiBroker.prototype.formatRuntimeId = UiBroker_formatRuntimeId;
UiBroker.prototype.messageCallback = UiBroker_messageCallback;

UiBroker.prototype.setDefaultHeadContent = UiBroker_setDefaultHeadContent;
UiBroker.prototype.setSessionStartURL = UiBroker_setSessionStartURL;
UiBroker.prototype.getSessionStartURL = UiBroker_getSessionStartURL;
UiBroker.prototype.clearContentFrame = UiBroker_clearContentFrame;

UiBroker.prototype.refreshCurrentPage = UiBroker_refreshCurrentPage;
UiBroker.prototype.goBack = UiBroker_goBack;

UiBroker.prototype.showOutput = UiBroker_showOutput;
UiBroker.prototype.showUrl = UiBroker_showUrl;

UiBroker.prototype.sessionTimeout = UiBroker_sessionTimeout;
UiBroker.prototype.pollProxy = UiBroker_pollProxy;
UiBroker.prototype.clearTimeoutHandles = UiBroker_clearTimeoutHandles;

//set context information 
UiBroker.prototype.setContextInfo = UiBroker_setContextInfo;

// set Background Color and Fore ground Color
UiBroker.prototype.setWndBGColor = UiBroker_setWndBGColor;
UiBroker.prototype.setCtrlBGColor = UiBroker_setCtrlBGColor;
UiBroker.prototype.setTxtColor = UiBroker_setTxtColor;

UiBroker.prototype.registerUIFrames = UiBroker_registerUIFrames;

// set and get method for ui Look
UiBroker.prototype.setUILook = UiBroker_setUILook;
UiBroker.prototype.getUILook = UiBroker_getUILook;
UiBroker.prototype.setAppearence = UiBroker_setAppearence;
UiBroker.prototype.getAppearence = UiBroker_getAppearence;

UiBroker.prototype.terminate = UiBroker_terminate;

timeArray = new Array ();
wdw = null;

function writeTimes (optTimeAry)
{
  if (! wdw) {
    wdw = window.open ("", "newconsole", "width=500,height=250,resizable=yes,scrollbars=yes", true);
    wdw.document.open ("text/plain");
  }
  var wdwDoc = wdw.document;
  if (optTimeAry) {
    var tAry = optTimeAry;
  } else {
    var tAry = timeArray;
  }
  var len = tAry.length;
  for (var i=0; i<len; i++) {
    var msec = tAry[i][1].valueOf() - tAry[i][0].valueOf (); 
    wdwDoc.writeln ('[' + msec + ']' + 'Elapsed ' + tAry[i][2]);
  }
  wdwDoc.writeln ('');
  tAry.length = 0;
  tAry = new Array ();
}


/*  czStack.js 115.5 2000/11/07 19:34:18 tabbott ship $ */

/**
* Class Stack
*/

function Stack ()
{
  this.entries = new Array ();
}

function Stack_push (stackEntry)
{
  var len = this.entries.length;
  this.entries[len] = stackEntry;
}

function Stack_pop ()
{
  var len = this.entries.length;
  if (len != 0) {
    var entry = this.entries[len-1];
    
    this.entries[len-1] = null;
    delete this.entries [len-1];
    this.entries.length--;
    
    return (entry);
  } else
    return null;
}

function Stack_peek ()
{
  return (this.entries[this.entries.length-1]);
}

function Stack_isEmpty ()
{
  return (this.entries.length == 0);
}

/**
* Public Methods
*/
Stack.prototype.push  = Stack_push;
Stack.prototype.pop   = Stack_pop;
Stack.prototype.peek  = Stack_peek;
Stack.prototype.isEmpty = Stack_isEmpty;
