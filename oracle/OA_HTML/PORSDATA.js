/*=================================================================+
|               Copyright (c) 1998 Oracle Corporation              |
|                  Redwood Shores, California, USA                 |
|                       All rights reserved.                       |
+==================================================================*/

/* $Header: PORSDATA.js 115.9 1999/11/04 13:23:01 pkm ship     $ */

TOOLTIP_IN_DELAY=1000;
TOOLTIP_OUT_DELAY=1000;

reqSessionId=0;
reqHeaderId=0;

PERCENT_DIGIT=5;
QUANTITY_DIGIT=5;
allForms= new Array();

//!! data below this are temporary, to be provided by middle tier
PROFILE_OVERRIDE_REQUESTER= 'NO';
PROFILE_OVERRIDE_LOCATION= 'NO';
if(!FND_MESSAGES)
{
  FND_MESSAGES= new Array();
}


var editReqTabs=new Array();



editReqTabs[0]= new Tab(FND_MESSAGES["ICX_POR_TAB_SELECTED_ITEMS"],
                        true,
                        returnTrue,
                        switchToEditLines,
                        FND_MESSAGES["ICX_POR_EDIT_ITEM_INFO"]
                        );

editReqTabs[1]= new Tab(FND_MESSAGES["ICX_POR_TAB_REQ_INFO"],
                        false, 

  // bug 1028794
  //                      returnTrue,
			new Function("return returnCheckTextArea('POR_HEADER_R')"),
                        switchToEditHeader,
                        FND_MESSAGES["ICX_POR_ENTER_REQ_HEADER_INFO"]+
                        "<img src=/OA_MEDIA/FNDIREQD.gif>" +
                        FND_MESSAGES["ICX_POR_REQUIRED_FIELD"]
                        );

editReqIndex = new Index(FND_MESSAGES["ICX_POR_TASK_EDIT"],
                         editReqTabs);

emgEditReqIndex = new Index(FND_MESSAGES["ICX_POR_TASK_EMG_EDIT"],
                         editReqTabs);


var addItemsTabs=new Array();

addItemsTabs[0]= new Tab(FND_MESSAGES["ICX_POR_TAB_SEARCH"],
                         true,
                         checkSelectedItems,
                         new Function("switchContent('/OA_HTML/PORSRCON.htm')"),
                         "&nbsp;"
                         );


addItemsTabs[1]= new Tab(FND_MESSAGES["ICX_POR_TAB_VIEW_LISTS"],
                         false, 
                         favListLinesChecked,
                         new Function('switchContent("/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN="+  getReqToken("favListContent", "viewFavList", "requisition"))'),
                         "&nbsp;"                         
                         );

	
addItemsTabs[2]= new Tab(FND_MESSAGES["ICX_POR_TAB_SPECIAL_ORDER"],
                         false, 
                         specialOrderEntered,
                         new Function('switchContent("/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN=" +  getReqToken("nonCatalogItemContent", "nonCatalogItemContent", "requisition"))'),
                         "&nbsp;"
                         );

addItemsIndex = new Index(FND_MESSAGES["ICX_POR_TASK_ADD_ITEMS"],
                          addItemsTabs);

if (emergencyReqFlag == 'Y')
{

  var emgAddItemsTabs=new Array();

  emgAddItemsTabs[0]= new Tab(FND_MESSAGES["ICX_POR_TAB_SEARCH"],
                           true,
                           checkSelectedItems,
                           new Function("switchContent('/OA_HTML/PORSRCON.htm')"),
                           "&nbsp;"
                           );


  if (emgRestrictToTemplates == 'N')
  {

    emgAddItemsTabs[1]= new Tab(FND_MESSAGES["ICX_POR_TAB_VIEW_LISTS"],
                             false, 
                             favListLinesChecked,
                             new Function('switchContent("/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN="+  getReqToken("favListContent", "viewFavList", "requisition"))'),
                             "&nbsp;"                         
                             );

	
    emgAddItemsTabs[2]= new Tab(FND_MESSAGES["ICX_POR_TAB_SPECIAL_ORDER"],
                             false, 
                             specialOrderEntered,
                             new Function('switchContent("/" + JAVA_VIRTUAL_PATH + "/oracle.apps.icx.por.apps.RealOrder?REQ_TOKEN=" +  getReqToken("nonCatalogItemContent", "nonCatalogItemContent", "requisition"))'),
                             "&nbsp;"
                             );
  }

  emgAddItemsIndex = new Index(FND_MESSAGES["ICX_POR_TASK_EMG_ADD_ITEMS"],
                            emgAddItemsTabs);

}


viewReqTabs= new Array();
   
viewReqIndex = new Index(FND_MESSAGES["ICX_POR_TASK_VIEW_REQS"],
                         viewReqTabs,
                         top.FND_MESSAGES["ICX_ADD_ITEMS_DES"]);



ATTRIBUTE_DEPENDENCY= new Array();

ATTRIBUTE_DEPENDENCY["POR_PROJECT"]= ["POR_PROJECT_ID", "POR_TASK_ID",
                                     "POR_TASK",
                                     "POR_EXPENDITURE_TYPE"];

ATTRIBUTE_DEPENDENCY["POR_TASK"]= ["POR_TASK_ID"];

//??  contact id supplier id
ATTRIBUTE_DEPENDENCY["POR_SUPPL_NAME"]=["POR_SUPPL_SITE",
                                       "POR_SUPPL_CONTACT",
                                       "POR_SUPPL_CONTACT_PHONE"];

ATTRIBUTE_DEPENDENCY["POR_DELIVER_TO_LOCATION"]=["POR_DELIVER_TO_LOCATION_ID",
                                                "POR_DELIVER_TO_ORG",
                                                "POR_DELIVER_TO_ORG_ID",
												"POR_DELIVER_TO_SUBINVENTORY",
												"POR_SUBINVENTORY"];

ATTRIBUTE_DEPENDENCY["POR_REQUESTER"]=["POR_REQUESTER_ID",
                                      "POR_REQUESTER_PHONE",
                                      "POR_REQUESTER_FAX",
                                      "POR_REQUESTER_EMAIL"];

ATTRIBUTE_DEPENDENCY["POR_DELIVER_TO_SUBINVENTORY"]=["POR_DELIVER_TO_SUBINVENTORY_ID"];


// Bug 903689 - this array contains a list of forms that may be more than once on a page,
// but should be submitted together, even if only one of them is updated.
// Bug 940581 - onChange event on approval list window is not firing, force sending all
// forms to the server
// FORM_SUBMIT_ALL = new Array('POR_LINE_DETAILS_R', 'POR_APPROVERS_R');

// Bug 966189 - This seems to be a general issue with IE 4 - adding all single row forms
// to this array
FORM_SUBMIT_ALL = new Array('POR_LINE_DETAILS_R',
                            'POR_APPROVERS_R', 
							'POR_HEADER_R', 
							'POR_INFO_TEMPLATE_R', 
                            'POR_MASS_UPDATE_R', 
                            'POR_EDIT_ATTACHMENTS_R');

function switchToEditLines()
{
  hide(getLayer("header_content"+suffix));
  show(getLayer("lines_content"+suffix));
  hide(getLayer("header_buttons"+suffix));
  show(getLayer("lines_buttons"+suffix));
  top.findFrame(top, "por_content").scroll(0,0);
}

function switchToEditHeader()
{
  hide(getLayer("lines_content"+suffix));
  show(getLayer("header_content"+suffix));
  hide(getLayer("lines_buttons"+suffix));
  show(getLayer("header_buttons"+suffix));
  top.findFrame(top, "por_content").scroll(0,0);
}

function returnTrue()
{
  if (checkSubmitFlag())
    return false;

  return true;
}

function returnCheckTextArea(formName)
{
  if (checkSubmitFlag())
    return false;

  return top.checkTextArea(formName);

}





function switchContent(url)
{
// this is commented out otherwise cannot click back button
// on edit reqs page.  onSwitchOut for tabs does this check.
//  if (checkSubmitFlag())
//    return;

  findFrame(top, "por_content").location=url;
}

function switchMain(url)
{
  findFrame(top, "por_main").location=url;
}

function checkSelectedItems()
{
  if (checkSubmitFlag())
    return;

  var searchHeader=findFrame(top, 'searchHeader');
  if(searchHeader&& searchHeader.hasSelectedItems &&searchHeader.hasSelectedItems())
  {
    alert(top.FND_MESSAGES["ICX_POR_ALRT_ADD_OR_DESELECT"]);
    return false;
  }
  else 
  {
    window.defaultStatus="";
    return true;
  }
}

function favListLinesChecked()
{
  if (checkSubmitFlag())
    return;

  var contentFrame=findFrame(top, 'content_results');
  if(contentFrame&& contentFrame.linesChecked &&contentFrame.linesChecked())
  {
    alert(top.FND_MESSAGES["ICX_POR_ALRT_ADD_OR_DESELECT"]);
    return false;
  }
  return true;
}

function specialOrderEntered()
{
  if (checkSubmitFlag())
    return;
 

  // Bug 953813 - return if form not yet generated
  if (!top.findFrame(top, "content_middle"))
    return true;

  var spForm;
  var itemType= top.findFrame(top, "content_middle").document.selectedItemType;
  if(itemType== "goods")
  {
    spForm=top.allForms["POR_SPECIAL_ORDER_GOODS_R"][0];
  }
  else if(itemType== "rate")
  {
    spForm=top.allForms["POR_SPECIAL_ORDER_RATE_R"][0];
  }
  else if(itemType== "amount")
  {
    spForm=top.allForms["POR_SPECIAL_ORDER_AMOUNT_R"][0];
  }
  
  // Bug 953813 - return if form not yet generated
  if (!spForm)
    return true;

  if(!spForm.updated)
  {
    return true;
  }
  for(var i=0; i<spForm.elements.length; i++)
  {
    if(spForm.elements[i].type=="text" &&
       !isEmpty(spForm.elements[i].value))
    {
      alert(top.FND_MESSAGES["ICX_POR_ALRT_ADD_OR_CLEAR"]);
      return false;
    }
  }

  itemType= top.findFrame(top, "content_middle").document.selectedItemType;
  if(itemType== "goods")
  {
 // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_GOODS_R")) return false;

  }
  else if(itemType== "rate")
  {
 // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_RATE_R")) return false;

  }
  else if(itemType== "amount")
  {
 // bug 1028794
  if(!top.checkTextArea("POR_SPECIAL_ORDER_AMOUNT_R")) return false;
  }


  return true;
}


function switchToSearchReq()
{
  submitRequest('requisition', '/' + JAVA_VIRTUAL_PATH + '/oracle.apps.icx.por.apps.Order', 'viewReqsContent', 'viewReqsContent', '', new Array(),'por_content');  
}




