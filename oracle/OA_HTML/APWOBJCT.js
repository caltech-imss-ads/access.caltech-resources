//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        APWOBJCT.js                       |
//  |                                                |
//  | Description: Client-side objects definition    |
//  |              				     |     
//  |                                                |
//  | History:     Created 03/07/2000                |
//  |              David Tong                        |
//  |                                                |
//  +================================================+
/* $Header: APWOBJCT.js 115.24 2000/09/13 13:28:47 pkm ship     $ */


/*===========================================================================*
 *  ProfileOptions Class - Container Of ProfileOption Values                 *
 *===========================================================================*/

 /*--------------------------------------------* 
  * CONSTRUCTOR ProfileOptions:                *
  *   Params:  None                            *
  *  Returns:  None                            * 
  *     Desc:  Creates a ProfileOptions object *
  *--------------------------------------------*/ 
  function ProfileOptions() {
    this.arrProfiles = new Object();
  }


 /*---------------------------------------------------------------*
  * METHOD mvAddProfileOption:                                    *
  *   Params:  p_strProfileName -  Profile Option Name (string)   *
  *            p_vValue         -  Profile Option Value           *
  *  Returns:  Profile Option Value added to Profile Options Obj  *
  *     Desc:  Adds a profile option with name p_strProfileName   *
  *            with value p_vValue in the Profile Options Object  *
  *            and returns the new profile option value           *
  *---------------------------------------------------------------*/
  function mvAddProfileOption(p_strProfileName, p_vValue) {
    this.arrProfiles[p_strProfileName] = p_vValue;
    return this.arrProfiles[p_strProfileName];
  }


/*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mvGetProfileOption:                                    *
  *   Params:  p_strProfileName -  Profile Option Name (String)             *
  *  Returns:  Profile Option Value or null                                 *
  *     Desc:  Returns the Profile Option Value with the name               *
  *            p_strProfileName.  If profile name does not exist null is    *
  *            returned                                                     *
  *-------------------------------------------------------------------------*/
  function mvGetProfileOption(p_strProfileName) {
    var undefined;

    if (this.arrProfiles[p_strProfileName] == undefined)
      return null;
    return this.arrProfiles[p_strProfileName];
  }

  ProfileOptions.prototype.mvAddProfileOption    = mvAddProfileOption;
  ProfileOptions.prototype.mvGetProfileOption    = mvGetProfileOption;

  top.g_objProfileOptions = new top.ProfileOptions();

/*===========================================================================*
 *  Messages Class - Container Of Message Text                               *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Messages:                    *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Messages object     *
  *------------------------------------------*/ 
  function Messages() {
    this.arrMessages = new Object();
  }

 /*----------------------------------------------------------------*
  * METHOD mAddMessage:                                            *
  *   Params:  p_strMsgName -  Message Name (string)               *
  *            p_strMsgText -  Message Text (string)               *
  *  Returns:  None                                                *
  *     Desc:  Adds a message with name p_strMsgName with the      *
  *            message text of p_strMsgText to the Messages object *
  *----------------------------------------------------------------*/
  function mstrAddMesg(p_strMsgName, p_strMsgText)
  {
    this.arrMessages[p_strMsgName] = p_strMsgText;
  }

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetMessage:                                        *
  *   Params:  p_strMsgName -  Message Name (string)                        *
  *  Returns:  Message Text or null                                         *
  *     Desc:  Returns the message text with the name p_strMsgName.         *
  *            If the message name does not exist null is returned          *
  *-------------------------------------------------------------------------*/
  function mstrGetMesg(p_strMsgName)
  {
    var undefined;

    if (this.arrMessages[p_strMsgName] == undefined)
      return null;
    return this.arrMessages[p_strMsgName];

  }

  Messages.prototype.mstrAddMesg         = mstrAddMesg;
  Messages.prototype.mstrGetMesg         = mstrGetMesg;

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetMessageWTokensReplace:                          *
  *   Params:  p_strMsgName -  Message Name (string)                        *
  *            p_arrTokenNames  -  Array of Token Names (array of strings)  *
  *            p_arrTokenVals   -  Array of Token Values (array of strings) *
  *  Returns:  Message Text or null                                         *
  *     Desc:  Returns the message text with the name p_strMsgName.         *
  *            If the message name does not exist null is returned          *
  *-------------------------------------------------------------------------*/
  function mstrGetMesgWTokenReplace(p_strMsgName, 
                                    p_arrTokenNames, 
                                    p_arrTokenVals)
  {
    var strMsg = this.arrMessages[p_strMsgName]; 
    var count = 0;
    var undefined;

    while (p_arrTokenNames[count] != undefined){
      strMsg = strMsg.replace(p_arrTokenNames[count], p_arrTokenVals[count]);
      count++;  
    }

    strNew = " " + strMsg; 
    return strNew;
  }
  Messages.prototype.mstrGetMesgWTokenReplace       = mstrGetMesgWTokenReplace;

/*===========================================================================*
 *  Regions Class - Container of Regions                                     *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Regions:                     *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Regions object      *
  *------------------------------------------*/ 
  function Regions() {
    this.arrRegions = new Object();
  }

 /*---------------------------------------------------------------*
  * METHOD mobjAddRegion:                                         *
  *   Params:  p_strRegionName -  Region_Name of type string      *
  *  Returns:  Region Object added to Regions Array Object        *
  *     Desc:  Creates a Region object with name p_strRegionName  *
  *            and adds it to the Regions object and returns the  *
  *            new region                                         *
  *---------------------------------------------------------------*/
  function mobjAddRegion(p_strRegionName) {
    this.arrRegions[p_strRegionName] = new Region();
    return this.arrRegions[p_strRegionName];
  }

 /*---------------------------------------------------------------------*
  * ACCCESSOR METHOD mobjGetRegion:                                     *
  *   Params:  p_strRegionName -  Region Name of type string            *
  *  Returns:  Region Object or null                                    *
  *     Desc:  Returns the Region object with the name p_strRegionName. *  
  *            If the region does not exist null is returned            *
  *---------------------------------------------------------------------*/
  function mobjGetRegion(p_strRegionName) {
    var undefined;
    if (this.arrRegions[p_strRegionName] == undefined)
      return null;
    return this.arrRegions[p_strRegionName];
  }

  Regions.prototype.mobjAddRegion    = mobjAddRegion;
  Regions.prototype.mobjGetRegion    = mobjGetRegion;

/*===========================================================================*
 *  Region Class - Container of Region Items                                 *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR Region:                      *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Region object       *
  *------------------------------------------*/ 
  function Region() {
    this.arrRegionItems = new Object();
  }

 /*-----------------------------------------------------------------------*
  * METHOD mobjAddRegionItem:                                             *
  *   Params:  p_strRegionItemName -  Region Item Name (string)           *
  *            p_strPrompt         -  Prompt of Region Item (string)      *
  *            p_iPromptLength     -  Prompt Display Length (int)         *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  Region Item Object added to Region Object                  *
  *     Desc:  Creates a Region Item object with name p_strRegionItemName *
  *            and adds it to the Region object and returns the new       *
  *            region item                                                *
  *-----------------------------------------------------------------------*/
  function mobjAddRegionItem(p_strRegionItemName, 
                             p_strPrompt, 
                             p_iPromptLength,
                             p_iValueLength,
                             p_strUpdateFlag,
                             p_strRequiredFlag) {
    this.arrRegionItems[p_strRegionItemName] = 
      new RegionItem(p_strPrompt, 
                     p_iPromptLength,
                     p_iValueLength,
                     p_strUpdateFlag,
                     p_strRequiredFlag);
    return this.arrRegionItems[p_strRegionItemName];
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mobjGetRegionItem:                                       *
  *   Params:  p_strRegionItemName -  Region Item Name (string)              *
  *  Returns:  Region Item Object or null                                    *
  *     Desc:  Returns the Region Item object with name p_strRegionItemName. *
  *            If the region item does not exist null is returned            *
  *--------------------------------------------------------------------------*/
  function mobjGetRegionItem(p_strRegionItemName) {
    var undefined;
    if (this.arrRegionItems[p_strRegionItemName] == undefined)
      return null;
    return this.arrRegionItems[p_strRegionItemName];
  }

  Region.prototype.mobjAddRegionItem    = mobjAddRegionItem;
  Region.prototype.mobjGetRegionItem    = mobjGetRegionItem;

/*===========================================================================*
 *  Region Item Class                                                        *
 *===========================================================================*/


// Constructor to create the RegionItem object
 /*-----------------------------------------------------------------------*
  * CONSTRUCTOR RegionItem:                                               *
  *   Params:  p_strPrompt          -  Prompt of Region Item (string)     *
  *            p_iPromptLength      -  Prompt Display Length (int)        *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  None                                                       *
  *     Desc:  Creates a Region Item object with the attributes assigned  *
  *            to the parameters passed                                   *
  *-----------------------------------------------------------------------*/
  function RegionItem(p_strPrompt, 
                      p_iPromptLength,
                      p_iValueLength,
                      p_strUpdateFlag,
                      p_strRequiredFlag) {
    this.strPrompt       = p_strPrompt;
    this.iPromptLength   = p_iPromptLength;
    this.iValueLength    = p_iValueLength;
    this.strUpdateFlag   = p_strUpdateFlag;
    this.strRequiredFlag = p_strRequiredFlag;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mstrGetPrompt:                                           *
  *   Params:  None                                                          *
  *  Returns:  Prompt of Region Item (string)                                *
  *     Desc:  Returns the Prompt of the Region Item object                  *
  *--------------------------------------------------------------------------*/
  function mstrGetPrompt() {
    return this.strPrompt;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD miGetPromptLength:                                       *
  *   Params:  None                                                          *
  *  Returns:  Prompt Display Length of Region Item (int)                    *
  *     Desc:  Returns the Prompt Length of the Region Item object           *
  *--------------------------------------------------------------------------*/
  function miGetPromptLength() {
    return this.iPromptLength;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD miGetValueLength:                                        *
  *   Params:  None                                                          *
  *  Returns:  Value Display Length of Region Item (int)                     *
  *     Desc:  Returns the Value Length of the Region Item object            *
  *--------------------------------------------------------------------------*/
  function miGetValueLength() {
    return this.iValueLength;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mtrGetUpdateFlag:                                        *
  *   Params:  None                                                          *
  *  Returns:  'Y' or 'N'                                                    *
  *     Desc:  Returns the Update Flag indicating if the field is updateable *
  *            or not 'Y' if updateable and 'N' if not                       *
  *--------------------------------------------------------------------------*/
  function mstrGetUpdateFlag() {
    return this.strUpdateFlag;
  }

 /*--------------------------------------------------------------------------*
  * ACCESSOR METHOD mtrGetRequiredFlag:                                      *
  *   Params:  None                                                          *
  *  Returns:  'Y' or 'N'                                                    *
  *     Desc:  Returns the Required Flag indicating if the field is required *
  *            or not, 'Y' if required and 'N' if not                        *
  *--------------------------------------------------------------------------*/
  function mstrGetRequiredFlag() {
    return this.strRequiredFlag;
  }

 /*-----------------------------------------------------------------------*
  * ACCESSOR METHOD mGetAttributes:                                       *
  *   Params:  p_strPrompt         -  Prompt of Region Item (string)      *
  *            p_iPromptLength     -  Prompt Display Length (int)         *
  *            p_iValueLength      -  Value Display Length (int)          *
  *            p_strUpdateFlag     -  Can field be updated (Y or N)       *
  *            p_strRequiredFlag   -  Is Field Required (Y or N)          *
  *  Returns:  None                                                       *
  *     Desc:  All the parameters are out parameters.  Function, returns  *
  *            all the attributes of the Region Item object by assigning  *
  *            them to the parameters.                                    *
  *-----------------------------------------------------------------------*/
  function mGetAttributes(p_strPrompt, 
                          p_iPromptLength,
                          p_iValueLength,
                          p_strUpdateFlag,
                          p_strRequiredFlag) {
    p_strPrompt       = this.iValueLength;
    p_iPromptLength   = this.strPrompt;
    p_iValueLength    = this.iValueLength;
    p_strUpdateFlag   = this.strUpdateFlag;
    p_strRequiredFlag = this.strRequiredFlag;
  }

  RegionItem.prototype.mstrGetPrompt       = mstrGetPrompt;
  RegionItem.prototype.miGetPromptLength   = miGetPromptLength;
  RegionItem.prototype.miGetValueLength    = miGetValueLength;
  RegionItem.prototype.mstrGetUpdateFlag   = mstrGetUpdateFlag;
  RegionItem.prototype.mstrGetRequiredFlag = mstrGetRequiredFlag;
  RegionItem.prototype.mGetAttributes      = mGetAttributes;


/*===========================================================================*
 *  LookupTypes Class - Container of Lookup Types                            *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR LookupTypes:                 *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a LookupTypes object  *
  *------------------------------------------*/ 
  function LookupTypes() {
    this.arrLookupTypes = new Object();
  }

 /*---------------------------------------------------------------*
  * METHOD mobjAddLookupType:                                     *
  *   Params:  p_strLookupType -  Lookup Type of type string      *
  *  Returns:  Lookup Type Object added to Lookup Types Object    *
  *     Desc:  Creates a Lookup Type object with name             *
  *            p_strLookup Type and adds it to the LookupTypes    *
  *            object and returns the new Lookup Type             *
  *---------------------------------------------------------------*/
  function mobjAddLookupType(p_strLookupTypeName) {
    this.arrLookupTypes[p_strLookupType] = new LookupType();
    return this.arrLookupTypes[p_strLookupType];
  }

 /*---------------------------------------------------------------------*
  * ACCCESSOR METHOD mobjLookupType:                                    *
  *   Params:  p_strLookupTypeName -  Lookup Type of type string        *
  *  Returns:  Lookup Type Object or null                               *
  *     Desc:  Returns the Lookup Type object with the name             *
  *            p_strLookupType.  If the region does not exist null is   *
  *            returned.                                                *
  *---------------------------------------------------------------------*/
  function mobjGetLookupType(p_strLookupType) {
    var undefined;
    if (this.arrLookupTypes[p_strLookupType] == undefined)
      return null;
    return this.arrLookupTypes[p_strLookupType];
  }

  LookupTypes.prototype.mobjAddLookupType  = mobjAddLookupType;
  LookupTypes.prototype.mobjGetLookupType  = mobjGetLookupType;

/*===========================================================================*
 *  LookupType Class - Container of Lookup Code displayed text               *
 *===========================================================================*/

 /*------------------------------------------* 
  * CONSTRUCTOR LookupType:                  *
  *   Params:  None                          *
  *  Returns:  None                          * 
  *     Desc:  Creates a Lookup Type object  *
  *------------------------------------------*/ 
  function LookupType() {
    this.arrLookupCodes = new Object();
  }

 /*-----------------------------------------------------------------------*
  * METHOD mobjAddLookupCode:                                             *
  *   Params:  p_strLookupCode     -  Lookup Code (string)                *
  *            p_strDisplayedVal   -  Displayed Value of Lookup (string)  *
  *  Returns:  n/a                                                        *
  *     Desc:  Adds the lookup code to the Lookup type object and         *
  *-----------------------------------------------------------------------*/
  function mobjAddLookupCode(p_strLookupCode, 
                             p_strDisplayedVal) 
  {
    this.arrLookupCodes[p_strLookupCode] = p_strDisplayedVal; 
  }

 /*-------------------------------------------------------------------------*
  * ACCCESSOR METHOD mstrGetLookupCode:                                     *
  *   Params:  p_strLookupCode -  Lookup Code(string)                       *
  *  Returns:  Displayed Value or null                                      *
  *     Desc:  Returns the displayed value of lookup code p_strLookupCode.  *
  *            If the lookup code does not exist null is returned           *
  *-------------------------------------------------------------------------*/
  function mobjGetLookupCode(p_strLookupCode)
  {
    var undefined;

    if (this.arrLookupCodes[p_strLookupCode] == undefined)
      return null;
    return this.arrLookupCodes[p_strLookupCode];

  }

  LookupType.prototype.mobjAddLookupCode    = mobjAddLookupCode;
  LookupType.prototype.mobjGetLookupCode    = mobjGetLookupCode;


/*===========================================================================*
 *  countryInfo Class                                                        *
 *===========================================================================*/

  function countryInfo(code, name)
  {   
    this.code = code;
    this.name = name;
  }

  function CY_getCode()
  {
    return this.code;
  }

  function CY_getName()
  { 
    return this.name;
  }

  countryInfo.prototype.getCode = CY_getCode;
  countryInfo.prototype.getName = CY_getName;


/*===========================================================================*
 *  creditCardTransaction Class                                              *
 *===========================================================================*/

  function creditCardTransaction(id, 
                                 date, 
                                 folioType, 
                                 merchant, 
                                 city, 
                                 amt, 
                                 trxCurrency, 
                                 trxAmt, 
                                 category, 
                                 refNum)
  {
    this.id               = id;
    this.date             = date;
    this.merchant         = merchant;
    this.city             = city;
    this.amt              = amt;
    this.folioType        = folioType;  
    this.trxCurrency      = trxCurrency;
    this.trxAmt           = trxAmt;
    this.category         = category;
    this.refNum           = refNum;
    this.oldCategory      = category;
  }

  // new creditCardTransaction(null, null, null, null, null, null, null);  

  function TRX_getId()
  {  
    return this.id; 
  }

  function TRX_getDate()
  {  
    return this.date; 
  }

  function TRX_getFolioType()
  {  
    return this.folioType; 
  }

  function TRX_getMerchant()
  {  
    return this.merchant; 
  }

  function TRX_getCity()
  {  
    return this.city; 
  }

  function TRX_getAmount()
  {  
    return this.amt; 
  }

  function TRX_getTrxCurr()
  {  
    return this.trxCurrency; 
  }


  function TRX_getTrxAmt()
  {    
    return this.trxAmt; 
  }

  function TRX_getRefNum() 
  { 
    return this.refNum;
  }

  function TRX_getCategory()
  {    
    return this.category; 
  }

  function TRX_setCategory(category)
  {    
    this.category = category; 
  }

  function TRX_isCategoryChanged()
  { 
    return (this.category != this.oldCategory);
  }

  function TRX_copy(trx)
  {
    this.id           = trx.getId();
    this.date         = trx.getDate();
    this.merchant     = trx.getMerchant();
    this.city         = trx.getCity();
    this.amt          = trx.getAmt();
    this.folioType    = trx.getFolioType();
    this.trxCurrency  = trx.getTrxCurr();
    this.trxAmt       = trx.getTrxAmt();
    this.category     = trx.getCategory();
    this.refNum       = trx.getRefNum();
    this.oldCategory  = trx.oldCategory;
  }

  function disputedCharges()
  {
    this.paymentTrxn  = null;
    this.creditTrxn   = null;
  }

  creditCardTransaction.prototype.getId             = TRX_getId;
  creditCardTransaction.prototype.getDate           = TRX_getDate;
  creditCardTransaction.prototype.getFolioType      = TRX_getFolioType;
  creditCardTransaction.prototype.getMerchant       = TRX_getMerchant;
  creditCardTransaction.prototype.getCity           = TRX_getCity;
  creditCardTransaction.prototype.getAmt            = TRX_getAmount;
  creditCardTransaction.prototype.getTrxCurr        = TRX_getTrxCurr;
  creditCardTransaction.prototype.getTrxAmt         = TRX_getTrxAmt;
  creditCardTransaction.prototype.getRefNum         = TRX_getRefNum;
  creditCardTransaction.prototype.getCategory       = TRX_getCategory;
  creditCardTransaction.prototype.setCategory       = TRX_setCategory;
  creditCardTransaction.prototype.isCategoryChanged = TRX_isCategoryChanged;
  creditCardTransaction.prototype.copy              = TRX_copy;

/*===========================================================================*
 *  creditCardTransactions Class - Container Class of Credit Card Trxns      *
 *===========================================================================*/

  // constructor for creditCardTransactions object
  
  function creditCardTransactions()
  {
    this.next       = null;
    this.arr        = new Array();
    this.disputeArr = new Array();
  }

  // Create a prototype object
  //new creditCardTransactions();

  function CCT_add(id, 
                   date, 
                   expType, 
                   merchant, 
                   city, 
                   amt, 
                   trxCurrency, 
                   trxAmt, 
                   category)
  {
    if (arguments.length == 1) 
      this.arr[this.arr.length] = arguments[0];
    else
      this.arr[this.arr.length] = new creditCardTransaction(id, 
                                                            date, 
                                                            expType, 
                                                            merchant, 
                                                            city, 
                                                            amt, 
                                                            trxCurrency, 
                                                            trxAmt, 
                                                            category); 
    
    return;
  }

  function CCT_insert(id, 
                      date, 
                      folioType, 
                      merchant, 
                      city, 
                      amt, 
                      trxCurrency, 
                      trxAmt, 
                      category)
  {
    var date1 = fStringToDate(date, false);
    var date2;
    var found = false;
    var i = 0;

    while (i<this.arr.length && found == false)
    {
       date2 = fStringToDate(this.arr[i].getDate(), false);
       if (date1.getTime() < date2.getTime())
           found = true;
       else
           i++;
    }

    this.arr[this.arr.length] = new creditCardTransaction(null,
                                                          null,
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null, 
                                                          null); 
    var j = 0;

    for (j=this.arr.length-2; j >= i; j--)
      this.arr[j+1].copy(this.arr[j]);

    this.arr[i] = new creditCardTransaction(id, 
                                            date, 
                                            folioType, 
                                            merchant, 
                                            city, 
                                            amt, 
                                            trxCurrency, 
                                            trxAmt, 
                                            category);
 
    return ;
  }

  function CCT_getTrx(id)
  {
    for (var i=0; i<this.arr.length; i++)
    {
      if (this.arr[i].id == id) 
        return this.arr[i];
    }
    return null;
  }


  function CCT_getNumberOfTransactions(id)
  {
    return this.arr.length;
  }

  function CCT_reset()
  {
    if (this.arr.length > 0)
      this.next = 0;
    else
      this.next = null;
    return ;
  }

  function CCT_nextTrxn()
  {
    return ((this.next != null) && (this.next < this.arr.length) 
                                       ? this.arr[this.next++] : null);
  }

  // this method MUST be called when:
  // 1. after the transactions object is inititalized
  // 2. after some charges are disputed
  // 3. after some disputed charges are cleared
  function CCT_flagDisputedCreditCharges()
  {
     var creditArr = new Array();

     this.disputeArr.length = 0;  // reset it

     for (var i=0; i< this.arr.length; i++)
     {
       if (this.arr[i].getCategory() == g_cDispute) {
         this.disputeArr[this.disputeArr.length] = new disputedCharges();
         this.disputeArr[this.disputeArr.length-1].paymentTrxn = this.arr[i];
       }else{
         if (this.arr[i].getCategory() == g_cCredit)
           this.arr[i].setCategory("Business");

         if (this.arr[i].getTrxAmt() < 0)
           creditArr[creditArr.length] = this.arr[i];
       }
     }

    // Flag the credit charges for disputed ones
    if (this.disputeArr.length > 0) 
    {
      for (var i=0; i< creditArr.length; i++)
      {
        for (var j=0; j< this.disputeArr.length; j++)
        {    
          if (creditArr[i].getMerchant() == 
                              this.disputeArr[j].paymentTrxn.getMerchant() &&
              creditArr[i].getTrxAmt() == -
                              (this.disputeArr[j].paymentTrxn.getTrxAmt()) &&
              creditArr[i].getRefNum() == 
                               this.disputeArr[j].paymentTrxn.getRefNum())
          {
            creditArr[i].setCategory(g_cCredit);
            this.disputeArr[j].creditTrxn = creditArr[i];
          }
        }    //  close for (var j...
      }      //  close for (var i..
    }        //  close if (this....
  }          //  close function


  function CCT_getDisputedCharges() 
  { 
    return this.disputeArr; 
  }

  creditCardTransactions.prototype.add                        = CCT_add;
  creditCardTransactions.prototype.insert                     = CCT_insert;
  creditCardTransactions.prototype.getTrx                     = CCT_getTrx;
  creditCardTransactions.prototype.getNumberOfTransactions    = 
                                                 CCT_getNumberOfTransactions;
  creditCardTransactions.prototype.reset                      = CCT_reset;
  creditCardTransactions.prototype.nextTrxn                   = CCT_nextTrxn;
  creditCardTransactions.prototype.fFlagDisputedCreditCharges = 
                                                 CCT_flagDisputedCreditCharges
  creditCardTransactions.prototype.fGetDisputedCharges        = 
                                                 CCT_getDisputedCharges;

/*===========================================================================*
 *  creditCardData Class                                                     *
 *===========================================================================*/

  //creditCardData constructor

  function creditCardData()
  {
    this.cardProgName = new Array();
    this.trxns        = new Array();
    this.paymtDueFrom = new Array();
    this.loaded       = false;
  }

  g_objCCardData = new creditCardData();

  creditCardData.prototype.dataLoaded            = CCD_dataLoaded;
  creditCardData.prototype.setLoaded             = CCD_setLoaded;
  creditCardData.prototype.add                   = CCD_add;
  creditCardData.prototype.getTrxns              = CCD_getTrxns;
  creditCardData.prototype.getPaymtDueFrom       = CCD_getPaymtDueFrom;
  creditCardData.prototype.getCardProgNames      = CCD_getCardProgNames;
  creditCardData.prototype.isDataChanged         = CCD_isDataChanged;
  creditCardData.prototype.getDataChanged        = CCD_getDataChanged;
  creditCardData.prototype.insertTrxn            = CCD_insertTrxn;
  creditCardData.prototype.packPersonalTrxns     = CCD_packPersonalTrxns;


  function CCD_dataLoaded() 
  { 
    return this.loaded; 
  }

  function CCD_setLoaded(loaded) 
  { 
    this.loaded = loaded; 
  }

  // add card program method
  function CCD_add(name, trxns, paymtDueFrom)
  {
    this.cardProgName[this.cardProgName.length] = name;
    this.trxns[this.trxns.length]               = trxns;
    this.paymtDueFrom[this.paymtDueFrom.length] = paymtDueFrom;
  }

  //CCD_getTrxns method to return the trxns for a card program
  function CCD_getTrxns(name)
  {
    for (var i=0; i<this.cardProgName.length; i++)
      if (this.cardProgName[i] == name) 
        return this.trxns[i];

    return null;
  }

  //CCD_getPaymtDueFrom method to return the paymentDueFrom for a card program
  function CCD_getPaymtDueFrom(name)
  {
    for (var i=0; i<this.cardProgName.length; i++)
      if (this.cardProgName[i] == name) 
        return this.paymtDueFrom[i];

    return null;
  }


  // method to return the array of card program name
  function CCD_getCardProgNames()
  { 
    return this.cardProgName; 
  }


  // To check if data(category) changed
  function CCD_isDataChanged()
  { var t;

    for (i=0; i< this.trxns.length; i++){   
      this.trxns[i].reset();
      t = this.trxns[i].nextTrxn();
      while (t != null){    
        if (t.isCategoryChanged()) return true;
          t = this.trxns[i].nextTrxn();
      }
    }

    return false;
  }

  // To return the trxns whose categories were changed. The format is:
  // <number>"@att@"<id>"@att@"<category>"@att@"<id>"@att@"<category>"@att@"...
  function CCD_getDataChanged()
  { 
    var s = "";
    var count=0;

    for (i=0; i< this.trxns.length; i++){   

      this.trxns[i].reset();
      t = this.trxns[i].nextTrxn();

      while (t != null){    

        if (t.isCategoryChanged()){   
          s += t.getId() + "@att@" + t.getCategory() + "@att@";
          count++;
        }
        t = this.trxns[i].nextTrxn();
      }

    }

    s = count + "@att@" + s;
    return s;
  }

  function CCD_insertTrxn(cardProgName, 
                          id, 
                          date, 
                          folioType, 
                          merchant, 
                          city, 
                          amt, 
                          trxCurrency, 
                          trxAmt, 
                          category)
  {
    for (i=0; i< this.cardProgName.length; i++){   

      if (this.cardProgName[i] == cardProgName){   

        this.trxns[i].insert(id, 
                             date, 
                             folioType, 
                             merchant, 
                             city, 
                             amt, 
                             trxCurrency, 
                             trxAmt, 
                             category);
         return;
       }
     }
  }


  // To return the trxns whose categories is PERSONAL. The format is:
  // <number>"@att@"<id>"@att@"<billed amount>"@att@"<id>"@att@"<billed amount>"@att@"...
  function CCD_packPersonalTrxns(cardProgName)
  { 
    var s = "";
    var count=0;

    for (i=0; i< this.trxns.length; i++){ 
  
      if (this.cardProgName[i] == cardProgName) {

        this.trxns[i].reset();
        t = this.trxns[i].nextTrxn();

        while (t != null){    

          if (t.getCategory() == g_cPersonal){   
            s += t.getId() + "@att@" + t.getAmt() + "@att@" + t.getTrxAmt() +
                  "@att@" + t.getDate() + "@att@" + t.getTrxCurr() + "@att@";
	    count++;
          }
               
          t = this.trxns[i].nextTrxn();
        }

        if (s != "") 
          s = count + "@att@" + s;
          return s;
        }
    }
    return s;
  }

/*===========================================================================*
 *  Receipt Class                                                            *
 *===========================================================================*/

function receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
if (arguments.length > 0) {
	this.date = date;
        if (fIsNum(occurs+"",false)) this.occurs = occurs; else this.occurs = 1;
        if (fIsNum(expensedAmount+"",false)) this.setExpensedAmount(expensedAmount); else this.setExpensedAmount(0);
        if (fIsNum(receiptAmount+"",false)) this.setReceiptAmount(receiptAmount); else this.setReceiptAmount(0);
	this.isMissing = ((isMissing) ? 'Y' : 'N');
        this.setReceiptCurr(receiptCurr);
        if (fIsNum(exchRate+"",false)) this.setExchRate(exchRate); else this.setExchRate(1);
	this.flexFields = new Array(); // of FlexFieldValues Object
        this.objExpenseType = objExpenseType; 
	this.justification = justification;
        this.cCardTrxnId = cCardTrxnId; 
        this.merchant = merchant;
	this.itemizeId = itemizeId; 
	this.expenseGroup = expenseGroup; 
        this.category = category;
        this.projectNumber = projectNumber;
        this.taskNumber = taskNumber;	
	this.taxInfo = new Tax();
 	this.reimbursAmount = this.calcReimbursAmount();
} else {
	this.date = null;
        this.occurs = null;
        this.setExpensedAmount(null); 
        this.setReceiptAmount(null);
	this.reimbursAmount = null;
	this.isMissing = 'N';
        this.setReceiptCurr(top.objExpenseReport.header.reimbursCurr);
        this.setExchRate(null);
	this.flexFields = new Array(); // of FlexFieldValues Object
        this.objExpenseType = null;
        this.taxInfo = new Tax();
	this.justification = "";
        this.cCardTrxnId = null; 
        this.merchant = null;
	this.itemizeId = null;
	this.expenseGroup = null; 
        this.category = "";
        this.projectNumber = "";
        this.taskNumber = "";
}

}



receiptObject.prototype.copy = R_copy;
receiptObject.prototype.getcCardTrxnId = R_getcCardTrxnId;
receiptObject.prototype.setJustification = R_setJustification;
receiptObject.prototype.getJustification = R_getJustification;
receiptObject.prototype.setIsMissing = R_setIsMissing;
receiptObject.prototype.getIsMissing = R_getIsMissing;
receiptObject.prototype.getGroup = R_getGroup;
receiptObject.prototype.setGroup = R_setGroup;
receiptObject.prototype.getCategory = R_getCategory;
receiptObject.prototype.getObjExpenseType = R_getObjExpenseType;
receiptObject.prototype.addFlexFields = R_addFlexFields;
receiptObject.prototype.setExpenseType = R_setExpenseType;
receiptObject.prototype.getCurrency = R_getCurrency;
receiptObject.prototype.setCurrency = R_setCurrency;
receiptObject.prototype.getOccurs = R_getOccurs;
receiptObject.prototype.setOccurs = R_setOccurs;
receiptObject.prototype.getDate = R_getDate;
receiptObject.prototype.setDate = R_setDate;
receiptObject.prototype.getReceiptCurr = R_getReceiptCurr;
receiptObject.prototype.setReceiptCurr = R_setReceiptCurr;
receiptObject.prototype.getReimbursAmount = R_getReimbursAmount;
receiptObject.prototype.setReimbursAmount = R_setReimbursAmount;
receiptObject.prototype.getExchRate = R_getExchRate;
receiptObject.prototype.setExchRate = R_setExchRate;
receiptObject.prototype.getDailyRate = R_getDailyRate;
receiptObject.prototype.getReceiptAmount = R_getReceiptAmount;
receiptObject.prototype.getNATaskNumber = R_getNATaskNumber;
receiptObject.prototype.getNAProjectNumber = R_getNAProjectNumber;
receiptObject.prototype.getTaskNumber = R_getTaskNumber;
receiptObject.prototype.getProjectNumber = R_getProjectNumber;
receiptObject.prototype.setTaskNumber = R_setTaskNumber;
receiptObject.prototype.setProjectNumber = R_setProjectNumber;
receiptObject.prototype.setReceiptAmount = R_setReceiptAmount;
receiptObject.prototype.getExpensedAmount = R_getExpensedAmount;
receiptObject.prototype.setExpensedAmount = R_setExpensedAmount;
receiptObject.prototype.setFlexValues = R_setFlexValues;
receiptObject.prototype.getAmountIncludesTax = R_getAmountIncludesTax;
receiptObject.prototype.setAmountIncludesTax = R_setAmountIncludesTax;
receiptObject.prototype.getOverrideFlag = R_getOverrideFlag;
receiptObject.prototype.setOverrideFlag = R_setOverrideFlag;
receiptObject.prototype.getTaxID = R_getTaxID;
receiptObject.prototype.setTaxID = R_setTaxID;
receiptObject.prototype.getTaxCode = R_getTaxCode;
receiptObject.prototype.getTaxCodeID = R_getTaxCodeID;
receiptObject.prototype.setTaxCode = R_setTaxCode;
receiptObject.prototype.getSupCountry = R_getSupCountry;
receiptObject.prototype.setSupCountry = R_setSupCountry;
receiptObject.prototype.getTaxPayerID = R_getTaxPayerID;
receiptObject.prototype.setTaxPayerID = R_setTaxPayerID;
receiptObject.prototype.getTaxRegNum = R_getTaxRegNum;
receiptObject.prototype.setTaxRegNum = R_setTaxRegNum;
receiptObject.prototype.getMerchRef = R_getMerchRef;
receiptObject.prototype.setMerchRef = R_setMerchRef;
receiptObject.prototype.getMerchant = R_getMerchant;
receiptObject.prototype.setMerchant = R_setMerchant;
receiptObject.prototype.getDocNum = R_getDocNum;
receiptObject.prototype.setDocNum = R_setDocNum;
receiptObject.prototype.addTax = R_addTax;
receiptObject.prototype.getObjFlexField = R_getObjFlexField
receiptObject.prototype.getNumOfFlexField = R_getNumOfFlexField;
receiptObject.prototype.calcReimbursAmount = R_calcReimbursAmount;
receiptObject.prototype.getExpTypeName = R_getExpTypeName;
receiptObject.prototype.getItemizeId = R_getItemizeId;
receiptObject.prototype.setItemizeId = R_setItemizeId;
receiptObject.prototype.isProjectEnabled = R_isProjectEnabled;

//
// copy all attributes of a receipt object
//
function R_copy(receipt) {
	var tempFFObj;
	this.date = receipt.date;
	this.occurs = receipt.occurs;
	this.setExpensedAmount(receipt.getExpensedAmount());
	this.setReceiptAmount(receipt.getReceiptAmount());
	this.reimbursAmount = receipt.getReimbursAmount();
	this.isMissing = receipt.isMissing;
	this.setReceiptCurr(receipt.getReceiptCurr());
	this.setExchRate(receipt.getExchRate());
	this.objExpenseType = receipt.getObjExpenseType();
	this.justification = receipt.justification;
	this.cCardTrxnId = receipt.cCardTrxnId;
	this.merchant = receipt.merchant;
	this.itemizeId = receipt.itemizeId;
	this.expenseGroup = receipt.expenseGroup;
	this.category = receipt.category;
	this.projectNumber = receipt.projectNumber;
	this.taskNumber = receipt.taskNumber;
	// this.flexFields has been created while setting expense type
	if (receipt.flexFields){
	  for (var i=0; i<receipt.flexFields.length; i++) {
			if (receipt.flexFields[i] != null) {
				tempFFObj = new flexFieldValues(receipt.flexFields[i].mGetName(),receipt.flexFields[i].mGetValue());
				this.addFlexFields(tempFFObj);
			}
	  }

	}
	else this.flexFields = null;
	if (this.taxInfo == null) this.taxInfo = new Tax();
		this.taxInfo.taxCodeID = receipt.taxInfo.taxCodeID;
		this.taxInfo.amountIncludesTax = receipt.taxInfo.amountIncludesTax;
		this.taxInfo.docNum = receipt.taxInfo.docNum;
		this.taxInfo.merchRef = receipt.taxInfo.merchRef;
		this.taxInfo.taxRegNum = receipt.taxInfo.taxRegNum;
		this.taxInfo.taxPayerID = receipt.taxInfo.taxPayerID;
		this.taxInfo.supCountry = receipt.taxInfo.supCountry;
		this.taxInfo.taxCode = receipt.taxInfo.taxCode;
		this.taxInfo.taxID = receipt.taxInfo.taxID;
		this.taxInfo.overrideFlag = receipt.taxInfo.overrideFlag;
}


function R_setExpensedAmount(expensedAmount){
        this.expensedAmount = expensedAmount;
	this.reimbursAmount = this.calcReimbursAmount();
}

function R_getExpensedAmount(){        
	return this.expensedAmount;
}

function R_setReceiptAmount(receiptAmount){        
	this.receiptAmount = receiptAmount;
}

function R_getExpTypeName(){        
	if (this.getObjExpenseType()) return this.getObjExpenseType().mGetstrName();
	else return "";
}

function R_setProjectNumber(projectNumber){        
	this.projectNumber = projectNumber;
}

function R_setTaskNumber(taskNumber){        
	this.taskNumber = taskNumber;
}

function R_getNAProjectNumber(){    
	if (this.projectNumber){ return this.projectNumber}
	else if (this.getObjExpenseType()){
	      if (this.getObjExpenseType().strExpenditureType == ''){
		  return top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');
	      }
	      else return '';
	     } 
	     else return '';
}

function R_getProjectNumber(){        
	return this.projectNumber;
}

function R_getNATaskNumber(){       
	if (this.taskNumber){ return this.taskNumber}
	else if (this.getObjExpenseType()){
	      if (this.getObjExpenseType().strExpenditureType == ''){
		  return top.g_objMessages.mstrGetMesg('AP_WEB_PROJECT_NA');
	      }
	      else return '';
	     }
	     else return ''; 
}

function R_getTaskNumber(){        
	return this.taskNumber;
}

function R_isProjectEnabled(){
	if (this.getObjExpenseType() != null){
	  if (this.getObjExpenseType().strExpenditureType != "") return true;
	  else return false;
	}
	else return true;
}

function R_getReceiptAmount(){        
	return this.receiptAmount;
}

function R_getDailyRate(){
	if (this.occurs >0){
	return(this.getExpensedAmount()/this.occurs);
	}
	else return 0;
}

function R_setExchRate(exchRate){        
	this.exchRate = exchRate;
	this.reimbursAmount = this.calcReimbursAmount();
}

function R_getExchRate(){        
	return this.exchRate;
}

function R_getReimbursAmount(){      
   	return(this.reimbursAmount);
}

function R_setReimbursAmount(value){        
	this.reimbursAmount = value;
}

function R_setReceiptCurr(receiptCurr){        
	this.receiptCurr = receiptCurr;
}

function R_getReceiptCurr(){        
	return(this.receiptCurr);
}

function R_setDate(date){       
	this.date = date;
}

function R_getDate(){       
	return this.date;
}

function R_setOccurs(occurs){        
	this.occurs = occurs;
}

function R_getOccurs(){        
	return this.occurs;
}

function R_setCurrency(receiptCurr){    
	this.receiptCurr = receiptCurr;
}

function R_getCurrency(){    
	return this.receiptCurr;
}

function R_setExpenseType(objE){
        this.objExpenseType = objE;
}

function R_addFlexFields(objFlexField){
   this.flexFields[this.flexFields.length] = objFlexField;
}

function R_setFlexArrValue(i,val){
	if (this.flexFields[i])
        this.flexFields[i].value = val;
}
receiptObject.prototype.setFlexArrValue = R_setFlexArrValue;

function R_getFlexArrValue(i){
	if (this.flexFields[i])
          return this.flexFields[i].value;
	else
	  return "";
}
receiptObject.prototype.getFlexArrValue = R_getFlexArrValue;


function R_setFlexArrName(i,val){
        this.flexFields[i].name = val;
}
receiptObject.prototype.setFlexArrName = R_setFlexArrName;

function R_getFlexArrName(i){
        return this.flexFields[i].name;
}
receiptObject.prototype.getFlexArrName = R_getFlexArrName;


function R_setFlexArrElem(i,obj){
        this.flexFields[i] = obj;
}
receiptObject.prototype.setFlexArrElem = R_setFlexArrElem;

function R_getFlexArrElem(i){
        return this.flexFields[i];
}
receiptObject.prototype.getFlexArrElem = R_getFlexArrElem;

function R_getObjExpenseType()
{ 
       return this.ObjExpenseType;
}

function R_getObjExpenseType(){        
	return this.objExpenseType;
}

function R_getCategory(){        
	return this.category;
}

function R_setGroup(group){        
	this.expenseGroup = group;
}

function R_getGroup(){        
	return this.expenseGroup;
}

function R_getIsMissing(){        
	return ((this.isMissing == 'Y') ? true : false);
}

function R_setIsMissing(isMissing){        
	this.isMissing = ((isMissing) ? 'Y' : 'N');
}

function R_getJustification(){
	return this.justification;
}

function R_setJustification(strJustif){
	this.justification = strJustif;
}

function R_getcCardTrxnId(){	
	return this.cCardTrxnId;
}

function R_addTax(docNum,merchRef, regNum,taxPayerID,supCountry,taxCode,taxID,overrideFlag,amountIncludesTax){
	this.taxInfo = new Tax(docNum, merchRef, regNum,taxPayerID,supCountry,taxCode,taxID,overrideFlag,amountIncludesTax);
}


function R_setDocNum(docNum){	
	this.taxInfo.docNum = docNum;
}

function R_getDocNum(){	
	return this.taxInfo.docNum;
}

function R_setMerchant(strMerchant){	
	this.merchant = strMerchant;
}

function R_getMerchant(){	
	return this.merchant;
}

function R_setMerchRef(merchRef){	
	this.taxInfo.merchRef = merchRef;
}

function R_getMerchRef(){	
	return this.taxInfo.merchRef;
}

function R_setTaxRegNum(taxRegNum){	
	this.taxInfo.taxRegNum = taxRegNum;
}

function R_getTaxRegNum(){	
	return this.taxInfo.taxRegNum;
}

function R_setTaxPayerID(taxPayerID){	
	this.taxInfo.taxPayerID = taxPayerID;
}

function R_getTaxPayerID(){	
	return this.taxInfo.taxPayerID;
}

function R_setSupCountry(supCountry){	
	this.taxInfo.supCountry = supCountry;
}

function R_getSupCountry(){	
	return this.taxInfo.supCountry;
}

function R_setTaxCode(taxCode){	
	this.taxInfo.taxCode = taxCode;
}

function R_getTaxCodeID(){
	return this.taxInfo.taxID;
}

function R_getTaxCode(){	
	return this.taxInfo.taxCode;
}

function R_setTaxID(taxID){	
	this.taxInfo.taxID = taxID;
}
function R_getTaxID(){		
	return this.taxInfo.taxID;
}

function R_setOverrideFlag(overrideFlag){	
	this.taxInfo.overrideFlag = overrideFlag;
}

function R_getOverrideFlag(){
	if ((!this.taxInfo.overrideFlag) || (this.taxInfo.overrideFlag == null)) this.taxInfo.overrideFlag = 'N';
	else this.taxInfo.overrideFlag = 'Y';
	return this.taxInfo.overrideFlag;
}

function R_setAmountIncludesTax(amountIncludesTax){
	this.taxInfo.amountIncludesTax = (amountIncludesTax ? 'Y':'N');
}

function R_getAmountIncludesTax(){
  	return ((this.taxInfo.amountIncludesTax == 'Y') ? true:false);
}

function R_setItemizeId(ItemizeId){
	this.itemizeId = ItemizeId;
}

function R_getItemizeId(){
  	return (this.itemizeId);
}

function R_setFlexValues(strName,strValue){
	var objFlexField = this.getObjFlexField(strName);
	if (objFlexField)
	objFlexField.mSetValue(strValue);
}

function R_getObjFlexField(strName){
	for (var i=0;i<this.getNumOfFlexField();i++){
	  if (this.flexFields[i].mGetName() == strName)
	  return this.flexFields[i];
	}
	return null;
}

function R_getNumOfFlexField(){
	if (this.flexFields)
	return this.flexFields.length;
	else return 0;
}


function R_calcReimbursAmount() {
   var ind = top.fGetCurrencyIndex(fGetObjHeader().getReimbursCurr());
   var l_reimbcurr_precision =0;
   if (ind >= 0 ) l_reimbcurr_precision = top.g_arrCurrency[ind].precision;
   if (g_objProfileOptions.mvGetProfileOption('DISPLAY_INVERSE_RATE')=='Y')
     return fRound( this.expensedAmount / this.exchRate, l_reimbcurr_precision);
   else
     return fRound( fMultiply(this.expensedAmount, this.exchRate), l_reimbcurr_precision);
}

/*===========================================================================*
 *  FlexFieldValues Class                                                    *
 *===========================================================================*/

function flexFieldValues(name,value){
       this.name = name + "";
       this.value = value;
}

flexFieldValues.prototype.mGetName = mGetName;
flexFieldValues.prototype.mGetValue = mGetValue;
flexFieldValues.prototype.mSetName = mSetName;
flexFieldValues.prototype.mSetValue = mSetValue;


function mGetName(){	
	return this.name;
}

function mGetValue(){	
	return this.value;
}

function mSetName(name){	
	this.name = name;
}

function mSetValue(value){	
	this.value = value;
}


/*===========================================================================*
 *  Env Class - Store Misc Environment variables                             *
 *===========================================================================*/

function envObject(bThirdPartyEnabled, bEmployeeNameReq, bCostCenterReq, bDescFlexEnabled, bTaxEnabled){
	this.bThirdPartyEnabled = bThirdPartyEnabled;
	this.bEmployeeNameReq = bEmployeeNameReq;
	this.bCostCenterReq = bCostCenterReq; 
	this.bDescFlexEnabled = bDescFlexEnabled;
	this.bTaxEnabled = bTaxEnabled;
}

envObject.prototype.mGetbThirdPartyEnabled = mGetbThirdPartyEnabled;
envObject.prototype.mGetbEmployeeNameRequired = mGetbEmployeeNameRequired;
envObject.prototype.mGetbCostCenterRequired = mGetbCostCenterRequired;
envObject.prototype.mGetbDescFlexEnabled = mGetbDescFlexEnabled;
envObject.prototype.mGetbTaxEnabled = mGetbTaxEnabled;
envObject.prototype.mSetThirdPartyEnabled = mSetThirdPartyEnabled;
envObject.prototype.mSetEmployeeNameRequired = mSetEmployeeNameRequired;
envObject.prototype.mSetCostCenterRequired = mSetCostCenterRequired;
envObject.prototype.mSetDescFlexEnabled = mSetDescFlexEnabled;
envObject.prototype.mSetTaxEnabled = mSetTaxEnabled;

function mGetbThirdPartyEnabled(){	
	return this.bThirdPartyEnabled;
}

function mGetbEmployeeNameRequired(){	
	return this.bEmployeeNameReq;
}

function mGetbCostCenterRequired(){	
	return this.bCostCenterReq;
}

function mGetbDescFlexEnabled(){	
	return this.bDescFlexEnabled;
}

function mGetbTaxEnabled(){	
	return this.bTaxEnabled;
}

function mSetThirdPartyEnabled(bThirdPartyEnabled){	
	this.bThirdPartyEnabled = bThirdPartyEnabled;
}

function mSetEmployeeNameRequired(bEmployeeNameReq){	
	this.bEmployeeNameReq = bEmployeeNameReq;
}

function mSetCostCenterRequired(bCostCenterReq){	
	this.bCostCenterReq = bCostCenterReq;
}

function mSetDescFlexEnabled(bDescFlexEnabled){	
	this.bDescFlexEnabled = bDescFlexEnabled;
}

function mSetTaxEnabled(bTaxEnabled){	
	this.bTaxEnabled = bTaxEnabled;
}

/*===========================================================================*
 *  ExpenseReport Class                                                      *
 *===========================================================================*/

function expenseReportObject(){
        this.header = null;
        this.oopReceipts = new Array();
        this.cCardReceipts = new Array();  
        this.creditCardProg = null; 
 }

expenseReportObject.prototype.getCreditCardProg = ER_getCreditCardProg;
expenseReportObject.prototype.setCreditCardProg = ER_setCreditCardProg;
expenseReportObject.prototype.deleteReceipts = ER_deleteReceipts
expenseReportObject.prototype.addCCardReceipt = ER_addCCardReceipt;
expenseReportObject.prototype.addReceipt = ER_addReceipt;
expenseReportObject.prototype.getReceiptCurrencyName = ER_getReceiptCurrencyName;
expenseReportObject.prototype.getNthReceipt = ER_getNthReceipt;
expenseReportObject.prototype.getTotalReceipts = ER_getTotalReceipts;
expenseReportObject.prototype.calculateTotal = ER_calculateTotal;
expenseReportObject.prototype.getNthLine = ER_getNthLine;
expenseReportObject.prototype.getHighestItemizeId = ER_getHighestItemizeId;

function ER_getCreditCardProg() { 
	return this.creditCardProg;
}

function ER_setCreditCardProg(sCreditCardProg) { 
	this.creditCardProg = sCreditCardProg;
}


function ER_deleteReceipts(receiptType)
{   if (receiptType == 'OOP')
        this.oopReceipts.length = 0;
    else
        this.cCardReceipts.length = 0;
}


function ER_addReceipt(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
        this.oopReceipts[this.oopReceipts.length] = new receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber);
        }


function ER_addCCardReceipt(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber){
        this.cCardReceipts[this.cCardReceipts.length] = new receiptObject(date,occurs,expensedAmount,receiptAmount,isMissing,receiptCurr,exchRate,objExpenseType,justification,cCardTrxnId,merchant,itemizeId,expenseGroup,category,projectNumber,taskNumber);
        }


function ER_getReceiptCurrencyName(iIndex){
   if ((iIndex>=0) && (iIndex<this.currencies.length))
   return(this.currencies[iIndex].shortName);
   else return null;
}

function ER_getNthLine(nIndex){
var i=0;
var r = null;
	if (top.fIsOOP()) 
	r = this.oopReceipts;
	else 
	r = this.cCardReceipts;

       for (var j=0;j< r.length;j++){
         if (r[j] != null) i++;
         if (j==nIndex) return i;
	}
       return -1;

}

function ER_getNthReceipt(n){
var i=0;
var r = null;
	if (top.fIsOOP()) 
	r = this.oopReceipts;
	else 
	r = this.cCardReceipts;

       for (var j=0;j< r.length;j++){
         if (r[j] != null) i++;
         if (i==n) return j;
	}
       return -1;
}


function ER_getTotalReceipts(strType){
var counter = 0; 
      if (strType == 'OOP'){
        for (var i=0;i< this.oopReceipts.length;i++){
          if (this.oopReceipts[i] != null){
          counter ++;
	  }
         }
       return counter;
      }
      else {
        for (var i=0;i< this.cCardReceipts.length;i++){
          if (this.cCardReceipts[i] != null){
          counter ++;
	  }
         }
       return counter;
      }
      return 0;
}

function ER_calculateTotal(strType){
var total = 0;
      if (strType == 'OOP'){
        for (var i=0;i< this.oopReceipts.length;i++){
          if (this.oopReceipts[i] != null){
          total += eval(this.oopReceipts[i].getReimbursAmount());
	  }
         }
       return total;
      }
      else {
        for (var i=0;i< this.cCardReceipts.length;i++){
          if (this.cCardReceipts[i] != null){
          total += eval(this.cCardReceipts[i].getReimbursAmount());
	  }
         }
       return total;
      }
      return 0;
}

function ER_getHighestItemizeId(){
  var tempId = 1000;
  for (var i=0;i< this.oopReceipts.length;i++){
	if (this.oopReceipts[i])
	  if (this.oopReceipts[i].itemizeId > tempId)
 	    tempId = this.oopReceipts[i].itemizeId;
  }

  for (var i=0;i< this.cCardReceipts.length;i++){
	if (this.cCardReceipts[i])
    	  if (this.cCardReceipts[i].itemizeId > tempId)
 		tempId = this.cCardReceipts[i].itemizeId;
  }
  
  return tempId;
}

/*===========================================================================*
 *  Header Class - Expense Report Header Cleass                              *
 *===========================================================================*/

function headerObject(reportHeaderID,objEmployee,approver,approverID,purpose,reimbursCurr,objExpTemplate, costCenter){
	this.reportHeaderID = reportHeaderID;
	this.objEmployee = objEmployee;;
        this.approver = approver;
	this.approverID = approverID;
        this.purpose = purpose;
	this.reimbursCurr = reimbursCurr;
	this.objExpTemplate = objExpTemplate;
	this.amtDueEmployee = 0;
	this.amtDueccCompany = 0;
	this.costCenter = costCenter;
}

headerObject.prototype.getApprover = H_getApprover;
headerObject.prototype.getApproverID = H_getApproverID;
headerObject.prototype.setApproverID = H_setApproverID;
headerObject.prototype.setApprover = H_setApprover;
headerObject.prototype.setCostCenter = H_setCostCenter;
headerObject.prototype.getCostCenter = H_getCostCenter;
headerObject.prototype.setAmtDueEmployee = H_setAmtDueEmployee;
headerObject.prototype.getAmtDueEmployee = H_getAmtDueEmployee;
headerObject.prototype.setAmtDueccCompany = H_setAmtDueccCompany;
headerObject.prototype.getAmtDueccCompany = H_getAmtDueccCompany;
headerObject.prototype.getPurpose = H_getPurpose;
headerObject.prototype.setPurpose = H_setPurpose;
headerObject.prototype.setReimbursCurr = H_setReimbursCurr;
headerObject.prototype.getReimbursCurr = H_getReimbursCurr;
headerObject.prototype.setExpTemplate = H_setExpTemplate;
headerObject.prototype.getObjExpTemplate = H_getObjExpTemplate;
headerObject.prototype.getExpTemplateID = H_getExpTemplateID;
headerObject.prototype.setEmployee = H_setEmployee;
headerObject.prototype.getObjEmployee = H_getObjEmployee;

function H_setCostCenter(costCenter){
	this.costCenter = costCenter;
}

function H_getCostCenter(){ 
	return this.costCenter;
}

function H_setAmtDueEmployee(amtDueEmployee){	
	this.amtDueEmployee = amtDueEmployee;
}

function H_getAmtDueEmployee(){	
	return (this.amtDueEmployee);
}

function H_setAmtDueccCompany(amtDueccCompany){	
	this.amtDueccCompany = amtDueccCompany;
}

function H_getAmtDueccCompany(){	
	return(this.amtDueccCompany);
}

function H_getPurpose(){        
	return (this.purpose);
}

function H_setPurpose(strPurpose){        
	this.purpose = strPurpose;
}

function H_setReimbursCurr(reimbursCurr){        
	this.reimbursCurr = reimbursCurr;
}

function H_getReimbursCurr(){        
	return (this.reimbursCurr);
}

function H_getExpTemplateID(){   
	if (this.objExpTemplate)
	  return this.objExpTemplate.id;
	else
	  return "";
}

function H_setExpTemplate(objExpTemplate){	
	  this.objExpTemplate = objExpTemplate;
}

function H_getObjExpTemplate(){
	return this.objExpTemplate;
}

function H_getReimbursCurr(){        
	return this.reimbursCurr;
}


function H_setEmployee(objEmployee){	
	  this.objEmployee = objEmployee;
}

function H_getObjEmployee()
{	
	return this.objEmployee;
}

function H_getApproverID(){
	if (top.fIsNum(this.approverID,false))
	return this.approverID;
	else return "";
}

function H_getApprover(){
	return this.approver;
}

function H_setApproverID(strApproverID){
	this.approverID = strApproverID;
}

function H_setApprover(strApprover){
	this.approver = strApprover;
}


/*===========================================================================*
 *  ExpenseTemplate Class                                                    *
 *===========================================================================*/

function expenseTemplateObject(p_id, p_name,expTypeArr){
  this.id = p_id;  //ID == .value ==strExpenseTemplateID
  this.name = p_name;
  this.arrExpTypes = new Array(); 
 }
expenseTemplateObject.arrAllExpenseType = new Array();  

expenseTemplateObject.prototype.mstrGetTemplateID = getTemplateID();
expenseTemplateObject.prototype.mstrGetTemplateName = getTemplateName();
expenseTemplateObject.prototype.addExpType = addExpType;
expenseTemplateObject.prototype.getExpTypeName = getExpTypeName;
expenseTemplateObject.prototype.getExpTypeID = getExpTypeID;
expenseTemplateObject.prototype.getExpTypeObj = getExpTypeObj;
expenseTemplateObject.prototype.getJustifReq = getJustifReq; 

function getTemplateID(){  
	return this.id;
}

function getTemplateName(){  
	return this.name;
}

function addExpType(id) {
  for (var j=0;j<expenseTemplateObject.arrAllExpenseType.length;j++){
	if (expenseTemplateObject.arrAllExpenseType[j].mGetID() == id){ 
	  this.arrExpTypes[this.arrExpTypes.length] = expenseTemplateObject.arrAllExpenseType[j];
	}
  }
}

function getExpTypeName(folioType) {
  for (var i=0; i< this.arrExpTypes.length;i++) {
    if ( this.arrExpTypes[i].mGetFolioType() == folioType) {
	return this.arrExpTypes[i].mGetstrName();
	}
    }//end for
   return "";
}//end getExpTypeName


function getExpTypeID(folioType) {
  for (var i=0; i< this.arrExpTypes.length;i++) {
    if ( this.arrExpTypes[i].mGetFolioType() == folioType) {
 	return this.arrExpTypes[i].mGetID();
	}
    }//end for
  return "";
}//end getID



function getJustifReq(ID) {
  if (this.getExpTypeObj(ID)) 
	return this.getExpTypeObj(ID).mGetbJustifRequried();
  else return 'N';
}

function getExpTypeObj(ID) {
  for (var i=0; i< expenseTemplateObject.arrAllExpenseType.length; i++) {
    if (expenseTemplateObject.arrAllExpenseType[i].mGetID() == ID)
       return expenseTemplateObject.arrAllExpenseType[i];
  } //end for
  return null;
} //end getExpTypeObj


/*===========================================================================*
 *  Expense Type Class                                                       *
 *===========================================================================*/

function expenseTypeObject(ID,name,code,folioType,nRequireReceiptAmount,bJustifRequired,bCalAmtFlag,bAITFlag, strDefaultTaxCode, bUpdateDefaultTaxCode, strExpenditureType){
  this.ID = ID;
  this.name = name; 
  this.code = code; 
  this.folioType = folioType;
  this.nRequireReceiptAmount = nRequireReceiptAmount;
  this.bJustifRequired = bJustifRequired;
  this.bCalAmtFlag = bCalAmtFlag;
  this.bAITFlag = bAITFlag;
  this.strDefaultTaxCode = strDefaultTaxCode;
  this.bUpdateDefaultTaxCode = bUpdateDefaultTaxCode;
  this.strExpenditureType = strExpenditureType;
}



expenseTypeObject.prototype.mGetID = ET_mGetID;
expenseTypeObject.prototype.mGetstrName = ET_mGetstrName;
expenseTypeObject.prototype.mGetstrCode = ET_mGetstrCode;
expenseTypeObject.prototype.mGetFolioType = ET_mGetCardExpType;
expenseTypeObject.prototype.mGetRequireReceiptAmount = ET_mGetRequireReceiptAmount;
expenseTypeObject.prototype.mGetbJustifRequired = ET_mGetbJustifRequired;
expenseTypeObject.prototype.mSetID = ET_mSetID;
expenseTypeObject.prototype.mSetName = ET_mSetName;
expenseTypeObject.prototype.mSetCode = ET_mSetCode;
expenseTypeObject.prototype.mSetFolioType = ET_mSetCardExpType;
expenseTypeObject.prototype.mSetRequireReceiptAmount = ET_mSetRequireReceiptAmount;
expenseTypeObject.prototype.mSetJustifRequired = ET_mSetJustifRequired;
expenseTypeObject.prototype.mGetbCalAmtFlag = ET_mGetbCalAmtFlag;
expenseTypeObject.prototype.mGetbAITFlag = ET_mGetbAITFlag;
expenseTypeObject.prototype.mGetstrDefaultTaxCode = ET_mGetstrDefaultTaxCode;

function ET_mGetstrDefaultTaxCode(){
   return this.strDefaultTaxCode;

}

function ET_mGetbAITFlag(){
   return this.AITFlag;
}


function ET_mGetID() {  
	return this.ID;
}

function ET_mGetstrName() {  
	return this.name;
}

function ET_mGetstrCode() {  
	return this.code;
}

function ET_mGetCardExpType() {  
	return this.folioType;
}

function ET_mGetRequireReceiptAmount() {  
	return this.nRequireReceiptAmount;
}

function ET_mGetbJustifRequired() {  
	return this.bJustifRequired;
}


function ET_mSetID(ID) {  
	this.ID = ID;
}

function ET_mSetName(strName) {  
	this.strName =strName;
}

function ET_mSetCode(strCode) {  
	this.strCode =strCode;
}

function ET_mSetCardExpType(folioType) {  
	this.folioType = folioType;
}

function ET_mSetRequireReceiptAmount(nRequireReceiptAmount) {  
	this.nRequireReceiptAmount = nRequireReceiptAmount;
}

function ET_mSetJustifRequired(bJustifRequired) {  
	this.bJustifRequired = bJustifRequired;
}

function ET_mGetbCalAmtFlag() {
  return this.bCalAmtFlag;
}


/*===========================================================================*
 *  Tax Class                                                                *
 *===========================================================================*/

// merchName has been moved to receipt object
function Tax(docNum, merchRef, taxRegNum, taxPayerID, supCountry, taxCode, taxID, overrideFlag, amountIncludesTax){
if (arguments.length > 0) {
	this.docNum = docNum;
	this.merchRef = merchRef;
	this.taxRegNum = taxRegNum;
	this.taxPayerID = taxPayerID;
	this.supCountry = supCountry;
	this.taxCode = taxCode;
	this.taxID = taxID;
	this.overrideFlag = overrideFlag;
	this.amountIncludesTax = amountIncludesTax;
  }
else{
	this.docNum = "";
	this.merchRef = "";
	this.taxRegNum = "";
	this.taxPayerID = "";
	this.supCountry = "";
	this.taxCode = "";
	this.taxID = "";
	this.overrideFlag = "";
	this.amountIncludesTax = "";
  }
}

Tax.prototype.mGetDocNum = Tax_mGetDocNum;
Tax.prototype.mGetMerchRef = Tax_mGetMerchRef;
Tax.prototype.mGetTaxRegNum = Tax_mGetTaxRegNum;
Tax.prototype.mGetTaxPayerID = Tax_mGetTaxPayerID;
Tax.prototype.mGetSupCountry = Tax_mGetSupCountry;
Tax.prototype.mGetTaxCode = Tax_mGetTaxCode;
Tax.prototype.mGetTaxID = Tax_mGetTaxID;
Tax.prototype.mGetOverrideFlag = Tax_mGetOverrideFlag;
Tax.prototype.mGetAmountIncludesTax = Tax_mGetAmountIncludesTax;
Tax.prototype.mSetDocNum = Tax_mSetDocNum;
Tax.prototype.mSetMerchRef = Tax_mSetMerchRef;
Tax.prototype.mSetTaxRegNum = Tax_mSetTaxRegNum;
Tax.prototype.mSetTaxPayerID = Tax_mSetTaxPayerID;
Tax.prototype.mSetSupCountry = Tax_mSetSupCountry;
Tax.prototype.mSetTaxCode = Tax_mSetTaxCode;
Tax.prototype.mSetTaxID = Tax_mSetTaxID;
Tax.prototype.mSetOverrideFlag = Tax_mSetOverrideFlag;
Tax.prototype.mSetAmountIncludesTax = Tax_mSetAmountIncludesTax;

function Tax_mGetDocNum(){	return this.docNum;}

function Tax_mGetMerchRef(){	return this.merchRef;}

function Tax_mGetTaxRegNum(){	
	return this.taxRegNum;
}

function Tax_mGetTaxPayerID(){	
	return this.taxPayerID;
}

function Tax_mGetSupCountry(){	
	return this.supCountry;
}

function Tax_mGetTaxCode(){	
	return this.taxCode;
}

function Tax_mGetTaxID(){	
	return this.taxID;
}

function Tax_mGetOverrideFlag(){	
	return this.overrideFlag;
}

function Tax_mGetAmountIncludesTax(){	
	return this.amountIncludesTax;
}

function Tax_mSetDocNum(docNum){	
	this.docNum = docNum;
}

function Tax_mSetMerchRef(merchRef){	
	this.merchRef = merchRef;
}

function Tax_mSetTaxRegNum(taxRegNum){	
	this.taxRegNum = taxRegNum;
}

function Tax_mSetTaxPayerID(taxPayerID){	
	this.taxPayerID = taxPayerID;
}

function Tax_mSetSupCountry(supCountry){	
	this.supCountry = supCountry;
}

function Tax_mSetTaxCode(taxCode){	
	this.taxCode = taxCode;
}

function Tax_mSetTaxID(taxID){	
	this.taxID = taxID;
}

function Tax_mSetOverrideFlag(overrideFlag){	
	this.overrideFlag = overrideFlag;
}

function Tax_mSetAmountIncludesTax(amountIncludesTax){	
	this.amountIncludesTax = amountIncludesTax;
}

/*===========================================================================*
 *  Currency Info Class                                                      *
 *===========================================================================*/

function objCurrencyInfo(currency, name, precision,
                                minimumAcctUnit,
                                euroRate, datEffective)
{
  this.currency = currency;
  this.name = name;
  this.precision = precision;
  this.minimumAcctUnit = minimumAcctUnit;
  this.euroRate = euroRate;
  this.datEffective = datEffective;

}

objCurrencyInfo.prototype.mGetCurrency = Currency_mGetCurrency;
objCurrencyInfo.prototype.mGetName = Currency_mGetName;
objCurrencyInfo.prototype.mGetPrecision = Currency_mGetPrecision;
objCurrencyInfo.prototype.mGetMinimumAcctUnit = Currency_mGetMinimumAcctUnit;
objCurrencyInfo.prototype.mGetEuroRate = Currency_mGetEuroRate;
objCurrencyInfo.prototype.mGetdatEffective = Currency_mGetdatEffective;
objCurrencyInfo.prototype.mSetCurrency = Currency_mSetCurrency;
objCurrencyInfo.prototype.mSetName = Currency_mSetName;
objCurrencyInfo.prototype.mSetPrecision = Currency_mSetPrecision;
objCurrencyInfo.prototype.mSetMinimumAcctUnit = Currency_mSetMinimumAcctUnit;
objCurrencyInfo.prototype.mSetEuroRate = Currency_mSetEuroRate;
objCurrencyInfo.prototype.mSetEffectiveDate = Currency_mSetEffectiveDate;

function Currency_mGetCurrency(){	
	return this.currency;
}

function Currency_mGetName(){	
	return this.name;
}

function Currency_mGetPrecision(){	
	return this.precision;
}

function Currency_mGetMinimumAcctUnit(){	
	return this.minimumAcctUnit;
}

function Currency_mGetEuroRate(){	
	return this.euroRate
}

function Currency_mGetdatEffective(){	
	return this.datEffective
}

function Currency_mSetCurrency(currency){	
	this.currency = currency;
}

function Currency_mSetName(name){	
	this.name = name;
}

function Currency_mSetPrecision(precision){	
	this.precision = precision;
}

function Currency_mSetMinimumAcctUnit(minimumAcctUnit){	
	this.minimumAcctUnit = minimumAcctUnit;
}

function Currency_mSetEuroRate(euroRate){	
	this.euroRate = euroRate;
}

function Currency_mSetEffectiveDate(datEffective){	
	this.datEffective = datEffective;
}

/*===========================================================================*
 *  Value Class                                                              *
 *===========================================================================*/

// This object stores info of an item in a poplist for one DFF segment 
function Value(p_strCode, p_strDesc) {
  this.strDesc = p_strDesc;
  this.strCode = p_strCode;
}

Value.prototype.mGetstrDesc = mGetstrDesc;
Value.prototype.mGetstrCode = mGetstrCode;
Value.prototype.mSetrDesc = mSetDesc;
Value.prototype.mSetCode = mSetCode;

function mGetstrDesc() {  
	return this.strDesc;
}

function mGetstrCode() {  
	return this.strCode;
}

function mSetDesc(strDesc) {  
	this.strDesc = strDesc;
}

function mSetCode(strCode) {  
	this.strCode = strCode;
}

/*===========================================================================*
 *  Values Class - Container class of Value Objects                          *
 *===========================================================================*/

function Values(p_strName) {
  this.strName = p_strName;
  this.arrValue = new Array();
}

Values.prototype.mAddValue = Values_mAddValue;
Values.prototype.miGetNumOfValues = Values_miGetNumOfValues;
Values.prototype.mobjFindValue = Values_mobjFindValue;
Values.prototype.mGetstrName = Values_mGetstrName;

function Values_mGetstrName() {
  return this.strName;
}

function Values_mAddValue(p_objValue) {
  this.arrValue[this.arrValue.length] = p_objValue;
}

function Values_miGetNumOfValues() {
  return this.arrValue.length;
}

function Values_mobjFindValue(p_strCode) {

  for(var i=0; i<this.arrValue.length; i++) {
    if (this.arrValue[i].mGetstrCode() == p_strCode)
      return this.arrValue[i].mGetstrDesc();
  }
  return "";
}

/*===========================================================================*
 *  CollectionOfValues Class                                                 *
 *===========================================================================*/

function CollectionOfValues() {
  this.arrValues = new Array();
}

CollectionOfValues.prototype.mobjAddValues = mobjAddValues;
CollectionOfValues.prototype.mobjGetValues = mobjGetValues;

function mobjAddValues(p_objValues) {
  this.arrValues[p_objValues.mGetstrName()] = p_objValues;
  return p_objValues;
}

function mobjGetValues(p_strName) {
  var undefined;

  var objValues = this.arrValues[p_strName];
  if (objValues == undefined)
    return null;
  return objValues;
}

/*===========================================================================*
 *  Flexfield Class                                                          *
 *===========================================================================*/

function FlexField(p_strPrompt, p_bIsRequired, p_strDefaultValue, p_iObjectType,
                 p_iTextBoxLength, p_objPoplistValues, p_bIsNumericOnly, 
                 p_bIsUpperCaseOnly) {
  
  this.strPrompt = p_strPrompt;
  this.bIsRequired = p_bIsRequired;
  this.strDefaultValue = p_strDefaultValue;
  this.iObjectType = p_iObjectType;
  this.iTextBoxLength = p_iTextBoxLength;
  this.objPoplistValues = p_objPoplistValues;
  this.bIsNumericOnly = p_bIsNumericOnly;
  this.bIsUpperCaseOnly = p_bIsUpperCaseOnly;
}

FlexField.prototype.mGetstrPrompt = mGetstrPrompt;
FlexField.prototype.mGetbIsRequired = mGetbIsRequired;
FlexField.prototype.mGetstrDefaultValue = mGetstrDefaultValue;
FlexField.prototype.mGetiObjectType = mGetiObjectType;
FlexField.prototype.mGetiTextBoxLength = mGetiTextBoxLength;
FlexField.prototype.mGetobjPoplistValues = mGetobjPoplistValues;
FlexField.prototype.mGetiColumn = mGetiColumn;
FlexField.prototype.mGetbIsNumericOnly = mGetbIsNumericOnly;
FlexField.prototype.mGetbIsUpperCaseOnly = mGetbIsUpperCaseOnly;
FlexField.prototype.mValidateFlexField = mValidateFlexField;
FlexField.prototype.mSetPrompt = mSetPrompt;
FlexField.prototype.mSetIsRequired = mSetIsRequired;
FlexField.prototype.mSetDefaultValue = mSetDefaultValue;
FlexField.prototype.mSetObjectType = mSetObjectType;
FlexField.prototype.mSetTextBoxLength = mSetTextBoxLength;
FlexField.prototype.mSetColumn = mSetColumn;
FlexField.prototype.mSetIsNumericOnly = mSetIsNumericOnly;
FlexField.prototype.mSetIsUpperCaseOnly = mSetIsUpperCaseOnly;


function mGetstrPrompt() {  
	return this.strPrompt;
}

function mGetbIsRequired() {  
	return this.bIsRequired;
}

function mGetstrDefaultValue() {  
	return this.strDefaultValue;
}

function mGetiObjectType() {  
	return this.iObjectType;
}

function mGetiTextBoxLength() {  
	return this.iTextBoxLength;
}

function mGetobjPoplistValues() {  
	return this.objPoplistValues;
}

function mGetiColumn() {  
	return this.iColumn;
}

function mGetbIsNumericOnly() {  
	return this.bIsNumericOnly;
}

function mGetbIsUpperCaseOnly() {  
	return this.bIsUpperCaseOnly;
}

function mSetPrompt(strPrompt) {  
	this.strPrompt = strPrompt;
}

function mSetIsRequired(bIsRequired) {  
	this.bIsRequired = bIsRequired;
}

function mSetDefaultValue(strDefaultValue) {  
	this.strDefaultValue = strDefaultValue;
}

function mSetObjectType(iObjectType) {  
	this.iObjectType = iObjectType;
}

function mSetTextBoxLength(iTextBoxLength) {  
	this.iTextBoxLength = iTextBoxLength;
}

function mSetColumn(iColumn) {  
	this.iColumn = iColumn;
}

function mSetIsNumericOnly(bIsNumericOnly) {  
	this.bIsNumericOnly = bIsNumericOnly;
}

function mSetIsUpperCaseOnly(bIsUpperCaseOnly) {  
	this.bIsUpperCaseOnly = bIsUpperCaseOnly;
}

function mValidateFlexField(p_strValue) {

  if ((this.mGetbIsRequired()) && (p_strValue == "")) {
    var strErrorMsg1 = top.g_objMessages.mstrGetMesg("AP_WEB_FLEX_FIELD_REQUIRED");
    strErrorMsg1 = strErrorMsg1.replace("&FLEXFIELD_NAME",this.mGetstrPrompt());
    strErrorMsg1 = strErrorMsg1.replace("&FLEXFIELD_NAME",this.mGetstrPrompt());
  }
  if ((this.mGetbIsNumericOnly()) && (!fIsNumericOnly(p_strValue))) {
    var strErrorMsg2 =  top.g_objMessages.mstrGetMesg("AP_WEB_CUSTOM_FIELD_NUMONLY");
    strErrorMsg2 = strErrorMsg2.replace("&CUSTFIELD", this.mGetstrPrompt());
  }
  if ((this.mGetbIsUpperCaseOnly()) && (!fIsAlphaUpperCaseOnly(p_strValue))) {
    var strErrorMsg3 =  top.g_objMessages.mstrGetMesg("AP_WEB_CUSTOM_FIELD_ALPHAUPPER");
    strErrorMsg3 = strErrorMsg3.replace("&CUSTFIELD", this.mGetstrPrompt());
  }

  var strErrorMsg = "";
  if (strErrorMsg1) strErrorMsg += strErrorMsg1+"<BR>";
  if (strErrorMsg2) strErrorMsg += strErrorMsg2+"<BR>";
  if (strErrorMsg3) strErrorMsg += strErrorMsg3+"<BR>";
  return strErrorMsg;

}

/*===========================================================================*
 *  Context Class                                                            *
 *===========================================================================*/

function Context(p_strContextName) {
  this.strContextName = p_strContextName;
  this.arrFlexFields = new Array();
}

Context.prototype.mGetstrContextName = mGetstrContextName;
Context.prototype.mGetobjFlexFields = mGetobjFlexFields;
Context.prototype.mGetstrWidgetName = mGetstrWidgetName;
Context.prototype.mGetstrSegStyle = mGetstrSegStyle;
Context.prototype.mCopyFlexField = mCopyFlexField;
Context.prototype.mAddFlexField = mAddFlexField;
Context.prototype.mobjGetFlexField = mobjGetFlexField;
Context.prototype.miGetNumOfFlexField = miGetNumOfFlexField;


function mAddFlexField(p_objFlexField) {
  this.arrFlexFields[this.arrFlexFields.length] = p_objFlexField;
}

function mobjGetFlexField(p_strPrompt) {
  for(var i=0; i<this.arrFlexFields.length; i++)
    if (this.arrFlexFields[i].strPrompt == p_strPrompt)
      return this.arrFlexFields[i];
  return null;
}

function mCopyFlexField(p_arrFlexFields) {
  for(var i=0; i<p_arrFlexFields.length; i++)
    this.mAddFlexField(p_arrFlexFields[i]);
}

function miGetNumOfFlexField() {
  return this.arrFlexFields.length;
}

function mGetstrContextName() {
  return this.strContextName;
}

function mGetobjFlexFields() {
  return this.arrFlexFields;
}

function mGetstrWidgetName(strSegName,iBaseIndex){
  for (var i=0;i<this.miGetNumOfFlexField();i++){
	if (this.arrFlexFields[i].mGetstrPrompt() == strSegName){
	  var strTemp = this.arrFlexFields[i].mGetstrPrompt();
	  strTemp = top.ReplaceInvalidChars(strTemp);
	  return strTemp;
	  }
  }
  return "";
}

function mGetstrSegStyle(strSegName){
  for (var i=0;i<this.objFlexFields.miGetNumOfFlexField();i++){
	if (this.arrFlexFields[i].mGetstrPrompt() == strSegName)
	  return this.arrFlexFields[i].mGetiObjectType();
  }
  return "";
}

/*===========================================================================*
 *  Contexts Class                                                           *
 *===========================================================================*/

function Contexts() {
  this.arrContext = new Array();
}

Contexts.prototype.mobjAddContext = mobjAddContext;
Contexts.prototype.mobjGetContext = mobjGetContext;
Contexts.prototype.mobjGetContextWithDefault = mobjGetContextWithDefault;


function mobjAddContext(p_objContext) {
  this.arrContext[p_objContext.mGetstrContextName()] = p_objContext;
  return p_objContext;
}

function mobjGetContext(p_strName) {
  var undefined;
  var objContext = this.arrContext[p_strName];
  if (objContext == undefined)
    return null;
  return objContext;
}

function mobjGetContextWithDefault(p_strName) {
  var objContext = this.mobjGetContext(p_strName);

  if (objContext == null)  
    objContext = this.mobjGetContext(C_strGLOBALCONTEXTNAME);
  return objContext;
}

/*===========================================================================*
 *  Employee Class                                                           *
 *===========================================================================*/

function empObject(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum){
	this.strEmployeeName = strEmployeeName;
	this.strEmployeeID = strEmployeeID;
	this.strCostCenter = strCostCenter;
	this.bProjectEnabled = bProjectEnabled;
	this.strEmployeeNum = strEmployeeNum;
}


empObject.prototype.mGetstrEmployeeName = mGetstrEmployeeName;
empObject.prototype.mGetstrEmployeeID = mGetstrEmployeeID;
empObject.prototype.mGetstrCostCenter = mGetstrCostCenter;
empObject.prototype.mGetbProjectEnabled = mGetbProjectEnabled;
empObject.prototype.mGetstrEmployeeNum = mGetstrEmployeeNum;
empObject.prototype.mSetEmployeeName = mSetEmployeeName;
empObject.prototype.mSetEmployeeID = mSetEmployeeID;
empObject.prototype.mSetCostCenter = mSetCostCenter;
empObject.prototype.mSetProjectEnabled = mSetProjectEnabled;
empObject.prototype.mSetEmployeeNum = mSetEmployeeNum;
empObject.prototype.mGetstrCostCenter = mGetstrCostCenter;
empObject.prototype.mGetbIsProjectEnabled = mGetbIsProjectEnabled;
empObject.prototype.mGetEmployeeName = mGetEmployeeName;
empObject.prototype.mGetEmployeeNum = mGetEmployeeNum;
	
function mGetstrEmployeeName(){	
	return this.strEmployeeName;
}

function mGetstrEmployeeID(){	
	return this.strEmployeeID;
}

function mGetstrCostCenter(){	
	return this.strCostCenter;
}

function mGetbProjectEnabled(){	
	return this.bProjectEnabled;
}

function mGetstrEmployeeNum(){	
	return this.strEmployeeNum;
}

function mSetEmployeeName(strEmployeeName){	
	this.strEmployeeName =  strEmployeeName;
}

function mSetEmployeeID(strEmployeeID){	
	this.strEmployeeID = strEmployeeID;
}

function mSetCostCenter(strCostCenter){	
	this.strCostCenter = strCostCenter;
}

function mSetProjectEnabled(bProjectEnabled){	
	this.bProjectEnabled = bProjectEnabled;
}

function mSetEmployeeNum(strEmployeeNum){  
	this.strEmployeeNum = strEmployeeNum;
}

function mGetstrCostCenter(){	
	return this.strCostCenter;
}

function mGetbIsProjectEnabled(){	
	return this.bProjectEnabled;
}

function mGetEmployeeName(){	
	return this.strEmployeeName;	
}

function mGetEmployeeNum(){	
	return this.strEmployeeNum;
}

/*===========================================================================*
 *  Employees Class                                                          *
 *===========================================================================*/

function Employees(){
  this.arrEmployee = new Array();
}
new Employees();

Employees.prototype.mobjGetEmployee = mobjGetEmployee;
Employees.prototype.mnGetNumOfEmployees = mnGetNumOfEmployees;
Employees.prototype.mobjAddEmployee = mobjAddEmployee;

function mnGetNumOfEmployees() {
  return this.arrEmployee.length;
}


function mobjGetEmployee(strID,strName) {
  if (strID){
    for (var i=0;i<this.mnGetNumOfEmployees();i++){
	if (this.arrEmployee[i].mGetstrEmployeeID() == strID)
	  return this.arrEmployee[i];
    }
  }
  else if (strName){
    for (var i=0;i<this.mnGetNumOfEmployees();i++){
	if (this.arrEmployee[i].mGetstrEmployeeName() == strName)
	  return this.arrEmployee[i];
    }
  }
  return null;
}

function mobjAddEmployee(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum){
	this.arrEmployee[this.arrEmployee.length] = new empObject(strEmployeeName, strEmployeeID, strCostCenter, bProjectEnabled, strEmployeeNum);
}



























