/* $Header: czCntOpt.js 115.18 2001/06/14 15:33:49 pkm ship      $ */
/*===========================================================================+
 |      Copyright (c) 1999 Oracle Corporation, Redwood Shores, CA, USA       |
 |                         All rights reserved.                              |
 +===========================================================================+
 | HISTORY                                                                   |
 |                                                                           |
 |       xxxx-xx-99  K MacClay, CK Jeyaprakash  Created.                     |
 |                                                                           |
 +===========================================================================*/

function CountedOptionItem ()
{	
  this.parentConstructor = LogicItem;
  this.parentConstructor ();
  this.type = 'CountedOptionItem';
}

function CountedOptionItem_getColumnDescriptors (bUseImg, bNoIcon)
{
  if (! this.columnDescriptors) {
    this.columnDescriptors = new Array ();
    this.colDescHash = new Array ();
  }
  if (this.columnDescriptors.length == 0) {  
    var index = 0;
    // params - name, type, index, ht, wd, pdgLt, pdgTp, valign, halign, defvis, spring;
    if (! bNoIcon) {
      this.columnDescriptors[index] = new ColumnDescriptor ('state', 'icon', index, 16, 20, 2, 0, 'top', 'left',true, false);
      index++;
    }
    this.columnDescriptors[index] = new ColumnDescriptor ('count', 'inputtext', index, 22, 50, 1, 0, 'top', 'left', true, false);
    index++;
    if (bUseImg) {
      //use image for description;
      this.columnDescriptors[index] = new ColumnDescriptor ('descImg', 'descImg', index, 22, 50, 0, 0, 'top', 'left', true, true);

    } else {
      //display description;
      this.columnDescriptors[index] = new ColumnDescriptor ('text', 'label', index, 22, 50, 1, 2, 'top', 'left', true, true);
    }
    index++;
    if (this.priceVisible) {
      this.columnDescriptors[index] = new ColumnDescriptor ('price', 'label', index, 22, 20, 1, 2, 'top', 'right', true, false);
    }
    this.hashAllColumnDescriptors(this.columnDescriptors);
  }
  return (this.columnDescriptors);
}

function CountedOptionItem_mouseupCallback (e, eSrc)
{
  if (this.locked) {
    if (eSrc.type == 'inputtext')
      return false;
    else
      return true;
  }
  self.cntObj = this;

  // grow the height of the item to allow for the rendering of;
  // the input form element of inputtext for NS.;
  if (eSrc.type == 'inputtext') {
    if (ns4 && (this.height < 28)) {
      this.changeHeight();
      this.setHeight (28);
      this.eventManager.addListener ('docMouseDown', this);
      this.eventManager.addListener ('docEnterKeyUp', this);
    }	
  }
  else {
    if ((this.modelItem.getState() != 'utrue') && (this.modelItem.getState() != 'ltrue')) {
      this.modelItem.setState('utrue');
    }else{
      this.modelItem.setState('unknown');
    }
    this.notifyListeners ('actionMouseUp', this.modelItem, this);
  }
  return true;
}

function CountedOptionItem_docEnterKeyUp (e)
{
  if (this.oldHeight) {
    this.setHeight (this.oldHeight);
    this.resetHeight();
  }
  this.eventManager.removeListener ('docEnterKeyUp', this);
  return true;
}

function CountedOptionItem_docMouseDown (e)
{
  var eSrc = (ns4) ? e.target : e.srcElement;
  if (eSrc) {
    var id = eSrc.id;
    if (! id) {
      if (eSrc.name)
        id = eSrc.name;
    }
    if (id == "editBox")
      return (true);
    else {
      //Reverse the old height for NS;
      if (this.oldHeight) {
        this.setHeight (this.oldHeight);
        this.resetHeight();
        this.eventManager.removeListener ('docMouseDown', this);
      }
    }
  }
  else {
    //Reverse the old height for NS;
    if (this.oldHeight) {
      this.setHeight (this.oldHeight);
      this.oldHeight = null;
      this.eventManager.removeListener ('docMouseDown', this);
    }
  }
  return (true);
}

function CountedOptionItem_resetHeight()
{
//  if ( this.listBorderClipped ){
  if (this.bLastCell) {
    this.setTop (this.oldTop);
    this.prevItem.setHeight (this.prevItem.oldHeight);
    this.prevItem.oldHeight = null;
  } else {
    this.nextItem.setTop (this.nextItem.oldTop);
    this.nextItem.setHeight (this.nextItem.oldHeight);
    this.nextItem.oldHeight = null;
    this.nextItem.oldTop = null;
    this.oldHeight = null;
  }
}

function CountedOptionItem_changeHeight(){
  this.oldHeight = this.height;
  this.oldTop = this.top;
  heightDiff = 28 - this.oldHeight;
//  if (this.listBorderClipped){
  if (this.bLastCell) {
    this.prevItem.oldHeight = this.prevItem.height;
    this.prevItem.setHeight(this.prevItem.height - heightDiff );
    this.setTop(this.top - heightDiff );
  } else {
    this.nextItem.oldTop = this.nextItem.top;
    this.nextItem.oldHeight = this.nextItem.height;
    this.nextItem.setTop (this.nextItem.top + heightDiff);
    this.nextItem.setHeight (this.nextItem.height - heightDiff);
  }
}

function CountedOptionItem_getMaxNumFocusCells()
{
  return 2;
}

//Extend from ListItem
HTMLHelper.importPrototypes(CountedOptionItem, LogicItem);

//Private Methods
CountedOptionItem.prototype.resetHeight = CountedOptionItem_resetHeight;
CountedOptionItem.prototype.changeHeight = CountedOptionItem_changeHeight;

// LogicItem overrides
CountedOptionItem.prototype.getColumnDescriptors = CountedOptionItem_getColumnDescriptors;
CountedOptionItem.prototype.getMaxNumFocusCells = CountedOptionItem_getMaxNumFocusCells;

//Event handlers
CountedOptionItem.prototype.mouseupCallback = CountedOptionItem_mouseupCallback;
CountedOptionItem.prototype.docMouseDown = CountedOptionItem_docMouseDown;
CountedOptionItem.prototype.docEnterKeyUp = CountedOptionItem_docEnterKeyUp;
