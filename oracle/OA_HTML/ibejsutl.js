////////////////////////////////////////////////////////////////////////////////
// File Contains: validation utility functions
// 
// DEPENDENCY:
////////////////////////////////////////////////////////////////////////////////
/* $Header: ibejsutl.js 115.0 2000/03/01 13:35:31 pkm ship  $ */

function isWhiteSpace(c)
{
    if (c > ' ')
	return false;
    else
	return true;
}

function isDigit(c)
{
    return ((c >= '0') && (c <= '9'));
}

function trim(str)
{
    if (str == null)
	return str;

    var strLen = str.length;
    if (strLen ==  0)
	return str;

    var startIndex = -1;
    var endIndex;
    var newStr;

    for (var i = 0; i < strLen; i++) {
	if (! isWhiteSpace(str.charAt(i))) {
	    startIndex = i;
	    break;
	}
    }

    if (startIndex == -1)
	return "";

    for (var i = strLen - 1; i >= startIndex; i--) {
	if (! isWhiteSpace(str.charAt(i))) {
	    endIndex = i;
	    break;
	}
    }	

    newStr = str.substring(startIndex, endIndex + 1);

    return newStr;
}

function isEmpty(str)
{
    if (str == null)
	return true;

    var strLen = str.length;
    if (strLen == 0)
	return true;

    for (var i = 0; i < strLen; i++) {
	if (! isWhiteSpace(str.charAt(i))) 
	    return false;	
    }

    return true;
}

function isInteger(str)
{
    var newStr = trim(str);

    if (isEmpty(newStr))
	return false;

    var strLen = newStr.length;
    var startIndex = 0;
    if ((newStr.charAt(0) == '+') || (newStr.charAt(0) == '-')) {
	for (startIndex = 1; startIndex < strLen; startIndex++) 
		if (! isWhiteSpace(newStr.charAt(startIndex)))
			break;
	if (startIndex >= strLen)
	    return false;
    }

    for (var i = startIndex; i < strLen; i++) {
	if (! isDigit(newStr.charAt(i)))
	    return false;
    }

    return true;
}

function isNumber(str)
{
	var newStr = trim(str);

	if (isEmpty(newStr)) 
		return false; 
		
	var strLen = newStr.length; 
	var startIndex = 0; 
	if ((newStr.charAt(0) == '+') || (newStr.charAt(0) == '-')) { 
		for (startIndex = 1; startIndex < strLen; startIndex++) 
			if (! isWhiteSpace(newStr.charAt(startIndex))) 
				break; 
		if (startIndex >= strLen) 
			return false; 
	}

	if (newStr.charAt(startIndex) != '.') {
		var i = startIndex;
		for (i = startIndex; i < strLen; i++)
			if (! isDigit(newStr.charAt(i)))
				break;
		if (i >= strLen)
			return true;
		if (newStr.charAt(i) != '.')
			return false;
		startIndex = i + 1;
	}
	else {
		startIndex ++;
		if (startIndex >= strLen)
			return false;
	}

	for (i = startIndex; i < strLen; i++) 
		if (! isDigit(newStr.charAt(i)))
			return false;
	
	return true;
}

function isSelectedOptionEmpty(theSelect) 
{
	if ((theSelect == null)
		|| (theSelect.options == null) 
		|| (theSelect.options.length == 0))
		return true;

	if (isEmpty(theSelect.options[theSelect.selectedIndex].value))
		return true;

	return false;
}

function isRadioButtonEmpty(theForm, radioName)
{
	if ((theForm == null) || isEmpty(radioName))
		return true;

	for (var i = 0; i < theForm.elements.length; i++) 
		if ((theForm.elements[i].name == radioName)
			&& theForm.elements[i].checked) 
			return false;

	return true;
}

function isCheckBoxEmpty(theForm, checkBoxName)
{
	if ((theForm == null) || isEmpty(checkBoxName))
		return true;

	for (var i = 0; i < theForm.elements.length; i++)
		if ((theForm.elements[i].name == checkBoxName)
			&& theForm.elements[i].checked) 
			return false;

	return true;
}
