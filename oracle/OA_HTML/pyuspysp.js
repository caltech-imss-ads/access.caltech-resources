<!-- Hide from browsers which cannot handle JavaScript.
// +======================================================================+
// |                Copyright (c) 1999 Oracle Corporation                 |
// |                   Redwood Shores, California, USA                    |
// |                        All rights reserved.                          |
// +======================================================================+
// File Name   : hrmecmnw.js
// Description : This file contains all the common JavaScript routines
//               used in MEE modules.
//
// Change List:
// ------------
//
// Name        Date         Version Bug     Text
// ----------- ----------   ------- ------- ---------------------------------
// ahanda      27-JAN-2000  110.5           Changed set_form_change to show no.
//                                          of payslips for pay prd and multi
//                                          assignment button.
// ahanda      17-JAN-2000  110.4           Removed setting of parameters for
//                                          Bottom Frame and moved it to
//                                          Middle Frame.
// ahanda      10-JAN-2000  110.3           Blank out bottom frame when the 
//                                          user performs any action.
// ahanda      24-DEC-1999  110.2           Changed next_payslip_onClick,
//                                          prev_payslip_onClick and 
//                                          set_form_change functions to
//                                          pass earned_date for accruals.
// ahanda      11-DEC-1999  110.1           Changed package call in the 
//                                          next_payslip_onClick and 
//                                          prev_payslip_onClick functions.
// ahanda      17-JUL-1999  110.0           Created.
// ==========================================================================
// $Header: pyuspysp.js 115.0 2000/02/28 15:45:20 pkm ship      $

// ------------------- Global Variables -------------------

// Detect  browser type and version
var Nav4 = ((navigator.appName == "Netscape") && 
            (parseInt(navigator.appVersion) == 4));
var browserVer = parseInt(navigator.appVersion);

// Flag to check whether we have submitted anything to server.
var submit_in_progress = "N";

// Place holder for sub window.
var subWindow;

var Win32;

if (Nav4) {
  Win32 = ((navigator.userAgent.indexOf("Win") != -1) && 
           (navigator.userAgent.indexOf("Win16") == -1));
} else {
  Win32 = ((navigator.userAgent.indexOf("Windows") != -1) && 
           (navigator.userAgent.indexOf("Windows 3.1") == -1));
}

// ------------------- Function Definitions -------------------

// Function which does nothing. Used in print function.
function do_nothing () {
  return true;
}


function print_gif_onClick () {
  // Single frame printing is not available for Mac.
  if (Win32) {
    if (Nav4) {
      top.container_middle.print();
    }
    else {
      // traps all script error messages hereafter until page reload
      window.onerror = do_nothing();
      // make sure desired frame has focus
      wind.focus();
      // change second parameter to 2 if you don't want the print dialog to appear
      IEControl.ExecWB(6, 1);
      }
  }
  else {
    alert (FND_MESSAGES['HR_CANNOT_PRINT_WEB']);
  }
}


// This function will reload the the container middle frame.
function reload_gif_onClick () {
  top.container_middle.history.go(0);
}


// This function will stop reloading the the container middle frame.
function stop_gif_onClick () {
  if (Nav4) {
    void top.container_middle.stop();
  }
  else if (Win32) {
    top.container_middle.focus();
    // requires same <OBJECT> as printing
    IEControl.ExecWB(23, 0);
  }
}


// This function will popup the help window.
var helpWindow;
function help_gif_onClick () {
 var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  p_url = '/OA_HTML/US/pyuspshp.htm';
  p_scrolling='Y';

 if (!helpWindow||helpWindow.closed){
        helpWindow=window.open(p_url,"","status=yes,menubar=yes,scrollbars=yes,toolbar=yes,location=yes,resizable=yes,alwaysRaised=yes,HEIGHT=500,WIDTH=500");
       helpWindow.opener=window;
 }
 else {
     helpWidnow.focus();
 }
}



// Function to generate a blank page with dark blue color.
function get_dark_blue_page () {
  return "<HTML><BODY BGCOLOR=#336699></BODY></HTML>"
}

// Function to generate a blank page with gray color.
function get_gray_page () {
  return "<HTML><BODY BGCOLOR=#CCCCCC></BODY></HTML>"
}


// This function will clear the container bottom frame so that buttons are
// not displayed when processing is going on in the container middle frame.
function clear_container_bottom_frame () {
  parent.container_bottom.document.open();
  parent.container_bottom.document.write (
    "<HTML>" +
    "<HEAD>" +
    "</HEAD>" +
    "<BODY BGCOLOR=#336699>" +
    "  <TABLE  WIDTH=100% CELLPADDING=0 CELLSPACING=0 BORDER=0>" +
    "  <TR BGCOLOR=#CCCCCC>" +
    "    <TD ALIGN=LEFT><IMG SRC=" + FND_IMAGES['FNDCTBL'] + " BORDER=0></TD>" +
    "    <TD WIDTH=100%><IMG SRC=" + FND_IMAGES['FNDPXG5'] + " BORDER=0></TD>" +
    "    <TD ALIGN=RIGHT><IMG SRC=" + FND_IMAGES['FNDCTBR'] + " BORDER=0></TD>" +
    "  </TR>" +
    "</TABLE>" +
    "</BODY>" +
    "</HTML>"
  );
  parent.container_bottom.document.close();
}

function blank_container_bottom_frame()
{
   parent.container_bottom.document.open();
   parent.container_bottom.document.write (
      "<HTML>" +
       "<HEAD>" +
       "</HEAD>" +
       "<BODY BGCOLOR=#336699>" +
        "<form name=PayslipBottom>" +
         "<INPUT TYPE=hidden NAME=p_multiple_assgn     VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_chk_multiple_assgn VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_payment_date       VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_employee_id        VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_assignment_id      VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_user_id            VALUE=>" +
         "<INPUT TYPE=hidden NAME=p_responsibility_id  VALUE=>" +
        "</FORM>" +
       "</BODY>" +
      "</HTML>"
      );
  parent.container_bottom.document.close();
}

// Function to refresh container_top and container_bottom frames.
function refresh_frames (p_container_top_frame, p_container_bottom_frame) {
  if (p_container_top_frame) {
    if (top.container_top.location.href != p_container_top_frame) {
      top.container_top.location.href = p_container_top_frame;
    }
  }
  if (p_container_bottom_frame) {
    if (top.container_bottom.location.href != p_container_bottom_frame) {
      top.container_bottom.location.href = p_container_bottom_frame;
    }
  }
}


// Function to check whether the given string is blank.
function is_blank (str) {
  for (var i = 0; i < str.length; i++) {
    var c = str.charAt(i);
    if ((c != ' ') && (c != '\n') && (c != '\t'))
      return false;
  }
  return true;
}


//Required to open a sub window
function openSubWindow(p_url, p_width, p_height, p_scrolling) {
  var openleft=100;
  var opentop=100;
  var scrollbar_attr;
  if (p_scrolling == "Y")
    {
     scrollbar_attr = ",scrollbars=yes";
    }
  else
    {
     scrollbar_attr = ",scrollbars=no";
    }
    
  if (!subWindow || subWindow.closed) {
    // center on the main window
    openleft = parseInt(window.screenX + ((window.outerWidth - p_width) / 2));
    opentop = parseInt(window.screenY + ((window.outerHeight - p_height) / 2));
    var attr = "screenX=" + openleft + ",screenY=" + opentop;
    attr += ",resizable=yes,width=" + p_width + ",height=" + p_height +
            scrollbar_attr;

    subWindow = window.open(p_url, 'sub_window', attr);
    subWindow.opener = window;
  }
  else {
    subWindow.focus();
  }
}

function checkMandatoryFields(input, requiredFields, fieldPrompts, msg) {
  var fieldCheck   = true;
  var fieldsNeeded = "\n" + msg + "\n\n\t";
  for(var fieldNum=0; fieldNum < requiredFields.length; fieldNum++) {
     // Check if it is a multi-row form.
     var e = input.elements[requiredFields[fieldNum]]; var obj_length = e.length;
     if ( obj_length < 1 || obj_length == null) {
         if ((e.value == "") || (e.value == null) || is_blank(e.value)) {
                fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                fieldCheck = false;
         }
     } else {
                 for ( var i = 0; i < e.length; i++) {
                     if ((e[i].value == "") ||
                            (e[i].value == null) || is_blank(e[i].value)) {
                        fieldsNeeded += fieldPrompts[fieldNum] + "\n\t";
                        fieldCheck = false;
                     }
                }
             }
     }
     // ALL REQUIRED FIELDS HAVE BEEN ENTERED
     if (fieldCheck == true) {
         return true;
     }
     // SOME REQUIRED FIELDS ARE MISSING VALUES
     alert(fieldsNeeded);
     return false;
 }
 
 
 
 
// Roll over image functions for onMouseOver and onMouseOut JavaScript events.
function set_onMouse_gif (p_document_name, p_image_name, p_gif_file) {
  if (browserVer >= 3 ) {
    eval(p_document_name + '.' + p_image_name + '.src=' + '"' + p_gif_file+'"');
  }
  else {
    return true;
  }
}


// Print the Payslip for the User Selected Date
function set_form_change(formlist)
{

     //Close the Sub window
     if (!subWindow || subWindow.closed) {
       //null 
     }
     else {
       subWindow.close();
     }

     var HiddenMiddleForm = top.frames.container_middle.document.forms.PayslipMiddle;

     for (var i = 0; i < formlist.length; i++)
     {
       if (formlist.options[i].selected)
       {
         var ld_pay_date = formlist.options[i].text
       }
     }

     //alert("Date Passed : " + ld_pay_date);
     var ln_tot_check  = 0;
     var l_index_flag = "Y";

     for (i=0; i < top.p_array_length; i++)
     {
       if (ld_pay_date == top.paydata[i].payment_date)
       {
         ln_tot_check = ln_tot_check + 1;
         if (l_index_flag = "Y")
         {
           top.p_cur_array_index = i;

           HiddenMiddleForm.p_chk_multiple_assgn.value = "Y"
           HiddenMiddleForm.p_payment_date.value = top.paydata[i].payment_date
           HiddenMiddleForm.p_earned_date.value  = top.paydata[i].earned_date
           HiddenMiddleForm.p_assignment_action_id.value = top.paydata[i].assignment_action_id
           l_index_flag = "N"
         }
       }
     }

     //Set variable for Multiple Assignment
     if (ln_tot_check > 1) {
       top.p_massignment = "Y"
     }

     //Total Check for that Period
     top.p_mcheck_max_count = ln_tot_check;
     top.p_mcheck_cur_count = ln_tot_check;

     HiddenMiddleForm.action = "pay_us_emp_payslip_frame_web.container_middle_frame";

     //Blank out the bottom frame whenever the user Click on any button
     blank_container_bottom_frame();

     HiddenMiddleForm.submit();

}


// Print the Next Payslip when the user Clicks on Next Button
function next_payslip_onClick()
{

     //Close the Sub window
     if (!subWindow || subWindow.closed) {
       //null 
     }
     else {
       subWindow.close();
     }

     var HiddenMiddleForm = top.frames.container_middle.document.forms.PayslipMiddle;

     var l_arr_curval = top.p_cur_array_index;
     var l_arr_nextval = 0;
     var ln_tot_check  = 0;

     if (l_arr_curval < (parseInt(top.p_array_length)-1))
     {
        l_arr_nextval = parseInt(l_arr_curval) + 1;
        top.p_cur_array_index = l_arr_nextval;

        if ( HiddenMiddleForm.p_payment_date.value == top.paydata[l_arr_nextval].payment_date)
        {
           top.p_mcheck_cur_count = parseInt(top.p_mcheck_cur_count) + 1;
           HiddenMiddleForm.p_chk_multiple_assgn.value = "N"
        }
        else
        {
           var ld_pay_date = top.paydata[l_arr_nextval].payment_date;

           for (i=l_arr_nextval; i < top.p_array_length; i++)
           {
               if (ld_pay_date == top.paydata[i].payment_date)
               {
                  ln_tot_check = ln_tot_check + 1;
               }
               else
               {
                  break
               }
           }

           //Total Check for that Period
           top.p_mcheck_max_count = ln_tot_check;
           top.p_mcheck_cur_count = 1;

           HiddenMiddleForm.p_chk_multiple_assgn.value = "Y"
           //Set variable for Multiple Assignment to N as it will be checked.
           top.p_massignment = "N"

        }

        //Blank out the bottom frame whenever the user Click on any button
        blank_container_bottom_frame();

        HiddenMiddleForm.p_payment_date.value = top.paydata[l_arr_nextval].payment_date
        HiddenMiddleForm.p_earned_date.value  = top.paydata[l_arr_nextval].earned_date
        HiddenMiddleForm.p_assignment_action_id.value = top.paydata[l_arr_nextval].assignment_action_id

        HiddenMiddleForm.action = "pay_us_emp_payslip_frame_web.container_middle_frame";
        HiddenMiddleForm.submit();

     }
     else
     {
         alert (FND_MESSAGES['PAY_NO_EMP_ASG_PAYSLIP_WEB']);
         //alert("There are no more Payslip for this Assignment.");
     }

}//End of function


// Print the Previous Payslip when the user Clicks on Back Button
function prev_payslip_onClick()
{

     //Close the Sub window
     if (!subWindow || subWindow.closed) {
       //null 
     }
     else {
       subWindow.close();
     }

     var HiddenMiddleForm = top.frames.container_middle.document.forms.PayslipMiddle;

     var l_arr_curval = top.p_cur_array_index;
     var l_arr_nextval = 0;
     var ln_tot_check  = 0;

     //alert("Value of p_cur_array_index : " + top.p_cur_array_index);
     if (l_arr_curval > 0 ) //(parseInt(top.p_array_length)-1))
     {
        l_arr_nextval = parseInt(l_arr_curval) - 1;
        top.p_cur_array_index = l_arr_nextval;

        if ( HiddenMiddleForm.p_payment_date.value == top.paydata[l_arr_nextval].payment_date)
        {
           top.p_mcheck_cur_count = parseInt(top.p_mcheck_cur_count) - 1;
           HiddenMiddleForm.p_chk_multiple_assgn.value = "N"
        }
        else
        {
           var ld_pay_date = top.paydata[l_arr_nextval].payment_date;

           for (i=l_arr_nextval; i >= 0; i--)
           {
               if (ld_pay_date == top.paydata[i].payment_date)
               {
                  ln_tot_check = ln_tot_check + 1;
               }
               else
               {
                  break
               }
           }

           //Total Check for that Period
           top.p_mcheck_max_count = ln_tot_check;
           top.p_mcheck_cur_count = ln_tot_check;

           HiddenMiddleForm.p_chk_multiple_assgn.value = "Y"
           //Set variable for Multiple Assignment to N as it will be checked.
           top.p_massignment = "N"

        }

        //Blank out the bottom frame whenever the user Click on any button
        blank_container_bottom_frame();

        HiddenMiddleForm.p_payment_date.value = top.paydata[l_arr_nextval].payment_date
        HiddenMiddleForm.p_earned_date.value  = top.paydata[l_arr_nextval].earned_date
        HiddenMiddleForm.p_assignment_action_id.value = top.paydata[l_arr_nextval].assignment_action_id

        HiddenMiddleForm.action = "pay_us_emp_payslip_frame_web.container_middle_frame";

        HiddenMiddleForm.submit();

     }
     else
     {
        alert (FND_MESSAGES['PAY_NO_EMP_ASG_PAYSLIP_WEB']);
        //alert("There are no more Payslip for this Assignment.");
     }
}//End of function


// The following function is invoked from the
// header frame when the user clicks on the
// ''Main Menu'' gif in the toolbar.
function main_menu_gif_onClick (msg) {
      top.location.href = 'hr_util_disp_web.hr_main_menu_page';
    }


// The following function is invoked from the
// middle frame once it has been loaded
function loadbottom()
{
   var HiddenBottomForm = top.frames.container_bottom.document.forms.PayslipBottom;

   HiddenBottomForm.p_employee_id.value        = top.p_employee_id

   HiddenBottomForm.p_assignment_id.value      = top.p_assignment_id
   HiddenBottomForm.p_user_id.value            = top.p_user_id
   HiddenBottomForm.p_responsibility_id.value  = top.p_responsibility_id

   HiddenBottomForm.p_multiple_assgn.value     = top.p_massignment

   HiddenBottomForm.p_payment_date.value       = top.frames.container_middle.document.forms.PayslipMiddle.p_payment_date.value
   HiddenBottomForm.p_chk_multiple_assgn.value = top.frames.container_middle.document.forms.PayslipMiddle.p_chk_multiple_assgn.value

   HiddenBottomForm.action = "pay_us_emp_payslip_frame_web.build_container_bottom_frame";
   HiddenBottomForm.submit();

}//End of function

<!-- Done hiding from browsers which cannot handle JavaScript. -->
