//  +================================================+
//  |    Copyright(c) 1999 Oracle Corporation        |
//  |      Redwood Shores, California, USA           |
//  |           All rights reserved.                 |
//  +================================================+
//  | Name:        pawtupld.js                       |
//  |                                                |
//  | Description: This is the standalone JavaScript |
//  |              file for SST upload screen        |
//  |                                                |
//  | History:     Created by Anitha Andra           |
//  |                                                |
//  +================================================+
/* $Header: pawtupld.js 115.10 2001/03/23 17:40:53 pkm ship      $ */

//  --------------------------------------------------
//  Function: fInitUpload()
//  Description:  
//
//  --------------------------------------------------
function fInitUpload() {
  top.g_objTimecard = new Timecard();
  fDrawToolbarFrame(top.framToolbar, top.C_strUPLOAD);
  fDrawUploadFrame(top.framMainBody, top.g_objEmployees);
  fDrawButtonFrame(top.framButtons, top.C_strUPLOAD);
}

function fDrawUploadEmployees(p_framTarget, p_objEmployees) {

  // Assumption is that the first employee is the employee logged in
  // Hence set the hidden fields hidPersonId and hidPErsonName that reflect the preparer information
  p_framTarget.document.writeln('<input type=hidden name="hidPersonId" value="' +
				p_objEmployees.arrEmployee[0].mGetiID() + '">');
  p_framTarget.document.writeln('<input type=hidden name="hidPersonName" value="' +
				p_objEmployees.arrEmployee[0].mGetstrName() + '">');


  if (p_objEmployees.miGetNumEmployees()==1) {   //Single Employee
    p_framTarget.document.writeln('<B>' + p_objEmployees.arrEmployee[0].mGetstrName() +  ' (' + 
					  p_objEmployees.arrEmployee[0].mGetiNumber() + ')</B>');
    p_framTarget.document.writeln('<input type=hidden name="popEmpId" value="' + 
					  p_objEmployees.arrEmployee[0].mGetiID() + '">' );
  }
  else {    //third party Entry
    p_framTarget.document.writeln('<select name = popEmpId>');
    //Set the default selected
    p_framTarget.document.writeln('<option SELECTED value="' + p_objEmployees.arrEmployee[0].mGetiID() + '">' +
                                    '<B>' + p_objEmployees.arrEmployee[0].mGetstrName() + ' (' + 
				     p_objEmployees.arrEmployee[0].mGetiNumber() + ')</B>');
    for (var i=1; i<p_objEmployees.miGetNumEmployees(); i++) {
      p_framTarget.document.writeln('<option value="' + p_objEmployees.arrEmployee[i].mGetiID() + '">' +
                                    '<B>' + p_objEmployees.arrEmployee[i].mGetstrName() + ' (' + 
				     p_objEmployees.arrEmployee[1].mGetiNumber() + ')</B>');
    }
    p_framTarget.document.writeln('</select>');   
  }
}



function fDrawUploadFrame(p_framTarget, p_objEmployees) {

  var l_objCurRegion = top.g_objAKRegions.mobjGetRegion("PA_WEB_TIME_UPLOAD");

  p_framTarget.document.open();
  p_framTarget.document.writeln('<HTML DIR="' + g_strDirection + '">');
  p_framTarget.document.writeln('<BASE HREF="' + top.g_strSessBaseHRef  + '">');
  p_framTarget.document.writeln('<HEAD>');
  p_framTarget.document.writeln('<LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawscabo.css"></LINK>');
  p_framTarget.document.writeln('<LINK REL=STYLESHEET TYPE="text/css" href="' + g_strHTMLLangPath + 'pawstime.css"></LINK></HEAD>');

  p_framTarget.document.writeln('<BODY class=panel>');
  p_framTarget.document.writeln('<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2 WIDTH="100%">');
  p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK>' + 
				g_objFNDMsg.mstrGetMsg("PA_WEB_UPLOAD_SCREEN_HELP") + '</TD></TR>');

  p_framTarget.document.writeln('<FORM NAME="formUploadTimecard" METHOD=post ACTION="' + top.g_strSessDAD  + '/PA_SELF_SERVICE_UPLOAD_PVT.PARSE_TIMECARD"> ');
 
  p_framTarget.document.writeln('<TR><TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2 WIDTH="50%">');
  // Draw Employees (includes setting hidden fields hidPersonId and hidPersonName)
  p_framTarget.document.writeln('<TR><TD CLASS=PROMPTBLACKRIGHT width=200 nowrap>' + 
				l_objCurRegion.mobjGetRegionItem("PA_WEB_EMPLOYEE").mGetstrLabel() + '</TD> ');
  p_framTarget.document.writeln('    <TD CLASS=DATABLACK colspan=2 nowrap>');

  fDrawUploadEmployees(p_framTarget, p_objEmployees);

  p_framTarget.document.writeln('</TD></TR>');

  // Draw Upload Spreadsheet Area
  p_framTarget.document.writeln('<TR valign=center><TD VALIGN="TOP" CLASS=PROMPTBLACKRIGHT width=200>' +
                           l_objCurRegion.mobjGetRegionItem("PA_WEB_UPLOAD_AREA").mGetstrLabel()  + '</TD>');
  p_framTarget.document.writeln('<TD CLASS=DATABLACK colspan=2 nowrap>' +
		'<textarea name="txaTime" rows=2 cols=20 wrap=VIRTUAL scrolling=no></textarea></TD></TR>');

  p_framTarget.document.writeln('</TABLE></TD></TR>');
  p_framTarget.document.writeln('</FORM>');
  p_framTarget.document.writeln('</TABLE>');

  p_framTarget.document.writeln('<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2 WIDTH="100%">');
  p_framTarget.document.writeln('<TR><TD>&nbsp</TD></TR>');
  p_framTarget.document.writeln('<TR><TD CLASS=INFOBLACK><B>' + 
		l_objCurRegion.mobjGetRegionItem("PA_WEB_INSTRUCTIONS").mGetstrLabel()+ '</B></TD></TR>');
  p_framTarget.document.writeln('<TR><TD><IMAGE src=' + g_strImagePath +'FNDPX3.gif width=98% height=2></TD></TR>');
  p_framTarget.document.writeln(g_objFNDMsg.mstrGetMsg("PA_WEB_UPLOAD_INSTRUCTIONS"));
  p_framTarget.document.writeln('</TABLE>');

  p_framTarget.document.writeln('</HTML>');
  p_framTarget.document.close();
}

function fOnClickClearUpload(p_objForm) {
  p_objForm.txaTime.value="";

  if (top.g_objEmployees.miGetNumEmployees() > 1) {
    for (var index=0; index < p_objForm.popEmpId.length; index++) {
      if (p_objForm.popEmpId.options[index].defaultSelected) {
        p_objForm.popEmpId.options[index].selected = true;
        break;
      }
    }
  }
   return;
}

function fOnClickValidate(p_objForm) {
  if (p_objForm.txaTime.value == "")  {
    alert(g_objFNDMsg.mstrGetMsg("PA_WEB_UPLOAD_EMPTY_TIMECARD"));
  }
  else {
    p_objForm.submit();
  }
}

function fOnClickDownload() {
  open(top.g_strSessBaseHRef + top.g_strHTMLLangPath + "pawtimes.xls", "_top");
}


function fIsSetupErrorsPresent(p_objErrors) {
 for (var i=0; i<p_objErrors.miGetNumOfErrors() ; i++) {
   if (p_objErrors.arrErrors[i].mbIsSetupError())
     return true;
 } 
 return false;
}


function fUploadControlSSToCSContinue(p_objDataErrors, p_objSetupErrors) {

  var l_strMesg;

  if (p_objSetupErrors.mbContainsErrors()) {  // Spreadsheet setup errors and / or warnings
    if (fIsSetupErrorsPresent(p_objSetupErrors)) {  // In case of setup errors (and warnings), draw errors window
      fDrawUploadFrame(top.framMainBody, top.g_objEmployees);
      fOpenErrorWin(p_objSetupErrors);
      return;
    }
    else {  // In case of setup warnings only, just alert the user for each warning.
      // Loop thru each warning
      for (var i=0 ; i<p_objSetupErrors.miGetNumOfErrors(); i++) {
        alert(p_objSetupErrors.arrErrors[i].mGetstrMessage());
      }
    }
  }  // Setup errors/warnings

  if (p_objDataErrors.arrErrors.length != 0) {  // Validation errors
    l_strMesg = g_objFNDMsg.mstrGetMsg("PA_WEB_UPLOAD_FAILURE");
    l_strMesg = fstrReplToken(l_strMesg, "&NUM_ERRORS", p_objDataErrors.arrErrors.length);
  }
  else {  // No Validation Errors
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) 
      l_strMesg = g_objFNDMsg.mstrGetMsg("PA_WEB_AUDIT_UPLOAD_SUCCESS");
    else
      l_strMesg = g_objFNDMsg.mstrGetMsg("PA_WEB_UPLOAD_SUCCESS");
  }

  if (confirm(l_strMesg)) {
    // Check whether Audit is set to FULL, if so, then go straight
    // to the Audit screen
    if (g_objProfOpt.mvGetProfOpt("PA_ONLINE_EXP_AUDIT") ==C_strFULL) {
      top.g_strCurrentAction = top.C_strUPLOAD;
      top.fControlSSToCSContinue(top.g_objTimecard, top.g_objErrors, top.C_strAUDIT, top.C_strUPLOAD, top.g_objAuditHistory, top.g_objDeletedAuditHistory);
    } 
    else { // otherwise, go to the final review screen
      top.g_strCurrentAction = top.C_strUPLOAD;
      top.fControlSSToCSContinue(top.g_objTimecard, top.g_objErrors, top.C_strFINAL_REVIEW, top.C_strUPLOAD, top.g_objAuditHistory, top.g_objDeletedAuditHistory);
    }
  }
  else {
    fDrawUploadFrame(top.framMainBody, top.g_objEmployees);
  }
}

function fOnClickExitUpload(p_objForm) {

  // Set confirm to false if the employee or spreadsheet upload area has been changed
  if ((top.g_objEmployees.miGetNumEmployees()==1) && (p_objForm.txaTime.value=="")) {
    fExitTimecard(false);  
  }
  else if ((top.g_objEmployees.miGetNumEmployees()>1) && 
          (p_objForm.popEmpId.options[p_objForm.popEmpId.selectedIndex].value==p_objForm.hidPersonId) && 
          (p_objForm.txaTime.value=="")) {
    fExitTimecard(false);
  }
  else {
    fExitTimecard(true);
  }

}
